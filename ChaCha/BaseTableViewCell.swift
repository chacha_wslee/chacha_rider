//
//  BaseTableViewCell.swift
//  Taxi
//
//  Created by wslee-chacha on 2018. 7. 26..
//  Copyright © 2018년 chachacreation.co.kr. All rights reserved.
//

import UIKit

class BaseTableViewCell: UITableViewCell {
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
