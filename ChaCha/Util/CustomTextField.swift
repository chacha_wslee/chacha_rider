//
//  CustomTextField.swift
//  Taxi
//
//  Created by wslee-chacha on 2018. 7. 23..
//  Copyright © 2018년 chachacreation.co.kr. All rights reserved.
//

import UIKit

class CustomTextField: UITextField {

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, UIEdgeInsetsMake(0, CommonObject.sizeByResolution(size: 15.0), 0, 0))
    }
    
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, UIEdgeInsetsMake(0, CommonObject.sizeByResolution(size: 15.0), 0, 0))
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, UIEdgeInsetsMake(0, CommonObject.sizeByResolution(size: 15.0), 0, 0))
    }
}
