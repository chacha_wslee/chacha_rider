//
//  GradientView.swift
//  Taxi
//
//  Created by wslee-chacha on 2018. 8. 3..
//  Copyright © 2018년 chachacreation.co.kr. All rights reserved.
//

import UIKit

@IBDesignable
class GradientView: UIView {
    
    @IBInspectable var startColor: UIColor = .clear
    @IBInspectable var endColor: UIColor = .clear

    override func draw(_ rect: CGRect) {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = CGRect(x: 0, y: 0, width: frame.size.width, height: frame.size.height)
        gradient.colors = [startColor.withAlphaComponent(0.1).cgColor,
                           startColor.withAlphaComponent(0.3).cgColor,
                           startColor.withAlphaComponent(0.5).cgColor,
                           startColor.withAlphaComponent(0.6).cgColor,
                           startColor.withAlphaComponent(0.7).cgColor,
                           endColor.withAlphaComponent(0.8).cgColor,
                           endColor.withAlphaComponent(0.9).cgColor,
                           endColor.withAlphaComponent(1.0).cgColor,
                           endColor.withAlphaComponent(1.0).cgColor]
        layer.insertSublayer(gradient, at: 0)
    }
}
