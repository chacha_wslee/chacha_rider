//
//  CommonObject.swift
//  Taxi
//
//  Created by wslee-chacha on 2018. 7. 18..
//  Copyright © 2018년 chachacreation.co.kr. All rights reserved.
//

import UIKit
import CoreLocation

class CommonObject: NSObject {
    
    static func setDeviceId(deviceId: String)
    {
        UserDefaults.standard.set(deviceId, forKey: USER.KEY.DEVICE_ID)
        UserDefaults.standard.synchronize()
    }
    
    static func deviceId() -> String?
    {
        return UserDefaults.standard.string(forKey: USER.KEY.DEVICE_ID)
    }
    
    static func setPushToken(pushToken: String)
    {
        UserDefaults.standard.set(pushToken, forKey: USER.KEY.PUSH_TOKEN)
        UserDefaults.standard.synchronize()
    }
    
    static func pushToken() -> String?
    {
        return UserDefaults.standard.string(forKey: USER.KEY.PUSH_TOKEN)
    }
    
    static func setPhoneNumber(phoneNumber: String)
    {
        UserDefaults.standard.set(phoneNumber, forKey: USER.KEY.PHONE_NUMBER)
        UserDefaults.standard.synchronize()
    }
    
    static func phoneNumber() -> String?
    {
        return UserDefaults.standard.string(forKey: USER.KEY.PHONE_NUMBER)
    }
    
    static func setUserToken(userToken: String)
    {
        UserDefaults.standard.set(phoneNumber, forKey: USER.KEY.USER_TOKEN)
        UserDefaults.standard.synchronize()
    }
    
    static func userToken() -> String?
    {
        return UserDefaults.standard.string(forKey: USER.KEY.USER_TOKEN)
    }
    
    static func numberToString(number: NSNumber) -> NSString
    {
        return number.stringValue as NSString
    }
    
    static func dataToString(data: Any? = nil) -> String
    {
        guard data != nil else {
            return ""
        }
        
        if data is Int {
            return String.init(format: "%d", data as! Int)
        } else if data is String {
            return data as! String
        } else {
            return ""
        }
    }
    
    static func dataToInt(data: Any? = nil) -> Int
    {
        guard data != nil else {
            return 0
        }
        
        if data is Int {
            return data as! Int
        } else if data is Float {
            return Int(data as! Float)
        } else if data is Double {
            return Int(data as! Double)
        } else if data is String {
            return Int(data as! String) ?? 0
        } else {
            return 0
        }
    }
    
    static func roundToPlaces(_ number:Double, toPlaces places:Int) -> Double
    {
        let divisor = pow(10.0, Double(places))
        return (number * divisor).rounded() / divisor
    }
    
    static func isEqualLocation(first firstCoord: CLLocationCoordinate2D, second secondCoord: CLLocationCoordinate2D) -> Bool
    {
        if roundToPlaces(firstCoord.latitude, toPlaces: 6) == roundToPlaces(secondCoord.latitude, toPlaces: 6) && roundToPlaces(firstCoord.longitude, toPlaces: 6) == roundToPlaces(secondCoord.longitude, toPlaces: 6) {
            return true
        } else {
            return false
        }
    }
    
    static func locationToFormattedString(location: CLLocationCoordinate2D) -> String
    {
        return String.init(format: "%@,%@", location.latitude.description, location.longitude.description)
    }
    
    static func dataToLocation(data: String) -> CLLocationCoordinate2D
    {
        let locations = data.components(separatedBy: ",")
        if let first = locations.first, let last = locations.last {
            if let latitude = CLLocationDegrees.init(first), let longitude = CLLocationDegrees.init(last) {
                return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
            }
        }
        
        return kCLLocationCoordinate2DInvalid
    }
    
    static func locationFromFormattedString(latitude: String, longitude: String) -> CLLocationCoordinate2D
    {
        guard latitude.count > 0, longitude.count > 0 else {
            return kCLLocationCoordinate2DInvalid
        }
        let lat = (latitude as NSString).doubleValue
        let lng = (longitude as NSString).doubleValue
        
        return CLLocationCoordinate2D(latitude: lat, longitude: lng)
    }
    
    static func stringToYYYYMMDDHHMMSS(string: String) -> NSDate?
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.locale = Locale(identifier: "ko_KR")
        return dateFormatter.date(from: CommonObject.dataToString(data: string)) as NSDate?
    }
    
    static func yearFromDate(date: NSDate) -> Int
    {
        let components = NSCalendar.current.dateComponents([.day, .month, .year], from: date as Date)
        return components.year ?? 0
    }
    
    static func monthFromDate(date: NSDate) -> Int
    {
        let components = NSCalendar.current.dateComponents([.day, .month, .year], from: date as Date)
        return components.month ?? 0
    }
    
    static func dayFromDate(date: NSDate) -> Int
    {
        let components = NSCalendar.current.dateComponents([.day, .month, .year], from: date as Date)
        return components.day ?? 0
    }
    
    static func hourFromDate(date: NSDate) -> Int
    {
        let components = NSCalendar.current.dateComponents([.hour], from: date as Date)
        return components.day ?? 0
    }
    
    static func colorByHex(rgb: Int) -> UIColor
    {
        return UIColor.init(red: (CGFloat((rgb>>16)&0xFF))/255.0, green: (CGFloat((rgb>>8)&0xFF))/255.0, blue: (CGFloat(rgb&0xFF))/255.0, alpha: 1.0)
    }
    
    static func sizeByResolution(size: CGFloat) -> CGFloat
    {
        return size * UIScreen.main.bounds.size.width / 375.0
    }
    
    static func setBorderLine(view: UIView)
    {
        view.layer.backgroundColor = UIColor.white.cgColor
        view.layer.borderColor = UIColor.black.cgColor
        view.layer.borderWidth = 1.0
    }
    
    static func setCornerRadius(view: UIView, _ value: CGFloat)
    {
        view.layer.cornerRadius = value
    }
    
    static func setShapeCircle(view: UIView)
    {
        view.layer.cornerRadius = sizeByResolution(size: view.frame.size.width) / 2
        view.clipsToBounds = true
    }
    
    static func addShadow(view: UIView)
    {
        view.clipsToBounds = false
        view.layer.masksToBounds = false
        
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOpacity = 0.6
        view.layer.shadowRadius = 4.0
        view.layer.shadowOffset = CGSize(width: 3.0, height: 3.0)
    }
    
    static func popupWithOneButton(target: UIViewController, title: String, message: String, handler:((UIAlertAction) -> Void)? = nil)
    {
        let alertController = UIAlertController.init(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction.init(title: "확인", style: .default, handler: handler)
        alertController.addAction(okAction)
        target.present(alertController, animated: true, completion: nil)
    }
    
    static func popupWithTwoButton(target: UIViewController, title: String, message: String, handler:((UIAlertAction) -> Void)? = nil)
    {
        let alertController = UIAlertController.init(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction.init(title: "확인", style: .default, handler: handler)
        let cancelAction = UIAlertAction.init(title: "취소", style: .cancel, handler: nil)
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        target.present(alertController, animated: true, completion: nil)
    }
}
