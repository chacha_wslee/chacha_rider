//
//  Constants.swift
//  Taxi
//
//  Created by wslee-chacha on 2018. 7. 18..
//  Copyright © 2018년 chachacreation.co.kr. All rights reserved.
//

import Foundation
import UIKit

struct APPURL {
    private struct Domains {
        static let Development = "http://api.tb.chachacreation.co.kr:80/ccapi/1.3"
        static let Distribution = ""
    }
    
    static let BaseURL = Domains.Development
}

struct API {
    static let kLogin = "UR110"
    static let kSendToken = "UR112"
    static let kRequestDriver = "UQ002"
    static let kOrderPreview = "UQ101"
    static let kOrderMatching = "UQ100"
    static let kPickUpState = "UQ200"
    static let kTripState = "UQ300"
    static let kOrder = "UD120"
    static let kOrderCancelByRider = "UD130"
    static let kPickupCancelByRider = "UD230"
    static let kOrderTimeOut = "UD131"
    static let kRatingDriver = "UD421"
    static let kUserProfileImage = "UR601"
    static let kDriverProfileImage = "UR602"
    static let kVehicleProfileImage = "UR603"
}

struct KAKAO {
    static let KEY = "402bbdfd947aa67418ed198ed657d231"
    static let SECRET = "2VdLSYMct51XddaKETXUhY4ecL2xwnZE"
}

struct IGAWORKS {
    static let KEY = "908293562"
    static let HASHKEY = "b18d3149d9ed47da"
}

struct GOOGLE {
    static let HTTPKEY = "AIzaSyA3CSa6BWsRdfIVHtO5GfvVUfrb4lZq56E"
    static let APIKEY = "AIzaSyA3CSa6BWsRdfIVHtO5GfvVUfrb4lZq56E"
    static let PLACEAPIKEY = "AIzaSyA3CSa6BWsRdfIVHtO5GfvVUfrb4lZq56E"
}

struct SERVICE {
    static let E = "E"
    static let X = "X"
    static let XL = "L"
}

struct TMAP {
    struct URL {
        static let ROUTE = "https://api2.sktelecom.com/tmap/routes"
        static let GEOCODE = "https://api2.sktelecom.com/tmap/geo/reversegeocoding"
        static let POI = "https://api2.sktelecom.com/tmap/pois"
    }
    static let APIKEY = "8bc4facb-74be-4bbf-9073-919d3a50b70a"
    struct PARAMETERS {
        static let VERSION = "version"
        static let VERSION_1 = "1"
        static let STARTX = "startX"
        static let STARTY = "startY"
        static let ENDX = "endX"
        static let ENDY = "endY"
        static let REQ_COORD = "reqCoordType"
        static let RES_COORD = "resCoordType"
        static let MAP_TYPE = "mapType"
        static let COORD_TYPE = "WGS84GEO"
        static let LAT = "lat"
        static let LON = "lon"
        static let ADDRESS_TYPE = "addressType"
        static let ADDRESS_A10 = "A10"
        static let SEARCH_KEYWORD = "searchKeyword"
        static let COUNT = "count"
        static let COUNT_VAL = "5"
        static let PAGE = "page"
        static let PAGE_VAL = "1"
        static let APPKEY = "appKey"
    }
    struct RESPONSE {
        struct ROUTE {
            
        }
        
        struct GEOCODE {
            static let ADRESS_INFO = "addressInfo"
            static let FULLADDRESS = "fullAddress"
            static let ADDRESS_TYPE = "addressType"
            static let CITY_DO = "city_do"
            static let GU_GUN = "gu_gun"
            static let EUP_MYUN = "eup_myun"
            static let ADMIN_DONG = "adminDong"
            static let ADMIN_DONG_CODE = "adminDongCode"
            static let LEGAL_DONG = "legalDong"
            static let LEGAL_DONG_CODE = "legalDongCode"
            static let RI = "ri"
            static let ROADNAME = "roadName"
            static let BUILDING_INDEX = "buildingIndex"
            static let BUILDING_NAME = "buildingName"
            static let MAPPING_DISTANCE = "mappingDistance"
            static let ROAD_CODE = "roadCode"
            static let BUNJI = "bunji"
        }
    }
}

struct TARGET {
    static var isSimulator: Bool {
        return TARGET_OS_SIMULATOR != 0
    }
    static let SE = "iPhone_se"
    static let NORMAL = "iPhone_8"
    static let PLUS = "iPhone_8Plus"
    static let X = "iPhone_X"
    static let IPAD = "iPad"
}

struct USER {
    struct KEY {
        static let DEVICE_ID = "diviceId"
        static let PUSH_TOKEN = "pushToken"
        static let PHONE_NUMBER = "phoneNumber"
        static let USER_TOKEN = "userToken" // "datoken" in server
    }
}

func FUNCLOG(functionName: NSString) {
    print("ws.lee : [\(functionName)]")
}

func MODEL() -> String {
    let screenHeight = UIScreen.main.bounds.size.height
    if (screenHeight == 568.0) {
        return TARGET.SE
    } else if (screenHeight == 667.0) {
        return TARGET.NORMAL
    } else if (screenHeight == 736.0) {
        return TARGET.PLUS
    } else if (screenHeight == 812.0) {
        return TARGET.X
    } else {
        return TARGET.IPAD
    }
}
