//
//  Marker.swift
//  Taxi
//
//  Created by wslee-chacha on 2018. 7. 27..
//  Copyright © 2018년 chachacreation.co.kr. All rights reserved.
//

import UIKit
import GoogleMaps

enum MARKER_TYPE: Int {
    case START = 0
    case END
    case DRIVER_E
    case DRIVER_X
    case DRIVER_XL
}

class Marker: GMSMarker {
    convenience init(_ with: MARKER_TYPE, where position: CLLocationCoordinate2D) {
        self.init()
        let icon: UIImage?
        if with == .START {
            icon = UIImage.init(named: "icon_pin_start")
        } else if with == .END {
            icon = UIImage.init(named: "icon_pin_end")
        } else if with == .DRIVER_E {
            icon = UIImage.init(named: "img_mapcar_E")
        } else if with == .DRIVER_X {
            icon = UIImage.init(named: "img_mapcar_X")
        } else if with == .DRIVER_XL {
            icon = UIImage.init(named: "img_mapcar_L")
        } else {
            icon = UIImage.init(named: "icon_fav_star01")
        }
        self.icon = icon
        self.position = position
    }
    
    func setMarkerTo(map: GMSMapView)
    {
        self.map = map
    }
    
    func setMarkerPosition(_ position: CLLocationCoordinate2D)
    {
        self.position = position
    }
    
    func rotateMarker(from: CLLocationCoordinate2D, to: CLLocationCoordinate2D)
    {
        let heading = GMSGeometryHeading(from, to)
        self.groundAnchor = CGPoint.init(x: 0.5, y: 0.5)
        self.rotation = heading
    }
    
    func animateMarker(to position: CLLocationCoordinate2D)
    {
        CATransaction.begin()
        CATransaction.setAnimationDuration(10)
        self.position = position
        CATransaction.commit()
    }
}
