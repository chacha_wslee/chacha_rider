//
//  CustomButton.swift
//  Taxi
//
//  Created by wslee-chacha on 2018. 7. 24..
//  Copyright © 2018년 chachacreation.co.kr. All rights reserved.
//

import UIKit

@IBDesignable class CustomButton: UIView {
    
    @IBInspectable var buttonText: String?
    @IBInspectable var buttonImage: UIImage?
    var label: UILabel?
    var button: UIButton?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        label = UILabel.init()
        label?.text = buttonText
        button = UIButton.init()
        button?.setBackgroundImage(buttonImage, for: .normal)
        addSubview(label!)
        addSubview(button!)
        bringSubview(toFront: button!)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        label?.frame = frame
        button?.frame = frame
    }
}
