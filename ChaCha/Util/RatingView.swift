//
//  RatingView.swift
//  Taxi
//
//  Created by wslee-chacha on 2018. 8. 6..
//  Copyright © 2018년 chachacreation.co.kr. All rights reserved.
//

import UIKit

@IBDesignable
class RatingView: UIView {

    @IBInspectable var maxRatingCount: Int = 5
    @IBInspectable var ratingOffImageView: UIImage?
    @IBInspectable var ratingOnImageView: UIImage?
    @IBInspectable var selectable: Bool = true
    var ratingButtons = [UIButton]()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setControls()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setControls()
    }
    
    func setControls()
    {
        guard let imgSize = ratingOnImageView?.size else {
            return
        }
        
        ratingButtons.removeAll()
        let spacing = (frame.size.width - imgSize.width * CGFloat.init(maxRatingCount)) / CGFloat.init(maxRatingCount - 1)
        for index in 0..<maxRatingCount {
            let frame = CGRect.init(x: CGFloat.init(index) * imgSize.width + CGFloat.init(index) * spacing, y: 0, width: imgSize.width, height: imgSize.height)
            let button = UIButton.init(type: .custom)
            button.frame = frame
            button.isSelected = false
            button.adjustsImageWhenHighlighted = false
            button.setImage(ratingOffImageView, for: .normal)
            button.setImage(ratingOnImageView, for: .highlighted)
            button.setImage(ratingOnImageView, for: .selected)
            if selectable {
                button.addTarget(self, action: #selector(onButtonSelected(button:)), for: .touchUpInside)
            }
            addSubview(button)
            ratingButtons.append(button)
        }
    }
    
    @objc func onButtonSelected(button: UIButton)
    {
        for index in 0..<ratingButtons.count {
            let btn = ratingButtons[index]
            if btn.isEqual(button) {
                if btn.isSelected {
                    for idx in index+1..<ratingButtons.count {
                        ratingButtons[idx].isSelected = false
                    }
                } else {
                    for idx in 0...index {
                        ratingButtons[idx].isSelected = true
                    }
                }
            }
        }
        print("rating: \(getRating())")
    }
    
    func setRating(point: Int)
    {
        guard point <= ratingButtons.count else {
            return
        }
        
        for index in 0...point {
            ratingButtons[index].isSelected = true
        }
    }
    
    func getRating() -> Int
    {
        guard ratingButtons.count > 0 else {
            return 0
        }
        
        for index in 0..<ratingButtons.count {
            if !ratingButtons[index].isSelected {
                return index
            }
        }
        return ratingButtons.count
    }

}
