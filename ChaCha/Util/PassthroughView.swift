//
//  PassthroughView.swift
//  Taxi
//
//  Created by wslee-chacha on 2018. 7. 26..
//  Copyright © 2018년 chachacreation.co.kr. All rights reserved.
//

import UIKit

class PassthroughView: UIView {

    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        for subview in subviews {
            if !subview.isHidden && subview.isUserInteractionEnabled && subview.point(inside: convert(point, to: subview), with: event) {
                return true
            }
        }
        return false
    }
}
