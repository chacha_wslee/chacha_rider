//
//  Color.swift
//  Taxi
//
//  Created by wslee-chacha on 2018. 8. 8..
//  Copyright © 2018년 chachacreation.co.kr. All rights reserved.
//

import Foundation

struct Color {
    static let ChaCha = 0x129a76
    static let OrderSelectBg = 0x1B9B76
}
