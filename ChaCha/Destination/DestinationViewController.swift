//
//  DestinationViewController.swift
//  Taxi
//
//  Created by wslee-chacha on 2018. 7. 24..
//  Copyright © 2018년 chachacreation.co.kr. All rights reserved.
//

import UIKit
import GoogleMaps

enum ViewType: Int {
    case from = 0       //출발지 설정
    case to             //목적지 설정
    case confirm_from   //출발지 확인
}

@objc protocol DestinationDelegate {
    func didSelectSource(source: CLLocationCoordinate2D, address: String)
    func didSelectDestination(destination: CLLocationCoordinate2D, address: String)
    func didConfirmSourceLocation(source: CLLocationCoordinate2D, address: String)
}

class DestinationViewController: BaseViewController {
    
    //Data
    var viewType: ViewType?
    var mapLocation: CLLocationCoordinate2D?
    {
        get {
            switch viewType {
                case .from?, .confirm_from?:
                    return locationMgr?.sourceLocation
                case .to?:
                    return locationMgr?.destinationLocation
                default:
                    print("DestinationViewController mapLocation get : Not to do")
                    return kCLLocationCoordinate2DInvalid
            }
        }
        
        set(position) {
            switch viewType {
                case .from?, .confirm_from?:
                    locationMgr?.sourceLocation = position!
                case .to?:
                    locationMgr?.destinationLocation = position!
                default:
                    print("DestinationViewController mapLocation set : Not to do")
            }
        }
    }
    var address: String?
    {
        willSet(foundAddress) {
            addressLabel.text = foundAddress
        }
    }
    var searchKeyword: String?
    var poiSearchResults: [AddressInfo]?
    
    //Delegate
    var delegate: DestinationDelegate?
    
    //Top Navigation Bar
    @IBOutlet weak var topNaviagtionBar: UIView!
    @IBOutlet weak var mapBtn: UIButton!
    
    //Search scene
    @IBOutlet weak var searchScene: UIView!
    @IBOutlet weak var searchBar: UIView!
    @IBOutlet weak var searchTextField: CustomTextField!
    @IBOutlet weak var clearBtn: UIButton!
    @IBOutlet weak var searchResultTableView: UITableView!
    
    //Map scene
    @IBOutlet weak var mapScene: UIView!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var locationBtn: UIButton!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var cancelMapBtn: UIButton!
    @IBOutlet weak var addressConfirmBtn: UIButton!
    
    //Bottom Address View
    @IBOutlet weak var addressView: UIView!
    
    //Constraint
    @IBOutlet weak var tableViewBottomMargin: NSLayoutConstraint!
    @IBOutlet weak var topNaviBarTopMargin: NSLayoutConstraint!
    @IBOutlet weak var addressViewBottomMargin: NSLayoutConstraint!
    
    //MARK: Instance Methods
    func enableSearchScene(isHidden: Bool)
    {
        searchScene.isHidden = isHidden
        mapScene.isHidden = !isHidden
        mapBtn.isHidden = isHidden
        if isHidden {
            searchTextField.resignFirstResponder()
        } else {
            searchTextField.becomeFirstResponder()
        }
    }
    
    func setControls()
    {
        backBtn?.addTarget(self, action: #selector(buttonSelected(button:)), for: .touchUpInside)
        mapBtn.addTarget(self, action: #selector(buttonSelected(button:)), for: .touchUpInside)
        clearBtn.addTarget(self, action: #selector(buttonSelected(button:)), for: .touchUpInside)
        locationBtn.addTarget(self, action: #selector(buttonSelected(button:)), for: .touchUpInside)
        cancelMapBtn.addTarget(self, action: #selector(buttonSelected(button:)), for: .touchUpInside)
        addressConfirmBtn.addTarget(self, action: #selector(buttonSelected(button:)), for: .touchUpInside)
        
        searchTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        if !(searchKeyword?.isEmpty)! {
            searchTextField.text = searchKeyword
            locationMgr?.getPOIfromTmap(keyword: searchKeyword!)
        }
        
        CommonObject.setBorderLine(view: searchBar)
        searchResultTableView.register(SearchResultTableViewCell.nib(), forCellReuseIdentifier: String(describing: SearchResultTableViewCell.self))
        
        searchTextField.becomeFirstResponder()
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: .UIKeyboardWillHide, object: nil)
    }
    
    func setControlsCaseConfirmSource()
    {
        backBtn?.addTarget(self, action: #selector(buttonSelected(button:)), for: .touchUpInside)
        locationBtn.addTarget(self, action: #selector(buttonSelected(button:)), for: .touchUpInside)
        addressConfirmBtn.addTarget(self, action: #selector(buttonSelected(button:)), for: .touchUpInside)
        
        enableSearchScene(isHidden: true)
        cancelMapBtn.isHidden = true
    }
    
    func animateWhenMapDragging(isDragging: Bool)
    {
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.5, delay: 0,
                           options: .curveLinear,
                           animations: {
                            if isDragging {
                                if #available(iOS 11.0, *) {
                                    self.addressViewBottomMargin.constant = (0.9 * self.addressConfirmBtn.frame.size.height) + (UIApplication.shared.keyWindow?.safeAreaInsets.bottom)!
                                } else {
                                    self.addressViewBottomMargin.constant = 0.9 * self.addressConfirmBtn.frame.size.height
                                }
                                self.topNaviBarTopMargin.constant = -2 * self.topNaviagtionBar.frame.height
                                self.locationBtn.alpha = 0
                            } else {
                                self.addressViewBottomMargin.constant = -10
                                self.topNaviBarTopMargin.constant = 0
                                self.locationBtn.alpha = 1
                            }
                            self.view.layoutIfNeeded()
            },
                           completion: nil)
        }
    }
    
    //MARK: Action Methods
    @objc func buttonSelected(button: UIButton)
    {
        if button.isEqual(backBtn) {
            //TODO: locationManager에 delegate를 add한 viewcontroller가 pop되면 removeDelegate()를 해야 하는데 좀 더 괜찮은 방법이 있는지 생각해보자
            locationMgr?.removeDelegate()
            if viewType == .confirm_from {
                dismiss(animated: true, completion: nil)
            } else {
                navigationController?.popViewController(animated: true)
            }
        } else if button.isEqual(mapBtn) {
            enableSearchScene(isHidden: true)
        } else if button.isEqual(clearBtn) {
            searchTextField.text?.removeAll()
            searchTextField.becomeFirstResponder()
        } else if button.isEqual(locationBtn) {
            mapView.animate(toLocation: (locationMgr?.getCurrentLocation())!)
        } else if button.isEqual(cancelMapBtn) {
            enableSearchScene(isHidden: false)
        } else if button.isEqual(addressConfirmBtn) {
            locationMgr?.setNeedsToRequestRoutes(locationType: viewType!, position: mapView.camera.target)
            mapLocation = mapView.camera.target
            locationMgr?.removeDelegate()
            locationMgr?.requestRoutesIfNeeded()
            
            switch viewType {
                case .from?:
                    if (delegate as AnyObject).responds(to: #selector(DestinationDelegate.didSelectSource(source:address:))) {
                        delegate?.didSelectSource(source: mapLocation!, address: address!)
                    }
                case .to?:
                    if (delegate as AnyObject).responds(to: #selector(DestinationDelegate.didSelectDestination(destination:address:))) {
                        delegate?.didSelectDestination(destination: mapLocation!, address: address!)
                    }
                case .confirm_from?:
                    if (delegate as AnyObject).responds(to: #selector(DestinationDelegate.didConfirmSourceLocation(source:address:))) {
                        delegate?.didConfirmSourceLocation(source: mapLocation!, address: address!)
                    }
                default:
                    print("DestinationViewController address Confirm button clicked : viewType Not Set")
            }
            
            if viewType == .confirm_from {
                dismiss(animated: true, completion: nil)
            } else {
                navigationController?.popViewController(animated: true)
            }
        }
    }
    
    //MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationMgr?.setDelegate(delegate: self)
        
        mapView.delegate = self
        mapView.settings.rotateGestures = false
        
        if viewType == .confirm_from {
            setControlsCaseConfirmSource()
        } else {
            setControls()
        }
        
        var cameraPosition: CLLocationCoordinate2D?
        switch viewType {
            case .from?:
                mainTitleLabel?.text = "출발지"
                cameraPosition = locationMgr?.getSourceLocation()
            case .confirm_from?:
                mainTitleLabel?.text = "출발지 확인"
                cameraPosition = locationMgr?.getSourceLocation()
            case .to?:
                mainTitleLabel?.text = "목적지"
                if !CommonObject.isEqualLocation(first: (locationMgr?.getDestinationLocation())!, second: kCLLocationCoordinate2DInvalid) {
                    cameraPosition = locationMgr?.getDestinationLocation()
                } else {
                    cameraPosition = locationMgr?.getCurrentLocation()
                }
            default:
                print("DestinationViewController ViewDidLoad() : viewType Not Set")
        }
        mapView.camera = GMSCameraPosition(target:cameraPosition! , zoom: 17, bearing: 0, viewingAngle: 0)
        locationMgr?.getAddressFromTmap(position: cameraPosition!)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }

}

extension DestinationViewController: UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return poiSearchResults?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: SearchResultTableViewCell.self), for: indexPath) as! SearchResultTableViewCell
        
        if let addressInfo = poiSearchResults?[indexPath.row] {
            cell.buildingLabel.text = addressInfo.roadInfo?.buildingName
            cell.addressLabel.text = addressInfo.adminInfo?.simpleAddress
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return SearchResultTableViewCell.height() / 375.0 * UIScreen.main.bounds.size.width
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        //get location
        if let addressInfo = poiSearchResults?[indexPath.row] {
            if !CommonObject.isEqualLocation(first: addressInfo.location, second: kCLLocationCoordinate2DInvalid) {
                //set location to gmsmapview
                mapView.camera = GMSCameraPosition(target:addressInfo.location , zoom: 17, bearing: 0, viewingAngle: 0)
                //go to map scene
                enableSearchScene(isHidden: true)
            }
        }
        tableView.deselectRow(at: indexPath, animated: false)
    }
}

//MARK: Location Delegate
extension DestinationViewController: LocationDelegate
{
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
        
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        FUNCLOG(functionName: #function)
    }
    
    func locationManager(_ manager: CLLocationManager, didFoundAddress address: String, location: CLLocationCoordinate2D) {
        self.address = address
        addressConfirmBtn.isEnabled = true
    }
    
    func locationManager(_ manager: CLLocationManager, didFoundPois pois: NSArray) {
        guard let results = pois as? [AddressInfo] else {
            return
        }
        poiSearchResults = results
        searchResultTableView.reloadData()
    }
}

// MARK: UITextField text changed
extension DestinationViewController: UITextFieldDelegate
{
    func textFieldDidChange(_ textField: UITextField)
    {
        if (textField.text?.isEmpty)! {
            poiSearchResults?.removeAll()
            searchResultTableView.reloadData()
        } else {
            locationMgr?.getPOIfromTmap(keyword: textField.text!)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        
        return true
    }
}

//MARK: keyboard notification
extension DestinationViewController
{
    func keyboardWillShow(notification: Notification)
    {
        if let userInfo = notification.userInfo as? [String: AnyObject] {
            let keyboardFrame = userInfo[UIKeyboardFrameEndUserInfoKey]?.cgRectValue
            let duration = userInfo[UIKeyboardAnimationDurationUserInfoKey]?.doubleValue
            UIView.animate(withDuration: duration ?? 1) {
                self.tableViewBottomMargin.constant = keyboardFrame?.size.height ?? 0
                self.searchScene.layoutIfNeeded()
            }
        }
        
    }
    
    func keyboardWillHide(notification: Notification)
    {
        if let userInfo = notification.userInfo as? [String: AnyObject] {
            let duration = userInfo[UIKeyboardAnimationDurationUserInfoKey]?.doubleValue
            UIView.animate(withDuration: duration ?? 1) {
                self.tableViewBottomMargin.constant = 0
                self.searchScene.layoutIfNeeded()
            }
        }
    }
}

extension DestinationViewController: GMSMapViewDelegate
{
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        locationMgr?.getAddressFromTmap(position: position.target)
        animateWhenMapDragging(isDragging: false)
    }
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        guard gesture else {
            return
        }
        addressLabel.text = "주소 탐색중.."
        addressConfirmBtn.isEnabled = false
        animateWhenMapDragging(isDragging: true)
    }
}
