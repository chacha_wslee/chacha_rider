//
//  SearchResultTableViewCell.swift
//  Taxi
//
//  Created by wslee-chacha on 2018. 7. 26..
//  Copyright © 2018년 chachacreation.co.kr. All rights reserved.
//

import UIKit

class SearchResultTableViewCell: BaseTableViewCell {
    @IBOutlet weak var buildingLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    static func nib() -> UINib
    {
        return UINib.init(nibName: String(describing: self), bundle: nil)
    }
    
    static func height() -> CGFloat
    {
        if let cell = Bundle.main.loadNibNamed(String(describing: self), owner: self, options: nil)?.first as? UITableViewCell {
            return cell.frame.size.height
        }
        return 70.0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
