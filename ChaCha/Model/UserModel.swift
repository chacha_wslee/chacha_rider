//
//  UserModel.swift
//  Taxi
//
//  Created by wslee-chacha on 2018. 7. 31..
//  Copyright © 2018년 chachacreation.co.kr. All rights reserved.
//

import UIKit
import NotificationCenter

enum USER_STATE: String {
    case NOT_SET = "0"
    case LOGIN_YET = "100"
    case LOGIN_COMPLETE = "110"
    case ORDER_PREVIEW = "115"
    case ORDER_CALL = "120"
    case ORDER_PICKUP = "130"
    case ONTRIP = "140"
}

class UserModel: NSObject {
    
    static let UserModelChanged = "UserModelChanged"
    static let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let stateMgr = (UIApplication.shared.delegate as! AppDelegate).stateMgr
    
    var name: String?
    var did: String?
    var picture: String?
    var oseq: String?
    var state: USER_STATE = .NOT_SET
    var tel: String?
    var loginType: String?
    var ustate: String?
    
    var ostate: DriverState = .DRIVER_DO_NOT
    var udevice: String?
    var uSeq: String?
    var datoken: String?
    var driverInfo: DriverModel?
    var chargeInfo: Charge?
    
    convenience init(userInfo: NSDictionary) {
        self.init()
        if let userDevice = userInfo["user_device"] as? NSDictionary {
            name = CommonObject.dataToString(data: userDevice["name"])
            did = CommonObject.dataToString(data: userDevice["did"])
            picture = CommonObject.dataToString(data: userDevice["picture"])
            oseq = CommonObject.dataToString(data: userDevice["oseq"])
            uSeq = CommonObject.dataToString(data: userDevice["useq"])
            state = userState(from: CommonObject.dataToString(data: userDevice["state"]))
            tel = CommonObject.dataToString(data: userDevice["telno"])
        }
        
        if let userState = userInfo["user_state"] as? NSDictionary {
            ustate = CommonObject.dataToString(data: userState["ustate"])
            ostate = DriverModel.dataToDriverState(data: CommonObject.dataToString(data: userState["ostate"]))
            udevice = CommonObject.dataToString(data: userState["udevice"])
        }
        
        if let _ = userInfo["provider"] as? NSDictionary {
            driverInfo = DriverModel.init(with: userInfo)
        }
        
        if let charge = userInfo["drive_charge"] as? NSDictionary {
            chargeInfo = Charge.init(with: charge)
        }
        
        loginType = CommonObject.dataToString(data: userInfo["socialsite"])
        datoken = CommonObject.dataToString(data: userInfo["datoken"])
        print("user \(name!) set!!! user sequence : \(uSeq!)")
    }
    
    static func setUser(_ user: UserModel)
    {
        let currentUser = appDelegate.userModel
        appDelegate.userModel = user
        NotificationCenter.default.post(name: NSNotification.Name(UserModel.UserModelChanged), object: self, userInfo: ["userModel" : [currentUser, user]])
    }
    
    static func getUser() -> UserModel
    {
        return appDelegate.userModel!
    }
    
    func userState(from string: String) -> USER_STATE
    {
        switch string {
        case "100":
            return .LOGIN_YET
        case "110":
            return .LOGIN_COMPLETE
        case "120":
            return .ORDER_CALL
        case "130":
            return .ORDER_PICKUP
        case "140":
            return .ONTRIP
        default:
            return .NOT_SET
        }
    }
    
    func getUserSeq() -> String
    {
        return uSeq ?? ""
    }
    
    func getOseq() -> String
    {
        return oseq ?? ""
    }
    
    func getState() -> USER_STATE
    {
        return state
    }
    
    func getOstate() -> DriverState
    {
        return ostate
    }
}
