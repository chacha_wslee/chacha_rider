//
//  StateManager.swift
//  Taxi
//
//  Created by wslee-chacha on 2018. 7. 31..
//  Copyright © 2018년 chachacreation.co.kr. All rights reserved.
//

import UIKit
import AFNetworking

@objc protocol StateManagerDelegate {
    @objc optional func didSetUserInformation()
    @objc optional func didPickUpStart()
    @objc optional func didPickUpFail()
    @objc optional func didPickUpCancel()
    @objc optional func didPickUpArrive()
    @objc optional func didTripStart()
    @objc optional func didTripArrive()
    @objc optional func didTripEnd()
    @objc optional func didCancelByDriver()
    @objc optional func didTimeOut()
    @objc optional func didUpdateState()
}

class StateManager: NSObject {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let requestMgr = (UIApplication.shared.delegate as! AppDelegate).requestMgr!
    
    private var delegates = NSMutableArray()
    private var timer: Timer?
    
    private var cancelType: DriverState = .DRIVER_DO_NOT
    private var needToWaitResponse = false      //when app launching, thus wait response and setting usermodel
    
    //MARK: Reuqest Server
    @objc func requestDriver()
    {
        DispatchQueue.global(qos: DispatchQoS.background.qosClass).async {
            let url = self.requestMgr.urlWithMethodId(methodId: API.kRequestDriver)
            
            let commonParameters = self.requestMgr.commonParameters()
            let requestParameters = self.requestMgr.requestParameters(methodId: API.kRequestDriver)
            let additionalParameters = ["useq" : UserModel.getUser().getUserSeq()]
            
            let parameter = NSMutableDictionary()
            parameter.addEntries(from: commonParameters as! [String : Any])
            parameter.addEntries(from: requestParameters as! [String : Any])
            parameter.addEntries(from: additionalParameters)
            
            let manager = AFHTTPSessionManager(baseURL: nil)
            manager.responseSerializer = AFJSONResponseSerializer()
            
            manager.get(url!, parameters: parameter, progress: { _ in }, success: {
                (operation, responseObject) in
                DispatchQueue.main.async {
                    print("Success : \(responseObject as! NSDictionary)")
                    let response = ResponseObject.init(responseObject: responseObject as Any)
                    if response.result == 1 {
                        UserModel.setUser(UserModel.init(userInfo: response.response!))
                    }
                }
            }) { (task, error) in
                print("failure : \(error.localizedDescription)")
            }
        }
    }
    
    @objc func requestOrderMatching()
    {
        DispatchQueue.global(qos: DispatchQoS.background.qosClass).async {
            let url = self.requestMgr.urlWithMethodId(methodId: API.kOrderMatching)
            
            let commonParameters = self.requestMgr.commonParameters()
            let requestParameters = self.requestMgr.requestParameters(methodId: API.kOrderMatching)
            let additionalParameters = ["useq" : UserModel.getUser().getUserSeq()]
            
            let parameter = NSMutableDictionary()
            parameter.addEntries(from: commonParameters as! [String : Any])
            parameter.addEntries(from: requestParameters as! [String : Any])
            parameter.addEntries(from: additionalParameters)
            
            let manager = AFHTTPSessionManager(baseURL: nil)
            manager.responseSerializer = AFJSONResponseSerializer()
            
            manager.get(url!, parameters: parameter, progress: { _ in }, success: {
                (operation, responseObject) in
                DispatchQueue.main.async {
                    print("Success : \(responseObject as! NSDictionary)")
                    let response = ResponseObject.init(responseObject: responseObject as Any)
                    if response.result == 1 {
                        UserModel.setUser(UserModel.init(userInfo: response.response!))
                    }
                    if self.needToWaitResponse {
                        self.needToWaitResponse = false
                        self.notifyState(selector: #selector(StateManagerDelegate.didSetUserInformation))
                    }
                }
            }) { (task, error) in
                print("failure : \(error.localizedDescription)")
            }
        }
    }
    
    @objc func requestPickUpState()
    {
        DispatchQueue.global(qos: DispatchQoS.background.qosClass).async {
            let url = self.requestMgr.urlWithMethodId(methodId: API.kPickUpState)
            
            let commonParameters = self.requestMgr.commonParameters()
            let requestParameters = self.requestMgr.requestParameters(methodId: API.kPickUpState)
            let additionalParameters = ["useq" : UserModel.getUser().getUserSeq(),
                                        "oseq" : UserModel.getUser().getOseq()]
            
            let parameter = NSMutableDictionary()
            parameter.addEntries(from: commonParameters as! [String : Any])
            parameter.addEntries(from: requestParameters as! [String : Any])
            parameter.addEntries(from: additionalParameters)
            
            let manager = AFHTTPSessionManager(baseURL: nil)
            manager.responseSerializer = AFJSONResponseSerializer()
            
            manager.get(url!, parameters: parameter, progress: { _ in }, success: {
                (operation, responseObject) in
                DispatchQueue.main.async {
                    print("Success : \(responseObject as! NSDictionary)")
                    let response = ResponseObject.init(responseObject: responseObject as Any)
                    if response.result == 1 {
                        UserModel.setUser(UserModel.init(userInfo: response.response!))
                    }
                    if self.needToWaitResponse {
                        self.needToWaitResponse = false
                        self.notifyState(selector: #selector(StateManagerDelegate.didSetUserInformation))
                    }
                }
            }) { (task, error) in
                print("failure : \(error.localizedDescription)")
            }
        }
    }
    
    @objc func requestOnTripState()
    {
        DispatchQueue.global(qos: DispatchQoS.background.qosClass).async {
            let url = self.requestMgr.urlWithMethodId(methodId: API.kTripState)
            
            let commonParameters = self.requestMgr.commonParameters()
            let requestParameters = self.requestMgr.requestParameters(methodId: API.kTripState)
            let additionalParameters = ["useq" : UserModel.getUser().getUserSeq()]
            
            let parameter = NSMutableDictionary()
            parameter.addEntries(from: commonParameters as! [String : Any])
            parameter.addEntries(from: requestParameters as! [String : Any])
            parameter.addEntries(from: additionalParameters)
            
            let manager = AFHTTPSessionManager(baseURL: nil)
            manager.responseSerializer = AFJSONResponseSerializer()
            
            manager.get(url!, parameters: parameter, progress: { _ in }, success: {
                (operation, responseObject) in
                DispatchQueue.main.async {
                    print("Success : \(responseObject as! NSDictionary)")
                    let response = ResponseObject.init(responseObject: responseObject as Any)
                    if response.result == 1 {
                        UserModel.setUser(UserModel.init(userInfo: response.response!))
                    }
                    if self.needToWaitResponse {
                        self.needToWaitResponse = false
                        self.notifyState(selector: #selector(StateManagerDelegate.didSetUserInformation))
                    }
                }
            }) { (task, error) in
                print("failure : \(error.localizedDescription)")
            }
        }
    }
    
    //MARK: State Management
    func requestByState(state: USER_STATE)
    {
        guard state != .NOT_SET else {
            print("guard \(#function) user state not set!!!")
            return
        }
        
        if timer != nil {
            timer?.invalidate()
            timer = nil
        }
        
        if state == .LOGIN_COMPLETE {
            requestDriver()
            timer = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(requestDriver), userInfo: nil, repeats: true)
        } else if state == .ORDER_CALL {
            requestOrderMatching()
            timer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(requestOrderMatching), userInfo: nil, repeats: true)
        } else if state == .ORDER_PICKUP {
            requestPickUpState()
            timer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(requestPickUpState), userInfo: nil, repeats: true)
        } else if state == .ONTRIP {
            requestOnTripState()
            timer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(requestOnTripState), userInfo: nil, repeats: true)
        }
    }
    
    @objc func didSetUserModel(_ notification: Notification)
    {
        guard let userModels = notification.userInfo!["userModel"] as? [UserModel] else {
            return
        }
        
        if let currentUser = userModels.first, let updateUser = userModels.last {
            if currentUser.state != updateUser.state {
                requestByState(state: updateUser.state)
                onStateChanged(current: currentUser.state, new: updateUser.state)
            } else {
                onStateUpdated(current: currentUser.getOstate(), new: updateUser.getOstate())
            }
        }
    }
    
    func onStateChanged(current: USER_STATE, new: USER_STATE)
    {
        var selector: Selector?
        if current == .NOT_SET {
            //regard as app launching & not idle state
            if new == .ORDER_CALL || new == .ORDER_PICKUP || new == .ONTRIP {
                needToWaitResponse = true
            }
        } else if current == .LOGIN_COMPLETE {
            //not to do
        } else if current == .ORDER_CALL {
            if new == .ORDER_PICKUP {
                selector = #selector(StateManagerDelegate.didPickUpStart)
            } else if new == .LOGIN_COMPLETE {
                selector = #selector(StateManagerDelegate.didPickUpFail)
            }
        } else if current == .ORDER_PICKUP {
            if new == .ONTRIP {
                selector = #selector(StateManagerDelegate.didTripStart)
            } else if new == .LOGIN_COMPLETE {
                if isRiderCancel() {
                    selector = #selector(StateManagerDelegate.didPickUpCancel)
                } else {
                    selector = #selector(StateManagerDelegate.didCancelByDriver)
                }
                setCancelType(.DRIVER_DO_NOT)
            }
        } else if current == .ONTRIP {
            if new == .LOGIN_COMPLETE {
                selector = #selector(StateManagerDelegate.didTripEnd)
            }
        }
        
        if selector != nil {
            notifyState(selector: selector!)
        }
    }
    
    func onStateUpdated(current: DriverState, new: DriverState)
    {
        var selector: Selector?
        
        if current == .DRIVER_DO_NOT {
            //not to do
        } else if current == .DRIVER_DO_ING {
            if new == .DRIVER_DO_END {
                let state = UserModel.getUser().getState()
                if state == .ORDER_PICKUP {
                    selector = #selector(StateManagerDelegate.didPickUpArrive)
                } else if state == .ONTRIP {
                    selector = #selector(StateManagerDelegate.didTripArrive)
                }
            } else if new == .DRIVER_CANCEL_ON_ORDER || new == .DRIVER_CANCEL_ON_START
                || new == .DRIVER_CANCEL_ON_ARRIVE || new == .DRIVER_CANCEL_ON_BEGIN {
                //cancel by driver
                selector = #selector(StateManagerDelegate.didCancelByDriver)
            } else if new == .RIDER_TIMEOUT {
                selector = #selector(StateManagerDelegate.didTimeOut)
            } else if new == .DRIVER_DO_ING {
                //just update state
                selector = #selector(StateManagerDelegate.didUpdateState)
            }
        } else if current == .DRIVER_DO_END {
            if new == .DRIVER_CANCEL_ON_ORDER || new == .DRIVER_CANCEL_ON_START
            || new == .DRIVER_CANCEL_ON_ARRIVE || new == .DRIVER_CANCEL_ON_BEGIN {
                //cancel by driver
                selector = #selector(StateManagerDelegate.didCancelByDriver)
            } else if new == .RIDER_TIMEOUT {
                selector = #selector(StateManagerDelegate.didTimeOut)
            }
        }
        
        if selector != nil {
            notifyState(selector: selector!)
        }
    }
    
    func notifyState(selector: Selector)
    {
        for delegate in delegates where delegate is StateManagerDelegate {
            if (delegate as AnyObject).responds(to: selector) {
                (delegate as AnyObject).perform(selector, with: nil, afterDelay: 0)
            }
        }
    }
    
    func setDelegate(delegate: StateManagerDelegate)
    {
        delegates.add(delegate)
    }
    
    func removeDelegate(delegate: StateManagerDelegate)
    {
        delegates.remove(delegate)
    }
    
    func isRiderCancel() -> Bool
    {
        if cancelType == .RIDER_CANCEL_ON_ORDER || cancelType == .RIDER_CANCEL_ON_START
            || cancelType == .RIDER_CANCEL_ON_ARRIVE || cancelType == .RIDER_CANCEL_ON_BEGIN {
            return true
        } else if cancelType == .RIDER_TIMEOUT {
            return true
        } else {
            return false
        }
    }
    
    func setCancelType(_ type:DriverState)
    {
        guard type != .DRIVER_DO_NOT && type != .DRIVER_DO_ING && type != .DRIVER_DO_END else {
            print("\(#function) canceltype is not valid")
            return
        }
        cancelType = type
    }
    
    override init() {
        super.init()
        NotificationCenter.default.addObserver(self, selector: #selector(didSetUserModel(_:)), name: NSNotification.Name(UserModel.UserModelChanged), object: nil)
    }
}
