//
//  PaymentManager.swift
//  Taxi
//
//  Created by wslee-chacha on 2018. 7. 31..
//  Copyright © 2018년 chachacreation.co.kr. All rights reserved.
//

import UIKit

struct Payment {
    var cseq: String?
    var pg: String?
    var mode: String?
    var expire: String?
    var regDate: String?
    var cardNo: String?
    var state: String?
    var type: String?
    
    func getCardSeq() -> String
    {
        return cseq ?? ""
    }
}

struct Charge {
    var sum: Int?
    var discount: Int?
    var payed: Int?
    var total: String?
    var renter: String?
    var driving: String?
    var charge: String?
}

extension Charge {
    init(with ChargeInfo: NSDictionary) {
        sum = CommonObject.dataToInt(data: ChargeInfo["fare_sum"])
        discount = CommonObject.dataToInt(data: ChargeInfo["fare_discount"])
        payed = CommonObject.dataToInt(data: ChargeInfo["fare_payed"])
        
        total = sum?.toCurrency()
        renter = round(Double(sum!) * 0.05).toCurrency()
        driving = round(Double(sum!) * 0.95).toCurrency()
        charge = payed?.toCurrency()
    }
}

class PaymentManager: NSObject {
    
    var cards = NSMutableArray()
    var charges = Charge()
    
    //MARK: card managment
    func addCard(cardInfo: NSDictionary)
    {
        var card = Payment()
        card.cseq = cardInfo["cseq"] as? String
        card.cardNo = cardInfo["no"] as? String
        card.type = cardInfo["name"] as? String
        card.expire = cardInfo["expire"] as? String
        if let mode = cardInfo["mode"] as? Int {
            card.mode = String.init(format: "%d", mode)
        }
        card.pg = cardInfo["pg"] as? String
        card.regDate = cardInfo["reg_date"] as? String
        card.state = cardInfo["state"] as? String
        cards.add(card)
    }
    
    func addCardArray(cardArray: NSArray)
    {
        for cardInfo in cardArray where cardInfo is NSDictionary {
            addCard(cardInfo: cardInfo as! NSDictionary)
        }
    }
    
    func getPaymentCard() -> Payment?
    {
        if let payment = cards.firstObject as? Payment {
            return payment
        } else {
            return nil
        }
    }
    
    func checkPayment() -> Bool
    {
        if cards.count > 0 {
            return true
        } else {
            return false
        }
    }
    
    func setChargeInfo(with chargeInfo: NSDictionary)
    {
        charges = Charge.init(with: chargeInfo)
    }
    
    func registerCardPopup(target: UIViewController)
    {
        let alertController = UIAlertController.init(title: "", message: "신용카드를 먼저 등록해야 이용할 수 있습니다.", preferredStyle: .alert)
        let okAction = UIAlertAction.init(title: "지금 등록하기", style: .default, handler: {
            (alertAction) in
            //TODO: 신용카드 등록 페이지로 go
        })
        let cancelAction = UIAlertAction.init(title: "나중에 등록하기", style: .cancel, handler: nil)
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        target.present(alertController, animated: true, completion: nil)
    }
}

extension Int
{
    func toCurrency() -> String
    {
        let formatter = NumberFormatter()
        formatter.currencyCode = "KRW"
        formatter.currencyCode = "원"
        formatter.numberStyle = .decimal
        return String.init(format: "%@원", formatter.string(for: self)!)
    }
}

extension Double
{
    func toCurrency() -> String
    {
        let formatter = NumberFormatter()
        formatter.currencyCode = "KRW"
        formatter.currencyCode = "원"
        formatter.numberStyle = .decimal
        return String.init(format: "%@원", formatter.string(for: self)!)
    }
}

extension NSNumber
{
    func toCurrency() -> String
    {
        let formatter = NumberFormatter()
        formatter.currencyCode = "KRW"
        formatter.currencyCode = "원"
        formatter.numberStyle = .decimal
        return String.init(format: "%@원", formatter.string(from: self)!)
    }
}
