//
//  DriverModel.swift
//  Taxi
//
//  Created by wslee-chacha on 2018. 8. 2..
//  Copyright © 2018년 chachacreation.co.kr. All rights reserved.
//

import UIKit
import CoreLocation

struct Driver {
    var state: DriverState?
    var pseq: String?
    var vseq: String?
    var service: String?
    var sourceLocation: CLLocationCoordinate2D = kCLLocationCoordinate2DInvalid {
        willSet (newLocation) {
            if !CommonObject.isEqualLocation(first: sourceLocation, second: newLocation) {
                DispatchQueue.main.async {
                    (UIApplication.shared.delegate as! AppDelegate).locationMgr?.setSourceLocation(position: newLocation)
                }
            }
        }
    }
    var destinationLocation: CLLocationCoordinate2D = kCLLocationCoordinate2DInvalid {
        willSet (newLocation) {
            if !CommonObject.isEqualLocation(first: sourceLocation, second: newLocation) {
                DispatchQueue.main.async {
                    (UIApplication.shared.delegate as! AppDelegate).locationMgr?.setDestinationLocation(position: newLocation)
                }
            }
        }
    }
    var sourceAddress: String?
    var destinationAddress: String?
    var tel: String?
    var name: String?
    var rating: String?
    var driverProfile: String?
    var carModel: String?
    var carNumber: String?
    var carProfile: String?
    var ntime: String?
    var ndistance: String?
    var nlocation: String?
    var eduration: String?
    var edistance: String?
    var auth: String?
    var startDate: NSDate?
    var endDate: NSDate?
}

enum DriverState: String {
    case DRIVER_DO_NOT = "0"
    case DRIVER_DO_ING = "10"
    case DRIVER_DO_END = "20"
    case RIDER_CANCEL_ON_ORDER = "-10"
    case RIDER_CANCEL_ON_START = "-11"
    case RIDER_CANCEL_ON_ARRIVE = "-12"
    case RIDER_CANCEL_ON_BEGIN = "-13"
    case RIDER_TIMEOUT = "-15"
    case DRIVER_CANCEL_ON_ORDER = "-20"
    case DRIVER_CANCEL_ON_START = "-21"
    case DRIVER_CANCEL_ON_ARRIVE = "-22"
    case DRIVER_CANCEL_ON_BEGIN = "-23"
    case DRIVER_TIMEOUT = "-25"
    case DRIVER_NONE = "-30"
    case ADMIN_CANCEL = "-40"
}

class DriverModel: NSObject {
    
    var driver = Driver()
    
    convenience init(with driverInfo: NSDictionary) {
        self.init()
        if let provider = driverInfo["provider"] as? NSDictionary {
            driver.rating = CommonObject.dataToString(data: provider["rate"])
            driver.auth = CommonObject.dataToString(data: provider["auth"])
        }
        
        if let providerDetail = driverInfo["provider_device"] as? NSDictionary {
            driver.pseq = CommonObject.dataToString(data: providerDetail["pseq"])
            driver.vseq = CommonObject.dataToString(data: providerDetail["vseq"])
            driver.tel = CommonObject.dataToString(data: providerDetail["telno"])
            driver.name = CommonObject.dataToString(data: providerDetail["name"])
            driver.driverProfile = CommonObject.dataToString(data: providerDetail["picture"])
            driver.service = CommonObject.dataToString(data: providerDetail["service_name"])
            driver.carModel = CommonObject.dataToString(data: providerDetail["vehiclemodel"])
            driver.carNumber = CommonObject.dataToString(data: providerDetail["vehicleno"])
            driver.carProfile = CommonObject.dataToString(data: providerDetail["vehicle"])
        }
        
        if let pickupStep = driverInfo["drive_pickup_step"] as? NSDictionary {
            driver.ntime = CommonObject.dataToString(data: pickupStep["ntime"])
            driver.ndistance = CommonObject.dataToString(data: pickupStep["ndistance"])
            driver.nlocation = CommonObject.dataToString(data: pickupStep["nlocation"])
            driver.eduration = CommonObject.dataToString(data: pickupStep["eduration"])
            driver.edistance = CommonObject.dataToString(data: pickupStep["edistance"])
        } else if let tripStep = driverInfo["drive_trip_step"] as? NSDictionary {
            driver.ntime = CommonObject.dataToString(data: tripStep["ntime"])
            driver.ndistance = CommonObject.dataToString(data: tripStep["ndistance"])
            driver.nlocation = CommonObject.dataToString(data: tripStep["nlocation"])
            driver.eduration = CommonObject.dataToString(data: tripStep["eduration"])
            driver.edistance = CommonObject.dataToString(data: tripStep["edistance"])
        }
        
        if let pickupInfo = driverInfo["drive_pickup"] as? NSDictionary {
            driver.startDate = CommonObject.stringToYYYYMMDDHHMMSS(string: CommonObject.dataToString(data: pickupInfo["reg_date"]))
            driver.sourceLocation = CommonObject.dataToLocation(data: CommonObject.dataToString(data: pickupInfo["lsrc"]))
            driver.destinationLocation = CommonObject.dataToLocation(data: CommonObject.dataToString(data: pickupInfo["ldst"]))
        } else if let tripInfo = driverInfo["drive_trip"] as? NSDictionary {
            driver.startDate = CommonObject.stringToYYYYMMDDHHMMSS(string: CommonObject.dataToString(data: tripInfo["reg_date"]))
            driver.sourceLocation = CommonObject.dataToLocation(data: CommonObject.dataToString(data: tripInfo["lsrc"]))
            driver.destinationLocation = CommonObject.dataToLocation(data: CommonObject.dataToString(data: tripInfo["ldst"]))
        }
    }
    
    static func dataToDriverState(data: String) -> DriverState
    {
        switch data {
            case "0":
                return .DRIVER_DO_NOT
            case "10":
                return .DRIVER_DO_ING
            case "20":
                return .DRIVER_DO_END
            default:
                return .DRIVER_DO_NOT
        }
    }
}

extension String {
    func toInt() -> Int
    {
        return Int(Float(self) ?? 0)
    }
}
