//
//  AppDelegate_Swift.swift
//  Taxi
//
//  Created by wslee-chacha on 2018. 7. 17..
//  Copyright © 2018년 chachacreation.co.kr. All rights reserved.
//

import UIKit
import UserNotifications
import FBSDKCoreKit
import AdSupport
import FirebaseCore
import FirebaseMessaging
import GoogleMaps
import GooglePlaces
import AFNetworking
import NotificationCenter

class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate, MessagingDelegate, CLLocationManagerDelegate {

    var window: UIWindow?
    var navigationController: UINavigationController?
    var requestMgr: RequestManager?
    var locationMgr: LocationManager?
    var userModel: UserModel?
    var paymentMgr: PaymentManager?
    var stateMgr: StateManager?
    
    // MARK: Server Request
    func sendTokenToServer(pushToken: String)
    {
        let url = requestMgr?.urlWithMethodId(methodId: API.kSendToken)
        
        let commonParameters = requestMgr?.commonParameters()
        let requestParameters = requestMgr?.requestParameters(methodId: API.kSendToken)
        let additionalParameters = ["ptoken" : pushToken]
        
        let parameter = NSMutableDictionary()
        parameter.addEntries(from: commonParameters as! [String : Any])
        parameter.addEntries(from: requestParameters as! [String : Any])
        parameter.addEntries(from: additionalParameters)
        
        let manager = AFHTTPSessionManager(baseURL: nil)
        manager.responseSerializer = AFJSONResponseSerializer()
        
        manager.put(url!, parameters: parameter, success: {
            (operation, responseObject: Any) in
            let response = ResponseObject(responseObject: responseObject)
            print("\(String(describing: response.response))")
            if response.result == 1 {
                //go to map view
            }
        }, failure: {
            (task, error) in
            print("failure : \(error.localizedDescription)")
        })
        
        CommonObject.setPushToken(pushToken: pushToken)
    }
    
    // MARK: Initialize Stuff
    func setUserData()
    {
        let uuid = UIDevice.current.identifierForVendor?.uuidString
        CommonObject.setDeviceId(deviceId: uuid!)
    }
    
    func setThirdParty(application: UIApplication, launchOptions: [UIApplicationLaunchOptionsKey:Any]?)
    {
        //Facebook
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
        //Kakao
        KOSession.shared().clientSecret = KAKAO.SECRET
        
        //IgaWorks
        if (NSClassFromString("ASIdentifierManager") != nil) {
            IgaworksCore.setAppleAdvertisingIdentifier(ASIdentifierManager.shared().advertisingIdentifier.uuidString, isAppleAdvertisingTrackingEnabled: ASIdentifierManager.shared().isAdvertisingTrackingEnabled)
        }
        IgaworksCore.igaworksCore(withAppKey: IGAWORKS.KEY, andHashKey: IGAWORKS.HASHKEY)
        IgaworksCore.setLogLevel(IgaworksCoreLogInfo)
        
        //Firebase
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        
        //Google Map Service
        GMSServices.provideAPIKey(GOOGLE.APIKEY)
    }
    
    // MARK: App Notification
    
    func registerRemoteNotification()
    {
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(options: authOptions, completionHandler: { _, _ in })
            
        } else {
            let settingType: UIUserNotificationType = [.alert, .badge, .sound]
            let settings = UIUserNotificationSettings.init(types: settingType, categories: nil)
            UIApplication.shared.registerUserNotificationSettings(settings)
        }
        
        UIApplication.shared.registerForRemoteNotifications()
    }
    
    // MARK: Firebase Messaging Delegate
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        FUNCLOG(functionName: #function)
        //sendTokenToServer(pushToken: fcmToken)
    }
    
    // MARK: Remote Notification Delegate
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        FUNCLOG(functionName: #function)
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        FUNCLOG(functionName: #function)
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        FUNCLOG(functionName: #function)
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        FUNCLOG(functionName: #function)
        Messaging.messaging().apnsToken = deviceToken
        //sendTokenToServer(pushToken: deviceToken.base64EncodedString())
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        FUNCLOG(functionName: #function)
    }
    
    // MARK: Application Life cycle
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey : Any]?) -> Bool {
        
        
        requestMgr = RequestManager()
        locationMgr = LocationManager()
        userModel = UserModel()
        paymentMgr = PaymentManager()
        stateMgr = StateManager()
        
        setThirdParty(application: application, launchOptions: launchOptions)
        
        registerRemoteNotification()
        
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.backgroundColor = .green
        self.window?.makeKeyAndVisible()
        
        let launcherViewController = LauncherViewController(nibName: String(describing: LauncherViewController.self), bundle: nil)
        navigationController = UINavigationController.init(rootViewController: launcherViewController)
        navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        navigationController?.view.backgroundColor = .yellow
        navigationController?.setNavigationBarHidden(true, animated: false)
        
        self.window?.rootViewController = navigationController
        return true
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        FUNCLOG(functionName: #function)
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        FUNCLOG(functionName: #function)
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        FUNCLOG(functionName: #function)
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        FUNCLOG(functionName: #function)
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        FUNCLOG(functionName: #function)
    }
}
