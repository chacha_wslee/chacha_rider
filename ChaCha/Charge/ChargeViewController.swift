//
//  ChargeViewController.swift
//  Taxi
//
//  Created by wslee-chacha on 2018. 8. 3..
//  Copyright © 2018년 chachacreation.co.kr. All rights reserved.
//

import UIKit
import AFNetworking

class ChargeViewController: BaseViewController {

    @IBOutlet weak var receiptView: UIView!
    @IBOutlet weak var receiptDateLabel: UILabel!
    @IBOutlet weak var receiptTimeLabel: UILabel!
    @IBOutlet weak var receiptTotalFeeLabel: UILabel!
    @IBOutlet weak var receiptRenterFeeLabel: UILabel!
    @IBOutlet weak var receiptDrivingFeeLabel: UILabel!
    @IBOutlet weak var receiptDiscountLabel: UILabel!
    @IBOutlet weak var receiptChargeLabel: UILabel!
    @IBOutlet weak var receiptEnableBtn: UIButton!
    @IBOutlet weak var receiptTopMargin: NSLayoutConstraint!
    @IBOutlet weak var receiptArrowImage: UIImageView!
    
    @IBOutlet weak var tripInfoDateLabel: UILabel!
    @IBOutlet weak var tripInfoFeeLabel: UILabel!
    
    @IBOutlet weak var driverInfoProfileImageView: UIImageView!
    @IBOutlet weak var driverInfoDriverNameLabel: UILabel!
    @IBOutlet weak var driverInfoNameServiceLabel: UILabel!
    @IBOutlet weak var driverInfoRatingView: RatingView!
    
    @IBOutlet weak var chargeConfirmBtn: UIButton!
    
    @IBOutlet weak var ratingScene: UIView!
    @IBOutlet weak var ratingConfirmBtn: UIButton!
    @IBOutlet weak var ratingView: RatingView!
    
    //MARK: Server Request
    func requestRatingDriver()
    {
        let url = requestMgr?.urlWithMethodId(methodId: API.kRatingDriver)
        
        let commonParameters = requestMgr?.commonParameters()
        let requestParameters = requestMgr?.requestParameters(methodId: API.kRatingDriver)
        let additionalParameters = ["useq" : UserModel.getUser().getUserSeq(),
                                    "oseq" : UserModel.getUser().getOseq(),
                                    "rating" : String.init(format: "%d", ratingView.getRating())
                                    ]
        
        let parameter = NSMutableDictionary()
        parameter.addEntries(from: commonParameters as! [String : Any])
        parameter.addEntries(from: requestParameters as! [String : Any])
        parameter.addEntries(from: additionalParameters)
        
        let manager = AFHTTPSessionManager(baseURL: nil)
        manager.responseSerializer = AFJSONResponseSerializer()
        
        manager.post(url!, parameters: parameter, progress: {
            _ in
        }, success: {
            (operation, responseObject) in
            let response = ResponseObject(responseObject: responseObject!)
            if response.result == 1 {
                
            } else {
                self.onServerError(response: response)
            }
        }) {
            (task, error) in
            print("failure : \(error.localizedDescription)")
        }
    }
    
    //MARK: Instance Methods
    func parsingChargeInfo()
    {
        if let driver = UserModel.getUser().driverInfo?.driver, let charge = UserModel.getUser().chargeInfo {
            let startDate = driver.startDate
            let fulldateFormatter = DateFormatter()
            fulldateFormatter.locale = Locale(identifier: "ko_KR")
            fulldateFormatter.dateFormat = "yyyy/MM/dd h:mm a"
            let dateFormatter = DateFormatter()
            dateFormatter.locale = Locale(identifier: "ko_KR")
            dateFormatter.dateFormat = "yyyy.MM.dd"
            let timeFormatter = DateFormatter()
            timeFormatter.locale = Locale(identifier: "ko_KR")
            timeFormatter.dateFormat = "h:mm a"
            tripInfoDateLabel.text = fulldateFormatter.string(from: startDate! as Date)
            tripInfoFeeLabel.text = charge.total
            
            receiptDateLabel.text = dateFormatter.string(from: startDate! as Date)
            receiptTimeLabel.text = timeFormatter.string(from: startDate! as Date)
            receiptTotalFeeLabel.text = charge.total
            receiptRenterFeeLabel.text = charge.renter
            receiptDrivingFeeLabel.text = charge.driving
            receiptDiscountLabel.text = charge.discount?.toCurrency()
            receiptChargeLabel.text = charge.charge
            
            requestMgr?.requestProfileImage(type: .Driver, seq: driver.pseq!, handler: {
                (image) in
                self.driverInfoProfileImageView.image = image
            })
            driverInfoDriverNameLabel.text = driver.name!
            driverInfoNameServiceLabel.text = String.init(format: "%@ / %@", driver.name!, driver.service!)
        }
    }
    
    func animateReceipt(toExpand: Bool)
    {
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseIn, animations: {
            if toExpand {
                self.receiptTopMargin.constant = 0
            } else {
                self.receiptTopMargin.constant = self.receiptEnableBtn.frame.size.height - self.receiptView.frame.size.height
            }
            self.view.layoutIfNeeded()
        }, completion: {
            [weak self] (animated) in
            if toExpand {
                self?.receiptArrowImage.image = UIImage.init(named: "icon_charge_close")
            } else {
                self?.receiptArrowImage.image = UIImage.init(named: "icon_charge_open")
            }
        })
    }
    
    func buttonSelected(button: UIButton)
    {
        if button.isEqual(receiptEnableBtn) {
            button.isSelected = !button.isSelected
            animateReceipt(toExpand: button.isSelected)
        } else if button.isEqual(chargeConfirmBtn) {
            self.navigationController?.popViewController(animated: true)
        } else if button.isEqual(ratingConfirmBtn) {
            ratingScene.isHidden = true
            driverInfoRatingView.setRating(point: ratingView.getRating() - 1)
            driverInfoDriverNameLabel.isHidden = true
            requestRatingDriver()
        }
    }
    
    //MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        parsingChargeInfo()
        
        receiptEnableBtn.addTarget(self, action: #selector(buttonSelected(button:)), for: .touchUpInside)
        chargeConfirmBtn.addTarget(self, action: #selector(buttonSelected(button:)), for: .touchUpInside)
        ratingConfirmBtn.addTarget(self, action: #selector(buttonSelected(button:)), for: .touchUpInside)
        
        CommonObject.setShapeCircle(view: driverInfoProfileImageView)
        //TODO: 처음 시작할 때 영수증 constant 오류. 많이 내려와있음
        self.receiptTopMargin.constant = CommonObject.sizeByResolution(size: self.receiptEnableBtn.frame.size.height) - CommonObject.sizeByResolution(size: self.receiptView.frame.size.height)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
