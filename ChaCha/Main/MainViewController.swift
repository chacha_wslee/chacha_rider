//
//  MainViewController.swift
//  Taxi
//
//  Created by wslee-chacha on 2018. 7. 18..
//  Copyright © 2018년 chachacreation.co.kr. All rights reserved.
//

import UIKit
import GoogleMaps
import AFNetworking

class MainViewController: BaseViewController {
    
    @IBOutlet weak var mapView: GMSMapView!
    
    //data
    var orderPreview: [NSDictionary]?
    var mapLocation: CLLocationCoordinate2D?
    
    //idle scene
    @IBOutlet weak var idleScene: UIView!
    @IBOutlet weak var topNaviBar: UIView!
    @IBOutlet weak var addressView: UIView!
    @IBOutlet weak var sourceAddressBtn: UIButton!
    @IBOutlet weak var destinationAddressBtn: UIButton!
    @IBOutlet weak var sourceLabel: CustomLabel!
    @IBOutlet weak var destinationLabel: CustomLabel!
    @IBOutlet weak var sourceIcon: UIImageView!
    @IBOutlet weak var idleLocationBtn: UIButton!
    
    //order preview scene
    @IBOutlet weak var orderPreviewScene: PassthroughView!
    @IBOutlet weak var orderAddressView: UIView!
    @IBOutlet weak var orderCancelBtn: UIButton!
    @IBOutlet weak var orderRemainTimeInfoLabel: UILabel!
    @IBOutlet weak var orderSourceLabel: CustomLabel!
    @IBOutlet weak var orderDestinationLabel: CustomLabel!
    @IBOutlet weak var orderSourceBtn: UIButton!
    @IBOutlet weak var orderDestinationBtn: UIButton!
    @IBOutlet weak var previewLocationBtn: UIButton!
    
    //order select view
    @IBOutlet var orderSelectCollection: [UIView]!
    @IBOutlet weak var orderE_View: UIView!
    @IBOutlet weak var orderE_Checkbox: UIButton!
    @IBOutlet weak var orderE_PriceLabel: UILabel!
    @IBOutlet weak var orderE_Btn: UIButton!
    
    @IBOutlet weak var orderX_View: UIView!
    @IBOutlet weak var orderX_Checkbox: UIButton!
    @IBOutlet weak var orderX_PriceLabel: UILabel!
    @IBOutlet weak var orderX_Btn: UIButton!
    
    @IBOutlet weak var orderXL_View: UIView!
    @IBOutlet weak var orderXL_Checkbox: UIButton!
    @IBOutlet weak var orderXL_PriceLabel: UILabel!
    @IBOutlet weak var orderXL_Btn: UIButton!

    @IBOutlet weak var orderConfirmBtn: UIButton!
    
    //order select constraint (for animation)
    @IBOutlet weak var orderE_bottomMargin: NSLayoutConstraint!
    @IBOutlet weak var orderX_bottomMargin: NSLayoutConstraint!
    @IBOutlet weak var orderXL_bottomMargin: NSLayoutConstraint!
    
    //pick & trip view
    @IBOutlet weak var tripScene: PassthroughView!
    @IBOutlet weak var tripMenuBtn: UIButton!
    @IBOutlet weak var tripRemainTimeInfoLabel: UILabel!
    @IBOutlet weak var tripAddressView: UIView!
    @IBOutlet weak var tripSourceLabel: CustomLabel!
    @IBOutlet weak var tripDestinationLabel: CustomLabel!
    @IBOutlet weak var tripSourceBtn: UIButton!
    @IBOutlet weak var tripDestinationBtn: UIButton!
    @IBOutlet weak var tripLocationBtn: UIButton!
    @IBOutlet weak var tripCancelBtn: UIButton!
    
    //driver info view
    @IBOutlet weak var driverInfoView: UIView!
    @IBOutlet weak var tripStateLabel: UILabel!
    @IBOutlet weak var tripStarView: UIView!
    @IBOutlet weak var driverRatingLabel: UILabel!
    @IBOutlet weak var driverImageView: UIImageView!
    @IBOutlet weak var driverServiceTypeLabel: UILabel!
    @IBOutlet weak var driverNameLabel: UILabel!
    @IBOutlet weak var vehicleImageView: UIImageView!
    @IBOutlet weak var vehicleTypeLabel: UILabel!
    @IBOutlet weak var vehicleNumberLabel: UILabel!
    
    //pick & trip view btn
    @IBOutlet weak var callToDriverBtn: UIButton!
    @IBOutlet weak var chatToDriverBtn: UIButton!
    
    //map dragging animation
    @IBOutlet weak var topNaviBarTopMargin: NSLayoutConstraint!
    @IBOutlet weak var addressViewBottomMargin: NSLayoutConstraint!
    
    
    // MARK: Server Requests
    func requestOrderPreview(from: CLLocationCoordinate2D, to: CLLocationCoordinate2D, routeInfo: [String: String])
    {
        let url = requestMgr?.urlWithMethodId(methodId: API.kOrderPreview)
        
        let commonParameters = requestMgr?.commonParameters()
        let requestParameters = requestMgr?.requestParameters(methodId: API.kOrderPreview)
        let additionalParameters = ["useq" : UserModel.getUser().getUserSeq(),
                                    "lsrc" : CommonObject.locationToFormattedString(location: from),
                                    "ldst" : CommonObject.locationToFormattedString(location: to),
                                    "trip_distance" : routeInfo["distance"] ?? "0",
                                    "trip_duration" : routeInfo["duration"] ?? "0"]
        
        let parameter = NSMutableDictionary()
        parameter.addEntries(from: commonParameters as! [String : Any])
        parameter.addEntries(from: requestParameters as! [String : Any])
        parameter.addEntries(from: additionalParameters)
        
        let manager = AFHTTPSessionManager(baseURL: nil)
        manager.responseSerializer = AFJSONResponseSerializer()
        
        manager.get(url!, parameters: parameter, progress: { _ in
            
        }, success: {
            (operation, responseObject) in
            print("success")
            let response = ResponseObject(responseObject: responseObject!)
            if response.result == 1 {
                self.updateOrderPreview(responseObject: responseObject as! [String: AnyObject])
            } else {
                self.onServerError(response: response)
            }
            
            }, failure: {
                (task, error) in
                print("failure : \(error.localizedDescription)")
        })
    }
    
    func requestOrder()
    {
        let url = requestMgr?.urlWithMethodId(methodId: API.kOrder)
        
        let commonParameters = requestMgr?.commonParameters()
        let requestParameters = requestMgr?.requestParameters(methodId: API.kOrder)
        let additionalParameters = ["cseq" : paymentMgr.getPaymentCard()?.getCardSeq() ?? "402",
                                    "service" : "E",
                                    "uname" : "",
                                    "code" : "",
                                    "lsrc" : CommonObject.locationToFormattedString(location: (locationMgr?.getSourceLocation())!),
                                    "ldst" : CommonObject.locationToFormattedString(location: (locationMgr?.getDestinationLocation())!),
                                    "fare_rate" : "1"]
        
        let parameter = NSMutableDictionary()
        parameter.addEntries(from: commonParameters as! [String : Any])
        parameter.addEntries(from: requestParameters as! [String : Any])
        parameter.addEntries(from: additionalParameters)
        
        let manager = AFHTTPSessionManager(baseURL: nil)
        manager.responseSerializer = AFJSONResponseSerializer()
        
        manager.post(url!, parameters: parameter, progress: nil, success: {
            (operation, responseObject) in
            FUNCLOG(functionName: #function)
            let response = ResponseObject(responseObject: responseObject as Any)
            if response.result == 1 {
                UserModel.setUser(UserModel.init(userInfo: response.response!))
                let callingViewController = CallingViewController.init(nibName: String(describing: CallingViewController.self), bundle: nil)
                self.present(callingViewController, animated: true, completion: nil)
            } else {
                self.onServerError(response: response)
            }
        }) { (task, error) in
            print("failure : \(error.localizedDescription)")
        }
    }
    
    func requestOrderIfAvailable()
    {
        if paymentMgr.checkPayment() {
            requestOrder()
        } else {
            paymentMgr.registerCardPopup(target: self)
        }
    }
    
    // MARK: Instance Methods
    func setSceneWhenAppLaunching()
    {
        let state = UserModel.getUser().getState()
        
        switch state {
            case .LOGIN_YET:
                print("Login yet... Idle scene show")
            case .LOGIN_COMPLETE:
                print("Login Complete. Idle scene show")
            case .ORDER_CALL:
                print("Calling now.. open call viewcontroller???")
            case .ORDER_PICKUP:
                print("Pick up now.. pickup & trip scene show")
                locationMgr?.getAddressFromTmap(position: (locationMgr?.getSourceLocation())!, handler: { [weak self] (address) in
                    self?.tripSourceLabel.text = address
                })
                locationMgr?.getAddressFromTmap(position: (locationMgr?.getDestinationLocation())!, handler: { [weak self] (address) in
                    self?.tripDestinationLabel.text = address
                })
                didPickUpStart()
            case .ONTRIP:
                let ostate = UserModel.getUser().getOstate()
                if ostate == .DRIVER_DO_ING {
                    print("trip now.. pickup & trip scene show")
                    locationMgr?.getAddressFromTmap(position: (locationMgr?.getSourceLocation())!, handler: { [weak self] (address) in
                        self?.tripSourceLabel.text = address
                    })
                    locationMgr?.getAddressFromTmap(position: (locationMgr?.getDestinationLocation())!, handler: { [weak self] (address) in
                        self?.tripDestinationLabel.text = address
                    })
                    didTripStart()
                } else if ostate == .DRIVER_DO_END {
                    didTripArrive()
                } else {
                    //not to do
                }
            default:
                print("State : \(state)... Idle scene show")
        }
    }
    
    func calculateDistanceFromRoutes(routes: NSDictionary) -> [String: String]?
    {
        guard let legs = routes["legs"] as? [String: AnyObject] else {
            return nil
        }
        
        if let distance = legs["distance"] as? String, let duration = legs["duration"] as? String {
            return ["distance" : distance, "duration" : duration]
        } else {
            return nil
        }
    }
    
    func updateOrderPreview(responseObject: [String: AnyObject])
    {
        guard let response = responseObject["drive_estimates"] as? [NSDictionary]  else {
            return
        }
        
        // set order preview data
        orderPreview = response
        
        for estimate in response {
            if let service = estimate["service"] as? String {
                let priceMin = (estimate["fare_min"] as? NSNumber)?.toCurrency()
                let priceMax = (estimate["fare_max"] as? NSNumber)?.toCurrency()
                let price = String(format: "%@\n~\n%@", priceMin ?? "0", priceMax ?? "0")
                switch service {
                    case SERVICE.E:
                        orderE_PriceLabel.text = price
                        break
                    
                    case SERVICE.X:
                        orderX_PriceLabel.text = price
                        break
                    
                    case SERVICE.XL:
                        orderXL_PriceLabel.text = price
                        break
                    
                    default:
                        break
                }
            }
        }
        
        if response.count > 0 {
            let duration = (response.first?.object(forKey: "trip_duration") as? NSNumber)?.intValue
            let distance = (response.first?.object(forKey: "trip_distance") as? NSNumber)?.stringValue
            orderRemainTimeInfoLabel.text = String(format: "%@ km / %d 분", distance ?? "0", duration ?? "0")
        }
        
        //TODO: order btn setting
        onOrderServiceSelected(view: orderE_View)
        previewLocationBtn.isSelected = true
        showIdleScene(isHidden: true)
    }
    
    func showIdleScene(isHidden: Bool)
    {
        //idle scene에 해당하는 view들은 여기서 처리하자
        idleScene.isHidden = isHidden
        if !isHidden {
            mapView.isMyLocationEnabled = true
            mapView.camera = GMSCameraPosition(target:(locationMgr?.getSourceLocation())!, zoom: 17, bearing: 0, viewingAngle: 0)
        }
    }
    
    func refreshOrderPreview()
    {
        mapView.clear()
        showIdleScene(isHidden: false)
        orderPreviewScene.isHidden = true
        orderE_PriceLabel.text?.removeAll()
        orderX_PriceLabel.text?.removeAll()
        orderXL_PriceLabel.text?.removeAll()
    }
    
    func setTripScene(by state: USER_STATE)
    {
        guard let tripInfo = UserModel.getUser().driverInfo?.driver else {
            return
        }
        
        if tripScene.isHidden {
            tripScene.isHidden = false
        }
        
        //common setting
        tripSourceLabel.text = orderSourceLabel.text
        tripDestinationLabel.text = orderDestinationLabel.text
        driverRatingLabel.text = tripInfo.rating
        driverServiceTypeLabel.text = tripInfo.service
        driverNameLabel.text = tripInfo.name
        vehicleTypeLabel.text = tripInfo.carModel
        vehicleNumberLabel.text = tripInfo.carNumber
        requestMgr?.requestProfileImage(type: .Driver, seq: tripInfo.pseq!, handler: {
            (image) in
            self.driverImageView.image = image
        })
        requestMgr?.requestProfileImage(type: .Vehicle, seq: tripInfo.vseq!, handler: {
            (image) in
            self.vehicleImageView.image = image
        })
        
        //by state
        if state == .ORDER_PICKUP {
            tripCancelBtn.isHidden = false
            if UserModel.getUser().getOstate() == DriverState.DRIVER_DO_ING {
                tripStateLabel.text = "차량이 오는 중"
                tripStateLabel.textColor = .black
                tripRemainTimeInfoLabel.text = String.init(format: "%@ km / %d 분", tripInfo.edistance ?? "0", tripInfo.eduration?.toInt() ?? 0)
            } else if UserModel.getUser().getOstate() == DriverState.DRIVER_DO_END {
                tripStateLabel.text = "차량이 도착했습니다"
                tripStateLabel.textColor = CommonObject.colorByHex(rgb: Color.ChaCha)
                tripRemainTimeInfoLabel.text = "운전자 대기중"
                
            }
        } else if state == .ONTRIP {
            tripCancelBtn.isHidden = true
            if UserModel.getUser().getOstate() == DriverState.DRIVER_DO_ING {
                tripRemainTimeInfoLabel.text?.removeAll()
                tripStateLabel.text = "목적지로 이동 중"
                tripStateLabel.textColor = .black
            } else if UserModel.getUser().getOstate() == DriverState.DRIVER_DO_END {
                tripStateLabel.text = "차량이 도착했습니다"
            }
        }
        
        locationMgr?.drawPickUpScene(mapView: mapView, driverLocation: CommonObject.dataToLocation(data: tripInfo.nlocation ?? ""))
    }
    
    func showTripScene(isHidden: Bool)
    {
        mapView.clear()
        mapView.isMyLocationEnabled = isHidden
        orderPreviewScene.isHidden = true
        tripScene.isHidden = isHidden
        if !isHidden {
            setTripScene(by: UserModel.getUser().getState())
        }
    }
    
    func updateTripScene()
    {
        //update driver marker
        guard let tripInfo = UserModel.getUser().driverInfo?.driver else {
            return
        }
        
        if UserModel.getUser().getOstate() == .DRIVER_DO_ING {
            tripRemainTimeInfoLabel.text = String.init(format: "%@ km / %d 분", tripInfo.edistance ?? "0", tripInfo.eduration?.toInt() ?? 0)
        }
        locationMgr?.updateDriverPosition(CommonObject.dataToLocation(data: tripInfo.nlocation ?? ""))
    }
    
    func onOrderServiceSelected(view: UIView)
    {
        var bottomMargin: NSLayoutConstraint?
        var orderBg: UIView?
        var orderCheckBtn: UIButton?
        if view.isEqual(orderE_View) {
            bottomMargin = orderE_bottomMargin
            orderBg = orderE_View
            orderCheckBtn = orderE_Checkbox
        } else if view.isEqual(orderX_View) {
            bottomMargin = orderX_bottomMargin
            orderBg = orderX_View
            orderCheckBtn = orderX_Checkbox
        } else if view.isEqual(orderXL_View) {
            bottomMargin = orderXL_bottomMargin
            orderBg = orderXL_View
            orderCheckBtn = orderXL_Checkbox
        }
        
        for orderSelection in orderSelectCollection {
            if orderSelection.isEqual(view) {
                orderBg?.backgroundColor = CommonObject.colorByHex(rgb: Color.OrderSelectBg)
                orderCheckBtn?.isSelected = true
                UIView.animate(withDuration: 0.1, delay: 0.0, options: .curveLinear, animations: {
                    bottomMargin?.constant = 100
                    self.view.layoutIfNeeded()
                }, completion: { _ in
                        UIView.animate(withDuration: 0.1, delay: 2, options: .curveLinear, animations: {
                            bottomMargin?.constant = 0
                            self.view.layoutIfNeeded()
                        }, completion: nil)
                    })
            } else {
                onOrderServiceUnSelected(view: orderSelection)
            }
        }
    }
    
    func onOrderServiceUnSelected(view: UIView)
    {
        var bottomMargin: NSLayoutConstraint?
        var orderBg: UIView?
        var orderCheckBtn: UIButton?
        if view.isEqual(orderE_View) {
            bottomMargin = orderE_bottomMargin
            orderBg = orderE_View
            orderCheckBtn = orderE_Checkbox
        } else if view.isEqual(orderX_View) {
            bottomMargin = orderX_bottomMargin
            orderBg = orderX_View
            orderCheckBtn = orderX_Checkbox
        } else if view.isEqual(orderXL_View) {
            bottomMargin = orderXL_bottomMargin
            orderBg = orderXL_View
            orderCheckBtn = orderXL_Checkbox
        }
        
        orderBg?.backgroundColor = .white
        orderCheckBtn?.isSelected = false
        view.layer.removeAllAnimations()
        UIView.animate(withDuration: 0.1, delay: 0.1, options: .curveLinear, animations: {
            bottomMargin?.constant = 0
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    func animateWhenMapDragging(isDragging: Bool)
    {
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.5, delay: 0,
                           options: .curveLinear,
                           animations: {
                            if isDragging {
                                if #available(iOS 11.0, *) {
                                    self.addressViewBottomMargin.constant = self.addressView.frame.size.height + (UIApplication.shared.keyWindow?.safeAreaInsets.bottom)!
                                } else {
                                    self.addressViewBottomMargin.constant = self.addressView.frame.size.height
                                }
                                self.topNaviBarTopMargin.constant = -2 * self.topNaviBar.frame.height
                                self.idleLocationBtn.alpha = 0
                            } else {
                                self.addressViewBottomMargin.constant = -10
                                self.topNaviBarTopMargin.constant = 0
                                self.idleLocationBtn.alpha = 1
                            }
                            self.view.layoutIfNeeded()
            },
                           completion: nil)
        }
    }
    
    //MARK: State Change
    override func didPickUpStart()
    {
        showTripScene(isHidden: false)
        showIdleScene(isHidden: true)
    }
    override func didPickUpFail()
    {
        //leave it. not to do
    }
    override func didPickUpCancel()
    {
        showTripScene(isHidden: true)
        showIdleScene(isHidden: false)
    }
    override func didPickUpArrive()
    {
        let state = UserModel.getUser().getState()
        setTripScene(by: state)
    }
    override func didTripStart()
    {
        showTripScene(isHidden: false)
        showIdleScene(isHidden: true)
        //update start marker position as driver location
        if let sourcePosition = UserModel.getUser().driverInfo?.driver.nlocation {
            locationMgr?.updateSourceMarkerPosition(CommonObject.dataToLocation(data: sourcePosition))
        }
    }
    override func didTripArrive()
    {
        let chargeViewController = ChargeViewController.init(nibName: String(describing: ChargeViewController.self), bundle: nil)
        navigationController?.pushViewController(chargeViewController, animated: true)
    }
    override func didTripEnd()
    {
        showTripScene(isHidden: true)
        showIdleScene(isHidden: false)
    }
    override func didCancelByDriver()
    {
        //not to do (should implement inherited viewcontroller)
        let cancelViewController = CancelViewController.init(with: .DRIVER_CANCEL_ON_ORDER)
        cancelViewController.delegate = self
        navigationController?.pushViewController(cancelViewController, animated: true)
    }
    override func didTimeOut()
    {
        //not to do (should implement inherited viewcontroller)
    }
    override func didUpdateState()
    {
        let state = UserModel.getUser().getState()
        
        if state == .LOGIN_COMPLETE {
            //update drivers marker
        } else if state == .ORDER_PICKUP || state == .ONTRIP {
            updateTripScene()
        }
    }
    
    // MARK: Action
    @objc func buttonSelected(button: UIButton)
    {
        if button.isEqual(sourceAddressBtn) {
            locationMgr?.setSourceLocation(position: mapLocation!)
            let destinationViewController = DestinationViewController.init(nibName: String(describing: DestinationViewController.self), bundle: nil)
            destinationViewController.delegate = self
            destinationViewController.searchKeyword = sourceLabel.text
            destinationViewController.viewType = .from
            navigationController?.pushViewController(destinationViewController, animated: true)
        } else if button.isEqual(destinationAddressBtn) {
            locationMgr?.setSourceLocation(position: mapLocation!)
            let destinationViewController = DestinationViewController.init(nibName: String(describing: DestinationViewController.self), bundle: nil)
            destinationViewController.delegate = self
            destinationViewController.searchKeyword = ""
            destinationViewController.viewType = .to
            navigationController?.pushViewController(destinationViewController, animated: true)
        } else if button.isEqual(orderSourceBtn) {
            let destinationViewController = DestinationViewController.init(nibName: String(describing: DestinationViewController.self), bundle: nil)
            destinationViewController.delegate = self
            destinationViewController.searchKeyword = orderSourceLabel.text
            destinationViewController.viewType = .from
            navigationController?.pushViewController(destinationViewController, animated: true)
        } else if button.isEqual(orderDestinationBtn) {
            let destinationViewController = DestinationViewController.init(nibName: String(describing: DestinationViewController.self), bundle: nil)
            destinationViewController.delegate = self
            destinationViewController.searchKeyword = orderDestinationLabel.text
            destinationViewController.viewType = .to
            navigationController?.pushViewController(destinationViewController, animated: true)
        } else if button.isEqual(orderCancelBtn) {
            locationMgr?.setDestinationLocation(position: kCLLocationCoordinate2DInvalid)
            refreshOrderPreview()
        } else if button.isEqual(orderE_Btn) {
            onOrderServiceSelected(view: orderE_View)
        } else if button.isEqual(orderX_Btn) {
            onOrderServiceSelected(view: orderX_View)
        } else if button.isEqual(orderXL_Btn) {
            onOrderServiceSelected(view: orderXL_View)
        } else if button.isEqual(idleLocationBtn) {
            mapView.animate(toLocation: (locationMgr?.getCurrentLocation())!)
            button.isSelected = true
        } else if button.isEqual(previewLocationBtn) {
            if let routes = locationMgr?.getCurrentRoutes() {
                mapView.animate(with: GMSCameraUpdate.fit(routes, with:mapView.routesEdgeInsets()))
            }
            button.isSelected = true
        } else if button.isEqual(orderConfirmBtn) {
            if (locationMgr?.shouldRemindLocationBeforePickUp())!
            {
                let destinationViewController = DestinationViewController.init(nibName: String(describing: DestinationViewController.self), bundle: nil)
                destinationViewController.delegate = self
                destinationViewController.viewType = .confirm_from
                present(destinationViewController, animated: true, completion: nil)
                locationMgr?.didRemindLocationBeforePickUp()
            } else {
                requestOrderIfAvailable()
            }
        } else if button.isEqual(tripCancelBtn) {
            let cancelViewController = CancelViewController.init(with: .RIDER_CANCEL_ON_START)
            navigationController?.pushViewController(cancelViewController, animated: true)
        }
    }
    
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.delegate = self
        mapView.settings.rotateGestures = false
        mapView.isMyLocationEnabled = true
        
        mapView.camera = GMSCameraPosition(target:(locationMgr?.getCurrentLocation())!, zoom: 17, bearing: 0, viewingAngle: 0)
        
        locationMgr?.setDelegate(delegate: self)
        
        CommonObject.addShadow(view: addressView)
        CommonObject.addShadow(view: orderAddressView)
        CommonObject.addShadow(view: tripAddressView)
        CommonObject.addShadow(view: orderE_Checkbox)
        CommonObject.addShadow(view: orderX_Checkbox)
        CommonObject.addShadow(view: orderXL_Checkbox)
        
        CommonObject.setBorderLine(view: tripCancelBtn)
        CommonObject.setCornerRadius(view: tripCancelBtn, 3)
        
        sourceLabel.textColor = CommonObject.colorByHex(rgb: Color.ChaCha)
        orderSourceLabel.textColor = CommonObject.colorByHex(rgb: Color.ChaCha)
        tripSourceLabel.textColor = CommonObject.colorByHex(rgb: Color.ChaCha)
        
        sourceAddressBtn.addTarget(self, action: #selector(buttonSelected(button:)), for: .touchUpInside)
        destinationAddressBtn.addTarget(self, action: #selector(buttonSelected(button:)), for: .touchUpInside)
        idleLocationBtn.addTarget(self, action: #selector(buttonSelected(button:)), for: .touchUpInside)
        orderSourceBtn.addTarget(self, action: #selector(buttonSelected(button:)), for: .touchUpInside)
        orderDestinationBtn.addTarget(self, action: #selector(buttonSelected(button:)), for: .touchUpInside)
        orderCancelBtn.addTarget(self, action: #selector(buttonSelected(button:)), for: .touchUpInside)
        orderE_Btn.addTarget(self, action: #selector(buttonSelected(button:)), for: .touchUpInside)
        orderX_Btn.addTarget(self, action: #selector(buttonSelected(button:)), for: .touchUpInside)
        orderXL_Btn.addTarget(self, action: #selector(buttonSelected(button:)), for: .touchUpInside)
        previewLocationBtn.addTarget(self, action: #selector(buttonSelected(button:)), for: .touchUpInside)
        orderConfirmBtn.addTarget(self, action: #selector(buttonSelected(button:)), for: .touchUpInside)
        callToDriverBtn.addTarget(self, action: #selector(buttonSelected(button:)), for: .touchUpInside)
        chatToDriverBtn.addTarget(self, action: #selector(buttonSelected(button:)), for: .touchUpInside)
        tripCancelBtn.addTarget(self, action: #selector(buttonSelected(button:)), for: .touchUpInside)
        
        orderE_View.layer.cornerRadius = orderE_View.frame.size.width / 2
        orderX_View.layer.cornerRadius = orderX_View.frame.size.width / 2
        orderXL_View.layer.cornerRadius = orderXL_View.frame.size.width / 2
        tripStarView.layer.cornerRadius = CommonObject.sizeByResolution(size: 8)
        driverImageView.layer.cornerRadius = CommonObject.sizeByResolution(size: driverImageView.frame.size.width) / 2
        vehicleImageView.layer.cornerRadius = CommonObject.sizeByResolution(size: driverImageView.frame.size.width) / 2
        
        //set controls by USER_STATE
        setSceneWhenAppLaunching()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

// MARK: User state
extension MainViewController
{
    //TODO: user state 가져오는 로직 수정할 것 (UserModel에서 가져오도록)
    /// 어떤 뷰가 보여지는에 따라 user state를 판단하자
    ///
    /// - Returns: enum USER_STATE

    func currentUserState() -> USER_STATE
    {
        if !idleScene.isHidden {
            return .LOGIN_COMPLETE
        } else if !orderPreviewScene.isHidden {
            return .ORDER_PREVIEW
        } else {
            return .LOGIN_YET
        }
    }
}

// MARK: Location Delegate
extension MainViewController: LocationDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateHeading newHeading: CLHeading)
    {
        
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
        
    }
    
    func locationManager(_ manager: CLLocationManager, didFoundAddress address: String, location: CLLocationCoordinate2D) {
        sourceLabel.text = address
    }
    
    func locationManager(_ manager: CLLocationManager, didFoundRoutes routes: NSDictionary, _ from: CLLocationCoordinate2D, _ to: CLLocationCoordinate2D) {
        if let result = calculateDistanceFromRoutes(routes: routes) {
            locationMgr?.drawRoutesPreviewLine(mapView: self.mapView)
            requestOrderPreview(from: from, to: to, routeInfo: result)
        }
    }
}

// MARK: Destination Delegate
extension MainViewController: DestinationDelegate
{
    func didSelectSource(source: CLLocationCoordinate2D, address: String)
    {
        //should update main address label
        sourceLabel.text = address
        orderSourceLabel.text = address
        
        if currentUserState() == .LOGIN_COMPLETE {
            mapView.camera = GMSCameraPosition(target:(locationMgr?.getSourceLocation())!, zoom: 17, bearing: 0, viewingAngle: 0)
        }
    }
    
    func didSelectDestination(destination: CLLocationCoordinate2D, address: String)
    {
        //should update order preview scene
        showIdleScene(isHidden: true)
        orderPreviewScene.isHidden = false
        orderSourceLabel.text = sourceLabel.text
        orderDestinationLabel.text = address
    }
    
    func didConfirmSourceLocation(source: CLLocationCoordinate2D, address: String)
    {
        //should update main address label
        sourceLabel.text = address
        orderSourceLabel.text = address
        
        if (locationMgr?.shouldRequestPickUp())! {
            requestOrderIfAvailable()
        }
    }
}

// MARK: CancelView Delegate
extension MainViewController: CancelViewDelegate
{
    func reOrder()
    {
        orderPreviewScene.isHidden = false
    }
    
    func orderLater()
    {
        showTripScene(isHidden: true)
        showIdleScene(isHidden: false)
    }
}

// MARK: Google Map Delegate
extension MainViewController: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        guard currentUserState() == .LOGIN_COMPLETE else {
            return
        }
        mapLocation = position.target
        locationMgr?.getAddressFromTmap(position: position.target)
        animateWhenMapDragging(isDragging: false)
    }
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        guard gesture else {
            return
        }
        let userState = currentUserState()
        if userState == .LOGIN_COMPLETE {
            idleLocationBtn.isSelected = false
        } else if userState == .ORDER_PREVIEW {
            previewLocationBtn.isSelected = false
        }
        animateWhenMapDragging(isDragging: true)
    }
}
