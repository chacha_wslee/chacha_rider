//
//  RequestManager.swift
//  Taxi
//
//  Created by wslee-chacha on 2018. 7. 17..
//  Copyright © 2018년 chachacreation.co.kr. All rights reserved.
//

import UIKit
import AFNetworking

enum ProfileType: Int {
    case User = 0
    case Driver
    case Vehicle
}

class RequestManager: NSObject {
    
    var apiList : NSDictionary?
    
    override init() {
        super.init()
        apiList = httpApi()
    }
    
    func httpApi() -> NSDictionary?
    {
        if let plistP = Bundle.main.path(forResource: "httpapi", ofType: "plist", inDirectory: "taxi.bundle") {
            return NSDictionary.init(contentsOfFile: plistP)
        }
        return nil
    }
    
    func urlWithMethodId(methodId: String) -> String?
    {
        if let api = apiList?.object(forKey: methodId) as? NSDictionary {
            let url = String(format: "%@/%@.php", APPURL.BaseURL, api.object(forKey: "url") as! String)
            return url
        }
        return nil
    }
    
    func requestParameters(methodId: String) -> NSDictionary?
    {
        if let api = apiList?.object(forKey: methodId) as? NSDictionary {
            return ["cmd" : api.object(forKey: "url") as! String,
                    "req" : api.object(forKey: "req") as! String,
                    "mid" : methodId]
        }
        return nil
    }
    
    func commonParameters() -> NSDictionary
    {
        let info = Bundle.main.infoDictionary
        let version = info!["CFBundleShortVersionString"] as! String
        let app = String(format: "%@:%@", Bundle.main.bundleIdentifier!, version)
        let deviceId = "AAAAAAAA-AAAA-AAAA-AAAA-AAAAAAAAAAAA"
        let ptoken = "56e313904c655181faf2b6ef3a4bc0ee6bd99d6361eb4742562901c841b87c05"
        let userId = "mcwooseok@naver.com"
        let datoken = String(format: "%@:%@", deviceId, ptoken)
        
        return ["uid" : userId,
                "ptoken" : ptoken,
                "did" : deviceId,
                "app" : app,
                "datoken" : datoken]
        
    }
    
    private func requestKey(by: ProfileType) -> String
    {
        if by == .User {
            return "useq"
        } else if by == .Driver {
            return "pseq"
        } else if by == .Vehicle {
            return "vseq"
        } else {
            return "useq"
        }
    }
    
    private func requestUrl(by: ProfileType) -> String
    {
        if by == .User {
            return API.kUserProfileImage
        } else if by == .Driver {
            return API.kDriverProfileImage
        } else if by == .Vehicle {
            return API.kVehicleProfileImage
        } else {
            return API.kUserProfileImage
        }
    }
    
    func requestProfileImage(type: ProfileType, seq: String, handler:((UIImage) -> Void)? = nil)
    {
        let url = urlWithMethodId(methodId: requestUrl(by: type))
        
        let commonParameters = self.commonParameters()
        let requestParameters = self.requestParameters(methodId: requestUrl(by: type))
        let additionalParameters = [requestKey(by: type) : seq]
        
        let parameter = NSMutableDictionary()
        parameter.addEntries(from: commonParameters as! [String : Any])
        parameter.addEntries(from: requestParameters as! [String : Any])
        parameter.addEntries(from: additionalParameters)
        
        let manager = AFHTTPSessionManager(baseURL: nil)
        manager.responseSerializer = AFImageResponseSerializer()
        manager.requestSerializer.cachePolicy = .returnCacheDataElseLoad
        manager.requestSerializer.setValue("image/jpeg", forHTTPHeaderField: "Content-Type")
        
        manager.get(url!, parameters: parameter, progress: { _ in
            
        }, success: {
            (operation, responseObject) in
            print("\(#function) success")
            if let image = responseObject as? UIImage {
                handler?(image)
            } else {
                if let currentViewController = (UIApplication.shared.delegate as! AppDelegate).navigationController?.visibleViewController {
                    CommonObject.popupWithOneButton(target: currentViewController, title: "", message: "이미지를 불러오지 못하였습니다")
                }
            }
            
        }, failure: {
            (task, error) in
            print("\(#function) failure : \(error.localizedDescription)")
        })
    }
}
