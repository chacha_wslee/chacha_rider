//
//  ResponseObject_Swift.swift
//  Taxi
//
//  Created by wslee-chacha on 2018. 7. 18..
//  Copyright © 2018년 chachacreation.co.kr. All rights reserved.
//

import UIKit

class ResponseObject: NSObject {
    
    var result: Int?
    var response: NSDictionary?
    var error: String?
    
    init(responseObject: Any) {
        if let responseDic = responseObject as? NSDictionary {
            result = responseDic.object(forKey: "rlt") as? Int ?? 0
            error = responseDic.object(forKey: "error") as? String ?? ""
            response = responseDic
        }
        super.init()
    }
}
