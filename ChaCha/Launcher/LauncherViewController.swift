//
//  LauncherViewController_Swift.swift
//  Taxi
//
//  Created by wslee-chacha on 2018. 7. 17..
//  Copyright © 2018년 chachacreation.co.kr. All rights reserved.
//

import UIKit
import AFNetworking

class LauncherViewController: BaseViewController {
    
    @IBOutlet var backgroundView: UIImageView!
    
    func requestLogin() {
        let url = requestMgr?.urlWithMethodId(methodId: API.kLogin)
        
        let commonParameters = requestMgr?.commonParameters()
        let requestParameters = requestMgr?.requestParameters(methodId: API.kLogin)
        let additionalParameters = ["utelno" : "+821041688760"]
        
        let parameter = NSMutableDictionary()
        parameter.addEntries(from: commonParameters as! [String : Any])
        parameter.addEntries(from: requestParameters as! [String : Any])
        parameter.addEntries(from: additionalParameters)
        
        let manager = AFHTTPSessionManager(baseURL: nil)
        manager.responseSerializer = AFJSONResponseSerializer()
        
        manager.put(url!, parameters: parameter, success: {
            (operation, responseObject: Any) in
            let response = ResponseObject(responseObject: responseObject)
            print("\(String(describing: response.response))")
            if response.result == 1 {
                UserModel.setUser(UserModel.init(userInfo: response.response!))
                if let payments = response.response!["user_payments"] as? NSArray {
                    self.paymentMgr.addCardArray(cardArray: payments)
                }
                //go to map view
                let state = UserModel.getUser().getState()
                if state == .LOGIN_COMPLETE {
                    let mainViewController = MainViewController.init(nibName: String(describing: MainViewController.self), bundle: nil)
                    self.navigationController?.pushViewController(mainViewController, animated: true)
                } else {
                    //wait for response by state
                    //mainviewcontroller will start after didSetUserInformation() called.
                }
            }
        }, failure: {
            (task, error) in
            print("failure : \(error.localizedDescription)")
        })
    }
    
    func launchImageByDevice() -> UIImage? {
        var bg: UIImage?
        let model = MODEL()
        
        switch model {
            case TARGET.SE:
                bg = #imageLiteral(resourceName: "bg_launch_se")
            case TARGET.NORMAL:
                bg = #imageLiteral(resourceName: "bg_launch_normal")
            case TARGET.PLUS:
                bg = #imageLiteral(resourceName: "bg_launch_plus")
            case TARGET.X:
                bg = #imageLiteral(resourceName: "bg_launch_iphoneX")
            default:
                bg = #imageLiteral(resourceName: "bg_launch_pad")
        }
        
        return bg
    }
    
    //MARK: State Manager
    override func didSetUserInformation()
    {
        let mainViewController = MainViewController.init(nibName: String(describing: MainViewController.self), bundle: nil)
        navigationController?.pushViewController(mainViewController, animated: true)
    }

    //MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        backgroundView.image = launchImageByDevice()
        requestLogin()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
