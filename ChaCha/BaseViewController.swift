//
//  BaseViewController_Swift.swift
//  Taxi
//
//  Created by wslee-chacha on 2018. 7. 17..
//  Copyright © 2018년 chachacreation.co.kr. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var requestMgr: RequestManager?
    var locationMgr: LocationManager?
    var userModel = (UIApplication.shared.delegate as! AppDelegate).userModel!
    var paymentMgr = (UIApplication.shared.delegate as! AppDelegate).paymentMgr!
    var stateMgr = (UIApplication.shared.delegate as! AppDelegate).stateMgr
    
    @IBOutlet var backBtn: UIButton?
    @IBOutlet var mainTitleLabel: UILabel?

    override func viewDidLoad() {
        super.viewDidLoad()
        requestMgr = appDelegate.requestMgr
        locationMgr = appDelegate.locationMgr
        stateMgr?.setDelegate(delegate: self)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if let navigationController = appDelegate.navigationController,
            !navigationController.viewControllers.contains(self) {
            //have been popped by UINavigationController
            //TODO: modal viewcontroller들은 어떤 식으로 remove할 것인가? 현재 modelviewcontroller는 disappear되면 항상 remove됨
            stateMgr?.removeDelegate(delegate: self)
        }
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }

    func onServerError(response: ResponseObject)
    {
        //TODO: error 처리
        print("\(response.error!)")
        CommonObject.popupWithOneButton(target: self, title: "", message: response.error!)
    }
}

extension BaseViewController: StateManagerDelegate
{
    func didSetUserInformation()
    {
        //not to do (should implement inherited viewcontroller)
    }
    func didPickUpStart()
    {
        //not to do (should implement inherited viewcontroller)
    }
    func didPickUpFail()
    {
        //not to do (should implement inherited viewcontroller)
    }
    func didPickUpCancel()
    {
        //not to do (should implement inherited viewcontroller)
    }
    func didPickUpArrive()
    {
        //not to do (should implement inherited viewcontroller)
    }
    func didTripStart()
    {
        //not to do (should implement inherited viewcontroller)
    }
    func didTripArrive()
    {
        //not to do (should implement inherited viewcontroller)
    }
    func didTripEnd()
    {
        //not to do (should implement inherited viewcontroller)
    }
    func didCancelByDriver()
    {
        //not to do (should implement inherited viewcontroller)
    }
    func didTimeOut()
    {
        //not to do (should implement inherited viewcontroller)
    }
    func didUpdateState()
    {
        //not to do (should implement inherited viewcontroller)
    }
}
