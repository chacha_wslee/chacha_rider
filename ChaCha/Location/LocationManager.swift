//
//  LocationManager.swift
//  Taxi
//
//  Created by wslee-chacha on 2018. 7. 19..
//  Copyright © 2018년 chachacreation.co.kr. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps
import AFNetworking

struct AddressInfo {
    var fullAddress: String?
    var adminInfo: ADMINTYPE?
    var roadInfo: ROADTYPE?
    var location: CLLocationCoordinate2D = kCLLocationCoordinate2DInvalid
    
    struct ROADTYPE {
        var address: String {
            get {
                return String(format: "%@ %@ %@ %@", gu_gun!, roadName!, buildingIndex!, buildingName!)
            }
        }
        var simpleAddress: String {
            get {
                return String(format: "%@ %@", gu_gun!, roadName!)
            }
        }
        var gu_gun: String?
        var roadName: String?
        var buildingIndex: String?
        var buildingName: String?
    }
    
    struct ADMINTYPE {
        var address: String {
            get {
                return String(format: "%@ %@ %@", gu_gun!, legalDong!, bunji!)
            }
        }
        var simpleAddress: String {
            get {
                return String(format: "%@ %@", gu_gun!, legalDong!)
            }
        }
        var gu_gun: String?
        var legalDong: String?
        var bunji: String?
    }
}

@objc protocol LocationDelegate: AnyObject {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    func locationManager(_ manager: CLLocationManager, didUpdateHeading newHeading: CLHeading)
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    func locationManager(_ manager: CLLocationManager, didFoundAddress address: String, location: CLLocationCoordinate2D)
    @objc optional func locationManager(_ manager: CLLocationManager, didFoundRoutes routes: NSDictionary, _ from: CLLocationCoordinate2D, _ to: CLLocationCoordinate2D)
    @objc optional func locationManager(_ manager: CLLocationManager, didFoundPois pois: NSArray)
}

class LocationManager: NSObject {

    var locationMgr = CLLocationManager()
    var requestMgr: RequestManager?
    
    var paths = GMSMutablePath()
    var tripLine: GMSPolyline?
    var historyLine: GMSPolyline?
    var bounds: GMSCoordinateBounds?
    var startMarker = Marker()
    var endMarker = Marker()
    var driverMarker = Marker()
    var unMatchedDrivers = NSMutableArray() //type of [Marker]
    
    var steps = NSMutableArray()
    
    var delegates = [LocationDelegate]()
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    var currentLocation: CLLocationCoordinate2D = kCLLocationCoordinate2DInvalid
    var sourceLocation: CLLocationCoordinate2D = kCLLocationCoordinate2DInvalid
    var destinationLocation: CLLocationCoordinate2D = kCLLocationCoordinate2DInvalid
    var driverLocation: CLLocationCoordinate2D = kCLLocationCoordinate2DInvalid {
        willSet (newLocation) {
            if !CommonObject.isEqualLocation(first: driverLocation, second: kCLLocationCoordinate2DInvalid) {
                animateDriverMovement(marker: driverMarker, previous: driverLocation, current: newLocation)
            }
        }
    }
    var sourceAddress = ""
    var destinationAddress = ""
    
    var remindLocationBeforePickUp: Bool = true
    var needToRequestRoutes: Bool = false
    
    
    override init() {
        super.init()
        locationMgr.desiredAccuracy = kCLLocationAccuracyBest
        locationMgr.distanceFilter = kCLDistanceFilterNone
        locationMgr.allowsBackgroundLocationUpdates = false
        locationMgr.pausesLocationUpdatesAutomatically = true
        locationMgr.delegate = self
        locationMgr.requestWhenInUseAuthorization()
    }
    
    func setDelegate(delegate: LocationDelegate)
    {
        delegates.append(delegate)
    }
    
    func removeDelegate()
    {
        delegates.removeLast()
    }
    
    func startUpdateLocations()
    {
        locationMgr.startUpdatingLocation()
    }
    
    func stopUpdateLocations()
    {
        locationMgr.stopUpdatingLocation()
    }
    
    func setCurrentLocation(position: CLLocationCoordinate2D)
    {
        currentLocation = position
    }
    
    func setSourceLocation(position: CLLocationCoordinate2D)
    {
        sourceLocation = position
    }
    
    func setDestinationLocation(position: CLLocationCoordinate2D)
    {
        destinationLocation = position
    }
    
    func setDriverLocation(position: CLLocationCoordinate2D)
    {
        driverLocation = position
    }
    
    func setSourceAddress(address: String)
    {
        sourceAddress = address
    }
    
    func setDestinationAddress(address: String)
    {
        destinationAddress = address
    }
    
    func clearLocation()
    {
        sourceLocation = kCLLocationCoordinate2DInvalid
        destinationLocation = kCLLocationCoordinate2DInvalid
    }
    
    func getCurrentLocation() -> CLLocationCoordinate2D
    {
        return currentLocation
    }
    
    func getSourceLocation() -> CLLocationCoordinate2D
    {
        return sourceLocation
    }
    
    func getDestinationLocation() -> CLLocationCoordinate2D
    {
        return destinationLocation
    }
    
    func getDriverLocation() -> CLLocationCoordinate2D
    {
        return driverLocation
    }
    
    func getSourceAddress() -> String
    {
        return sourceAddress
    }
    
    func getDestinationAddress() -> String
    {
        return destinationAddress
    }
    
    func getCurrentRoutes() -> GMSCoordinateBounds?
    {
        return bounds ?? nil
    }
    
    // MARK: drawRoute
    func drawRoutesPreviewLine(mapView: GMSMapView)
    {
        if self.paths.count() > 0 {
            mapView.clear()
            
            tripLine = GMSPolyline.init(path: paths)
            tripLine?.strokeWidth = 5.0
            tripLine?.strokeColor = UIColor.black
            tripLine?.map = mapView
            
            bounds = GMSCoordinateBounds(path: paths)
            mapView.animate(with: GMSCameraUpdate.fit(bounds!, with: mapView.routesEdgeInsets()))
            
            startMarker = Marker.init(.START, where: paths.sourcePosition())
            endMarker = Marker.init(.END, where: paths.destinationPosition())
            startMarker.setMarkerTo(map: mapView)
            endMarker.setMarkerTo(map: mapView)
        }
    }
    
    // MARK: draw pick up
    func drawPickUpScene(mapView: GMSMapView, driverLocation: CLLocationCoordinate2D)
    {
        //TODO: app 껐다 켤 때 paths size = 0
        startMarker = Marker.init(.START, where: getSourceLocation())
        endMarker = Marker.init(.END, where: getDestinationLocation())
        driverMarker = Marker.init(.DRIVER_E, where: driverLocation)
        startMarker.setMarkerTo(map: mapView)
        endMarker.setMarkerTo(map: mapView)
        driverMarker.setMarkerTo(map: mapView)
    }
    
    func updateSourceMarkerPosition(_ position: CLLocationCoordinate2D)
    {
        startMarker.setMarkerPosition(position)
    }
    
    func updateDriverPosition(_ position: CLLocationCoordinate2D)
    {
        setDriverLocation(position: position)
        driverMarker.setMarkerPosition(position)
    }
    
    func animateDriverMovement(marker: Marker, previous: CLLocationCoordinate2D, current: CLLocationCoordinate2D)
    {
        marker.rotateMarker(from: previous, to: current)
        marker.animateMarker(to: current)
    }
    
    // MARK: T-map Request
    func convertToAddressInfo(responseObject: [String: AnyObject]) -> AddressInfo?
    {
        if let info = responseObject["addressInfo"] as? [String: String] {
            var addressInfo = AddressInfo.init()
            var adminInfo = AddressInfo.ADMINTYPE.init()
            var roadInfo = AddressInfo.ROADTYPE.init()
            
            addressInfo.fullAddress = info[TMAP.RESPONSE.GEOCODE.FULLADDRESS]
            
            adminInfo.gu_gun = info[TMAP.RESPONSE.GEOCODE.GU_GUN]
            adminInfo.legalDong = info[TMAP.RESPONSE.GEOCODE.LEGAL_DONG]
            adminInfo.bunji = info[TMAP.RESPONSE.GEOCODE.BUNJI]
            
            roadInfo.gu_gun = info[TMAP.RESPONSE.GEOCODE.GU_GUN]
            roadInfo.roadName = info[TMAP.RESPONSE.GEOCODE.ROADNAME]
            roadInfo.buildingIndex = info[TMAP.RESPONSE.GEOCODE.BUILDING_INDEX]
            roadInfo.buildingName = info[TMAP.RESPONSE.GEOCODE.BUILDING_NAME]
            
            addressInfo.adminInfo = adminInfo
            addressInfo.roadInfo = roadInfo
            return addressInfo
        } else {
            return nil
        }
    }
    
    func convertPOIsFromTmap(responseObject: NSDictionary) -> [AddressInfo]?
    {
        if let searchResult = responseObject["searchPoiInfo"] as? [String: AnyObject] {
            if let pois = searchResult["pois"] as? [String: AnyObject] {
                if let poi = pois["poi"] as? [NSDictionary] {
                    var results = [AddressInfo]()
                    for po in poi {
                        if po is [String: String] {
                            let location = CommonObject.locationFromFormattedString(latitude: po["frontLat"] as! String, longitude: po["frontLon"] as! String)
                            var roadInfo = AddressInfo.ROADTYPE.init()
                            roadInfo.gu_gun = String(format: "%@ %@", po["upperAddrName"] as! String, po["middleAddrName"] as! String)
                            roadInfo.roadName = (po["roadName"] as! String)
                            roadInfo.buildingIndex = (po["firstBuildNo"] as! String)
                            roadInfo.buildingName = (po["name"] as! String)
                            var adminInfo = AddressInfo.ADMINTYPE.init()
                            adminInfo.gu_gun = String(format: "%@ %@", po["upperAddrName"] as! String, po["middleAddrName"] as! String)
                            adminInfo.legalDong = (po["lowerAddrName"] as! String)
                            adminInfo.bunji = (po["firstNo"] as! String)
                            results.append(AddressInfo.init(fullAddress: adminInfo.address, adminInfo: adminInfo, roadInfo: roadInfo, location: location))
                        }
                    }
                    return results
                }
            }
        }
        return nil
    }
    
    func convertToGMSPath(responseObj: NSDictionary) -> NSDictionary?
    {
        guard let features = responseObj.object(forKey: "features") as? [NSDictionary] else {
            return nil
        }
        
        let turnTypeArr = ["0": "휴게소",
                            "1": "도곽에 의한 점",
                            "2": "타일에 의한 점",
                            "3": "고속도로에 의한 안내없음",
                            "4": "일반도로에 의한 안내없음",
                            "5": "특수한 경우 안내없음",
                            "6": "Y자 오른쪽 안내없음",
                            "7": "Y자 왼쪽 안내없음",
                            "11": "직진",
                            "12": "좌회전",
                            "13": "우회전",
                            "14": "U 턴",
                            "15": "P 턴",
                            "16": "8시 방향 좌회전",
                            "17": "10시 방향 좌회전",
                            "18": "2시 방향 우회전",
                            "19": "4시 방향 우회전",
                            "43": "오른쪽",
                            "44": "왼쪽",
                            "51": "직진 방향",
                            "52": "왼쪽 차선",
                            "53": "오른쪽 차선",
                            "54": "1차선",
                            "55": "2차선",
                            "56": "3차선",
                            "57": "4차선",
                            "58": "5차선",
                            "59": "6차선",
                            "60": "7차선",
                            "61": "8차선",
                            "62": "9차선",
                            "63": "10차선",
                            "71": "첫번째 출구",
                            "72": "두번째 출구",
                            "73": "첫번째 오른쪽 길",
                            "74": "두번째 오른쪽 길",
                            "75": "첫번째 왼쪽 길",
                            "76": "두번째 왼쪽 길",
                            "101": "우측 고속도로 입구",
                            "102": "좌측 고속도로 입구",
                            "103": "전방 고속도로 입구",
                            "104": "우측 고속도로 출구",
                            "105": "좌측 고속도로 출구",
                            "106": "전방 고속도로 출구",
                            "111": "우측 도시고속도로 입구",
                            "112": "좌측 도시고속도로 입구",
                            "113": "전방 도시고속도로 입구",
                            "114": "우측 도시고속도로 출구",
                            "115": "좌측 도시고속도로 출구",
                            "116": "전방 도시고속도로 출구",
                            "117": "우측 방향",
                            "118": "좌측 방향",
                            "119": "지하차도",
                            "120": "고가도로",
                            "121": "터널",
                            "122": "교량",
                            "123": "지하차도 옆",
                            "124": "고가도로 옆",
                            "130": "토끼굴 진입",
                            "131": "1시 방향",
                            "132": "2시 방향",
                            "133": "3시 방향",
                            "134": "4시 방향",
                            "135": "5시 방향",
                            "136": "6시 방향",
                            "137": "7시 방향",
                            "138": "8시 방향",
                            "139": "9시 방향",
                            "140": "10시 방향",
                            "141": "11시 방향",
                            "142": "12시 방향",
                            "150": "졸음쉼터",
                            "151": "휴게소",
                            "182": "왼쪽방향 도착안내",
                            "183": "오른쪽방향 도착안내",
                            "184": "경유지",
                            "185": "첫번째경유지",
                            "186": "두번째경유지",
                            "187": "세번째경유지",
                            "188": "네번째경유지",
                            "189": "다섯번째경유지",
                            "191": "제한속도",
                            "192": "사고다발",
                            "193": "급커브",
                            "194": "낙석주의",
                            "200": "출발지",
                            "201": "도착지",
                            "203": "목적지건너편",
                            "211": "횡단보도",
                            "212": "좌측 횡단보도",
                            "213": "우측 횡단보도",
                            "214": "8시 방향 횡단보도",
                            "215": "10시 방향 횡단보도",
                            "216": "2시 방향 횡단보도",
                            "217": "4시 방향 횡단보도",
                            "218": "엘리베이터",
                            "233": "직진 임시"]
        
        if let first = features.first as? [String: AnyObject], let last = features.last as? [String: AnyObject]
        {
            self.paths.removeAllCoordinates()
            self.steps.removeAllObjects()
            
            let totalDistance = (first["properties"]!["totalDistance"] as! NSNumber).stringValue
            let totalDuration = (first["properties"]!["totalTime"] as! NSNumber).stringValue
            let startLatitude = ((first["geometry"]!["coordinates"] as! [NSNumber]).last)?.stringValue
            let startLongitude = ((first["geometry"]!["coordinates"] as! [NSNumber]).first)?.stringValue
            let endLatitude = ((last["geometry"]!["coordinates"] as! [NSNumber]).last)?.stringValue
            let endLongitude = ((last["geometry"]!["coordinates"] as! [NSNumber]).first)?.stringValue
            
            let steps: NSMutableArray = []
            for step in features {
                if (features.first?.isEqual(step))! {
                    continue
                }
                let geometry = step.object(forKey: "geometry") as! [String: AnyObject]
                let properties = step.object(forKey: "properties") as! [String: AnyObject]
                if (geometry["type"] as! NSString).isEqual(to: "LineString"), !(properties["name"] as! NSString).isEqual(to: "") {
                    let coordinates = geometry["coordinates"] as! NSArray
                    let properties = step["properties"] as! NSDictionary
                    let distance = (properties["distance"] as! NSNumber).stringValue
                    let duration = (properties["time"] as! NSNumber).stringValue
                    let startLocation = coordinates.firstObject as! [NSNumber]
                    let endLocation = coordinates.lastObject as! [NSNumber]
                    
                    let polyline = NSMutableArray()
                    
                    for coordinate in coordinates {
                        let pointLocation = CLLocation(latitude: ((coordinate as! NSArray).lastObject as! NSNumber).doubleValue, longitude: ((coordinate as! NSArray).firstObject as! NSNumber).doubleValue)
                        polyline.add(pointLocation)
                    }
                    
                    if let val = properties["turnType"] as? NSNumber {
                        let ttype = val.stringValue
                        if let turn = turnTypeArr[ttype] as NSString? {
                            let description = NSString.init(format: "%@ %@ %@", properties["nextRoadName"] as! NSString, distance, turn)
                            steps.add([
                                "start_location":["lat":startLocation.last?.stringValue, "lng":startLocation.first?.stringValue],
                                "end_location":["lat":endLocation.last?.stringValue, "lng":endLocation.first?.stringValue],
                                "distance":["text":distance, "value":distance],
                                "duration":["text":duration, "value":duration],
                                "polyline":polyline,
                                "html_instructions":description,
                                "maneuver":turn
                                        ])
                        } else {
                            steps.add([
                                "start_location":["lat":startLocation.last?.stringValue, "lng":startLocation.first?.stringValue],
                                "end_location":["lat":endLocation.last?.stringValue, "lng":endLocation.first?.stringValue],
                                "distance":["text":distance, "value":distance],
                                "duration":["text":duration, "value":duration],
                                "polyline":polyline,
                                "html_instructions":properties["nextRoadName"] as! NSString,
                                ])
                        }
                    } else {
                        steps.add([
                            "start_location":["lat":startLocation.last?.stringValue, "lng":startLocation.first?.stringValue],
                            "end_location":["lat":endLocation.last?.stringValue, "lng":endLocation.first?.stringValue],
                            "distance":["text":distance, "value":distance],
                            "duration":["text":duration, "value":duration],
                            "polyline":polyline,
                            "html_instructions":properties["description"] as! NSString,
                            ])
                    }
                } else if (geometry["type"] as! NSString).isEqual(to: "Point") {
                    if let properties = step.object(forKey: "properties") as? NSDictionary, var updateSteps = steps.lastObject as? [String: AnyObject] {
                        if let ttype = properties["turnType"] as? NSNumber {
                            let turnType = ttype.stringValue
                            let turn = turnTypeArr[turnType]! as NSString
                            updateSteps["maneuver"] = turn
                            steps.replaceObject(at: steps.count-1, with: updateSteps)
                        }
                    }
                }
            }
            
            let googleJSON = ["routes":["legs":[
                "distance":totalDistance,
                "duration":totalDuration,
                "start_location":["lat": startLatitude, "lng": startLongitude],
                "end_location":["lat": endLatitude, "lng": endLongitude],
                "steps":steps]],
                              "status":"OK"] as [String : Any]
            
            return googleJSON as NSDictionary
        }
        return nil
    }
    
    func parametersRouteForTmap(from source: CLLocationCoordinate2D, to destination: CLLocationCoordinate2D) -> NSDictionary
    {
        return [
                    TMAP.PARAMETERS.VERSION : TMAP.PARAMETERS.VERSION_1,
                    TMAP.PARAMETERS.STARTX : source.longitude.description,
                    TMAP.PARAMETERS.STARTY : source.latitude.description,
                    TMAP.PARAMETERS.ENDX : destination.longitude.description,
                    TMAP.PARAMETERS.ENDY : destination.latitude.description,
                    TMAP.PARAMETERS.REQ_COORD : TMAP.PARAMETERS.COORD_TYPE,
                    TMAP.PARAMETERS.RES_COORD : TMAP.PARAMETERS.COORD_TYPE,
                ]
    }
    
    func parametersPOIforTmap(keyword: String) -> [String: String]
    {
        return [
                    TMAP.PARAMETERS.VERSION : TMAP.PARAMETERS.VERSION_1,
                    TMAP.PARAMETERS.SEARCH_KEYWORD : keyword,
                    TMAP.PARAMETERS.COUNT : TMAP.PARAMETERS.COUNT_VAL,
                    TMAP.PARAMETERS.PAGE : TMAP.PARAMETERS.PAGE_VAL,
                    TMAP.PARAMETERS.REQ_COORD : TMAP.PARAMETERS.COORD_TYPE,
                    TMAP.PARAMETERS.RES_COORD : TMAP.PARAMETERS.COORD_TYPE
                ]
    }
    
    func getPOIfromTmap(keyword: String)
    {
        let url = TMAP.URL.POI
        
        let manager = AFHTTPSessionManager(baseURL: nil)
        manager.responseSerializer = AFJSONResponseSerializer()
        manager.requestSerializer.setValue(TMAP.APIKEY, forHTTPHeaderField: TMAP.PARAMETERS.APPKEY)
        
        let parameter = parametersPOIforTmap(keyword: keyword)
        
        manager.get(url, parameters: parameter, progress: {
            (progress) in
        },
                    success: { (operation, responseObject) in
                        print("success")
                        if let response = responseObject as? NSDictionary {
                            if let pois = self.convertPOIsFromTmap(responseObject: response) {
                                for delegate in self.delegates {
                                    if (delegate as AnyObject).responds(to: #selector(LocationDelegate.locationManager(_:didFoundPois:))) {
                                        delegate.locationManager!(self.locationMgr, didFoundPois: pois as NSArray)
                                    }
                                }
                            }
                        }
        },
                    failure: { (task, error) in
                        print("failure : \(error.localizedDescription)")
        })
    }
    
    func getAddressFromTmap(position: CLLocationCoordinate2D)
    {
        let url = String(format: "%@?%@=%@&%@=%@&%@=%@&%@=%@&%@=%@", TMAP.URL.GEOCODE, TMAP.PARAMETERS.VERSION, TMAP.PARAMETERS.VERSION_1,
                                                                    TMAP.PARAMETERS.LAT, position.latitude.description,
                                                                    TMAP.PARAMETERS.LON, position.longitude.description,
                                                                    TMAP.PARAMETERS.COORD_TYPE, TMAP.PARAMETERS.MAP_TYPE,
                                                                    TMAP.PARAMETERS.ADDRESS_TYPE, TMAP.PARAMETERS.ADDRESS_A10)
        
        let manager = AFHTTPSessionManager(baseURL: nil)
        manager.responseSerializer = AFJSONResponseSerializer()
        manager.requestSerializer.setValue(TMAP.APIKEY, forHTTPHeaderField: TMAP.PARAMETERS.APPKEY)
        
        manager.get(url, parameters: nil, progress: {
            (progress) in
            },
            success: { (operation, responseObject) in
                print("success")
                if let response = responseObject as? [String: AnyObject] {
                    let address = self.convertToAddressInfo(responseObject: response)
                    // 주소 찾기는 현재 메인 뷰와 목적지 뷰뿐이라 현재 active된 뷰에만 이벤트 전달하기로 한다
                    if address != nil, let delegate = self.delegates.last {
                        if (delegate as AnyObject).responds(to: #selector(LocationDelegate.locationManager(_:didFoundAddress:location:))) {
                            delegate.locationManager(self.locationMgr, didFoundAddress: address?.roadInfo?.address ?? "Invalid Address", location: position)
                        }
                    }
                }
            },
            failure: { (task, error) in
                print("failure : \(error.localizedDescription)")
                })
    }
    
    /// 픽업이나 이동 중에 앱 재시작을 위한 request. handler에서 sourceLabel, destinationLabel을 set 해준다
    ///
    /// - Parameters:
    ///   - position: location
    ///   - handler: 주소를 받아서 처리할 closure
    func getAddressFromTmap(position: CLLocationCoordinate2D, handler: ((String) -> Void)? = nil)
    {
        let url = String(format: "%@?%@=%@&%@=%@&%@=%@&%@=%@&%@=%@", TMAP.URL.GEOCODE, TMAP.PARAMETERS.VERSION, TMAP.PARAMETERS.VERSION_1,
                         TMAP.PARAMETERS.LAT, position.latitude.description,
                         TMAP.PARAMETERS.LON, position.longitude.description,
                         TMAP.PARAMETERS.COORD_TYPE, TMAP.PARAMETERS.MAP_TYPE,
                         TMAP.PARAMETERS.ADDRESS_TYPE, TMAP.PARAMETERS.ADDRESS_A10)
        
        let manager = AFHTTPSessionManager(baseURL: nil)
        manager.responseSerializer = AFJSONResponseSerializer()
        manager.requestSerializer.setValue(TMAP.APIKEY, forHTTPHeaderField: TMAP.PARAMETERS.APPKEY)
        
        manager.get(url, parameters: nil, progress: {
            (progress) in
        },
                    success: { (operation, responseObject) in
                        print("success")
                        if let response = responseObject as? [String: AnyObject] {
                            let address = self.convertToAddressInfo(responseObject: response)
                            if address != nil, handler != nil {
                                handler!(address?.roadInfo?.address ?? "")
                            }
                        }
        },
                    failure: { (task, error) in
                        print("failure : \(error.localizedDescription)")
        })
    }
    
    func getPolyLineRouteFromTmap(from source: CLLocationCoordinate2D, to destination: CLLocationCoordinate2D)
    {
        let url = TMAP.URL.ROUTE
        
        let parameters = parametersRouteForTmap(from: source, to: destination)
        
        let manager = AFHTTPSessionManager(baseURL: nil)
        manager.responseSerializer = AFJSONResponseSerializer()
        manager.requestSerializer.setValue(TMAP.APIKEY, forHTTPHeaderField: TMAP.PARAMETERS.APPKEY)
        
        manager.post(url, parameters: parameters, progress: nil, success: {
            (operation, responseObject) in
            print("success")
            if let response = responseObject as? NSDictionary {
                print("response is \(response)")
                if let user = self.convertToGMSPath(responseObj: response)
                {
                    if let routesArray = user["routes"] as? NSDictionary,
                        let legs = routesArray["legs"] as? NSDictionary,
                        let steps = legs["steps"] as? [NSDictionary]
                    {
                        for step in steps {
                            if let polylines = step["polyline"] as? NSMutableArray {
                                for polyline in polylines {
                                    self.paths.add((polyline as! CLLocation).coordinate)
                                }
                                self.steps.add(step)
                            }
                        }
                        DispatchQueue.main.async {
                            for delegate in self.delegates {
                                if (delegate as AnyObject).responds(to: #selector(LocationDelegate.locationManager(_:didFoundRoutes:_:_:))) {
                                    delegate.locationManager!(self.locationMgr, didFoundRoutes: routesArray, source, destination)
                                }
                            }
                        }
                    }
                }
            }
            }, failure: {
                (task, error) in
            print("failure : \(error.localizedDescription)")
        })
    }
    
    func shouldRemindLocationBeforePickUp() -> Bool
    {
        return remindLocationBeforePickUp
    }
    
    func didRemindLocationBeforePickUp()
    {
        remindLocationBeforePickUp = false
    }
    
    func shouldRequestPickUp() -> Bool
    {
        return isLocationsAllValid() && !needToRequestRoutes
    }
    
    func requestRoutesIfNeeded()
    {
        guard isLocationsAllValid() && needToRequestRoutes else {
            return
        }

        getPolyLineRouteFromTmap(from: sourceLocation, to: destinationLocation)
    }
    
    func isLocationsAllValid() -> Bool
    {
        return !CommonObject.isEqualLocation(first: sourceLocation, second: kCLLocationCoordinate2DInvalid)
            && !CommonObject.isEqualLocation(first: destinationLocation, second: kCLLocationCoordinate2DInvalid)
    }
    
    func setNeedsToRequestRoutes(locationType: ViewType, position: CLLocationCoordinate2D)
    {
        var currentPosition: CLLocation
        
        switch locationType {
        case .from, .confirm_from:
            currentPosition = CLLocation(latitude: getSourceLocation().latitude, longitude: getSourceLocation().longitude)
        case .to:
            currentPosition = CLLocation(latitude: getDestinationLocation().latitude, longitude: getDestinationLocation().longitude)
        }
        
        let newPosition = CLLocation(latitude: position.latitude, longitude: position.longitude)
        let distance = currentPosition.distance(from: newPosition)
        if CommonObject.isEqualLocation(first: currentPosition.coordinate, second: kCLLocationCoordinate2DInvalid) || distance > 10 {
            needToRequestRoutes = true
        } else {
            needToRequestRoutes = false
        }
    }
}



// MARK: CLLocation Manager Delegate
extension LocationManager: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
        
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        guard status == .authorizedWhenInUse else {
            return
        }
        manager.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else {
            return
        }
        
        for delegate in delegates {
            if (delegate as AnyObject).responds(to: #selector(LocationDelegate.locationManager(_:didUpdateLocations:))) {
                delegate.locationManager(manager, didUpdateLocations: locations)
            }
        }
        
        setCurrentLocation(position: location.coordinate)
    }
}

extension GMSMutablePath
{
    func sourcePosition() -> CLLocationCoordinate2D
    {
        return self.coordinate(at: 0)
    }
    
    func destinationPosition() -> CLLocationCoordinate2D
    {
        return self.coordinate(at: self.count() - 1)
    }
}

extension GMSMapView
{
    func routesEdgeInsets() -> UIEdgeInsets
    {
        for view in self.subviews {
            if view.accessibilityIdentifier == "routeBounds" {
                print("\(view.frame)")
                let routeBounds = view.frame
                return UIEdgeInsetsMake(routeBounds.origin.y, routeBounds.origin.x, self.bounds.origin.y + self.bounds.size.height - (routeBounds.origin.y + routeBounds.size.height), self.bounds.origin.x + self.bounds.size.width - (routeBounds.origin.x + routeBounds.size.width))
            }
        }
        
        return UIEdgeInsetsMake(20, 20, 20, 20)
    }
}
