//
//  CallingViewController.swift
//  Taxi
//
//  Created by wslee-chacha on 2018. 7. 30..
//  Copyright © 2018년 chachacreation.co.kr. All rights reserved.
//

import UIKit
import AFNetworking


class CallingViewController: BaseViewController {
    
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var sourceLabel: UILabel!
    @IBOutlet weak var destinationLabel: UILabel!
    @IBOutlet weak var haloView: UIView!
    
    var waitTimer: Timer?
    var pastTime: Int = 0
    var timerLayer: PulsingHaloLayer?
    
    //MARK: Server Requests
    func requestOrderCancel()
    {
        let url = requestMgr?.urlWithMethodId(methodId: API.kOrderCancelByRider)
        
        let commonParameters = requestMgr?.commonParameters()
        let requestParameters = requestMgr?.requestParameters(methodId: API.kOrderCancelByRider)
        let additionalParameters = ["useq" : userModel.getUserSeq(),
                                    "oseq" : userModel.getOseq()]
        
        let parameter = NSMutableDictionary()
        parameter.addEntries(from: commonParameters as! [String : Any])
        parameter.addEntries(from: requestParameters as! [String : Any])
        parameter.addEntries(from: additionalParameters)
        
        let manager = AFHTTPSessionManager(baseURL: nil)
        manager.responseSerializer = AFJSONResponseSerializer()
        
        manager.delete(url!, parameters: parameter, success: {
            (operation, responseObject) in
            FUNCLOG(functionName: #function)
            self.stateMgr?.removeDelegate(delegate: self)
            self.dismiss(animated: true, completion: nil)
        }) {
            (task, error) in
            print("failure : \(error.localizedDescription)")
        }
    }
    
    func requestOrderTimeOut()
    {
        let url = requestMgr?.urlWithMethodId(methodId: API.kOrderTimeOut)
        
        let commonParameters = requestMgr?.commonParameters()
        let requestParameters = requestMgr?.requestParameters(methodId: API.kOrderTimeOut)
        let additionalParameters = ["useq" : userModel.getUserSeq(),
                                    "oseq" : userModel.getOseq()]
        
        let parameter = NSMutableDictionary()
        parameter.addEntries(from: commonParameters as! [String : Any])
        parameter.addEntries(from: requestParameters as! [String : Any])
        parameter.addEntries(from: additionalParameters)
        
        let manager = AFHTTPSessionManager(baseURL: nil)
        manager.responseSerializer = AFJSONResponseSerializer()
        
        manager.delete(url!, parameters: parameter, success: {
            (operation, responseObject) in
            FUNCLOG(functionName: #function)
            self.stateMgr?.removeDelegate(delegate: self)
            self.dismiss(animated: true, completion: nil)
        }) {
            (task, error) in
            print("failure : \(error.localizedDescription)")
        }
    }
    
    //MARK: Instance Methods
    @objc func excuteTimer()
    {
        if pastTime >= 40 {
            stopTimer()
            requestOrderTimeOut()
        } else {
            pastTime = pastTime + 1
            timeLabel.text = String.init(format: "%d", 40 - pastTime)
        }
    }
    
    func startTimer()
    {
        waitTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(excuteTimer), userInfo: nil, repeats: true)
        
        timerLayer = PulsingHaloLayer.init()
        timerLayer?.position = haloView.center
        timerLayer?.haloLayerNumber = 3
        timerLayer?.radius = haloView.frame.width / 2
        timerLayer?.animationDuration = 2.5
        timerLayer?.useTimingFunction = false
        timerLayer?.fromValueForRadius = 0.1
        timerLayer?.keyTimeForHalfOpacity = 0.2
        timerLayer?.backgroundColor = CommonObject.colorByHex(rgb: Color.ChaCha).cgColor
        haloView.layer.addSublayer(timerLayer!)
        timerLayer?.start()
        
        timeLabel.text = String.init(format: "%d", 40 - pastTime)
    }
    
    func stopTimer()
    {
        if waitTimer != nil {
            waitTimer?.invalidate()
            waitTimer = nil
        }
        pastTime = 0
    }
    
    override func didPickUpStart()
    {
        dismiss(animated: true, completion: {
            self.stopTimer()
            self.stateMgr?.removeDelegate(delegate: self)
        })
        print("go to pick up view")
    }
    
    override func didPickUpFail()
    {
        dismiss(animated: true, completion: {
            self.stopTimer()
            self.stateMgr?.removeDelegate(delegate: self)
        })
        print("back to order preview view")
    }
    
    //MARK: Action
    @objc func buttonSelected(button: UIButton)
    {
        if button.isEqual(cancelBtn) {
            let alertController = UIAlertController.init(title: "", message: "정말 취소하시겠습니까?", preferredStyle: .alert)
            let okAction = UIAlertAction.init(title: "확인", style: .default) {
                [weak self] (action) in
                self?.requestOrderCancel()
            }
            let cancelAction = UIAlertAction.init(title: "취소", style: .cancel) {
                (action) in
            }
            alertController.addAction(okAction)
            alertController.addAction(cancelAction)
            present(alertController, animated: true, completion: nil)
        }
    }

    //MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        cancelBtn.addTarget(self, action: #selector(buttonSelected(button:)), for: .touchUpInside)
        CommonObject.setBorderLine(view: cancelBtn)
        CommonObject.setCornerRadius(view: cancelBtn, 3)
        startTimer()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
