//
//  CancelViewController.swift
//  Taxi
//
//  Created by wslee-chacha on 2018. 8. 7..
//  Copyright © 2018년 chachacreation.co.kr. All rights reserved.
//

import UIKit
import AFNetworking

@objc protocol CancelViewDelegate {
    func reOrder()
    func orderLater()
}

class CancelViewController: BaseViewController {
    
    @IBOutlet weak var imgCancel: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var cancelConfirmBtn: UIButton!
    @IBOutlet weak var orderLaterBtn: UIButton!
    
    private var cancelType: DriverState = .DRIVER_DO_NOT
    var delegate: CancelViewDelegate?
    
    //MARK: Server Request
    func requestCancel()
    {
        guard cancelType != .DRIVER_DO_NOT else {
            print("cancelType is Not Set!!!")
            return
        }
        
        var methodId = ""
        if cancelType == .RIDER_CANCEL_ON_ORDER {
            methodId = API.kOrderCancelByRider
        } else if cancelType == .RIDER_CANCEL_ON_START {
            methodId = API.kPickupCancelByRider
        }
        let url = requestMgr?.urlWithMethodId(methodId: methodId)
        
        let commonParameters = requestMgr?.commonParameters()
        let requestParameters = requestMgr?.requestParameters(methodId: methodId)
        let additionalParameters = ["useq" : UserModel.getUser().getUserSeq(),
                                    "oseq" : UserModel.getUser().getOseq()]
        
        let parameter = NSMutableDictionary()
        parameter.addEntries(from: commonParameters as! [String : Any])
        parameter.addEntries(from: requestParameters as! [String : Any])
        parameter.addEntries(from: additionalParameters)
        
        let manager = AFHTTPSessionManager(baseURL: nil)
        manager.responseSerializer = AFJSONResponseSerializer()
        
        manager.delete(url!, parameters: parameter, success: {
            [weak self] (operation, responseObject) in
            let response = ResponseObject(responseObject: responseObject!)
            if response.result == 1 {
                self?.stateMgr?.setCancelType(self?.cancelType ?? .DRIVER_DO_NOT)
                UserModel.setUser(UserModel.init(userInfo: response.response!))
                self?.navigationController?.popViewController(animated: true)
            } else {
                self?.onServerError(response: response)
            }
        }) { (task, error) in
            print("failure : \(error.localizedDescription)")
        }
    }
    
    //MARK: Instance Methods
    convenience init(with type: DriverState) {
        self.init(nibName: String.init(describing: CancelViewController.self), bundle: nil)
        cancelType = type
    }
    
    func setControls()
    {
        switch cancelType
        {
            case .RIDER_CANCEL_ON_ORDER, .RIDER_CANCEL_ON_START:
                imgCancel.image = UIImage.init(named: "img_cnaceltrip")
                titleLabel.text = "정말 취소하시겠습니까?"
                detailLabel.text = "정말 취소할까요? 매칭 후 5분이 지나면\n수수료가 부과됩니다."
                orderLaterBtn.isHidden = true
                cancelConfirmBtn.setTitle("네, 취소합니다", for: .normal)
                cancelConfirmBtn.setTitle("네, 취소합니다", for: .highlighted)
                cancelConfirmBtn.setTitle("네, 취소합니다", for: .selected)
        case .DRIVER_CANCEL_ON_ORDER, .DRIVER_CANCEL_ON_BEGIN, .DRIVER_CANCEL_ON_START, .DRIVER_CANCEL_ON_ARRIVE:
                imgCancel.image = UIImage.init(named: "img_sorry")
                titleLabel.text = "죄송합니다. 여정이 취소되었습니다."
                detailLabel.text = "운전자의 사정 혹은 시스템 문제로 취소되었습니다."
                orderLaterBtn.isHidden = false
                cancelConfirmBtn.setTitle("다시 주문해 주세요", for: .normal)
                cancelConfirmBtn.setTitle("다시 주문해 주세요", for: .highlighted)
                cancelConfirmBtn.setTitle("다시 주문해 주세요", for: .selected)
            
            default:
                print("\(cancelType)")
        }
        cancelConfirmBtn.addTarget(self, action: #selector(buttonSelected(button:)), for: .touchUpInside)
        orderLaterBtn.addTarget(self, action: #selector(buttonSelected(button:)), for: .touchUpInside)
        backBtn?.addTarget(self, action: #selector(buttonSelected(button:)), for: .touchUpInside)
    }
    
    @objc func buttonSelected(button: UIButton)
    {
        if button.isEqual(cancelConfirmBtn) {
            if cancelType != .DRIVER_DO_NOT {
                delegate?.reOrder()
            } else {
                requestCancel()
            }
        } else if button.isEqual(orderLaterBtn) {
            delegate?.orderLater()
        } else if button.isEqual(backBtn) {
            if cancelType != .DRIVER_DO_NOT {
                delegate?.orderLater()
            }
            navigationController?.popViewController(animated: true)
        }
    }

    //MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setControls()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
