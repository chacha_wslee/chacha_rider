#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import "TXBaseViewController.h"

@protocol TXDestinationsVCDelegate <NSObject>
@required
- (void) didFoundAddress:(NSString *) address withLatitute:(double)lat andLongtitute:(double)longi forTarget:(id)target;

@end

@interface TXLocationVC : TXBaseViewController <UITextFieldDelegate,
CLLocationManagerDelegate, GMSMapViewDelegate,
UITableViewDataSource,UITableViewDelegate>

@property (nonatomic, strong)  GMSMapView *mapView_;
@property (nonatomic, strong) id _target;
@property (nonatomic, assign) NSInteger _step;
@property (nonatomic, assign) CLLocationCoordinate2D destPosition;
@property (nonatomic, assign) id<TXDestinationsVCDelegate> delegate;

@property (nonatomic, strong) IBOutlet UIView *addrView;
@property (nonatomic, strong) IBOutlet UIView *addrBGView;
@property (nonatomic, strong) IBOutlet UITextField *searchField;
@property (nonatomic, strong) IBOutlet UIButton *btnClear;

@property (nonatomic, strong) IBOutlet UIView *confirmView;
@property (nonatomic, strong) IBOutlet UIView *addrTextView;
@property (nonatomic, strong) IBOutlet UILabel *lbAddress;

@property (nonatomic, strong) IBOutlet UIButton *btnConfirm;

@end
