#if !defined(__has_feature) || !__has_feature(objc_arc)
#error "This file requires ARC support."
#endif

#import "TXDestinationsVC.h"
#import <GooglePlaces/GooglePlaces.h>

#import "PlaceTableCell.h"
#import "LGAlertView.h"

static float   kOverlayMovingTime = 0.5f;

// This demo shows how to manually present a UITableViewController and supply it with autocomplete
// text from an arbitrary source, in this case a UITextField.
//GMSAutocompleteTableDataSourceDelegate,
@interface TXDestinationsVC () <UIGestureRecognizerDelegate> //<CLocationDelegate>

@property (nonatomic, strong) NSMutableArray *matchedAddress;
@property (nonatomic, strong) NSArray *historyAddress;

@end

@implementation TXDestinationsVC {
    
    UITableViewController *_resultsController;
    //GMSAutocompleteTableDataSource *_tableDataSource;
    //UITextView *_resultView;

    //GMSPlacesClient *_placesClient;

    NSString *_address;
    CLLocationCoordinate2D _coor;
    
    //UITableView *mTableViewSuggest;
    UIView *background;
    
    BOOL _firstLocationUpdate;
    //GMSMapView *_mapView;
    GMSMarker *_placeMarker;
    
    UIImageView *mImageFocus;
    
    CGFloat cellHeight;
    
    BOOL isMoveMapBYGesture;
    
    NSInteger cstep_;
    
    UITextField *_tfedit;
    NSDictionary *items;
    //    LGAlertView *alertView;
    
    BOOL isKeywordShow;
    
    // rowcount type
    NSInteger rowType;
}

@synthesize _target, _step;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor yellowColor];
    
    BOOL isMoveLocation = YES;
    CLLocationCoordinate2D coor = appDelegate.location.coordinate;
    if ([GoogleUtils isLocationValid:self.destPosition]) {
        coor = self.destPosition;
    }

    //[GMSPlacesClient provideAPIKey:_GOOGLE_PLACE_API_KEY];
    //https://console.developers.google.com/flows/enableapi?apiid=placesios,maps_ios_backend&keyType=CLIENT_SIDE_IOS
    //_placesClient = [GMSPlacesClient sharedClient];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    UIPinchGestureRecognizer *pinch = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(pinchGestureHandler:)];
    pinch.delegate = self;
    [self.mapView_ addGestureRecognizer:pinch];
    
    self.matchedAddress = [[NSMutableArray alloc] init];
    //self.historyAddress = nil;
    self.historyAddress = (NSMutableArray*)PLACE_SELECT;
    rowType = 1;
    
    _placeMarker = [[GMSMarker alloc] init];
    
    self->cellHeight = 62;
    
    PLACE_DELETE_HISTORY_ALL;
    PLACE_DELETE_NULL;
    PLACE_DELETE_LIMIT;
    
    // 주소입력
    if (_step == 110 || _step == 210) {
        UITextField *tf = (UITextField*)_target;
        if (![tf.text isEqualToString:@""]) {
            _searchField.text = tf.text;
            [self textFieldDidChange:_searchField];
            //isMoveLocation = NO;
        }
        else {
            [self onClearButtonClick:nil];
        }
    }
    else if (_step == 310) {
        // add favorite
        if (_target != nil) {
            items = (NSDictionary*)_target;
        }
    }
    else if (_step == 11) {
        // modify
        if (_target != nil) {
            items = (NSDictionary*)_target;
            [self getAddressFromFavorite:items[@"location"]];
            coor = [Utils getLocation:items[@"location"]];
            //isMoveLocation = NO;
        }
    }
    else if (_step == 21) {
        // modify
        if (_target != nil) {
            items = (NSDictionary*)_target;
            if (([items[@"place"] isEqualToString:@""] || items == nil ) && [GoogleUtils isLocationValid:self.destPosition] ) {
                //
            }
            else {
                [self getAddressFromFavorite:items[@"location"]];
                coor = [Utils getLocation:items[@"location"]];
                //isMoveLocation = NO;
            }
        }
    }
    else if (_step == 420 ) {
        UITextField *tf = (UITextField*)_target;
        if (![tf.text isEqualToString:@""]) {
            _searchField.text = tf.text;
            _lbAddress.text = tf.text;
        }
        //[self getAddressFromLatLon:self.destPosition];
//        isMoveLocation = NO;
    }
/*
    if (isMoveLocation) {
        NSLog(@"cameramove:%f,%f",coor.latitude,coor.longitude);
        GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:coor.latitude longitude:coor.longitude zoom:17];
        [self.mapView_ animateToCameraPosition:camera];
        NSLog(@"cameramove:%f,%f",self.mapView_.camera.target.latitude,self.mapView_.camera.target.longitude);
    }
*/
}

-(void)initMapView {
    
    GMSCameraPosition *camera;
    
    if ([GoogleUtils isLocationValid:_destPosition]) {
        camera = [GMSCameraPosition cameraWithLatitude:_destPosition.latitude
                                             longitude:_destPosition.longitude
                                                  zoom:17];
    }
    else {
        camera = [GMSCameraPosition cameraWithLatitude:appDelegate.location.coordinate.latitude
                                             longitude:appDelegate.location.coordinate.longitude
                                                  zoom:14];
    }

    NSLog(@"cameramove:%f,%f",camera.target.latitude,camera.target.longitude);
    self.mapView_ = [GMSMapView mapWithFrame:CGRectMake(0, kTopHeight, self.view.bounds.size.width,
                                                        self.view.bounds.size.height) camera:camera];
    self.mapView_.delegate = self;
    NSLog(@"cameramove:%f,%f",self.mapView_.camera.target.latitude,self.mapView_.camera.target.longitude);
    
    self.mapView_.autoresizingMask = UIViewAutoresizingFlexibleWidth |
    UIViewAutoresizingFlexibleHeight |
    UIViewAutoresizingFlexibleBottomMargin;
    
    /*
     // Listen to the myLocation property of GMSMapView.
     [self.mapView_ addObserver:self
     forKeyPath:@"myLocation"
     options:NSKeyValueObservingOptionNew
     context:NULL];
     */
    // Ask for My Location data after the map has already been added to the UI.
    
    self.mapView_.settings.rotateGestures = NO;
    self.mapView_.settings.compassButton = NO;
    self.mapView_.settings.myLocationButton = NO;
    self.mapView_.settings.indoorPicker = NO;
    self.mapView_.settings.scrollGestures = YES;
    self.mapView_.settings.allowScrollGesturesDuringRotateOrZoom = NO;
    
    [self showCposButton];
    if (![GoogleUtils isLocationValid:_destPosition]) {
        [self pushCpos];
    }
    
    [self.view addSubview:self.mapView_];
}
/*
 - (void)dealloc {
 [self.mapView_ removeObserver:self
 forKeyPath:@"myLocation"
 context:NULL];
 }
 */
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self addObservers];
    
    isMoveMapBYGesture = NO;
    
    if ([INTULocationManager locationServicesState]) {
        [appDelegate onCommonErrorAlert:LocalizedStr(@"Intro.GPS.Error.text")];
    }
    
    CGRect rect = _btnConfirm.frame;
    rect.origin.x = 10;
    rect.origin.y = rect.origin.y - 10;
    rect.size.width = rect.size.width - rect.origin.x*2;
    _btnConfirm.frame = rect;
    
    _btnConfirm.layer.cornerRadius = 3;
    _btnConfirm.clipsToBounds = YES;
    _btnConfirm.layer.masksToBounds = YES;
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self removeObservers];
    // Implement here if the view has registered KVO
}

#pragma mark -
#pragma mark UIGestureRecognizerDelegate
#pragma mark -

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer{
    //default is no - if we dont set it to on then gesture is captured by GMSMapView and myGestureHandler never called
    return YES;
}

#pragma mark -
#pragma mark UIPanGestureRecognizer handler
#pragma mark -

-(void)pinchGestureHandler:(UIPanGestureRecognizer *)recognizer {
    
    if([recognizer state] == UIGestureRecognizerStatePossible)
    {
    }
    else if([recognizer state] == UIGestureRecognizerStateBegan)
    {
    }
    else if([recognizer state] == UIGestureRecognizerStateChanged)
    {
    }
    else if([recognizer state] == UIGestureRecognizerStateEnded)
    {
    }
    else if([recognizer state] == UIGestureRecognizerStateCancelled)
    {
    }
    else if([recognizer state] == UIGestureRecognizerStateFailed)
    {
    }
    else{
        NSLog(@"ERROR:<%@ %@:(%d)> %s UNHANDLED STATE", self, [[NSString stringWithUTF8String:__FILE__] lastPathComponent], __LINE__, __PRETTY_FUNCTION__);
    }
    //-----------------------------------------------------------------------------------
    
}

#pragma mark KVO
-(void)addObservers {
    [self.mapView_ addObserver:self forKeyPath:@"myLocation" options:NSKeyValueObservingOptionNew context: nil];
    
    // location delegate
    //appDelegate.locationDelegate = self;
}

-(void)removeObservers {
    [self.mapView_ removeObserver:self forKeyPath:@"myLocation"];
}

-(void)configure {
    
    [super configure];
    
    [self initMapView];
    
    cstep_ = _step;
    CGRect rect = self.mapView_.frame;
//    rect.origin.y = 20;
//    rect.size.width = self.view.bounds.size.width;
//    rect.size.height = self.view.bounds.size.height - rect.origin.y;
//    self.mapView_.frame = rect;
    
    [super configureBottomLine];
    UIView  *view = (UIView*)[[super navigationView] viewWithTag:1500];
    view.hidden = YES;
    UIView *n = [super navigationView];
    NSInteger _y = n.frame.origin.y + n.frame.size.height;
    
    UIView  *topview = (UIView*)[[super navigationView] viewWithTag:60];
    UILabel *toplabel = (UILabel*)[[super navigationView] viewWithTag:1210];
    
    _btnMap = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width - 40 - 10, toplabel.frame.origin.y, 40, 40)];
    [_btnMap setImage:[UIImage imageNamed:@"icon_setlocation"] forState:UIControlStateNormal];
    [_btnMap addTarget:self action:@selector(onMapViewButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [topview addSubview:_btnMap];
    [topview bringSubviewToFront:_btnMap];
    // map 이미지 만큼 줄인다
    rect = toplabel.frame;
    rect.size.width -= 50;
    toplabel.frame = rect;
    
    //    UIView* statev = [n viewWithTag:50];
    //    [[super navigationView] bringSubviewToFront:statev];
    
    // 1. init searchbar
    if (cstep_ != 21) {
        [self configureSearchBar:_y];
    }
    
    //
    if (_step == 120 || _step == 220 || _step == 21 || _step == 420 ) {
        [self changeStepView];
    }
    
    //_y -= 4;
    // 4. init mapview & layout
    _y = [self configureMapView:_y];
    
    _y = self.view.bounds.size.height - _confirmView.frame.size.height - kBottomBottonSafeArea;
    _y = [self configureConfirmView:_y];
    
    [self.view bringSubviewToFront:n];
}

-(void)configureStylesAddrBG {
#ifdef _WITZM
    _addrBGView.backgroundColor = UIColorDefault;
    _searchField.textColor = UIColorButtonText;
#elif defined _CHACHA
    _addrBGView.backgroundColor = [UIColor whiteColor];
    _addrBGView.layer.borderColor = [UIColor blackColor].CGColor;
    _addrBGView.layer.borderWidth = 1.0;
    _searchField.textColor = UIColorDefault2;
#endif
}

-(void)configureStylesNavigation:(UIView*)n label:(UILabel*)lb {
#ifdef _WITZM
    [n setBackgroundColor:UIColorDefault];
    lb.textColor = UIColorDefault2;
#elif defined _CHACHA
    [n setBackgroundColor:[UIColor whiteColor]];
    lb.textColor = UIColorDefault2;
#endif
}

-(void)configureStyles {
    [super configureStyles];
    
    UIView *n = [super navigationView];
    UILabel *lb = [n viewWithTag:1210];
    UIView *v = [[super navigationView] viewWithTag:1220];
    _tfedit = [v viewWithTag:10];
    
    _lbAddress.text = @"";
    _searchField.font = [UIFont fontWithName:@"HelveticaNeue-Regular" size:14];
    // 출발지
    if (cstep_ == 110) {
        [n setBackgroundColor:[UIColor whiteColor]];
        lb.textColor = UIColorDefault2;
        [super navigationType01X:[UIImage imageNamed:@"btn_back"] centerText:LocalizedStr(@"Map.Address.SRC.Select.title")];
        
        _addrView.hidden = NO;
        _confirmView.hidden = YES;
        v.hidden = YES;
        [self configureStylesAddrBG];

        [_btnClear setImage:[UIImage imageNamed:@"btn_delete_small"] forState:UIControlStateNormal];
        
        [_searchField becomeFirstResponder];
    }
    // 출발지 지도
    else if (cstep_ == 120) {
        [self configureStylesNavigation:n label:lb];
#ifdef _WITZM
        [super navigationType01X:[UIImage imageNamed:@"btn_back"] centerText:LocalizedStr(@"Map.Address.SRC.Select.title")];
#elif defined _CHACHA
        [super navigationType01X:[UIImage imageNamed:@"btn_back"] centerText:LocalizedStr(@"Map.Address.SRC.Select.title")];
#endif
        
        _addrView.hidden = YES;
        _confirmView.hidden = NO;
        v.hidden = YES;
        
        [self configureStylesAddrBG];

        [_btnClear setImage:[UIImage imageNamed:@"btn_delete_small"] forState:UIControlStateNormal];
        
        //_btnCpos.hidden = YES;
        _lbAddress.text = LocalizedStr(@"String.Address.Searching.text");
    }
    // 출발지 확인
    else if (cstep_ == 420) {
        [self configureStylesNavigation:n label:lb];
#ifdef _WITZM
        [super navigationType01X:[UIImage imageNamed:@"btn_back"] centerText:LocalizedStr(@"Map.Address.SRC.Confirm.title")];
#elif defined _CHACHA
        [super navigationType01X:[UIImage imageNamed:@"btn_back"] centerText:LocalizedStr(@"Map.Address.SRC.Confirm.title")];
#endif
        
        _addrView.hidden = YES;
        _confirmView.hidden = NO;
        v.hidden = YES;
        
        [self configureStylesAddrBG];

        [_btnClear setImage:[UIImage imageNamed:@"btn_delete_small"] forState:UIControlStateNormal];
        
        //_btnCpos.hidden = YES;
        _btnCancel.hidden = YES;
        _lbAddress.text = LocalizedStr(@"String.Address.Searching.text");
    }
    // 목적지
    else if (cstep_ == 210) {
        [n setBackgroundColor:[UIColor whiteColor]];
        lb.textColor = UIColorDefault2;
        if (appDelegate.isDriverMode) {
            if (appDelegate.pstate == 240) {
                [super navigationType01X:[UIImage imageNamed:@"btn_back"] centerText:LocalizedStr(@"Map.Address.DST.Select.title")];
            }
            else {
                [super navigationType01X:[UIImage imageNamed:@"btn_back"] centerText:LocalizedStr(@"Map.Address.DST.Driver.Select.title")];
            }
        }
        else {
            [super navigationType01X:[UIImage imageNamed:@"btn_back"] centerText:LocalizedStr(@"Map.Address.DST.Select.title")];
        }
        
        _addrView.hidden = NO;
        _confirmView.hidden = YES;
        v.hidden = YES;
        
        [self configureStylesAddrBG];
        
        //_searchField.text = @"";
#ifdef _WITZM
        [_btnClear setImage:[UIImage imageNamed:@"btn_delete_small_color01"] forState:UIControlStateNormal];
#elif defined _CHACHA
        [_btnClear setImage:[UIImage imageNamed:@"btn_delete_small"] forState:UIControlStateNormal];
#endif
        
        [_searchField becomeFirstResponder];
    }
    // 목적지 지도
    else if (cstep_ == 220) {
        [self configureStylesNavigation:n label:lb];
        
        if (appDelegate.isDriverMode) {
            if (appDelegate.pstate == 240) {
#ifdef _WITZM
                [super navigationType01X:[UIImage imageNamed:@"btn_back_02"] centerText:LocalizedStr(@"Map.Address.DST.Select.title")];
#elif defined _CHACHA
                [super navigationType01X:[UIImage imageNamed:@"btn_back"] centerText:LocalizedStr(@"Map.Address.DST.Select.title")];
#endif
            }
            else {
#ifdef _WITZM
                [super navigationType01X:[UIImage imageNamed:@"btn_back_02"] centerText:LocalizedStr(@"Map.Address.DST.Driver.Select.title")];
#elif defined _CHACHA
                [super navigationType01X:[UIImage imageNamed:@"btn_back"] centerText:LocalizedStr(@"Map.Address.DST.Driver.Select.title")];
#endif
                
            }
        }
        else {
#ifdef _WITZM
            [super navigationType01X:[UIImage imageNamed:@"btn_back_02"] centerText:LocalizedStr(@"Map.Address.DST.Select.title")];
#elif defined _CHACHA
            [super navigationType01X:[UIImage imageNamed:@"btn_back"] centerText:LocalizedStr(@"Map.Address.DST.Select.title")];
#endif
        }
        
        _addrView.hidden = YES;
        _confirmView.hidden = NO;
        v.hidden = YES;
        
        [self configureStylesAddrBG];
        
#ifdef _WITZM
        [_btnClear setImage:[UIImage imageNamed:@"btn_delete_small_color01"] forState:UIControlStateNormal];
#elif defined _CHACHA
        [_btnClear setImage:[UIImage imageNamed:@"btn_delete_small"] forState:UIControlStateNormal];
#endif
        
        //_btnCpos.hidden = YES;
        _lbAddress.text = LocalizedStr(@"String.Address.Searching.text");
    }
    // add favorite(빈 주소를 눌러서 들어오면 주소 설정을 할수 있게 이동한다)
    else if (cstep_ == 310) {
        [n setBackgroundColor:[UIColor whiteColor]];
        lb.textColor = UIColorDefault2;
        [super navigationType01X:[UIImage imageNamed:@"btn_back"] centerText:LocalizedStr(@"Map.Search.Address.FAVORITE.text")];
        
        _addrView.hidden = NO;
        _confirmView.hidden = YES;
        v.hidden = YES;
        
        [self configureStylesAddrBG];
        
        //_searchField.text = @"";
#ifdef _WITZM
        [_btnClear setImage:[UIImage imageNamed:@"btn_delete_small_color01"] forState:UIControlStateNormal];
#elif defined _CHACHA
        [_btnClear setImage:[UIImage imageNamed:@"btn_delete_small"] forState:UIControlStateNormal];
#endif
        
        [_searchField becomeFirstResponder];
    }
    else if (cstep_ == 111) {
        [n setBackgroundColor:[UIColor whiteColor]];
        lb.textColor = UIColorDefault2;
        [super navigationType01X:[UIImage imageNamed:@"btn_back"] centerText:@""];
        
        _addrView.hidden = NO;
        _confirmView.hidden = YES;
        v.hidden = YES;
        
        [self configureStylesAddrBG];

#ifdef _WITZM
        [_btnClear setImage:[UIImage imageNamed:@"btn_delete_small_color01"] forState:UIControlStateNormal];
#elif defined _CHACHA
        [_btnClear setImage:[UIImage imageNamed:@"btn_delete_small"] forState:UIControlStateNormal];
#endif
        
        [_searchField becomeFirstResponder];
    }
    else if (cstep_ == 121) {
        [self configureStylesNavigation:n label:lb];

        [super navigationType01X:[UIImage imageNamed:@"btn_back"] centerText:@""];
        
        _addrView.hidden = YES;
        _confirmView.hidden = NO;
        v.hidden = YES;
        
        [self configureStylesAddrBG];

        [_btnClear setImage:[UIImage imageNamed:@"btn_delete_small"] forState:UIControlStateNormal];
        
        //_btnCancel.hidden = YES;
    }
    // add place(즐겨찾기 추가 버튼)
    else if (cstep_ == 11) {
        [n setBackgroundColor:[UIColor whiteColor]];
        lb.textColor = UIColorDefault2;
        if ([items[@"place"] isEqualToString:@""] || items == nil) {
            [super navigationType01X:[UIImage imageNamed:@"btn_back"] centerText:LocalizedStr(@"String.Address.Add.text")];
        }
        else {
            [super navigationType01X:[UIImage imageNamed:@"btn_back"] centerText:items[@"place"]];
        }
        
        _addrView.hidden = NO;
        _confirmView.hidden = YES;
        v.hidden = YES;
        
        [self configureStylesAddrBG];

        if (![self.matchedAddress count]) {
            //_searchField.text = @"";
        }

#ifdef _WITZM
        [_btnClear setImage:[UIImage imageNamed:@"btn_delete_small_color01"] forState:UIControlStateNormal];
#elif defined _CHACHA
        [_btnClear setImage:[UIImage imageNamed:@"btn_delete_small"] forState:UIControlStateNormal];
#endif
        
        [_searchField becomeFirstResponder];
    }
    // 즐겨찾기 리스트(위치관리) 편집 버튼 21, 추가 (310->)31
    else if (cstep_ == 21 || cstep_ == 31) {
        [self configureStylesNavigation:n label:lb];
#ifdef _WITZM
        [super navigationType01X:[UIImage imageNamed:@"btn_back_02"] centerText:@""];
#elif defined _CHACHA
        [super navigationType01X:[UIImage imageNamed:@"btn_back"] centerText:@""];
#endif
        
        //UILabel *lb = [[super navigationView] viewWithTag:1210];
        
        // 해당 정보에 주소가 저장되어 있지 않으면
        if ([items[@"place"] isEqualToString:@""]) {
            NSInteger selectType = [items[@"type"] intValue];
            if (selectType == 1) {
                _tfedit.text = LocalizedStr(@"Map.Search.Address.HOME.text");
            }
            else if (selectType == 2) {
                _tfedit.text = LocalizedStr(@"Map.Search.Address.WORK.text");
            }
            else {
                _tfedit.text = LocalizedStr(@"String.Address.Default.text");
            }
            //self.mapView_.userInteractionEnabled = YES;
            //_btnCpos.hidden = YES;
        }
        else {
            _tfedit.text = items[@"place"];
            // 지도 움직이지 않는다.
            //self.mapView_.userInteractionEnabled = NO;
            //_btnCpos.hidden = YES; // 현재위치는 막는다
        }
        
        lb.hidden = YES;
        v.hidden = NO;
        
        _addrView.hidden = YES;
        _confirmView.hidden = NO;
        
        [self configureStylesAddrBG];

        [_btnClear setImage:[UIImage imageNamed:@"btn_delete_small"] forState:UIControlStateNormal];
        
        _btnCpos.hidden = NO;
        //_btnCancel.hidden = YES;
        
        [_tfedit becomeFirstResponder];
    }
    
    _btnConfirm.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:18];
    [_btnConfirm setTitle:LocalizedStr(@"Map.Search.Address.BTN.confirm.text") forState:UIControlStateNormal];
    [_btnConfirm setTitleColor:UIColorButtonText forState:UIControlStateNormal];
}

-(NSInteger) configureSearchBar:(NSInteger)_y {
    
    background=[[UIView alloc] initWithFrame: CGRectMake ( 0,
                                                          _y - _addrView.frame.size.height,
                                                          self.view.bounds.size.width,
                                                          self.view.bounds.size.height - _y + _addrView.frame.size.height)];
    background.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:background];
    
    _addrView.frame = CGRectMake(0,
                                 _y,
                                 self.view.bounds.size.width,
                                 _addrView.frame.size.height);
    
    _searchField.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    //    _searchField.borderStyle = UITextBorderStyleNone;
    _searchField.adjustsFontSizeToFitWidth = YES;
    _searchField.placeholder = LocalizedStr(@"Map.Address.placeholder.text");
    _searchField.autocorrectionType = UITextAutocorrectionTypeNo;
    _searchField.keyboardType = UIKeyboardTypeDefault;
    _searchField.returnKeyType = UIReturnKeyDone;
    _searchField.clearButtonMode = UITextFieldViewModeNever;
    _searchField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    
    [_searchField addTarget:self
                     action:@selector(textFieldDidChange:)
           forControlEvents:UIControlEventEditingChanged];
    _searchField.delegate = self;
    
    [self.view addSubview:_addrView];
    
    
    // search result tableview
    _resultsController = [[UITableViewController alloc] initWithStyle:UITableViewStylePlain];
    _resultsController.tableView.delegate = self;
    _resultsController.tableView.dataSource = self;
    _resultsController.tableView.backgroundColor = [UIColor clearColor];
    _resultsController.tableView.separatorColor = [UIColor clearColor];
    
    return (background.frame.origin.y);
}

-(NSInteger) configureConfirmView:(NSInteger)_y {
    
    _confirmView.frame = CGRectMake(0,
                                    _y,
                                    self.view.bounds.size.width,
                                    _confirmView.frame.size.height);
    
    CAGradientLayer *layer = [CAGradientLayer layer];
    layer.frame = _addrTextView.bounds;
    UIColor *theColor = [UIColor whiteColor];
    layer.colors = [NSArray arrayWithObjects:
                    (id)[[UIColor clearColor] CGColor],
                    (id)[[UIColor clearColor] CGColor],
                    (id)[[theColor colorWithAlphaComponent:0.0f] CGColor],
                    (id)[[theColor colorWithAlphaComponent:0.1f] CGColor],
                    (id)[[theColor colorWithAlphaComponent:0.3f] CGColor],
                    (id)[[theColor colorWithAlphaComponent:0.5f] CGColor],
                    (id)[[theColor colorWithAlphaComponent:0.6f] CGColor],
                    (id)[[theColor colorWithAlphaComponent:0.9f] CGColor],
                    (id)[theColor CGColor],
                    nil];
    
    [_addrTextView.layer insertSublayer:layer atIndex:0];
    _confirmView.hidden = YES;
    
    [self.view addSubview:_confirmView];
    
    return (_confirmView.frame.origin.y);
}

-(NSInteger) configureMapView:(NSInteger)_y {
    
    CGRect rect = self.mapView_.frame;
    //    rect.origin.y = _y;
    //    rect.size.width = self.view.bounds.size.width;
    //    rect.size.height= self.view.bounds.size.height - _y;
    
    // 1.MAP view
    [self.mapView_ setFrame:rect];
    
    CGPoint point = [self getCenterPointMapView:rect];
    rect = CGRectMake(point.x, point.y, kMarkerSizeW,kMarkerSizeH);
    
    mImageFocus = [[UIImageView alloc] initWithFrame:rect];
    mImageFocus.contentMode = UIViewContentModeScaleAspectFit;
    //    mImageFocus.image = [Utils image:[UIImage imageNamed:@"pinHome"] scaledToSize:CGSizeMake(50.0f, 50.0f)];
    
    UIButton *btn = (UIButton*)_target;
    
    // 즐겨찾기 목적지
    if (_target == nil) {
        mImageFocus.image = [Utils image:[UIImage imageNamed:@"icon_pin_start"] scaledToSize:CGSizeMake(kMarkerSizeW, kMarkerSizeH)];
    }
    else if ([_target isKindOfClass:[NSDictionary class]]) {
        mImageFocus.image = [Utils image:[UIImage imageNamed:@"icon_pin_start"] scaledToSize:CGSizeMake(kMarkerSizeW, kMarkerSizeH)];
    }
    // 출발지
    else if (btn.tag == 101 || btn.tag == 110) {
        mImageFocus.image = [Utils image:[UIImage imageNamed:@"icon_pin_end"] scaledToSize:CGSizeMake(kMarkerSizeW, kMarkerSizeH)];
    }
    else {
        mImageFocus.image = [Utils image:[UIImage imageNamed:@"icon_pin_start"] scaledToSize:CGSizeMake(kMarkerSizeW, kMarkerSizeH)];
    }
    [self.mapView_ addSubview:mImageFocus];
    
    [self markerWillMove];
    
    return (self.mapView_.frame.origin.y + self.mapView_.frame.size.height);
}

- (void)changeStepView {
    // 검색화면
    if (cstep_ == 110 || cstep_ == 210 || cstep_ == 11) {
        background.hidden = NO;
        [self textFieldDidChange:_searchField];
        _btnMap.hidden = NO;
    }
    // 지도화면
    else {
        _btnMap.hidden = YES;
        _addrView.hidden = YES;
        [_searchField resignFirstResponder];
        background.hidden = YES;
        
        self.historyAddress = nil;
        //[self.matchedAddress removeAllObjects];
    }
    
    [self configureStyles];
}

-(CGPoint)getCenterPointMapView:(CGRect)frame {
    CGPoint point = CGPointZero;
    point.x = frame.size.width/2;
    point.y = frame.size.height/2;
    point.x = point.x - kMarkerSizeW/2;
    point.y = point.y - kMarkerSizeH;
    
    return point;
}

-(void)markerWillMove {
    CGRect rect = self.mapView_.frame;
    UIEdgeInsets padding = self.mapView_.padding;
    
    rect.size.height -= padding.bottom;
    CGPoint point = [self getCenterPointMapView:rect];
    rect = CGRectMake(point.x, point.y, kMarkerSizeW,kMarkerSizeH);
    
    mImageFocus.frame = rect;
}

-(void)moveMarker {
    CGRect rect = self.mapView_.frame;
    rect.size.height = self.view.bounds.size.height - rect.origin.y;
    
    CGRect rect2 = mImageFocus.frame;
    CGPoint point = [self getCenterPointMapView:rect];
    rect2.origin = point;
    
    [UIView animateWithDuration:0.2
                     animations:^{
                         self.mapView_.frame = rect;
                         mImageFocus.frame = rect2;
                     } ];
}

- (void)updateMarker:(NSString*)title withPosition:(CLLocationCoordinate2D)coordinate {
    _placeMarker.title = title;
    _placeMarker.position = CLLocationCoordinate2DMake(coordinate.latitude, coordinate.longitude);
    _placeMarker.appearAnimation = kGMSMarkerAnimationPop;
    _placeMarker.flat = YES;
    _placeMarker.draggable = NO;
    _placeMarker.groundAnchor = CGPointMake(0.5, 0.5);
    //    australiaMarker.icon = [UIImage imageNamed:@"australia"];
    _placeMarker.map = self.mapView_;
}

//- (void)getAddressFromLatLonByAppleAPI:(CLLocationCoordinate2D)coor {
//    //CGPoint point = self.mapView_.center;
//    //CLLocationCoordinate2D coor = [self.mapView_.projection coordinateForPoint:point];
//
//    CLGeocoder *ceo = [[CLGeocoder alloc]init];
//    CLLocation *loc = [[CLLocation alloc]initWithLatitude:coor.latitude longitude:coor.longitude]; //insert your coordinates
//    [ceo reverseGeocodeLocation:loc
//              completionHandler:^(NSArray *placemarks, NSError *error) {
//                  CLPlacemark *placemark = [placemarks objectAtIndex:0];
//                  NSLog(@"location %@",placemark.name);
//                  NSLog(@"location %@",placemark.location);
//                  NSString *locatedAt = [[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "];
//                  NSLog(@"location %@",locatedAt);
//              }
//     ];
//}

-(void)getAddressFromLatLon2:(NSString*)coorString
{
    CLLocationCoordinate2D coor = [Utils getLocation:coorString];
    [self getAddressFromLatLon:coor];
}

-(void)getAddressFromLatLon:(CLLocationCoordinate2D)coor
{
    NSLog(@"----------------------:%f,%f",coor.latitude,coor.longitude);
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    [locMgr geocodeAPICall:coor
                completion:^(NSDictionary *user, NSString* params, NSString *str, int status) {
                    
                    if (status==1) {
                        //NSAssert(![NSThread isMainThread], @"This shouldn't be called from the main thread");
                        //NSLog(@"responseDict:%@",user);
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            // Update the UI
                            _lbAddress.text = user[@"fullAddress"];
                            
                            if ([_lbAddress.text isEqualToString:@""]) {
                                _coor = kCLLocationCoordinate2DInvalid;
                                _lbAddress.text = LocalizedStr(@"Address.None.text");
                                _address = @"";
                            }
                            else {
                                _coor = coor;
                                _address = _lbAddress.text;
                            }
                        });
                    }
                }];
}

-(void)getAddressFromFavorite:(NSString*)coorString
{
    CLLocationCoordinate2D coor = [Utils getLocation:coorString];
    NSLog(@"----------------------:%f,%f",coor.latitude,coor.longitude);
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    [locMgr geocodeAPICall:coor
                completion:^(NSDictionary *user, NSString* params, NSString *str, int status) {
                    
                    if (status==1) {
                        //NSAssert(![NSThread isMainThread], @"This shouldn't be called from the main thread");
                        //NSLog(@"responseDict:%@",user);
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            // Update the UI
                            _lbAddress.text = user[@"fullAddress"];
                            
                            if ([_lbAddress.text isEqualToString:@""]) {
                                _coor = kCLLocationCoordinate2DInvalid;
                                _lbAddress.text = LocalizedStr(@"Address.None.text");
                                _address = @"";
                            }
                            else {
                                _coor = coor;
                                _address = _lbAddress.text;
                            }
                            _searchField.text = _address;
                            [self textFieldDidChange:_searchField];
                        });
                    }
                }];
}

#pragma mark - Button - CurrentPosition
-(NSString*)currentPosition {
    // 현재 위치
    NSString *lat;
    NSString *lng;
    
    //CLLocation *location = self.mapView_.myLocation;
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    CLLocation *location = locMgr.realTimeCLocation;
    
    if (location) {
        lat = LOCATION_STR(location.coordinate.latitude);
        lng = LOCATION_STR(location.coordinate.longitude);
    }
    
    return [NSString stringWithFormat:@"%@,%@",lat,lng];
}

- (void)showCposButton {
    
    if (_btnCpos != nil) {
        return;
    }
    
    UIView *n = [super navigationView];
    UIView *v = [n viewWithTag:60];
    
#ifdef _WITZM
    _btnCpos = [[UIButton alloc]
                initWithFrame:CGRectMake(self.mapView_.bounds.size.width - kBasicMargin - 54,
                                         kTopHeight + v.frame.size.height + kBasicMargin,
                                         54, 54)];
#elif defined _CHACHA
    _btnCpos = [[UIButton alloc]
                initWithFrame:CGRectMake(kBasicMargin,
                                         kTopHeight + v.frame.size.height + kBasicMargin,
                                         54, 54)];
#endif
    
    //[btnNavi setBackgroundColor:colorFromRGB(102, 0, 102, 1)];
    [_btnCpos addTarget:self action:@selector(pushCpos) forControlEvents:UIControlEventTouchUpInside];
    [_btnCpos setTag:801];
    //_btnCpos.hidden = YES;
    
    [self.mapView_ addSubview:_btnCpos];
    
    //[self pushCpos];
    
    CLLocationCoordinate2D cloc = [Utils getLocation:[self currentPosition]];
    CLLocationCoordinate2D coor = [self.mapView_.projection coordinateForPoint:self.mapView_.center];
    coor = [Utils getLocation:[Utils getLocationByString:coor]];
    
    if (cloc.longitude == coor.latitude && cloc.longitude == coor.longitude) {
        [self updateCposImage:YES];
    }
    else {
        [self updateCposImage:NO];
    }
}

- (void)updateCposImage:(BOOL)isMyLoc
{
    if (isMyLoc)
        [_btnCpos setImage:[UIImage imageNamed:@"btn_location_on"] forState:UIControlStateNormal];
    else {
        [_btnCpos setImage:[UIImage imageNamed:@"btn_location"] forState:UIControlStateNormal];
    }
}

- (IBAction)pushCpos {

    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    CLLocation *location = locMgr.realTimeCLocation;

    if (location && [Utils isLocationValid:location.coordinate]) {
        NSLog(@"cameramove:%f,%f",location.coordinate.latitude,location.coordinate.longitude);
        [self.mapView_ animateToLocation:location.coordinate];
        NSLog(@"cameramove:%f,%f",self.mapView_.camera.target.latitude,self.mapView_.camera.target.longitude);
    }
    
    [self updateCposImage:YES];
}


#pragma mark - IBAction
-(IBAction)onNaviButtonClick:(id)sender {
    UIButton *btn = (UIButton *)sender;
    NSLog(@"onNaviButtonClick");
    
    //[self dismissViewControllerAnimated:YES completion:nil];
    
    // 호출전에 출발지를 재 조정한다.
    if (btn.tag == 1100 && _step == 420) {
        // back
        [self dismissViewControllerAnimated:YES completion:nil];
        return;
    }
    else if (btn.tag == 1400 && _step == 420) {
        // 확인
        [self.delegate didFoundAddress:_address withLatitute:_coor.latitude andLongtitute:_coor.longitude forTarget:nil];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [self dismissViewControllerAnimated:YES completion:nil];
        });
        return;
    }
    
    if (btn.tag == 1100 || btn.tag == 1101) {
        if ((_step == 110 || _step == 210) && (cstep_ == 21)) {
            if (cstep_ == 120 || cstep_ == 220) {
                cstep_ = (int)(cstep_/100)*100+10;
            }
            else {
                cstep_ = _step;
            }
            [self changeStepView];
            
            self.historyAddress = (NSMutableArray*)PLACE_SELECT;
            
            [self onReloadResultController];
            
            return;
        }
        else if ((_step == 110 || _step == 210 || _step == 310) && (cstep_ == 31)) {
            if (cstep_ == 120 || cstep_ == 220) {
                cstep_ = (int)(cstep_/100)*100+10;
            }
            else {
                cstep_ = _step;
            }
            [self changeStepView];
            
            self.historyAddress = (NSMutableArray*)PLACE_SELECT;
            
            [self onReloadResultController];
            
            return;
        }
        
        //        [self.navigationController popViewControllerAnimated:YES];
    }
    else if (btn.tag == 1400) { // confirm
        // 주소를 찾지 못했으면 처리하지 않는다.
        if ([_address isEqualToString:@""]) {
            return;
        }
        
        if ( [self.delegate respondsToSelector:@selector(didFoundAddress:withLatitute:andLongtitute:forTarget:)] ) {
            
            if (![GoogleUtils isLocationValid:_coor]) {
                
                [self alertError:@"" message:LocalizedStr(@"Address.None.text")];
                return;
            }
            
            NSString *loc = [NSString stringWithFormat:@"%@,%@",LOCATION_STR(_coor.latitude), LOCATION_STR(_coor.longitude)];
            
            // 주소 선택(21:수정)
            if (cstep_ == 21 && (_step == 110 || _step == 210)) {
                if (!appDelegate.isDriverMode) {
                    if (_target == nil) {
                        PLACE_DELETE_FAVORITE(loc);
                        PLACE_INSERT_FAVORITE(loc,_address);
                    }
                    else {
                        NSDictionary *item = (NSDictionary*)_target;
                        
                        NSInteger selectType = [item[@"type"] intValue];
                        
                        if (selectType == 1) {
                            PLACE_UPDATE_HOME(loc,_address);
                        }
                        else if (selectType == 2) {
                            PLACE_UPDATE_WORK(loc,_address);
                        }
                        else if (selectType == 3) {
                            //PLACE_DELETE_FAVORITE(loc);
                            PLACE_DELETE_FAVORITE(item[@"location"]);
                            PLACE_INSERT_FAVORITE(loc,_address);
                        }
                        //                        else if (selectType == 4) {
                        //                            PLACE_DELETE_HISTORY(loc);
                        //                            PLACE_INSERT_HISTORY(loc,_address);
                        //                        }
                    }
                }
                
                [self.delegate didFoundAddress:_tfedit.text withLatitute:_coor.latitude andLongtitute:_coor.longitude forTarget:_target];
            }
            // pickup location > 별표선택 후 정보 변경
            else if (cstep_ == 31) {
                if (_target == nil) {
                    PLACE_DELETE_FAVORITE(loc);
                    PLACE_INSERT_FAVORITE(loc,_tfedit.text);
                }
                else {
                    NSDictionary *item = items;
                    
                    NSInteger selectType = [item[@"type"] intValue];
                    
                    if (selectType == 1) {
                        PLACE_UPDATE_HOME(loc,_tfedit.text);
                    }
                    else if (selectType == 2) {
                        PLACE_UPDATE_WORK(loc,_tfedit.text);
                    }
                    else if (selectType == 3) {
                        //PLACE_DELETE_FAVORITE(loc);
                        PLACE_DELETE_FAVORITE(item[@"location"]);
                        PLACE_INSERT_FAVORITE(loc,_tfedit.text);
                    }
                    //                    else if (selectType == 4) {
                    //                        PLACE_DELETE_HISTORY(loc);
                    //                        PLACE_INSERT_HISTORY(loc,_tfedit.text);
                    //                    }
                }
                
                if (_step != 310) {
                    cstep_ = _step;
                    [self changeStepView];
                    return;
                }
            }
            // add place
            else if (cstep_ == 21) {
                // 입력받은 이름이 없으면
                if ([_tfedit.text isEqualToString:@""]) {
                    [_tfedit becomeFirstResponder];
                    return;
                }
                
                if (_target == nil) {
                    PLACE_DELETE_FAVORITE(loc);
                    PLACE_INSERT_FAVORITE(loc,_tfedit.text);
                }
                else {
                    NSDictionary *item = (NSDictionary*)_target;
                    
                    NSInteger selectType = [item[@"type"] intValue];
                    
                    if (selectType == 1) {
                        PLACE_UPDATE_HOME(loc,_tfedit.text);
                    }
                    else if (selectType == 2) {
                        PLACE_UPDATE_WORK(loc,_tfedit.text);
                    }
                    else if (selectType == 3) {
                        //PLACE_DELETE_FAVORITE(loc);
                        PLACE_DELETE_FAVORITE(item[@"location"]);
                        PLACE_INSERT_FAVORITE(loc,_tfedit.text);
                    }
                    //                    else if (selectType == 4) {
                    //                        PLACE_DELETE_HISTORY(loc);
                    //                        PLACE_INSERT_HISTORY(loc,_tfedit.text);
                    //                    }
                }
                
                [self.delegate didFoundAddress:_address withLatitute:_coor.latitude andLongtitute:_coor.longitude forTarget:_target];
            }
            else {
                //UITextField *textfield = (UITextField*)_target;
                //if (![textfield.text isEqualToString:_address]) {
                //_address = _lbAddress.text;
                // trip 이 끝나면 저장한다.
                //                NSString *loc = [NSString stringWithFormat:@"%f,%f",_coor.latitude, _coor.longitude];
                //                PLACE_DELETE_HISTORY(loc);
                //                PLACE_INSERT_HISTORY(loc,_lbAddress.text);
                //dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [self.delegate didFoundAddress:_address withLatitute:_coor.latitude andLongtitute:_coor.longitude forTarget:_target];
                //});
                
                //}
            }
            
        }
    }
    else if (btn.tag == 1401) { // 주소지우기 -> 주소검색화면 이전
        if (cstep_ == 120 || cstep_ == 220) {
            cstep_ = (int)(cstep_/100)*100+10;
        }
        else {
            cstep_ = _step;
        }
        [self changeStepView];
        return;
    }
    //[self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
    //[[self navigationController] popViewControllerAnimated:YES];
}

-(IBAction)onClearButtonClick:(id)sender {
    
    self.historyAddress = (NSMutableArray*)PLACE_SELECT;
    
    rowType = 1;
    _searchField.text = @"";
    NSLog(@"_searchField.text:%@",_searchField.text);
    [self.matchedAddress removeAllObjects];
    
    [self onReloadResultController];
}

-(void)deleteFavorite:(UIButton*)btn {
    
    NSDictionary *items__ = [self.historyAddress objectAtIndex:btn.tag];
    
    if ([items__[@"type"] intValue] == 1) {
        PLACE_UPDATE_HOME(@"",@"");
    }
    else if ([items__[@"type"] intValue] == 2) {
        PLACE_UPDATE_WORK(@"",@"");
    }
    else if ([items__[@"type"] intValue] == 3) {
        PLACE_DELETE_FAVORITE(items__[@"location"]);
    }
    else if ([items__[@"type"] intValue] == 4) {
        PLACE_DELETE_HISTORY(items__[@"location"]);
    }
    
    self.historyAddress = (NSMutableArray*)PLACE_SELECT;
    
    [self onReloadResultController];
    
}

-(IBAction)onMapViewButtonClick:(id)sender {
    
    CLLocationCoordinate2D coor = appDelegate.location.coordinate;
    if (cstep_ == 110 || cstep_ == 210 || cstep_ == 420) {
        if ([GoogleUtils isLocationValid:self.destPosition]) {
            coor = self.destPosition;
        }
    }
    NSLog(@"----------------------:%f,%f",coor.latitude,coor.longitude);
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    [locMgr geocodeAPICall:coor
                completion:^(NSDictionary *user, NSString* params, NSString *str, int status) {
                    
                    if (status==1) {
                        //NSAssert(![NSThread isMainThread], @"This shouldn't be called from the main thread");
                        //NSLog(@"responseDict:%@",user);
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            // Update the UI
                            //NSString *loc = [Utils formattedPosition:coor];//[NSString stringWithFormat:@"%f,%f",appDelegate.location.coordinate.latitude, appDelegate.location.coordinate.longitude];
                            // trip 이 끝나면 저장한다.
                            //                    PLACE_DELETE_HISTORY(__loc);
                            //                    PLACE_INSERT_HISTORY(__loc,_lbAddress.text);
                            //NSLog(@"loc:%@",loc);
                            self.historyAddress = nil;
                            //[self.historyAddress removeAllObjects];
                            
                            NSLog(@"cameramove:%f,%f",coor.latitude,coor.longitude);
                            GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:coor.latitude longitude:coor.longitude zoom:17];
                            [self.mapView_ animateToCameraPosition:camera];
                            NSLog(@"cameramove:%f,%f",self.mapView_.camera.target.latitude,self.mapView_.camera.target.longitude);
                            
                            self->_coor = coor;
                            //                    _place = position;
                            self->_address = user[@"fullAddress"];
                            //background.hidden = YES;
                            [self hideResultController];
                            
                            if (cstep_ == 110) {
                                self->cstep_ = 120;
                                [self changeStepView];
                            }
                            else if (cstep_ == 210) {
                                self->cstep_ = 220;
                                [self changeStepView];
                            }
                            else if (cstep_ == 310) {
                                self->cstep_ = 31;
                                [self changeStepView];
                            }
                            else if (cstep_ == 11) {
                                cstep_ = 21;
                                [self changeStepView];
                            }
                            
                            _lbAddress.text = user[@"fullAddress"];
                            NSLog(@"_lbAddress.text:%@",self->_lbAddress.text);
                        });
                    }
                }];
    
}

-(IBAction)onDeleteButtonClick:(id)sender {
    UIButton *btn = (UIButton*)sender;
    NSLog(@"btn.tag:%d",(int)btn.tag);
    
    if (self.historyAddress != nil && self.historyAddress.count) {
        LGAlertView *alertView = [[LGAlertView alloc] initWithTitle:nil
                                                            message:LocalizedStr(@"Map.Search.Address.Favorite.Delete.title")
                                                              style:LGAlertViewStyleAlert
                                                       buttonTitles:@[LocalizedStr(@"Button.OK")]
                                                  cancelButtonTitle:LocalizedStr(@"Button.CANCEL")
                                             destructiveButtonTitle:nil
                                                      actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                                                          [self deleteFavorite:btn];
                                                      }
                                                      cancelHandler:^(LGAlertView *alertView) {
                                                      }
                                                 destructiveHandler:^(LGAlertView *alertView) {
                                                 }];
        [Utils initAlertButtonColor:alertView];
        alertView.windowLevel = LGAlertViewWindowLevelAboveStatusBar;
        [alertView showAnimated:YES completionHandler:nil];
        
    }
}

-(IBAction)onEditButtonClick:(id)sender {
    UIButton *btn = (UIButton*)sender;
    NSLog(@"btn.tag:%d",(int)btn.tag);
    
    if (self.historyAddress != nil && self.historyAddress.count) {
        
        NSDictionary *loc = [self.historyAddress objectAtIndex:btn.tag];
        
        NSString *location = [Utils nullToString:loc[@"location"]];
        if ([location isEqualToString:@""] || [location isEqualToString:@"0,0"]) {
            location = @"0,0";
        }
        NSArray* stringComponents = [location componentsSeparatedByString:@","];
        
        CLLocationCoordinate2D position;
        position.latitude  = [[NSDecimalNumber decimalNumberWithString:[stringComponents objectAtIndex:0]] doubleValue];
        position.longitude = [[NSDecimalNumber decimalNumberWithString:[stringComponents objectAtIndex:1]] doubleValue];
        
        if (![GoogleUtils isLocationValid:position]) {
            return;
        }
        NSLog(@"----------------------:%f,%f",position.latitude,position.longitude);
        //[self showBusyIndicator:@"Searching..."];
        INTULocationManager *locMgr = [INTULocationManager sharedInstance];
        [locMgr geocodeAPICall:position
                    completion:^(NSDictionary *user, NSString* params, NSString *str, int status) {
                        
                        if (status==1) {
                            //NSAssert(![NSThread isMainThread], @"This shouldn't be called from the main thread");
                            //NSLog(@"responseDict:%@",user);
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                // Update the UI
                               
                                _lbAddress.text = user[@"fullAddress"];
                                NSLog(@"_lbAddress.text:%@",_lbAddress.text);
                                
                                items = loc;
                                
                                NSLog(@"cameramove:%f,%f",position.latitude,position.longitude);
                                GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:position.latitude longitude:position.longitude zoom:17];
                                [self.mapView_ animateToCameraPosition:camera];
                                NSLog(@"cameramove:%f,%f",self.mapView_.camera.target.latitude,self.mapView_.camera.target.longitude);
                                
                                _coor = position;
                                //                    _place = position;
                                _address = _lbAddress.text;
                                
                                //background.hidden = YES;
                                [self hideResultController];
                                
                                cstep_ = 31;
                                [self changeStepView];
                                
                            });
                        }
                    }];
    }
}

-(IBAction)onAddButtonClick:(id)sender {
    //PLACE_INSERT(@"123,430",@"3",@"3");
    
    if (![GoogleUtils isLocationValid:_coor]) {
        
        [self alertError:@"" message:LocalizedStr(@"Address.None.text")];
        return;
    }
    
    __block LGAlertView *alertView1 = [[LGAlertView alloc] initWithTitle:nil
                                                                 message:nil
                                                                   style:LGAlertViewStyleActionSheet
                                                            buttonTitles:@[LocalizedStr(@"Map.Search.Address.HOME.text"), LocalizedStr(@"Map.Search.Address.WORK.text"), LocalizedStr(@"Map.Search.Address.FAVORITE.text")]
                                                       cancelButtonTitle:nil
                                                  destructiveButtonTitle:nil
                                                           actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                                                               NSLog(@"actionHandler, %@, %lu", title, (long unsigned)index);
                                                               __block NSInteger selectType = index+1;
                                                               
                                                               [self updatePlaceInfo:title type:selectType];
                                                               
                                                           }
                                                           cancelHandler:^(LGAlertView *alertView) {
                                                               NSLog(@"cancelHandler");
                                                           }
                                                      destructiveHandler:^(LGAlertView *alertView) {
                                                          NSLog(@"destructiveHandler");
                                                      }];
    [alertView1 showAnimated:YES completionHandler:nil];
    
    //[alertView setButtonAtIndex:0 enabled:NO];
    //[alertView2 showAnimated:YES completionHandler:nil];
}

- (void)updatePlaceInfo:(NSString*)title type:(NSInteger)selectType
{
    LGAlertView *alertView2 = [[LGAlertView alloc] initWithTextFieldsAndTitle:title
                                                                      message:nil
                                                           numberOfTextFields:1
                                                       textFieldsSetupHandler:^(UITextField *textField, NSUInteger index)
                               {
                                   if (index == 0) {
                                       textField.placeholder = LocalizedStr(@"Map.Search.Address.Add.placeholder.text");
                                       textField.text = @"";
                                   }
                                   
                                   textField.tag = index;
                                   //textField.delegate = self;
                                   textField.enablesReturnKeyAutomatically = YES;
                                   textField.autocapitalizationType = NO;
                                   textField.autocorrectionType = NO;
                                   textField.clearButtonMode = UITextFieldViewModeNever;
                                   textField.textAlignment = NSTextAlignmentCenter;
                               }
                                                                 buttonTitles:@[LocalizedStr(@"Button.OK")]
                                                            cancelButtonTitle:LocalizedStr(@"Button.CANCEL")
                                                       destructiveButtonTitle:nil
                                                                actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                                                                    UITextField *secondTextField = alertView.textFieldsArray[index];
                                                                    NSString *loc = [NSString stringWithFormat:@"%f,%f",_coor.latitude, _coor.longitude];
                                                                    NSLog(@"loc:%@",loc);
                                                                    
                                                                    if ([GoogleUtils isLocationValid:_coor] && ![[Utils nullToString:secondTextField.text] isEqualToString:@""]) {
                                                                        if (selectType == 1) {
                                                                            PLACE_UPDATE_HOME(loc,secondTextField.text);
                                                                        }
                                                                        else if (selectType == 2) {
                                                                            PLACE_UPDATE_WORK(loc,secondTextField.text);
                                                                        }
                                                                        else if (selectType == 3) {
                                                                            PLACE_DELETE_FAVORITE(loc);
                                                                            PLACE_INSERT_FAVORITE(loc,secondTextField.text);
                                                                        }
                                                                    }
                                                                    
                                                                    
                                                                }
                                                                cancelHandler:^(LGAlertView *alertView) {
                                                                    NSLog(@"cancelHandler");
                                                                    
                                                                }
                                                           destructiveHandler:^(LGAlertView *alertView) {
                                                               NSLog(@"destructiveHandler");
                                                           }];
    //[alertView1 transitionToAlertView:alertView2 completionHandler:nil];
    [Utils initAlertButtonColor:alertView2];
    [alertView2 showAnimated:YES completionHandler:nil];
}


#pragma mark - GMSMapViewDelegate

- (void)mapView:(GMSMapView *)mapView willMove:(BOOL)gesture
{
    if (!gesture) {
        return;
    }
    isMoveMapBYGesture = gesture;
    
    if (_placeMarker.map) {
        [self.mapView_ clear];
        _placeMarker.appearAnimation = kGMSMarkerAnimationNone;
        _placeMarker.map = nil;
    }
    //    _searchField.text = @"";
    _lbAddress.text = LocalizedStr(@"String.Address.Searching.text");
    _address = @"";
    
    //NSLog(@"willMove");
    [UIView animateWithDuration:1.0 animations:^{
        //mImageFocus.alpha = 0.0f;
    }];
    
    
    [[NSOperationQueue mainQueue] addOperationWithBlock:^ {
        [UIView animateWithDuration:kOverlayMovingTime animations:^{
            // 상단
            UIView *n = [super navigationView];
            UIView *navigationView = [n viewWithTag:60];
            CGRect rect1 = navigationView.frame;
            rect1.origin.y = -(rect1.size.height);
            navigationView.frame = rect1;
            
            rect1 = _addrView.frame;
            rect1.origin.y = -(rect1.size.height);
            _addrView.frame = rect1;
            
            //[[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationFade];
            
            // 하단.
            CGRect rect4 = _confirmView.frame;
            rect4.origin.y = self.view.frame.size.height - _addrTextView.frame.size.height;
            _confirmView.frame = rect4;
            
            // mylocation
            //            TXButton* btnCpos = [self.mapView_ viewWithTag:801];
            //            CGRect rect5 = btnCpos.frame;
            //            rect5.origin.y = self.mapView_.frame.size.height - btnCpos.frame.size.height - kBasicMargin;
            //            btnCpos.frame = rect5;
            
            [self updateCposImage:NO];
        }];
    }];
    
    _btnCpos.alpha = 0;
    _btnCancel.alpha = 0;
}

- (void)mapView:(GMSMapView *)mapView didChangeCameraPosition:(GMSCameraPosition *)position {
    //NSLog(@"didChangeCameraPosition geocode point (%f,%f)",position.target.latitude, position.target.longitude);
}

- (void)mapView:(GMSMapView *)mapView idleAtCameraPosition:(GMSCameraPosition *)camera {
    
    if (isMoveMapBYGesture) {
        
        [UIView animateWithDuration:kOverlayMovingTime animations:^{
            
            // 상단
            UIView *n = [super navigationView];
            //NSInteger _y = n.frame.origin.y + n.frame.size.height;
            UIView *navigationView = [n viewWithTag:60];
            CGRect rect1 = navigationView.frame;
            
            rect1.origin.y = kTopHeight;
            navigationView.frame = rect1;
            
            rect1 = _addrView.frame;
            rect1.origin.y = navigationView.frame.origin.y + navigationView.frame.size.height;
            _addrView.frame = rect1;
            
            //[[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];
            
            // 하단.
            CGRect rect4 = _confirmView.frame;
            rect4.origin.y = self.view.frame.size.height - rect4.size.height - kBottomBottonSafeArea;
            _confirmView.frame = rect4;
            
            // mylocation
            //            TXButton* btnCpos = [self.mapView_ viewWithTag:801];
            //            CGRect rect5 = btnCpos.frame;
            //            rect5.origin.y = self.mapView_.frame.size.height - btnCpos.frame.size.height - kBasicMargin;
            //            btnCpos.frame = rect5;
            
            _btnCpos.alpha = 1;
            _btnCancel.alpha = 1;
        }];
    }
    
    if (appDelegate.isDriverMode && isMoveMapBYGesture) {
        
    }
    else {
        isMoveMapBYGesture = NO;
    }
    
    [self getAddressFromLatLon:camera.target];
    //[self getAddressFromLatLonByAppleAPI:camera.target];
    
    [UIView animateWithDuration:0.5 animations:^{
        mImageFocus.alpha = 1.0f;
    }];
    NSLog(@"cameramove:%f,%f",self.mapView_.camera.target.latitude,self.mapView_.camera.target.longitude);
    NSLog(@"geocode point (%f,%f)",camera.target.latitude, camera.target.longitude);
}


- (void)mapView:(GMSMapView *)mapView didLongPressAtCoordinate:(CLLocationCoordinate2D)coordinate {
    // On a long press, reverse geocode this location.
    NSLog(@"geocode point (%f,%f)",
          coordinate.latitude, coordinate.longitude);
    
    //[self updateMarker:@"" withPosition:coordinate];
    
}

#pragma mark - CLLocationManager delegate
// location 초기화
-(void)didUpdateLocations:(CLLocation*)location {
    /*
    if ([GoogleUtils isLocationValid:_destPosition]) {
        [self.mapView_ animateToLocation:_destPosition];
    }
    else {
        [self.mapView_ animateToLocation:location.coordinate];
    }
     */
}

#pragma mark - KVO updates

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context {
    if (!_firstLocationUpdate) {
        // If the first location update has not yet been recieved, then jump to that
        // location.
        _firstLocationUpdate = YES;
        
        CLLocation *location = [change objectForKey:NSKeyValueChangeNewKey];
        if ([GoogleUtils isLocationValid:_destPosition]) {
            self.mapView_.camera = [GMSCameraPosition cameraWithTarget:_destPosition
                                                                  zoom:17];
        }
        else {
            self.mapView_.camera = [GMSCameraPosition cameraWithTarget:location.coordinate
                                                                  zoom:14];
        }
        
        NSLog(@"cameramove:%f,%f",self.mapView_.camera.target.latitude,self.mapView_.camera.target.longitude);
        //        CLLocation *location = [object valueForKeyPath:keyPath];
        //        self.mapView_.settings.myLocationButton = YES;
        //        self.mapView_.camera = [GMSCameraPosition cameraWithTarget:location.coordinate
        //                                                              zoom:14];
    }
}

#pragma mark - TableDataSourceDelegate
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [tableView setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [tableView setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
    
    UIView *seperatorView = [[UIView alloc] initWithFrame:CGRectMake(10, cell.frame.size.height - 1, cell.frame.size.width-10*2, 1)];
    seperatorView.backgroundColor = UIColorTableSeperator;
    [cell addSubview:seperatorView];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 62;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    // This will create a "invisible" footer
    return 0.01f;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [UIView new];
    
    // If you are not using ARC:
    // return [[UIView new] autorelease];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (rowType == 1) {
        return (NSInteger)(self.historyAddress.count);
    }
    else {
        return (NSInteger)self.matchedAddress.count;
    }
//    if (cstep_ == 11 || cstep_ == 21) {
//        rowType = 2;
//        return (NSInteger)self.matchedAddress.count;
//    }
//
//    if (self.historyAddress != nil && self.historyAddress.count) {
//        rowType = 1;
//        return (NSInteger)(self.historyAddress.count);
//    }
//    rowType = 2;
//    return (NSInteger)self.matchedAddress.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *simpleTableIdentifier = @"PlaceTableCell";
    
    //UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    PlaceTableCell *cell = (PlaceTableCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    [cell.contentView setBackgroundColor:[UIColor colorWithRed:0.839 green:0.129 blue:0.129 alpha:1]];
    
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"PlaceTableCell"
                                                     owner:(self)
                                                   options:nil];
        cell = [nib objectAtIndex:0];
    }
    self->cellHeight = cell.frame.size.height;
//    NSLog(@"indexPath.row:%ld",(long)indexPath.row);
//    NSLog(@"indexPath.row:%@",self.historyAddress);
    //    NSLog(@"cell.height:%d",(int)cell.frame.size.height);
    /*
     CGFloat separatorInset; // Separator x position
     CGFloat separatorHeight;
     CGFloat separatorWidth;
     CGFloat separatorY;
     UIImageView *separator;
     UIColor *separatorBGColor;
     
     separatorY      = cell.frame.size.height;
     separatorHeight = (1.0 / [UIScreen mainScreen].scale);  // This assures you to have a 1px line height whatever the screen resolution
     separatorWidth  = cell.frame.size.width;
     separatorInset  = 15.0f;
     separatorBGColor  = [UIColor colorWithRed: 204.0/255.0 green: 204.0/255.0 blue: 204.0/255.0 alpha:1.0];
     
     separator = [[UIImageView alloc] initWithFrame:CGRectMake(separatorInset, separatorY, separatorWidth,separatorHeight)];
     separator.backgroundColor = separatorBGColor;
     [cell addSubview:separator];
     */
    
    cell.btnDelete.hidden = YES;
    cell.btnEdit.hidden = YES;
    cell.btnEdit.tag = indexPath.row;
    
    CGRect frame = cell.nameLabel.frame;
    frame.size.width = cell.frame.size.width - 10*2;
    cell.nameLabel.frame = frame;
    
    frame = cell.placeLabel.frame;
    frame.size.width = cell.frame.size.width - 10*2;
    cell.placeLabel.frame = frame;
    
    frame = cell.addressLabel.frame;
    frame.size.width = cell.frame.size.width - 10*2;
    cell.addressLabel.frame = frame;
    
    if (rowType==1) { // history
        cell.nameLabel.hidden = NO;
        cell.placeLabel.hidden = YES;
        cell.addressLabel.hidden = YES;
        // 마지막이면 기록삭제를 넣는다.
        //        if ((self.historyAddress.count) == indexPath.row) {
        //            cell.nameLabel.text = LocalizedStr(@"Map.Search.Address.BTN.delete.text");
        //            cell.nameLabel.textAlignment = NSTextAlignmentCenter;
        //            cell.btnDelete.tag = -9;
        //            cell.thumbnailImageView.hidden = YES;
        //            cell.btnDelete.hidden = YES;
        //            cell.btnEdit.hidden = YES;
        //
        //            return cell;
        //        }
        
        NSDictionary *item = [self.historyAddress objectAtIndex:indexPath.row];
        cell.nameLabel.text = item[@"place"];
        cell.btnDelete.tag = indexPath.row;
        if ([item[@"type"] intValue] == 1) {
            cell.thumbnailImageView.image = [UIImage imageNamed:@"ic_favorite_home_gold"];
            //if ([[Utils nullToString:item[@"place"]] isEqualToString:@""])
            {
                cell.nameLabel.text = LocalizedStr(@"Map.Search.Address.HOME.text");
                cell.btnDelete.hidden = YES;
                cell.thumbnailImageView.image = [UIImage imageNamed:@"ic_favorite_home_gold"];
                [cell.btnEdit setImage:[UIImage imageNamed:@"icon_fav_star01"] forState:UIControlStateNormal];
                cell.btnEdit.hidden = NO;
                
                CGRect frame = cell.nameLabel.frame;
                frame.size.width = cell.frame.size.width - 10*2 - cell.btnEdit.frame.size.width;
                cell.nameLabel.frame = frame;
            }
            
            if ([[Utils nullToString:item[@"place"]] isEqualToString:@""]) {
                cell.nameLabel.textColor = [UIColor grayColor];
            }
            else {
                cell.nameLabel.textColor = [UIColor blackColor];
            }
        }
        else if ([item[@"type"] intValue] == 2) {
            cell.thumbnailImageView.image = [UIImage imageNamed:@"ic_favorite_company_gold"];
            //if ([[Utils nullToString:item[@"place"]] isEqualToString:@""])
            {
                cell.nameLabel.text = LocalizedStr(@"Map.Search.Address.WORK.text");
                cell.btnDelete.hidden = YES;
                [cell.btnEdit setImage:[UIImage imageNamed:@"icon_fav_star01"] forState:UIControlStateNormal];
                cell.btnEdit.hidden = NO;
                
                CGRect frame = cell.nameLabel.frame;
                frame.size.width = cell.frame.size.width - 10*2- cell.btnEdit.frame.size.width;
                cell.nameLabel.frame = frame;
            }
            
            if ([[Utils nullToString:item[@"place"]] isEqualToString:@""]) {
                cell.nameLabel.textColor = [UIColor grayColor];
            }
            else {
                cell.nameLabel.textColor = [UIColor blackColor];
            }
        }
        else if ([item[@"type"] intValue] == 3) {
            cell.thumbnailImageView.image = [UIImage imageNamed:@"ic_favorite_gold"];
            [cell.btnEdit setImage:[UIImage imageNamed:@"icon_fav_star02"] forState:UIControlStateNormal];
            cell.btnEdit.hidden = NO;
            
            CGRect frame = cell.nameLabel.frame;
            frame.size.width = cell.frame.size.width - 10*2 - cell.btnEdit.frame.size.width;
            cell.nameLabel.frame = frame;
        }
        else if ([item[@"type"] intValue] == 4) {
            cell.thumbnailImageView.image = [UIImage imageNamed:@"ic_location_history"];
            
        }
        return cell;
    }
    else { // match
        cell.nameLabel.hidden = YES;
        cell.placeLabel.hidden = NO;
        cell.addressLabel.hidden = NO;
        
        NSDictionary *item = [self.matchedAddress objectAtIndex:indexPath.row];
        cell.placeLabel.text = item[@"name"];
        cell.addressLabel.text = item[@"address"];
        cell.thumbnailImageView.image = [UIImage imageNamed:@"ic_location"];
        cell.btnDelete.hidden = YES;
        cell.btnDelete.tag = indexPath.row;
        
        return cell;
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    PlaceTableCell *cell = (PlaceTableCell *)[tableView cellForRowAtIndexPath:indexPath];
    
    // 기록삭제
    if (cell.btnDelete.tag == -9) {
        PLACE_DELETE_ALL;
        return;
    }
    
    if (rowType==1) { // history
        
        NSDictionary *loc = [self.historyAddress objectAtIndex:indexPath.row];
        
        NSString *location = [Utils nullToString:loc[@"location"]];
        if ([location isEqualToString:@""] || [location isEqualToString:@"0,0"]) {
            location = @"0,0";
        }
        NSArray* stringComponents = [location componentsSeparatedByString:@","];
        
        CLLocationCoordinate2D position;
        position.latitude  = [[NSDecimalNumber decimalNumberWithString:[stringComponents objectAtIndex:0]] doubleValue];
        position.longitude = [[NSDecimalNumber decimalNumberWithString:[stringComponents objectAtIndex:1]] doubleValue];
        
        if (![GoogleUtils isLocationValid:position]) {
            //[appDelegate onCommonErrorAlert:LocalizedStr(@"String.Address.Not.Define.text")];
            
            UIAlertController * alert=   [UIAlertController
                                          alertControllerWithTitle:LocalizedStr(@"String.Address.Not.Define.text")
                                          message:nil
                                          preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* ok = [UIAlertAction
                                 actionWithTitle:LocalizedStr(@"Button.OK")
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                     
                                 }];
            [alert addAction:ok];
            alert.view.tintColor = UIColorBasicText;
            
            [self presentViewController:alert animated:YES completion:nil];
            
            //            UIAlertController * alert=   [UIAlertController
            //                                          alertControllerWithTitle:@""
            //                                          message:LocalizedStr(@"String.Address.Not.Define.text")
            //                                          preferredStyle:UIAlertControllerStyleAlert];
            //
            //            [self presentViewController:alert animated:YES completion:nil];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [alert dismissViewControllerAnimated:YES completion:nil];
            });
            return;
        }
        
        //[self showBusyIndicator:@"Searching..."];
        NSLog(@"----------------------:%f,%f",position.latitude,position.longitude);
        INTULocationManager *locMgr = [INTULocationManager sharedInstance];
        [locMgr geocodeAPICall:position
                    completion:^(NSDictionary *user, NSString* params, NSString *str, int status) {
                        
                        if (status==1) {
                            //NSAssert(![NSThread isMainThread], @"This shouldn't be called from the main thread");
                            //NSLog(@"responseDict:%@",user);
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                // Update the UI
                                
                                _lbAddress.text = user[@"fullAddress"];
                                NSString *loc = [Utils formattedPosition:position];
                                // trip 이 끝나면 저장한다.
                                //                    PLACE_DELETE_HISTORY(loc);
                                //                    PLACE_INSERT_HISTORY(loc,_lbAddress.text);
                                NSLog(@"loc:%@",loc);
                                self.historyAddress = nil;
                                //[self.historyAddress removeAllObjects];
                                
                                NSLog(@"cameramove:%f,%f",position.latitude,position.longitude);
                                GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:position.latitude longitude:position.longitude zoom:17];
                                [self.mapView_ animateToCameraPosition:camera];
                                NSLog(@"cameramove:%f,%f",self.mapView_.camera.target.latitude,self.mapView_.camera.target.longitude);
                                
                                _coor = position;
                                //                    _place = position;
                                _address = user[@"fullAddress"];
                                //background.hidden = YES;
                                [self hideResultController];
                                
                                if (cstep_ == 110) {
                                    cstep_ = 120;
                                    [self changeStepView];
                                }
                                else if (cstep_ == 210) {
                                    cstep_ = 220;
                                    [self changeStepView];
                                }
                                else if (cstep_ == 310) {
                                    cstep_ = 31;
                                    [self changeStepView];
                                }
                                else if (cstep_ == 11) {
                                    cstep_ = 21;
                                    [self changeStepView];
                                }
                                
                                _lbAddress.text = user[@"fullAddress"];
                                //_searchField.text = address.attributedFullText.string;
                                NSLog(@"_lbAddress.text:%@",_lbAddress.text);
                                
                            });
                        }
                    }];
    }
    else { // match
        // 오류
        if ([self.matchedAddress count] <= indexPath.row) {
            return;
        }
        //[self showBusyIndicator:@"Searching..."];
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        
        INTULocationManager *locMgr = [INTULocationManager sharedInstance];
        NSDictionary *address = [self.matchedAddress objectAtIndex:indexPath.row];
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        
        if (![Utils isDictionary:address]) {
            [self hideBusyIndicator];
            return;
        }
        
        CLLocationCoordinate2D coordinate;
        if (locMgr.isKorea) {
            coordinate = [GoogleUtils formatedLocation:address[@"lat"] lng:address[@"lon"]];
            [self selectCell:coordinate name:address[@"name"]];
        }
        else {
            /*
            [_placesClient lookUpPlaceID:address[@"placeID"] callback:^(GMSPlace *place, NSError *error) {
                [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                if (error != nil) {
                    NSLog(@"Place Details error %@", [error localizedDescription]);
                    [self hideBusyIndicator];
                    return;
                }
                
                if (place != nil) {
                    
                    [_searchField resignFirstResponder];
                    NSMutableAttributedString *text =
                    [[NSMutableAttributedString alloc] initWithString:[place description]];
                    if (place.attributions) {
                        [text appendAttributedString:[[NSAttributedString alloc] initWithString:@"\n\n"]];
                        [text appendAttributedString:place.attributions];
                    }
                    
                    [self selectCell:place.coordinate name:address[@"name"]];
                    
                } else {
                    NSLog(@"No place details for %@", address[@"placeID"]);
                }
                [self hideBusyIndicator];
            }];
             */
        }
    }
}

//// Override to support conditional editing of the table view.
//// This only needs to be implemented if you are going to be returning NO
//// for some items. By default, all items are editable.
//- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
//    // Return YES if you want the specified item to be editable.
//
//    if (self.historyAddress != nil && self.historyAddress.count) {
//        NSDictionary *loc = [self.historyAddress objectAtIndex:indexPath.row];
//
//        NSString *location = [Utils nullToString:loc[@"location"]];
//        if (indexPath.row == 0 || indexPath.row == 1) {
//            if ([location isEqualToString:@""] || [location isEqualToString:@"0,0"]) {
//                return NO;
//            }
//        }
//        return YES;
//    }
//
//    return NO;
//}
//
//// Override to support editing the table view.
//- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
//    if (editingStyle == UITableViewCellEditingStyleDelete) {
//        //add code here for when you hit delete
//
//        if (self.historyAddress != nil && self.historyAddress.count) {
//            NSDictionary *items = [self.historyAddress objectAtIndex:indexPath.row];
//
//            PLACE_DELETE_HISTORY(items[@"location"]);
//            self.historyAddress = (NSMutableArray*)PLACE_SELECT;
//
//            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
//
//        }
//    }
//}

- (void)selectCell:(CLLocationCoordinate2D)coordinate name:(NSString*)name {
    [_searchField resignFirstResponder];
    _lbAddress.text = name;
    NSLog(@"_lbAddress.text:%@",_lbAddress.text);
    
    NSLog(@"cameramove:%f,%f",coordinate.latitude,coordinate.longitude);
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:coordinate.latitude longitude:coordinate.longitude zoom:17];
    [self.mapView_ animateToCameraPosition:camera];
    NSLog(@"cameramove:%f,%f",self.mapView_.camera.target.latitude,self.mapView_.camera.target.longitude);
    
    _coor = coordinate;
    //        _place = place;
    _address = name;
    //background.hidden = YES;
    
    [self hideResultController];
    
    if (cstep_ == 110) {
        cstep_ = 120;
        [self changeStepView];
    }
    else if (cstep_ == 210) {
        cstep_ = 220;
        [self changeStepView];
    }
    else if (cstep_ == 310) {
        cstep_ = 31;
        [self changeStepView];
    }
    else if (cstep_ == 11) {
        cstep_ = 21;
        //items = @{@"place":_address, @"type":@"3", @"location":loc};
        [self changeStepView];
    }
    
    // 카메라의 위치가 같으면 주소를 직접 입력한다.
    if ([Utils isEqualLocation:coordinate location:self.mapView_.camera.target]) {
        //[self getAddressFromLatLon:camera.target];
    }
    [self hideBusyIndicator];
}

- (void)showResultController
{
    background.hidden = NO;
    isKeywordShow = YES;
    
    if(![_resultsController.view isDescendantOfView:self.view]) {
        [self addChildViewController:_resultsController];
        
        [self.view addSubview:_resultsController.view];
    }
    /*
    [self addChildViewController:_resultsController];
    
    [self.view addSubview:_resultsController.view];
    */
    [self.view layoutIfNeeded];
    [UIView animateWithDuration:0.5
                     animations:^{
                         _resultsController.view.alpha = 1.0f;
                     } ];
    [_resultsController didMoveToParentViewController:self];
    
    [self onReloadResultController];
}

- (void)hideResultController
{
    isKeywordShow = NO;
    [_searchField resignFirstResponder];
    //    background.hidden = YES;
    
    [_resultsController willMoveToParentViewController:nil];
    _resultsController.view.alpha = 0.0f;
    self.historyAddress = nil;
    //[self.matchedAddress removeAllObjects];
//    [UIView animateWithDuration:0.1
//                     animations:^{
//                         _resultsController.view.alpha = 0.0f;
//                     }
//                     completion:^(BOOL finished) {
//                         //[_resultsController.view removeFromSuperview];
//                         //[_resultsController removeFromParentViewController];
//
//                         self.historyAddress = nil;
//                         [self.matchedAddress removeAllObjects];
//                     }];
}

- (void)onReloadResultController
{
    //_resultsController.view.frame = [self contentRect];
    _resultsController.tableView.frame = _resultsController.view.frame;
    
    [_resultsController.tableView reloadData];
}

#pragma mark - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    // super textfield는 무시
    if (textField.tag == 10) {
        return;
    }
    
//    if ([_searchField.text isEqualToString:@""]) {
//        //[self.matchedAddress addObjectsFromArray:self.historyAddress];
//        [self.matchedAddress removeAllObjects];
//        self.historyAddress = (NSMutableArray*)PLACE_SELECT;
//    }
    
    [self moveMarker];
    
    [self showResultController];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    // super textfield는 무시
    if (textField.tag == 10) {
        return;
    }
    
    //[self hideResultController];
    
    [self moveMarker];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}

- (void)textFieldDidChange:(UITextField *)textField {
    static BOOL isNull = YES; // 검색하는동안 검색어를 모두 지우면 주소검색이 동작하면 않된다.
    // super textfield는 무시
    if (textField.tag == 10) {
        return;
    }
    
    NSString *searchString = textField.text;
    self.historyAddress = nil;
    if(searchString.length==0){
        isNull = YES;
        
        self.historyAddress = (NSMutableArray*)PLACE_SELECT;
        //_searchField.text = @"";
        NSLog(@"_searchField.text:%@",_searchField.text);
        [self.matchedAddress removeAllObjects];
        rowType = 1;
        
        [self onReloadResultController];
        return;
    }
    isNull = NO;
    if(searchString.length>0){
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        INTULocationManager *locMgr = [INTULocationManager sharedInstance];
        [locMgr POIAPICall:searchString
                completion:^(NSDictionary *user, NSString* params, NSString *str, int status) {
                    
                    if (isNull) {
                        return;
                    }
                    
                    if (status==1) {
                        //NSAssert(![NSThread isMainThread], @"This shouldn't be called from the main thread");
                        //NSLog(@"responseDict:%@",user);
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            rowType = 2;
                            // Update the UI
                            if([user[@"poi"] count]){
                                [self.matchedAddress removeAllObjects];
                                NSLog(@"result.count:%d",(int)[user[@"poi"] count]);
                                
                                for(NSDictionary* result in user[@"poi"])
                                {
                                    [self.matchedAddress addObject:result];
                                }
                                
                                NSLog(@"result.count2:%d",(int)self.matchedAddress.count);
                                
                                [self onReloadResultController];
                            }
                            // case of google
                            else {
                                
                            }
                            
                        });
                    }
                }];
    }
}

#pragma mark - Keyboard Notification Method
- (void)keyboardWillShow:(NSNotification*)note{
    NSDictionary *userInfo = note.userInfo;
    CGRect finalKeyboardFrame = [userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    NSTimeInterval animationDuration = [userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    float inputViewFinalYPosition = self.view.bounds.size.height - finalKeyboardFrame.size.height;
    CGRect inputViewFrame = _resultsController.view.frame;
    inputViewFrame.origin.y = _addrView.frame.origin.y + _addrView.frame.size.height;
    inputViewFrame.size.height = inputViewFinalYPosition - inputViewFrame.origin.y;
    
    [UIView animateWithDuration:animationDuration animations:^{
        _resultsController.view.frame = inputViewFrame;
        _resultsController.tableView.frame = _resultsController.view.frame;
    }];
}

- (void)keyboardWillHide:(NSNotification*)note{
    
    NSDictionary *userInfo = note.userInfo;
    NSTimeInterval animationDuration = [userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    CGRect inputViewFrame = _resultsController.view.frame;
    inputViewFrame.size.height = self.view.frame.size.height - inputViewFrame.origin.y;
    inputViewFrame.origin.y = _addrView.frame.origin.y + _addrView.frame.size.height;
    
    if (isKeywordShow) {
        [UIView animateWithDuration:animationDuration animations:^{
            _resultsController.view.frame = inputViewFrame;
            _resultsController.tableView.frame = _resultsController.view.frame;
        }];
    }
    
}

#pragma mark - Private Methods

//- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
//    [_searchField resignFirstResponder];
//    //[_searchField endEditing:YES];
//    //[self.view endEditing:YES];
//    background.hidden = YES;
//
//    self.historyAddress = nil;
//    [self.matchedAddress removeAllObjects];
//}

- (CGRect)contentRect {
    NSInteger rows = self.matchedAddress.count * self->cellHeight;
    
    if (self.historyAddress != nil && self.historyAddress.count) {
        rows = self.historyAddress.count * self->cellHeight + self->cellHeight;
    }
    
    NSLog(@"rows:%ld",(long)rows);
    //return CGRectMake(0, kTextFieldHeight + 44, self.view.bounds.size.width,rows);
    
    if (rows > background.frame.size.height/2) {
        return CGRectMake(0, _addrView.frame.origin.y + _addrView.frame.size.height, self.view.bounds.size.width, background.frame.size.height/2);
    }
    
    
    return CGRectMake(0,
                      _addrView.frame.origin.y + _addrView.frame.size.height,
                      self.view.bounds.size.width,
                      rows);
}

@end

