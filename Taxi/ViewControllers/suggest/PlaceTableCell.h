//
//  OptionTableCell.h
//  OptionTableCell
//

//

#import <UIKit/UIKit.h>

@interface PlaceTableCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *nameLabel;
@property (nonatomic, weak) IBOutlet UILabel *placeLabel;
@property (nonatomic, weak) IBOutlet UILabel *addressLabel;
@property (nonatomic, weak) IBOutlet UIImageView *thumbnailImageView;
@property (nonatomic, weak) IBOutlet UIButton *btnDelete;
@property (nonatomic, weak) IBOutlet UIButton *btnEdit;
@property (nonatomic, assign)  BOOL refFlag;
@property (nonatomic, assign)  BOOL lockFlag;

-(void)stateChanged;
- (void)refresh;

@end
