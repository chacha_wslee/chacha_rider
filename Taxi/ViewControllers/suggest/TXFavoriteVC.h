#import <UIKit/UIKit.h>
#import "TXBaseViewController.h"
#import "TXDestinationsVC.h"

@protocol TXFavoriteVCDelegate <NSObject>
@required
- (void) didFoundAddress:(NSString *) address withLatitute:(double)lat andLongtitute:(double)longi forTarget:(id)target;

@end

@interface TXFavoriteVC : TXBaseViewController <UITableViewDataSource,UITableViewDelegate,TXDestinationsVCDelegate>

@property (nonatomic, strong) NSString *cellIdentifier;
@property (nonatomic, strong) IBOutlet UITableView *tableView;

@property (nonatomic, strong) id _target;
@property (nonatomic, strong) id<TXDestinationsVCDelegate> delegate;

@end
