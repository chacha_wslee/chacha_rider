#if !defined(__has_feature) || !__has_feature(objc_arc)
#error "This file requires ARC support."
#endif

#import "TXFavoriteVC.h"
#import "PlaceTableCell.h"
#import "LGAlertView.h"

@interface TXFavoriteVC ()

@property (nonatomic, strong) NSArray *historyAddress;

@end

@implementation TXFavoriteVC {
    
    UITableViewController *_resultsController;
    
    NSString *_address;
    CLLocationCoordinate2D _coor;
    
    //UITableView *mTableViewSuggest;
    UIView *background;
    
    BOOL _firstLocationUpdate;
    
    UIImageView *mImageFocus;
    
    CGFloat cellHeight;
    
    BOOL isMoveMapBYGesture;
}

@synthesize _target;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    
    self->cellHeight = 62;
    
    PLACE_DELETE_NULL;
    PLACE_DELETE_LIMIT;
    
    
    self.historyAddress = (NSMutableArray*)PLACE_SELECT;
}

/*
- (void)dealloc {
    [self.mapView_ removeObserver:self
                  forKeyPath:@"myLocation"
                     context:NULL];
}
*/

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    isMoveMapBYGesture = NO;
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    // Implement here if the view has registered KVO
}

-(void)configure {
    
    [super configure];
    
    [super configureBottomList];
    UIView *n = [super navigationView];
    NSInteger _y = n.frame.origin.y + n.frame.size.height;
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, _y, self.view.bounds.size.width, self.view.bounds.size.height - _y - kBottomButtonHeight - kBottomBottonSafeArea)
                                                  style:UITableViewStylePlain];
    self.tableView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:self.tableView];
    
    UINib *nib = [UINib nibWithNibName:@"PlaceTableCell" bundle:nil];
    self.cellIdentifier = @"PlaceTableCell";
    [self.tableView registerNib:nib forCellReuseIdentifier:self.cellIdentifier];
    
    self.tableView.separatorColor = [UIColor clearColor];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.opaque = NO;
    self.tableView.backgroundColor = [UIColor clearColor];
    
    //self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, kBasicHeight + kBasicMargin*2 - kBottomButtonHeight)];
    self.tableView.tableFooterView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    
    UIButton *btn = [Utils addBottomButton:LocalizedStr(@"Map.Search.Address.BTN.addplace.text") target:self selector:@selector(onAddButtonClick:) color:YES];
    [self.view addSubview:btn];
    self.tableView.scrollsToTop = YES;
}

-(void)configureStyles {
    [super configureStyles];
    
    UIView *n = [super navigationView];
    UILabel *lb = [n viewWithTag:1210];
    
    [n setBackgroundColor:[UIColor whiteColor]];
    lb.textColor = UIColorDefault2;
    [super navigationType01X:[UIImage imageNamed:@"btn_back"] centerText:LocalizedStr(@"Map.Search.Address.Favorite.Edit.title")];
}

#pragma mark - IBAction
-(IBAction)onNaviButtonClick:(id)sender {
    UIButton *btn = (UIButton *)sender;
    NSLog(@"onNaviButtonClick");
    
    //[self dismissViewControllerAnimated:YES completion:nil];
    if (btn.tag == 1100 || btn.tag == 1101) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

-(void)deleteFavorite:(UIButton*)btn {
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:btn.tag inSection:0];
    NSDictionary *items = [self.historyAddress objectAtIndex:indexPath.row];
    
    if ([items[@"type"] intValue] == 1) {
        PLACE_UPDATE_HOME(@"",@"");
    }
    else if ([items[@"type"] intValue] == 2) {
        PLACE_UPDATE_WORK(@"",@"");
    }
    else if ([items[@"type"] intValue] == 3) {
        PLACE_DELETE_FAVORITE(items[@"location"]);
    }
    else if ([items[@"type"] intValue] == 4) {
        PLACE_DELETE_HISTORY(items[@"location"]);
    }
    
    self.historyAddress = (NSMutableArray*)PLACE_SELECT;
    
    [self.tableView reloadData];
    
}

-(IBAction)onDeleteButtonClick:(id)sender {
    UIButton *btn = (UIButton*)sender;
    NSLog(@"btn.tag:%d",(int)btn.tag);
    
    if (self.historyAddress != nil && self.historyAddress.count) {
        LGAlertView *alertView = [[LGAlertView alloc] initWithTitle:nil
                                                            message:LocalizedStr(@"Map.Search.Address.Favorite.Delete.title")
                                                              style:LGAlertViewStyleAlert
                                                       buttonTitles:@[LocalizedStr(@"Button.OK")]
                                                  cancelButtonTitle:LocalizedStr(@"Button.CANCEL")
                                             destructiveButtonTitle:nil
                                                      actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                                                          [self deleteFavorite:btn];
                                                      }
                                                      cancelHandler:^(LGAlertView *alertView) {
                                                      }
                                                 destructiveHandler:^(LGAlertView *alertView) {
                                                 }];
        [Utils initAlertButtonColor:alertView];
        alertView.windowLevel = LGAlertViewWindowLevelAboveStatusBar;
        [alertView showAnimated:YES completionHandler:nil];
        
    }
}

-(IBAction)onAddButtonClick:(id)sender {
    UIButton *btn = (UIButton*)sender;
    NSLog(@"btn.tag:%d",(int)btn.tag);
    
    TXDestinationsVC *result = [[TXDestinationsVC alloc] initWithNibName:@"TXDestinationsVC" bundle:nil];
    result._target = nil;
    result._step = 11;
    result.destPosition = kCLLocationCoordinate2DInvalid;
    result.delegate = self;
    [self pushViewController:result];
}

-(IBAction)onEditButtonClick:(id)sender {
    UIButton *btn = (UIButton*)sender;
    NSLog(@"btn.tag:%d",(int)btn.tag);
    
    // step 1,2,3,4 UI전환
    CLLocationCoordinate2D ldst__;
    NSDictionary *items = [self.historyAddress objectAtIndex:btn.tag];
    
    ldst__ = [Utils getLocation:items[@"location"]];
    
    if ([GoogleUtils isLocationValid:ldst__]) {
        TXDestinationsVC *result = [[TXDestinationsVC alloc] initWithNibName:@"TXDestinationsVC" bundle:nil];
        result._target = items;
        result._step = 21;
        result.destPosition = ldst__;
        result.delegate = self;
        [self pushViewController:result];
    }
    else {
        // 설정된 정보가 없다.
        TXDestinationsVC *result = [[TXDestinationsVC alloc] initWithNibName:@"TXDestinationsVC" bundle:nil];
        result._target = items;
        result._step = 11;// 지도화면
        result.destPosition = [Utils currentPositionCoor];
        
//        result._target = items;
//        result._step = 310;// 주소검색화면
//        result.destPosition = kCLLocationCoordinate2DInvalid;
        result.delegate = self;
        [self pushViewController:result];
    }
}

#pragma mark - Destination Delegate
- (void) didFoundAddress:(NSString *) address withLatitute:(double)lat andLongtitute:(double)longi forTarget:(id)_target {
    //Move map to address
    //[self goToLocationWithLatitute:lat andLongtitute:longi];
/*
    NSString *loc = [NSString stringWithFormat:@"%@,%@",LOCATION_STR(lat), LOCATION_STR(longi)];
    
    if (_target == nil) {
        PLACE_DELETE_FAVORITE(loc);
        PLACE_INSERT_FAVORITE(loc,address);
    }
    else {
        NSDictionary *item = (NSDictionary*)_target;
        
        NSInteger selectType = [item[@"type"] intValue];
        
        if (selectType == 1) {
            PLACE_UPDATE_HOME(loc,address);
        }
        else if (selectType == 2) {
            PLACE_UPDATE_WORK(loc,address);
        }
        else if (selectType == 3) {
            PLACE_DELETE_FAVORITE(loc);
            PLACE_INSERT_FAVORITE(loc,address);
        }
        else if (selectType == 4) {
            PLACE_DELETE_HISTORY(loc);
            PLACE_INSERT_HISTORY(loc,address);
        }
    }
*/
    self.historyAddress = (NSMutableArray*)PLACE_SELECT;
    
    [self.tableView reloadData];
}

#pragma mark - TableDataSourceDelegate
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [tableView setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [tableView setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
    
    UIView *seperatorView = [[UIView alloc] initWithFrame:CGRectMake(10, cell.frame.size.height - 1, cell.frame.size.width - 10*2, 1)];
    seperatorView.backgroundColor = UIColorTableSeperator;
    [cell addSubview:seperatorView];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 62;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    // This will create a "invisible" footer
    return 0.01f;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [UIView new];
    
    // If you are not using ARC:
    // return [[UIView new] autorelease];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return (NSInteger)(self.historyAddress.count);
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    PlaceTableCell *cell = (PlaceTableCell *)[tableView dequeueReusableCellWithIdentifier:self.cellIdentifier];
//    [cell.contentView setBackgroundColor:[UIColor colorWithRed:0.839 green:0.129 blue:0.129 alpha:1]];
    
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"PlaceTableCell"
                                                     owner:(self)
                                                   options:nil];
        cell = [nib objectAtIndex:0];
    }
    self->cellHeight = cell.frame.size.height;
    
    cell.nameLabel.hidden = NO;
    cell.placeLabel.hidden = YES;
    cell.addressLabel.hidden = YES;
    
    NSDictionary *item = [self.historyAddress objectAtIndex:indexPath.row];
    cell.nameLabel.text = item[@"place"];
    cell.btnDelete.tag = indexPath.row;
    cell.btnEdit.tag = indexPath.row;
    cell.nameLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:16];
    cell.nameLabel.textColor = HEXCOLOR(0x333333FF);
    [cell.btnEdit setImage:[UIImage imageNamed:@"btn_edit_list"] forState:UIControlStateNormal];
    
    if ([item[@"type"] intValue] == 1) {
        cell.thumbnailImageView.image = [UIImage imageNamed:@"ic_favorite_home_gold"];
        if ([[Utils nullToString:item[@"place"]] isEqualToString:@""]) {
            cell.nameLabel.text = LocalizedStr(@"Map.Search.Address.HOME.text");
            cell.nameLabel.textColor = [UIColor grayColor];
            cell.btnDelete.hidden = YES;
        }
        else {
            cell.nameLabel.textColor = [UIColor blackColor];
            cell.btnDelete.hidden = NO;
        }
    }
    else if ([item[@"type"] intValue] == 2) {
        cell.thumbnailImageView.image = [UIImage imageNamed:@"ic_favorite_company_gold"];
        if ([[Utils nullToString:item[@"place"]] isEqualToString:@""]) {
            cell.nameLabel.text = LocalizedStr(@"Map.Search.Address.WORK.text");
            cell.nameLabel.textColor = [UIColor grayColor];
            cell.btnDelete.hidden = YES;
        }
        else {
            cell.nameLabel.textColor = [UIColor blackColor];
            cell.btnDelete.hidden = NO;
        }
    }
    else if ([item[@"type"] intValue] == 3) {
        cell.thumbnailImageView.image = [UIImage imageNamed:@"ic_favorite_gold"];
    }
    else if ([item[@"type"] intValue] == 4) {
        cell.thumbnailImageView.image = [UIImage imageNamed:@"ic_location_history"];
    }
    return cell;

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    NSDictionary *item = [self.historyAddress objectAtIndex:indexPath.row];
    if ([[Utils nullToString:item[@"place"]] isEqualToString:@""]) {
        // 주소수정
        PlaceTableCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        [self onEditButtonClick:cell.btnEdit];
        
        return;
    }
    
    if (!appDelegate.isDriverMode && appDelegate.ustate < 130) {
        NSDictionary *item = [self.historyAddress objectAtIndex:indexPath.row];
        
        CLLocationCoordinate2D coor = [Utils getLocation:item[@"location"]];
        [self.delegate didFoundAddress:item[@"place"] withLatitute:coor.latitude andLongtitute:coor.longitude forTarget:_target];
        
        [self.navigationController popViewControllerAnimated:YES];
    }
    
}

//// Override to support conditional editing of the table view.
//// This only needs to be implemented if you are going to be returning NO
//// for some items. By default, all items are editable.
//- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
//    // Return YES if you want the specified item to be editable.
//    
//    if (self.historyAddress != nil && self.historyAddress.count) {
//        NSDictionary *loc = [self.historyAddress objectAtIndex:indexPath.row];
//        
//        NSString *location = [Utils nullToString:loc[@"location"]];
//        if (indexPath.row == 0 || indexPath.row == 1) {
//            if ([location isEqualToString:@""] || [location isEqualToString:@"0,0"]) {
//                return NO;
//            }
//        }
//        return YES;
//    }
//    
//    return NO;
//}
//
//// Override to support editing the table view.
//- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
//    if (editingStyle == UITableViewCellEditingStyleDelete) {
//        //add code here for when you hit delete
//        
//        if (self.historyAddress != nil && self.historyAddress.count) {
//            NSDictionary *items = [self.historyAddress objectAtIndex:indexPath.row];
//            
//            PLACE_DELETE_HISTORY(items[@"location"]);
//            self.historyAddress = (NSMutableArray*)PLACE_SELECT;
//            
//            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
//            
//        }
//    }
//}


@end
