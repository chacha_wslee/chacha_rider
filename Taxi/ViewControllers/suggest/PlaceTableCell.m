//
//  OptionTableCell.m
//  OptionTableCell
//

//

#import "PlaceTableCell.h"

@implementation PlaceTableCell
@synthesize nameLabel = _nameLabel;
@synthesize placeLabel = _placeLabel;
@synthesize addressLabel = _addressLabel;
@synthesize thumbnailImageView = _thumbnailImageView;
@synthesize btnDelete = _btnDelete;
@synthesize btnEdit = _btnEdit;
@synthesize refFlag = _refFlag;
@synthesize lockFlag = _lockFlag;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        //[_nameLabel setTextColor:HEXCOLOR(0x464646FF)];
    }
    return self;
}

- (void)refresh
{
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)stateChanged
{
    
}

@end
