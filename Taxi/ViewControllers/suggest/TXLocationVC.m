#if !defined(__has_feature) || !__has_feature(objc_arc)
#error "This file requires ARC support."
#endif

#import "TXLocationVC.h"
#import <GooglePlaces/GooglePlaces.h>
#import "PlaceTableCell.h"
#import "LGAlertView.h"

static float   kOverlayMovingTime = 0.5f;

// This demo shows how to manually present a UITableViewController and supply it with autocomplete
// text from an arbitrary source, in this case a UITextField.
//GMSAutocompleteTableDataSourceDelegate,
@interface TXLocationVC ()

@property (nonatomic, strong) NSMutableArray *matchedAddress;
@property (nonatomic, strong) NSArray *historyAddress;

@end

@implementation TXLocationVC {
    
    UITableViewController *_resultsController;
    //GMSAutocompleteTableDataSource *_tableDataSource;
    //UITextView *_resultView;
    
    GMSPlacesClient *_placesClient;
    GMSPlace *_place;
    
    NSString *_address;
    CLLocationCoordinate2D _coor;
    
    //UITableView *mTableViewSuggest;
    UIView *background;
    
    BOOL _firstLocationUpdate;
    //GMSMapView *_mapView;
    GMSMarker *_placeMarker;
    
    CLLocationManager *locationMgr;
    UIImageView *mImageFocus;
    GMSGeocoder *_geocoder;
    
    CGFloat cellHeight;
    NSString *country;
    
    BOOL isMoveMapBYGesture;
    
//    LGAlertView *alertView;
}

@synthesize _target, _step;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor yellowColor];
    
    [GMSPlacesClient provideAPIKey:_GOOGLE_PLACE_API_KEY];
    //https://console.developers.google.com/flows/enableapi?apiid=placesios,maps_ios_backend&keyType=CLIENT_SIDE_IOS
    _placesClient = [GMSPlacesClient sharedClient];
    
    self.matchedAddress = [[NSMutableArray alloc] init];
    self.historyAddress = nil;
    
    _placeMarker = [[GMSMarker alloc] init];
    _geocoder = [[GMSGeocoder alloc] init];
    
    self->cellHeight = 38;
    
    if (appDelegate.isKorea) {
        self->country = @"KR";
    }
    else {
        self->country = [Utils getCountryCode];
    }
    
//    NSString *langID    = [[NSLocale preferredLanguages] objectAtIndex:0];
//    NSArray* foo = [langID componentsSeparatedByString: @"-"];
//    self->country = [foo objectAtIndex:1]; // [0]language [1]national
    
    PLACE_DELETE_LIMIT;
}

-(void)initMapView {
    
//    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:self->locationMgr.location.coordinate.latitude
//                                                            longitude:self->locationMgr.location.coordinate.longitude
//                                                                 zoom:6];
    GMSCameraPosition *camera;
    if (CLLocationCoordinate2DIsValid(_destPosition) && [Utils isLocationValid:_destPosition.latitude long:_destPosition.longitude]) {
        camera = [GMSCameraPosition cameraWithLatitude:_destPosition.latitude
                                             longitude:_destPosition.longitude
                                                  zoom:17];
    }
    else {
        camera = [GMSCameraPosition cameraWithLatitude:appDelegate.currLocation.latitude
                                             longitude:appDelegate.currLocation.longitude
                                                  zoom:14];
    }
    
#if TARGET_IPHONE_SIMULATOR

    if (CLLocationCoordinate2DIsValid(_destPosition) && [Utils isLocationValid:_destPosition.latitude long:_destPosition.longitude]) {
        camera = [GMSCameraPosition cameraWithLatitude:_destPosition.latitude
                                             longitude:_destPosition.longitude
                                                  zoom:17];
    }
    else {
        camera = [GMSCameraPosition cameraWithLatitude:[_GEO_LAT floatValue]
                                             longitude:[_GEO_LAT floatValue]
                                                  zoom:14];
    }
#endif
    
    self.mapView_ = [GMSMapView mapWithFrame:CGRectMake(0, kNaviTopHeight, self.view.bounds.size.width,
                                                   self.view.bounds.size.height - kNaviTopHeight) camera:camera];
    self.mapView_.delegate = self;
    
    self.mapView_.autoresizingMask = UIViewAutoresizingFlexibleWidth |
    UIViewAutoresizingFlexibleHeight |
    UIViewAutoresizingFlexibleBottomMargin;
    
    /*
    // Listen to the myLocation property of GMSMapView.
    [self.mapView_ addObserver:self
               forKeyPath:@"myLocation"
                  options:NSKeyValueObservingOptionNew
                  context:NULL];
    */
    // Ask for My Location data after the map has already been added to the UI.

    self.mapView_.settings.compassButton = NO;
    
    [self showCposButton];
    
    [self.view addSubview:self.mapView_];
}
/*
- (void)dealloc {
    [self.mapView_ removeObserver:self
                  forKeyPath:@"myLocation"
                     context:NULL];
}
*/
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self addObservers];
    
    isMoveMapBYGesture = NO;
    
//#if !TARGET_IPHONE_SIMULATOR
    if ([CLLocationManager locationServicesEnabled]) {
        
        [self startUpdatingLocation];
    }
    else {
        
        [appDelegate onCommonErrorAlert:LocalizedStr(@"Intro.GPS.Error.text")];
//        if ([CLLocationManager authorizationStatus] != kCLAuthorizationStatusAuthorizedAlways) {
//            if ([self->locationMgr respondsToSelector:@selector(requestAlwaysAuthorization)]) {
//                [self->locationMgr requestAlwaysAuthorization];
//            }
//        }
    }
//#endif
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self removeObservers];
    // Implement here if the view has registered KVO
}

#pragma mark KVO
-(void)addObservers {
    [self.mapView_ addObserver:self forKeyPath:@"myLocation" options:NSKeyValueObservingOptionNew context: nil];
}

-(void)removeObservers {
    [self.mapView_ removeObserver:self forKeyPath:@"myLocation"];
}

-(void)configure {
    
    [super configure];
    
    [self initMapView];
    
    CGRect rect = self.mapView_.frame;
    rect.origin.y = 20;
    rect.size.width = self.view.bounds.size.width;
    rect.size.height = self.view.bounds.size.height - rect.origin.y;
    self.mapView_.frame = rect;
    
    [super configureBottomLine];
    UIView  *view = (UIView*)[[super navigationView] viewWithTag:1500];
    view.hidden = YES;
    UIView *n = [super navigationView];
    NSInteger _y = n.frame.origin.y + n.frame.size.height;
    
    // 1. init searchbar
    [self configureSearchBar:_y];
    
    //_y -= 4;
    // 4. init mapview & layout
    _y = [self configureMapView:_y];
    
    _y = self.view.bounds.size.height - _confirmView.frame.size.height;
    _y = [self configureConfirmView:_y];
    
    [self.view bringSubviewToFront:n];
}

-(void)configureStyles {
    [super configureStyles];
    
    UIView *n = [super navigationView];
    UILabel *lb = [n viewWithTag:1210];
    
    _searchField.font = [UIFont fontWithName:@"HelveticaNeue-Regular" size:14];
    // 출발지
    if (_step == 110) {
        [n setBackgroundColor:[UIColor whiteColor]];
        lb.textColor = UIColorDefault2;
        [super navigationType01X:[UIImage imageNamed:@"btn_back"] centerText:LocalizedStr(@"Map.Address.SRC.Select.title")];
        
        _addrView.hidden = NO;
        _confirmView.hidden = YES;
        
        _addrBGView.backgroundColor = UIColorDefault;
        _searchField.textColor = UIColorDefault2;
        [_btnClear setImage:[UIImage imageNamed:@"btn_delete_small"] forState:UIControlStateNormal];
    }
    else if (_step == 120) {
        [n setBackgroundColor:UIColorDefault];
        lb.textColor = UIColorDefault2;
        [super navigationType01X:[UIImage imageNamed:@"btn_back"] centerText:LocalizedStr(@"Map.Address.SRC.Select.title")];
        
        _addrView.hidden = YES;
        _confirmView.hidden = NO;
        
        _addrBGView.backgroundColor = UIColorDefault;
        _searchField.textColor = UIColorDefault2;
        [_btnClear setImage:[UIImage imageNamed:@"btn_delete_small"] forState:UIControlStateNormal];
    }
    else if (_step == 210) {
        [n setBackgroundColor:[UIColor whiteColor]];
        lb.textColor = UIColorDefault2;
        [super navigationType01X:[UIImage imageNamed:@"btn_back"] centerText:LocalizedStr(@"Map.Address.DST.Select.title")];
        
        _addrView.hidden = NO;
        _confirmView.hidden = YES;
        
        _addrBGView.backgroundColor = UIColorDefault2;
        _searchField.textColor = UIColorDefault;
        [_btnClear setImage:[UIImage imageNamed:@"btn_delete_small_color01"] forState:UIControlStateNormal];
    }
    else if (_step == 220) {
        [n setBackgroundColor:UIColorDefault2];
        lb.textColor = UIColorDefault;
        [super navigationType01X:[UIImage imageNamed:@"btn_back_03"] centerText:LocalizedStr(@"Map.Address.DST.Select.title")];
        
        _addrView.hidden = YES;
        _confirmView.hidden = NO;
        
        _addrBGView.backgroundColor = UIColorDefault2;
        _searchField.textColor = UIColorDefault;
        [_btnClear setImage:[UIImage imageNamed:@"btn_delete_small_color01"] forState:UIControlStateNormal];
    }
    
    _btnConfirm.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:18];
    [_btnConfirm setTitle:LocalizedStr(@"Map.Search.Address.BTN.confirm.text") forState:UIControlStateNormal];
    [_btnConfirm setTitleColor:UIColorDefault forState:UIControlStateNormal];
}

-(NSInteger) configureSearchBar:(NSInteger)_y {
    
//    _searchField.frame = CGRectMake(0,
//                                    _y,
//                                    self.view.bounds.size.width,
//                                    _searchField.frame.size.height);
    _addrView.frame = CGRectMake(0,
                                    _y,
                                    self.view.bounds.size.width,
                                    _addrView.frame.size.height);
    
    _searchField.autoresizingMask = UIViewAutoresizingFlexibleWidth;
//    _searchField.borderStyle = UITextBorderStyleNone;
    _searchField.adjustsFontSizeToFitWidth = YES;
    _searchField.placeholder = LocalizedStr(@"Map.Address.placeholder.text");
    _searchField.autocorrectionType = UITextAutocorrectionTypeNo;
    _searchField.keyboardType = UIKeyboardTypeDefault;
    _searchField.returnKeyType = UIReturnKeyDone;
    _searchField.clearButtonMode = UITextFieldViewModeNever;
    _searchField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    
    [_searchField addTarget:self
                     action:@selector(textFieldDidChange:)
           forControlEvents:UIControlEventEditingChanged];
    _searchField.delegate = self;
    
    [self.view addSubview:_addrView];
    
    
    // search result tableview
    _resultsController = [[UITableViewController alloc] initWithStyle:UITableViewStylePlain];
    _resultsController.tableView.delegate = self;
    _resultsController.tableView.dataSource = self;
    _resultsController.tableView.backgroundColor = [UIColor clearColor];
    _resultsController.tableView.separatorColor = [UIColor clearColor];
    
    //mTableViewSuggest = [[UITableView alloc] initWithFrame:[self contentRect]];
    //mTableViewSuggest.delegate = self;
    //mTableViewSuggest.dataSource = self;
    background.hidden=YES;

    //[mTableViewSuggest setHidden:YES];
    //mTableViewSuggest.backgroundColor =[UIColor whiteColor];
    background=[[UIView alloc] initWithFrame: CGRectMake ( 0,
                                                          kNaviTopHeight + kBasicHeight,
                                                          self.view.bounds.size.width,
                                                          self.view.bounds.size.height - kNaviTopHeight - kBasicHeight)];
    background.backgroundColor =[UIColor colorWithRed:.1 green:.1 blue:.1 alpha: .4];
    [self.view addSubview:background];
    //[self.view addSubview:mTableViewSuggest];
    background.hidden=YES;
    
    return (background.frame.origin.y);
}

-(NSInteger) configureConfirmView:(NSInteger)_y {
    
    _confirmView.frame = CGRectMake(0,
                                    _y,
                                    self.view.bounds.size.width,
                                    _confirmView.frame.size.height);
    
    CAGradientLayer *layer = [CAGradientLayer layer];
    layer.frame = _addrTextView.bounds;
    UIColor *theColor = [UIColor whiteColor];
    layer.colors = [NSArray arrayWithObjects:
                    (id)[[UIColor clearColor] CGColor],
                    (id)[[UIColor clearColor] CGColor],
                    (id)[[theColor colorWithAlphaComponent:0.0f] CGColor],
                    (id)[[theColor colorWithAlphaComponent:0.1f] CGColor],
                    (id)[[theColor colorWithAlphaComponent:0.3f] CGColor],
                    (id)[[theColor colorWithAlphaComponent:0.5f] CGColor],
                    (id)[[theColor colorWithAlphaComponent:0.6f] CGColor],
                    (id)[[theColor colorWithAlphaComponent:0.9f] CGColor],
                    (id)[theColor CGColor],
                    nil];
    
    [_addrTextView.layer insertSublayer:layer atIndex:0];
    _confirmView.hidden = YES;
    
    [self.view addSubview:_confirmView];
    
    return (_confirmView.frame.origin.y);
}

-(NSInteger) configureMapView:(NSInteger)_y {
    
    CGRect rect = self.mapView_.frame;
//    rect.origin.y = _y;
//    rect.size.width = self.view.bounds.size.width;
//    rect.size.height= self.view.bounds.size.height - _y;
    
    // 1.MAP view
    [self.mapView_ setFrame:rect];
    
    CGPoint point = [self getCenterPointMapView:rect];
    rect = CGRectMake(point.x, point.y, kMarkerSizeW,kMarkerSizeH);
    
    mImageFocus = [[UIImageView alloc] initWithFrame:rect];
    mImageFocus.contentMode = UIViewContentModeScaleAspectFit;
    //    mImageFocus.image = [Utils image:[UIImage imageNamed:@"pinHome"] scaledToSize:CGSizeMake(50.0f, 50.0f)];
    
    UIButton *btn = (UIButton*)_target;
    
    // 출발지
    if (btn.tag == 100) {
        mImageFocus.image = [Utils image:[UIImage imageNamed:@"icon_pin_start"] scaledToSize:CGSizeMake(kMarkerSizeW, kMarkerSizeH)];
    }
    else {
        mImageFocus.image = [Utils image:[UIImage imageNamed:@"icon_pin_end"] scaledToSize:CGSizeMake(kMarkerSizeW, kMarkerSizeH)];
    }
    [self.mapView_ addSubview:mImageFocus];
    
    [self markerWillMove];

    return (self.mapView_.frame.origin.y + self.mapView_.frame.size.height);
}

-(CGPoint)getCenterPointMapView:(CGRect)frame {
    CGPoint point = CGPointZero;
    point.x = frame.size.width/2;
    point.y = frame.size.height/2;
    point.x = point.x - kMarkerSizeW/2;
    point.y = point.y - kMarkerSizeH;
    
    return point;
}

-(void)markerWillMove {
    CGRect rect = self.mapView_.frame;
    UIEdgeInsets padding = self.mapView_.padding;
    
    rect.size.height -= padding.bottom;
    CGPoint point = [self getCenterPointMapView:rect];
    rect = CGRectMake(point.x, point.y, kMarkerSizeW,kMarkerSizeH);
    
    mImageFocus.frame = rect;
}

-(void)moveMarker {
    CGRect rect = self.mapView_.frame;
    rect.size.height = self.view.bounds.size.height - rect.origin.y;
    
    CGRect rect2 = mImageFocus.frame;
    CGPoint point = [self getCenterPointMapView:rect];
    rect2.origin = point;
    
    [UIView animateWithDuration:0.2
                     animations:^{
                         self.mapView_.frame = rect;
                         mImageFocus.frame = rect2;
                     } ];
}


-(void)startUpdatingLocation {
    self->locationMgr = [[CLLocationManager alloc] init];
    self->locationMgr.delegate = self;

    if (appDelegate.isDriverMode) {
        // 항상 위치 정보 사용 요청
//        if ([self->locationMgr respondsToSelector:@selector(requestAlwaysAuthorization)]) {
//            [self->locationMgr requestAlwaysAuthorization];
//        }
        if ([self->locationMgr respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
            [self->locationMgr requestWhenInUseAuthorization];
        }
    }
    else {
        // 사용중일때만 요청
        if ([self->locationMgr respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
            [self->locationMgr requestWhenInUseAuthorization];
        }
    }
    
    self->locationMgr.distanceFilter = kCLDistanceFilterNone;
    self->locationMgr.desiredAccuracy = kCLLocationAccuracyBest;
    [self->locationMgr startUpdatingLocation];
    
    self.mapView_.myLocationEnabled = YES;
    //self.mapView_.settings.myLocationButton = YES;
}

- (void)updateMarker:(NSString*)title withPosition:(CLLocationCoordinate2D)coordinate {
    _placeMarker.title = title;
    _placeMarker.position = CLLocationCoordinate2DMake(coordinate.latitude, coordinate.longitude);
    _placeMarker.appearAnimation = kGMSMarkerAnimationPop;
    _placeMarker.flat = YES;
    _placeMarker.draggable = NO;
    _placeMarker.groundAnchor = CGPointMake(0.5, 0.5);
    //    australiaMarker.icon = [UIImage imageNamed:@"australia"];
    _placeMarker.map = self.mapView_;
}

//- (void)getAddressFromLatLonByAppleAPI:(CLLocationCoordinate2D)coor {
//    //CGPoint point = self.mapView_.center;
//    //CLLocationCoordinate2D coor = [self.mapView_.projection coordinateForPoint:point];
//    
//    CLGeocoder *ceo = [[CLGeocoder alloc]init];
//    CLLocation *loc = [[CLLocation alloc]initWithLatitude:coor.latitude longitude:coor.longitude]; //insert your coordinates
//    [ceo reverseGeocodeLocation:loc
//              completionHandler:^(NSArray *placemarks, NSError *error) {
//                  CLPlacemark *placemark = [placemarks objectAtIndex:0];
//                  NSLog(@"location %@",placemark.name);
//                  NSLog(@"location %@",placemark.location);
//                  NSString *locatedAt = [[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "];
//                  NSLog(@"location %@",locatedAt);
//              }
//     ];
//}

-(void)getAddressFromLatLon2:(NSString*)coorString
{
    CLLocationCoordinate2D coor = [Utils getLocation:coorString];
    [self getAddressFromLatLon:coor];
}

-(void)getAddressFromLatLon:(CLLocationCoordinate2D)coor
{
    GMSReverseGeocodeCallback handler = ^(GMSReverseGeocodeResponse *response, NSError *error) {
        GMSAddress *address = response.firstResult;
        if (address) {
            NSLog(@"Geocoder result: %@", address);
            
//            if ([address.lines count] > 0) {
//                _searchField.text = [address.lines firstObject];
//            }
            
            _searchField.text = [Utils addressToString:address];//address.thoroughfare;
            NSLog(@"_searchField.text:%@,%@",_searchField.text,address.thoroughfare);
            
            if ([_searchField.text isEqualToString:@""]) {
                _coor = kCLLocationCoordinate2DInvalid;
                _searchField.text = LocalizedStr(@"Address.None.text");
                _address = _searchField.text;
            }
            else {
                _coor = coor;
                _address = _searchField.text;
                //_address = address.thoroughfare;
            }

        } else {
            NSLog(@"Could not reverse geocode point (%f,%f): %@",
                  coor.latitude, coor.longitude, error);
            
            dispatch_async(dispatch_get_main_queue(), ^(void){
                __block LGAlertView *alertView = [[LGAlertView alloc] initWithTitle:@""
                                                                            message:LocalizedStr(@"Intro.GPS.Fetch.Error.text")
                                                                              style:LGAlertViewStyleAlert
                                                                       buttonTitles:@[LocalizedStr(@"Button.OK")]
                                                                  cancelButtonTitle:nil
                                                             destructiveButtonTitle:nil
                                                                      actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                                                                          [self performSelector:@selector(getAddressFromLatLon2:) withObject:[Utils formattedPosition:coor] afterDelay:0.1];
                                                                      }
                                                                      cancelHandler:^(LGAlertView *alertView) {
                                                                          NSLog(@"cancelHandler");
                                                                      }
                                                                 destructiveHandler:^(LGAlertView *alertView) {
                                                                     NSLog(@"destructiveHandler");
                                                                 }];
                [Utils initAlertButtonColor:alertView];
                [alertView showAnimated:YES completionHandler:^(void)
                 {
                     dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                         [alertView dismissAnimated:YES completionHandler:nil];
                         [self performSelector:@selector(getAddressFromLatLon2:) withObject:[Utils formattedPosition:coor] afterDelay:0.1];
                     });
                 }];
            });
        }
    };
    [_geocoder reverseGeocodeCoordinate:coor completionHandler:handler];

}

#pragma mark - Button - CurrentPosition
-(NSString*)currentPosition {
    // 현재 위치
    NSString *lat;
    NSString *lng;
#if TARGET_IPHONE_SIMULATOR
    
    CLLocationCoordinate2D coord = CLLocationCoordinate2DMake([_GEO_LAT doubleValue], [_GEO_LNG doubleValue]);
    
    lat = LOCATION_STR(coord.latitude);
    lng = LOCATION_STR(coord.longitude);
    
#else
    
    //CLLocation *location = self.mapView_.myLocation;
    CLLocation *location = self.locationMgr.location;
    if (location) {
        lat = LOCATION_STR(location.coordinate.latitude);
        lng = LOCATION_STR(location.coordinate.longitude);
    }
#endif

    return [NSString stringWithFormat:@"%@,%@",lat,lng];
}

-(BOOL)checkCposButton {
    TXButton* btnCpos = [self.mapView_ viewWithTag:801];
    if (!btnCpos || btnCpos == (id)[NSNull null]) {
        return NO;
    }
    
    return YES;
}

-(void)hideCposButton {
    TXButton* btnCpos = [self.mapView_ viewWithTag:801];
    [btnCpos removeFromSuperview];
}

-(void)showCposButton {
    
    TXButton* btnCpos = [self.mapView_ viewWithTag:801];
    if (btnCpos != nil) {
        return;
    }
    
    btnCpos = [[TXButton alloc]
               initWithFrame:CGRectMake(self.mapView_.bounds.size.width - kBasicMargin - 59,
                                        85,
                                        59, 59)];
    
    //[btnNavi setBackgroundColor:colorFromRGB(102, 0, 102, 1)];
    [btnCpos addTarget:self action:@selector(pushCpos) forControlEvents:UIControlEventTouchUpInside];
    [btnCpos setTag:801];
    
    [self.mapView_ addSubview:btnCpos];
    [self pushCpos];
    
    CLLocationCoordinate2D cloc = [Utils getLocation:[self currentPosition]];
    CLLocationCoordinate2D coor = [self.mapView_.projection coordinateForPoint:self.mapView_.center];
    
    if (cloc.longitude == coor.latitude && cloc.longitude == coor.longitude) {
        [self updateCposImage:YES];
    }
    else {
        [self updateCposImage:NO];
    }
}

-(void)updateCposImage:(BOOL)isMyLoc
{
    TXButton* btnCpos = [self.mapView_ viewWithTag:801];
    if (isMyLoc)
        [btnCpos setImage:[UIImage imageNamed:@"btn_location_on"] forState:UIControlStateNormal];
    else {
        [btnCpos setImage:[UIImage imageNamed:@"btn_location"] forState:UIControlStateNormal];
    }
}

-(void)pushCpos {
    
#if TARGET_IPHONE_SIMULATOR
    
    CLLocationCoordinate2D coord = CLLocationCoordinate2DMake([_GEO_LAT doubleValue], [_GEO_LNG doubleValue]);
    
    [self.mapView_ animateToLocation:coord];
    
    [self updateCposImage:YES];
#else
    
    //CLLocation *location = self.mapView_.myLocation;
    CLLocation *location = self.locationMgr.location;
    if (location) {
        appDelegate.currLocation = location.coordinate;
        [self.mapView_ animateToLocation:location.coordinate];
        
        NSString *lat = LOCATION_STR(location.coordinate.latitude);
        NSString *lng = LOCATION_STR(location.coordinate.longitude);
    }

    [self updateCposImage:YES];
#endif
}


#pragma mark - IBAction
-(IBAction)onNaviButtonClick:(id)sender {
    UIButton *btn = (UIButton *)sender;
    NSLog(@"onNaviButtonClick");
    
    //[self dismissViewControllerAnimated:YES completion:nil];
    if (btn.tag == 1100 || btn.tag == 1101) {
        appDelegate.isInitRouteNo = YES;
//        [self.navigationController popViewControllerAnimated:YES];
    }
    else if (btn.tag == 1300) {
        // 주소를 찾지 못했으면 처리하지 않는다.
        if ([_searchField.text isEqualToString:@""]) {
            return;
        }
        if ( [self.delegate respondsToSelector:@selector(didFoundAddress:withLatitute:andLongtitute:forTarget:)] ) {
            
            if (![Utils isLocationValid:_coor.latitude long:_coor.longitude]) {
                
                [self alertError:@"" message:LocalizedStr(@"Address.None.text")];
                return;
            }
            
            UITextField *textfield = (UITextField*)_target;
            if (![textfield.text isEqualToString:_searchField.text]) {
                _address = _searchField.text;
                NSString *loc = [NSString stringWithFormat:@"%f,%f",_coor.latitude, _coor.longitude];
                PLACE_DELETE_HISTORY(loc);
                PLACE_INSERT_HISTORY(loc,_searchField.text);
                [self.delegate didFoundAddress:_address withLatitute:_coor.latitude andLongtitute:_coor.longitude forTarget:_target];
            }
        }
    }
    //[self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
    //[[self navigationController] popViewControllerAnimated:YES];
}

-(IBAction)onClearButtonClick:(id)sender {
    
    self.historyAddress = (NSMutableArray*)PLACE_SELECT;
    
    _searchField.text = @"";
    NSLog(@"_searchField.text:%@",_searchField.text);
    [self.matchedAddress removeAllObjects];
    
    [self onReloadResultController];
}

-(IBAction)onDeleteButtonClick:(id)sender {
    UIButton *btn = (UIButton*)sender;
    NSLog(@"btn.tag:%d",(int)btn.tag);
    if (self.historyAddress != nil && self.historyAddress.count) {
        
        //NSIndexPath *indexPath = [NSIndexPath indexPathForRow:btn.tag inSection:0];
        NSDictionary *items = [self.historyAddress objectAtIndex:btn.tag];
        
//        NSArray *deleteIndexPaths = [[NSArray alloc] initWithObjects:
//                                     [NSIndexPath indexPathForRow:btn.tag inSection:0],
//                                     nil];
        
        if ([items[@"type"] intValue] == 1) {
            PLACE_UPDATE_HOME(items[@"location"],@"");
        }
        else if ([items[@"type"] intValue] == 2) {
            PLACE_UPDATE_WORK(items[@"location"],@"");
        }
        else if ([items[@"type"] intValue] == 3) {
            PLACE_DELETE_FAVORITE(items[@"location"]);
        }
        else if ([items[@"type"] intValue] == 4) {
            PLACE_DELETE_HISTORY(items[@"location"]);
        }
        
        self.historyAddress = (NSMutableArray*)PLACE_SELECT;
        
        //[_resultsController.tableView reloadData];
        
        [self onReloadResultController];
        
//        [_resultsController.tableView  beginUpdates];
//        //[self.tableView insertRowsAtIndexPaths:insertIndexPaths withRowAnimation:UITableViewRowAnimationFade];
//        [_resultsController.tableView  deleteRowsAtIndexPaths:deleteIndexPaths withRowAnimation:UITableViewRowAnimationAutomatic];
//        [_resultsController.tableView  endUpdates];
        
        
        
        //PlaceTableCell *cell = [_resultsController.tableView cellForRowAtIndexPath:indexPath];
        
        //[_resultsController.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
}

-(IBAction)onAddButtonClick:(id)sender {
    //PLACE_INSERT(@"123,430",@"3",@"3");
    
    if (![Utils isLocationValid:_coor.latitude long:_coor.longitude]) {
        
        [self alertError:@"" message:LocalizedStr(@"Address.None.text")];
        return;
    }
    
    __block LGAlertView *alertView1 = [[LGAlertView alloc] initWithTitle:nil
                                                         message:nil
                                                           style:LGAlertViewStyleActionSheet
                                                    buttonTitles:@[LocalizedStr(@"Map.Search.Address.HOME.text"), LocalizedStr(@"Map.Search.Address.WORK.text"), LocalizedStr(@"Map.Search.Address.FAVORITE.text")]
                                               cancelButtonTitle:nil
                                          destructiveButtonTitle:nil
                                                   actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                                                       NSLog(@"actionHandler, %@, %lu", title, (long unsigned)index);
                                                       __block NSInteger selectType = index+1;
                                                       
                                                       [self updatePlaceInfo:title type:selectType];
                                                       
                                                   }
                                                   cancelHandler:^(LGAlertView *alertView) {
                                                       NSLog(@"cancelHandler");
                                                   }
                                              destructiveHandler:^(LGAlertView *alertView) {
                                                  NSLog(@"destructiveHandler");
                                              }];
    [alertView1 showAnimated:YES completionHandler:nil];
    
    //[alertView setButtonAtIndex:0 enabled:NO];
    //[alertView2 showAnimated:YES completionHandler:nil];
}

- (void)updatePlaceInfo:(NSString*)title type:(NSInteger)selectType
{
    LGAlertView *alertView2 = [[LGAlertView alloc] initWithTextFieldsAndTitle:title
                                                                      message:nil
                                                           numberOfTextFields:1
                                                       textFieldsSetupHandler:^(UITextField *textField, NSUInteger index)
                               {
                                   if (index == 0) {
                                       textField.placeholder = LocalizedStr(@"Map.Search.Address.Add.placeholder.text");
                                       textField.text = @"";
                                   }
                                   
                                   textField.tag = index;
                                   //textField.delegate = self;
                                   textField.enablesReturnKeyAutomatically = YES;
                                   textField.autocapitalizationType = NO;
                                   textField.autocorrectionType = NO;
                                   textField.clearButtonMode = UITextFieldViewModeNever;
                                   textField.textAlignment = NSTextAlignmentCenter;
                               }
                                                                 buttonTitles:@[LocalizedStr(@"Button.OK")]
                                                            cancelButtonTitle:LocalizedStr(@"Button.CANCEL")
                                                       destructiveButtonTitle:nil
                                                                actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                                                                    UITextField *secondTextField = alertView.textFieldsArray[index];
                                                                    NSString *loc = [NSString stringWithFormat:@"%f,%f",_coor.latitude, _coor.longitude];
                                                                    NSLog(@"loc:%@",loc);
                                                                    
                                                                    if ([Utils isLocationValid:_coor.latitude long:_coor.longitude] && ![[Utils nullToString:secondTextField.text] isEqualToString:@""]) {
                                                                        if (selectType == 1) {
                                                                            PLACE_UPDATE_HOME(loc,secondTextField.text);
                                                                        }
                                                                        else if (selectType == 2) {
                                                                            PLACE_UPDATE_WORK(loc,secondTextField.text);
                                                                        }
                                                                        else if (selectType == 3) {
                                                                            PLACE_DELETE_FAVORITE(loc);
                                                                            PLACE_INSERT_FAVORITE(loc,secondTextField.text);
                                                                        }
                                                                    }

                                                                    
                                                                }
                                                                cancelHandler:^(LGAlertView *alertView) {
                                                                    NSLog(@"cancelHandler");
                                                                    
                                                                }
                                                           destructiveHandler:^(LGAlertView *alertView) {
                                                               NSLog(@"destructiveHandler");
                                                           }];
    //[alertView1 transitionToAlertView:alertView2 completionHandler:nil];
    [Utils initAlertButtonColor:alertView2];
    [alertView2 showAnimated:YES completionHandler:nil];
}


#pragma mark - GMSMapViewDelegate

- (void)mapView:(GMSMapView *)mapView willMove:(BOOL)gesture
{
    if (!gesture) {
        return;
    }
    isMoveMapBYGesture = gesture;
    
    if (_placeMarker.map) {
        [self.mapView_ clear];
        _placeMarker.appearAnimation = kGMSMarkerAnimationNone;
        _placeMarker.map = nil;
    }
    _searchField.text = @"";
    
    //NSLog(@"willMove");
    [UIView animateWithDuration:1.0 animations:^{
        //mImageFocus.alpha = 0.0f;
    }];
    

    [[NSOperationQueue mainQueue] addOperationWithBlock:^ {
        [UIView animateWithDuration:kOverlayMovingTime animations:^{
            // 상단
            UIView *n = [super navigationView];
            UIView *navigationView = [n viewWithTag:60];
            CGRect rect1 = navigationView.frame;
            rect1.origin.y = -(rect1.size.height - kTopHeight);
            navigationView.frame = rect1;
            
            rect1 = _addrView.frame;
            rect1.origin.y = -(rect1.size.height - kTopHeight);
            _addrView.frame = rect1;
            
            //[[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationFade];
            
            // 하단.
            CGRect rect4 = _confirmView.frame;
            rect4.origin.y = self.view.frame.size.height - _addrTextView.frame.size.height;
            _confirmView.frame = rect4;
            
            // mylocation
            TXButton* btnCpos = [self.mapView_ viewWithTag:801];
            CGRect rect5 = btnCpos.frame;
            rect5.origin.y = self.mapView_.frame.size.height - btnCpos.frame.size.height - kBasicMargin;
            btnCpos.frame = rect5;
            
            [self updateCposImage:NO];
        }];
    }];
}

- (void)mapView:(GMSMapView *)mapView didChangeCameraPosition:(GMSCameraPosition *)position {
    //NSLog(@"didChangeCameraPosition geocode point (%f,%f)",position.target.latitude, position.target.longitude);
}

- (void)mapView:(GMSMapView *)mapView idleAtCameraPosition:(GMSCameraPosition *)camera {
    
    if (isMoveMapBYGesture) {
        
        [UIView animateWithDuration:kOverlayMovingTime animations:^{
            
            // 상단
            UIView *n = [super navigationView];
            NSInteger _y = n.frame.origin.y + n.frame.size.height;
            UIView *navigationView = [n viewWithTag:60];
            CGRect rect1 = navigationView.frame;
            
            rect1.origin.y = kTopHeight;
            navigationView.frame = rect1;
            
            rect1 = _addrView.frame;
            rect1.origin.y = navigationView.frame.origin.y + navigationView.frame.size.height;
            _addrView.frame = rect1;
            
            //[[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];
            
            // 하단.
            CGRect rect4 = _confirmView.frame;
            rect4.origin.y = self.view.frame.size.height - rect4.size.height;
            _confirmView.frame = rect4;
            
            // mylocation
            TXButton* btnCpos = [self.mapView_ viewWithTag:801];
            CGRect rect5 = btnCpos.frame;
            rect5.origin.y = self.mapView_.frame.size.height - btnCpos.frame.size.height - kBasicMargin;
            btnCpos.frame = rect5;
        }];
    }
    
    if (appDelegate.isDriverMode && isMoveMapBYGesture) {
        
    }
    else {
        isMoveMapBYGesture = NO;
    }
    
    [self getAddressFromLatLon:camera.target];
    //[self getAddressFromLatLonByAppleAPI:camera.target];
    
    [UIView animateWithDuration:0.5 animations:^{
        mImageFocus.alpha = 1.0f;
    }];
    
    NSLog(@"geocode point (%f,%f)",camera.target.latitude, camera.target.longitude);
}


- (void)mapView:(GMSMapView *)mapView didLongPressAtCoordinate:(CLLocationCoordinate2D)coordinate {
    // On a long press, reverse geocode this location.
    NSLog(@"geocode point (%f,%f)",
          coordinate.latitude, coordinate.longitude);
    
    //[self updateMarker:@"" withPosition:coordinate];
    
}

#pragma mark - CLLocationManager delegate
// location 초기화
-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    
    
    CLLocation *location = [manager location];
    if (CLLocationCoordinate2DIsValid(_destPosition) && [Utils isLocationValid:_destPosition.latitude long:_destPosition.longitude]) {
        [self.mapView_ animateToLocation:_destPosition];
    }
    else {
        [self.mapView_ animateToLocation:location.coordinate];
    }
    
    [self->locationMgr stopUpdatingLocation];
    NSLog(@"1didUpdateToLocation: %@", location);
    
    //    [self deviceLocation];
    //[self getAddressFromLatLon:location.coordinate.latitude withLongitude:location.coordinate.longitude];
}

-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways) {
        [self->locationMgr startUpdatingLocation];
        self.mapView_.myLocationEnabled = YES;
        //self.mapView_.settings.myLocationButton = YES;
    }
}

#pragma mark - KVO updates

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context {
    if (!_firstLocationUpdate) {
        // If the first location update has not yet been recieved, then jump to that
        // location.
        _firstLocationUpdate = YES;
        
        CLLocation *location = [change objectForKey:NSKeyValueChangeNewKey];
        if (CLLocationCoordinate2DIsValid(_destPosition) && [Utils isLocationValid:_destPosition.latitude long:_destPosition.longitude]) {
            self.mapView_.camera = [GMSCameraPosition cameraWithTarget:_destPosition
                                                                  zoom:17];
        }
        else {
            self.mapView_.camera = [GMSCameraPosition cameraWithTarget:location.coordinate
                                                                  zoom:14];
        }
        
        
//        CLLocation *location = [object valueForKeyPath:keyPath];
//        self.mapView_.settings.myLocationButton = YES;
//        self.mapView_.camera = [GMSCameraPosition cameraWithTarget:location.coordinate
//                                                              zoom:14];
    }
}

/*
#pragma mark - GMSAutocompleteTableDataSourceDelegate

- (void)tableDataSource:(GMSAutocompleteTableDataSource *)tableDataSource
didAutocompleteWithPlace:(GMSPlace *)place {
    [_searchField resignFirstResponder];
    NSMutableAttributedString *text =
    [[NSMutableAttributedString alloc] initWithString:[place description]];
    if (place.attributions) {
        [text appendAttributedString:[[NSAttributedString alloc] initWithString:@"\n\n"]];
        [text appendAttributedString:place.attributions];
    }
    _resultView.attributedText = text;
    _searchField.text = place.name;
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:place.coordinate.latitude longitude:place.coordinate.longitude zoom:15.5];
    [self.mapView_ animateToCameraPosition:camera];
    
    //[self updateMarker:place.name withPosition:place.coordinate];
    
}

- (void)tableDataSource:(GMSAutocompleteTableDataSource *)tableDataSource
didFailAutocompleteWithError:(NSError *)error {
    [_searchField resignFirstResponder];
    _resultView.text =
    [NSString stringWithFormat:@"Autocomplete failed with error: %@", error.localizedDescription];
    _searchField.text = @"";
}

- (void)didRequestAutocompletePredictionsForTableDataSource:
(GMSAutocompleteTableDataSource *)tableDataSource {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    [_resultsController.tableView reloadData];
}

- (void)didUpdateAutocompletePredictionsForTableDataSource:
(GMSAutocompleteTableDataSource *)tableDataSource {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [_resultsController.tableView reloadData];
}


#pragma mark - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    [self addChildViewController:_resultsController];
    _resultsController.view.frame = [self contentRect];
    _resultsController.view.alpha = 0.0f;
    [self.view addSubview:_resultsController.view];
    [_resultsController.tableView reloadData];
    [self.view layoutIfNeeded];
    [UIView animateWithDuration:0.5
                     animations:^{
                         _resultsController.view.alpha = 1.0f;
                     } ];
    [_resultsController didMoveToParentViewController:self];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [_resultsController willMoveToParentViewController:nil];
    [UIView animateWithDuration:0.5
                     animations:^{
                         _resultsController.view.alpha = 0.0f;
                     }
                     completion:^(BOOL finished) {
                         [_resultsController.view removeFromSuperview];
                         [_resultsController removeFromParentViewController];
                     }];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}

#pragma mark - Private Methods

- (void)textFieldDidChange:(UITextField *)textField {
    [_tableDataSource sourceTextHasChanged:textField.text];
}
*/

#pragma mark - TableDataSourceDelegate
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [tableView setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [tableView setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
    
    UIView *seperatorView = [[UIView alloc] initWithFrame:CGRectMake(0, cell.frame.size.height - 1, cell.frame.size.width, 1)];
    seperatorView.backgroundColor = UIColorBasicBack;
    [cell addSubview:seperatorView];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 40;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    // This will create a "invisible" footer
    return 0.01f;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [UIView new];
    
    // If you are not using ARC:
    // return [[UIView new] autorelease];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (self.historyAddress != nil && self.historyAddress.count) {
        return (NSInteger)(self.historyAddress.count+1);
    }
    return (NSInteger)self.matchedAddress.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *simpleTableIdentifier = @"PlaceTableCell";
    
    //UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    PlaceTableCell *cell = (PlaceTableCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    [cell.contentView setBackgroundColor:[UIColor colorWithRed:0.839 green:0.129 blue:0.129 alpha:1]];
    
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"PlaceTableCell"
                                                     owner:(self)
                                                   options:nil];
        cell = [nib objectAtIndex:0];
    }
    self->cellHeight = cell.frame.size.height;
//    NSLog(@"cell.height:%d",(int)cell.frame.size.height);
    /*
    CGFloat separatorInset; // Separator x position
    CGFloat separatorHeight;
    CGFloat separatorWidth;
    CGFloat separatorY;
    UIImageView *separator;
    UIColor *separatorBGColor;
    
    separatorY      = cell.frame.size.height;
    separatorHeight = (1.0 / [UIScreen mainScreen].scale);  // This assures you to have a 1px line height whatever the screen resolution
    separatorWidth  = cell.frame.size.width;
    separatorInset  = 15.0f;
    separatorBGColor  = [UIColor colorWithRed: 204.0/255.0 green: 204.0/255.0 blue: 204.0/255.0 alpha:1.0];
    
    separator = [[UIImageView alloc] initWithFrame:CGRectMake(separatorInset, separatorY, separatorWidth,separatorHeight)];
    separator.backgroundColor = separatorBGColor;
    [cell addSubview:separator];
    */
    
    if (self.historyAddress != nil && self.historyAddress.count) {
        // 마지막이면 기록삭제를 넣는다.
        if ((self.historyAddress.count) == indexPath.row) {
            cell.nameLabel.text = @"기록삭제";
            cell.nameLabel.textAlignment = NSTextAlignmentCenter;
            cell.btnDelete.tag = indexPath.row;
            cell.thumbnailImageView.hidden = YES;
            cell.btnDelete.hidden = YES;
            
            return cell;
        }
        
        NSDictionary *item = [self.historyAddress objectAtIndex:indexPath.row];
        cell.nameLabel.text = item[@"place"];
        cell.btnDelete.tag = indexPath.row;
        if ([item[@"type"] intValue] == 1) {
            cell.thumbnailImageView.image = [UIImage imageNamed:@"ic_favorite_home_gold"];
            if ([[Utils nullToString:item[@"place"]] isEqualToString:@""]) {
                cell.nameLabel.text = @"home";
                cell.btnDelete.hidden = YES;
            }
//            cell.btnDelete.hidden = YES;
        }
        else if ([item[@"type"] intValue] == 2) {
            cell.thumbnailImageView.image = [UIImage imageNamed:@"ic_favorite_company_gold"];
            if ([[Utils nullToString:item[@"place"]] isEqualToString:@""]) {
                cell.nameLabel.text = @"work";
                cell.btnDelete.hidden = YES;
            }
//            cell.btnDelete.hidden = YES;
        }
        else if ([item[@"type"] intValue] == 3) {
            cell.thumbnailImageView.image = [UIImage imageNamed:@"ic_favorite_gold"];
        }
        else if ([item[@"type"] intValue] == 4) {
            cell.thumbnailImageView.image = [UIImage imageNamed:@"ic_location_history"];
        }
        return cell;
    }
    else {
        GMSAutocompletePrediction *item = [self.matchedAddress objectAtIndex:indexPath.row];
        cell.nameLabel.text = item.attributedFullText.string;
        cell.thumbnailImageView.image = [UIImage imageNamed:@"ic_location"];
        cell.btnDelete.hidden = YES;
        cell.btnDelete.tag = indexPath.row;
        return cell;
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
//    if (self.historyAddress.count>=3) {
//        [self.historyAddress removeObjectAtIndex:0];
//    }
//    [self.historyAddress addObject:address];
    
    if (self.historyAddress != nil && self.historyAddress.count) {
        // 마지막이면 기록삭제를 넣는다.
        if ((self.historyAddress.count) == indexPath.row) {
            PLACE_DELETE_ALL;
            self.historyAddress = (NSMutableArray*)PLACE_SELECT;
            [self onReloadResultController];
            return;
        }
        
        NSDictionary *loc = [self.historyAddress objectAtIndex:indexPath.row];
        
        NSString *location = [Utils nullToString:loc[@"location"]];
        if ([location isEqualToString:@""] || [location isEqualToString:@"0,0"]) {
            location = @"0,0";
        }
        NSArray* stringComponents = [location componentsSeparatedByString:@","];
        
        CLLocationCoordinate2D position;
        position.latitude  = [[NSDecimalNumber decimalNumberWithString:[stringComponents objectAtIndex:0]] doubleValue];
        position.longitude = [[NSDecimalNumber decimalNumberWithString:[stringComponents objectAtIndex:1]] doubleValue];
        
        if (![Utils isLocationValid:position.latitude long:position.longitude]) {
            return;
        }
        
        [self showBusyIndicator:@"Searching..."];
        GMSReverseGeocodeCallback handler = ^(GMSReverseGeocodeResponse *response, NSError *error) {
            GMSAddress *address = response.firstResult;
            if (address) {
                NSLog(@"Geocoder result: %@", address);
                
                //  NSString *locString = [NSString stringWithFormat:@"%@",address.lines];
                
                if ([address.lines count] > 0) {
                    
                    _searchField.text = [Utils addressToString:address];//address.thoroughfare;
                    //_searchField.text = address.attributedFullText.string;
                    NSLog(@"_searchField.text:%@",_searchField.text);
                    
                    NSString *loc = [NSString stringWithFormat:@"%f,%f",position.latitude, position.longitude];
                    PLACE_DELETE_HISTORY(loc);
                    PLACE_INSERT_HISTORY(loc,_searchField.text);
                    NSLog(@"loc:%@",loc);
                    self.historyAddress = nil;
                    //[self.historyAddress removeAllObjects];
                    
                    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:position.latitude longitude:position.longitude zoom:17];
                    [self.mapView_ animateToCameraPosition:camera];
                    
                    _coor = position;
//                    _place = position;
                    _address = _searchField.text;
                    //background.hidden = YES;
                    [self hideResultController];
                }
                
            } else {
                NSLog(@"Could not reverse geocode point (%f,%f): %@",
                      position.latitude, position.longitude, error);
                
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    __block LGAlertView *alertView = [[LGAlertView alloc] initWithTitle:@""
                                                                                message:LocalizedStr(@"Intro.GPS.Fetch.Error.text")
                                                                                  style:LGAlertViewStyleAlert
                                                                           buttonTitles:@[LocalizedStr(@"Button.OK")]
                                                                      cancelButtonTitle:nil
                                                                 destructiveButtonTitle:nil
                                                                          actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                                                                              
                                                                          }
                                                                          cancelHandler:^(LGAlertView *alertView) {
                                                                              NSLog(@"cancelHandler");
                                                                          }
                                                                     destructiveHandler:^(LGAlertView *alertView) {
                                                                         NSLog(@"destructiveHandler");
                                                                     }];
                    [Utils initAlertButtonColor:alertView];
                    [alertView showAnimated:YES completionHandler:^(void)
                     {
                         dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                             [alertView dismissAnimated:YES completionHandler:nil];
                         });
                     }];
                });
            }
            [self hideBusyIndicator];
        };
        [_geocoder reverseGeocodeCoordinate:position completionHandler:handler];
    }
    else {
        // 오류
        if ([self.matchedAddress count] <= indexPath.row) {
            return;
        }
        [self showBusyIndicator:@"Searching..."];
        
        GMSAutocompletePrediction *address = [self.matchedAddress objectAtIndex:indexPath.row];
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        [_placesClient lookUpPlaceID:address.placeID callback:^(GMSPlace *place, NSError *error) {
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            if (error != nil) {
                NSLog(@"Place Details error %@", [error localizedDescription]);
                [self hideBusyIndicator];
                return;
            }
            
            if (place != nil) {
                
                [_searchField resignFirstResponder];
                NSMutableAttributedString *text =
                [[NSMutableAttributedString alloc] initWithString:[place description]];
                if (place.attributions) {
                    [text appendAttributedString:[[NSAttributedString alloc] initWithString:@"\n\n"]];
                    [text appendAttributedString:place.attributions];
                }
                //_resultView.attributedText = text;
                //_searchField.text = address.attributedFullText.string;//place.name;
                _searchField.text = place.name;
                _searchField.text = address.attributedFullText.string;
                NSLog(@"_searchField.text:%@",_searchField.text);
                
                NSString *loc = [NSString stringWithFormat:@"%f,%f",place.coordinate.latitude, place.coordinate.longitude];
                PLACE_DELETE_HISTORY(loc);
                PLACE_INSERT_HISTORY(loc,_searchField.text);
                
                GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:place.coordinate.latitude longitude:place.coordinate.longitude zoom:17];
                [self.mapView_ animateToCameraPosition:camera];
                
                _coor = place.coordinate;
                _place = place;
                _address = place.name;
                //background.hidden = YES;
                
                [self hideResultController];
            } else {
                NSLog(@"No place details for %@", address.placeID);
            }
            [self hideBusyIndicator];
        }];
    }
    
    
}

//// Override to support conditional editing of the table view.
//// This only needs to be implemented if you are going to be returning NO
//// for some items. By default, all items are editable.
//- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
//    // Return YES if you want the specified item to be editable.
//    
//    if (self.historyAddress != nil && self.historyAddress.count) {
//        NSDictionary *loc = [self.historyAddress objectAtIndex:indexPath.row];
//        
//        NSString *location = [Utils nullToString:loc[@"location"]];
//        if (indexPath.row == 0 || indexPath.row == 1) {
//            if ([location isEqualToString:@""] || [location isEqualToString:@"0,0"]) {
//                return NO;
//            }
//        }
//        return YES;
//    }
//    
//    return NO;
//}
//
//// Override to support editing the table view.
//- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
//    if (editingStyle == UITableViewCellEditingStyleDelete) {
//        //add code here for when you hit delete
//        
//        if (self.historyAddress != nil && self.historyAddress.count) {
//            NSDictionary *items = [self.historyAddress objectAtIndex:indexPath.row];
//            
//            PLACE_DELETE_HISTORY(items[@"location"]);
//            self.historyAddress = (NSMutableArray*)PLACE_SELECT;
//            
//            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
//            
//        }
//    }
//}

- (void)showResultController
{
    background.hidden = NO;
    [self addChildViewController:_resultsController];
    
    [self.view addSubview:_resultsController.view];
    
    [self.view layoutIfNeeded];
    [UIView animateWithDuration:0.5
                     animations:^{
                         _resultsController.view.alpha = 1.0f;
                     } ];
    [_resultsController didMoveToParentViewController:self];
    
    [self onReloadResultController];

}

- (void)hideResultController
{
    [_searchField resignFirstResponder];
    background.hidden = YES;
    
    [_resultsController willMoveToParentViewController:nil];
    [UIView animateWithDuration:0.5
                     animations:^{
                         _resultsController.view.alpha = 0.0f;
                     }
                     completion:^(BOOL finished) {
                         [_resultsController.view removeFromSuperview];
                         [_resultsController removeFromParentViewController];
                         
                         self.historyAddress = nil;
                         [self.matchedAddress removeAllObjects];
                     }];
}

- (void)onReloadResultController
{
    _resultsController.view.frame = [self contentRect];
    _resultsController.tableView.frame = _resultsController.view.frame;
    
    [_resultsController.tableView reloadData];
}

#pragma mark - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    if ([_searchField.text isEqualToString:@""]) {
        //[self.matchedAddress addObjectsFromArray:self.historyAddress];
        self.historyAddress = (NSMutableArray*)PLACE_SELECT;
    }
    
    [self moveMarker];
    
    [self showResultController];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    [self hideResultController];
    
    [self moveMarker];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}

- (void)textFieldDidChange:(UITextField *)textField {
    NSString *searchString = textField.text;
    
    [NSObject cancelPreviousPerformRequestsWithTarget:_placesClient selector:@selector(autocompleteQuery:bounds:filter:callback:) object:self];
    
    self.historyAddress = nil;
    if(searchString.length==0){
        self.historyAddress = (NSMutableArray*)PLACE_SELECT;
        _searchField.text = @"";
        NSLog(@"_searchField.text:%@",_searchField.text);
        [self.matchedAddress removeAllObjects];
        
        [self onReloadResultController];
        return;
    }
    
    if(searchString.length>0){
        GMSVisibleRegion visibleRegion = self.mapView_.projection.visibleRegion;
        GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithCoordinate:visibleRegion.farLeft
                                                                           coordinate:visibleRegion.nearRight];
        GMSAutocompleteFilter *filter = [[GMSAutocompleteFilter alloc] init];
        //filter.type = kGMSPlacesAutocompleteTypeFilterAddress;
        filter.type = kGMSPlacesAutocompleteTypeFilterNoFilter;

        //if (!appDelegate.isKorea) {
        filter.country = self->country;
        //}
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        [_placesClient autocompleteQuery:searchString
                                  bounds:bounds
                                  filter:filter
                                callback:^(NSArray *results, NSError *error) {
                                    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                                    if (error != nil) {
                                        [_searchField resignFirstResponder];
                                        NSLog(@"%@",[NSString stringWithFormat:@"Autocomplete failed with error: %@", error.localizedDescription]);
                                        //_resultView.text = [NSString stringWithFormat:@"Autocomplete failed with error: %@", error.localizedDescription];
                                        _searchField.text = @"";
                                        NSLog(@"_searchField.text:%@",_searchField.text);
                                        background.hidden = YES;
                                        return;
                                    }
                                    if(results.count>0){
                                        [self.matchedAddress removeAllObjects];
                                        NSLog(@"result.count:%d",(int)results.count);
                                        for (GMSAutocompletePrediction* result in results) {
                                            [self.matchedAddress addObject:result];
                                        }
                                        NSLog(@"result.count2:%d",(int)self.matchedAddress.count);
                                        
                                        [self onReloadResultController];
                                    }
                                }];
    }
}

#pragma mark - Private Methods

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [_searchField resignFirstResponder];
    //[_searchField endEditing:YES];
    //[self.view endEditing:YES];
    background.hidden = YES;
    
    self.historyAddress = nil;
    [self.matchedAddress removeAllObjects];
}

- (CGRect)contentRect {
    NSInteger rows = self.matchedAddress.count * 40;
    
    if (self.historyAddress != nil && self.historyAddress.count) {
        rows = self.historyAddress.count * 40 + 40;
    }
    
    NSLog(@"rows:%ld",(long)rows);
    //return CGRectMake(0, kTextFieldHeight + 44, self.view.bounds.size.width,rows);
    
    if (rows > background.frame.size.height/2) {
        return CGRectMake(0, background.frame.origin.y, self.view.bounds.size.width, background.frame.size.height/2);
    }
    
    
    return CGRectMake(0,
                      background.frame.origin.y,
                      self.view.bounds.size.width,
                      rows);
}

@end
