//
//  MenuViewController.m
//  SlideMenu
//

//

#import "MenuViewController.h"
#import "StrConsts.h"
#import "TXSharedObj.h"

#import "MenuNavigationController.h"
#import "UIViewController+REFrostedViewController.h"
#import "WZLBadgeImport.h"

int const MENU_ITEMS_COUNT = 4;


@implementation TXSettingsMenuItem
@synthesize title, image, vc;

+(id) create:(NSString *) title image:(NSString *) image viewController:(NSString *)vc {
    return [[self alloc] initWithProperties:title image:image viewController:vc];
}

-(id)initWithProperties:(NSString *) title_ image:(NSString *) image_ viewController:(NSString *)vc_ {
    
    if(self = [super init]) {
        
        self.title = title_;
        self.image = [UIImage imageNamed:image_];
        //self.vc = [[[TXSharedObj instance] currentStoryBoard] instantiateViewControllerWithIdentifier:vc_];
    }
    
    return self;
    
}

@end

@interface MenuViewController() {
    NSMutableArray *items;
    NSArray *_msgList;
}

@end

@implementation MenuViewController
@synthesize cellIdentifier, _target;

-(void)viewDidLoad {
    
    [super viewDidLoad];
    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(statusBarFrameChanged:) name:UIApplicationDidChangeStatusBarFrameNotification object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(statusBarFrameWillChange:) name:UIApplicationWillChangeStatusBarFrameNotification object:nil];
    
    [super backroundView].hidden = YES;
    self.view.backgroundColor = HEXCOLOR(0xFFFFFFFF);
    
    NSString* _name = [Utils getUserName];

    UIView *n = [super navigationView];
    
    [n setBackgroundColor:HEXCOLOR(0xFFFFFFFF)];
    NSInteger _y = n.frame.origin.y + n.frame.size.height;
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, n.frame.size.width, _y)];
    
    UIImageView *imageView = [Utils makeCircleProfile:CGRectMake(22, kTopHeight+10, 56, 56) image:nil tag:3101];
    
    // label의 크기가 menuViewSize를 넘지 않게 계산
    NSInteger _x = imageView.frame.origin.x + imageView.frame.size.width + 21;
    NSInteger _width = 200;
    if ((appDelegate.frostedViewController.menuViewSize.width-20)<(_x+_width)) {
        _width = (appDelegate.frostedViewController.menuViewSize.width-20) - _x;
    }
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(_x,
                                                               kTopHeight+14+10,
                                                               _width,
                                                               23)];
    label.text = _name;
    label.tag = 3102;
    label.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:20];
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor blackColor];
    label.lineBreakMode = NSLineBreakByTruncatingTail;
    //[label sizeToFit];
    
    NSLog(@"_width:%ld,%d,%f,%f",(long)_x,(int)_width,appDelegate.frostedViewController.menuViewSize.width,appDelegate.frostedViewController.menuViewSize.width-20);
#ifdef _DEBUG
    // for debug
    UIButton *debugbutton = [UIButton buttonWithType:UIButtonTypeCustom];
    debugbutton.backgroundColor = [UIColor clearColor];
    debugbutton.tag = 9090;
    debugbutton.frame = imageView.frame;
    [debugbutton addTarget:self action:@selector(onNaviButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:debugbutton];
    // end of debug
#endif
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.tag = 1100;
    button.frame = CGRectMake(-40,
                              35+10,
                              kRadiusImageSize,
                              kRadiusImageSize);
    [button setImage:[UIImage imageNamed:@"btn_close_menu"] forState:UIControlStateNormal];
    CGRect frame = button.frame;
    frame.origin.x = self.view.frame.size.width - button.frame.size.width - kLeftMargin*2;
    button.frame = frame;

    // Add an action in current code file (i.e. target)
    [button addTarget:self action:@selector(onNaviButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    
    [view addSubview:imageView];
    [view addSubview:label];
    
#ifdef _DRIVER_MODE
    [label sizeToFit];
    _lbDriverRating.text = [Utils getDriverRate];
    frame = _ratingView.frame;
    frame.origin.x = label.frame.origin.x + label.frame.size.width + 20;
    frame.origin.y = label.frame.origin.y + 2;
    _ratingView.frame = frame;
    [view addSubview:_ratingView];
#endif
    
    
#ifdef _DRIVER_MODE
    
#ifdef _WITZM
    [view addSubview:button];
#elif defined _CHACHA
    [view addSubview:button];
#endif
    
#else
    
#ifdef _WITZM
#elif defined _CHACHA
#endif
    
#endif


    view.backgroundColor = [UIColor clearColor];
    [self.view addSubview:view];
    
    UINib *nib = [UINib nibWithNibName:@"MenuTableViewCell" bundle:nil];
    self.cellIdentifier = @"leftMenuCell";
    [self.tableView registerNib:nib forCellReuseIdentifier:self.cellIdentifier];

    self.tableView.frame = CGRectMake(0, _y, self.view.bounds.size.width, self.view.bounds.size.height-_y);
    self.tableView.translatesAutoresizingMaskIntoConstraints = NO;
    self.tableView.separatorColor = [UIColor clearColor];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.opaque = NO;
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.tableHeaderView = ({
        
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0,
                                                                  0,
                                                                  self.view.frame.size.width,
                                                                  8)];
        
        view;
    });
    
    [self reloadMenu];
    
//    self.tableView.scrollEnabled = NO;
//    if ([[UIDevice currentDevice].model isEqualToString:@"iPad"]) {
//        self.tableView.scrollEnabled = YES;
//    }
}

-(void)viewDidLayoutSubviews {
    
    UIView *n = [super navigationView];
    NSInteger _y = n.frame.origin.y + n.frame.size.height;
    
    CGRect rect = self.view.frame;
    rect.origin.y = _y;
    rect.size.height -= _y;
    //rect.size.width = self.view.frame.size.width/2 + self.view.frame.size.width/6;
    self.tableView.frame = rect;

    //[self reloadMenu];
    //[self.tableView setContentOffset:CGPointZero animated:NO];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self reloadInfo];
    [self reloadMenu];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    [self reloadInfo];
    [self reloadMenu];
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self removeEventListeners];
}

-(void)reloadInfo {
    NSDictionary *user;
    
    if (appDelegate.isDriverMode) {
        user = [appDelegate.dicDriver objectForKey:@"provider_device"];
    }
    else {
        user = [appDelegate.dicRider objectForKey:@"user_device"];
    }
    
//    UIImageView *imageView = [self.view viewWithTag:3100];
//    imageView.image = [Utils ProfileLevelImage:[user objectForKey:@"trips"]];
    
    UIImageView *profile = [self.view viewWithTag:3101];
    if (appDelegate.isDriverMode) {
        profile.image = appDelegate.imgProfileDriver;
        if (appDelegate.imgProfileDriver == nil) {
            profile.image = [UIImage imageNamed:@"ic_user_white"];
        }
    }
    else {
        profile.image = appDelegate.imgProfileRider;
        if (appDelegate.imgProfileRider == nil) {
            profile.image = [UIImage imageNamed:@"ic_user_white"];
        }
    }
    
    NSString* _name = [Utils nameToString:[user objectForKey:@"name"]];
    ((UILabel *)[self.view viewWithTag:3102]).text = _name;
    
    //[Utils labelMenuUserInfo:((UILabel *)[self.view viewWithTag:3102]) trips:[user objectForKey:@"trips"] rate:[user objectForKey:@"rate"]];
}

#ifdef _DRIVER_MODE

#ifdef _WITZM
#pragma mark - Driver WitzM
-(void)reloadMenu {
    
    self->items = [NSMutableArray arrayWithCapacity:MENU_ITEMS_COUNT];
    
    TXSettingsMenuItem *item = [TXSettingsMenuItem create:LocalizedStr(@"Menu.Driver.Profile.title")
                                                    image:@"icon_menu_account"
                                           viewController:LEFT_MENU.Controllers.HOME];
    [items addObject:item];
    
    item = [TXSettingsMenuItem create:LocalizedStr(@"Menu.Driver.Vehicle.title")
                                image:@"icon_menu_vehicle"
                       viewController:LEFT_MENU.Controllers.PROFILE];
    [items addObject:item];
    
    item = [TXSettingsMenuItem create:LocalizedStr(@"Menu.Driver.TripHistory.title")
                                image:@"icon_menu_triphistory"
                       viewController:LEFT_MENU.Controllers.SETTINGS];
    [items addObject:item];
    
    item = [TXSettingsMenuItem create:LocalizedStr(@"Menu.Driver.Earning.title")
                                image:@"icon_menu_earnings"
                       viewController:LEFT_MENU.Controllers.SETTINGS];
    [items addObject:item];
    
    item = [TXSettingsMenuItem create:LocalizedStr(@"Menu.News.title")
                                image:@"icon_menu_news"
                       viewController:LEFT_MENU.Controllers.SETTINGS];
    [items addObject:item];

    item = [TXSettingsMenuItem create:LocalizedStr(@"Menu.Faq.title")
                                image:@"icon_menu_faq"
                       viewController:LEFT_MENU.Controllers.SETTINGS];
    [items addObject:item];
    
    item = [TXSettingsMenuItem create:LocalizedStr(@"Menu.Qna.title")
                                image:@"icon_menu_qna"
                       viewController:LEFT_MENU.Controllers.SETTINGS];
    [items addObject:item];
    
    item = [TXSettingsMenuItem create:LocalizedStr(@"Menu.Settings.title")
                                image:@"icon_menu_setting"
                       viewController:LEFT_MENU.Controllers.SETTINGS];
    [items addObject:item];
    
    if ([[Utils nullToString:[[[TXApp instance] getSettings] getDriveMode]] isEqualToString:DRIVE_MODE_TRAINING]) {
        
        if (appDelegate.pstate <= 210) {
            item = [TXSettingsMenuItem create:LocalizedStr(@"Menu.Testdrive.end.title")
                                        image:@"icon_menu_trainingmode"
                               viewController:LEFT_MENU.Controllers.SETTINGS];
        }
        else {
            item = [TXSettingsMenuItem create:@""
                                        image:nil
                               viewController:LEFT_MENU.Controllers.SETTINGS];
        }
        
        [items addObject:item];
    }
    else {

        // vehicle을 승인하지 않았다면 시험운행은 하지 않는다.
        NSInteger vcount = 0;
        NSArray *provider_vehicle = [appDelegate.dicDriver objectForKey:@"provider_vehicles"];
        for (NSDictionary* key in provider_vehicle) {
            if ([key[@"state"] intValue] == 1) {
                vcount++;
            }
        }
        
        if (appDelegate.pstate == 200 && (appDelegate.ppstate != 1 || !vcount)) {
            item = [TXSettingsMenuItem create:@""
                                        image:nil
                               viewController:LEFT_MENU.Controllers.SETTINGS];
            [items addObject:item];
        } else if (appDelegate.pstate == 200) {
            item = [TXSettingsMenuItem create:LocalizedStr(@"Menu.Testdrive.start.title")
                                        image:@"icon_menu_trainingmode"
                               viewController:LEFT_MENU.Controllers.SETTINGS];
            [items addObject:item];
        }
        else if (appDelegate.pstate == 210) {
            item = [TXSettingsMenuItem create:LocalizedStr(@"Menu.Driver.Offline.title")
                                        image:@"icon_menu_gooffline"
                               viewController:LEFT_MENU.Controllers.SETTINGS];
            [items addObject:item];
        }
        else {
            item = [TXSettingsMenuItem create:@""
                                        image:nil
                               viewController:LEFT_MENU.Controllers.SETTINGS];
            [items addObject:item];
        }
    }
    
    [self.tableView reloadData];
}
#elif defined _CHACHA
#pragma mark - Driver ChaCha
-(void)reloadMenu {
    
    self->items = [NSMutableArray arrayWithCapacity:MENU_ITEMS_COUNT];
    
    TXSettingsMenuItem *item = [TXSettingsMenuItem create:LocalizedStr(@"Menu.Driver.Profile.title")
                                                    image:@"icon_menu_account"
                                           viewController:LEFT_MENU.Controllers.HOME];
    [items addObject:item];
    
    item = [TXSettingsMenuItem create:LocalizedStr(@"Menu.Driver.Vehicle.title")
                                image:@"icon_menu_vehicle"
                       viewController:LEFT_MENU.Controllers.PROFILE];
    [items addObject:item];
    
    item = [TXSettingsMenuItem create:LocalizedStr(@"Menu.Driver.TripHistory.title")
                                image:@"icon_menu_triphistory"
                       viewController:LEFT_MENU.Controllers.SETTINGS];
    [items addObject:item];
    
    item = [TXSettingsMenuItem create:LocalizedStr(@"Menu.Driver.Earning.title")
                                image:@"icon_menu_earnings"
                       viewController:LEFT_MENU.Controllers.SETTINGS];
    [items addObject:item];
    
    item = [TXSettingsMenuItem create:LocalizedStr(@"Menu.News.title")
                                image:@"icon_menu_news"
                       viewController:LEFT_MENU.Controllers.SETTINGS];
    [items addObject:item];

    item = [TXSettingsMenuItem create:LocalizedStr(@"Menu.Faq.title")
                                image:@"icon_menu_faq"
                       viewController:LEFT_MENU.Controllers.SETTINGS];
    [items addObject:item];
    
    item = [TXSettingsMenuItem create:LocalizedStr(@"Menu.Qna.title")
                                image:@"icon_menu_qna"
                       viewController:LEFT_MENU.Controllers.SETTINGS];
    [items addObject:item];
    
    item = [TXSettingsMenuItem create:LocalizedStr(@"Menu.Settings.title")
                                image:@"icon_menu_setting"
                       viewController:LEFT_MENU.Controllers.SETTINGS];
    [items addObject:item];
    
    if ([[Utils nullToString:[[[TXApp instance] getSettings] getDriveMode]] isEqualToString:DRIVE_MODE_TRAINING]) {
        
        if (appDelegate.pstate <= 210) {
            item = [TXSettingsMenuItem create:LocalizedStr(@"Menu.Testdrive.end.title")
                                        image:@"icon_menu_trainingmode"
                               viewController:LEFT_MENU.Controllers.SETTINGS];
        }
        else {
            item = [TXSettingsMenuItem create:@""
                                        image:nil
                               viewController:LEFT_MENU.Controllers.SETTINGS];
        }
        
        [items addObject:item];
    }
    else {

        // vehicle을 승인하지 않았다면 시험운행은 하지 않는다.
        NSInteger vcount = 0;
        NSArray *provider_vehicle = [appDelegate.dicDriver objectForKey:@"provider_vehicles"];
        for (NSDictionary* key in provider_vehicle) {
            if ([key[@"state"] intValue] == 1) {
                vcount++;
            }
        }
        if (appDelegate.pstate == 200 && (appDelegate.ppstate != 1 || !vcount)) {
            item = [TXSettingsMenuItem create:@""
                                        image:nil
                               viewController:LEFT_MENU.Controllers.SETTINGS];
            [items addObject:item];
        } else if (appDelegate.pstate == 200) {
            item = [TXSettingsMenuItem create:LocalizedStr(@"Menu.Testdrive.start.title")
                                        image:@"icon_menu_trainingmode"
                               viewController:LEFT_MENU.Controllers.SETTINGS];
            [items addObject:item];
        }
    
        else if (appDelegate.pstate == 210) {
            item = [TXSettingsMenuItem create:LocalizedStr(@"Menu.Driver.Offline.title")
                                        image:@"icon_menu_gooffline"
                               viewController:LEFT_MENU.Controllers.SETTINGS];
            [items addObject:item];
        }
        else {
            item = [TXSettingsMenuItem create:@""
                                        image:nil
                               viewController:LEFT_MENU.Controllers.SETTINGS];
            [items addObject:item];
        }
    }
    
    [self.tableView reloadData];
}
#endif

#else

#ifdef _WITZM
#pragma mark - Rider WitzM
-(void)reloadMenu {
    
    self->items = [NSMutableArray arrayWithCapacity:MENU_ITEMS_COUNT];
    
    TXSettingsMenuItem *item = [TXSettingsMenuItem create:LocalizedStr(@"Menu.Account.title")
                                                    image:@"icon_menu_account"
                                           viewController:LEFT_MENU.Controllers.HOME];
    [items addObject:item];
    
    item = [TXSettingsMenuItem create:LocalizedStr(@"Menu.Payment.title")
                                image:@"icon_menu_payment"
                       viewController:LEFT_MENU.Controllers.PROFILE];
    [items addObject:item];
    
    item = [TXSettingsMenuItem create:LocalizedStr(@"Menu.TripHistory.title")
                                image:@"icon_menu_triphistory"
                       viewController:LEFT_MENU.Controllers.SETTINGS];
    [items addObject:item];
    
    item = [TXSettingsMenuItem create:LocalizedStr(@"Menu.Favorite.title") //LocalizedStr(@"Menu.ShareTripHistory.title")
                                image:@"icon_menu_tripsharehistory"
                       viewController:LEFT_MENU.Controllers.SETTINGS];
    [items addObject:item];
    
    item = [TXSettingsMenuItem create:LocalizedStr(@"Menu.News.title")
                                image:@"icon_menu_news"
                       viewController:LEFT_MENU.Controllers.SETTINGS];
    [items addObject:item];
    
    item = [TXSettingsMenuItem create:LocalizedStr(@"Menu.Promotion.title")
                                image:@"icon_menu_freerides"
                       viewController:LEFT_MENU.Controllers.SETTINGS];
    [items addObject:item];
    
    item = [TXSettingsMenuItem create:LocalizedStr(@"Menu.Invite.title")
                                image:@"icon_menu_invitefriends"
                       viewController:LEFT_MENU.Controllers.SETTINGS];
    [items addObject:item];
    
    
    item = [TXSettingsMenuItem create:LocalizedStr(@"Menu.Faq.title")
                                image:@"icon_menu_faq"
                       viewController:LEFT_MENU.Controllers.SETTINGS];
    [items addObject:item];
    
    item = [TXSettingsMenuItem create:LocalizedStr(@"Menu.Qna.title")
                                image:@"icon_menu_qna"
                       viewController:LEFT_MENU.Controllers.SETTINGS];
    [items addObject:item];
    
    item = [TXSettingsMenuItem create:LocalizedStr(@"Menu.Settings.title")
                                image:@"icon_menu_setting"
                       viewController:LEFT_MENU.Controllers.SETTINGS];
    [items addObject:item];
    
    item = [TXSettingsMenuItem create:@""
                                image:nil
                       viewController:LEFT_MENU.Controllers.SETTINGS];
    [items addObject:item];
    
    if ([Utils checkAppInstalled]) {
        item = [TXSettingsMenuItem create:LocalizedStr(@"Menu.BecomeMode.title")
                                    image:@"icon_menu_becomedriver"
                           viewController:LEFT_MENU.Controllers.SIGNOUT];
    }
    else {
        item = [TXSettingsMenuItem create:LocalizedStr(@"Menu.DriverMode.title")
                                    image:@"icon_menu_becomedriver"
                           viewController:LEFT_MENU.Controllers.SIGNOUT];
    }
    [items addObject:item];
    
    [self.tableView reloadData];
}
#elif defined _CHACHA
#pragma mark - Rider ChaCha
-(void)reloadMenu {
    
    self->items = [NSMutableArray arrayWithCapacity:MENU_ITEMS_COUNT];
    
    TXSettingsMenuItem *item = [TXSettingsMenuItem create:LocalizedStr(@"Menu.Account.title")
                                                    image:@"icon_menu_account"
                                           viewController:LEFT_MENU.Controllers.HOME];
    
    [items addObject:item];
    
    item = [TXSettingsMenuItem create:LocalizedStr(@"Menu.Payment.title")
                                image:@"icon_menu_payment"
                       viewController:LEFT_MENU.Controllers.PROFILE];
    
    [items addObject:item];
    
    item = [TXSettingsMenuItem create:LocalizedStr(@"Menu.TripHistory.title")
                                image:@"icon_menu_triphistory"
                       viewController:LEFT_MENU.Controllers.SETTINGS];
    
    [items addObject:item];
    
    item = [TXSettingsMenuItem create:LocalizedStr(@"Menu.Favorite.title") //LocalizedStr(@"Menu.ShareTripHistory.title")
                                image:@"icon_menu_tripsharehistory"
                       viewController:LEFT_MENU.Controllers.SETTINGS];
    
    [items addObject:item];
    
    item = [TXSettingsMenuItem create:LocalizedStr(@"Menu.News.title")
                                image:@"icon_menu_news"
                       viewController:LEFT_MENU.Controllers.SETTINGS];
    
    [items addObject:item];
    
    item = [TXSettingsMenuItem create:LocalizedStr(@"Menu.Promotion.title")
                                image:@"icon_menu_freerides"
                       viewController:LEFT_MENU.Controllers.SETTINGS];
    
    [items addObject:item];
    
    item = [TXSettingsMenuItem create:LocalizedStr(@"Menu.Invite.title")
                                image:@"icon_menu_invitefriends"
                       viewController:LEFT_MENU.Controllers.SETTINGS];
    
    [items addObject:item];
    
    
    item = [TXSettingsMenuItem create:LocalizedStr(@"Menu.Faq.title")
                                image:@"icon_menu_faq"
                       viewController:LEFT_MENU.Controllers.SETTINGS];
    
    [items addObject:item];
    
    item = [TXSettingsMenuItem create:LocalizedStr(@"Menu.Qna.title")
                                image:@"icon_menu_qna"
                       viewController:LEFT_MENU.Controllers.SETTINGS];
    
    [items addObject:item];
    
    item = [TXSettingsMenuItem create:LocalizedStr(@"Menu.Settings.title")
                                image:@"icon_menu_setting"
                       viewController:LEFT_MENU.Controllers.SETTINGS];
    
    [items addObject:item];
    
    item = [TXSettingsMenuItem create:@""
                                image:nil
                       viewController:LEFT_MENU.Controllers.SETTINGS];
    
    [items addObject:item];
    
    if ([Utils checkAppInstalled]) {
        item = [TXSettingsMenuItem create:LocalizedStr(@"Menu.BecomeMode.title")
                                    image:@"icon_menu_becomedriver"
                           viewController:LEFT_MENU.Controllers.SIGNOUT];
    }
    else {
        item = [TXSettingsMenuItem create:LocalizedStr(@"Menu.DriverMode.title")
                                    image:@"icon_menu_becomedriver"
                           viewController:LEFT_MENU.Controllers.SIGNOUT];
    }
    
    [items addObject:item];
    
    [self.tableView reloadData];
}
#endif

#endif



#pragma mark - Configure

-(void)configureStyles {
    [super configureStyles];
}

-(void)configure {
    [super configure];
    
    // 2. init event
    _msgList = @[
                 @[@"UR601",@""], // rider profile
                 @[@"PP602",@""], // driver profile
                 ];
    
//    [self registerEventListeners];
//    
//    if (!appDelegate.isDriverMode) {
//        [self->model UR601];
//    }
//    else {
//        [self->model PP602];
//    }
    
}

-(void) registerEventListeners {
    for (NSArray *msg in _msgList) {
        [self->model addEventListener:self forEvent:[msg objectAtIndex:0] eventParams:nil];
    }
}

-(void) removeEventListeners {
    for (NSArray *msg in _msgList) {
        [self->model removeEventListener:self forEvent:[msg objectAtIndex:0]];
    }
}

#pragma mark - Action
-(IBAction)onNaviButtonClick:(id)sender {
    UIButton *btn = (UIButton*)sender;
    NSLog(@"onNaviButtonClick");
    //[super onNaviButtonClick:sender];
    
    if (btn.tag == 1100) {
        [self removeEventListeners];
        [self.frostedViewController hideMenuViewController];
        if ( [self.delegate respondsToSelector:@selector(didSideMenuSelected:)] ) {
            [self.delegate didSideMenuSelected:9090];
        }
    }
    else if (btn.tag == 9090) {
        //
        appDelegate.isDebugMode = !appDelegate.isDebugMode;
        [Utils onErrorAlert:[NSString stringWithFormat:@"Debug mode is %d",appDelegate.isDebugMode]];
    }
}


#pragma mark - UITableView Delegate & Datasrouce -

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
//    
//    return 38;
//    
//}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return self->items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:self.cellIdentifier];
	
    cell.contentView.backgroundColor = [UIColor clearColor];
    
    TXSettingsMenuItem *item = self->items[indexPath.row];
    
    ((UILabel *)[cell.contentView viewWithTag:1]).text = item.title;
    ((UILabel *)[cell.contentView viewWithTag:1]).font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:18];
    
    UIImageView* cellImage = ((UIImageView*)[cell.contentView viewWithTag:2]);
    cellImage.image = item.image;
    
    
#ifdef _DRIVER_MODE
    
#ifdef _WITZM
#pragma mark - Driver WitzM
    if (indexPath.row == 2) {
        if ([Utils showAlrim:CODE_TYPE_CHAT]) {
            //[cellImage showBadgeWithStyle:WBadgeStyleNew value:0 animationType:WBadgeAnimTypeNone];
            [cellImage clearBadge];
        }
        else {
            [cellImage clearBadge];
        }
    }
    else {
        [cellImage clearBadge];
    }
#elif defined _CHACHA
#pragma mark - Driver ChaCha
    if (indexPath.row == 2) {
        if ([Utils showAlrim:CODE_TYPE_CHAT]) {
            //[cellImage showBadgeWithStyle:WBadgeStyleNew value:0 animationType:WBadgeAnimTypeNone];
            [cellImage clearBadge];
        }
        else {
            [cellImage clearBadge];
        }
    }
    else {
        [cellImage clearBadge];
    }
#endif
    
#else
    
#ifdef _WITZM
#pragma mark - Rider WitzM
    if (indexPath.row == 2) {
        if ([Utils showAlrim:CODE_TYPE_CHAT]) {
            //[cellImage showBadgeWithStyle:WBadgeStyleNew value:0 animationType:WBadgeAnimTypeNone];
            [cellImage clearBadge];
        }
        else {
            [cellImage clearBadge];
        }
    }
    else if (indexPath.row == 3) {
        if ([Utils showAlrim:CODE_TYPE_SHARE_TRIP]) {
            [cellImage showBadgeWithStyle:WBadgeStyleNew value:0 animationType:WBadgeAnimTypeNone];
        }
        else {
            [cellImage clearBadge];
        }
    }
    else {
        [cellImage clearBadge];
    }
#elif defined _CHACHA
#pragma mark - Rider ChaCha
    if (indexPath.row == 2) {
        if ([Utils showAlrim:CODE_TYPE_CHAT]) {
            //[cellImage showBadgeWithStyle:WBadgeStyleNew value:0 animationType:WBadgeAnimTypeNone];
            [cellImage clearBadge];
        }
        else {
            [cellImage clearBadge];
        }
    }
    else if (indexPath.row == 3) {
        if ([Utils showAlrim:CODE_TYPE_SHARE_TRIP]) {
            [cellImage showBadgeWithStyle:WBadgeStyleNew value:0 animationType:WBadgeAnimTypeNone];
        }
        else {
            [cellImage clearBadge];
        }
    }
    else {
        [cellImage clearBadge];
    }
#endif
    
#endif
    
    //[cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    if (!appDelegate.isDriverMode && indexPath.item == 9) {
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    
#ifdef _DRIVER_MODE
    
#ifdef _WITZM
    if (appDelegate.isDriverMode && indexPath.row == 8) {
        ((UILabel *)[cell.contentView viewWithTag:1]).textColor = UIColorDefault;
    }
    else {
        ((UILabel *)[cell.contentView viewWithTag:1]).textColor = [UIColor blackColor];
    }
#elif defined _CHACHA
    if (appDelegate.isDriverMode && indexPath.row == 8) {
        ((UILabel *)[cell.contentView viewWithTag:1]).textColor = UIColorDefault;
    }
    else {
        ((UILabel *)[cell.contentView viewWithTag:1]).textColor = [UIColor blackColor];
    }
#endif
    
#else
    
#ifdef _WITZM
    if (indexPath.item == 11) {
        ((UILabel *)[cell.contentView viewWithTag:1]).textColor = UIColorDefault;
    }
    else {
        ((UILabel *)[cell.contentView viewWithTag:1]).textColor = [UIColor blackColor];
    }
#elif defined _CHACHA
    if (indexPath.item == 11) {
        ((UILabel *)[cell.contentView viewWithTag:1]).textColor = UIColorDefault;
    }
    else {
        ((UILabel *)[cell.contentView viewWithTag:1]).textColor = [UIColor blackColor];
    }
#endif
    
#endif

	
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

#ifdef _DRIVER_MODE
    
    if ([[Utils nullToString:[[[TXApp instance] getSettings] getDriveMode]] isEqualToString:DRIVE_MODE_TRAINING]) {
        if (indexPath.row == 0 || indexPath.row == 1) {
            // alert
            [self alertError:@"" message:LocalizedStr(@"String.Driver.Permit.None.testmode")];
            return;
        }
    }
    
#ifdef _WITZM
#pragma mark - Driver WitzM
    
    if (appDelegate.isBecomeMode) {
        if (indexPath.section == 0 && (indexPath.row == 0 || indexPath.row == 1)) {
            NSString *step = [Utils getBCDSTEP];
            if ([step  intValue]== 1 && indexPath.row == 1) {
                // alert
                [self alertError:LocalizedStr(@"String.Alert") message:LocalizedStr(@"String.Driver.Permit.None.text")];
                return;
            }
            else if ([step  intValue]== 2 && indexPath.row == 0) {
                // alert
                [self alertError:LocalizedStr(@"String.Alert") message:LocalizedStr(@"String.Driver.Permit.None.text")];
                return;
            }
            else if ([step  intValue]== 3) {
                // alert
                [self alertError:LocalizedStr(@"String.Alert") message:LocalizedStr(@"String.Driver.Permit.None.text")];
                return;
            }
        }
    }
    
#elif defined _CHACHA
#pragma mark - Driver ChaCha
    
    if (appDelegate.isBecomeMode) {
        if (indexPath.section == 0 && (indexPath.row == 0 || indexPath.row == 1)) {
            NSString *step = [Utils getBCDSTEP];
            if ([step  intValue]== 1 && indexPath.row == 1) {
                // alert
                [self alertError:LocalizedStr(@"String.Alert") message:LocalizedStr(@"String.Driver.Permit.None.text")];
                return;
            }
            else if ([step  intValue]== 2 && indexPath.row == 0) {
                // alert
                [self alertError:LocalizedStr(@"String.Alert") message:LocalizedStr(@"String.Driver.Permit.None.text")];
                return;
            }
            else if ([step  intValue]== 3) {
                // alert
                [self alertError:LocalizedStr(@"String.Alert") message:LocalizedStr(@"String.Driver.Permit.None.text")];
                return;
            }
        }
    }
    
#endif
    
#else
    
#ifdef _WITZM
#pragma mark - Rider WitzM
    
    // rider인 경우 카드list에 들어가지 못한다.
    if (indexPath.row == 1) {
        if (appDelegate.ustate > 110) {
            // alert
            [self alertError:LocalizedStr(@"String.Alert") message:LocalizedStr(@"String.Driver.Permit.None.payment")];
            return;
        }
    }
    
#elif defined _CHACHA
#pragma mark - Rider ChaCha
    
    // rider인 경우 카드list에 들어가지 못한다.
    if (indexPath.row == 1) {
        if (appDelegate.ustate > 110) {
            // alert
            [self alertError:LocalizedStr(@"String.Alert") message:LocalizedStr(@"String.Driver.Permit.None.payment")];
            return;
        }
    }
    
#endif
    
#endif
    
    if (!appDelegate.isDriverMode && indexPath.row == 10) {
        return;
    }
    
    [self.frostedViewController hideMenuViewController];
    
    if ( [self.delegate respondsToSelector:@selector(didSideMenuSelected:)] ) {
        [self.delegate didSideMenuSelected:indexPath.row];
    }
}

#pragma mark - onEvent
-(void)onEvent:(TXEvent *)event eventParams:(id)subscriptionParams {
    
    TXResponseDescriptor * descriptor = [event getEventProperty:TXEvents.Params.DESCRIPTOR];
    TXRequestObj *request = [event getEventProperty:TXEvents.Params.REQUEST];
    
    [self hideBusyIndicator];
    if([event.name isEqualToString:@"UR601"] || [event.name isEqualToString:@"PP602"]) {
        
        if(descriptor.success == true) {
            NSString     *response = [[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding];
            NSDictionary *result   = getJSONObj(response);
            
            
            NSString *pictureData              = [result valueForKey:@"picture"];
            
            if ([pictureData length]) {
//                NSData *data = [[NSData alloc] initWithBase64EncodedString:pictureData options:0];
//                UIImage *image = [[UIImage alloc] initWithData:data];
                UIImage *image = [Utils decodeBase64ToImage:pictureData];
                
                UIImageView *profile = [self.view viewWithTag:3101];
                profile.image = image;
                
                NSDictionary *user;
                if (appDelegate.isDriverMode) {
                    appDelegate.pictureSizeDriver = [NSString stringWithFormat:@"%lu",(unsigned long)[pictureData length]];
                    appDelegate.imgProfileDriver = image;
                    user = [appDelegate.dicDriver objectForKey:@"provider_device"];
                }
                else {
                    appDelegate.pictureSizeRider = [NSString stringWithFormat:@"%lu",(unsigned long)[pictureData length]];
                    appDelegate.imgProfileRider = image;
                    user = [appDelegate.dicRider objectForKey:@"user_device"];
                }
                
                NSString* _name = [Utils nameToString:[user objectForKey:@"name"]];
                ((UILabel *)[self.view viewWithTag:3102]).text = _name;
            }
            
        } else {
        //    [self alertError:@"Error" message:descriptor.error];
        }
    }
}

@end
