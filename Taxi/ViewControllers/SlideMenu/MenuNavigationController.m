//
//  DEMONavigationController.m
//  REFrostedViewControllerExample
//
//  Created by Roman Efimov on 9/18/13.
//  Copyright (c) 2013 Roman Efimov. All rights reserved.
//

#import "MenuNavigationController.h"
#import "MenuViewController.h"
#import "UIViewController+REFrostedViewController.h"

@interface MenuNavigationController ()

@property (strong, readwrite, nonatomic) MenuNavigationController *menuViewController;

@end

@implementation MenuNavigationController

- (void)viewDidLoad
{
    [super viewDidLoad];
    //[self.view addGestureRecognizer:[[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panGestureRecognized:)]];
}

- (void)showMenu
{
    // Dismiss keyboard (optional)
    //
#ifdef _DRIVER_MODE
    
    [self.view endEditing:NO];
    [self.frostedViewController.view endEditing:NO];
    
#else

#ifdef _WITZM
    [self.view endEditing:YES];
    [self.frostedViewController.view endEditing:YES];
#elif defined _CHACHA
    [self.view endEditing:YES];
    [self.frostedViewController.view endEditing:YES];
#endif
    
#endif
    
    // Present the view controller
    //
    [self.frostedViewController presentMenuViewController];
}

#pragma mark -
#pragma mark Gesture recognizer

- (void)panGestureRecognized:(UIPanGestureRecognizer *)sender
{
    // Dismiss keyboard (optional)
    //
    
#ifdef _DRIVER_MODE
    
    [self.view endEditing:NO];
    [self.frostedViewController.view endEditing:NO];
    
#else
    
#ifdef _WITZM
    [self.view endEditing:YES];
    [self.frostedViewController.view endEditing:YES];
#elif defined _CHACHA
    [self.view endEditing:YES];
    [self.frostedViewController.view endEditing:YES];
#endif
    
#endif
    
    // Present the view controller
    //
    //[self.frostedViewController panGestureRecognized:sender];
}

@end
