//
//  MenuViewController.h
//  SlideMenu
//

//

#import <UIKit/UIKit.h>

#import "TXBaseViewController.h"
#import "REFrostedViewController.h"

@interface TXSettingsMenuItem : NSObject
    
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) UIImage  *image;
@property (nonatomic, strong) TXBaseViewController *vc;

+(id) create:(NSString *) title image:(NSString *) image viewController:(NSString *)vc;

@end


@protocol SideMenuSelectDelegate <NSObject>
@required
- (void) didSideMenuSelected:(NSInteger) tag;

@end


@interface MenuViewController : TXBaseViewController <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) NSString *cellIdentifier;
@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (nonatomic, strong) IBOutlet UIView *ratingView;
@property (nonatomic, strong) IBOutlet UILabel *lbDriverRating;

@property (nonatomic, strong) id _target;
@property (nonatomic, assign) id<SideMenuSelectDelegate> delegate;

@end
