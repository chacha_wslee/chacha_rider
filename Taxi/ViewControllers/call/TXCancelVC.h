//
//  TXChargeVC.h
//  Taxi
//

//

#import "TXUserVC.h"

@interface TXCancelVC : TXUserVC {
}

@property (nonatomic, strong) IBOutlet UIImageView* imgIcon;
@property (nonatomic, strong) IBOutlet UILabel* lbTitle;
@property (nonatomic, strong) IBOutlet UILabel* lbDetail;

@property (nonatomic, strong) IBOutlet TXButton* btnCancel;

@property (nonatomic, strong) IBOutlet TXButton* btnSubmit;

@property (nonatomic, assign) NSInteger step;
@property (nonatomic, assign) NSInteger state;

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil step:(NSInteger)__step state:(NSInteger)__state;
    
@end
