//
//  TXMapVC.h
//  Taxi
//

//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import "TXBaseViewController.h"
#import "NARootTouchThroughView.h"

#import "TXDestinationsVC.h"
#import "MenuViewController.h"
#ifdef _DRIVER_MODE
    #import "TXVehicleVC.h"
#endif
#import "converter.h"
#import "BackgroundTask.h"

@interface TXMapVC : TXBaseViewController<GMSMapViewDelegate,
                                            UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate,
                                            TXDestinationsVCDelegate,
                                            SideMenuSelectDelegate
#ifdef _DRIVER_MODE
,VehicleSelectDelegate,
SpotifyDelegate
#else
,UIScrollViewDelegate
#endif
>
{
    BackgroundTask * bgTask;
}

// for debug
@property (nonatomic, retain) IBOutlet UITextView *activity;

@property (assign, nonatomic) BOOL isDriveMode;

@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (nonatomic, strong)  GMSMapView *mapView_;

@property (nonatomic, strong) IBOutlet UITextField *txtSearch;
@property (nonatomic, strong) IBOutlet UITextField *autoCompleteTextField;


// global
@property (strong, nonatomic) NSMutableDictionary *uiKey;

@property (nonatomic, strong) IBOutlet UIButton *btnEta;

#ifdef _DRIVER_MODE
// view
@property (nonatomic, strong) IBOutlet UIView *vTopDriverNavi200;
@property (nonatomic, strong) IBOutlet UIView *vTopDriverNavi210;
@property (nonatomic, strong) IBOutlet UIView *vTopDriverNavi2X0;
@property (nonatomic, strong) IBOutlet UIView *vToolBar;
@property (nonatomic, strong) IBOutlet UIView *vMap;
@property (nonatomic, strong) IBOutlet UIView *vAddress;


@property (nonatomic, strong) IBOutlet UIView *vCallChat;
@property (nonatomic, strong) IBOutlet UIView *vPrefer;

// vToolBar
@property (nonatomic, strong) IBOutlet UIButton *btnMusic;
@property (nonatomic, strong) IBOutlet UIButton *btnMusicPlay;
@property (nonatomic, strong) IBOutlet UIButton *btnMusicNext;
@property (nonatomic, strong) IBOutlet UIButton *btnNavi;

// vTopDriverNavi200
@property (nonatomic, strong) IBOutlet UISlider *naviSlideToUnlock;
@property (nonatomic, strong) IBOutlet UIView *naviSlideContainer;
@property (nonatomic, strong) IBOutlet UIImageView *imgTopTitle;

// vTopDriverNavi210
@property (nonatomic, strong) IBOutlet UILabel *lbDriveInfo;
@property (nonatomic, strong) IBOutlet UILabel *lbDriveStatus;
@property (nonatomic, strong) IBOutlet UILabel *lbTopTitle;

// address View
@property (nonatomic, strong) IBOutlet UIView *dstLocationBG;
@property (nonatomic, strong) IBOutlet UITextField *dstLocationTextField;
@property (nonatomic, strong) IBOutlet UIButton *btnAddressClear;

// prefer view
@property (nonatomic, strong) IBOutlet UILabel *lbPrefer;
@property (nonatomic, strong) IBOutlet UIButton *btnPrefer;

// chat call view
@property (nonatomic, strong) IBOutlet UIButton *btnRider;
@property (nonatomic, strong) IBOutlet UIButton *btnRiderName;

@property (nonatomic, strong) IBOutlet UIButton *btnCall;
@property (nonatomic, strong) IBOutlet UIButton *btnCancel;

@property (nonatomic, strong) IBOutlet UIView *vSurge;
@property (nonatomic, strong) IBOutlet UIView *vTestMode;

// slide
@property (nonatomic, strong) IBOutlet UIView *vSlider;
@property (nonatomic, strong) IBOutlet UIView *vSlideContainer;
@property (nonatomic, strong) IBOutlet UISlider *slideToUnlock;
@property (nonatomic, strong) IBOutlet UIButton *slideLockButton;
@property (nonatomic, strong) IBOutlet UILabel *lbSlider;
@property (nonatomic, strong) IBOutlet UIImageView *imgSlideBackground;

// vehicle view
@property (nonatomic, strong) IBOutlet UIView *vVehicle;
@property (nonatomic, strong) IBOutlet UIView *vVehicleSelect;
@property (nonatomic, strong) IBOutlet UILabel *lbVehicleSelect;
@property (nonatomic, strong) IBOutlet UIScrollView  *vVehicleSelectView;
@property (nonatomic, strong) IBOutlet UIButton *btnVehicle;


#else
// view
@property (nonatomic, strong) IBOutlet UIView *vTopMainNavi;
@property (nonatomic, strong) IBOutlet UIView *vTopRiderNavi;
@property (nonatomic, strong) IBOutlet UIView *vMap;
@property (nonatomic, strong) IBOutlet UIView *vAddress;
//@property (nonatomic, strong) IBOutlet UIView *vFavorite;
@property (nonatomic, strong) IBOutlet UIView *vDriveStatus;
@property (nonatomic, strong) IBOutlet UIView *vCallChat;
@property (nonatomic, strong) IBOutlet UIView *vPrefer;
@property (strong, nonatomic) IBOutlet NARootTouchThroughView *vOrder;
//@property (nonatomic, strong) IBOutlet UIView *vOrder;
@property (nonatomic, strong) IBOutlet UIView *vOrderSelect;


// top main navi
@property (nonatomic, strong) IBOutlet UIButton *btnQna;
@property (nonatomic, strong) IBOutlet UIButton *btnSchedule;
@property (nonatomic, strong) IBOutlet UIButton *btnDriver;
@property (nonatomic, strong) IBOutlet UIImageView *imgTopTitle;

// top rider navi
@property (nonatomic, strong) IBOutlet UIButton *btnClose;
@property (nonatomic, strong) IBOutlet UILabel *lbTopTitle;
@property (nonatomic, strong) IBOutlet UIButton *btnMenu;

// address View
@property (nonatomic, strong) IBOutlet UIView *srcLocationBG;
@property (nonatomic, strong) IBOutlet UIView *dstLocationBG;
@property (nonatomic, strong) IBOutlet UITextField *srcLocationTextField;
@property (nonatomic, strong) IBOutlet UITextField *dstLocationTextField;
@property (nonatomic, strong) IBOutlet UIButton *btnAddressClear;

// favorite view
@property (nonatomic, strong) IBOutlet UIButton *btnFavorite1;
@property (nonatomic, strong) IBOutlet UIButton *btnFavorite2;
@property (nonatomic, strong) IBOutlet UIButton *btnFavorite3;
@property (nonatomic, strong) IBOutlet UIButton *btnFavoriteHome;
@property (nonatomic, strong) IBOutlet UIButton *btnFavoriteWork;
@property (nonatomic, strong) IBOutlet UIButton *btnFavoriteSetting;
@property (nonatomic, strong) IBOutlet UIButton *btnFavoriteLast;

@property (nonatomic, strong) IBOutlet UIView *vFavorite1;
@property (nonatomic, strong) IBOutlet UIView *vFavorite2;
@property (nonatomic, strong) IBOutlet UIView *vFavorite3;
@property (nonatomic, strong) IBOutlet UIView *vFavoriteHome;
@property (nonatomic, strong) IBOutlet UIView *vFavoriteWork;
@property (nonatomic, strong) IBOutlet UIView *vFavoriteSetting;
@property (nonatomic, strong) IBOutlet UIView *vFavoriteLast;

@property (nonatomic, strong) IBOutlet UILabel *lbFavorite1;
@property (nonatomic, strong) IBOutlet UILabel *lbFavorite2;
@property (nonatomic, strong) IBOutlet UILabel *lbFavorite3;
@property (nonatomic, strong) IBOutlet UILabel *lbFavoriteHome;
@property (nonatomic, strong) IBOutlet UILabel *lbFavoriteWork;
@property (nonatomic, strong) IBOutlet UILabel *lbFavoriteSetting;
@property (nonatomic, strong) IBOutlet UILabel *lbFavoriteLast;

// clone
@property (nonatomic, strong) IBOutlet UIView *vsFavorite;
@property (nonatomic, strong) IBOutlet UIView *vsFavoriteHome;


// prefer view
@property (nonatomic, strong) IBOutlet UILabel *lbPrefer;
@property (nonatomic, strong) IBOutlet UIButton *btnPrefer;

// drive status view
@property (nonatomic, strong) IBOutlet UILabel *lbDriveStatus;
@property (nonatomic, strong) IBOutlet UIImageView *imgDriver;
@property (nonatomic, strong) IBOutlet UIImageView *imgVehicle;
@property (nonatomic, strong) IBOutlet UIButton *btnimgDriver;
@property (nonatomic, strong) IBOutlet UIButton *btnimgVehicle;

@property (nonatomic, strong) IBOutlet UILabel *lbDriveri18n;
@property (nonatomic, strong) IBOutlet UILabel *lbVehicleModel;
@property (nonatomic, strong) IBOutlet UILabel *lbDriver;
@property (nonatomic, strong) IBOutlet UILabel *lbDriverRating;
@property (nonatomic, strong) IBOutlet UILabel *lbVehicle;
@property (nonatomic, strong) IBOutlet UIButton *btnTripShare;

// chat call view
@property (nonatomic, strong) IBOutlet UIButton *btnChat;
@property (nonatomic, strong) IBOutlet UIButton *btnCall;
@property (nonatomic, strong) IBOutlet UIButton *btnCancel;


// order view
//@property (nonatomic, strong) IBOutlet UIView  *vOrderSelectView;
@property (nonatomic, strong) IBOutlet UIScrollView  *vOrderSelectView;
@property (nonatomic, strong) IBOutlet UIView  *vOrderBottomView;
@property (nonatomic, strong) IBOutlet UILabel *lbCardInfo;
@property (nonatomic, strong) IBOutlet UILabel *lbCouponInfo;
@property (nonatomic, strong) IBOutlet UIButton *btnCoupon;
@property (nonatomic, strong) IBOutlet UIButton *btnRequest;

#endif


@property (strong, nonatomic) NSArray *countryObjects;
@property NSMutableDictionary *markers;

/*
 tag
 0: ui초기화 시작
 1: ui초기화 끝 -> 1이면 api 요청 필요(2로 바뀐다)
 2: 데이터 요청했음
 4: 데이터 수정필요
 9: 데이터 처리됨(완료) -> api요청 가능(2로 바뀐다)
 
 view인 경우 tag는 state(3)state2(2)
 예 : 12010 120/10
 */

-(void)onChangeUI;
-(void)pushUD150;
-(void)pushUD151;
-(void)didUpdateLocations:(CLLocation*)location;

@end
