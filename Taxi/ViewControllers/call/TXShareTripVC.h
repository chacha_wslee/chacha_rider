//
//  TXAskCardNumberVC.h
//  Taxi
//
//

#import <UIKit/UIKit.h>
#import "TXBaseViewController.h"
#import "UITextView+Placeholder.h"

@interface TXShareTripVC : TXBaseViewController <UITextViewDelegate> {
    
}

@property (strong, nonatomic) IBOutlet TXButton    *btnNext;
@property (strong, nonatomic) IBOutlet UILabel    *lbCategory;
@property (strong, nonatomic) IBOutlet UIButton    *btnCategory;
@property (nonatomic, strong) IBOutlet UITextField  *tfText;
@property (nonatomic, strong) IBOutlet UITextView  *tvText;


@end
