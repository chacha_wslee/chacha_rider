//
//  TXAskCardNumberVC.h
//  Taxi
//
//

#import <UIKit/UIKit.h>
#import "TXBaseViewController.h"
#import "UITextView+Placeholder.h"

@interface TXCouponVC : TXBaseViewController <UITextViewDelegate> {
    
}

@property (strong, nonatomic) IBOutlet TXButton    *btnNext;
@property (strong, nonatomic) IBOutlet UILabel    *lbCategory;
@property (nonatomic, strong) IBOutlet UITextField  *tfText;

@property (nonatomic, strong) UILabel  *calltfText;
@property (nonatomic, strong) NSString  *callText;

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil target:(NSString*)__target label:(UILabel*)__textfield;

@end
