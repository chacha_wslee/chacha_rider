//
//  TXAskCardNumberVC.m
//  Taxi

//

#import "TXShareTripVC.h"

#import "Validator.h"
#import "WToast.h"
//#import "CRToast.h"


#if __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_9_0
    @import Contacts;
    @import ContactsUI;
#else
    #import <AddressBook/AddressBook.h>
    #import <AddressBookUI/AddressBookUI.h>
#endif


@interface TXShareTripVC () <UITableViewDelegate, UITableViewDataSource,
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_9_0
CNContactPickerDelegate,
#else
ABPeoplePickerNavigationControllerDelegate,
#endif
ValidatorDelegate>{
    NSArray *dataArray;
    NSArray *_msgList;
    
    NSString *localeCountryCode;
}

@end

@implementation TXShareTripVC {
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [super configureBottomLine];
    UIView *line = (UIView*)[[super navigationView] viewWithTag:1500];
    CGRect rect = line.frame;
    rect.size.height = 2;
    line.frame = rect;
//    UILabel *label = (UILabel*)[[super navigationView] viewWithTag:1111];
//    label.tag = 2001;
//    label.text = LocalizedStr(@"Menu.Qna.title");
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    UIButton *btn = [Utils addBottomButton:LocalizedStr(@"Button.OK") target:self selector:@selector(onNaviButtonClick:) color:YES];
    btn.tag = 2300;
    [self.view addSubview:btn];
    
    localeCountryCode = [Utils getCountryCode];
    
    [_tfText becomeFirstResponder];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

-(void)dealloc {
    [self removeEventListeners];
}

-(void)configureStyles {
    [super configureStyles];
    
    [super navigationType01X:[UIImage imageNamed:@"btn_back_02"] centerText:LocalizedStr(@"Menu.ShareTrip.Alert.title")];
}

- (void)nextButtonOn:(BOOL)on
{
    UIButton *btn = [super.view viewWithTag:2300];
    btn.enabled = on;
    if (on) {
#ifdef _WITZM
        [btn setTitleColor:UIColorBasicText forState:UIControlStateNormal];
#elif defined _CHACHA
        [btn setTitleColor:UIColorBasicTextOn forState:UIControlStateNormal];
#endif
    }
    else
        [btn setTitleColor:UIColorBasicTextOff forState:UIControlStateNormal];
}

-(void) configure {
    [super configure];
    
    UIView *n = [super navigationView];
    NSInteger _y = n.frame.origin.y + n.frame.size.height;
    [n setBackgroundColor:HEXCOLOR(0x333333ff)];
    UILabel *lb = [n viewWithTag:1210];
    
    lb.textColor = [UIColor whiteColor];
    
    // 2. init event
    _msgList = @[
                 @[@"UD322",@""], // rider post
                 ];
    
    [self registerEventListeners];
    
    dataArray = @[];
    
    NSString *str = LocalizedStr(@"Menu.ShareTrip.Alert.text.input.placeholder");
    _tfText.placeholder = str;
#ifdef _WITZM
    NSAttributedString *attr = [[NSAttributedString alloc] initWithString:str attributes:@{ NSForegroundColorAttributeName : UIColorDefault }];
    _tfText.textColor = UIColorDefault;
#elif defined _CHACHA
    NSAttributedString *attr = [[NSAttributedString alloc] initWithString:str attributes:@{ NSForegroundColorAttributeName : [UIColor whiteColor] }];
    _tfText.textColor = [UIColor whiteColor];
#endif

    _tfText.attributedPlaceholder = attr;
    
    _lbCategory.text = LocalizedStr(@"Button.Address");
    
//    UIButton *btn = [self.view viewWithTag:2300];
//    CGRect frame = _tvText.frame;
//    frame.origin.y = _y + 22;
//    frame.size.height = self.view.frame.size.height - frame.origin.y - btn.frame.size.height - 20;
//    _tvText.frame = frame;
    //------------------------------------------------------------------------------------------------------------------------
//    ((UILabel *)[self.view viewWithTag:111]).text = LocalizedStr(@"Menu.Payment.List1.text1");
//    ((UILabel *)[self.view viewWithTag:121]).text = LocalizedStr(@"Menu.Payment.List1.text1");
    _tvText.textColor = UIColorDefault;
    _tvText.placeholder = LocalizedStr(@"Menu.ShareTrip.Alert.text2.input.placeholder");
    //------------------------------------------------------------------------------------------------------------------------
    
    self.view.backgroundColor = HEXCOLOR(0x333333ff);
}

-(void) registerEventListeners {
    for (NSArray *msg in _msgList) {
        [self->model addEventListener:self forEvent:[msg objectAtIndex:0] eventParams:nil];
    }
}

-(void) removeEventListeners {
    for (NSArray *msg in _msgList) {
        [self->model removeEventListener:self forEvent:[msg objectAtIndex:0]];
    }
}

#pragma mark - Logic
-(void)openPeoplePicker
{
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_9_0
    // iOS 9, 10, use CNContactPickerViewController
    CNContactPickerViewController *picker = [[CNContactPickerViewController alloc] init];
    picker.delegate = self;
    picker.displayedPropertyKeys = @[CNContactFamilyNameKey, CNContactGivenNameKey, CNContactPhoneNumbersKey];
    [self presentViewController:picker animated:YES completion:nil];
#else
    // iOS 8 Below, use ABPeoplePickerNavigationController
    ABPeoplePickerNavigationController *picker = [ABPeoplePickerNavigationController new];
    
    picker.peoplePickerDelegate = self;
    [self presentViewController:picker animated:YES completion:nil];
#endif
}

#if __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_9_0
//- (void)contactPicker:(CNContactPickerViewController *)picker didSelectContactProperty:(CNContactProperty *)contactProperty{
- (void)contactPicker:(CNContactPickerViewController *)picker didSelectContact:(CNContact *)contact{
    
    NSString *phoneStr = ((CNPhoneNumber *)(contact.phoneNumbers.lastObject.value)).stringValue;
    
    NSString *firstName  = contact.givenName;
    NSString *middleName = contact.middleName;
    NSString *lastName   = contact.familyName;
    
    _tfText.text = phoneStr;
    [self sendShareTrip:phoneStr first:firstName middle:middleName lastName:lastName];
}
#else
- (void)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker didSelectPerson:(ABRecordRef)person {
    
    ABMultiValueRef phone = (ABMultiValueRef) ABRecordCopyValue(person, kABPersonPhoneProperty);
    CFStringRef phoneID = ABMultiValueCopyValueAtIndex(phone, 0);
    NSString *phoneNumber = [NSString stringWithFormat:@"%@", phoneID];
    CFRelease(phoneID);
    CFRelease(phone);
    
    NSString *firstName;
    NSString *middleName;
    NSString *lastName;
    
    // get the first name
    firstName = (__bridge_transfer NSString *)ABRecordCopyValue(person, kABPersonFirstNameProperty);
    
    //get the middle name
    middleName = (__bridge_transfer NSString*)ABRecordCopyValue(person, kABPersonMiddleNameProperty);
    
    // get the last name
    lastName = (__bridge_transfer NSString *)ABRecordCopyValue(person, kABPersonLastNameProperty);
    
    [self sendShareTrip:phoneNumber first:firstName middle:middleName lastName:lastName];
}
#endif

-(void)sendShareTrip:(NSString*)phoneNumber first:(NSString*)firstName middle:(NSString*)middleName lastName:(NSString*)lastName
{
    NSArray *data = [[NSArray alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"countries" ofType:@"plist"]];
    
    NSString *dialcode = @"";
    // locale이용
    if (![phoneNumber isEqualToString:@""]) {
        for (NSDictionary *code in data) {
            if ([code[@"code"] isEqualToString:localeCountryCode]) {
                if ([phoneNumber rangeOfString:code[@"dial_code"]].location == NSNotFound) {
                    dialcode = code[@"dial_code"];
                    if ([phoneNumber rangeOfString:@"+"].location == NSNotFound) {
                        phoneNumber = [Utils NBPhoneNumber:[NSString stringWithFormat:@"%@%@", dialcode, phoneNumber] locale:localeCountryCode];
                        NSLog(@"phoneNumber:%@",phoneNumber);
                    }
                }
                break;
            }
        }
    }
    
    NSString *retrievedName;
    
    //set the name
    if (firstName != NULL && middleName != NULL && lastName != NULL)
    {
        retrievedName = [[NSString alloc] initWithFormat:@"%@ %@ %@",firstName,middleName,lastName];
    }
    
    if (firstName != NULL && middleName != NULL & lastName == NULL)
    {
        retrievedName = [[NSString alloc] initWithFormat:@"%@ %@",firstName, middleName];
    }
    
    if (firstName != NULL && middleName == NULL && lastName != NULL)
    {
        retrievedName = [[NSString alloc] initWithFormat:@"%@ %@",firstName,lastName];
    }
    
    if (firstName != NULL && middleName == NULL && lastName == NULL)
    {
        retrievedName = [[NSString alloc] initWithFormat:@"%@",firstName];
    }
    
    if (firstName == NULL && middleName != NULL && lastName != NULL)
    {
        retrievedName = [[NSString alloc] initWithFormat:@"%@ %@",middleName, lastName];
    }
    
    if (firstName == NULL && middleName != NULL && lastName == NULL)
    {
        retrievedName = [[NSString alloc] initWithFormat:@"%@",middleName];
    }
    
    if (firstName == NULL && middleName == NULL && lastName != NULL)
    {
        retrievedName = [[NSString alloc] initWithFormat:@"%@", lastName];
    }
    
    retrievedName = [Utils trimStringOnly:retrievedName];
    
    if ([phoneNumber isEqualToString:@""]) {
        [self showPopupShareAlert:phoneNumber name:retrievedName locale:localeCountryCode];
    }
    else {
        [self dismissViewControllerAnimated:NO completion:^(){
            
            [self showPopupShareAlert:phoneNumber name:retrievedName locale:localeCountryCode];
            
        }];
    }
}

-(void)showPopupShareAlert:(NSString*)phoneNumber name:(NSString*)retrievedName locale:(NSString*)__localeCountryCode {
    
    NSString *msg = @"";
    if ([phoneNumber isEqualToString:@""]) {
        msg = LocalizedStr(@"Menu.ShareTrip.Alert.text2");
    }
    else {
        msg = [NSString stringWithFormat:LocalizedStr(@"Menu.ShareTrip.Alert.text"),
               retrievedName,
               [Utils NBPhoneNumberRFC3966:phoneNumber locale:__localeCountryCode]];
    }
    
    _tfText.text = phoneNumber;
}

#pragma mark - Action
-(IBAction)onNaviButtonClick:(id)sender {
    UIButton *btn = (UIButton*)sender;
    if (btn.tag == 1100 || btn.tag == 1101) {
        [self removeEventListeners];
        [self.navigationController popViewControllerAnimated:YES];
    }
    else if(btn.tag == 2200) {
        [self openPeoplePicker];
    }
    else if(btn.tag == 2300) {
        [self next:nil];
    }
    
    
}

-(void)next:(id)sender {
    
    [self validateAction:sender];
    return;
}



#pragma mark - Event
-(void)onFail:(id)object error:(TXError *)error {
    
}

-(void)onEvent:(TXEvent *)event eventParams:(id)subscriptionParams {
    
    TXResponseDescriptor *descriptor = [event getEventProperty:TXEvents.Params.DESCRIPTOR];
    //TXRequestObj *request = [event getEventProperty:TXEvents.Params.REQUEST];
    
    if([event.name isEqualToString:@"UD322"]) {
        
        if(descriptor.success == true) {
            
            [self removeEventListeners];
            [self.navigationController popViewControllerAnimated:YES];
            
        } else {
            //[self alertError:LocalizedStr(@"String.Alert") message:descriptor.error];
            //        NSString *message = [TXCode2MsgTranslator messageForCode:descriptor.rlt];
            //        [self alertError:@"Error" message:message];
        }
        [self hideBusyIndicator];
    }
}

#pragma mark - Keyboard Notification Method
- (void)keyboardWillShow:(NSNotification*)note{
    NSDictionary *userInfo = note.userInfo;
    CGRect finalKeyboardFrame = [userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    NSTimeInterval animationDuration = [userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    UIButton *btn = [self.view viewWithTag:2300];
    float inputViewFinalYPosition = self.view.bounds.size.height - finalKeyboardFrame.size.height - btn.frame.size.height;
    CGRect inputViewFrame = btn.bounds;
    inputViewFrame.origin.y = inputViewFinalYPosition;
    
    [UIView animateWithDuration:animationDuration animations:^{
        btn.frame = inputViewFrame;
    }];
    
    // textview
    UIView *n = [super navigationView];
    NSInteger _y = n.frame.origin.y + n.frame.size.height;
    CGRect frame = _tvText.frame;
    //frame.origin.y = _y + 22;
    frame.size.height = self.view.frame.size.height - frame.origin.y - inputViewFrame.origin.y - 20;
    _tvText.frame = frame;
}

- (void)keyboardWillHide:(NSNotification*)note{
    
    NSDictionary *userInfo = note.userInfo;
    NSTimeInterval animationDuration = [userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    UIButton *btn = [self.view viewWithTag:2300];
    CGRect inputViewFrame = btn.bounds;
    inputViewFrame.origin.y = self.view.bounds.size.height - btn.frame.size.height;
    
    [UIView animateWithDuration:animationDuration animations:^{
        btn.frame = inputViewFrame;
    }];
    
    // textview
    UIView *n = [super navigationView];
    NSInteger _y = n.frame.origin.y + n.frame.size.height;
    CGRect frame = _tvText.frame;
    //frame.origin.y = _y + 22;
    frame.size.height = self.view.frame.size.height - frame.origin.y - btn.frame.size.height - 20;
    _tvText.frame = frame;
}

#pragma mark - UITableView DataSource Methods
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [tableView setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [tableView setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
    UIView *seperatorView = [[UIView alloc] initWithFrame:CGRectMake(10, cell.frame.size.height - 1, cell.frame.size.width - 10*2, 1)];
    seperatorView.backgroundColor = UIColorTableSeperator;
    [cell addSubview:seperatorView];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    // This will create a "invisible" footer
    return 0.01f;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [UIView new];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [dataArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    cell.tag = indexPath.row;
    int tag = (int)indexPath.row+100;
    //[self changeCell:tableView withIndexPath:indexPath andTag:tag];
    [self addCell:tableView withCell:cell withIndexPath:indexPath andTag:tag];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    cell.textLabel.text = [NSString stringWithFormat:@"      %@",[dataArray objectAtIndex:indexPath.row]];
    
    return cell;
}

#pragma mark - UITableView Delegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self changeCell:tableView withIndexPath:indexPath andTag:((int)indexPath.row+100)];
    
    //cell.accessoryView.hidden = NO;
}
- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self changeCell:tableView withIndexPath:indexPath andTag:((int)indexPath.row+100)];
    
    //cell.accessoryView.hidden = YES;
}

-(void)addCell:(UITableView*)tableView withCell:(UITableViewCell*)cell withIndexPath:(NSIndexPath*)indexPath andTag:(int)tag{
    
    UIImageView *preView = [cell.contentView viewWithTag:tag];
    [preView removeFromSuperview];
    
    UILabel *preLabel = [cell.contentView viewWithTag:(tag+100)];
    [preLabel removeFromSuperview];
    
    NSInteger _h = cell.frame.size.height;
    NSInteger _x = 16;
    UIImageView* imv = [[UIImageView alloc]initWithFrame:CGRectMake(cell.frame.size.width - _x*4 ,_h/2-_x/2, _x, _x)];
    imv.tag = tag;
    UIImage *cellImage;
    
    if([[tableView indexPathsForSelectedRows] containsObject:indexPath]) {
        cellImage = [UIImage imageNamed:@"form_radio_chk"];
    }
    else {
        cellImage = [UIImage imageNamed:@"form_radio_normal"];
    }
    imv.image=cellImage;
    [cell.contentView addSubview:imv];
}

-(void)changeCell:(UITableView*)tableView withIndexPath:(NSIndexPath*)indexPath andTag:(int)tag{
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    UIImageView *preView = [cell.contentView viewWithTag:tag];
    [preView removeFromSuperview];
    
    UILabel *preLabel = [cell.contentView viewWithTag:(tag+100)];
    [preLabel removeFromSuperview];
    
    NSInteger _h = cell.frame.size.height;
    NSInteger _x = 16;
    UIImageView* imv = [[UIImageView alloc]initWithFrame:CGRectMake(cell.frame.size.width - _x*4 ,_h/2-_x/2, _x, _x)];
    //    UIImageView* imv = [[UIImageView alloc]initWithFrame:CGRectMake(0,_h/2-_x/2, _x, _x)];
    imv.tag = tag;
    UIImage *cellImage;
    
    if([[tableView indexPathsForSelectedRows] containsObject:indexPath]) {
        cellImage = [UIImage imageNamed:@"form_radio_chk"];
    }
    else {
        cellImage = [UIImage imageNamed:@"form_radio_normal"];
    }
    imv.image=cellImage;
    [cell.contentView addSubview:imv];
}

#pragma mark -
#pragma mark Validator
#pragma mark -
-(void) resignKeyboard
{
    [self.view endEditing:YES];
}

- (IBAction)validateAction:(id)sender
{
    [self resignKeyboard];
#ifdef _WITZM
    if (self.tvText.text.length == 0) {
        [WToast showWithText:LocalizedStr(@"Validator.Qna.text") duration:kWTShort roundedCorners:NO gravity:kWTGravityTop];
        return;
    }
#elif defined _CHACHA
#endif
    Validator *validator = [[Validator alloc] init];
    validator.delegate   = self;
    [validator putRule:[Rules minLength:8 withFailureString:LocalizedStr(@"Validator.Telno.text") forTextField:self.tfText]];
    
    [validator validate];

//    [self onSuccess];
}

#pragma ValidatorDelegate - Delegate methods

- (void) preValidation
{
}

- (void)onSuccess
{
    NSString *pNumber = [Utils NBPhoneNumber:_tfText.text locale:localeCountryCode];
    NSLog(@"phoneNumber:%@",pNumber);
    [self showBusyIndicator:@"Requesting ... "];
    [self->model UD322:pNumber message:_tvText.text];
}

- (void)onFailure:(Rule *)failedRule
{
    [WToast showWithText:failedRule.failureMessage duration:kWTShort roundedCorners:NO gravity:kWTGravityTop];
}

@end
