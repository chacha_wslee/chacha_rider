//
//  TXMapVC.m
//  Taxi
//
#import <AVFoundation/AVFoundation.h>
#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import <CoreTelephony/CTCarrier.h>
#import <MessageUI/MessageUI.h>
#import "TXMapVC.h"
#import "TXGoogleRequestManager.h"
#import "utils.h"
#import "TXHttpRequestManager.h"
#import "StrConsts.h"
#import "NSString+TXNSString.h"
#import "TXCallModel.h"
#import "TXSignInVC.h"
#import <AudioToolbox/AudioServices.h>
#import "TXLauncherVC.h"
#import "TXappDelegate.h"
#import "TXTaxiModel.h"
//#import "MenuViewController.h"

//#import "PulsingHaloLayer.h"
#import "MenuNavigationController.h"
//#import "SVPulsingAnnotationView.h"
#import "LGAlertView.h"

//#import "TXSubscriptionVC.h"
#import "TXChargeVC.h"

#import "TXTripHistoryVC.h"
#import "TXNewsVC.h"

#ifdef _WITZM
    #ifdef _DRIVER_MODE
    #import "TXNewsDetailVC.h"
    #else
    #import "TXNewsAlertVC.h"
    #endif
#elif defined _CHACHA
    #import "TXNewsAlertVC.h"
#else
    #import "TXNewsDetailVC.h"
#endif

//#import "RedisSingleton.h"

#ifdef _DRIVER_MODE
#import "CMMapLauncher.h"
#import "TXBecomeVC.h"
#import "TXWaybillVC.h"
#import "TXEarningsVC.h"
#import "TXVehicleVC.h"
#import "TXProfileAccountVC.h"

#import "SpotifyLoginController.h"
#import "SpotifyViewController.h"

#import "TXAboutVC.h"

#import "TXBecomeVehicleVC.h"

#else
#import "TXAccountVC.h"
#import "TXPaymentVC.h"

//#import "TXCardVC.h"
#import "TXAskCardNumberVC.h"

#import "TXPromotionsVC.h"

//#import "TXOtherTripHistoryVC.h"
#import "TXFavoriteVC.h"
#import "TXShareTripVC.h"
#import "TXCouponVC.h"
#endif

#import "TXPreferVC.h"
#import "TXMapCallVC.h"
#import "TXCancelVC.h"
#import "TXQnaVC.h"

#import "TXFaqVC.h"
#import "TXInviteVC.h"

#import "TXSettingsVC.h"
#import "ChatView.h"

#import "Validator.h"
#import "WToast.h"
//#import "CRToast.h"

#import "NavigationController.h"
#import "WZLBadgeImport.h"
#import "UIButton+Badge.h"

@import UITextView_Placeholder;


/*
 "drive_trip_step" =     {
 duration = 23;
 edistance = "1.36"; (witz면 mile, chacha면 miter)
 eduration = "9.45"; 분
 ndistance = "0.05";
 nlocation = "37.402959,127.100909";
 ntime = 20;
 oseq = 3034;
 };
 */

#define TITLE_IMG (self->appDelegate.isDriverMode?@"img_top_logo_driver":@"img_top_logo")

#if defined (_CHACHA) || defined (_WITZM)
    // google map(chacha)
    #define PRINT_ETA(distance,eta)   ([eta isEqualToString:@"0"]?@"":[NSString stringWithFormat:@"%@ %@ %@ / %@ %@", LocalizedStr(@"Map.Drive.Pickup.ETA.text"), distance, LocalizedStr(@"String.Killo"), eta, LocalizedStr(@"String.Minute")]);

    #define DISTANCE_TEXT(val)              [NSString stringWithFormat:@"%.2f", (float)[val intValue]/1000]; // mile
    #define DURATION_TEXT(val)              [NSString stringWithFormat:@"%d",(int)round([val intValue]/60)];

    #define DISTANCE_TEXT_MAP_API(val)             [NSString stringWithFormat:@"%.2f", (float)([val floatValue])]; // mile
    #define DURATION_TEXT_MAP_API(val)             [NSString stringWithFormat:@"%d",(int)round([val intValue])];
#else
    // google map(witz)
    #define PRINT_ETA(distance,eta)         (([eta isEqualToString:@"0"]&&([distance isEqualToString:@"0"]||[distance isEqualToString:@"0.0"]))?@"":[NSString stringWithFormat:@"%@ %@ %@ / %@ %@", LocalizedStr(@"Map.Drive.Pickup.ETA.text"), distance, LocalizedStr(@"String.Mile"), ([eta isEqualToString:@"0"]?@"1":eta), LocalizedStr(@"String.Minute")]);

    #define DISTANCE_TEXT(val)              [NSString stringWithFormat:@"%.2f", (float)[val intValue]/1000]; // mile
    #define DURATION_TEXT(val)              [NSString stringWithFormat:@"%d",(int)round([val intValue]/60)];

    #define DISTANCE_TEXT_MAP_API(val)             [NSString stringWithFormat:@"%.2f", (float)([val floatValue])]; // mile
    #define DURATION_TEXT_MAP_API(val)             [NSString stringWithFormat:@"%d",(int)round([val intValue])];

#endif

const NSString *SPACE_BAR = @" ";

static CGFloat kTopOverlayHeight = 38.0f;
static CGFloat kOverlayHeight = 168.0f;
static float   kOverlayMovingTime = 0.5f;

@interface TXMapVC () <CLocationDelegate, TXEventListener, ValidatorDelegate, UIGestureRecognizerDelegate,
UITextViewDelegate> {
    NSMutableArray *items;
    TXGoogleRequestManager *googleReqMgr;
    NSString *country;
    //CLLocationCoordinate2D pickupCoordinate;
    
    NSArray *_msgList;
    
    //PulsingHaloLayer *halo;
    
    //MenuViewController *menuVC;
    
    NSMutableArray *screenMoveViews;
    
    // polyline
    NSMutableArray *tripPath;
    GMSPath *pathDynamic;
    // trip info
    NSArray *features;
    NSMutableArray *steps;
    NSMutableDictionary *stepsDic;
    NSInteger stepsIndex;
    
    // 주문접수 대기시간 timer
    NSTimer *animationTimer;
    // 지도이동 timer
    NSTimer *mapMoveTimer;
    NSDate *mapTouchMoveTimer;
    
    // 200화면 막음
    CALayer* containerLayer;
    UIView *containerImageView;
    
    // network montitor
    UIImageView *pNetworkMonitor;
    UILabel *pNetworkMonitorLabel;
    
    // 선택된 차량정보
    NSString *selectedVechicleInfo;
    
    // map loading
    NSInteger reDrawMapCount;
    
    // trip 사작여부
    NSInteger isTripCount;
    
    // trippath 변경시 검색결과 eta시간저장
    //NSString *changeEta;
    
    BOOL isBecomeActive;
    
    NSDate *bgTime;
    NSDate *drawLineTime; // direction api 재사용 시간 체크 // 10초안에 같은 요청을 막는다.
    
    ChatView *chatView;
    
    // TTS
    NSInteger ttspstate;
    NSInteger ttspstate2;
    
    // polygon
    NSMutableArray *overlays;
    
    // ployline geometry last index
    NSInteger geomIndex;
    
    // favorite dictionaly
    NSMutableDictionary *dicFavorite;
    
    TXMapCallVC *vcMapCall;
    TXCancelVC *vcCancel;
    TXChargeVC *vcCharge;
    
    // vehicle List
    NSArray *vehicleList;
    
    // position button
    TXButton* btnCpos;
    
    // map api report
    NSMutableDictionary *reportApi;
    
}
//@property int seconds;
//@property float distance;

@property (nonatomic, strong) NSArray *taxis;
@property (nonatomic, strong) NSMutableArray *locations;

-(IBAction)keyTyped:(id)sender;
-(IBAction)editProfile:(id)sender;

@end

@implementation TXMapVC {
    BOOL firstLocationUpdate_;
    
    // 카메라의 표적은 여백이 적용된 영역의 중심
    UIImageView *mImagePin;
    
    // UI상에 표시된 출발지, 목적지 -> SLAT,SLNG/DLAT,DLNG는 프로토콜상의 주소
    CLLocationCoordinate2D lsrc;
    CLLocationCoordinate2D ldst;
    
    // 현재위치 - drive 상태
    CLLocationCoordinate2D ldrv;
    
    UITextField *_textField;
    UITextView *_resultText;
    
    TXSettings  *settings;
    TXUser *user;
    
    NSDictionary *estimate_result;
    
    BOOL isRunTimeout_;
    // promotion code
    NSString* promoCode;
    // user name
    NSString* uName;
    //NSInteger serviceType; // 0:cue 1:cue+ 10:cue favorite 11:cue+ favorite
    NSString *serviceType;
    
    NSMutableArray *dataArray;
    
    // ET clock
    NSDate *etTimer;
    float _MAX_etTimer;
    BOOL _isEtTimerTimeout;
    NSDate *mapMoveTime;
    
    // step report
    NSString* _ndistance; // 최종 리포트 거리좌표
    int _lastReportTime; // 최종 리포트 시간
    
    int _lastRequestGoogleTrip; // google path api 사용 시간 -> 60초 마다 요청하기 위해
    
    BOOL isFirstLoading;
    
    // slider frame 하단
    float sliderDownRect;
    float sliderUpRect;
    
    // 자동 로그아웃
    BOOL isLogout;
    
    //CGPoint offset;
    
    // animationFade
    BOOL isAniTestMode;
    BOOL isAniAddress;
    
    BOOL isBCDFlag;
#ifndef _DRIVER_MODE
#ifdef _MAP_TOUCH_MOVE
    BOOL isMapMoved; // 맵이 움직였는지 여부(for 110)
#endif
#endif
}

float oldX, oldY;
BOOL dragging;
int etTime;
BOOL _isViewChanged;


#pragma mark View

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    
    if(self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        _isDriveMode = NO;
        _isViewChanged = NO;
        //        _is110_1 = NO;
        
        self->_MAX_etTimer = 0;
        self->_isEtTimerTimeout = NO;
        //self->_isTripLineClock = NO;
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    CATransition *applicationLoadViewIn =[CATransition animation];
    [applicationLoadViewIn setDuration:0.6];
    [applicationLoadViewIn setType:kCATransitionReveal];
    [applicationLoadViewIn setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn]];
    [[self.view layer]addAnimation:applicationLoadViewIn forKey:kCATransitionReveal];
    
    self->appDelegate.isMyCancel = NO;
    self->isFirstLoading = YES;
    isLogout = NO;
    
    //[appDelegate resetTripInfo];
    isBecomeActive = NO;
    chatView = nil;
    vcCancel = nil;
    vcCharge = nil;
    
    isAniTestMode = NO;
    isAniAddress = NO;
#ifndef _DRIVER_MODE
#ifdef _MAP_TOUCH_MOVE
    isMapMoved = NO;
#endif
#endif
    [self initUIDraw];
    overlays = [NSMutableArray array];
    dicFavorite = [NSMutableDictionary dictionary];
    vehicleList = [NSArray array];
    bgTask = [[BackgroundTask alloc] init];
    reportApi = [NSMutableDictionary dictionary];
    
    [self initReportApi];
    
    [appDelegate onTokenToServer];
    //[Utils setFirstLogin];
    
#ifdef _DEBUG_ACTIVITY
    self.activity.hidden = NO;
    _activity = [[UITextView alloc] initWithFrame:CGRectMake(0, kBasicHeight*2, self.view.frame.size.width, 160)];
    _activity.backgroundColor = HEXCOLOR(0xFFFFFF33);
    [self.mapView_ addSubview:_activity];
    //[self.mapView_.layer addSublayer:_activity.layer];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receivedNotification:) name:@"EDQueueJobDidSucceed" object:nil];
#endif
    
    
    
    //---------------------------------------------------------------------------------------------------------------------------------------------
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logout:) name:NOTIFICATION_LOGOUT object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationWillTerminate) name:UIApplicationWillTerminateNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didEnterBackground) name:UIApplicationDidEnterBackgroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didBecomeActive) name:UIApplicationDidBecomeActiveNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(clearDestAddress:) name:NOTIFICATION_110 object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reStore111:) name:NOTIFICATION_111 object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dismissVcCancel:) name:NOTIFICATION_DISSMISS_CANCEL object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onPD220) name:NOTIFICATION_DISSMISS_CALL object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onETAPrint) name:NOTIFICATION_ETA_PRINT object:nil];
    //---------------------------------------------------------------------------------------------------------------------------------------------
//#ifdef _DRIVER_MODE
    UIPanGestureRecognizer* panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panGestureHandler:)];
    panGesture.delegate = self;
    [self.mapView_ addGestureRecognizer:panGesture];
    UIPinchGestureRecognizer *pinch = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(pinchGestureHandler:)];
    panGesture.minimumNumberOfTouches = 1;
    pinch.delegate = self;
    [self.mapView_ addGestureRecognizer:pinch];
//    UIRotationGestureRecognizer *rotate = [[UIRotationGestureRecognizer alloc] initWithTarget:self action:@selector(panGestureHandler:)];
//    rotate.delegate = self;
//    [self.mapView_ addGestureRecognizer:rotate];
    
//    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(panGestureHandler:)];
//    tapGesture.numberOfTapsRequired = 1;
//    [tapGesture setDelaysTouchesBegan : YES];
//    tapGesture.delegate = self;
//    [self.mapView_ addGestureRecognizer:tapGesture];
    
//    UITapGestureRecognizer *dbTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(panGestureHandler:)];
//    dbTapGesture.numberOfTapsRequired = 2;
////    [tapGesture requireGestureRecognizerToFail : dbTapGesture];
//    [dbTapGesture setDelaysTouchesBegan : YES];
//
//    dbTapGesture.delegate = self;
//    [self.mapView_ addGestureRecognizer:dbTapGesture];
//#endif
    //Init Marker
    self.markers = [[NSMutableDictionary alloc] init];
    dataArray = [[NSMutableArray alloc] init];
    //tripPath = [[NSMutableArray alloc] init];
    
    //_placesClient = [[GMSPlacesClient alloc] init];
    
    //appDelegate = (TXAppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate debugActivityState];
    
    //menuVC = [[MenuViewController alloc] initWithNibName:@"MenuViewController" bundle:nil];
    
    [appDelegate menuViewDelegate:self];
    
    //--------------------------------------------------------------------------------------------------------------
    //--------------------------------------------------------------------------------------------------------------
    //    _srcLocationTextField.placeholder = LocalizedStr(@"Map.Address.SRC.placeholder.text");
    //    _dstLocationTextField.placeholder = LocalizedStr(@"Map.Address.DST.placeholder.text");
    
#ifndef _DRIVER_MODE
    _srcLocationTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:LocalizedStr(@"Map.Address.SRC.placeholder.text") attributes:@{ NSForegroundColorAttributeName : UIColorDefault2 }];
#endif
    
    _dstLocationTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:LocalizedStr(@"Map.Address.DST.placeholder.text") attributes:@{ NSForegroundColorAttributeName : UIColorButtonText }];
    
#ifdef _DRIVER_MODE
    
#ifdef _WITZM
#elif defined _CHACHA
#endif
    
#else
    
#ifdef _WITZM
    _dstLocationTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:LocalizedStr(@"Map.Address.DST.placeholder.text") attributes:@{ NSForegroundColorAttributeName : UIColorDefault2 }];
#elif defined _CHACHA
    _dstLocationTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:LocalizedStr(@"Map.Address.DST.placeholder.text") attributes:@{ NSForegroundColorAttributeName : UIColorDefault2 }];
#endif
    
#endif
    
    _btnAddressClear.hidden = YES;

    // app update check
    [Utils updateApp:NO];
    
    // change to share view
    [self openShareTripView];
    
    // side menu icon
    UIView *n = [super navigationView];
    UIButton *btn = [n viewWithTag:1101];
    if (btn.tag == 1101) {
        [btn setImage:[UIImage imageNamed:@"ic_sidemenu"] forState:UIControlStateNormal];
    }
    
    if (self->appDelegate.isDriverMode) {
        NSDictionary *account = [self->appDelegate.dicDriver objectForKey:@"provider_device"];
        if ([account[@"picture"] intValue]) {
            self->appDelegate.pictureSizeDriver = [NSString stringWithFormat:@"%d",[account[@"picture"] intValue]];
            [self onPP602];
        }
    }
    else {
        NSDictionary *account = [self->appDelegate.dicRider objectForKey:@"user_device"];
        if ([account[@"picture"] intValue]) {
            self->appDelegate.pictureSizeRider = [NSString stringWithFormat:@"%d",[account[@"picture"] intValue]];
            [self onUR601];
        }
    }
    

#ifdef _DRIVER_MODE
    if (self->appDelegate.isBecomeMode) {
        //[appDelegate onCommonErrorAlert:LocalizedStr(@"Menu.BecomeADriver.First.alert.text")];
        //[[[TXApp instance] getSettings] setFDKeychain:@"BCD_STEP" value:@"0"];
        //[self loadBecomeADriver];
        
        NSString *step = [Utils getBCDSTEP];
        
#ifdef _WITZM
        NSInteger __step = 6;
#elif defined _CHACHA
        NSInteger __step = 3;
#endif

        if ([step intValue] == __step) {
            [self loadNews];
        }
    }
    else {
        [self loadNews];
    }
#else
    [self loadNews];
#endif
    
//    // install event
//    [self configureEvent];
//    
//    if (self->appDelegate.isDriverMode) {
//        if (self->appDelegate.pstate == 200) {
//            [self onPP400];
//        }
//    }
//    else {
//        if (self->appDelegate.ustate == 120) {
//            [self->model UD131];
//        }
//        else if (self->appDelegate.ustate == 130) {
//            // UQ200
//            [self onUQ200];
//        }
//        else if (self->appDelegate.ustate == 140 && self->appDelegate.ustate2 == 10) {
//            // UQ300
//            [self onUQ300];
//        }
//    }
    
    [self onUpdateUI];
    [self changeMode];
    
#ifdef _DRIVER_MODE
    if (self->appDelegate.isBecomeMode) {
        //[appDelegate onCommonErrorAlert:LocalizedStr(@"Menu.BecomeADriver.First.alert.text")];
        //[[[TXApp instance] getSettings] setFDKeychain:@"BCD_STEP" value:@"0"];
        [self loadBecomeADriver];
    }
#endif
}


-(void)dealloc{
    [self removeEventListeners];
    [self removeObservers];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFICATION_LOGOUT object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillTerminateNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidEnterBackgroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFICATION_110 object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFICATION_111 object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFICATION_DISSMISS_CANCEL object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFICATION_DISSMISS_CALL object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFICATION_ETA_PRINT object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"NotificationCustomURLReceived" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"NotificationBackgroundFetchReceived" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"NotificationNetworkReachableReceived" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"NotificationPushReceived" object:nil];
}

-(void)applicationWillTerminate{
    [self sessionOut];
    
//    [self.locationMgr stopMonitoringSignificantLocationChanges];
//    [self.locationMgr stopUpdatingLocation];
    [appDelegate forceCompleteRequest];
    
    [bgTask stopBackgroundTask];
    
    //    if (self->appDelegate.isDriverMode && self->appDelegate.pstate == 210) {
    //        [self onPP135]; // 차량해제
    //    }
}

-(void)didEnterBackground{
    
    if (self->appDelegate.isDriverMode && self->appDelegate.pstate < 210) {
        //[self.locationMgr stopUpdatingLocation];
        [appDelegate forceCompleteRequest];
    }
    
    if (!self->appDelegate.isDriverMode && self->appDelegate.pstate < 120) {
        //[self.locationMgr stopUpdatingLocation];
        [appDelegate forceCompleteRequest];
    }
    
    if (![appDelegate isLoginState]) {
        return;
    }
    
    // background
    //[self.locationMgr stopUpdatingLocation];
    // init tts
    ttspstate = 0;
    ttspstate2 = 0;
    
    bgTime = [NSDate date]; // 현재시간 보관
    // 현재시간 저장
    [[[TXApp instance] getSettings] setFDKeychain:RIDER_ENTER_BACKGROUND_TIME value:[Utils getStringFromDate:bgTime]];
    
    if (self->appDelegate.isDriverMode && self->appDelegate.pstate == 200) {
        //        [self sessionOut];
        //[self applicationWillTerminate]; // 로그아웃을 막지 않는다
    }
    else if (!self->appDelegate.isDriverMode && self->appDelegate.ustate <= 110) {
        //[self sessionOut];
        //[self applicationWillTerminate]; // 로그아웃을 막지 않는다
    }
    
    //if (self->appDelegate.isDriverMode && !self->appDelegate.isBecomeMode && self->appDelegate.pstate >= 200) { //
    if (self->appDelegate.isDriverMode && !self->appDelegate.isBecomeMode && self->appDelegate.pstate > 200) { //
        //[self.locationMgr startMonitoringSignificantLocationChanges];
        [bgTask startBackgroundTasks:2 target:self selector:@selector(backgroundCallback:)];
    }
}

-(void)didBecomeActive{
    
    CGRect rect;
    rect = [[UIApplication sharedApplication] statusBarFrame];
    NSLog(@"Statusbar frame: %1.0f, %1.0f, %1.0f, %1.0f", rect.origin.x, rect.origin.y, rect.size.width, rect.size.height);
    if (rect.size.height == 20) {
        [super refreshView];
    }
    
    if (self->appDelegate.isDriverMode) {
        //[self startUpdatingLocation];
        [appDelegate startLocationUpdateSubscription];
    }
    else if (self->appDelegate.ustate < 120) {
        //[self startUpdatingLocation];
        [appDelegate startLocationUpdateSubscription];
    }
    
    if (![appDelegate isLoginState]) {
        return;
    }
    
    if (!self->appDelegate.isNetwork) {
        [self showBusyIndicator:@"Network.."];
    }
    [Utils updateApp:NO];
    
    isBecomeActive = YES;
    //[self.locationMgr stopMonitoringSignificantLocationChanges];
    //[self.locationMgr startUpdatingLocation];
    
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    
    [bgTask stopBackgroundTask];
    
//    if (!self->appDelegate.isDriverMode && self->appDelegate.ustate <= 110) {
//        [self pushCpos:btnCpos];
//    }
    
    //    if (self->appDelegate.isDriverMode) {
    //        if (self->appDelegate.pstate == 240 && self->appDelegate.pstate2 == 10) {
    //            //
    //            NSDictionary *pdevice = [self->appDelegate.dicDrive objectForKey:@"drive_trip"];
    //
    //            // ldst가 존재하면
    //            if (![[Utils nullToString:[pdevice objectForKey:@"ldst"]] isEqualToString:@""] && [_dstLocationTextField.text isEqualToString:@""]) {
    //                [self getRoute:[self currentPosition] dst:[self destinationPosition]];
    //
    //                NSArray* loc = [[pdevice objectForKey:@"ldst"] componentsSeparatedByString:@","];
    //
    //                // 위치
    //                CLLocationCoordinate2D position;
    //                position.latitude  = [self formattedPositionCoor:[[loc objectAtIndex:0] doubleValue]];
    //                position.longitude = [self formattedPositionCoor:[[loc objectAtIndex:1] doubleValue]];
    //
    //                [Utils setStateUIInfo:UI_STATE_END_ADDR value:UI_STATE_STEP_FIN oldvalue:nil];
    //                // 목적지 업데이트
    //                [self getDestinationAddress:position];
    //
    //                // eta 업데이트 __DEBUG
    //                //[self etaType3:@"" str5:[NSString stringWithFormat:@"ETA %@ min",[step__ objectForKey:@"ntime"]]];
    //            }
    //        }
    //    }
    //
    // forground
    // login
    //if (self->appDelegate.isDriverMode && self->appDelegate.pstate >= 200) {
    if (self->appDelegate.isDriverMode) {
        [self onPP110Auto];
    }
    else {
        // 현재시간 저장
        NSString* stime = [[[TXApp instance] getSettings] getFDKeychain:RIDER_ENTER_BACKGROUND_TIME];
        NSDate *chkTime = [Utils getDateFromString:stime];
        
        NSTimeInterval executionTime = [[NSDate date] timeIntervalSinceDate:chkTime]/60*60; //초
//        NSLog(@"executionTime:%f",executionTime);
        if (self->appDelegate.ustate <= 110 && executionTime>TIMEOUT_BG_TO_FG) {
            [self pushCpos:btnCpos];
        }
        else {//if (executionTime>TIMEOUT_BG_TO_FG) {
            //[self onUR110Auto];
            [self pushCpos:nil];
        }
        
        [self onUR110Auto];
    }
#ifdef _DRIVER_MODE
    [self updatePrefer:self->appDelegate.dicDriver[@"provider_matchs"]];
#else
    [self updatePrefer:self->appDelegate.dicRider[@"user_matchs"]];
#endif
    
    // 230.20 대기시간 출력
#ifdef _DRIVER_MODE
    if (self->appDelegate.pstate == 230 && self->appDelegate.pstate2 == 20) {
        [self updateETClock];
    }
#endif
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self->appDelegate.locationDelegate = self;
    
    if (self->appDelegate.isDebugMode) {
        self.activity.hidden = NO;
    }
    else {
        self.activity.hidden = YES;
    }
#ifdef _DRIVER_MODE
    self->appDelegate.spotifyDelegate = self;
    
    [self didChangeUISpotify:self->appDelegate.player.playbackState.isPlaying];
    
#endif
    
    if (!self->appDelegate.isNetwork) {
        [self showBusyIndicator:@"Network.."];
    }
    //[super statusBarStyleDefault];
    
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    locMgr.isMoveMapBYGesture = NO;
    _isViewChanged = NO;
    //[self addObservers];

//    if (![self->appDelegate.mapVCBeforeVC isEqualToString:@"TXDestinationsVC"] && ![self->appDelegate.mapVCBeforeVC isEqualToString:@"TXAskCardNumberVC"]) {
//        [self changeMode];
//    }
    
    /*
    // cancel 된 경우 charge로 넘어간다.
    if ([self->appDelegate.mapVCBeforeVC isEqualToString:@"TXCancelVC"]) {
        if ((self->appDelegate.ustate == 130 || self->appDelegate.ustate == 140) && self->appDelegate.ustate2 <= -10) {
            if (vcCharge == nil) {
                vcCharge = [[TXChargeVC alloc] initWithNibName:@"TXChargeVC" bundle:nil];
                [self pushViewController:vcCharge];
            }
        }
        else if ((self->appDelegate.pstate == 230 || self->appDelegate.pstate == 240) && self->appDelegate.pstate2 <= -10) {
            if (vcCharge == nil) {
                vcCharge = [[TXChargeVC alloc] initWithNibName:@"TXChargeDriverVC" bundle:nil];
                [self pushViewController:vcCharge];
            }
        }
    }
    */
    
#ifdef _DRIVER_MODE
    
    [self onPP602];
    
    [self reloadSelectedVehicleInfo];
    
    if ([self->appDelegate.mapVCBeforeVC isEqualToString:@"TXChargeVC"]) {
        // remove useq info
        [[TXCallModel instance] onCleanUseqSession];
        
        // testdrive인 경우 재시도 확인 창
        [self testModeRetryAlert];

        // prefer
        [self onPP206];
        
        if ([[Utils nullToString:[[[TXApp instance] getSettings] getDriveMode]] isEqualToString:DRIVE_MODE_DRIVING]) {
            [self callLoop:@"PQ100" withCancel:YES andDirect:YES];
        }
    }
    else if ([self->appDelegate.mapVCBeforeVC isEqualToString:@"TXVehicleVC"]) {
        if (self->appDelegate.pstate == 200) {
            [self onPP400];
        }
    }
    else if ([self->appDelegate.mapVCBeforeVC isEqualToString:@"TXCancelVC"] && self->appDelegate.ustate > 220) {
        [self onUpdateUI];
    }
    else if ([self->appDelegate.mapVCBeforeVC isEqualToString:@"TXMapCallVC"] && self->appDelegate.ustate > 210) {
        [self onUpdateUI];
    }
    else if ([self->appDelegate.mapVCBeforeVC isEqualToString:@"TXPreferVC"]) {
        [self onPP206];
    }
    
    if ((self->appDelegate.pstate == 230 || self->appDelegate.pstate == 240) && self->appDelegate.imgProfileRider == nil) {
        [self onPP601];
    }
    
    if ([self->appDelegate.mapVCBeforeVC isEqualToString:@"TXBecomeFirstVC"]
        || [self->appDelegate.mapVCBeforeVC isEqualToString:@"TXAboutVC"]
        || [self->appDelegate.mapVCBeforeVC isEqualToString:@"TXCheckrVC"]
        || [self->appDelegate.mapVCBeforeVC isEqualToString:@"TXBecomeVC"]
        || [self->appDelegate.mapVCBeforeVC isEqualToString:@"TXBecomeVehicleVC"]
        || [self->appDelegate.mapVCBeforeVC isEqualToString:@"TXBecomeFinishVC"]) {
        [self loadNews];
    }
    
#else
    
    [self onUR601];
    
    if ([self->appDelegate.mapVCBeforeVC isEqualToString:@"TXChargeVC"]) {
        // remove pseq info
        [[TXCallModel instance] onCleanPseqSession];
        // 출발지 리셋
        [self initAddressSrc];
        // prefer
        [self onUR206];
    }
    else if ([self->appDelegate.mapVCBeforeVC isEqualToString:@"TXCancelVC"] && self->appDelegate.ustate > 120) {
        [self onUpdateUI];
    }
    else if ([self->appDelegate.mapVCBeforeVC isEqualToString:@"TXMapCallVC"] && self->appDelegate.ustate > 110) {
        [self onUpdateUI];
    }
    else if ([self->appDelegate.mapVCBeforeVC isEqualToString:@"TXAskCardNumberVC"] && self->appDelegate.ustate <= 110) {
        //payment
        [self->model requsetAPI:@"UR300" property:nil];
    }
    else if ([self->appDelegate.mapVCBeforeVC isEqualToString:@"TXCardVC"] && self->appDelegate.ustate <= 110) {
        //payment
        [self->model requsetAPI:@"UR300" property:nil];
    }
    else if ([self->appDelegate.mapVCBeforeVC isEqualToString:@"TXPaymentVC"] && self->appDelegate.ustate <= 110) {
        //payment
        [self->model requsetAPI:@"UR300" property:nil];
    }
    else if (([self->appDelegate.mapVCBeforeVC isEqualToString:@"TXDestinationsVC"] || [self->appDelegate.mapVCBeforeVC isEqualToString:@"TXFavoriteVC"]) && self->appDelegate.ustate == 110) {
        //[self updateFavorite];
    }
    else if ([self->appDelegate.mapVCBeforeVC isEqualToString:@"TXPreferVC"]) {
        [self onUR206];
    }

    
    
    
    if ((self->appDelegate.ustate == 130 || self->appDelegate.ustate == 140) && self->appDelegate.imgProfileDriver == nil) {
        [self onUR602];
    }
    
    if ((self->appDelegate.ustate == 130 || self->appDelegate.ustate == 140) && self->appDelegate.imgVehicleDriver == nil) {
        [self onUR603];
    }
    
#endif
    
    [self checkLoop];
#ifndef _DRIVER_MODE
    if (!self->appDelegate.isDriverMode && self->appDelegate.ustate <= 110 && !self->isFirstLoading) {
        // 지도 marker찍기
        //if (pathDynamic != nil && ![self.dstLocationTextField.text isEqualToString:@""]) {
        if (![self.dstLocationTextField.text isEqualToString:@""]) {
            
            if (![self->appDelegate.mapVCBeforeVC isEqualToString:@"TXDestinationsVC"]) {
                //[self reDrawTripLine];
                
                // marker
                // 찍은 주소로 처리됨.
                CLLocationCoordinate2D scoordinate = [Utils srcPositionCoor];
                CLLocationCoordinate2D ecoordinate = [Utils destinationPositionCoor];
                
                INTULocationManager *locMgr = [INTULocationManager sharedInstance];
                locMgr.markerStart.map = nil;
                locMgr.markerFinish.map = nil;
                
                //locMgr.markerStart.snippet = [self srcAddress];
                [locMgr updateStartMarker:scoordinate];
                
                //locMgr.markerFinish.snippet = [self dstAddress];
                [locMgr updateFinishMarker:ecoordinate];
                
                NSValue *sValue = [NSValue valueWithMKCoordinate:scoordinate];
                NSValue *eValue = [NSValue valueWithMKCoordinate:ecoordinate];
                
                NSArray* __coords = [[NSArray alloc] initWithObjects:sValue, eValue, nil];
                
                [self updateMapFitBounds:__coords];
            }
            
        }
        else if (![_srcLocationTextField.text isEqualToString:@""] && ![_dstLocationTextField.text isEqualToString:@""]) {
            if (![self->appDelegate.mapVCBeforeVC isEqualToString:@"TXDestinationsVC"]) {
                [self updateRealStartEndAddress:YES];
            }
        }
    }
#endif
    
    if ([self->appDelegate.mapVCBeforeVC isEqualToString:@"TXChargeVC"]) {
        //[self startUpdatingLocation];
        [appDelegate startLocationUpdateSubscription];
        
        [self showBusyIndicator:@"Searching Location ... "];
#ifdef _WITZM
        [self performSelector:@selector(resetLocationNPosition) withObject:nil afterDelay:0.5];//2];
#elif defined _CHACHA
        [self performSelector:@selector(resetLocationNPosition) withObject:nil afterDelay:0.5];
#endif
        
    }
    else if (self->isFirstLoading) {
        //[self startUpdatingLocation];
        [appDelegate startLocationUpdateSubscription];
        
        [self showBusyIndicator:@"Searching Location ... "];
#ifdef _WITZM
        [self performSelector:@selector(resetLocationNPosition) withObject:nil afterDelay:0.5];//2];
#elif defined _CHACHA
        [self performSelector:@selector(resetLocationNPosition) withObject:nil afterDelay:0.5];
#endif
    }
    
    self->appDelegate.mapVCBeforeVC = @"";
    chatView = nil;
    //[self updateRightBadge:NO];
    
    if ((self->appDelegate.isDriverMode && self->appDelegate.pstate > 220) || (!self->appDelegate.isDriverMode && self->appDelegate.ustate > 120)) {
        [self showNewChat];
    }
    
    [self showAlrim];
    
    [self onUpdateData];

#ifndef _DRIVER_MODE
    if ([self->appDelegate.mapVCBeforeVC isEqualToString:@""] ||
        [self->appDelegate.mapVCBeforeVC isEqualToString:@"TXDestinationsVC"]) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [_vOrder viewWithTag:779].hidden = YES;
            [self pushCpos:nil];
        });
    }
#endif
    
#ifdef _DRIVER_MODE
    [self updatePrefer:self->appDelegate.dicDriver[@"provider_matchs"]];
#else
    [self updatePrefer:self->appDelegate.dicRider[@"user_matchs"]];
#endif
    //[[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
    
    self->isFirstLoading = NO;
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    _isViewChanged = YES;
    
#ifdef _DRIVER_MODE
    self->appDelegate.spotifyDelegate = nil;
#endif
    
    
    // 지속적으로 이벤트를 처리하기 위해
    //[self removeObservers];
    // Implement here if the view has registered KVO
    //[self clearMode];
}

//-(BOOL)prefersStatusBarHidden{
//    return NO;
//}


-(void) registerEventListeners {
    for (NSArray *msg in _msgList) {
        [self->model addEventListener:self forEvent:[msg objectAtIndex:0] eventParams:nil];
    }
}

-(void) removeEventListeners {
    for (NSArray *msg in _msgList) {
        [self->model removeEventListener:self forEvent:[msg objectAtIndex:0]];
    }
}

#ifdef _DRIVER_MODE
- (void) loadBecomeADriver {
    // become a driver
    //[[[TXApp instance] getSettings] setFDKeychain:@"BCD_STEP" value:@"1"];

    NSString *step = [Utils getBCDSTEP];
/*
    // 1드라이버신청전 : 신청,취소 provider_profile=null,provider_vehicles=null
    // 2드라이버신청후 : 확인 provider_profile!=null,provider_vehicles!=null
    NSDictionary *profile  = [self->appDelegate.dicDriver objectForKey:@"provider_profile"];
    NSDictionary *vehicles = [self->appDelegate.dicDriver objectForKey:@"provider_vehicles"];
    // 2
    if ([Utils isDictionary:profile] && [Utils isDictionary:vehicles]) {
        if ([step intValue] != 3) {
            step = @"3";
        }
    }
    // 비정상
    else if ([Utils isDictionary:profile] || [Utils isDictionary:vehicles]) {
        if ([step intValue] != 3) {
            step = @"0";
        }
    }
    // 1
    else {
        if ([step intValue] == 3) {
            step = @"0";
        }
    }
*/
    
#ifdef _WITZM
    if (step == nil || [step intValue] == 0) {
        TXBecomeFirstVC *mvc = [[TXBecomeFirstVC alloc] initWithNibName:@"TXBecomeFirstVC" bundle:nil];
        [self pushViewController:(TXBaseViewController *)mvc];
        return;
    }
    else if ([step intValue] == 4) {
        TXBecomeVC *mvc = [[TXBecomeVC alloc] initWithNibName:@"TXBecomeVC" bundle:nil];
        [self pushViewController:(TXBaseViewController *)mvc];
        // becomevc
        return;
    }
    else if ([step intValue] == 5) {
        TXBecomeVehicleVC *mvc = [[TXBecomeVehicleVC alloc] initWithNibName:@"TXBecomeVehicleVC" bundle:[NSBundle mainBundle] isnew:NO];
        [self pushViewController:(TXBaseViewController *)mvc];
        // becomevcvehicle
        return;
    }
    else if ([step intValue] == 6) {
        return;
    }
    
    TXCheckrVC *mvc = [[TXCheckrVC alloc] initWithNibName:@"TXCheckrVC" bundle:nil];
    mvc.setupMode = [step intValue];
    BOOL isAnimation = YES;
    if (mvc.setupMode >1) {
        isAnimation = NO;
    }
    [self.navigationController pushViewController:mvc animated:isAnimation];
#elif defined _CHACHA
    if (step == nil || [step intValue] == 0) { // intro
        //TXBecomeFirstVC *mvc = [[TXBecomeFirstVC alloc] initWithNibName:@"TXBecomeFirstVC" bundle:nil];
        /*
        TXAboutVC *mvc = [[TXAboutVC alloc] initWithNibName:@"TXAboutVC" bundle:nil step:0];
        [self pushViewController:(TXBaseViewController *)mvc];
         */
        __block LGAlertView *alertView = [[LGAlertView alloc] initWithTitle:LocalizedStr(@"Menu.BecomeADriver.First.title")
                                                                    message:nil
                                                                      style:LGAlertViewStyleAlert
                                                               buttonTitles:@[LocalizedStr(@"Button.Request")]
                                                          cancelButtonTitle:LocalizedStr(@"Button.CANCEL")
                                                     destructiveButtonTitle:nil
                                                              actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                                                                  [Utils setBCDSTEP:@"1"];
                                                                  TXBecomeVC *mvc = [[TXBecomeVC alloc] initWithNibName:@"TXBecomeVC" bundle:nil];
                                                                  [self pushViewController:(TXBaseViewController *)mvc];
                                                              }
                                                              cancelHandler:^(LGAlertView *alertView) {
                                                              }
                                                         destructiveHandler:^(LGAlertView *alertView) {
                                                         }];
        
        [Utils initAlertButtonColor:alertView];
        alertView.windowLevel = LGAlertViewWindowLevelAboveStatusBar;
        [alertView showAnimated:YES completionHandler:nil];
        return;
    }
    else if ([step intValue] == 1) { // 프로필UI
        TXBecomeVC *mvc = [[TXBecomeVC alloc] initWithNibName:@"TXBecomeVC" bundle:nil];
        [self pushViewController:(TXBaseViewController *)mvc];
        // becomevc
        return;
    }
    else if ([step intValue] == 2) { // 차량등록UI
        TXBecomeVehicleVC *mvc = [[TXBecomeVehicleVC alloc] initWithNibName:@"TXBecomeVehicleVC" bundle:[NSBundle mainBundle] isnew:NO];
        [self pushViewController:(TXBaseViewController *)mvc];
        // becomevcvehicle
        return;
    }
    else if ([step intValue] == 3) { // 등록요청완료UI
        if (!isBCDFlag) {
            //TXBecomeFinishVC *mvc = [[TXBecomeFinishVC alloc] initWithNibName:@"TXBecomeFinishVC" bundle:[NSBundle mainBundle]];
            TXAboutVC *mvc = [[TXAboutVC alloc] initWithNibName:@"TXAboutVC" bundle:nil step:3];
            [self.navigationController pushViewController:mvc animated:YES];
        }
        
        isBCDFlag = YES;
        return;
    }
#endif
}
#endif

- (void)loadNews {
    //공지사항
    @try {
        NSArray *news = nil;
        if (self->appDelegate.isDriverMode) {
            news = [self->appDelegate.dicDriver objectForKey:@"news_providers"];
        }
        else {
            news = [self->appDelegate.dicRider objectForKey:@"news_users"];
        }
        if ([Utils isArray:news]) {
            
            NSDate *date = [NSDate date];
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:DATE_FORMAT_YYYYMMDD];
            NSString *dateString = [dateFormatter stringFromDate:date];
            
            NSString* nseq = [[[TXApp instance] getSettings] getFDKeychain:@"NEWS_NSEQ"];

            if (![dateString isEqualToString:nseq]) {
                
#if defined (_CHACHA) || defined (_WITZM)
                TXNewsAlertVC *vc = [[TXNewsAlertVC alloc] initWithNibName:@"TXNewsAlertVC" bundle:nil];
                vc.view.frame = self.view.frame;
                
                vc.isPreOrder = YES;
                
                // clear background modal view
                vc.modalPresentationStyle =  UIModalPresentationCustom;
                //vc.transitioningDelegate = self;
                vc.modalPresentationCapturesStatusBarAppearance = YES;
                [vc setNeedsStatusBarAppearanceUpdate];
                if ([self.presentedViewController isKindOfClass:vc.class]) {
                    return;
                }
                // endof
                
                [self presentViewController:vc animated:NO completion:nil];

#else
                TXNewsDetailVC *vc = [[TXNewsDetailVC alloc] initWithNibName:@"TXNewsDetailVC" bundle:nil];
                vc.view.frame = self.view.frame;
                
                vc.isPreOrder = YES;
                [self presentViewController:vc animated:NO completion:nil];
#endif
                
            }
        }
    }
    @catch (NSException *exception) {
        NSLog(@"Exception: %@, %@", exception.name, exception.reason);
    }
}


#pragma mark - UpdateUI
- (void)onUpdateUI {
    int __state  = self->appDelegate.uistate;
    int __state2 = self->appDelegate.uistate2;
    
    int state__ = 0;
    int state2__ = 0;
    
    // state가 바뀌었으면 UI도 바뀐다.
    if (self->appDelegate.isDriverMode) {
        state__ =  self->appDelegate.pstate;
        state2__ =  self->appDelegate.pstate2;
    }
    else {
        state__ =  self->appDelegate.ustate;
        state2__ =  self->appDelegate.ustate2;
    }
    
    // 상태가 없는경우 에러처리
    if (state__ == 0 || (state__ == 0 && state2__ == 0)) {
        return;
    }
    
    if (__state != state__ || __state2 != state2__) {
        NSLog(@"======== onUpdateUI %d,%d -> %d,%d",__state,__state2,state__,state2__);
        [self onHideUI:__state state2:__state2];
        [self resetUIDraw];
        
        if (self->appDelegate.isDriverMode && __state >= 230 && state__ <= 220) {
            [self updateMapHeading];
        }
        
        [self onShowUI:state__ state2:state2__];
        
        // state변경 시 처리할 로직들
        if (self->appDelegate.isDriverMode) {
            // driver cancel UI
            if (__state >=230 && state__ <= 220) {
                [self isChangedCancelAlert:__state];
            }
            // 차량 조회
            else if (state__ == 200) {
                [self onPP400];
            }
        }
        else {
            // driver cancel UI
            if (__state >=130 && state__ <= 120) {
                [self isNotPikcupedAlert:__state];
            }
        }
        
        self->appDelegate.uistate  = state__;
        // 취소된경우 -10인 경우
        if (state2__ <= -10 && (state__ == 130 || state__ == 140 || state__ == 230 || state__ == 240)) {
            self->appDelegate.uistate2 = state2__;
        }
        else {
            if (state2__<0) state2__ = 0;
            self->appDelegate.uistate2 = state2__;
        }
        
        [self onUpdateData];
    }
    // state가 바뀌지 않았지만 data 가 바뀌면 해당 값을 변경한다.
    else {
        [self onUpdateData];
    }
    
    
}

- (void)hideView:(UIView*)v {
#ifndef _DRIVER_MODE
    if (v == _vDriveStatus) {
        NSLog(@"_vDriveStatus:%d",v.tag);
    }
#endif
    if (v.tag>0) {
        v.hidden = YES;
        v.tag = 2;
#ifdef _DRIVER_MODE
        if ([v isEqual:_vCallChat]) {
            _btnRider.hidden = YES;
        }
#endif
    }
}

-(void) rePositionPosButton {
    
#ifdef _DRIVER_MODE
    if (self->appDelegate.pstate <= 200) {
        CGRect rect = btnCpos.frame;
        rect.origin.y = self.view.frame.size.height - rect.size.height - 24;
        btnCpos.frame = rect;
        btnCpos.hidden = NO;
    }
    else if (self->appDelegate.pstate == 210) {
        CGRect rect = btnCpos.frame;
        rect.origin.y = self.view.frame.size.height - rect.size.height - 24;
        btnCpos.frame = rect;
        btnCpos.hidden = NO;
    }
    else if (self->appDelegate.pstate > 210) {
        CGRect rect = btnCpos.frame;
        rect.origin.y = _vCallChat.frame.origin.y - rect.size.height + 0;
        btnCpos.frame = rect;
        btnCpos.hidden = NO;
    }
#else
    if (self->appDelegate.ustate <= 110 || (self->appDelegate.ustate == 140 && self->appDelegate.ustate2 == 20)) {
        CGRect rect = btnCpos.frame;
        if (![_dstLocationTextField.text isEqualToString:@""]) {
            rect.origin.y = kTopHeight + _vTopMainNavi.frame.size.height + _vAddress.frame.size.height + 17;
        }
        else {
            rect.origin.y = kTopHeight + _vTopMainNavi.frame.size.height + 17;
        }
        btnCpos.frame = rect;
        btnCpos.hidden = NO;
    }
    else {
        CGRect rect = btnCpos.frame;
        rect.origin.y = kTopHeight + _vTopMainNavi.frame.size.height + _vAddress.frame.size.height + 17;
        btnCpos.frame = rect;
        btnCpos.hidden = NO;
    }
#endif
    
}

-(NSInteger) initvMap:(NSInteger)_y {
    UIView *view__ = self.mapView_;
    
    // view가 add되어 있으면
    if (view__.tag == 1) {
        view__.hidden = NO;
        CGRect frame = view__.frame;
        frame.origin.y = _y;
        frame.size.height = self.view.frame.size.height - _y;
        view__.frame = frame;
        
#ifdef _DRIVER_MODE
        [self hideFocusPin];
#else
#ifdef _MAP_TOUCH_MOVE
        [self showFocusPin];
#else
        [self hideFocusPin];
#endif
#endif
        return (view__.frame.origin.y + view__.frame.size.height);
    }
    else if (view__.tag == 2) { // hide
        CGRect frame = view__.frame;
        frame.origin.y = _y;
        frame.size.height = self.view.frame.size.height - _y;
        view__.frame = frame;
        
#ifdef _DRIVER_MODE
        [self hideFocusPin];
#else
#ifdef _MAP_TOUCH_MOVE
        [self showFocusPin];
#else
        [self hideFocusPin];
#endif
#endif
        return (view__.frame.origin.y + view__.frame.size.height);
    }
    else if (view__.tag == 3) { // show
        view__.hidden = NO;
        view__.tag = 1;
        
        CGRect frame = view__.frame;
        frame.origin.y = _y;
        frame.size.height = self.view.frame.size.height - _y;
        view__.frame = frame;
        
#ifdef _DRIVER_MODE
        [self hideFocusPin];
#else
#ifdef _MAP_TOUCH_MOVE
        [self showFocusPin];
#else
        [self hideFocusPin];
#endif
#endif
        return (view__.frame.origin.y + view__.frame.size.height);
    }
    
    //------------------------------------------------------------------------------------
    CGRect rect = view__.frame;
    rect.origin.y = _y;
    rect.size.width = self.view.bounds.size.width;
    rect.size.height = self.view.bounds.size.height - _y;
    view__.frame = rect;
    
    [self.view addSubview:view__];
    view__.tag = [self getStateTag];
    view__.hidden = NO;
    //------------------------------------------------------------------------------------
    [mImagePin removeFromSuperview];
    rect = view__.frame;
    CGPoint point = [self getCenterPointMapView:rect];
    rect = CGRectMake(point.x, point.y, kMarkerSizeW,kMarkerSizeH);
    
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    mImagePin = [[UIImageView alloc] initWithFrame:rect];
    mImagePin.contentMode = UIViewContentModeScaleAspectFit;
    mImagePin.image = [locMgr getMarkerIcon:@"route_start"];
    mImagePin.alpha = 1.0f;
    [self.mapView_ addSubview:mImagePin];
    
#ifdef _DRIVER_MODE
    [self hideFocusPin];
#else
#ifdef _MAP_TOUCH_MOVE
    [self showFocusPin];
#else
    [self hideFocusPin];
#endif
#endif
    
    //------------------------------------------------------------------------------------
    return (view__.frame.origin.y + view__.frame.size.height);
}

-(void)initMapView {

    GMSCameraPosition *camera = nil;
    
    //---------------------------------------------------------------------------------------------------------------------------------------------
    // 해당위치로 움직이지 않고 바로 이동
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    camera = [GMSCameraPosition cameraWithLatitude:locMgr.realTimeCLocation.coordinate.latitude
                                         longitude:locMgr.realTimeCLocation.coordinate.longitude
                                              zoom:[self getZoomByState]];
    
    NSLog(@"startLocationUpdateSubscription locMgr.realTimeCLocation.coordinate:%f,%f",locMgr.realTimeCLocation.coordinate.latitude,locMgr.realTimeCLocation.coordinate.longitude);
    
    self.mapView_ = [GMSMapView mapWithFrame:CGRectMake(0, 0, self.view.bounds.size.width,
                                                        self.view.bounds.size.height) camera:camera];
    self.mapView_.delegate = self;
    [self addObservers];
    
    [locMgr initMapInfo:self.mapView_];
    
    // map style
    //    NSBundle *mainBundle = [NSBundle mainBundle];
    //    NSURL *styleUrl = [mainBundle URLForResource:@"mapstyle" withExtension:@"json"];
    //    NSError *error;
    //
    //    // Set the map style by passing the URL for style.json.
    //    GMSMapStyle *style = [GMSMapStyle styleWithContentsOfFileURL:styleUrl error:&error];
    //
    //    if (!style) {
    //        NSLog(@"The style definition could not be loaded: %@", error);
    //    }
    //
    //    self.mapView_.mapStyle = style;
    
    
    self.mapView_.autoresizingMask = UIViewAutoresizingFlexibleWidth |
    UIViewAutoresizingFlexibleHeight |
    UIViewAutoresizingFlexibleBottomMargin;
    
    // setting
    self.mapView_.settings.rotateGestures = NO;
    self.mapView_.settings.compassButton = NO;
    self.mapView_.settings.myLocationButton = NO;
    self.mapView_.settings.indoorPicker = NO;
    self.mapView_.settings.scrollGestures = YES;
    self.mapView_.settings.allowScrollGesturesDuringRotateOrZoom = NO;
    
    // enable
    //    if (self->appDelegate.isDriverMode) {
    dispatch_async(dispatch_get_main_queue(), ^{
        //self.mapView_.myLocationEnabled = YES;
    });
    //    }
    
    [self.view addSubview:self.mapView_];
    [self.view sendSubviewToBack:self.mapView_];
    
    UIView *top = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, kTopHeight)];
    top.backgroundColor = [UIColor whiteColor];
    top.tag = 15;
    [self.mapView_ addSubview:top];
    //---------------------------------------------------------------------------------------------------------------------------------------------
    
    self->items = [NSMutableArray new];
    //self->googleReqMgr = [[TXGoogleRequestManager alloc] init];

    [self showCposButton];
    
    // manually gps location update
    [self didUpdateLocations];
}

-(void)updatePrefer:(NSArray*)array {
    //NSString *preferLang = @"";
    NSString *prefer = @"";
    
    for(NSDictionary *match in array) {
        /*
//#ifndef _CHACHA
        if ([match[@"name"] isEqualToString:@"language"]) {
            for(NSString *lang in match[@"values"]) {
                preferLang = [NSString stringWithFormat:@"%@    %@",preferLang, lang];
            }
        }
        else
#endif
         */
            if ([match[@"name"] isEqualToString:@"tag"]) {
                for(NSString *lang in match[@"values"]) {
                    prefer = [NSString stringWithFormat:@"%@    %@",prefer, lang];
                }
            }
    }
    // vprefer
    
#ifdef _WITZM
    _lbPrefer.text = [Utils trimStringOnly:[NSString stringWithFormat:@"%@",prefer]];
    //_lbPrefer.text = [Utils trimStringOnly:[NSString stringWithFormat:@"%@ %@",preferLang, prefer]];
#elif defined _CHACHA
    _lbPrefer.text = [Utils trimStringOnly:[NSString stringWithFormat:@"%@",prefer]];
#endif

}

-(NSInteger) initvPrefer:(NSInteger)_y {
    UIView *view__ = _vPrefer;
    
    // view가 add되어 있으면
    if (view__.tag == 1) {
        view__.hidden = NO;
        
        return (view__.frame.origin.y + view__.frame.size.height);
    }
    else if (view__.tag == 2) { // hide
        view__.hidden = YES;
        
        return (view__.frame.origin.y + view__.frame.size.height);
    }
    else if (view__.tag == 3) { // show
        view__.hidden = NO;
        view__.tag = 1;
        [self.view bringSubviewToFront:view__];
#ifdef _DRIVER_MODE
        [self updatePrefer:self->appDelegate.dicDriver[@"provider_matchs"]];
#else
        [self updatePrefer:self->appDelegate.dicRider[@"user_matchs"]];
#endif
        return (view__.frame.origin.y + view__.frame.size.height);
    }
    
    //------------------------------------------------------------------------------------
    CGRect rect = view__.frame;
    rect.origin.y = _y;
    rect.size.width = self.view.bounds.size.width;
    view__.frame = rect;
    
    [self.mapView_ addSubview:view__];
    view__.tag = [self getStateTag];
    view__.hidden = NO;
    //------------------------------------------------------------------------------------
    _lbPrefer.text = @"";
    _lbPrefer.textColor = HEXCOLOR(0x030303FF);
    
    //------------------------------------------------------------------------------------
    return (view__.frame.origin.y + view__.frame.size.height);
}

-(IBAction)onETAClick:(id)sender
{
    if (self->appDelegate.isDriverMode && (self->appDelegate.pstate == 230 && self->appDelegate.pstate2 == 20)) {
        // 대기시간일때는 동작을 막는다.
        return;
    }
    if (!self->appDelegate.isDriverMode && (self->appDelegate.ustate == 130 && self->appDelegate.ustate2 == 10)) {
        // 대기시간일때는 동작을 막는다.
        return;
    }
    UIButton *btn = (UIButton*)sender;
    if (btn.tag == 1) {
        btn.tag = 2;
    }
    else {
        btn.tag = 1;
    }
/*
    if (!self->appDelegate.isDriverMode && self->appDelegate.ustate == 110) {
        NSString *dist = DISTANCE_TO_TEXT(estimate_result[@"drive_estimates"][0][@"trip_distance"]);
        NSString *durt = DURATION_TO_TEXT(estimate_result[@"drive_estimates"][0][@"trip_duration"]);
        NSDictionary *__dic = @{
                                @"edistance": dist,
                                @"eduration": durt
                                };
        [self print_eta:__dic toggle:btn.tag];
    }
    else {
        [self print_eta:nil toggle:btn.tag];
    }
*/
    if (!self->appDelegate.isDriverMode && (self->appDelegate.ustate == 110 || self->appDelegate.ustate == 100)) {
        //[self print_eta:nil toggle:btn.tag api:YES];
        [self print_eta:nil toggle:0 api:YES];
    }
    else {
        //[self print_eta:nil toggle:btn.tag api:NO];
        [self print_eta:nil toggle:0 api:NO];
    }
}

-(void)print_eta:(NSDictionary*)driver_step toggle:(NSInteger)__toggle api:(BOOL)__api
{
    static NSDictionary* __dic = nil;
    static NSString* eta_time = @"";
    static NSString *eta = @"";
    static BOOL toggle = NO;
    static BOOL api = NO;
    
    if (__toggle == 9) {
        eta_time = @"";
        eta = @"";
        return;
    }
    // 기본값은 거리/소요시간
    //if (__toggle == 0) __toggle = 1;
//    if (__toggle==1) {
//        toggle = YES;
//    }
//    else if (__toggle==2) {
//        toggle = NO;
//    }
    
    if (_btnEta.tag==1) {
        toggle = YES;
    }
    else if (_btnEta.tag==2) {
        toggle = NO;
    }
    
    if (driver_step == nil) {
        //driver_step = __dic;
    }
    else {
        __dic = driver_step;
    }
    
    if (__api != api) {
        api = __api;
    }
    
    if (driver_step != nil) {
        // ETA
        NSString *dist = DISTANCE_TEXT(driver_step[@"edistance"]); // mile, km 5.5km
        NSString *durt = DURATION_TEXT(driver_step[@"eduration"]); // min
        if (!api) {
            dist = DISTANCE_TEXT_MAP_API(driver_step[@"edistance"]); // mile, km 5500m
            durt = DURATION_TEXT_MAP_API(driver_step[@"eduration"]); // min
        }

        // 정보가 없으면 무시한다.
        
        if ([[Utils numberCheck:driver_step[@"edistance"]] isEqualToString:@"0.00"] &&
            [[Utils numberCheck:driver_step[@"eduration"]] isEqualToString:@"0.00"]
            ) {
            // 처리하지 않는다.
        }
        else {
            if (([durt isEqualToString:@"0"] || [durt isEqualToString:@"0.0"]) && dist > 0) {
                durt = @"1";
            }
            
            if ([durt isEqualToString:@"0"] || [durt isEqualToString:@"0.0"]) {
                eta = @"";
            }
            else {
                eta = [NSString stringWithFormat:@"%@ %@ %@ / %@ %@",
                       LocalizedStr(@"Map.Drive.Pickup.ETA.text"),
                       dist,
                       LocalizedStr(@"String.Killo"),
                       durt,
                       LocalizedStr(@"String.Minute")];
                
                eta = [NSString stringWithFormat:@"%@ %@ / %@ %@",
                       dist,
                       LocalizedStr(@"String.Killo"),
                       durt,
                       LocalizedStr(@"String.Minute")];
                
                NSDate *etaTime = [[NSDate date] dateByAddingTimeInterval:[durt integerValue]*60];
                eta_time = [NSString stringWithFormat:@"%@ %@ %@ / %@ %@",
                            LocalizedStr(@"Map.Drive.Pickup.ETA.text"),
                            dist,
                            LocalizedStr(@"String.Killo"),
                            StringFormatDate2DestinationTime(etaTime),
                            @""];
                
                eta_time = [NSString stringWithFormat:@"%@ %@ / %@ %@",
                            dist,
                            LocalizedStr(@"String.Killo"),
                            StringFormatDate2DestinationTime(etaTime),
                            @""];
                ;
            }
        }
    }
//    else {
//        eta_time = @"";
//        eta = @"";
//        return;
//    }
    
    if (toggle) {
        if (![_lbTopTitle.text isEqualToString:eta]) {
            _lbTopTitle.text = eta;
        }
    }
    else {
        if (![_lbTopTitle.text isEqualToString:eta_time]) {
            _lbTopTitle.text = eta_time;
        }
    }
    
}

- (BOOL)onUpdateSrcAddr:(NSString*)srcGeoStr tts:(BOOL)ttsflag{
    
    BOOL isChangeFocus = NO;
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    if ([Utils isLocationValid:[Utils getLocation:srcGeoStr]] &&
        (
         ![Utils isEqualLocation:locMgr.markerStart.position location:[Utils getLocation:srcGeoStr]]
#ifndef _DRIVER_MODE
         || [_srcLocationTextField.text isEqualToString:@""]
#endif
         )
        ) {
        BOOL istts = NO;
        if ([Utils isLocationValid:locMgr.markerStart.position]) {
            istts = YES;
        }
#ifdef _DRIVER_MODE
        if (self->appDelegate.pstate == 230 && self->appDelegate.pstate2 == 10) { // on pickup
            // tripline 삭제
            [self cleanTripMapOnly];
        }
#else
        isChangeFocus = YES;
#endif
        // TTS
        if (istts && ![Utils isEqualLocation:locMgr.markerStart.position location:[Utils getLocation:srcGeoStr]]) {
            if (ttsflag) {
#ifdef _DRIVER_MODE
                [self playTTS:LocalizedStr(@"Map.Driver.SRC.Change.text") vol:1.0f];
#endif
            }
        }
        
        //
        // 픽업지 주소 엡데이트
        [self updateStartPosition:srcGeoStr];
        
        // 마커가 없으면
        if ([Utils nullToImage:locMgr.markerStart.icon]) {
            [self showMarker:locMgr.markerStart position:srcGeoStr icon:[locMgr getMarkerIcon:@"route_start"]];
        }
        else {
            [self showMarker:locMgr.markerStart position:srcGeoStr];
        }
        
        // 노티 얼러트
        [self changePickupNotofication];
    }
    
    return isChangeFocus;
}

- (void)onUpdateDestAddr:(NSString*)dstGeoStr {
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    NSLog(@"onUpdateDestAddr:%@,",dstGeoStr);
    NSLog(@"onUpdateDestAddr:%f,%f,",locMgr.markerFinish.position.latitude,locMgr.markerFinish.position.longitude);
    
    if ([Utils isLocationValid:[Utils getLocation:dstGeoStr]] &&
        (
         ![Utils isEqualLocation:locMgr.markerFinish.position location:[Utils getLocation:dstGeoStr]] ||
         [_dstLocationTextField.text isEqualToString:@""]
         )
        ) {
        BOOL istts = NO;
        if ([Utils isLocationValid:locMgr.markerFinish.position]) {
            istts = YES;
        }
        NSLog(@"");
        // TTS
        if (istts && [Utils isLocationValid:locMgr.markerFinish.position] && ![Utils isEqualLocation:locMgr.markerFinish.position location:[Utils getLocation:dstGeoStr]]) {
            [self playTTS:LocalizedStr(@"Map.Driver.DST.Change.text") vol:1.0f];
        }
        
        //
        // 도착지 주소 엡데이트
        [self updateFinishPosition:dstGeoStr];
        
        // 마커가 없으면
        if ([Utils nullToImage:locMgr.markerFinish.icon]) {
            [self showMarker:locMgr.markerFinish position:dstGeoStr icon:[locMgr getMarkerIcon:@"route_end"]];
        }
        else {
            [self showMarker:locMgr.markerFinish position:dstGeoStr];
        }
        
        // 노티 얼러트
        [self changeDestinationNotofication];
    }
}

-(void)changeStatusBarClear:(BOOL)flag
{
    UIView *v = [super statusView];
    if (flag) {
        [super backroundView].hidden = YES;
        v.backgroundColor = [UIColor clearColor];
    }
    else {
        [super backroundView].hidden = NO;
        v.backgroundColor = [UIColor whiteColor];
    }
}

- (void)didTapMyLocation {
    CLLocation *location = self.mapView_.myLocation;
    if (!location || ![GoogleUtils isLocationValid:location.coordinate]) {
        return;
    }
    
    self.mapView_.layer.cameraLatitude = location.coordinate.latitude;
    self.mapView_.layer.cameraLongitude = location.coordinate.longitude;
    self.mapView_.layer.cameraBearing = 0.0;
    
    // Access the GMSMapLayer directly to modify the following properties with a
    // specified timing function and duration.
    
    CAMediaTimingFunction *curve =
    [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    CABasicAnimation *animation;
    
    animation = [CABasicAnimation animationWithKeyPath:kGMSLayerCameraLatitudeKey];
    animation.duration = 2.0f;
    animation.timingFunction = curve;
    animation.toValue = @(location.coordinate.latitude);
    [self.mapView_.layer addAnimation:animation forKey:kGMSLayerCameraLatitudeKey];
    
    animation = [CABasicAnimation animationWithKeyPath:kGMSLayerCameraLongitudeKey];
    animation.duration = 2.0f;
    animation.timingFunction = curve;
    animation.toValue = @(location.coordinate.longitude);
    [self.mapView_.layer addAnimation:animation forKey:kGMSLayerCameraLongitudeKey];
    
    animation = [CABasicAnimation animationWithKeyPath:kGMSLayerCameraBearingKey];
    animation.duration = 2.0f;
    animation.timingFunction = curve;
    animation.toValue = @0.0;
    [self.mapView_.layer addAnimation:animation forKey:kGMSLayerCameraBearingKey];
    
    // Fly out to the minimum zoom and then zoom back to the current zoom!
    CGFloat zoom = self.mapView_.camera.zoom;
    NSArray *keyValues = @[@(zoom), @(kGMSMinZoomLevel), @(zoom)];
    CAKeyframeAnimation *keyFrameAnimation =
    [CAKeyframeAnimation animationWithKeyPath:kGMSLayerCameraZoomLevelKey];
    keyFrameAnimation.duration = 2.0f;
    keyFrameAnimation.values = keyValues;
    [self.mapView_.layer addAnimation:keyFrameAnimation forKey:kGMSLayerCameraZoomLevelKey];
}

-(void)initReportApi {
//    NSDictionary *dic = @{@"direction-google":"0",@"distance-google":"0",@"direction-tmap":"0",@"traffic-tmap":"0"};
//    reportApi = (NSMutableDictionary*)dic;
    reportApi[@"direction-google"] = @"0";
    reportApi[@"distance-google"] = @"0";
    reportApi[@"direction-tmap"] = @"0";
    reportApi[@"traffic-tmap"] = @"0";
}

-(NSString*)getReportApi {
#ifdef _WITZM
    NSString *navi = [NSString stringWithFormat:@"direction-tmap:%@,traffic-tmap:%@",
                      reportApi[@"direction-tmap"],reportApi[@"traffic-tmap"]];
#elif defined _CHACHA
    NSString *navi = [NSString stringWithFormat:@"direction-tmap:%@,traffic-tmap:%@",
                      reportApi[@"direction-tmap"],reportApi[@"traffic-tmap"]];
#else
    NSString *navi = [NSString stringWithFormat:@"direction-google:%@,distance-google:%@,direction-tmap:%@,traffic-tmap:%@",
                      reportApi[@"direction-google"],reportApi[@"distance-google"],reportApi[@"direction-tmap"],reportApi[@"traffic-tmap"]];
    if (self->appDelegate.isKorea) {
        navi = [NSString stringWithFormat:@"direction-tmap:%@,traffic-tmap:%@",
                          reportApi[@"direction-tmap"],reportApi[@"traffic-tmap"]];
    }
#endif
    return navi;
}

-(void)updateReportApi:(NSString*)navi {
    reportApi[navi] = [NSString stringWithFormat:@"%d", ([reportApi[navi] intValue] + 1)];
}

#ifdef _DRIVER_MODE
#pragma mark - hideUI - Driver

- (void)onHideUIp200 {
    if (self->appDelegate.pstate != 200) {
        [self hideView:_vTopDriverNavi200];
    }
    [self hideView:_vCallChat];
    [self hideView:_vPrefer];
    [self hideView:_vSlider];
    [self releaseLockIt];
    [self hideMarker];
    
    if (vcCharge != nil) {
        [self.navigationController popToRootViewControllerAnimated:YES];
        vcCharge = nil;
//        [vcCharge dismissViewControllerAnimated:YES completion:^(){
//            vcCharge = nil;
//        }];
    }
    //NSLog(@"-------------- HIDe 200");
}

- (void)onHideUIp210 {
    if (self->appDelegate.pstate != 210) {
        [self hideView:_vTopDriverNavi210];
    }
    [self hideView:_vAddress];
    [self hideView:_vCallChat];
    [self hideView:_vSlider];
    [self hideView:_vSurge];
    [self hideView:_vVehicle];
    [self hideMarker];
    
#ifdef _WITZM
#elif defined _CHACHA
    [self hideView:_vPrefer]; // add
#endif

    if (vcCharge != nil) {
        [self.navigationController popToRootViewControllerAnimated:YES];
        vcCharge = nil;
//        [vcCharge dismissViewControllerAnimated:YES completion:^(){
//            vcCharge = nil;
//        }];
    }
    //NSLog(@"-------------- HIDe 210");
}

- (void)onHideUIp220 {
    if (vcMapCall != nil) {
        if (!(self->appDelegate.pstate == 220 && self->appDelegate.pstate2 == 10)) {
            [vcMapCall dismissViewControllerAnimated:YES completion:^(){
                vcMapCall = nil;
            }];
        }
    }
    
    //NSLog(@"-------------- HIDe 220");
}

- (void)onHideUIp230 {
    if (vcMapCall != nil) {
        if (!(self->appDelegate.pstate == 220 && self->appDelegate.pstate2 == 10)) {
            [vcMapCall dismissViewControllerAnimated:YES completion:^(){
                vcMapCall = nil;
            }];
        }
    }
    
    if (self->appDelegate.pstate < 230 || (self->appDelegate.pstate == 240 && self->appDelegate.pstate2 == 20) || (self->appDelegate.pstate == 230 && self->appDelegate.pstate2 <= -10) || (self->appDelegate.pstate == 240 && self->appDelegate.pstate2 <= -10)) {
        [self hideView:_vTopDriverNavi2X0];
    }
    [self hideView:_vPrefer];
    
    // 취소버튼
    _btnCancel.enabled = NO;
    [_btnCancel setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    //NSLog(@"-------------- HIDe 230");
}

- (void)onHideUIp240 {
    //[self hideView:_vCallChat];
    
    // 취소버튼
    _btnCancel.enabled = NO;
    [_btnCancel setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    //NSLog(@"-------------- HIDe 240");
}

- (void)onHideUI:(NSInteger)state state2:(NSInteger)state2 {
    //UIView *n = [super navigationView];
    //NSInteger _y = n.frame.origin.y + n.frame.size.height;
    
    
    if (state == 200) {
        [self onHideUIp240];
        [self onHideUIp230];
        [self onHideUIp220];
        if (self->appDelegate.pstate != 220) {
            [self onHideUIp210];
            if (self->appDelegate.pstate != state) {
                [self onHideUIp200];
            }
        }
        
        //NSLog(@"-------------- HIDEUI 200");
    }
    else if (state == 210) { // wait
        [self onHideUIp240];
        [self onHideUIp230];
        [self onHideUIp220];
        if (self->appDelegate.pstate != 220) {
            if (self->appDelegate.pstate != state) {
                [self onHideUIp210];
            }
            [self onHideUIp200];
        }
        
        //NSLog(@"-------------- HIDEUI 210");
    }
    else if (state == 220 && state2 == 10) { // request
        if (self->appDelegate.pstate != state) {
            [self onHideUIp220];
        }
        [self onHideUIp210];
        [self onHideUIp200];
        
        //NSLog(@"-------------- HIDEUI 22010");
    }
    else if (state == 220 && state2 == 20) { // catch
        if (self->appDelegate.pstate != state) {
            [self onHideUIp220];
        }
        [self onHideUIp210];
        [self onHideUIp200];
        
        //NSLog(@"-------------- HIDEUI 22020");
    }
    else if (state == 230 && state2 == 10) { // on pickup
        if (self->appDelegate.pstate != state) {
            [self onHideUIp230];
        }
        [self onHideUIp220];
        [self onHideUIp210];
        [self onHideUIp200];
        
        //NSLog(@"-------------- HIDEUI 23010");
    }
    else if (state == 230 && state2 == 20) { // arrived
        if (self->appDelegate.pstate != state) {
            [self onHideUIp230];
        }
        [self onHideUIp220];
        [self onHideUIp210];
        [self onHideUIp200];
        
        _lbTopTitle.text = @""; // 대기시간 초기화
        //NSLog(@"-------------- HIDEUI 23020");
    }
    else if (state == 240 && state2 == 10) { // on trip
        
        if (self->appDelegate.pstate != state) {
            [self onHideUIp240];
        }
        [self onHideUIp230];
        [self onHideUIp220];
        [self onHideUIp210];
        [self onHideUIp200];
        
        //NSLog(@"-------------- HIDEUI 24010");
    }
    else if ((state == 240 && state2 == 20) || (state == 230 && state2 <= -10) || (state == 240 && state2 <= -10)) { // pay
        [self onHideUIp240];
        [self onHideUIp230];
        [self onHideUIp220];
        [self onHideUIp210];
        [self onHideUIp200];
        
        //NSLog(@"-------------- HIDEUI 24020");
    }
    else if (state == 0 && state2 == 0) { // 초기화
        [self onHideUIp240];
        [self onHideUIp230];
        [self onHideUIp220];
        
        if ((self->appDelegate.pstate == 230 || self->appDelegate.pstate == 240) && self->appDelegate.pstate2 == 10) {
            
        }
        else {
            [self onHideUIp210];
            [self onHideUIp200];
        }
        
        //NSLog(@"-------------- HIDEUI 24020");
    }
}

#pragma mark - showUI - Driver
- (void)onShowUIp200 {
    // show
    if (_vTopDriverNavi200.tag==2) _vTopDriverNavi200.tag = 3;
    if (_vToolBar.tag==2) _vToolBar.tag = 3;
    if (_vAddress.tag==2) _vAddress.tag = 3;
    
#ifdef _WITZM
    if (_vPrefer.tag==2) _vPrefer.tag = 3; //add
#elif defined _CHACHA
#endif

    if (_vSurge.tag==2) _vSurge.tag = 3;
    if (_vVehicle.tag==2) _vVehicle.tag = 3;
    
    //TXButton* btnCpos = [self.mapView_ viewWithTag:801];
    btnCpos.hidden = NO;
    
    //NSLog(@"-------------- SHOWUI 200");
}

- (void)onShowUIp210 {
    // show
    if (_vTopDriverNavi210.tag==2) _vTopDriverNavi210.tag = 3;
    if (_vToolBar.tag==2) _vToolBar.tag = 3;
    if (_vAddress.tag==2) _vAddress.tag = 3;
    
    //TXButton* btnCpos = [self.mapView_ viewWithTag:801];
    btnCpos.hidden = NO;
    
    //NSLog(@"-------------- SHOWUI 210");
}

- (void)onShowUIp230 {
    if (vcMapCall != nil) {
        if (!(self->appDelegate.pstate == 220 && self->appDelegate.pstate2 == 10)) {
            [vcMapCall dismissViewControllerAnimated:YES completion:^(){
                vcMapCall = nil;
            }];
        }
    }
    
    // show
    if (_vTopDriverNavi2X0.tag==2) _vTopDriverNavi2X0.tag = 3;
    if (_vToolBar.tag==2) _vToolBar.tag = 3;
    if (_vAddress.tag==2) _vAddress.tag = 3;
    
    if (_vCallChat.tag==2) _vCallChat.tag = 3;
    if (_vSlider.tag==2) _vSlider.tag = 3;
    
    //TXButton* btnCpos = [self.mapView_ viewWithTag:801];
    btnCpos.hidden = NO;
    
    // 취소버튼
    _btnCancel.enabled = YES;
    [_btnCancel setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    //NSLog(@"-------------- SHOWUI 230");
}

- (void)onShowUIp240 {
    // show
    if (_vTopDriverNavi2X0.tag==2) _vTopDriverNavi2X0.tag = 3;
    if (_vToolBar.tag==2) _vToolBar.tag = 3;
    if (_vAddress.tag==2) _vAddress.tag = 3;
    
    if (_vCallChat.tag==2) _vCallChat.tag = 3;
    if (_vSlider.tag==2) _vSlider.tag = 3;
    
    //TXButton* btnCpos = [self.mapView_ viewWithTag:801];
    btnCpos.hidden = NO;
    
    //NSLog(@"-------------- SHOWUI 240");
}

- (void)onShowUI:(NSInteger)state state2:(NSInteger)state2 {
    
    //TXButton* btnCpos = [self.mapView_ viewWithTag:801];
    
    //
    [self clearPolygon];
    //
    if ([[Utils nullToString:[[[TXApp instance] getSettings] getDriveMode]] isEqualToString:DRIVE_MODE_TRAINING]) {
        if (_vTestMode.tag==2) _vTestMode.tag = 3;
        [self hideView:_vSurge];
    }
    else {
        isAniTestMode = NO;
        [self hideView:_vTestMode];
    }
    
    if (self->appDelegate.pstate < 220) {
        // init profile
        [self initRiderProfileImage];
        
        // remove useq info
        [[TXCallModel instance] onCleanUseqSession];
        
        _lbTopTitle.text = @""; // 대기시간 초기화
        
        if (vcCancel != nil) {
            [self.navigationController popToRootViewControllerAnimated:YES];
            vcCancel = nil;
//            [vcCancel dismissViewControllerAnimated:NO completion:^(){
//                vcCancel = nil;
//                NSLog(@"vcCancel0");
//            }];
        }
        
        // hide drive marker
        [self hideDriveMarker];
        
        [self updateMapHeading];
        
        [Utils resetStateUIInfo];
        
        [self onPP206];
        
#ifdef _DRIVER_MODE
        [self updatePrefer:self->appDelegate.dicDriver[@"provider_matchs"]];
#else
        [self updatePrefer:self->appDelegate.dicRider[@"user_matchs"]];
#endif
    }
    
    if (state == 200) { // ready
        [self onShowUIp200];
        
        [self initvMap:0];
        
        [self cancelUpdateTripClock];
        
        [self initvTopDriverNavi200];
        
        NSInteger __height = _vTopDriverNavi200.frame.origin.y + _vTopDriverNavi200.frame.size.height;
        [self initvToolbar:__height];
        
#ifdef _WITZM
        __height = self.view.frame.size.height - _vPrefer.frame.size.height;
        [self initvPrefer:__height];
#elif defined _CHACHA
#endif

        [self initvAddress:[Utils frameUnderY:_vToolBar dim:9]];
        
        if ([[Utils nullToString:[[[TXApp instance] getSettings] getDriveMode]] isEqualToString:DRIVE_MODE_TRAINING]) {
            __height = _vAddress.frame.origin.y + _vAddress.frame.size.height + 10;
        }
        else {
            __height = _vAddress.frame.origin.y + _vAddress.frame.size.height + 10;
            [self initvSurge:__height];
            __height = _vSurge.frame.origin.y + _vSurge.frame.size.height + 30;
        }
        [self initvTestMode:__height];
        
        __height = self.view.frame.size.height + _vVehicle.frame.size.height;
        [self initvVehicle:__height];
        
        [self rePositionPosButton];
        
        // map padding
        [self updateMapPadding:state];
        
        if (!self.mapView_.myLocationEnabled) {
            self.mapView_.myLocationEnabled = YES;
        }
        
        //
        [self unblockDestinationAddress];
        
        // map 초기화
        [self cleanTripMapOnly];
        //[self cleanMapOnly];
        
        [self pushCpos:btnCpos];
        
        // 차량이미지 제거
        [self hideDriveMarker];
    }
    else if (state == 210) { // wait
        [self onShowUIp210];
        
        [self reloadSelectedVehicleInfo];
        
        [self initvMap:0];
        
        [self cancelUpdateTripClock];
        
        [self initvTopDriverNavi210];
        
        NSInteger __height = _vTopDriverNavi210.frame.origin.y + _vTopDriverNavi210.frame.size.height;
        [self initvToolbar:__height];
        
        [self initvAddress:[Utils frameUnderY:_vToolBar dim:9]];
        
        __height = _vAddress.frame.origin.y + _vAddress.frame.size.height + 30;
        [self initvTestMode:__height];
        
        [self rePositionPosButton];
        
        // map padding
        [self updateMapPadding:state];
        
        if (!self.mapView_.myLocationEnabled) {
            self.mapView_.myLocationEnabled = YES;
        }
        
        //[self initTripView];
        
        //
        [self blockDestinationAddress];
        
        // map 초기화
        [self cleanTripMapOnly];
        //[self cleanMapOnly];
        
        [self pushCpos:btnCpos];
        
        // 차량이미지 제거
        [self hideDriveMarker];
        
        if (self->appDelegate.imgProfileDriver == nil) {
            [self onPP602]; // driver profile
        }
        if (self->appDelegate.imgVehicleDriver == nil) {
            [self onPP603]; // driver vehicle
        }
    }
    else if (state == 220 && state2 == 10) { // request
        //[tripPath removeAllObjects];
        //ROUTE_DELETE;
        
        [self cancelUpdateTripClock];
        
        //
        [self blockDestinationAddress];
        
        
        NSDictionary *dic = [self->appDelegate.dicDriver objectForKey:@"drive_order"];
        if ([Utils isDictionary:dic]) {
            vcMapCall = [[TXMapCallVC alloc] initWithNibName:@"TXMapCallVC" bundle:nil];
            vcMapCall.view.frame = self.view.frame;
            [vcMapCall initLocation:dic[@"lsrc"] dest:dic[@"ldst"]];
            [self presentViewController:vcMapCall animated:YES completion:nil];
        }
        
        //
        [self releaseLockIt];
        
        if (self->appDelegate.imgProfileDriver == nil) {
            [self onPP602]; // driver profile
        }
        if (self->appDelegate.imgVehicleDriver == nil) {
            [self onPP603]; // driver vehicle
        }
    }
    else if (state == 220 && state2 == 20) { // catch
        [self releaseLockIt];
    }
    else if (state == 230 && state2 == 10) { // on pickup
        
        if (self->appDelegate.imgProfileDriver == nil) {
            [self onPP602]; // driver profile
        }
        if (self->appDelegate.imgVehicleDriver == nil) {
            [self onPP603]; // driver vehicle
        }
        
        // eta 거리/시간의 출력 기본값 변경
        _btnEta.tag = 1;
        
        [self onShowUIp230];
        
        [self initvMap:0];
        
        [self initvTopDriverNavi2X0];
        
        NSInteger __height = _vTopDriverNavi2X0.frame.origin.y + _vTopDriverNavi2X0.frame.size.height;
        [self initvToolbar:__height];
        
        [self initvAddress:[Utils frameUnderY:_vToolBar dim:9]];
        
        __height = _vAddress.frame.origin.y + _vAddress.frame.size.height + 30;
        [self initvTestMode:__height];
        
        __height = self.view.frame.size.height - _vCallChat.frame.size.height;
        [self initvCallChat:__height];
        
        [self rePositionPosButton];
        
        __height = self.view.frame.size.height - _vSlider.frame.size.height - 107;
        [self initvSlider:__height];
        
        // map padding
        [self updateMapPadding:state];
        
        if (!self.mapView_.myLocationEnabled) {
            self.mapView_.myLocationEnabled = YES;
        }
        
        //[self performSelector:@selector(pushCpos) withObject:nil afterDelay:1];
        [self pushCpos:btnCpos];
        
        self->appDelegate.isMyCancel = NO;
        
        //
        [self blockDestinationAddress];
        
        CLLocationDistance distance = GMSGeometryDistance([Utils currentPositionCoor], [Utils srcPositionCoor]);
        // 100m 이내면 보이지 않는다.
        if (distance <= 100) {
            // 설정된 네비게이션을 호출하시겠습니까?
            //[self onAutoChangeToNavigation];
        }
        
        // 차량이미지 교체
        INTULocationManager *locMgr = [INTULocationManager sharedInstance];
        [locMgr showDriveMarker:kCLLocationCoordinate2DInvalid];
    }
    else if (state == 230 && state2 == 20) { // arrived
        if (self->appDelegate.imgProfileDriver == nil) {
            [self onPP602]; // driver profile
        }
        if (self->appDelegate.imgVehicleDriver == nil) {
            [self onPP603]; // driver vehicle
        }
        
        
        [self onShowUIp230];
        
        [self initvMap:0];
        
        [self cancelUpdateTripClock];
        
        [self initvTopDriverNavi2X0];
        
        NSInteger __height = _vTopDriverNavi2X0.frame.origin.y + _vTopDriverNavi2X0.frame.size.height;
        [self initvToolbar:__height];
        
        [self initvAddress:[Utils frameUnderY:_vToolBar dim:9]];
        
        __height = _vAddress.frame.origin.y + _vAddress.frame.size.height + 30;
        [self initvTestMode:__height];
        
        __height = self.view.frame.size.height - _vCallChat.frame.size.height;
        [self initvCallChat:__height];
        
        [self rePositionPosButton];
        
        __height = self.view.frame.size.height - _vSlider.frame.size.height - 107;
        [self initvSlider:__height];
        
        // map padding
        [self updateMapPadding:state];
        
        if (!self.mapView_.myLocationEnabled) {
            self.mapView_.myLocationEnabled = YES;
        }
        
        [self pushCpos:btnCpos];
        
        //[tripPath removeAllObjects];
        //ROUTE_DELETE;
        
        // map 초기화
        [self cleanTripMapOnly];
        //[self cleanMapOnly];
        
        //
        [self blockDestinationAddress];
        
        // 차량이미지 제거
        [self hideDriveMarker];
    }
    else if (state == 240 && state2 == 10) { // on trip
        
        //_lbTopTitle.text = @""; // 대기시간 초기화
        
        if (self->appDelegate.imgProfileDriver == nil) {
            [self onPP602]; // driver profile
        }
        if (self->appDelegate.imgVehicleDriver == nil) {
            [self onPP603]; // driver vehicle
        }
        
        // eta 거리/시간의 출력 기본값 변경
        _btnEta.tag = 1;
        
        [self onShowUIp240];
        
        [self initvMap:0];
        
        [self initvTopDriverNavi2X0];
        
        NSInteger __height = _vTopDriverNavi2X0.frame.origin.y + _vTopDriverNavi2X0.frame.size.height;
        [self initvToolbar:__height];
        
        [self initvAddress:[Utils frameUnderY:_vToolBar dim:9]];
        
        __height = _vAddress.frame.origin.y + _vAddress.frame.size.height + 30;
        [self initvTestMode:__height];

        __height = self.view.frame.size.height - _vCallChat.frame.size.height;
        [self initvCallChat:__height];
        
        [self rePositionPosButton];
        
        __height = self.view.frame.size.height - _vSlider.frame.size.height - 107;
        [self initvSlider:__height];
        
        // map padding
        [self updateMapPadding:state];
        
        if (!self.mapView_.myLocationEnabled) {
            self.mapView_.myLocationEnabled = YES;
        }
        
        [self pushCpos:btnCpos];
        
        [self unblockDestinationAddress];
        
        CLLocationDistance distance = GMSGeometryDistance([Utils currentPositionCoor], [Utils destinationPositionCoor]);
        // 100m 이내면 보이지 않는다.
        if (distance <= 100) {
            // 설정된 네비게이션을 호출하시겠습니까?
            //[self onAutoChangeToNavigation];
        }
        
        // 차량이미지 교체
        INTULocationManager *locMgr = [INTULocationManager sharedInstance];
        [locMgr showDriveMarker:kCLLocationCoordinate2DInvalid];
    }
    else if ((state == 240 && state2 == 20) || (state == 230 && state2 <= -10) || (state == 240 && state2 <= -10)) { // pay
        // 110 처리됨 초기화 해야 함.
        //[self cleanEtaType];
        
        [self initvMap:0];
        
        [self cancelUpdateTripClock];
        
        // view가 바뀐다.
        _isViewChanged = YES;
        self->appDelegate.isMyCancel = YES;
        // 주소를 초기화 한다.
        [self initAddressDst];
        //[self initTripView];
        
        _lbDriveInfo.text = selectedVechicleInfo;
        [Utils resetStateUIInfo];
        
        [self removeBadge];
        
        //[self dismissVcCancel:nil];
        if (vcCancel != nil) {
            [self.navigationController popToRootViewControllerAnimated:YES];
            vcCancel = nil;
//            [vcCancel dismissViewControllerAnimated:NO completion:^(){
//                vcCancel = nil;
//                NSLog(@"vcCancel0");
//            }];
        }
        
        if (vcCharge == nil) {
            vcCharge = [[TXChargeVC alloc] initWithNibName:@"TXChargeDriverVC" bundle:nil];
            [self pushViewController:vcCharge];
            
            // 라이더 취소시 알림음과 진동처리
            [appDelegate playSound:SOUND_NOTI vibrate:YES];
            
            NSLog(@"appDelegate playSound");
            
            // 노티 얼러트
            [self changePickupCancelNotofication];
        }
        
        if (!self.mapView_.myLocationEnabled) {
            self.mapView_.myLocationEnabled = YES;
        }
        
//        // location 재시작
//        [self.locationMgr stopUpdatingLocation];
//        [self startUpdatingLocation];
        
        //[tripPath removeAllObjects];
        //ROUTE_DELETE;
        
        [self pushCpos:btnCpos];
        
        // map 초기화
        [self cleanTripMapOnly];
        //[self cleanMapOnly];
        
        [self onShowUI:210 state2:0];
        
        // 차량이미지 제거
        [self hideDriveMarker];
    }
    
    [self.mapView_ bringSubviewToFront:btnCpos];
    
    //[self writeToDebugFile];
}

#pragma mark - UpdateData - Driver
-(void)onAutoChangeToNavigation {
    // 설정된 네비게이션을 호출하시겠습니까?
    __block LGAlertView *alertView = [[LGAlertView alloc] initWithTitle:nil
                                                                message:LocalizedStr(@"Menu.Settings.Driver.Map.AutoPopup.text")
                                                                  style:LGAlertViewStyleAlert
                                                           buttonTitles:@[LocalizedStr(@"Button.OK")]
                                                      cancelButtonTitle:LocalizedStr(@"Button.CANCEL")
                                                 destructiveButtonTitle:nil
                                                          actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                                                              [self pushNavi];
                                                          }
                                                          cancelHandler:^(LGAlertView *alertView) {
                                                          }
                                                     destructiveHandler:^(LGAlertView *alertView) {
                                                     }];
    
    [Utils initAlertButtonColor:alertView];
    alertView.windowLevel = LGAlertViewWindowLevelAboveStatusBar;
    [alertView showAnimated:YES completionHandler:nil];
}

- (void)onFitMap {
    return;
    // fit
    /*
    if (self->appDelegate.pstate == 230 && self->appDelegate.pstate2 == 10) {
        if (self->appDelegate.pstateold == 220 && self->appDelegate.pstate2old == 20) {
            if (locMgr.markerDrive.map != nil) {
                [self updateVehicleFitBounds];
                self->appDelegate.pstate2old = 0;
            }
        }
        else if (self->appDelegate.pstateold == 0 && self->appDelegate.pstate2old == 0) {
            if (locMgr.markerDrive.map != nil) {
                [self updateVehicleFitBounds];
                self->appDelegate.pstateold = 220;
            }
        }
    }
    else if (self->appDelegate.pstate == 240 && self->appDelegate.pstate2 == 10) {
        if (self->appDelegate.pstateold == 230 && self->appDelegate.pstate2old == 20) {
            //[self updateVehicleFitBounds];
            [self pushCpos:btnCpos];
            self->appDelegate.pstate2old = 0;
        }
        else if (self->appDelegate.pstateold == 0 && self->appDelegate.pstate2old == 0) {
            //[self updateVehicleFitBounds];
            [self pushCpos:btnCpos];
            self->appDelegate.pstateold = 230;
        }
    }
     */
}

- (void)onUpdate230:(id)updata src:(NSString*)srcGeoStr dst:(NSString*)dstGeoStr {
    
    if ((self->appDelegate.pstate == 230 && self->appDelegate.pstate2 == 10) || (self->appDelegate.pstate == 240 && self->appDelegate.pstate2 == 10)) {
        
//        NSDictionary* driver_step = (NSDictionary*)updata;
//        // ETA
//        [self print_eta:driver_step toggle:0 api:NO];
    }
    else {
        if ([updata isKindOfClass:[NSString class]]) {
            NSString* eta  = (NSString*)updata;
            if (![_lbTopTitle.text isEqualToString:eta]) {
                _lbTopTitle.text = eta;
            }
        }
    }
}

-(void)onRepareUI {
    if (self->appDelegate.pstate <= 200) {
        if (_vTopDriverNavi200.hidden) {
            // ui reload
            self->appDelegate.pstate2 = 0;
            [self onUpdateUI];
        }
    }
    else if (self->appDelegate.pstate == 210) {
        if (_vTopDriverNavi210.hidden) {
            // ui reload
            self->appDelegate.pstate2 = 0;
            [self onUpdateUI];
        }
    }
    else if (self->appDelegate.pstate == 230 && self->appDelegate.pstate2 == 10) {
        if (_vTopDriverNavi2X0.hidden || ![_lbDriveStatus.text isEqualToString:LocalizedStr(@"Map.Driver.OnPickup.title")]) {
            // ui reload
            self->appDelegate.pstate2 = 0;
            [self onUpdateUI];
        }
    }
    else if (self->appDelegate.pstate == 230 && self->appDelegate.pstate2 == 20) {
        if (_vTopDriverNavi2X0.hidden || ![_lbDriveStatus.text isEqualToString:LocalizedStr(@"Map.Driver.PickupArrived.title")]) {
            // ui reload
            self->appDelegate.pstate2 = 0;
            [self onUpdateUI];
        }
    }
    else if (self->appDelegate.pstate == 240 && self->appDelegate.pstate2 == 10) {
        if (_vTopDriverNavi2X0.hidden || ![_lbDriveStatus.text isEqualToString:LocalizedStr(@"Map.Driver.OnTrip.title")]) {
            // ui reload
            self->appDelegate.pstate2 = 0;
            [self onUpdateUI];
        }
    }
}

- (void)onUpdateData {
    
    NSString* drvGeoStr = @"";
    NSString* srcGeoStr = @"";
    NSString* dstGeoStr = @"";
    NSString* service   = @"";
    NSString* uname     = @"";
    
    NSDictionary *drive_pickup      = [self->appDelegate.dicDriver objectForKey:@"drive_pickup"];
    NSDictionary *provider_device   = [self->appDelegate.dicDriver objectForKey:@"provider_device"];
    NSDictionary *user_device       = [self->appDelegate.dicDriver objectForKey:@"user_device"];
    NSDictionary *drive_pickup_step = [self->appDelegate.dicDriver objectForKey:@"drive_pickup_step"];
    
    NSDictionary *drive_trip        = [self->appDelegate.dicDriver objectForKey:@"drive_trip"];
    NSDictionary *drive_trip_step   = [self->appDelegate.dicDriver objectForKey:@"drive_trip_step"];
    
    //uname       = [Utils nameOfuname:user_device[@"name"]];
    
    if (self->appDelegate.pstate == 200) {
        NSDictionary *dic = [self->appDelegate.dicDriver objectForKey:@"provider_device"];
        if ([Utils isDictionary:dic]) {
            if (![[Utils nullToString:dic[@"ldst"]] isEqualToString:@""] && [_dstLocationTextField.text isEqualToString:@""]) {
                // where want to go
                [self getAddress:[Utils getLocation:dic[@"ldst"]] select:@selector(updateAddressTextfield:)];
            }
        }
        // tripline 삭제
        [self cleanTripMapOnly];
        // fit
        //[self updateMapPadding:self->appDelegate.pstate];
        return;
    }
    else if (self->appDelegate.pstate == 210) { // wait
        NSDictionary *dic = [self->appDelegate.dicDriver objectForKey:@"provider_device"];
        if ([Utils isDictionary:dic]) {
            if (![[Utils nullToString:dic[@"ldst"]] isEqualToString:@""] && [_dstLocationTextField.text isEqualToString:@""]) {
                // where want to go
                [self getAddress:[Utils getLocation:dic[@"ldst"]] select:@selector(updateAddressTextfield:)];
            }
        }
        // tripline 삭제
        [self cleanTripMapOnly];
        // fit
        //[self updateMapPadding:self->appDelegate.pstate];
        return;
    }
    else if (self->appDelegate.pstate == 220 && self->appDelegate.pstate2 == 10) { // request
        return;
    }
    else if (self->appDelegate.pstate == 220 && self->appDelegate.pstate2 == 20) { // catch
        _dstLocationTextField.text = @"";
        return;
    }
    else if (self->appDelegate.pstate == 230 && self->appDelegate.pstate2 == 10) { // on pickup
        
        drvGeoStr   = [Utils nullTolocation:[provider_device objectForKey:@"location"]];
        srcGeoStr   = [Utils nullTolocation:[drive_pickup objectForKey:@"lsrc"]];
        dstGeoStr   = [Utils nullTolocation:[drive_pickup objectForKey:@"ldst"]];

        service     = [Utils nullToString:drive_pickup[@"service"]];
        
        // ETA : drive_pickup_step.eduration, edistance : ETA 시간(분), 거리km
        //NSString* eta         = [NSString stringWithFormat:@"%d",[[drive_pickup_step objectForKey:@"eduration"] intValue]];
        
        // fit
        [self onFitMap];
        //[self updateMapPadding:self->appDelegate.pstate];
        
        // 주소변경 확인
        // 픽업지 이전 위치와 다르면
        [self onUpdateSrcAddr:srcGeoStr tts:YES];

        // ETA, 주소, DriverInfo
        //[self onUpdate230:drive_pickup_step src:srcGeoStr dst:dstGeoStr];
        
        // 타이머
        [self startETClock:_PICKUP_TIME_OUT withDate:@"5"];
        
        // 드리이브 위치
        //[updateDrivePosition];
        //[self updateDrivePosition:service uname:uname lsrc:drvGeoStr];
        //[self updateDrivePosition:service lsrc:drvGeoStr];
        
        if (self->appDelegate.imgProfileRider == nil) {
            [self onUR601]; // rider profile
        }
        
        // 차량 위치 업데이트 -> api응답에서 직접한다.
        // 지도에 라인이 없으면 다시 그린다.
        INTULocationManager *locMgr = [INTULocationManager sharedInstance];
        if (locMgr.tripline.map == nil) {
            [self drawRoute];
        }
        
        // slider의 상태를 확인하고 없으면 다시 그린다.
        [self updateSlider];
    }
    else if (self->appDelegate.pstate == 230 && self->appDelegate.pstate2 == 20) { // arrived
        // 출도착 변경처리 필요
        
        drvGeoStr   = [Utils nullTolocation:[provider_device objectForKey:@"location"]];
        srcGeoStr   = [Utils nullTolocation:[drive_pickup objectForKey:@"lsrc"]];
        dstGeoStr   = [Utils nullTolocation:[drive_pickup objectForKey:@"ldst"]];

        service     = [Utils nullToString:drive_pickup[@"service"]];
        
        // fit
        //[self onFitMap];
        //[self updateMapPadding:self->appDelegate.pstate];
        
        // 주소변경 확인
        // 픽업지 이전 위치와 다르면
        [self onUpdateSrcAddr:srcGeoStr tts:YES];
        
        // ETA, 주소, DriverInfo
        //[self onUpdate230:@"" src:srcGeoStr dst:dstGeoStr];
        
        // 타이머
        // ET는 update_date부터 0~5분 증가, 빨간색으로표시하고  5분이 되면 timeout처리 한다., 기사 marker는 사라진다. 경로 지운다. - 1분단위로 ET update
        //[self startETClock:_PICKUP_TIME_OUT withDate:[drive_pickup objectForKey:@"update_date"]];
        [self startETClock:_PICKUP_TIME_OUT withDate:[drive_pickup objectForKey:@"arrive_date"]];
        
        // 드리이브 위치
        //[self updateDrivePosition];
        //[self updateDrivePosition:service uname:uname lsrc:drvGeoStr];
        //[self updateDrivePosition:service lsrc:drvGeoStr];
        
        if (self->appDelegate.imgProfileRider == nil) {
            [self onUR601]; // rider profile
        }
        
        // tripline 삭제
        [self cleanTripMapOnly];
    }
    else if (self->appDelegate.pstate == 240 && self->appDelegate.pstate2 == 10) { // on trip
        // 출도착 변경처리 필요
        
        drvGeoStr   = [Utils nullTolocation:[provider_device objectForKey:@"location"]];
        srcGeoStr   = [Utils nullTolocation:[drive_trip objectForKey:@"begin_location"]];
        dstGeoStr   = [Utils nullTolocation:[drive_trip objectForKey:@"ldst"]];
        
        service     = [Utils nullToString:drive_trip[@"service"]];
        
        // fit
        //[self onFitMap];
        //[self updateMapPadding:self->appDelegate.pstate];
        
        // 주소변경 확인
        // 픽업지 이전 위치와 다르면
        //[self onUpdateSrcAddr:srcGeoStr tts:YES];
        [self onUpdateDestAddr:dstGeoStr];
        
        // ETA : drive_trip_step.eduration, edistance : ETA 시간(분), 거리km
        //NSString* eta         = [NSString stringWithFormat:@"%d",[[drive_trip_step objectForKey:@"eduration"] intValue]];
        
        // ETA, 주소, DriverInfo
        //[self onUpdate230:drive_trip_step src:srcGeoStr dst:dstGeoStr];
        
        // 타이머
        // ET는 update_date부터 0~5분 증가, 빨간색으로표시하고  5분이 되면 timeout처리 한다., 기사 marker는 사라진다. 경로 지운다. - 1분단위로 ET update
        [self startETClock:_PICKUP_TIME_OUT withDate:[drive_trip objectForKey:@"update_date"]];
        
        // 드리이브 위치
        //[self updateDrivePosition];
        //[self updateDrivePosition:service uname:uname lsrc:drvGeoStr];
        //[self updateDrivePosition:service lsrc:drvGeoStr];
        
        if (self->appDelegate.imgProfileRider == nil) {
            [self onUR601]; // rider profile
        }
        
        // 지도에 라인이 없으면 다시 그린다.
        INTULocationManager *locMgr = [INTULocationManager sharedInstance];
        if (locMgr.tripline.map == nil) {
#ifdef _DEBUG_ACTIVITY
            [self performSelectorOnMainThread:@selector(debugNotification:)
                                   withObject:[NSString stringWithFormat:@"drawRouteOnMAP onUpdateData240.10"]
                                waitUntilDone:false];
#endif

            [self drawRoute];
        }
    }
    else if ((self->appDelegate.pstate == 240 && self->appDelegate.pstate2 == 20) || (self->appDelegate.pstate == 230 && self->appDelegate.pstate2 <= -10) || (self->appDelegate.pstate == 240 && self->appDelegate.pstate2 <= -10)) { // pay
        // tripline 삭제
        [self cleanTripMapOnly];
        
        return;
    }
    
    [self onRepareUI];
}

#pragma mark - initView - Driver
-(void)updateTopDriverNavi200 {
    //_imgTopTitle.image = [UIImage imageNamed:TITLE_IMG];
    if ([[Utils nullToString:[[[TXApp instance] getSettings] getDriveMode]] isEqualToString:DRIVE_MODE_TRAINING]) {
        _vTopDriverNavi200.backgroundColor = UIColorDefault;
        UIView *top = [self.mapView_ viewWithTag:15];
        top.backgroundColor = UIColorDefault;
    }
    else {
        _vTopDriverNavi200.backgroundColor = [UIColor whiteColor];
        UIView *top = [self.mapView_ viewWithTag:15];
        top.backgroundColor = [UIColor whiteColor];
    }
}

-(void) initvTopDriverNavi200 {
    UIView *view__ = _vTopDriverNavi200;
    
    // view가 add되어 있으면
    if (view__.tag == 1) {
        view__.hidden = NO;
        [self.view bringSubviewToFront:view__];
        return;
    }
    else if (view__.tag == 2) { // hide
        return;
    }
    else if (view__.tag == 3) { // show
        view__.hidden = NO;
        view__.tag = 1;
        
        // 목적지를 지운다
        ldst = kCLLocationCoordinate2DInvalid;
        [self.view bringSubviewToFront:view__];
        [self updateTopDriverNavi200];
        return;
    }
    //------------------------------------------------------------------------------------
    //UIView *n = [super navigationView];
    NSInteger _y = kTopHeight;
    CGRect rect = view__.frame;
    rect.origin.y = _y;
    rect.size.width = self.view.bounds.size.width;
    view__.frame = rect;
    
    //[n addSubview:view__];
    [self.mapView_ addSubview:view__];
    view__.tag = [self getStateTag];
    view__.hidden = NO;
    //------------------------------------------------------------------------------------
    [self updateTopDriverNavi200];
    
    //[self.naviSlideToUnlock addTarget:self action:@selector(fadeLabel) forControlEvents:UIControlEventValueChanged];
    [self.naviSlideToUnlock addTarget:self action:@selector(UnLockIt) forControlEvents:UIControlEventTouchUpInside];
    [self.naviSlideToUnlock addTarget:self action:@selector(UnLockIt) forControlEvents:UIControlEventTouchCancel];
    [self.naviSlideToUnlock addTarget:self action:@selector(UnLockIt) forControlEvents:UIControlEventTouchUpOutside];
    //self.naviSlideToUnlock.backgroundColor = [UIColor clearColor];
    [self.naviSlideToUnlock setMinimumTrackImage:[UIImage new] forState:UIControlStateNormal];
    [self.naviSlideToUnlock setMaximumTrackImage:[UIImage new] forState:UIControlStateNormal];
    
    UIImage *myBadgedImage = [UIImage imageNamed:@"btn_gotoonline"];
    [self.naviSlideToUnlock setThumbImage: myBadgedImage forState:UIControlStateNormal];
    
    //self.naviSlideToUnlock.text = LocalizedStr(@"Map.Driver.Slider.Online.text");
    //self.naviSlideToUnlock.textColor = UIColorDefault;
    self.naviSlideToUnlock.value = 0.0;
    self.naviSlideToUnlock.enabled = YES;
    self.naviSlideToUnlock.tag = 200;
}

-(void)updateTopDriverNavi210 {
    _lbDriveInfo.text = selectedVechicleInfo;
    if ([[Utils nullToString:[[[TXApp instance] getSettings] getDriveMode]] isEqualToString:DRIVE_MODE_TRAINING]) {
        _vTopDriverNavi210.backgroundColor = UIColorDefault;
        UIView *top = [self.mapView_ viewWithTag:15];
        top.backgroundColor = UIColorDefault;
    }
    else {
        _vTopDriverNavi210.backgroundColor = [UIColor whiteColor];
        UIView *top = [self.mapView_ viewWithTag:15];
        top.backgroundColor = [UIColor whiteColor];
    }
}

-(void) initvTopDriverNavi210 {
    UIView *view__ = _vTopDriverNavi210;
    
    // view가 add되어 있으면
    if (view__.tag == 1) {
        view__.hidden = NO;
        [self updateTopDriverNavi210];
        [self.view bringSubviewToFront:view__];
        return;
    }
    else if (view__.tag == 2) { // hide
        return;
    }
    else if (view__.tag == 3) { // show
        view__.hidden = NO;
        view__.tag = 1;
        
        // 목적지를 지운다
        ldst = kCLLocationCoordinate2DInvalid;
        [self.view bringSubviewToFront:view__];
        [self updateTopDriverNavi210];
        return;
    }
    //------------------------------------------------------------------------------------
    //UIView *n = [super navigationView];
    NSInteger _y = kTopHeight;
    CGRect rect = view__.frame;
    rect.origin.y = _y;
    rect.size.width = self.view.bounds.size.width;
    view__.frame = rect;
    
    //[n addSubview:view__];
    [self.mapView_ addSubview:view__];
    view__.tag = [self getStateTag];
    view__.hidden = NO;
    //------------------------------------------------------------------------------------
    [self updateTopDriverNavi210];
}

-(void)updateTopDriverNavi2X0 {

    if (self->appDelegate.pstate == 230 && self->appDelegate.pstate2 == 10) {
        _lbTopTitle.text = @"";
        _lbTopTitle.textColor = UIColorDefault2;
        _lbDriveStatus.text = LocalizedStr(@"Map.Driver.OnPickup.title");
    }
    else if (self->appDelegate.pstate == 230 && self->appDelegate.pstate2 == 20) {
//        _lbTopTitle.text = @"";
//        _lbTopTitle.textColor = UIColorDefault2;
        _lbDriveStatus.text = LocalizedStr(@"Map.Driver.PickupArrived.title");
    }
    else if (self->appDelegate.pstate == 240 && self->appDelegate.pstate2 == 10) {
//        _lbTopTitle.text = @"";
        _lbTopTitle.textColor = UIColorDefault2;
        _lbDriveStatus.text = LocalizedStr(@"Map.Driver.OnTrip.title");
    }
    
    if ([[Utils nullToString:[[[TXApp instance] getSettings] getDriveMode]] isEqualToString:DRIVE_MODE_TRAINING]) {
        _vTopDriverNavi2X0.backgroundColor = UIColorDefault;
        UIView *top = [self.mapView_ viewWithTag:15];
        top.backgroundColor = UIColorDefault;
    }
    else {
        _vTopDriverNavi2X0.backgroundColor = [UIColor whiteColor];
        UIView *top = [self.mapView_ viewWithTag:15];
        top.backgroundColor = [UIColor whiteColor];
    }
}

-(void) initvTopDriverNavi2X0 {
    UIView *view__ = _vTopDriverNavi2X0;
    
    // view가 add되어 있으면
    if (view__.tag == 1) {
        view__.hidden = NO;
        [self updateTopDriverNavi2X0];
        [self.view bringSubviewToFront:view__];
        return;
    }
    else if (view__.tag == 2) { // hide
        return;
    }
    else if (view__.tag == 3) { // show
        view__.hidden = NO;
        view__.tag = 1;
        [self updateTopDriverNavi2X0];
        [self.view bringSubviewToFront:view__];
        return;
    }
    
    //------------------------------------------------------------------------------------
    //UIView *n = [super navigationView];
    NSInteger _y = kTopHeight;
    CGRect rect = view__.frame;
    rect.origin.y = _y;
    rect.size.width = self.view.bounds.size.width;
    view__.frame = rect;
    
    //[n addSubview:view__];
    [self.mapView_ addSubview:view__];
    view__.tag = [self getStateTag];
    view__.hidden = NO;
    //------------------------------------------------------------------------------------
    
    _lbDriveStatus.text = @"";
    _lbTopTitle.text = @"";
    
    [self updateTopDriverNavi2X0];
}

-(void) updateToolbar {
    if (self->appDelegate.pstate == 230 || (self->appDelegate.pstate == 240 && self->appDelegate.pstate2 == 10)) {
        _btnNavi.hidden = NO;
    }
    else {
        _btnNavi.hidden = YES;
    }
}

-(NSInteger) initvToolbar:(NSInteger)_y {
    UIView *view__ = _vToolBar;
    
    // view가 add되어 있으면
    if (view__.tag == 1) {
        view__.hidden = NO;
        
        CGRect frame = view__.frame;
        frame.origin.y = _y;
        view__.frame = frame;
        [self.view bringSubviewToFront:view__];
        [self updateToolbar];
        return (view__.frame.origin.y + view__.frame.size.height);
    }
    else if (view__.tag == 2) { // hide
        view__.hidden = YES;
        
        CGRect frame = view__.frame;
        frame.origin.y = _y;
        view__.frame = frame;
        [self.view bringSubviewToFront:view__];
        return (view__.frame.origin.y + view__.frame.size.height);
    }
    else if (view__.tag == 3) { // show
        view__.hidden = NO;
        view__.tag = 1;
        
        CGRect frame = view__.frame;
        frame.origin.y = _y;
        view__.frame = frame;
        [self.view bringSubviewToFront:view__];
        [self updateToolbar];
        return (view__.frame.origin.y + view__.frame.size.height);
    }
    
    //------------------------------------------------------------------------------------
    CGRect rect = view__.frame;
    rect.origin.y = _y;
    rect.size.width = self.view.bounds.size.width;
    view__.frame = rect;
    
    [self.mapView_ addSubview:view__];
    view__.tag = [self getStateTag];
    view__.hidden = NO;
    //------------------------------------------------------------------------------------
    
    [self updateToolbar];
    //------------------------------------------------------------------------------------
    return (view__.frame.origin.y + view__.frame.size.height);
}

-(void) updateAddressBG:(BOOL)flag
{
#ifdef _WITZM
    // src
    if (flag) {
        _dstLocationBG.backgroundColor = UIColorDefault;
        _dstLocationTextField.textColor = HEXCOLOR(0x2e2e2eCC);
    }
    // dst
    else {
        _dstLocationBG.backgroundColor = HEXCOLOR(0x000000CC);
        _dstLocationTextField.textColor = UIColorDefault;
    }
#elif defined _CHACHA
    // src
    if (flag) {
        _dstLocationBG.backgroundColor = UIColorDefault;
        _dstLocationTextField.textColor = UIColorButtonText;
        //_dstLocationBG.alpha = 0.9;
        _dstLocationBG.alpha = 1;
    }
    // dst
    else {
        _dstLocationBG.backgroundColor = UIColorDefault2;
        _dstLocationTextField.textColor = UIColorButtonText;
        //_dstLocationBG.alpha = 0.8;
        _dstLocationBG.alpha = 1;
    }
#endif

}

-(void) updateAddressTextfield:(id)address {
    
#ifdef _WITZM
    _dstLocationTextField.text = address[@"fullAddress"];
#elif defined _CHACHA
    _dstLocationTextField.text = address[@"fullAddress"];
#else
    //_dstLocationTextField.text = [Utils addressToString:address];//address.thoroughfare; //[address.lines firstObject];
#endif

}


-(void) updateAddress {
    if (self->appDelegate.pstate == 200) {
        [self updateAddressBG:NO];
        NSDictionary *dic = [self->appDelegate.dicDriver objectForKey:@"provider_device"];
        if ([Utils isDictionary:dic]) {
            int ldcnt = [dic[@"ldcnt"] intValue];
            int ldmax = [dic[@"ldmax"] intValue];
            _dstLocationTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:LocalizedStr(@"Map.Address.DST.Driver.placeholder.text"),ldcnt,ldmax]
                                                                                          attributes:@{ NSForegroundColorAttributeName : UIColorButtonText }];
            //provider_device.ldcnt, ldmax (ldcnt == ldmax 면 ui를 가린다.) - ok
            if (ldcnt >= ldmax) {
                [self hideView:_vAddress];
            }
            
            if ([[Utils nullToString:dic[@"ldst"]] isEqualToString:@""]) {
                _dstLocationTextField.text = @"";
            }
        }
        else {
            _dstLocationTextField.text = @"";
        }
        isAniAddress = NO;
    }
    else if (self->appDelegate.pstate == 210) {
        [self updateAddressBG:NO];
        NSDictionary *dic = [self->appDelegate.dicDriver objectForKey:@"provider_device"];
        if ([Utils isDictionary:dic]) {
            int ldcnt = [dic[@"ldcnt"] intValue];
            int ldmax = [dic[@"ldmax"] intValue];
            _dstLocationTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:LocalizedStr(@"Map.Address.DST.Driver.placeholder.text"),ldcnt,ldmax]
                                                                                          attributes:@{ NSForegroundColorAttributeName : UIColorButtonText }];
            
            if (ldcnt >= ldmax) {
                [self hideView:_vAddress];
            }
            
            if (![[Utils nullToString:dic[@"ldst"]] isEqualToString:@""]) {
                if (!isAniAddress) {
                    isAniAddress = YES;
                    [self animationFade:_vAddress];
                }
            }
            else {
                [self hideView:_vAddress];
                _dstLocationTextField.text = @"";
                isAniAddress = NO;
            }
        }
        else {
            [self hideView:_vAddress];
            _dstLocationTextField.text = @"";
            isAniAddress = NO;
        }
    }
    else if (self->appDelegate.pstate == 220 && self->appDelegate.pstate2 == 10) {
    }
    else if (self->appDelegate.pstate == 220 && self->appDelegate.pstate2 == 20) {
        isAniAddress = NO;
    }
    else if (self->appDelegate.pstate == 230 && self->appDelegate.pstate2 == 10) {
        isAniAddress = NO;
        [self updateAddressBG:YES];
        //_dstLocationTextField.text = @"";
        _dstLocationTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:LocalizedStr(@"String.Address.Searching.text") attributes:@{ NSForegroundColorAttributeName : UIColorButtonText }];
    }
    else if (self->appDelegate.pstate == 230 && self->appDelegate.pstate2 == 20) {
        [self updateAddressBG:YES];
        _dstLocationTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:LocalizedStr(@"String.Address.Searching.text") attributes:@{ NSForegroundColorAttributeName : UIColorButtonText }];
    }
    else if (self->appDelegate.pstate == 240 && self->appDelegate.pstate2 == 10) {
        [self updateAddressBG:NO];
        //_dstLocationTextField.text = @"";
        _dstLocationTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:LocalizedStr(@"String.Address.Searching.text") attributes:@{ NSForegroundColorAttributeName : UIColorButtonText }];
    }
    else if ((self->appDelegate.pstate == 240 && self->appDelegate.pstate2 == 20) || (self->appDelegate.pstate == 230 && self->appDelegate.pstate2 <= -10) || (self->appDelegate.pstate == 240 && self->appDelegate.pstate2 <= -10)) {
        [self updateAddressBG:NO];
        _dstLocationTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"" attributes:@{ NSForegroundColorAttributeName : UIColorButtonText }];
    }
}

-(NSInteger) initvAddress:(NSInteger)_y {
    UIView *view__ = _vAddress;
    
    // view가 add되어 있으면
    if (view__.tag == 1) {
        view__.hidden = NO;
        
        CGRect frame = view__.frame;
        frame.origin.y = _y;
        view__.frame = frame;
        [self.view bringSubviewToFront:view__];
        [self updateAddress];
        return (view__.frame.origin.y + view__.frame.size.height);
    }
    else if (view__.tag == 2) { // hide
        view__.hidden = YES;
        
        CGRect frame = view__.frame;
        frame.origin.y = _y;
        view__.frame = frame;
        [self.view bringSubviewToFront:view__];
        return (view__.frame.origin.y + view__.frame.size.height);
    }
    else if (view__.tag == 3) { // show
        view__.hidden = NO;
        view__.tag = 1;
        
        CGRect frame = view__.frame;
        frame.origin.y = _y;
        view__.frame = frame;
        [self updateAddress];
        [self.view bringSubviewToFront:view__];
        return (view__.frame.origin.y + view__.frame.size.height);
    }
    
    //------------------------------------------------------------------------------------
    CGRect rect = view__.frame;
    rect.origin.y = _y;
    rect.size.width = self.view.bounds.size.width;
    view__.frame = rect;
    
    [self.mapView_ addSubview:view__];
    view__.tag = [self getStateTag];
    view__.hidden = NO;
    //------------------------------------------------------------------------------------
    _dstLocationTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    
    [self updateAddress];
    [Utils addShadow:_dstLocationBG];
    //------------------------------------------------------------------------------------
    return (view__.frame.origin.y + view__.frame.size.height);
}

-(void) updateSlider {
    if (self->appDelegate.pstate == 230 && self->appDelegate.pstate2 == 10) {
        if (_imgSlideBackground.tag != 23010) {
            UIImage *myBadgedImage = [UIImage imageNamed:@"icon_slide_arrived"];
            [self.slideToUnlock setThumbImage: myBadgedImage forState:UIControlStateNormal];
            self.slideToUnlock.value = 0.0;
            _imgSlideBackground.image = [UIImage imageNamed:@"bg_slide_arrived"];
            _imgSlideBackground.tag = 23010;
        }
        
        if (!self.vSlider.hidden) {
            self.vSlider.alpha = 1.0;
        }
    }
    else if (self->appDelegate.pstate == 230 && self->appDelegate.pstate2 == 20) {
        if (_imgSlideBackground.tag != 23020) {
            UIImage *myBadgedImage = [UIImage imageNamed:@"icon_slide_start"];
            [self.slideToUnlock setThumbImage: myBadgedImage forState:UIControlStateNormal];
            self.slideToUnlock.value = 1.0;
            _imgSlideBackground.image = [UIImage imageNamed:@"bg_slide_start"];
            
            self.vSlider.alpha = 0.0;
            [self performSelector:@selector(delayShowSlider) withObject:nil afterDelay:5];
            _imgSlideBackground.tag = 23020;
        }
    }
    else if (self->appDelegate.pstate == 240 && self->appDelegate.pstate2 == 10) {
        if (_imgSlideBackground.tag != 24010) {
            UIImage *myBadgedImage = [UIImage imageNamed:@"icon_slide_endtrip"];
            [self.slideToUnlock setThumbImage: myBadgedImage forState:UIControlStateNormal];
            self.slideToUnlock.value = 0.0;
            _imgSlideBackground.image = [UIImage imageNamed:@"bg_slide_endtrip"];
            
            self.vSlider.alpha = 0.0;
            [self performSelector:@selector(delayShowSlider) withObject:nil afterDelay:10];
            _imgSlideBackground.tag = 24010;
        }
    }
    else {
        _imgSlideBackground.tag = 0;
    }
    
    if (self->appDelegate.pstate == 240 && self->appDelegate.pstate2 == 10) {
        //[self sliderViewUp:NO];
        [self sliderViewUp:YES];
    }
    else {
        [self sliderViewUp:YES];
    }
    
    [self releaseLockIt];
}

-(NSInteger) initvSlider:(NSInteger)_y {
    UIView *view__ = _vSlider;
    
    // view가 add되어 있으면
    if (view__.tag == 1) {
        view__.hidden = NO;
        
        CGRect frame = view__.frame;
        frame.origin.y = _y;
        view__.frame = frame;
        
        if (self->appDelegate.pstate == 240 && self->appDelegate.pstate2 == 10) {
            [self sliderViewUp:YES];
            //[self sliderViewUp:NO];
        }
        else {
            [self sliderViewUp:YES];
        }
        
        [self releaseLockIt];
        
        return (view__.frame.origin.y + view__.frame.size.height);
    }
    else if (view__.tag == 2) { // hide
        view__.hidden = YES;
        
        CGRect frame = view__.frame;
        frame.origin.y = _y;
        view__.frame = frame;
        return (view__.frame.origin.y + view__.frame.size.height);
    }
    else if (view__.tag == 3) { // show
        view__.hidden = NO;
        view__.tag = 1;
        
        CGRect frame = view__.frame;
        frame.origin.y = _y;
        view__.frame = frame;
        [self updateSlider];
        return (view__.frame.origin.y + view__.frame.size.height);
    }
    
    //------------------------------------------------------------------------------------
    CGRect rect = view__.frame;
    rect.origin.x = self.mapView_.frame.size.width/2 - rect.size.width/2;
    rect.origin.y = _y;
    //rect.size.width = self.view.bounds.size.width;
    view__.frame = rect;
    
    [self.mapView_ addSubview:view__];
    view__.tag = [self getStateTag];
    view__.hidden = NO;
    //------------------------------------------------------------------------------------
    //[self.slideToUnlock addTarget:self action:@selector(fadeLabel) forControlEvents:UIControlEventValueChanged];
    [self.slideToUnlock addTarget:self action:@selector(UnLockIt) forControlEvents:UIControlEventTouchUpInside];
    [self.slideToUnlock addTarget:self action:@selector(UnLockIt) forControlEvents:UIControlEventTouchCancel];
    [self.slideToUnlock addTarget:self action:@selector(UnLockIt) forControlEvents:UIControlEventTouchUpOutside];
    self.slideToUnlock.backgroundColor = [UIColor clearColor];
    [self.slideToUnlock setMinimumTrackImage:[UIImage new] forState:UIControlStateNormal];
    [self.slideToUnlock setMaximumTrackImage:[UIImage new] forState:UIControlStateNormal];
    
    //self.vSlideContainer.layer.cornerRadius = self.slideContainer.frame.size.width/2;
    self.vSlideContainer.layer.cornerRadius = 20;
    self.lbSlider.adjustsFontSizeToFitWidth = YES;
    
    sliderUpRect = view__.frame.origin.y;
    //sliderDownRect = view__.frame.origin.y + view__.frame.size.height;
    
    //TXButton* btnCpos = [self.mapView_ viewWithTag:801];
    sliderDownRect = btnCpos.frame.origin.y - view__.frame.size.height - 10;
    sliderDownRect = sliderUpRect + view__.frame.size.height/2;
    
    [self updateSlider];
    //------------------------------------------------------------------------------------
    return (view__.frame.origin.y + view__.frame.size.height);
}

-(void) updateSurge {
    NSString* mode = [[[TXApp instance] getSettings] getDriveMode];
    if (![[Utils nullToString:mode] isEqualToString:DRIVE_MODE_DRIVING]) {
        _vSurge.hidden = YES;
    }
    else {
        _vSurge.hidden = NO;
    }
}

-(NSInteger) initvSurge:(NSInteger)_y {
    UIView *view__ = _vSurge;
    
    // view가 add되어 있으면
    if (view__.tag == 1) {
        view__.hidden = NO;
        
        CGRect frame = view__.frame;
        frame.origin.y = _y;
        view__.frame = frame;
        return (view__.frame.origin.y + view__.frame.size.height);
    }
    else if (view__.tag == 2) { // hide
        view__.hidden = YES;
        
        CGRect frame = view__.frame;
        frame.origin.y = _y;
        view__.frame = frame;
        return (view__.frame.origin.y + view__.frame.size.height);
    }
    else if (view__.tag == 3) { // show
        view__.hidden = NO;
        view__.tag = 1;
        
        CGRect frame = view__.frame;
        frame.origin.y = _y;
        view__.frame = frame;
        [self updateSurge];
        return (view__.frame.origin.y + view__.frame.size.height);
    }
    
    //------------------------------------------------------------------------------------
    CGRect rect = view__.frame;
    rect.size.width = self.mapView_.frame.size.width - 20*2;
    rect.origin.x = self.mapView_.frame.size.width/2 - rect.size.width/2;
    rect.origin.y = _y;
    view__.frame = rect;
    view__.autoresizingMask = UIViewAutoresizingFlexibleRightMargin;
    
    [self.mapView_ addSubview:view__];
    view__.tag = [self getStateTag];
    view__.hidden = NO;
    //------------------------------------------------------------------------------------
    [self updateSurge];
    //------------------------------------------------------------------------------------
    return (view__.frame.origin.y + view__.frame.size.height);
}

-(void) updateTestMode {
    //NSInteger __y = kBasicMargin;
    
    NSString* mode = [[[TXApp instance] getSettings] getDriveMode];
    if ([[Utils nullToString:mode] isEqualToString:DRIVE_MODE_TRAINING]) {
        if (!_vTestMode.hidden) {
            return;
        }
        isAniTestMode = YES;
        _vTestMode.hidden = NO;
        [_vTestMode setAlpha:1.0f];
        [self animationFade:_vTestMode];
    }
    else {
        isAniTestMode = NO;
        [self hideView:_vTestMode];
        [_vTestMode setAlpha:1.0f];
        return;
    }
    
//    [UIView animateWithDuration:0.2
//                     animations:^{
//                         // hide
//                         CGRect rect = _vTestMode.frame;
//                         rect.origin.y = __y;
//                         _vTestMode.frame = rect;
//                     }];
}

-(NSInteger) initvTestMode:(NSInteger)_y {
    UIView *view__ = _vTestMode;
    
    // view가 add되어 있으면
    if (view__.tag == 1) {
        //view__.hidden = NO;
        
        CGRect frame = view__.frame;
        frame.origin.y = _y;
        view__.frame = frame;
        [self updateTestMode];
        return (view__.frame.origin.y + view__.frame.size.height);
    }
    else if (view__.tag == 2) { // hide
        view__.hidden = YES;
        
        CGRect frame = view__.frame;
        frame.origin.y = _y;
        view__.frame = frame;
        [self updateTestMode];
        return (view__.frame.origin.y + view__.frame.size.height);
    }
    else if (view__.tag == 3) { // show
        //view__.hidden = NO;
        view__.tag = 1;
        
        CGRect frame = view__.frame;
        frame.origin.y = _y;
        view__.frame = frame;
        [self updateTestMode];
        return (view__.frame.origin.y + view__.frame.size.height);
    }
    
    //------------------------------------------------------------------------------------
    CGRect rect = view__.frame;
    rect.origin.x = 10;
    rect.origin.y = _y;
    //rect.size.width = self.view.bounds.size.width;
    view__.frame = rect;
    
    [self.mapView_ addSubview:view__];
    view__.tag = [self getStateTag];
    //view__.hidden = NO;
    //------------------------------------------------------------------------------------
    _vTestMode.hidden = YES;
    [self updateTestMode];
    //------------------------------------------------------------------------------------
    return (view__.frame.origin.y + view__.frame.size.height);
}

-(void) updateCallChat {
    NSDictionary *drive_pickup      = [self->appDelegate.dicDriver objectForKey:@"drive_pickup"];
    NSDictionary *drive_trip        = [self->appDelegate.dicDriver objectForKey:@"drive_trip"];
    
    NSString *uname = @"";
    if (self->appDelegate.pstate == 230 && self->appDelegate.pstate2 == 10) {
        uname       = [Utils nameOfuname:drive_pickup[@"uname"]];
    }
    else if (self->appDelegate.pstate == 230 && self->appDelegate.pstate2 == 20) {
        uname       = [Utils nameOfuname:drive_pickup[@"uname"]];
    }
    else if (self->appDelegate.pstate == 240 && self->appDelegate.pstate2 == 10) {
        uname       = [Utils nameOfuname:drive_trip[@"uname"]];
    }
    [_btnRiderName setTitle:uname forState:UIControlStateNormal];
}

-(NSInteger) initvCallChat:(NSInteger)_y {
    UIView *view__ = _vCallChat;
    
    // view가 add되어 있으면
    if (view__.tag == 1) {
        view__.hidden = NO;
        _btnRider.hidden = NO;
        return (view__.frame.origin.y + view__.frame.size.height);
    }
    else if (view__.tag == 2) { // hide
        view__.hidden = YES;
        _btnRider.hidden = YES;
        return (view__.frame.origin.y + view__.frame.size.height);
    }
    else if (view__.tag == 3) { // show
        view__.hidden = NO;
        view__.tag = 1;
        
        _btnRider.hidden = NO;
        CGRect rect = view__.frame;
        rect.origin.y = _y;
        rect.size.width = self.view.bounds.size.width;
        view__.frame = rect;
        //[self.view bringSubviewToFront:view__];
        [self updateCallChat];
        return (view__.frame.origin.y + view__.frame.size.height);
    }
    
    //------------------------------------------------------------------------------------
    CGRect rect = view__.frame;
    rect.origin.y = _y;
    rect.size.width = self.view.bounds.size.width;
    view__.frame = rect;
    
    [self.mapView_ addSubview:view__];
    view__.tag = [self getStateTag];
    view__.hidden = NO;
    //------------------------------------------------------------------------------------
    NSString *text = [NSString stringWithFormat:@" %@", LocalizedStr(@"Button.CALL")];
    _btnCall.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:14];
    [_btnCall setTitle:text forState:UIControlStateNormal];
    [_btnCall setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    text = @"";
    _btnRiderName.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:14];
    [_btnRiderName setTitle:text forState:UIControlStateNormal];
    [_btnRiderName setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_btnRiderName setTitleEdgeInsets:UIEdgeInsetsMake(10, 0, 0, 0)];//set ur title insects also

    [Utils setCircleImage:_btnRider];
    [[_btnRider imageView] setContentMode: UIViewContentModeScaleAspectFill];
    
    text = [NSString stringWithFormat:@" %@", LocalizedStr(@"Button.CANCEL")];
    _btnCancel.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:14];
    [_btnCancel setTitle:text forState:UIControlStateNormal];
    [_btnCancel setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    if (self->appDelegate.pstate == 240) {
        [_btnCancel setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    }
    
    [self updateCallChat];
    
    rect = _btnRider.frame;
    rect.origin.x = self.view.bounds.size.width/2 - rect.size.width/2;
    rect.origin.y = self.view.bounds.size.height - view__.frame.size.height - rect.size.height/2;
    _btnRider.frame = rect;
    [self.mapView_ addSubview:_btnRider];
    //------------------------------------------------------------------------------------
    return (view__.frame.origin.y + view__.frame.size.height);
}

-(IBAction)chkButtonPressed:(id)sender {
    UIButton *button = (UIButton*)sender;
    
    for (UIView *aView in _vVehicleSelectView.subviews) {
        UIButton *btn = [aView viewWithTag:11];
        if (![button isEqual:btn] && btn.tag == 11) {
            [self changeButton:btn isOn:NO];
        }
    }
    
    if (button.tag == 11) {
        // 선택된건 비선으로 만들지 못한다.
        //[self changeButton:button isOn:NO];
    }
    else {
        [self changeButton:button isOn:YES];
    }
}

-(void)changeButton:(UIButton*)button isOn:(BOOL)flag {
    
    // on
    if (flag) {
        [button setImage:[UIImage imageNamed:@"form_chkbox_chk"] forState:UIControlStateNormal];
        button.tag = 11;
    }
    else {
        [button setImage:[UIImage imageNamed:@"form_chkbox_normal"] forState:UIControlStateNormal];
        button.tag = 10;
    }
}

-(void)updateUIServiceType {
    BOOL flag = NO;
    
    
    for (UIView *aView in _vVehicleSelectView.subviews) {
        UIButton *btn = [aView viewWithTag:11];
        if (btn) {
            flag = YES;
        }
    }
    
    if (!flag) {
        [self initVehicleSelect];
    }
}

- (void)initVehicleSelect {
    
    NSArray *vehicles = [self->appDelegate.dicDriver objectForKey:@"provider_vehicles"];
    if (![Utils isArray:vehicles]) {
        vehicles = vehicleList;
    }
    
    CGRect rect = _vVehicleSelectView.frame;
    [_vVehicleSelectView removeFromSuperview];
    
    _vVehicleSelectView = [[UIScrollView alloc] initWithFrame:rect];
    [_vVehicle addSubview:_vVehicleSelectView];
    //[_vVehicle sendSubviewToBack:_vVehicleSelectView];
    
//    UIView *blockView = [self.mapView_ viewWithTag:90];
//    if (blockView) {
//        [_vVehicle bringSubviewToFront:blockView];
//    }
    
    int cnt = -1;
    CGRect frame = _vVehicleSelect.frame;
    for (NSDictionary *vehicle in vehicles) {
        if ([Utils isDictionary:vehicle]) {
            cnt++;
            if ([vehicle[@"state"] intValue] != 1) {
                continue;
            }
            
            UIView *aView = [Utils clone:_vVehicleSelect];
            //[self updateVehicle:aView];
            if (cnt == 0) {
                frame.origin.x = 0;
            }
            else {
                frame.origin.x = frame.origin.x + frame.size.width + 0;
            }
            frame.origin.y = 0;
            aView.frame = frame;
            aView.tag = cnt;
            
            UIButton *button = [aView viewWithTag:100];
            if (cnt == 0) {
                [self changeButton:button isOn:YES];
            }
            else {
                [self changeButton:button isOn:NO];
            }
            [button addTarget:self action:@selector(chkButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
            
            button = [aView viewWithTag:200];
            button.hidden = YES;
            
            UILabel *lbVehicleNo = [aView viewWithTag:101];
            lbVehicleNo.text = [NSString stringWithFormat:@"%@ / %@",[Utils nullToString:vehicle[@"service_name"]],[Utils nullToString:vehicle[@"vehicleno"]]];
            lbVehicleNo.textColor = HEXCOLOR(0x333333FF);
            
            UILabel *lbVehicleModel = [aView viewWithTag:102];
            lbVehicleModel.text = [Utils nullToString:vehicle[@"vehiclemodel"]];
            lbVehicleModel.textColor = HEXCOLOR(0x333333FF);
            
            UIImageView *img = [aView viewWithTag:103];
            // 3. call api
            //dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                //Background Thread
                NSDictionary *pMap = @{
                                       @"vseq" : vehicle[@"vseq"],
                                       @"name" : @"vehicle",
                                       @"size" : vehicle[@"vehicle"]
                                       };
                [Utils downloadImageWithURL:@"PP603" property:pMap completionBlock:^(BOOL succeeded, UIImage *image) {
                    if (succeeded && image) {
                        
                        dispatch_async(dispatch_get_main_queue(), ^(void){
                            //Run UI Updates
                            // change the image in the cell
                            img.image = image;
                            [Utils setCircleImage:img];
                        });
                    }
                }];
                
            //});
            
            [_vVehicleSelectView addSubview:aView];
            
        }
    }
    
    // 스크롤 뷰의 컨텐츠 사이즈를 미리 만들어둡니다.
    CGSize contentSize = _vVehicleSelectView.frame.size;
    contentSize.width = frame.origin.x + frame.size.width + 0;
    //contentSize.width = _vOrderSelectView.frame.size.width * cnt;
    //[_vOrderSelectView setContentOffset:CGPointMake(_vOrderSelect.frame.size.width, 0.0f)];
    
    // 스크롤 뷰의 컨텐츠 사이즈를 설정합니다.
    [_vVehicleSelectView setContentSize:contentSize];
}

- (void)updteVehicleSelect {
    
    for (UIView *aView in _vVehicleSelectView.subviews) {
        UIButton* btn = [aView viewWithTag:11];
        if (btn) {
            [self changeButton:btn isOn:NO];
        }
    }
}

-(NSInteger) initvVehicle:(NSInteger)_y {
    UIView *view__ = _vVehicle;
    
    // view가 add되어 있으면
    if (view__.tag == 1) {
        view__.hidden = NO;
        
        [self.view bringSubviewToFront:view__];
        return (view__.frame.origin.y + view__.frame.size.height);
    }
    else if (view__.tag == 2) { // hide
        view__.hidden = YES;
        
        
        return (view__.frame.origin.y + view__.frame.size.height);
    }
    else if (view__.tag == 3) { // show
        view__.hidden = NO;
        view__.tag = 1;
        
        if (!_vVehicleSelectView.subviews.count) {
            [self initVehicleSelect];
        }
        [self.view bringSubviewToFront:view__];
        
        [self updateUIServiceType];
        //        [self updteVehicleSelect];
        return (view__.frame.origin.y + view__.frame.size.height);
    }
    
    //------------------------------------------------------------------------------------
    CGRect rect = view__.frame;
    rect.origin.y = _y;
    rect.size.width = self.view.bounds.size.width;
    view__.frame = rect;
    
    [self.view addSubview:view__];
    view__.tag = [self getStateTag];
    view__.hidden = NO;
    //------------------------------------------------------------------------------------
    
    [self initVehicleSelect];
    
    // 스크롤 뷰의 Delegate를 설정합니다. ScrollView Delegate 함수를 사용하기 위함입니다.
    [_vVehicleSelectView setDelegate:self];
    // 스크롤 뷰의 페이징 기능을 ON합니다.
    [_vVehicleSelectView setPagingEnabled:NO];
    // 스크롤 뷰의 Bounce를 Disabled합니다 첫 페이지와 마지막 페이지에서 애니메이션이 발생하지않습니다.
    [_vVehicleSelectView setBounces:YES];
    [_vVehicleSelectView setScrollsToTop:NO];
    [_vVehicleSelectView setScrollEnabled:YES];
    [_vVehicleSelectView setClipsToBounds:NO];
    
    // 스크롤 바들을 보이지 않습니다.
    _vVehicleSelectView.showsHorizontalScrollIndicator = NO;
    _vVehicleSelectView.showsVerticalScrollIndicator = NO;
    
    // 스크롤뷰를 터치할 경우 컨텐츠 내부의 뷰에 이벤트 전달
    //    [_vVehicleSelectView touchesShouldBegin:touches withEvent:evt inContentView:self.mapView_];
    //    [_vVehicleSelectView touchesShouldCancelInContentView:self.mapView_];
    
    _lbVehicleSelect.text = LocalizedStr(@"Driver.Vechile.Select.alert.text");
    _lbVehicleSelect.textColor = HEXCOLOR(0x4a4a4aFF);

    [_btnVehicle setTitle:LocalizedStr(@"Button.OK") forState:UIControlStateNormal];
    
#ifdef _WITZM
#elif defined _CHACHA
    [_btnVehicle setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
#endif

    //------------------------------------------------------------------------------------
    return (view__.frame.origin.y + view__.frame.size.height);
}

#else
#pragma mark - hideUI - Rider

- (void)onHideUIu110_2 {
    [self hideView:_vTopRiderNavi];
    
#ifdef _WITZM
#elif defined _CHACHA
    [self hideView:_vPrefer];
#endif

    [self hideView:_vOrder];
    
    NSLog(@"-------------- HIDe 111");
}

- (void)onHideUIu110_1 {
    // hide
    [self hideView:_vTopMainNavi];
    [self hideView:_vOrder];
    
#ifdef _WITZM
#elif defined _CHACHA
    [self hideView:_vPrefer];
#endif

    _vFavorite1.hidden = YES;
    _vFavorite2.hidden = YES;
    _vFavorite3.hidden = YES;
    _vFavoriteHome.hidden = YES;
    _vFavoriteWork.hidden = YES;
    _vFavoriteSetting.hidden = YES;
    _vFavoriteLast.hidden = YES;
    
    [self hideView:_lbFavoriteSetting];
    //[self hideView:_lbFavoriteLast];
    
    NSLog(@"-------------- HIDE 110");
}

- (void)onHideUIu110 {
    if ( [_srcLocationTextField.text isEqualToString:@""] || [_dstLocationTextField.text isEqualToString:@""]) {
        [self onHideUIu110_2];
    }
    else { // request 가능 상태
        [self onHideUIu110_1];
    }
    //TXButton* btnCpos = [self.mapView_ viewWithTag:801];
    btnCpos.hidden = NO;
    
    [self hideView:_vPrefer];
    [self hideView:_vDriveStatus];
    [self hideView:_vCallChat];

}

- (void)onHideUIu120 {
    if (vcMapCall != nil) {
        if (!self->appDelegate.isDriverMode && !(self->appDelegate.ustate == 120 && self->appDelegate.ustate2 == 10)) {
            [vcMapCall dismissViewControllerAnimated:YES completion:^(){
                vcMapCall = nil;
            }];
        }
    }
    
    //NSLog(@"-------------- HIDe 120");
}

- (void)onHideUIu130 {
    // hide
    [self hideView:_vTopRiderNavi];
    [self hideView:_vPrefer];
    [self hideView:_vDriveStatus];
    [self hideView:_vCallChat];
    
    NSLog(@"-------------- HIDE 130");
}

- (void)onHideUIu140 {
    // hide
    //[self hideView:_vCallChat];
    
    // 취소버튼
    _btnCancel.enabled = NO;
    //NSLog(@"-------------- HIDE 140");
}

- (void)onHideUI:(NSInteger)state state2:(NSInteger)state2 {
    //UIView *n = [super navigationView];
    //NSInteger _y = n.frame.origin.y + n.frame.size.height;
    
    if (state <= 100) {
        [self onHideUIu140];
        [self onHideUIu130];
        [self onHideUIu120];
        if (self->appDelegate.ustate != 120) {
            [self onHideUIu110];
        }
        
        //NSLog(@"-------------- HIDEUI 100");
    }
    else if (state == 110) { // wait
        [self onHideUIu140];
        [self onHideUIu130];
        [self onHideUIu120];
        if (self->appDelegate.ustate != 120) {
            [self onHideUIu110];
        }
        
        //NSLog(@"-------------- HIDEUI 110");
    }
    else if (state == 120 && state2 == 10) { // request
        //[self onHideUIu120];
        if (self->appDelegate.ustate != state) {
            [self onHideUIu120];
        }
        [self onHideUIu110_1];
        [self onHideUIu110_2];
        
        //NSLog(@"-------------- HIDEUI 12010");
    }
    else if (state == 120 && state2 == 20) { // catch
        if (self->appDelegate.ustate != state) {
            [self onHideUIu120];
        }
        [self onHideUIu110_1];
        [self onHideUIu110_2];
        
        //NSLog(@"-------------- HIDEUI 12020");
    }
    else if (state == 130 && state2 == 10) { // on pickup
        [self onHideUIu130];
        [self onHideUIu120];
        [self onHideUIu110_1];
        [self onHideUIu110_2];
        
        //NSLog(@"-------------- HIDEUI 13010");
    }
    else if (state == 130 && state2 == 20) { // arrived
        
        [self hideView:_vCallChat];
        [self onHideUIu120];
        [self onHideUIu110_1];
        [self onHideUIu110_2];
        
        //NSLog(@"-------------- HIDEUI 13020");
    }
    else if (state == 140 && state2 == 10) { // on trip
        
        if (self->appDelegate.ustate != state) {
            [self onHideUIu140];
        }
        [self onHideUIu130];
        [self onHideUIu120];
        [self onHideUIu110_1];
        [self onHideUIu110_2];
        
        //NSLog(@"-------------- HIDEUI 14010");
    }
    else if ((state == 140 && state2 == 20) || (state == 130 && state2 <= -10) || (state == 140 && state2 <= -10)) { // pay
        [self onHideUIu140];
        [self onHideUIu130];
        [self onHideUIu120];
        [self onHideUIu110_1];
        [self onHideUIu110_2];
        
        //NSLog(@"-------------- HIDEUI 14020");
    }
    else if (state == 0 && state2 == 0) { // 초기화
        [self onHideUIu140];
        [self onHideUIu130];
        [self onHideUIu120];
        [self onHideUIu110_1];
        [self onHideUIu110_2];
        
        //NSLog(@"-------------- HIDEUI 24020");
    }
}

#pragma mark - showUI - Rider
- (void)onShowUIu110 {
    if ( [_srcLocationTextField.text isEqualToString:@""] || [_dstLocationTextField.text isEqualToString:@""]) {
        // show
        if (_vTopMainNavi.tag==2) _vTopMainNavi.tag = 3;
        
#ifdef _WITZM
        if (_vPrefer.tag==2) _vPrefer.tag = 3;
#elif defined _CHACHA
#endif
        
        if (_lbFavoriteSetting.tag==2) _lbFavoriteSetting.tag = 3;
        //if (_lbFavoriteLast.tag==2) _lbFavoriteLast.tag = 3;
        
        //        [self setServiceType:DEFAULT_SERVICE];
        
        // code 초기화
        promoCode = @"";
        [[[TXApp instance] getSettings] setFDKeychain:@"promoCode" value:promoCode];
        _lbCouponInfo.text = LocalizedStr(@"Map.Vechile.Request.CouponCode.title");

        //NSLog(@"-------------- SHOW 110");
    }
    else { // request 가능 상태
        // show
        if (_vTopRiderNavi.tag==2) _vTopRiderNavi.tag = 3;
        if (_vOrder.tag==2) _vOrder.tag = 3;
        
        //NSLog(@"-------------- SHOW 111");
    }
    
    //[self hideView:_vDriveStatus];
    //TXButton* btnCpos = [self.mapView_ viewWithTag:801];
    btnCpos.hidden = NO;
}

- (void)onShowUIu130 {
    if (vcMapCall != nil) {
        if (!(self->appDelegate.ustate == 120 && self->appDelegate.ustate2 == 10)) {
            [vcMapCall dismissViewControllerAnimated:YES completion:^(){
                vcMapCall = nil;
            }];
        }
    }
    
    // show
    if (_vTopRiderNavi.tag==2) _vTopRiderNavi.tag = 3;
    if (_vCallChat.tag==2) _vCallChat.tag = 3;
    if (_vDriveStatus.tag==2) _vDriveStatus.tag = 3;
//    if (!(self->appDelegate.ustate == 130 && self->appDelegate.ustate2 == 10)) {
//        if (_vPickUp.tag==2) _vPickUp.tag = 3;
//    }
    
//    // _vDriveStatus
//    [self updateDriveStatus];
//    
//    // trip map을 지운다.
//    [self cleanTripMapOnly];
//    
//    // vehicle 지운다.
//    [self hideAllVehicle];
    
    //TXButton* btnCpos = [self.mapView_ viewWithTag:801];
    btnCpos.hidden = YES;
    
    // 취소버튼
    _btnCancel.enabled = YES;
    
    //NSLog(@"-------------- SHOW 130");
}

- (void)onShowUIu140 {
    // show
    if (_vTopRiderNavi.tag==2) _vTopRiderNavi.tag = 3;
    if (_vCallChat.tag==2) _vCallChat.tag = 3;
    if (_vDriveStatus.tag==2) _vDriveStatus.tag = 3;
    
    //TXButton* btnCpos = [self.mapView_ viewWithTag:801];
    btnCpos.hidden = YES;
    
    //NSLog(@"-------------- SHOW 140");
}

- (void)onShowUI:(NSInteger)state state2:(NSInteger)state2 {
    
    //TXButton* btnCpos = [self.mapView_ viewWithTag:801];
    
    if (self->appDelegate.ustate < 120) {
        
        // remove useq info
        [[TXCallModel instance] onCleanPseqSession];
        
        PLACE_DELETE_NULL;
        
        // init profile
        [self initDriderProfileImage];
        
        [self initDriveStatus];
        
        // hide drive marker
        [self hideDriveMarker];
        
        [Utils resetStateUIInfo];
        
        [self onUR206];
        
#ifdef _DRIVER_MODE
        [self updatePrefer:self->appDelegate.dicDriver[@"provider_matchs"]];
#else
        [self updatePrefer:self->appDelegate.dicRider[@"user_matchs"]];
#endif
        
        
    }
    
    if (state == 100) {
        [self hideAllVehicle];
    }
    
    if (state <= 110) { // wait
        [self onShowUIu110];
        // 출/도착 주소가 정해지지 않은 상태
        if ( [_srcLocationTextField.text isEqualToString:@""] || [_dstLocationTextField.text isEqualToString:@""]) {
            
            isMapMoved = NO;
            // eta 거리/시간의 출력 기본값 변경
            _btnEta.tag = 1;
            // 초기화
            [self print_eta:nil toggle:9 api:NO];
            
            //if (_srcLocationTextField.tag != 9 || _dstLocationTextField.tag != 9) {
            
            [self changeStatusBarClear:YES];
            
            //_y = [Utils frameUnderY:_vTopMainNavi dim:0];
            [self initvMap:0];
            
            [self initvTopMainNavi];
            
#ifdef _WITZM
            NSInteger __height = self.view.frame.size.height - _vPrefer.frame.size.height;
            [self initvPrefer:__height];
            [self initvAddress:[Utils frameOverY:_vAddress overView:_vPrefer dim:20]];
#elif defined _CHACHA
            //NSInteger __height = (self.view.frame.size.height - _vPrefer.frame.size.height - 20 - _vAddress.frame.size.height);
            NSInteger __height = (self.view.frame.size.height - kBottomSafeArea - _vAddress.frame.size.height);
            [self initvAddress:__height];
#endif
            
            CGRect rect = btnCpos.frame;
            rect.origin.y = kTopHeight + _vTopMainNavi.frame.size.height + 17;
            btnCpos.frame = rect;
            btnCpos.hidden = NO;
            
            [self initvFavorite:0];
            
            // map padding
            [self updateMapPadding:110];
            
            // map 초기화
            [self cleanTripMapOnly];
            //[self cleanMapOnly]; // 현재위치로 이동됨
            // 출발지로 이동됨
            if ( ![_srcLocationTextField.text isEqualToString:@""] ) {
                CLLocationCoordinate2D coordinate = [Utils srcPositionCoor];
                //CLLocationCoordinate2D coordinate = [Utils currentPositionCoor];
                //NSLog(@"coordinate:%f,%f",coordinate.latitude,coordinate.longitude);
                self.mapView_.camera = [GMSCameraPosition cameraWithTarget:coordinate
                                                                      zoom:[self getZoomByState]];
                
                if (![GoogleUtils isEqualLocation:coordinate location:[Utils currentPositionCoor]]) {
                    [self updateCposImage:NO];
                }
                [self updateCposImage:NO];
            }
            // end
            
            if (!self.mapView_.myLocationEnabled) {
                self.mapView_.myLocationEnabled = YES;
            }
            
            //[tripPath removeAllObjects];
            // marker제거
            INTULocationManager *locMgr = [INTULocationManager sharedInstance];
            [locMgr clearMarker:locMgr.markerStart];
            [locMgr clearMarker:locMgr.markerFinish];
            
            [self unblockSrcAddress];
            [self unblockDestinationAddress];
        }
        else { // request 가능 상태
            
            [self changeStatusBarClear:NO];
            
            [self initvMap:0];
            
            [self initvTopRiderNavi];
            
            [self initvAddress:[Utils frameUnderY:_vTopRiderNavi dim:10]];
            
            [self rePositionPosButton];
            
            NSInteger __height = self.view.frame.size.height - _vOrder.frame.size.height - kBottomBottonSafeArea;
            [self initvOrder:__height];
            [_vOrderSelectView setContentOffset:CGPointZero animated:YES];
            
            // map padding
            [self updateMapPadding:111];
            
            // 주문예상 조회
            //[self onUQ101]; //-> 지도그리는 쪽에 넣는다.
            
            if (self.mapView_.myLocationEnabled) {
                self.mapView_.myLocationEnabled = NO;
            }
            
            [self unblockSrcAddress];
            [self unblockDestinationAddress];
        }
    }
    else if (state == 120 && state2 == 10) { // request
        [self changeStatusBarClear:NO];
        
        // 초기화
        [self print_eta:nil toggle:9 api:NO];
    }
    //    else if (state == 120 && state2 == 20) { // catch
    //
    //    }
    else if (state == 130 && state2 == 10) { // on pickup
        // eta 거리/시간의 출력 기본값 변경
        _btnEta.tag = 1;
        
        [self changeStatusBarClear:NO];
        
        // 드라이버 매칭시 알림음과 진동처리
        [appDelegate playSound:SOUND_NOTI vibrate:YES];
        
        [self onShowUIu130];
        
        [self initvMap:0];
        
        [self initvTopRiderNavi];
        
        [self initvAddress:[Utils frameUnderY:_vTopRiderNavi dim:10]];
        
        NSInteger __height = self.view.frame.size.height - _vCallChat.frame.size.height - kBottomBottonSafeArea;
        [self initvCallChat:__height];
        
        __height = self.view.frame.size.height - _vCallChat.frame.size.height - _vDriveStatus.frame.size.height - kBottomBottonSafeArea;
        [self initvDriveStatus:__height];
        
        [self rePositionPosButton];
        
        [_vDriveStatus bringSubviewToFront:btnCpos];
        
        // map padding
        [self updateMapPadding:state];
        
        if (self.mapView_.myLocationEnabled) {
            self.mapView_.myLocationEnabled = NO;
        }
        
        // _vDriveStatus
        [self updateDriveStatus];
        
        // trip map을 지운다.
        [self cleanTripMapOnly];
        
        // vehicle 지운다.
        [self hideAllVehicle];
        
        // 드리이브 위치
        [self updateDrivePosition];
        
        [self pushCpos:btnCpos];
        
        [self unblockSrcAddress];
        [self unblockDestinationAddress];
    }
    else if (state == 130 && state2 == 20) { // arrived
        [self onShowUIu130];
        
        [self initvMap:0];
        
        [self initvTopRiderNavi];
        
        [self initvAddress:[Utils frameUnderY:_vTopRiderNavi dim:10]];
        
        NSInteger __height = self.view.frame.size.height - _vCallChat.frame.size.height - kBottomBottonSafeArea;
        [self initvCallChat:__height];
        
        __height = self.view.frame.size.height - _vCallChat.frame.size.height - _vDriveStatus.frame.size.height - kBottomBottonSafeArea;
        [self initvDriveStatus:__height];
        
        [self rePositionPosButton];
        
        // map padding
        [self updateMapPadding:state];
        
        if (self.mapView_.myLocationEnabled) {
            self.mapView_.myLocationEnabled = NO;
        }
        
        [self pushCpos:btnCpos];
        
        // 지도에 위치 표시
        [self updateDriveMarker];
        
        // remove focus
        [self hideFocusPin];
        
        [self unblockSrcAddress];
        [self unblockDestinationAddress];
    }
    else if (state == 140 && state2 == 10) { // on trip
        // eta 거리/시간의 출력 기본값 변경
        _btnEta.tag = 1;
        
        [self onShowUIu140];
        
        [self initvMap:0];
        
        [self initvTopRiderNavi];
        // trip중 cancel불가
        [self.vTopRiderNavi viewWithTag:1109].hidden = YES;
        
        [self initvAddress:[Utils frameUnderY:_vTopRiderNavi dim:10]];
        
        NSInteger __height = self.view.frame.size.height - _vCallChat.frame.size.height - kBottomBottonSafeArea;
        [self initvCallChat:__height];

        __height = self.view.frame.size.height - _vCallChat.frame.size.height - _vDriveStatus.frame.size.height - kBottomBottonSafeArea;
        [self initvDriveStatus:__height];
        
//        NSInteger __height = self.view.frame.size.height - _vDriveStatus.frame.size.height;
//        [self initvDriveStatus:__height];
        
        [self rePositionPosButton];
        
        //[_vDriveStatus bringSubviewToFront:btnCpos];
        
        // map padding
        [self updateMapPadding:state];
        
        if (self.mapView_.myLocationEnabled) {
            self.mapView_.myLocationEnabled = NO;
        }
        
        // 지도에 위치 표시
        [self updateDriveMarker];
        
        // remove focus
        [self hideFocusPin];
        
        [self blockSrcAddress];
        [self unblockDestinationAddress];
    }
    else if ((state == 140 && state2 == 20)  || (state == 130 && state2 <= -10) || (state == 140 && state2 <= -10)) { // pay
        // 110 처리됨 초기화 해야 함.
        
        // location 재시작
//        [self.locationMgr stopUpdatingLocation];
//        [self startUpdatingLocation];
        [appDelegate startLocationUpdateSubscription];
        
        [self initvMap:0];
        
        if (state == 140 && state2 == 20) {
            // trip이 끝나면 favorite에 등록한다.
            CLLocationCoordinate2D position = [Utils destinationPositionCoor];
            NSString *loc = [NSString stringWithFormat:@"%@,%@",LOCATION_STR(position.latitude), LOCATION_STR(position.longitude)];
            PLACE_DELETE_FAVORITE(loc);
            PLACE_INSERT_FAVORITE(loc,_dstLocationTextField.text);
        }
        
        // view가 바뀐다.
        _isViewChanged = YES;
        self->appDelegate.isMyCancel = YES;
        // 주소를 초기화 한다.
        [self initAddressSrcDst];
        [self initAddressSrc];
        //[self initTripView];
        
        [Utils resetStateUIInfo];
        
        promoCode = @"";
        [[[TXApp instance] getSettings] setFDKeychain:@"promoCode" value:promoCode];
        //[[[TXApp instance] getSettings] setFDKeychain:@"serviceType" value:@""];
        //pathDynamic = nil;
        //[tripPath removeAllObjects];
        _lbCouponInfo.text = LocalizedStr(@"Map.Vechile.Request.CouponCode.title");
        _lbDriveStatus.text = @"";
        
        [self removeBadge];
        
        //[self dismissVcCancel:nil];
        if (vcCancel != nil) {
            [self.navigationController popToRootViewControllerAnimated:YES];
            vcCancel = nil;
//            [vcCancel dismissViewControllerAnimated:NO completion:^(){
//                vcCancel = nil;
//                NSLog(@"vcCancel0");
//            }];
        }
        
        if (vcCharge == nil) {
            vcCharge = [[TXChargeVC alloc] initWithNibName:@"TXChargeVC" bundle:nil];
            [self pushViewController:vcCharge];
        }
        
        if (!self.mapView_.myLocationEnabled) {
            self.mapView_.myLocationEnabled = YES;
        }
        
        // service를 default로 변경한다.
        [self getServiceType];
        if ([serviceType isEqualToString:@""]) {
            [self setServiceType:DEFAULT_SERVICE];
        }
        
        [self onShowUI:110 state2:0];
    }
    
    [self.mapView_ bringSubviewToFront:btnCpos];
}

#pragma mark - UpdateData - Rider
- (void)onFitMap {
    // 한번만 fit한다
    static NSInteger __state = 0;
    if (__state == self->appDelegate.ustate) {
        return;
    }
    __state = self->appDelegate.ustate;
    
    // fit
    if (self->appDelegate.ustate == 130 && self->appDelegate.ustate2 == 10) {
        [self updateVehicleFitBounds];
    }
    else if (self->appDelegate.ustate == 140 && self->appDelegate.ustate2 == 10) {
        [self updateVehicleFitBounds];
        //[self pushCpos];
    }
//    if (self->appDelegate.ustate == 130 && self->appDelegate.ustate2 == 10) {
//        if (self->appDelegate.ustateold == 120 && self->appDelegate.ustate2old == 20) {
//            if (locMgr.markerDrive.map != nil) {
//                [self updateVehicleFitBounds];
//                self->appDelegate.ustate2old = 0;
//            }
//        }
//        else if (self->appDelegate.ustateold == 0 && self->appDelegate.ustate2old == 0) {
//            if (locMgr.markerDrive.map != nil) {
//                [self updateVehicleFitBounds];
//                self->appDelegate.ustateold = 120;
//            }
//        }
//    }
//    else if (self->appDelegate.ustate == 140 && self->appDelegate.ustate2 == 10) {
//        if (self->appDelegate.ustateold == 130 && self->appDelegate.ustate2old == 20) {
//            //[self updateVehicleFitBounds];
//            [self pushCpos];
//            self->appDelegate.ustate2old = 0;
//        }
//        else if (self->appDelegate.ustateold == 0 && self->appDelegate.ustate2old == 0) {
//            //[self updateVehicleFitBounds];
//            [self pushCpos];
//            self->appDelegate.ustateold = 130;
//        }
//    }
}

- (void)onUpdate130:(id)updata src:(NSString*)srcGeoStr dst:(NSString*)dstGeoStr {
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    
    if (self->appDelegate.ustate >= 130) {
        // 마커가 없으면
        if ([Utils nullToImage:locMgr.markerStart.icon]) {
            [self showMarker:locMgr.markerStart position:srcGeoStr];
        }
        if ([Utils nullToImage:locMgr.markerFinish.icon]) {
            [self showMarker:locMgr.markerFinish position:dstGeoStr];
        }
    }
    
    if ((self->appDelegate.ustate == 130 && self->appDelegate.ustate2 == 10) || (self->appDelegate.ustate == 140 && self->appDelegate.ustate2 == 10)) {
        
        NSDictionary* driver_step = (NSDictionary*)updata;
        // ETA
        [self print_eta:driver_step toggle:0 api:NO];
        CLLocationCoordinate2D sposition = [Utils getLocation:srcGeoStr];
        CLLocationCoordinate2D eposition = [Utils getLocation:dstGeoStr];
        if ([Utils isLocationValid:sposition] && ![Utils isEqualLocation:sposition location:[Utils srcPositionCoor]]) {
            [Utils updateSrcPosition:LOCATION_STR(sposition.latitude) lng:LOCATION_STR(sposition.longitude)];
            [self pushCpos:btnCpos];
        }
        if ([Utils isLocationValid:eposition] && ![Utils isEqualLocation:eposition location:[Utils destinationPositionCoor]]) {
            [Utils updateDstPosition:LOCATION_STR(eposition.latitude) lng:LOCATION_STR(eposition.longitude)];
            [self pushCpos:btnCpos];
        }
    }
    else {
        if ([updata isKindOfClass:[NSString class]]) {
            NSString* eta  = (NSString*)updata;
            if (![_lbTopTitle.text isEqualToString:eta]) {
                _lbTopTitle.text = eta;
            }
        }
    }

    // update drive status
    [self updateDriveInfo];
}
/*
 <= 110 : 아래쪽 주소창
 = 130.10 : 상태글자
 = 130.20 : 상태글자
 = 140.10 : 상태글자
 */

-(void)onRepareUI {
    if (self->appDelegate.ustate <= 110) {
        // 110
        if ([_dstLocationTextField.text isEqualToString:@""]) {
            if (_vAddress.hidden || _vAddress.frame.origin.y < self.view.frame.size.height/2) {
                // ui reload
                self->appDelegate.ustate2 = 0;
                //[self performSelector:@selector(onUpdateUI) withObject:nil afterDelay:0.1];
                [self onUpdateUI];
            }
        }
    }
    else if (self->appDelegate.ustate == 130 && self->appDelegate.ustate2 == 10) {
        if (_vDriveStatus.hidden || ![_lbDriveStatus.text isEqualToString:LocalizedStr(@"Map.Drive.OnPickup.title")]) {
            // ui reload
            self->appDelegate.ustate2 = 0;
            [self onUpdateUI];
        }
        if (_vOrder.tag != 2 && _vOrder.tag>0) {
            // ui reload
            self->appDelegate.ustate2 = 0;
            [self onUpdateUI];
        }
    }
    else if (self->appDelegate.ustate == 130 && self->appDelegate.ustate2 == 20) {
        if (_vDriveStatus.hidden || ![_lbDriveStatus.text isEqualToString:LocalizedStr(@"Map.Drive.PickupArrived.title")]) {
            // ui reload
            self->appDelegate.ustate2 = 0;
            [self onUpdateUI];
        }
        if (_vOrder.tag != 2 && _vOrder.tag>0) {
            // ui reload
            self->appDelegate.ustate2 = 0;
            [self onUpdateUI];
        }
    }
    else if (self->appDelegate.ustate == 140 && self->appDelegate.ustate2 == 10) {
        if (_vDriveStatus.hidden || ![_lbDriveStatus.text isEqualToString:LocalizedStr(@"Map.Drive.OnTrip.title")]) {
            // ui reload
            self->appDelegate.ustate2 = 0;
            [self onUpdateUI];
        }
        if (_vOrder.tag != 2 && _vOrder.tag>0) {
            // ui reload
            self->appDelegate.ustate2 = 0;
            [self onUpdateUI];
        }
    }
}

- (void)onUpdateData {
    
#ifndef _DRIVER_MODE
    if (!appDelegate.isDriverMode && appDelegate.ustate < 120 && _vDriveStatus.tag == 1) {
        [self hideView:_vDriveStatus];
    }
#endif
    
    BOOL isChangeFocus = NO;
    NSString* drvGeoStr = @"";
    NSString* srcGeoStr = @"";
    NSString* dstGeoStr = @"";
    NSString* service   = @"";
    NSString* uname     = @"";
    
    NSDictionary *drive_pickup      = [self->appDelegate.dicRider objectForKey:@"drive_pickup"];
    NSDictionary *provider_device   = [self->appDelegate.dicRider objectForKey:@"provider_device"];
    NSDictionary *drive_pickup_step = [self->appDelegate.dicRider objectForKey:@"drive_pickup_step"];
    
    NSDictionary *drive_trip        = [self->appDelegate.dicRider objectForKey:@"drive_trip"];
    NSDictionary *drive_trip_step   = [self->appDelegate.dicRider objectForKey:@"drive_trip_step"];
    
//    if (self->appDelegate.ustate == 100) {
//        // 주소변경 확인
//        // 픽업지 이전 위치와 다르면
//        if ([_srcLocationTextField.text isEqualToString:@""]) {
//            //
//            NSString *srcGeoStr = [Utils currentPosition];
//            CLLocationCoordinate2D srcGeo = [Utils currentPositionCoor];
//            if ([GoogleUtils isLocationValid:srcGeo]) {
//                // 픽업지 주소 엡데이트
//                [self updateStartPosition:srcGeoStr];
//                
//                // 마커가 없으면
//                if ([Utils nullToImage:locMgr.markerStart.icon]) {
//                    [self showMarker:locMgr.markerStart position:srcGeoStr icon:[locMgr getMarkerIcon:@"route_start"]];
//                }
//                else {
//                    [self showMarker:locMgr.markerStart position:srcGeoStr];
//                }
//            }
//        }
//        // tripline 삭제
//        [self cleanTripMapOnly];
//        
//        return;
//    }
    if (self->appDelegate.ustate <= 110) { // wait
        // 주소변경 확인
        // 픽업지 이전 위치와 다르면
        if ([_srcLocationTextField.text isEqualToString:@""]) {
            //
            NSString *srcGeoStr = [Utils currentPosition];
            CLLocationCoordinate2D srcGeo = [Utils currentPositionCoor];
            if ([GoogleUtils isLocationValid:srcGeo]) {
                // 픽업지 주소 엡데이트
                [self updateStartPosition:srcGeoStr];
                INTULocationManager *locMgr = [INTULocationManager sharedInstance];
                
                // 마커가 없으면
                if ([Utils nullToImage:locMgr.markerStart.icon]) {
                    [self showMarker:locMgr.markerStart position:srcGeoStr icon:[locMgr getMarkerIcon:@"route_start"]];
                }
                else {
                    [self showMarker:locMgr.markerStart position:srcGeoStr];
                }
            }
        }
        
        if ([_dstLocationTextField.text isEqualToString:@""]) {
            // tripline 삭제
            [self cleanTripMapOnly];
            
            // favorite refresh오류
            [self checkErrorFavoriteText];
            // map padding
            //[self updateMapPadding:110];
        }
        else {
            // map padding
            //[self updateMapPadding:111];
        }
        
        INTULocationManager *locMgr = [INTULocationManager sharedInstance];
        if (![_srcLocationTextField.text isEqualToString:@""] &&
            ![_dstLocationTextField.text isEqualToString:@""] &&
            locMgr.tripline.map == nil) {
//            // 110 상태에서 출/도착지가 있으면 UI가 바뀐다.
//            [self drawEstimateLineRider];
            // route 그리기 : 차량요청
            [self drawRoute];
        }
        return;
    }
    else if (self->appDelegate.ustate == 120 && self->appDelegate.ustate2 == 10) { // request
        return;
    }
    //    else if (self->appDelegate.ustate == 120 && self->appDelegate.ustate2 == 20) { // catch
    //
    //    }
    else if (self->appDelegate.ustate == 130 && self->appDelegate.ustate2 == 10) { // on pickup
        
        srcGeoStr   = [Utils nullTolocation:[drive_pickup objectForKey:@"lsrc"]];
        dstGeoStr   = [Utils nullTolocation:[drive_pickup objectForKey:@"ldst"]];
        
        service     = [Utils nullToString:drive_pickup[@"service"]];
        
        // ETA : drive_pickup_step.eduration, edistance : ETA 시간(분), 거리km
        //NSString* eta         = [NSString stringWithFormat:@"%d",[[drive_pickup_step objectForKey:@"eduration"] intValue]];
        
        // fit
        [self onFitMap];
        //[self updateMapPadding:self->appDelegate.ustate];
        
        // 주소변경 확인
        // 픽업지 이전 위치와 다르면
        isChangeFocus = [self onUpdateSrcAddr:srcGeoStr tts:YES];
        [self onUpdateDestAddr:dstGeoStr];

        // ETA, 주소, DriverInfo
        [self onUpdate130:drive_pickup_step src:srcGeoStr dst:dstGeoStr];
        
        // show drive marker
        // 드리이브 위치
        [self updateDrivePosition];
        
        if (self->appDelegate.imgProfileDriver == nil) {
            [self onUR602]; // driver profile
        }
        if (self->appDelegate.imgVehicleDriver == nil) {
            [self onUR603]; // driver vehicle
        }
        // 차량 위치 업데이트 -> api응답에서 직접한다.
    }
    else if (self->appDelegate.ustate == 130 && self->appDelegate.ustate2 == 20) { // arrived
        // 출도착 변경처리 필요
        uname       = [Utils nameOfuname:provider_device[@"name"]];
        
        drvGeoStr   = [Utils nullTolocation:[provider_device objectForKey:@"location"]];
        srcGeoStr   = [Utils nullTolocation:[drive_pickup objectForKey:@"lsrc"]];
        dstGeoStr   = [Utils nullTolocation:[drive_pickup objectForKey:@"ldst"]];
        
        service     = [Utils nullToString:drive_pickup[@"service"]];
        
        // ET는 update_date부터 0~5분 증가, 빨간색으로표시하고  5분이 되면 timeout처리 한다., 기사 marker는 사라진다. 경로 지운다. - 1분단위로 ET update
        NSString* eta         = [drive_pickup objectForKey:@"update_date"];
        //[self cleanEtaType];
        [self startETClock:_PICKUP_TIME_OUT withDate:eta];
        
        // fit
        //[self onFitMap];
        //[self updateMapPadding:self->appDelegate.ustate];
        
        // 주소변경 확인
        // 픽업지 이전 위치와 다르면
        isChangeFocus = [self onUpdateSrcAddr:srcGeoStr tts:YES];
        [self onUpdateDestAddr:dstGeoStr];
        
        // ETA, 주소, DriverInfo
        [self onUpdate130:LocalizedStr(@"Map.Rider.Pickup.top.text") src:srcGeoStr dst:dstGeoStr];
        
        // 드리이브 위치
        [self updateDrivePosition:service uname:uname lsrc:drvGeoStr];
        
        if (self->appDelegate.imgProfileDriver == nil) {
            [self onUR602]; // driver profile
        }
        if (self->appDelegate.imgVehicleDriver == nil) {
            [self onUR603]; // driver vehicle
        }
        
        // tripline 삭제
        [self cleanTripMapOnly];
    }
    else if (self->appDelegate.ustate == 140 && self->appDelegate.ustate2 == 10) { // on trip
        // 출도착 변경처리 필요
        
        drvGeoStr   = [Utils nullTolocation:[provider_device objectForKey:@"location"]];
        srcGeoStr   = [Utils nullTolocation:[drive_trip objectForKey:@"begin_location"]];
        dstGeoStr   = [Utils nullTolocation:[drive_trip objectForKey:@"ldst"]];
        
        service     = [Utils nullToString:drive_trip[@"service"]];
        
        // ETA : drive_trip_step.eduration, edistance : ETA 시간(분), 거리km
        //NSString* eta         = [NSString stringWithFormat:@"%d",[[drive_trip_step objectForKey:@"eduration"] intValue]];
        
        // fit
        [self onFitMap];
        //[self updateMapPadding:self->appDelegate.ustate];
        
        // 주소변경 확인
        // 픽업지 이전 위치와 다르면
        [self onUpdateSrcAddr:srcGeoStr tts:NO];
        [self onUpdateDestAddr:dstGeoStr];
        
        // ETA, 주소, DriverInfo
        [self onUpdate130:drive_trip_step src:srcGeoStr dst:dstGeoStr];
        
        // show drive marker
        // 드리이브 위치
        [self updateDrivePosition];
        
        if (self->appDelegate.imgProfileDriver == nil) {
            [self onUR602]; // driver profile
        }
        if (self->appDelegate.imgVehicleDriver == nil) {
            [self onUR603]; // driver vehicle
        }
    }
    else if ((self->appDelegate.ustate == 140 && self->appDelegate.ustate2 == 20) || (self->appDelegate.ustate == 130 && self->appDelegate.ustate2 <= -10) || (self->appDelegate.ustate == 140 && self->appDelegate.ustate2 <= -10)) { // pay
        
        return;
    }
    
    if (isChangeFocus) {
        [self pushCpos:btnCpos];
    }
    
    [self onRepareUI];
    //[self writeToDebugFile];
}

#pragma mark - initView - Rider
-(void)updateTopMainNavi {
    _imgTopTitle.image = [UIImage imageNamed:TITLE_IMG];
}

-(void) initvTopMainNavi {
    UIView *view__ = _vTopMainNavi;
    
    // view가 add되어 있으면
    if (view__.tag == 1) {
        view__.hidden = NO;
        [self.view bringSubviewToFront:view__];
        return;
    }
    else if (view__.tag == 2) { // hide
        return;
    }
    else if (view__.tag == 3) { // show
        view__.hidden = NO;
        view__.tag = 1;
        
        // 목적지를 지운다
        ldst = kCLLocationCoordinate2DInvalid;
        [self.view bringSubviewToFront:view__];
        return;
    }
    //------------------------------------------------------------------------------------
    //UIView *n = [super navigationView];
    NSInteger _y = kTopHeight;
    CGRect rect = view__.frame;
    rect.origin.y = _y;
    rect.size.width = self.view.bounds.size.width;
    view__.frame = rect;
    
    //[n addSubview:view__];
    [self.view addSubview:view__];
    view__.tag = [self getStateTag];
    view__.hidden = NO;
    //------------------------------------------------------------------------------------
    [self updateTopMainNavi];
}

-(void)swapTopButtonPosition:(BOOL)flag
{
    if (flag) {
        CGRect rect = _btnMenu.frame;
        rect.origin.x = 0;
        _btnMenu.frame = rect;
        
        rect = _btnClose.frame;
        rect.origin.x = self.view.frame.size.width - rect.size.width;
        _btnClose.frame = rect;
    }
    else {
        CGRect rect = _btnClose.frame;
        rect.origin.x = 0;
        _btnClose.frame = rect;
        
        rect = _btnMenu.frame;
        rect.origin.x = self.view.frame.size.width - rect.size.width;
        _btnMenu.frame = rect;
    }
    
}

-(void)updateTopRiderNaviCancel:(BOOL)flag {

    if (flag) {
        UIButton *btn = [self.vTopRiderNavi viewWithTag:1109];
        btn.layer.borderColor            = UIColorLabelTextColor.CGColor;
        btn.layer.borderWidth            = 1.0f;
        btn.layer.cornerRadius           = 4.0f;
        //            _btnClose.backgroundColor               = HEXCOLOR(0xf7f7f7FF);
        //            _btnClose.tintColor                     = HEXCOLOR(0xd42327FF);
        [btn setImage:nil forState:UIControlStateNormal];
        [btn setTitle:LocalizedStr(@"Button.CANCEL") forState:UIControlStateNormal];
        btn.hidden = NO;
        
        [_btnClose setImage:nil forState:UIControlStateNormal];
    }
    else {
        UIButton *btn = [self.vTopRiderNavi viewWithTag:1109];
        btn.hidden = YES;
        
        [_btnClose setImage:[UIImage imageNamed:@"btn_close_small"] forState:UIControlStateNormal];
    }

}
-(void)updateTopRiderNavi {
    if (!self->appDelegate.isDriverMode) {
        if (self->appDelegate.ustate == 100) {
            if (self->appDelegate.ustateold > 120) {
                _lbTopTitle.text = @"";
            }
            [_btnClose setImage:[UIImage imageNamed:@"btn_back"] forState:UIControlStateNormal];
            [self updateTopRiderNaviCancel:NO];
            _btnClose.hidden = NO;
            _btnMenu.hidden = YES;
            
            [self swapTopButtonPosition:NO];
            [self updateTopRiderNaviCancel:NO];
        }
        else if (self->appDelegate.ustate == 110) {
            if (self->appDelegate.ustateold > 120) {
                _lbTopTitle.text = @"";
            }
            [_btnClose setImage:[UIImage imageNamed:@"btn_back"] forState:UIControlStateNormal];
            [self updateTopRiderNaviCancel:NO];
            _btnClose.hidden = NO;
            _btnMenu.hidden = YES;
            
            [self swapTopButtonPosition:NO];
            [self updateTopRiderNaviCancel:NO];
        }
        else if (self->appDelegate.ustate == 130) {
            [_btnClose setImage:[UIImage imageNamed:@"btn_close_small"] forState:UIControlStateNormal];
            [self updateTopRiderNaviCancel:YES];
            
            _btnClose.hidden = NO;
            _btnMenu.hidden = NO;
            
            [self swapTopButtonPosition:YES];
            [self updateTopRiderNaviCancel:YES];
        }
        else if (self->appDelegate.ustate == 140) {
            _btnClose.hidden = YES;
            [_btnClose setImage:[UIImage imageNamed:@"btn_close_small"] forState:UIControlStateNormal];
            [self updateTopRiderNaviCancel:YES];
            _btnMenu.hidden = NO;
            
            [self swapTopButtonPosition:YES];
            [self updateTopRiderNaviCancel:YES];
        }
    }
}

-(void) initvTopRiderNavi {
    UIView *view__ = _vTopRiderNavi;
    
    // view가 add되어 있으면
    if (view__.tag == 1) {
        view__.hidden = NO;
        [self updateTopRiderNavi];
        [self.view bringSubviewToFront:view__];
        return;
    }
    else if (view__.tag == 2) { // hide
        return;
    }
    else if (view__.tag == 3) { // show
        view__.hidden = NO;
        view__.tag = 1;
        [self updateTopRiderNavi];
        [self.view bringSubviewToFront:view__];
        return;
    }
    
    //------------------------------------------------------------------------------------
    //UIView *n = [super navigationView];
    NSInteger _y = kTopHeight;
    CGRect rect = view__.frame;
    rect.origin.y = _y;
    rect.size.width = self.view.bounds.size.width;
    view__.frame = rect;
    
    //[n addSubview:view__];
    [self.view addSubview:view__];
    view__.tag = [self getStateTag];
    view__.hidden = NO;
    //------------------------------------------------------------------------------------
    //_lbTopTitle.text = @"";
    
    [self updateTopRiderNavi];
}

-(NSInteger) initvAddress:(NSInteger)_y {
    UIView *view__ = _vAddress;
    
    // view가 add되어 있으면
    if (view__.tag == 1) {
        view__.hidden = NO;
        
        CGRect frame = view__.frame;
        frame.origin.y = _y;
        view__.frame = frame;
        [self.view bringSubviewToFront:view__];
        return (view__.frame.origin.y + view__.frame.size.height);
    }
    else if (view__.tag == 2) { // hide
        view__.hidden = YES;
        
        CGRect frame = view__.frame;
        frame.origin.y = _y;
        view__.frame = frame;
        [self.view bringSubviewToFront:view__];
        return (view__.frame.origin.y + view__.frame.size.height);
    }
    else if (view__.tag == 3) { // show
        view__.hidden = NO;
        view__.tag = 1;
        
        CGRect frame = view__.frame;
        frame.origin.y = _y;
        view__.frame = frame;
        [self.view bringSubviewToFront:view__];
        return (view__.frame.origin.y + view__.frame.size.height);
    }
    
    //------------------------------------------------------------------------------------
    CGRect rect = view__.frame;
    rect.origin.y = _y;
    rect.size.width = self.view.bounds.size.width;
    view__.frame = rect;
    
    [self.view addSubview:view__];
    view__.tag = [self getStateTag];
    view__.hidden = NO;
    //------------------------------------------------------------------------------------
    _srcLocationTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    _dstLocationTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    
    if (_dstLocationTextField.tag != 9) {
        //_btnAddressClear.hidden = YES;
    }
    
#ifdef _WITZM
    
    _srcLocationTextField.textColor = UIColorDefault2;
    _dstLocationTextField.textColor = UIColorDefault;
#ifdef _DRIVER_MODE
    [Utils addShadow:_srcLocationBG];
    [Utils addShadow:_dstLocationBG];
#else
    [Utils addShadow:[_vAddress viewWithTag:80]];
    //[Utils addShadowOnlyRight:_srcLocationBG];
    //[Utils addShadow:_dstLocationBG];
#endif
    
#elif defined _CHACHA
    _srcLocationBG.backgroundColor = UIColorDefault;
    _dstLocationBG.backgroundColor = UIColorDefault2;
    
    #ifdef _DRIVER_MODE
        _srcLocationTextField.textColor = UIColorButtonText;
        _dstLocationTextField.textColor = UIColorButtonText;
        [Utils addShadow:_srcLocationBG];
        [Utils addShadow:_dstLocationBG];
    #else
        _srcLocationBG.backgroundColor = UIColorInputTextBG;
        _dstLocationBG.backgroundColor = UIColorInputTextBG;
        _srcLocationTextField.textColor = UIColorDefault;
        _dstLocationTextField.textColor = UIColorDefault2;
        _srcLocationTextField.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:14];
        _dstLocationTextField.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:14];
        [Utils addShadow:[_vAddress viewWithTag:80]];
        //[Utils addShadowOnlyRight:_srcLocationBG];
        //[Utils addShadow:_dstLocationBG];
    #endif
#endif
    //------------------------------------------------------------------------------------
    return (view__.frame.origin.y + view__.frame.size.height);
}

- (void)commonFavorite:(UIView*)view {
    
    view.layer.cornerRadius = view.frame.size.width/2;
    view.clipsToBounds = YES;
    view.layer.masksToBounds = YES;
    
#ifdef _WITZM
    view.backgroundColor = HEXCOLOR(0x333333FF);
#elif defined _CHACHA
    view.backgroundColor = HEXCOLOR(0xffffffD9);
#endif


    UILabel *lb = [view viewWithTag:1099];
    lb.text = @"";
    
#ifdef _WITZM
    lb.textColor = UIColorButtonText;
#elif defined _CHACHA
    lb.textColor = HEXCOLOR(0x333333FF);
#endif

    //    lb.font = [UIFont fontWithName:@"HelveticaNeue-Regular" size:12];
    //    lb.numberOfLines = 1;
    //    lb.lineBreakMode = NSLineBreakByTruncatingTail;
    
    [Utils addShadow:view];
}

-(void)updateFavoriteFrame:(BOOL)_f1 f2:(BOOL)_f2 f3:(BOOL)_f3 {
    
    NSMutableArray *array = [[NSMutableArray alloc] init];
    if (_f1) {
        [array addObject:_vFavoriteHome];
    }
    if (_f2) {
        [array addObject:_vFavoriteWork];
    }
    if (_f3) {
        [array addObject:_vFavoriteLast];
    }
    
    NSInteger __gap = (self.view.frame.size.width - _vFavoriteHome.frame.size.width * [array count])/([array count]+1);
    
    UIView *prev = nil;
    for (UIView *v in array) {
        //
        CGRect rect = v.frame;
        if (prev == nil) {
            rect.origin.x = __gap;
        }
        else {
            rect.origin.x = prev.frame.origin.x + prev.frame.size.width + __gap;
        }
        prev = v;
        v.frame = rect;
        v.hidden = NO;
    }
}

-(void)checkErrorFavoriteText {
    if (!_lbFavoriteHome.hidden && [[Utils nullToString:_lbFavoriteHome.text] isEqualToString:@""]) {
        [self updateFavorite];
    }
    if (!_lbFavoriteWork.hidden && [[Utils nullToString:_lbFavoriteWork.text] isEqualToString:@""]) {
        [self updateFavorite];
    }
    if (!_lbFavoriteLast.hidden && [[Utils nullToString:_lbFavoriteLast.text] isEqualToString:@""]) {
        [self updateFavorite];
    }
}

-(void)updateFavorite {
    if ( [_srcLocationTextField.text isEqualToString:@""] || [_dstLocationTextField.text isEqualToString:@""]) {

//        NSLog(@"_btnFavoriteLast:%@",_btnFavoriteLast);
        [dicFavorite removeAllObjects];
        
        _vFavoriteHome.hidden = YES;
        _vFavoriteWork.hidden = YES;
        _vFavoriteLast.hidden = YES;
        
        BOOL f1 = NO;
        BOOL f2 = NO;
        BOOL f3 = NO;
        
        // delete dummy
//        NSString *loc = [NSString stringWithFormat:@"0.000000,0.000000"];
//        PLACE_DELETE_FAVORITE(loc);
        PLACE_DELETE_NULL;
        
        NSArray *historyAddress = (NSArray*)PLACE_SELECT;
        for (NSDictionary *items__ in historyAddress) {
            // Home
            if ([items__[@"type"] intValue] == 1) {
                if ([[Utils nullToString:items__[@"place"]] isEqualToString:@""]) {
                    _lbFavoriteHome.text = LocalizedStr(@"Map.Search.Address.HOME.text");
                }
                else {
                    _lbFavoriteHome.text = items__[@"place"];
                }
                UIImageView *img = [_vFavoriteHome viewWithTag:30];
                if ([[Utils nullToString:items__[@"location"]] isEqualToString:@""] || [[Utils nullToString:items__[@"location"]] isEqualToString:@"0,0"]) {
                    img.hidden = YES;
                }
                else {
                    img.hidden = NO;
                    [dicFavorite setObject:items__ forKey:[NSString stringWithFormat:@"%ld",(long)_btnFavoriteHome.tag]];
                    f1 = YES;
                }
            }
            // Work
            else if ([items__[@"type"] intValue] == 2) {
                if ([[Utils nullToString:items__[@"place"]] isEqualToString:@""]) {
                    _lbFavoriteWork.text = LocalizedStr(@"Map.Search.Address.WORK.text");
                }
                else {
                    _lbFavoriteWork.text = items__[@"place"];
                }
                UIImageView *img = [_vFavoriteWork viewWithTag:30];
                if ([[Utils nullToString:items__[@"location"]] isEqualToString:@""] || [[Utils nullToString:items__[@"location"]] isEqualToString:@"0,0"]) {
                    img.hidden = YES;
                }
                else {
                    img.hidden = NO;
                    [dicFavorite setObject:items__ forKey:[NSString stringWithFormat:@"%ld",(long)_btnFavoriteWork.tag]];
                    f2 = YES;
                }
            }
            // last
            else if ([items__[@"type"] intValue] == 3) {
                BOOL isDup = NO;
                // 같은 장소가 있었다면 무시한다.
                for (NSString *tag in dicFavorite) {
                    NSDictionary *item = dicFavorite[tag];
                    if ([items__[@"location"] isEqualToString:item[@"location"]]) {
                        isDup = YES;
                        break;
                    }
                }
                if (!isDup) {
                    _lbFavoriteLast.text = items__[@"place"];
                    UIImageView *img = [_vFavoriteLast viewWithTag:30];
                    if ([[Utils nullToString:items__[@"location"]] isEqualToString:@""] || [[Utils nullToString:items__[@"location"]] isEqualToString:@"0,0"]) {
                        img.hidden = YES;
                    }
                    else {
                        img.hidden = YES;//NO;//별
                        [dicFavorite setObject:items__ forKey:[NSString stringWithFormat:@"%ld",(long)_btnFavoriteLast.tag]];
                        f3 = YES;
                    }
                    break;
                }
                
            }
        }
        [self updateFavoriteFrame:f1 f2:f2 f3:f3];
    }
}


-(NSInteger) initvFavorite:(NSInteger)_y {
    UIView *view__ = _lbFavoriteSetting;
    
    // view가 add되어 있으면
    if (view__.tag == 1) {
        view__.hidden = NO;
        
        [self updateFavorite];
        return (view__.frame.origin.y + view__.frame.size.height);
    }
    else if (view__.tag == 2) { // hide
        view__.hidden = YES;
        
        return (view__.frame.origin.y + view__.frame.size.height);
    }
    else if (view__.tag == 3) { // show
        view__.hidden = NO;
        view__.tag = 1;
        
        [self updateFavorite];
        [self.view bringSubviewToFront:view__];
        return (view__.frame.origin.y + view__.frame.size.height);
    }
    
    //------------------------------------------------------------------------------------
    CGRect rect = view__.frame;
    rect.origin.y = _y;
    rect.size.width = self.view.bounds.size.width;
    view__.frame = rect;
    
    [self.mapView_ addSubview:view__];
    view__.tag = [self getStateTag];
    view__.hidden = NO;
    //------------------------------------------------------------------------------------
    
    _lbFavoriteHome = [_vFavoriteHome viewWithTag:1099];
    _btnFavoriteHome = [_vFavoriteHome viewWithTag:24];
    _btnFavoriteHome.tag = 24;
    _lbFavoriteWork = [_vFavoriteWork viewWithTag:1099];
    _btnFavoriteWork = [_vFavoriteWork viewWithTag:24];
    _btnFavoriteWork.tag = 25;
    _lbFavoriteLast = [_vFavoriteLast viewWithTag:1099];
    _btnFavoriteLast = [_vFavoriteLast viewWithTag:24];
    _btnFavoriteLast.tag = 26;
    
    // favorite의 상태정보처리를 위해 임시로 사용한다.
    _lbFavoriteSetting = [_vFavoriteSetting viewWithTag:1099];
    _lbFavoriteSetting.tag = 1;
    _btnFavoriteSetting  = [_vFavoriteSetting viewWithTag:24];
    _btnFavoriteSetting.tag = 27;
    
    [_btnFavoriteHome addTarget:self action:@selector(selectFavoriteDestAddress:) forControlEvents:UIControlEventTouchUpInside];
    [_btnFavoriteWork addTarget:self action:@selector(selectFavoriteDestAddress:) forControlEvents:UIControlEventTouchUpInside];
    [_btnFavoriteLast addTarget:self action:@selector(selectFavoriteDestAddress:) forControlEvents:UIControlEventTouchUpInside];
    
    [self commonFavorite:_vFavoriteHome];
    [self commonFavorite:_vFavoriteWork];
    [self commonFavorite:_vFavoriteLast];
    
    _lbFavoriteHome.text = LocalizedStr(@"Map.Search.Address.HOME.text");
    _lbFavoriteWork.text = LocalizedStr(@"Map.Search.Address.WORK.text");
    _lbFavoriteLast.text = @"";
    
    //
    
    rect = _vFavoriteHome.frame;
    //rect.origin.y = _vAddress.frame.origin.y - 30 - kMainNaviTopHeight;
    rect.origin.y = self.view.frame.size.height - _vAddress.frame.size.height - (kBottomSafeArea + 20) - rect.size.height;
    
    _vFavoriteHome.frame = rect;
    _vFavoriteHome.hidden = YES;
    
    rect = _vFavoriteWork.frame;
    rect.origin.y = _vFavoriteHome.frame.origin.y;
    _vFavoriteWork.frame = rect;
    _vFavoriteWork.hidden = YES;
    
    rect = _vFavoriteLast.frame;
    rect.origin.y = _vFavoriteHome.frame.origin.y;
    _vFavoriteLast.frame = rect;
    _vFavoriteLast.hidden = YES;
    
    [self.mapView_ addSubview:_vFavoriteHome];
    [self.mapView_ addSubview:_vFavoriteWork];
    [self.mapView_ addSubview:_vFavoriteLast];
    
    [self updateFavorite];
    
    //------------------------------------------------------------------------------------
    return (view__.frame.origin.y + view__.frame.size.height);
}


-(void)initDriveStatus {
    _lbDriveStatus.text = @"";
    
    _lbDriverRating.text = @"";
    
    //_lbDriveri18n.text = LocalizedStr(@"String.Driver");
    _lbDriveri18n.text = @"";
    _lbDriver.text = @"";
    
    _lbVehicleModel.text = @"";
    _lbVehicle.text = @"";
    
    
    _imgDriver.image = nil;
    _imgVehicle.image = nil;
    [_btnimgDriver setImage:nil forState:UIControlStateNormal];
    [_btnimgVehicle setImage:nil forState:UIControlStateNormal];
}

-(void)updateDriveStatus {
    if (self->appDelegate.ustate == 130 && self->appDelegate.ustate2 == 10) {
        _lbDriveStatus.text = LocalizedStr(@"Map.Drive.OnPickup.title");
        _lbDriveStatus.textColor = UIColorDefault2;
    }
    else if (self->appDelegate.ustate == 130 && self->appDelegate.ustate2 == 20) {
        _lbDriveStatus.text = LocalizedStr(@"Map.Drive.PickupArrived.title");
        _lbDriveStatus.textColor = UIColorDefault;
    }
    else if (self->appDelegate.ustate == 140 && self->appDelegate.ustate2 == 10) {
        _lbDriveStatus.text = LocalizedStr(@"Map.Drive.OnTrip.title");
        _lbDriveStatus.textColor = UIColorDefault2;
    }
    
    [self updateDriveInfo];
}

-(void)updateDriveInfo {
    if ([_lbDriverRating.text isEqualToString:@""]) {
        _lbDriverRating.text = [Utils getDriverRate];
    }
    
    if ([_lbVehicleModel.text isEqualToString:@""]) {
        NSDictionary *__vehicle = [self->appDelegate.dicRider objectForKey:@"provider_device"];
        
        if ([Utils isDictionary:__vehicle]) {
            
            _lbDriverRating.text = [Utils getDriverRate];
            _lbDriveri18n.text = [Utils nullToString:__vehicle[@"service_name"]];
            _lbDriver.text = [NSString stringWithFormat:@"%@\n",[Utils nameOfuname:__vehicle[@"name"]]];
            _lbVehicleModel.text = [Utils nullToString:__vehicle[@"vehiclemodel"]];
            _lbVehicle.text = [NSString stringWithFormat:@"%@\n",[Utils nullToString:__vehicle[@"vehicleno"]]];
            
            //[_lbDriver sizeToFit];
            //[_lbVehicle sizeToFit];
            
            if (self->appDelegate.imgProfileDriver == nil) {
                [self onUR602]; // driver profile
            }
            if (self->appDelegate.imgVehicleDriver == nil) {
                [self onUR603]; // driver vehicle
            }
        }
    }
}

-(NSInteger) initvDriveStatus:(NSInteger)_y {
    UIView *view__ = _vDriveStatus;
    
    // view가 add되어 있으면
    if (view__.tag == 1) {
        view__.hidden = NO;
        
        CGRect frame = view__.frame;
        frame.origin.y = _y;
        view__.frame = frame;
        [self updateDriveStatus];
        return (view__.frame.origin.y + view__.frame.size.height);
    }
    else if (view__.tag == 2) { // hide
        view__.hidden = YES;
        
        CGRect frame = view__.frame;
        frame.origin.y = _y;
        view__.frame = frame;
        return (view__.frame.origin.y + view__.frame.size.height);
    }
    else if (view__.tag == 3) { // show
        view__.hidden = NO;
        view__.tag = 1;
        
        CGRect frame = view__.frame;
        frame.origin.y = _y;
        view__.frame = frame;
        [self updateDriveStatus];
        //[self.view bringSubviewToFront:view__];
        return (view__.frame.origin.y + view__.frame.size.height);
    }
    
    //------------------------------------------------------------------------------------
    CGRect rect = view__.frame;
    rect.origin.y = _y;
    rect.size.width = self.view.bounds.size.width;
    view__.frame = rect;
    
    [self.mapView_ addSubview:view__];
    view__.tag = [self getStateTag];
    view__.hidden = NO;
    //------------------------------------------------------------------------------------
    [view__ setBackgroundColor:[UIColor clearColor]];
    
    CAGradientLayer *layer = [CAGradientLayer layer];
    layer.frame = view__.bounds;
    UIColor *theColor = [UIColor whiteColor];
    layer.colors = [NSArray arrayWithObjects:
                    //                    (id)[[UIColor clearColor] CGColor],
                    //                    (id)[[UIColor clearColor] CGColor],
                    (id)[[theColor colorWithAlphaComponent:0.1f] CGColor],
                    (id)[[theColor colorWithAlphaComponent:0.3f] CGColor],
                    (id)[[theColor colorWithAlphaComponent:0.5f] CGColor],
                    (id)[[theColor colorWithAlphaComponent:0.6f] CGColor],
                    (id)[[theColor colorWithAlphaComponent:0.7f] CGColor],
                    (id)[[theColor colorWithAlphaComponent:0.8f] CGColor],
                    (id)[[theColor colorWithAlphaComponent:0.9f] CGColor],
                    (id)[theColor CGColor],
                    (id)[theColor CGColor],
                    nil];
    
    [view__.layer insertSublayer:layer atIndex:0];
    
    _lbDriveStatus.textColor = [UIColor blackColor];
    
    _lbDriveri18n.text = LocalizedStr(@"String.Driver");
    _lbDriveri18n.textColor = HEXCOLOR(0x666666FF);
    _lbDriver.textColor = HEXCOLOR(0x333333FF);
    
    _lbVehicleModel.textColor = HEXCOLOR(0x333333FF);
    _lbVehicle.textColor = HEXCOLOR(0x666666FF);
    
    [Utils setCircleImage:_imgDriver];
    [Utils setCircleImage:_imgVehicle];
    
    [Utils addShadow:_btnimgDriver];
    [Utils addShadow:_btnimgVehicle];
    
    [self initDriveStatus];
    
    _btnTripShare.tag = 1410;
    _btnTripShare.backgroundColor = UIColorDefault;
    [_btnTripShare setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_btnTripShare setTitle:LocalizedStr(@"Menu.ShareTrip.Menu.title") forState:UIControlStateNormal];
    //------------------------------------------------------------------------------------
    
    [self updateDriveStatus];
    
    return (view__.frame.origin.y + view__.frame.size.height);
}

-(NSInteger) initvCallChat:(NSInteger)_y {
    UIView *view__ = _vCallChat;
    
    // view가 add되어 있으면
    if (view__.tag == 1) {
        view__.hidden = NO;
        
        return (view__.frame.origin.y + view__.frame.size.height);
    }
    else if (view__.tag == 2) { // hide
        view__.hidden = YES;
        
        return (view__.frame.origin.y + view__.frame.size.height);
    }
    else if (view__.tag == 3) { // show
        view__.hidden = NO;
        view__.tag = 1;
        
        CGRect rect = view__.frame;
        rect.origin.y = _y;
        rect.size.width = self.view.bounds.size.width;
        view__.frame = rect;
        [self.view bringSubviewToFront:view__];
        return (view__.frame.origin.y + view__.frame.size.height);
    }
    
    //------------------------------------------------------------------------------------
    CGRect rect = view__.frame;
    rect.origin.y = _y;
    rect.size.width = self.view.bounds.size.width;
    view__.frame = rect;
    
    [self.mapView_ addSubview:view__];
    view__.tag = [self getStateTag];
    view__.hidden = NO;
    //------------------------------------------------------------------------------------
    NSString *text = [NSString stringWithFormat:@" %@", LocalizedStr(@"Button.CALL")];
    _btnCall.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:18];
    [_btnCall setTitle:text forState:UIControlStateNormal];
    [_btnCall setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    text = [NSString stringWithFormat:@" %@", LocalizedStr(@"Button.CHAT")];
    _btnChat.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:18];
    [_btnChat setTitle:text forState:UIControlStateNormal];
    [_btnChat setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    //------------------------------------------------------------------------------------
    return (view__.frame.origin.y + view__.frame.size.height);
}

- (void)updateOrder:(UIView*)view {
    
    UIView *aView = [view viewWithTag:900];
    aView.layer.cornerRadius = 50;
    aView.clipsToBounds = YES;
    aView.layer.masksToBounds = YES;
    //view.backgroundColor = HEXCOLOR(0x333333FF);
}

-(IBAction)chkButtonPressed:(id)sender {
    UIButton *button = (UIButton*)sender;
    UIView *bView;
    
    for (UIView *aView in _vOrderSelectView.subviews) {
        UIButton *btn = [aView viewWithTag:10];
        if ([button isEqual:btn]) {
            bView = aView;
        }
        btn = [aView viewWithTag:11];
        if ([button isEqual:btn]) {
            bView = aView;
        }
        if (![button isEqual:btn] && btn.tag == 11) {
            [self changeButton:btn isOn:NO];
            /*
             [UIView animateWithDuration:0.5
             animations:^{
             CGRect frame = aView.frame;
             frame.origin.y = 120;
             aView.frame = frame;
             }
             completion:^(BOOL finished) {
             }];
             */
        }
    }
    
    if (button.tag == 11) {
        [self changeButton:button isOn:NO];
        /*
         
         [UIView animateWithDuration:0.5
         animations:^{
         CGRect frame = bView.frame;
         frame.origin.y = 120;
         bView.frame = frame;
         }
         completion:^(BOOL finished) {
         }];
         */
    }
    else {
        [self changeButton:button isOn:YES];

        int idx = (int)bView.tag;
        [self updateServiceType:idx];
        
        /*
         [UIView animateWithDuration:0.5
         animations:^{
         CGRect frame = bView.frame;
         frame.origin.y = 0;
         bView.frame = frame;
         }
         completion:^(BOOL finished) {
         }];
         */
    }
    
}

-(void)changeButton:(UIButton*)button isOn:(BOOL)flag {
    
    // on
    if (flag) {
        [button setImage:[UIImage imageNamed:@"form_orderchk_chk"] forState:UIControlStateNormal];
        button.tag = 11;
    }
    else {
//        [self setServiceType:@""];

        [button setImage:[UIImage imageNamed:@"form_orderchk_normal"] forState:UIControlStateNormal];
        button.tag = 10;
    }
}

-(void)unSelectButton {
    for (UIView *aView in _vOrderSelectView.subviews) {
        UIButton *btn = [aView viewWithTag:21];
        [self selectButton:btn isOn:NO];
        
        [UIView animateWithDuration:0.1
                         animations:^{
                             CGRect frame = aView.frame;
                             frame.origin.y = 100;
                             aView.frame = frame;
                         }
                         completion:^(BOOL finished) {
                         }];
    }
}

-(IBAction)selectButtonPressed:(id)sender {
    UIButton *button = (UIButton*)sender;
    UIView *bView;
    
    if (button.tag == 21) {
        return;
    }
    
    for (UIView *aView in _vOrderSelectView.subviews) {
        UIButton *btn = [aView viewWithTag:20];
        if ([button isEqual:btn]) {
            bView = aView;
        }
        btn = [aView viewWithTag:21];
        if ([button isEqual:btn]) {
            bView = aView;
        }
        
        UIButton *cbtn = [aView viewWithTag:11];
        if (![button isEqual:btn] && cbtn.tag == 11) {
            [self changeButton:cbtn isOn:NO];
            [aView viewWithTag:80].backgroundColor = [UIColor whiteColor];
            UILabel *lbType = [aView viewWithTag:102];
            lbType.textColor = [UIColor blackColor];
        }
        
        if (![button isEqual:btn] && btn.tag == 21) {
            [self selectButton:btn isOn:NO];
            [aView viewWithTag:80].backgroundColor = [UIColor whiteColor];
            UILabel *lbType = [aView viewWithTag:102];
            lbType.textColor = [UIColor blackColor];
            
            [UIView animateWithDuration:0.1
                             animations:^{
                                 CGRect frame = aView.frame;
                                 frame.origin.y = 100;
                                 aView.frame = frame;
                             }
                             completion:^(BOOL finished) {
                             }];
            
        }
        
        [NSRunLoop cancelPreviousPerformRequestsWithTarget:self selector:@selector(unSelectButton) object:nil];
    }
    
    if (button.tag == 21) {
        [self selectButton:button isOn:NO];
        [bView viewWithTag:80].backgroundColor = [UIColor whiteColor];
        UILabel *lbType = [bView viewWithTag:102];
        lbType.textColor = [UIColor blackColor];

        [NSRunLoop cancelPreviousPerformRequestsWithTarget:self selector:@selector(unSelectButton) object:nil];
        
        [UIView animateWithDuration:0.1
                         animations:^{
                             CGRect frame = bView.frame;
                             frame.origin.y = 100;
                             bView.frame = frame;
                         }
                         completion:^(BOOL finished) {
                         }];
         
    }
    else {
        [self selectButton:button isOn:YES];
        [bView viewWithTag:80].backgroundColor = UIColorDefault;
        UILabel *lbType = [bView viewWithTag:102];
        lbType.textColor = [UIColor whiteColor];
        
        UIButton *btn = [bView viewWithTag:10];
        if (btn) {
            [self changeButton:btn isOn:YES];
            int idx = (int)bView.tag;
            [self updateServiceType:idx];
            //NSArray *drive_estimates = estimate_result[@"drive_estimates"];
            //[_btnRequest setTitle:drive_estimates[idx][@"service_button"] forState:UIControlStateNormal];
            [_btnRequest setTitle:LocalizedStr(@"Map.Vechile.Request.BTN.text") forState:UIControlStateNormal];
        }
        
        [self performSelector:@selector(unSelectButton) withObject:nil afterDelay:1.5];
        
        [UIView animateWithDuration:0.1
                         animations:^{
                             CGRect frame = bView.frame;
                             frame.origin.y = 10;
                             bView.frame = frame;
                         }
                         completion:^(BOOL finished) {
                         }];
         
    }
}

-(void)selectButton:(UIButton*)button isOn:(BOOL)flag {
    
    // on
    if (flag) {
        button.tag = 21;
    }
    else {
        button.tag = 20;
    }
}

-(UIImage*)orderService:(NSString*)service btn:(UIButton*)button{
/*
    if ([[service uppercaseString] isEqualToString:DEFAULT_SERVICE]) {
        
        [self setServiceType:DEFAULT_SERVICE];
        
        [self changeButton:button isOn:YES];
    }
*/
    return [UIImage imageNamed:[NSString stringWithFormat:@"img_service_%@",service]];
}

-(void)updateServiceType:(NSInteger)idx {
    NSArray *drive_estimates = estimate_result[@"drive_estimates"];
    if ([Utils isArray:drive_estimates]) {
        if ([Utils nullToString:drive_estimates[idx][@"service"]]) {
            serviceType = [drive_estimates[idx][@"service"] uppercaseString];
        }
        else {
            serviceType = DEFAULT_SERVICE;
        }
        [[[TXApp instance] getSettings] setFDKeychain:@"serviceType" value:serviceType];
    }
}

#ifndef _DRIVER_MODE
-(void)getServiceType {
    serviceType = [[[TXApp instance] getSettings] getFDKeychain:@"serviceType"];
    if (serviceType == nil) {
        serviceType = DEFAULT_SERVICE;
        [self setServiceType:DEFAULT_SERVICE];
    }
}

-(void)setServiceType:(NSString*)stype {
    serviceType = stype;
    [[[TXApp instance] getSettings] setFDKeychain:@"serviceType" value:serviceType];
}
#endif

-(void)updateUIServiceType {
    /*
    BOOL flag = NO;
    
    
    for (UIView *aView in _vOrderSelectView.subviews) {
        UIButton *btn = [aView viewWithTag:11];
        if (btn) {
            flag = YES;
        }
    }
    
    if (!flag) {
        [self initOrderSelect];
    }
     */
}

/*
 "fare_discount" = 0;
 "fare_estimate" = "7.65";
 "fare_max" = "9.18";
 "fare_min" = "7.65";
 "fare_rate" = "1.0";
 "fare_safety" = "1.65";
 "fare_trip" = 6;
 ldst = "";
 lsrc = "37.403677,127.101449";
 "passenger_max" = 7;
 service = P;
 "trip_distance" = 0;
 "trip_duration" = 0;
 "update_date" = "2017-05-10 15:05:56";
 */
- (void)initOrderSelect {
    
    NSLog(@"subview:%lu",(unsigned long)[_vOrderSelectView.subviews count]);
//    if ([_vOrderSelectView.subviews count]>2) {
//        return;
//    }
    
    NSArray *drive_estimates = estimate_result[@"drive_estimates"];
    if (![Utils isArray:drive_estimates]) {
        return;
        //drive_estimates = self->appDelegate.dicRider[@"drive_estimates"];
    }
    CGRect rect = _vOrderSelectView.frame;
    [_vOrderSelectView removeFromSuperview];
    
    _vOrderSelectView = [[UIScrollView alloc] initWithFrame:rect];
    [_vOrder addSubview:_vOrderSelectView];
    [_vOrder sendSubviewToBack:_vOrderSelectView];
    
    //offset      = CGPointMake(0,0);
    
    [self getServiceType];
    if ([serviceType isEqualToString:@""]) {
        //[self setServiceType:DEFAULT_SERVICE];
    }
    int cnt = 0;
    NSString *__service = @"";
    CGRect frame = _vOrderSelect.frame;
    for (NSDictionary *estimate in drive_estimates) {
        if ([Utils isDictionary:estimate]) {
            cnt++;
        }
    }
    NSInteger __gap = (self.view.frame.size.width - _vOrderSelect.frame.size.width * cnt)/(cnt+1);
    // 3개를 기준으로 잡는다. 3개 이상이면 3개를 보여준다
    if (cnt>3) {
        __gap = (self.view.frame.size.width - _vOrderSelect.frame.size.width * 3)/4;
    }
    cnt = 0;
    for (NSDictionary *estimate in drive_estimates) {
        if ([Utils isDictionary:estimate]) {
            UIView *aView = [Utils clone:_vOrderSelect];
            //[Utils addShadow:aView];
            [self updateOrder:aView];
            if (cnt == 0) {
                frame.origin.x = __gap;
            }
            else {
                frame.origin.x = frame.origin.x + frame.size.width + __gap;
            }
            frame.origin.y = 120-20;
            aView.frame = frame;
            aView.tag = cnt;
            
            UIButton *button = [aView viewWithTag:100];
            [self changeButton:button isOn:NO];
            button.userInteractionEnabled = NO;
            [Utils addShadow:button];
            //[button addTarget:self action:@selector(chkButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
            
            UIButton *btn = [aView viewWithTag:200];
            [btn addTarget:self action:@selector(selectButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
            [self selectButton:btn isOn:NO];
            [aView viewWithTag:80].backgroundColor = [UIColor whiteColor];
            
            UILabel *lbPrice = [aView viewWithTag:101];
            
#ifdef _DRIVER_MODE
            
            NSString *price = [NSString stringWithFormat:@"%@\n~\n%@",CURRENCY_FORMAT(estimate[@"fare_min"]),CURRENCY_FORMAT(estimate[@"fare_max"])];
            
#else
            
#ifdef _WITZM
            //NSString *price = [NSString stringWithFormat:@"%@",CURRENCY_FORMAT(estimate[@"fare_estimate"])];
            NSString *price = [NSString stringWithFormat:@"%@\n~\n%@",CURRENCY_FORMAT(estimate[@"fare_min"]),CURRENCY_FORMAT(estimate[@"fare_max"])];
#elif defined _CHACHA
            NSString *price = [NSString stringWithFormat:@"%@\n~\n%@",CURRENCY_FORMAT(estimate[@"fare_min"]),CURRENCY_FORMAT(estimate[@"fare_max"])];
#endif
            
#endif

            lbPrice.text = price;
            UILabel *lbType = [aView viewWithTag:102];
            lbType.textColor = [UIColor blackColor];
            lbType.text = estimate[@"service_name"];
            UIImageView *img = [aView viewWithTag:103];
            img.image = [self orderService:estimate[@"service"] btn:button];
            //
            if ([[estimate[@"service"] uppercaseString] isEqualToString:serviceType]) {
//            if ([[estimate[@"service"] uppercaseString] isEqualToString:DEFAULT_SERVICE]) {
                [aView viewWithTag:80].backgroundColor = UIColorDefault;
                UILabel *lbType = [aView viewWithTag:102];
                lbType.textColor = [UIColor whiteColor];
                
                [self changeButton:button isOn:YES];
                [self selectButton:btn isOn:YES];
                btn.tag = 20;
                
                //[_btnRequest setTitle:estimate[@"service_button"] forState:UIControlStateNormal];
                [_btnRequest setTitle:LocalizedStr(@"Map.Vechile.Request.BTN.text") forState:UIControlStateNormal];
            }
            //UILabel *lbDesc = [aView viewWithTag:104];
            
//            if ([estimate[@"service"] isEqualToString:DEFAULT_SERVICE]) {
//                __service = DEFAULT_SERVICE;
//            }
            [_vOrderSelectView addSubview:aView];
            cnt++;
        }
    }
    
    // 스크롤 뷰의 컨텐츠 사이즈를 미리 만들어둡니다.
    CGSize contentSize = _vOrderSelectView.frame.size;
    contentSize.width = frame.origin.x + frame.size.width + __gap;
    //contentSize.width = _vOrderSelectView.frame.size.width * cnt;
    //[_vOrderSelectView setContentOffset:CGPointMake(_vOrderSelect.frame.size.width, 0.0f)];
    
    // 스크롤 뷰의 컨텐츠 사이즈를 설정합니다.
    [_vOrderSelectView setContentSize:contentSize];
    
    if (![__service isEqualToString:@""]) {
        [self setServiceType:DEFAULT_SERVICE];
    }
    
    [_vOrderSelectView setContentOffset:CGPointZero animated:YES];
    
    [_vOrder viewWithTag:779].hidden = YES;
}

- (void)updteOrderSelect {
    /*
    for (UIView *aView in _vOrderSelectView.subviews) {
        UIButton* btn = [aView viewWithTag:21];
        if (btn) {
            [self selectButton:btn isOn:NO];
        }
        btn = [aView viewWithTag:11];
        if (btn) {
            [self changeButton:btn isOn:NO];
        }
    }
     */
}

- (void)updtePriceOrderSelect {
    /*
    for (UIView *aView in _vOrderSelectView.subviews) {
        UILabel *lbPrice = [aView viewWithTag:101];
        lbPrice.text = @"...";
    }
     */
}

-(NSInteger) initvOrder:(NSInteger)_y {
    UIView *view__ = _vOrder;
    // 선택불가색
    [_btnRequest setTitleColor:HEXCOLOR(0xB6B6B6FF) forState:UIControlStateNormal];
    _btnRequest.enabled = NO;
    
    // view가 add되어 있으면
    if (view__.tag == 1) {
        view__.hidden = NO;
        
        [_btnRequest setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _btnRequest.enabled = YES;
        [_vOrder viewWithTag:779].hidden = NO;
        [self.view bringSubviewToFront:view__];
        return (view__.frame.origin.y + view__.frame.size.height);
    }
    else if (view__.tag == 2) { // hide
        view__.hidden = YES;
        
        
        return (view__.frame.origin.y + view__.frame.size.height);
    }
    else if (view__.tag == 3) { // show
        view__.hidden = NO;
        view__.tag = 1;
        
//        if (!_vOrderSelectView.subviews.count) {
//            [self initOrderSelect];
//        }
        
        [_btnRequest setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _btnRequest.enabled = YES;
        [_vOrder viewWithTag:779].hidden = NO;
        [self.view bringSubviewToFront:view__];
        
//        [self updateUIServiceType];
        //        [self updteOrderSelect];
        return (view__.frame.origin.y + view__.frame.size.height);
    }
    
    //------------------------------------------------------------------------------------
    CGRect rect = view__.frame;
    rect.origin.y = _y;
    rect.size.width = self.view.bounds.size.width;
    view__.frame = rect;
    
    //[self.mapView_.layer addSublayer:view__.layer];
    //[self.view addSubview:view__];
    [self.mapView_ addSubview:view__];
    view__.tag = [self getStateTag];
    view__.hidden = NO;
    //------------------------------------------------------------------------------------
    NSString* no = [self getAvailableCardNo];
    if ([no isEqualToString:@""]) {
        no = @"-";
    }
    _lbCardInfo.text = no;
    _lbCardInfo.textColor = HEXCOLOR(0x333333FF);
    _lbCouponInfo.text = LocalizedStr(@"Map.Vechile.Request.CouponCode.title");
    _lbCouponInfo.textColor = HEXCOLOR(0x333333FF);
    
    UILabel *orderLabel = [_vOrderBottomView viewWithTag:690];
    orderLabel.text = LocalizedStr(@"Map.Vechile.Request.Desc.text");
    
    //[self initOrderSelect];
    
    // 스크롤 뷰의 Delegate를 설정합니다. ScrollView Delegate 함수를 사용하기 위함입니다.
    [_vOrderSelectView setDelegate:self];
    // 스크롤 뷰의 페이징 기능을 ON합니다.
    [_vOrderSelectView setPagingEnabled:NO];
    // 스크롤 뷰의 Bounce를 Disabled합니다 첫 페이지와 마지막 페이지에서 애니메이션이 발생하지않습니다.
    [_vOrderSelectView setBounces:YES];
    [_vOrderSelectView setScrollsToTop:NO];
    [_vOrderSelectView setScrollEnabled:YES];
    [_vOrderSelectView setClipsToBounds:NO];
    
    // 스크롤 바들을 보이지 않습니다.
    _vOrderSelectView.showsHorizontalScrollIndicator = NO;
    _vOrderSelectView.showsVerticalScrollIndicator = NO;
    
    // 스크롤뷰를 터치할 경우 컨텐츠 내부의 뷰에 이벤트 전달
//        [_vOrderSelectView touchesShouldBegin:touches withEvent:evt inContentView:self.mapView_];
//        [_vOrderSelectView touchesShouldCancelInContentView:self.mapView_];
    
    //[_btnCoupon setTitleColor:UIColorDefault forState:UIControlStateNormal];
    //[_btnCoupon setTitle:@"" forState:UIControlStateNormal];
    [_btnRequest setTitle:LocalizedStr(@"Map.Vechile.Request.BTN.text") forState:UIControlStateNormal];
    
#ifdef _WITZM
    
#elif defined _CHACHA
    [_btnRequest setTitleColor:UIColorBasicBack forState:UIControlStateNormal];
#endif

    _btnRequest.layer.cornerRadius = 3;
    _btnRequest.clipsToBounds = YES;
    _btnRequest.layer.masksToBounds = YES;
    
    //------------------------------------------------------------------------------------
    return (view__.frame.origin.y + view__.frame.size.height);
}


//-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
//    CGPoint locationPoint = [[touches anyObject] locationInView:self.view];
//    CGPoint viewPoint = [_vOrderSelectView convertPoint:locationPoint fromView:self.view];
//    if ([self.mapView_ pointInside:viewPoint withEvent:event]) {
//        //
//        NSLog(@"touchesBegan");
//        [super touchesBegan: touches withEvent: event];
//    }
//}

//-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
//    if ([touches count] == 1) {
//        for (UITouch *touch in touches) {
//            
//            CGPoint point = [touch locationInView:[touch view]];
//            CALayer *alayer = [self.mapView_.layer hitTest:point];
//            if (alayer) NSLog(@"layer %@ touched",alayer.name);
//            else NSLog(@"layer not detected");
//            
//        } 
//    } 
//}

- (BOOL)touchesShouldCancelInContentView:(UIView *)view
{
    NSLog(@"touchesShouldCancelInContentView");
    
    if ([view isKindOfClass:[UIButton class]])
        return NO;
    else
        return YES;
}

#endif

#pragma mark - Update UI Draw
- (void)initUIDraw;
{
    _uiKey = [[NSMutableDictionary alloc] init];
}

- (void)resetUIDraw;
{
    [_uiKey removeAllObjects];
}

- (void)updateUIDraw:(NSString*)key update:(NSInteger)update;
{
    NSString *value = [NSString stringWithFormat:@"%ld",(long)update];
    if (![_uiKey valueForKey:key]) {
        [_uiKey setObject:value forKey:key];
    }
    else {
        [_uiKey setObject:value forKey:key];
    }
}

// 값이 없으면 request
/*
 tag
 0: ui초기화 시작
 1: ui초기화 끝 -> 1이면 api 요청 필요(2로 바뀐다)
 2: 데이터 요청했음
 4: 데이터 수정필요
 9: 데이터 처리됨(완료) -> api요청 가능(2로 바뀐다)
 
 view인 경우 tag는 state(3)state2(2)
 예 : 12010 120/10
 */

- (BOOL)getUIDraw:(NSString*)key;
{
    if (![_uiKey valueForKey:key]) {
        return YES;
    }
    else {
        NSString* value = [_uiKey valueForKey:key];
        if ([value isEqualToString:@"0"]) {
            return NO;
        }
    }
    return YES;
}

#pragma mark - Util
/*
//- (void)downloadImageWithURL:(NSString *)code property:(NSDictionary*)pMap completionBlock:(void (^)(BOOL succeeded, UIImage *image))completionBlock
{
    TXRequestObj *request     = [self->model createRequest:code];
    
    request.body = getJSONStr(pMap);
    
    NSMutableURLRequest *httpRequest = [request createHTTPRequest];
    DLogI(@"Sent Async Request to URL - %@\n\n", request.reqUrl);
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:httpRequest
                                            completionHandler:
                                  ^(NSData *data, NSURLResponse *response, NSError *error) {
                                      if ([response isKindOfClass:[NSHTTPURLResponse class]] &&
                                          ((NSHTTPURLResponse *)response).statusCode == 200 &&
                                          error == nil &&
                                          data != nil) {
                                          if ([data length]) {
                                              
                                              UIImage *image = [UIImage imageWithData:data];
                                              completionBlock(YES,image);
                                          }
                                          else {
                                              completionBlock(YES,[UIImage imageNamed:@"img_car_small"]);
                                          }
                                      } else {
                                          completionBlock(NO,nil);
                                      }
                                  }];
    [task resume];
    //
    [NSURLConnection sendAsynchronousRequest:httpRequest
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               if ( !error )
                               {
                                   if ([data length]) {
                                       
                                       UIImage *image = [UIImage imageWithData:data];
                                       completionBlock(YES,image);
                                   }
                                   else {
                                       completionBlock(YES,[UIImage imageNamed:@"img_car_small"]);
                                   }
                               } else{
                                   completionBlock(NO,nil);
                               }
                           }];
     //
}
*/

- (NSInteger) getStateTag {
    //    NSString *tag__ = [NSString stringWithFormat:@"%3d%2d",self->appDelegate.ustate,self->appDelegate.ustate2];
    //    return [tag__ integerValue];
    return 1;
}

#ifdef _DRIVER_MODE
-(void) setOverlayDriver {
    //[self cancelUpdateClock];
}
#else
-(void) setOverlay {
    //[self cancelUpdateClock];
    
    //[self onUpdateUI];
}
#endif


-(CGPoint)getCenterPointMapView:(CGRect)frame {
    CGPoint point = CGPointZero;
    point.x = frame.size.width/2;
    point.y = frame.size.height/2;
    point.x = point.x - kMarkerSizeW/2;
    point.y = point.y - kMarkerSizeH;
    
    return point;
}

// padding
-(void)updateMapPadding:(NSInteger)__state {
    static NSInteger oldstate = 0;
    
    if (oldstate == __state) {
        return;
    }
#ifdef _DRIVER_MODE
    if (__state == 200) {
        self.mapView_.padding = UIEdgeInsetsMake(_vToolBar.frame.origin.y + _vToolBar.frame.size.height + 10,
                                                 0,
                                                 0,
                                                 0);
    }
    else if (__state == 210) {
        self.mapView_.padding = UIEdgeInsetsMake(_vToolBar.frame.origin.y + _vToolBar.frame.size.height + 10,
                                                 0,
                                                 0,
                                                 0);
    }
    else if (__state == 230) {
        self.mapView_.padding = UIEdgeInsetsMake(_vAddress.frame.origin.y + _vAddress.frame.size.height + 10,
                                                 0,
                                                 _vCallChat.frame.size.height,
                                                 0);
    }
    else if (__state == 240) {
        self.mapView_.padding = UIEdgeInsetsMake(_vAddress.frame.origin.y + _vAddress.frame.size.height + 10,
                                                 0,
                                                 0,
                                                 0);
    }
#else
    if (__state == 110) {
        self.mapView_.padding = UIEdgeInsetsMake(kTopHeight + _vTopMainNavi.frame.size.height + 10,
                                                 0,
                                                 _vAddress.frame.size.height + 10,
                                                 0);
    }
    else if (__state == 111) {
        self.mapView_.padding = UIEdgeInsetsMake(kTopHeight + _vTopMainNavi.frame.size.height + 10 + _vAddress.frame.size.height + 10,
                                                 0,
                                                 _vOrder.frame.origin.y,
                                                 0);
    }
    else if (__state == 130) {
        self.mapView_.padding = UIEdgeInsetsMake(kTopHeight + _vTopRiderNavi.frame.size.height + 10 + _vAddress.frame.size.height + 10,
                                                 0,
                                                 _vDriveStatus.frame.size.height + _vCallChat.frame.size.height,
                                                 0);
    }
    else if (__state == 140) {
        self.mapView_.padding = UIEdgeInsetsMake(kTopHeight + _vTopRiderNavi.frame.size.height + 10 + _vAddress.frame.size.height + 10,
                                                 0,
                                                 _vDriveStatus.frame.size.height + _vCallChat.frame.size.height,
                                                 0);
    }
#endif
    
//    NSLog(@"self.mapView_.padding:%@,%@,%@,%@,",self.mapView_.padding.up,self.mapView_.padding.left,self.mapView_.padding.right,self.mapView_.padding.bottom);
    
//    NSLog(@"self.mapView_.projection:%@",self.mapView_.projection.visibleRegion);
    oldstate = __state;
}

-(void)showMarker:(GMSMarker*)marker__ position:(NSString*)postion__
{
    NSArray* loc = [[Utils nullTolocation:postion__] componentsSeparatedByString:@","];
    
    // 위치
    CLLocationCoordinate2D position;
    position.latitude  = [Utils formattedPositionCoor:[[loc objectAtIndex:0] doubleValue]];
    position.longitude = [Utils formattedPositionCoor:[[loc objectAtIndex:1] doubleValue]];
    
    // puls
    //self->pickupMarker = [GMSMarker markerWithPosition:position];
    //[locMgr clearMarker:marker__];

#ifndef _DRIVER_MODE
#ifdef _MAP_TOUCH_MOVE
    // 110인 경우 start marker를 보이지 않게 한다. imagepin을 사용하기 위해
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    if ([marker__ isEqual:locMgr.markerStart] &&
        !self->appDelegate.isDriverMode && self->appDelegate.ustate <= 110 &&
        ([_srcLocationTextField.text isEqualToString:@""] || [_dstLocationTextField.text isEqualToString:@""])) {
        [locMgr clearMarker:marker__];
        
        [self showFocusPin];
        return;
    }
    else {
        [self hideFocusPin];
    }
#endif
#endif
    marker__.position = position;
    marker__.map  = self.mapView_;
}

-(void)showMarker:(GMSMarker*)marker__ position:(NSString*)postion__ icon:(UIImage*)icon__
{
    NSArray* loc = [[Utils nullTolocation:postion__] componentsSeparatedByString:@","];
    
    // 위치
    CLLocationCoordinate2D position;
    position.latitude  = [Utils formattedPositionCoor:[[loc objectAtIndex:0] doubleValue]];
    position.longitude = [Utils formattedPositionCoor:[[loc objectAtIndex:1] doubleValue]];
    
    // puls
    //self->pickupMarker = [GMSMarker markerWithPosition:position];
    
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    [locMgr clearMarker:marker__];
#if defined (_MAP_TOUCH_MOVE) && ! defined (_DRIVER_MODE)
    // 110인 경우 start marker를 보이지 않게 한다. imagepin을 사용하기 위해
    if ([marker__ isEqual:locMgr.markerStart] &&
        !self->appDelegate.isDriverMode && self->appDelegate.ustate <= 110 &&
        ([_srcLocationTextField.text isEqualToString:@""] || [_dstLocationTextField.text isEqualToString:@""])) {
        [self showFocusPin];
        return;
    }
    else {
        [self hideFocusPin];
    }
#endif

    marker__.position = position;
    marker__.icon = icon__;
    marker__.map  = self.mapView_;
}

-(void)hideMarker {
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    [locMgr clearMarker:locMgr.markerStart];
    [locMgr clearMarker:locMgr.markerFinish];
    [locMgr clearMarker:locMgr.markerDrive];
}


-(void)hideMarker:(GMSMarker*)marker__ {
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    [locMgr clearMarker:marker__];
}


- (void)reloadSelectedVehicleInfo
{
    if (self->appDelegate.isDriverMode && [[Utils nullToString:selectedVechicleInfo] isEqualToString:@""] && self->appDelegate.pstate > 200) {
        
        selectedVechicleInfo = [self getVehicleInfo];
        
    }
}

//- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
//    NSLog(@"MissCheckScrollView hitTest");
//    
//    UIView *result = nil;
//    for (UIView *child in self.subviews) {
//        if ([child isKindOfClass:[MissCheckView class]]) {
//            // 이것은 contentView를 찾기 위해서 인데, 다른 조건을 넣어도 무방하다. (pointInside 등)
//            if ((result = [child hitTest:point withEvent:event]) != nil)
//                break;
//        }
//    }
//    if (![self pointInside:point withEvent:event]) return nil;
//    // 이 스크롤 뷰에 point가 있는게 아니라면 nil 리턴. -> 요부분을 하지 않으면 스크롤 뷰 외의 다른 버튼들이 동작하지 않는다.
//    if (result == nil) return self;
//    // 하위 뷰 중 해당하는 뷰를 찾지 못했다면 자기 자신을 리턴 -> 요부분을 하지 않으면 스크롤이 동작하지 않는다.
//    return result;       // 조건에 맞는 하위 뷰를 찾았다면 리턴
//}


#pragma mark - Background Callback
-(void) playTTS:(NSString*)message
{
    [self playTTS:message vol:0.5f];
}

-(void) playTTS:(NSString*)message vol:(float)vol
{
    AVSpeechSynthesizer *synthesizer = [[AVSpeechSynthesizer alloc]init];
    AVSpeechUtterance *utterance = [AVSpeechUtterance speechUtteranceWithString:message];
    [utterance setRate:AVSpeechUtteranceDefaultSpeechRate];
    [utterance setVolume:vol];
    //[utterance setVolume:0.5f];
    //[utterance setPitchMultiplier:0.25];
    [synthesizer speakUtterance:utterance];
}

-(void)driverTTS
{
    [self driverTTS:0.5f];
}

-(void)driverTTS:(float)vol
{
    if (self->appDelegate.isDriverMode && self->appDelegate.pstate == 210) {
        if (ttspstate != self->appDelegate.pstate) {
            ttspstate = self->appDelegate.pstate;
        }
        else {
            return;
        }
        
        [self playTTS:LocalizedStr(@"TTS.210.0.text") vol:vol];
        return;
    }
    //    else if (self->appDelegate.isDriverMode && self->appDelegate.pstate == 220 && self->appDelegate.pstate2 == 10) {
    //        [self playTTS:LocalizedStr(@"TTS.220.10.text")];
    //        return;
    //    }
    //    else if (self->appDelegate.isDriverMode && self->appDelegate.pstate == 220 && self->appDelegate.pstate2 == 20) {
    //        [self playTTS:LocalizedStr(@"TTS.220.20.text")];
    //        return;
    //    }
    else if (self->appDelegate.isDriverMode && self->appDelegate.pstate == 230 && self->appDelegate.pstate2 == 10) {
        if (ttspstate != self->appDelegate.pstate && ttspstate2 != self->appDelegate.pstate2) {
            ttspstate  = self->appDelegate.pstate;
            ttspstate2 = self->appDelegate.pstate2;
        }
        else {
            return;
        }
        
        [self playTTS:LocalizedStr(@"TTS.230.10.text") vol:vol];
        return;
    }
    else if (self->appDelegate.isDriverMode && self->appDelegate.pstate == 230 && self->appDelegate.pstate2 == 20) {
        if (ttspstate != self->appDelegate.pstate && ttspstate2 != self->appDelegate.pstate2) {
            ttspstate  = self->appDelegate.pstate;
            ttspstate2 = self->appDelegate.pstate2;
        }
        else {
            return;
        }
        
        [self playTTS:LocalizedStr(@"TTS.230.20.text") vol:vol];
        return;
    }
    else if (self->appDelegate.isDriverMode && self->appDelegate.pstate == 240 && self->appDelegate.pstate2 == 10) {
        if (ttspstate != self->appDelegate.pstate && ttspstate2 != self->appDelegate.pstate2) {
            ttspstate  = self->appDelegate.pstate;
            ttspstate2 = self->appDelegate.pstate2;
        }
        else {
            return;
        }
        
        [self playTTS:LocalizedStr(@"TTS.240.10.text") vol:vol];
        return;
    }
    else if (self->appDelegate.isDriverMode && self->appDelegate.pstate == 240 && self->appDelegate.pstate2 == 20) {
        if (ttspstate != self->appDelegate.pstate && ttspstate2 != self->appDelegate.pstate2) {
            ttspstate  = self->appDelegate.pstate;
            ttspstate2 = self->appDelegate.pstate2;
        }
        else {
            return;
        }
        
        [self playTTS:LocalizedStr(@"TTS.240.20.text") vol:vol];
        return;
    }
}

#pragma mark - Background Callback
-(void) backgroundCallback:(id)info
{
    //    NSLog(@"########");
    //    NSLog(@"###### BG TASK RUNNING");
    
    NSDate *chkTime = [NSDate date];
    
    NSTimeInterval executionTime = [chkTime timeIntervalSinceDate:bgTime]/60*60; //초
    NSLog(@"executionTime = %f", executionTime);
    if (executionTime >= DRIVER_READY_TIMEOUT) {
        // 10분을 초과하면 차량해제
        if (self->appDelegate.isDriverMode && self->appDelegate.pstate == 210) {
            [self applicationWillTerminate];
            
            //            [self onPP135]; // 차량해제
            //            [bgTask stopBackgroundTask];
            //
            //            [self playTTS:@""];
            return;
        }
    }
    
    if (executionTime < 10) {
        if (executionTime >= 4 && executionTime <= 5) {
            [self driverTTS];
        }
    }
    else if (executionTime >= 10) {
        if (((int)executionTime % 30) == 0) {
            //[self driverTTS];
        }
    }
}

#pragma mark -

#ifdef _DRIVER_MODE

-(void)animationFade:(UIView*)view__
{
    //fade in/out
    [UIView animateWithDuration:1.f delay:0.f options:UIViewAnimationOptionCurveEaseIn animations:^{
        [view__ setAlpha:1.f];
    } completion:^(BOOL finished) {
        if ([view__ isEqual:_vAddress] && !isAniAddress) {
            [view__ setAlpha:1.0f];
            return;
        }
        else if ([view__ isEqual:_vTestMode] && !isAniTestMode) {
            [view__ setAlpha:1.0f];
            return;
        }

        [UIView animateWithDuration:0.9f delay:0.1f options:UIViewAnimationOptionCurveEaseInOut animations:^{
            [view__ setAlpha:0.2f];
        } completion:^(BOOL finished) {
            if ([view__ isEqual:_vAddress] && !isAniAddress) {
                [view__ setAlpha:1.0f];
                return;
            }
            else if ([view__ isEqual:_vTestMode] && !isAniTestMode) {
                [view__ setAlpha:1.0f];
                return;
            }
            [self performSelector:@selector(animationFade:) withObject:view__ afterDelay:0.01];
        }];

    }];
}

#endif

#ifdef _DRIVER_MODE
#pragma mark -
#pragma mark Slider

-(IBAction)fadeLabel {
    
    if (self->appDelegate.pstate == 230 && self->appDelegate.pstate2 == 10) { //1
        self.lbSlider.alpha = 1.0 - self.slideToUnlock.value;
    }
    else if (self->appDelegate.pstate == 230 && self->appDelegate.pstate2 == 20) {//0
        self.lbSlider.alpha = self.slideToUnlock.value;
    }
    else if (self->appDelegate.pstate == 240 && self->appDelegate.pstate2 == 10) {//1
        self.lbSlider.alpha = 1.0 - self.slideToUnlock.value;
    }
    
}

-(IBAction)UnLockIt {
    
    if (self->appDelegate.pstate == 200) {
        if (self.naviSlideToUnlock.value ==1.0) {  // if user slide far enough, stop the operation
            [self blockDriverSlider];
            
            // Put here what happens when it is unlocked
            if (self->appDelegate.pstate < 200) {
                [self didDone210];
            }
            else if (self->appDelegate.pstate == 200) {
                [self didDone210];
            }
        } else {
            [self releaseLockIt];
        }
    }
    
    if (self->appDelegate.pstate == 230 && self->appDelegate.pstate2 == 10) {
        if (self.slideToUnlock.value ==1.0) {  // if user slide far enough, stop the operation
            [self blockDriverSlider];
            
            // pd213
            [self onPD213];
        }
        else {
            [self releaseLockIt];
        }
    }
    else if (self->appDelegate.pstate == 230 && self->appDelegate.pstate2 == 20) {
        if (self.slideToUnlock.value ==0.0) {  // if user slide far enough, stop the operation
            [self blockDriverSlider];
            
            // 목적지가 없으면 alert
            NSDictionary* pdevice = [self->appDelegate.dicDriver objectForKey:@"drive_pickup"];
            if ([[Utils nullToString:pdevice[@"ldst"]] isEqualToString:@""]) {
                [self releaseLockIt];
                
                __block LGAlertView *alertView = [[LGAlertView alloc] initWithTitle:nil
                                                                            message:LocalizedStr(@"Map.Drive.DstAddress.Fail.text")
                                                                              style:LGAlertViewStyleAlert
                                                                       buttonTitles:@[LocalizedStr(@"Button.OK")]
                                                                  cancelButtonTitle:nil
                                                             destructiveButtonTitle:nil
                                                                      actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                                                                      }
                                                                      cancelHandler:^(LGAlertView *alertView) {
                                                                      }
                                                                 destructiveHandler:^(LGAlertView *alertView) {
                                                                 }];
                [Utils initAlertButtonColor:alertView];
                [alertView showAnimated:YES completionHandler:^(void)
                 {
                 }];
                return;
            }
            // pd320
            [self onPD320];
        }
        else {
            [self releaseLockIt];
        }
    }
    else if (self->appDelegate.pstate == 240 && self->appDelegate.pstate2 == 10) {
        if (self.slideToUnlock.value ==1.0) {  // if user slide far enough, stop the operation
            [self blockDriverSlider];
            
            if (![self onPD313]) {
                [self releaseLockIt];
            }
        }
        else {
            [self releaseLockIt];
        }
    }
}

-(void)releaseLockIt
{
    if (self->appDelegate.pstate <= 220) {
        // user did not slide far enough, so return back to 0 position
        [UIView beginAnimations: @"SlideCanceled" context: nil];
        [UIView setAnimationDelegate: self];
        [UIView setAnimationDuration: 0.25];
        // use CurveEaseOut to create "spring" effect
        [UIView setAnimationCurve: UIViewAnimationCurveEaseOut];
        
        self.naviSlideToUnlock.enabled = YES;
        [self.naviSlideToUnlock setValue:0 animated:YES];
        self.naviSlideToUnlock.alpha = 1.0;
        [UIView commitAnimations];
        
        return;
    }
    else {
        // user did not slide far enough, so return back to 0 position
        [UIView beginAnimations: @"SlideCanceled" context: nil];
        [UIView setAnimationDelegate: self];
        [UIView setAnimationDuration: 0.25];
        // use CurveEaseOut to create "spring" effect
        [UIView setAnimationCurve: UIViewAnimationCurveEaseOut];
        
        self.slideToUnlock.enabled = YES;
        
        if (self->appDelegate.pstate == 230 && self->appDelegate.pstate2 == 10) { //1
            [self.slideToUnlock setValue:0 animated:YES];
        }
        else if (self->appDelegate.pstate == 230 && self->appDelegate.pstate2 == 20) {//0
            [self.slideToUnlock setValue:1 animated:YES];
        }
        else if (self->appDelegate.pstate == 240 && self->appDelegate.pstate2 == 10) {//1
            [self.slideToUnlock setValue:0 animated:YES];
        }
        
        self.lbSlider.alpha = 1.0;
        [UIView commitAnimations];
        
        [self fadeLabel];
    }
}

-(void)blockDriverSlider {
    if (self->appDelegate.pstate == 200) {
        self.naviSlideToUnlock.enabled = NO;
    }
    else {
        self.slideToUnlock.enabled = NO;
    }
}

-(void)sliderViewUp:(BOOL)flag
{
    CGRect rect = self.vSlider.frame;
    if (flag) {
        rect.origin.y = sliderUpRect;
    }
    else {
        rect.origin.y = sliderDownRect;
    }
    self.vSlider.frame = rect;
}

-(void)delayShowSlider {
    
    if (self.vSlider.hidden) {
        return;
    }
    [UIView animateWithDuration:0.2
                     animations:^{
                         self.vSlider.alpha = 1.0;
                     } ];
}

#endif

//#pragma mark - AddressView
//-(void)routeImageDidLoad {
//    
//    [self hideFocusPin];
//    
//}

#pragma mark - Destination Delegate
- (void) didFoundAddress:(NSString *) address
            withLatitute:(double)lat
           andLongtitute:(double)longi
               forTarget:(id)_target
{
    
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    //Move map to address
    //[self goToLocationWithLatitute:lat andLongtitute:longi];
    
    // 출발지와 목적지의 위치가 각각 동일해도 무시한다.
    //    if ([Utils pointCompare:lsrc.latitude comp:lat] && [Utils pointCompare:lsrc.longitude comp:longi]) {
    //        self.mapView_.camera = [GMSCameraPosition cameraWithLatitude:lat longitude:longi zoom:16];
    //        return;
    //    }
    //    if ([Utils pointCompare:ldst.latitude comp:lat] && [Utils pointCompare:ldst.longitude comp:longi]) {
    //        return;
    //    }
    
    UITextField *textfield;
#ifdef _DRIVER_MODE
    textfield = (UITextField*)_target;
#else
    // 출발지 재 조정결과
    if (_target == nil) {
        textfield = _srcLocationTextField;
    }
    else {
        textfield = (UITextField*)_target;
    }
#endif
    
    NSLog(@"didFoundAddress:%@,%f,%f,%ld",address,lat,longi,(long)textfield.tag);
    //self.txt_address.text = address.attributedFullText.string;
    
    CLLocationCoordinate2D __spos;
    CLLocationCoordinate2D __dpos;
#ifdef _DRIVER_MODE
    if ([textfield isEqual:_dstLocationTextField] && [address isEqualToString:@""]) {
        return;
    }
    //
    __dpos.latitude  = [Utils formattedPositionCoor:lat];
    __dpos.longitude = [Utils formattedPositionCoor:longi];
    
    __spos = lsrc;
    
    // 도착지 설정
    ldst = __dpos;
    // 200 상태는 드라이버가 가고자하는 지역을 설정하는 것
    if (self->appDelegate.pstate == 200) {
        textfield.text = address;
        return;
    }
    
    _dstLocationTextField.tag = 110;
    
    [self hideFocusPin];
    
    [Utils updateDstPosition:LOCATION_STR(lat) lng:LOCATION_STR(longi)];
    
    // 도착점
    //if (![Utils isEqualLocation:locMgr.markerFinish.position location:ldst]) {
//    [locMgr clearMarker:locMgr.markerFinish];
//    dispatch_async(dispatch_get_main_queue(), ^{
//        [locMgr updateFinishMarker:ldst];
//    });
    //}
    
    // 주소변경
    if (self->appDelegate.pstate == 240) {
        [self onPD310];
    }
    
    textfield.text = address;
    //[self routeImageDidLoad];
    
#else
    BOOL isSrcLocationChanged = NO;
    if ([textfield isEqual:_srcLocationTextField]) {
        //
        __spos.latitude  = [Utils formattedPositionCoor:lat];
        __spos.longitude = [Utils formattedPositionCoor:longi];
        
        if (![Utils isEqualLocation:lsrc location:__spos]) {
            isSrcLocationChanged = YES;
        }
        
        __dpos = ldst;
    }
    else {
        //
        __dpos.latitude  = [Utils formattedPositionCoor:lat];
        __dpos.longitude = [Utils formattedPositionCoor:longi];
        
        __spos = lsrc;
    }
    /*
    if ([textfield isEqual:_dstLocationTextField] && ![textfield.text isEqualToString:@""]) {
        [self clearDsetAddressTextField];
    }
    */
    // 출발지 설정
    if ([textfield isEqual:_srcLocationTextField]) {
        lsrc = __spos;
        _srcLocationTextField.tag = 109;
        
        [Utils updateSrcPosition:LOCATION_STR(lsrc.latitude) lng:LOCATION_STR(lsrc.longitude)];
        
        self.mapView_.camera = [GMSCameraPosition cameraWithLatitude:lat longitude:longi zoom:[self getZoomByState]];
        
        // picup marker 220이상은 서버 결과에 따라서 마커를 찍는다.
        if (self->appDelegate.ustate <= 110) {
            if (![Utils isEqualLocation:locMgr.markerStart.position location:lsrc]) {
                [locMgr clearMarker:locMgr.markerStart];
#ifdef _MAP_TOUCH_MOVE
                // 110인 경우 start marker를 보이지 않게 한다. imagepin을 사용하기 위해
                if ([_srcLocationTextField.text isEqualToString:@""] || [_dstLocationTextField.text isEqualToString:@""]) {
                    
                }
                else {
                    [self hideFocusPin];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        locMgr.markerStart.position = lsrc;
                        if (locMgr.markerStart.map == nil) {
                            locMgr.markerStart.map = self.mapView_;
                        }
                        [self pushCpos:nil];
                    });
                }
#else
                dispatch_async(dispatch_get_main_queue(), ^{
                    locMgr.markerStart.position = lsrc;
                    if (locMgr.markerStart.map == nil) {
                        locMgr.markerStart.map = self.mapView_;
                    }
                    [self pushCpos:nil];
                });
#endif
            }
        }
        
        
        // 주소변경
        if (self->appDelegate.isDriverMode) {
            if (self->appDelegate.pstate == 240) {
                //                [self onPD351];
            }
        }
        else {
            if(self->appDelegate.ustate == 120) {
                [self onUD110];
            }
            else if(self->appDelegate.ustate == 130) {
                [self onUD210];
            }
            else if(self->appDelegate.ustate == 140) {
                //                [self onUD310];
            }
        }
    }
    // 도착지 설정
    else if ([textfield isEqual:_dstLocationTextField]) {
        ldst = __dpos;
        
        _dstLocationTextField.tag = 110;
        //_btnAddressClear.hidden = NO;
        [self hideFocusPin];
        
        [Utils updateDstPosition:LOCATION_STR(lat) lng:LOCATION_STR(longi)];
        
        if (!self->appDelegate.isDriverMode && self->appDelegate.ustate == 140 && self->appDelegate.ustate2 == 10) {
            [self updateVehicleFitBounds];
        }
        else {
            if (self->appDelegate.isDriverMode && self->appDelegate.pstate == 240) {
                //
            }
            else {
                [self pushCpos:nil];
                //self.mapView_.camera = [GMSCameraPosition cameraWithLatitude:lat longitude:longi zoom:16];
                //[self performSelector:@selector(pushCpos) withObject:nil afterDelay:3];
            }
        }
        
        // picup marker 220이상은 서버 결과에 따라서 마커를 찍는다.
        if (self->appDelegate.ustate <= 110) {
            if (![Utils isEqualLocation:locMgr.markerStart.position location:lsrc]) {
                [locMgr clearMarker:locMgr.markerStart];
#ifdef _MAP_TOUCH_MOVE
                // 110인 경우 start marker를 보이지 않게 한다. imagepin을 사용하기 위해
                if ([_srcLocationTextField.text isEqualToString:@""] || [_dstLocationTextField.text isEqualToString:@""]) {
                    
                }
                else {
                    [self hideFocusPin];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        locMgr.markerStart.position = lsrc;
                        if (locMgr.markerStart.map == nil) {
                            locMgr.markerStart.map = self.mapView_;
                        }
                        [self pushCpos:nil];
                    });
                }
#else
                dispatch_async(dispatch_get_main_queue(), ^{
                    locMgr.markerStart.position = lsrc;
                    if (locMgr.markerStart.map == nil) {
                        locMgr.markerStart.map = self.mapView_;
                    }
                    [self pushCpos:nil];
                });
#endif
            }
        }
        
        // 도착점
        // picup marker 220이상은 서버 결과에 따라서 마커를 찍는다.
        //if (self->appDelegate.pstate < 220) {
            //if (![Utils isEqualLocation:locMgr.markerFinish.position location:ldst]) {
            [locMgr clearMarker:locMgr.markerFinish];
            dispatch_async(dispatch_get_main_queue(), ^{
                [locMgr updateFinishMarker:ldst];
//                locMgr.markerFinish.position = ldst;
//                if (locMgr.markerFinish.map == nil) {
//                    locMgr.markerFinish.map = self.mapView_;
//                }
            });
            //}
        //}
        
        
        // 주소변경
        if (self->appDelegate.isDriverMode) {
            if (self->appDelegate.pstate == 240) {
                [self onPD310];
            }
        }
        else {
            if(self->appDelegate.ustate == 110) {
                
#ifdef _WITZM
                // favorite hide
                _vFavoriteHome.hidden = YES;
                _vFavoriteWork.hidden = YES;
                _vFavoriteLast.hidden = YES;
                
                //[self hideView:_lbFavoriteLast];
                [self hideView:_lbFavoriteSetting];
#elif defined _CHACHA
                // favorite hide
                _vFavoriteHome.hidden = YES;
                _vFavoriteWork.hidden = YES;
                _vFavoriteLast.hidden = YES;
                
                //[self hideView:_lbFavoriteLast];
                [self hideView:_lbFavoriteSetting];
#endif

            }
            else if(self->appDelegate.ustate == 120) {
                [self onUD110];
            }
            else if(self->appDelegate.ustate == 130) {
                [self onUD210];
            }
            else if(self->appDelegate.ustate == 140) {
                [self onUD310];
            }
        }
        
        //[self showVehicleView];
    }
    
    textfield.text = address;
    //[self routeImageDidLoad];
    
    
    // 110 상태에서 출/도착지가 있으면 UI가 바뀐다.
    if(!self->appDelegate.isDriverMode &&
       (self->appDelegate.ustate == 110 || self->appDelegate.ustate == 100) &&
       ![_srcLocationTextField.text isEqualToString:@""] &&
       ![_dstLocationTextField.text isEqualToString:@""]
       ) {
        // 출발지 재 조정결과는 라인을 다시 그리지 않는다.
        if (_target == nil) {
            if (isSrcLocationChanged) {
                [self drawRoute];
                //[self drawEstimateLineRider];
            }
            [self onRequestRiderStep:nil];
        }
        else {
            [self drawEstimateLineRider];
        }
    }
    
#endif
    
    // 출도착이 같아도 무시한다.
    //    if ([Utils isEqualLocation:__spos location:__dpos]) {
    //        return;
    //    }
    
}

#ifndef _DRIVER_MODE
- (void)drawEstimateLineRider {
    // 110 상태에서 출/도착지가 있으면 UI가 바뀐다.
    NSString* no = [self getAvailableCardNo];
    if ([no isEqualToString:@""]) {
        no = @"-";
    }
    _lbCardInfo.text = no;
    
    //drawLineTime = [[NSDate date] dateByAddingTimeInterval:-7];
    
    [Utils setStateUIInfo:UI_STATE_TRIP_LINE value:UI_STATE_STEP_WAIT oldvalue:nil];
    self->appDelegate.ustate2 = -9;
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    dic = [estimate_result mutableCopy];
    dic[@"drive_estimates"] = [[NSArray alloc] initWithObjects:@"", nil];
    estimate_result = dic;
    [self updateUIServiceType];
    //[self updteOrderSelect];
    [self updtePriceOrderSelect];
    [self onUpdateUI];
    
    // ETA
    _lbTopTitle.text = [NSString stringWithFormat:@"%@ ...", LocalizedStr(@"Map.Drive.Pickup.ETA.text")];
    
    // route 그리기 : 차량요청
    if (![self drawRoute]) {
        [self onETAPrint];
    }
}
#endif

// 목적지 변경
-(void)updateRealStartEndAddress:(BOOL)flag__
{
    BOOL __flag = NO;
    if (self->appDelegate.isDriverMode) {
        if (self->appDelegate.pstate == 240) {
            __flag = YES;
        }
    }
    else {
        if(self->appDelegate.ustate <= 110) {
            __flag = YES;
        }
        else if(self->appDelegate.ustate == 120) {
            __flag = YES;
        }
        else if(self->appDelegate.ustate == 130) {
            __flag = YES;
        }
        else if(self->appDelegate.ustate == 140) {
            __flag = YES;
        }
    }
    
    if (!__flag) {
        return;
    }
    
    // 주소변경이 완료되지 않았다면 내부 정보를 변경한다.
    if (!flag__) {
        INTULocationManager *locMgr = [INTULocationManager sharedInstance];
        lsrc = locMgr.markerStart.position;
        
        [Utils updateSrcPosition:LOCATION_STR(locMgr.markerStart.position.latitude) lng:LOCATION_STR(locMgr.markerStart.position.longitude)];
        
        ldst = locMgr.markerFinish.position;
        
        [Utils updateDstPosition:LOCATION_STR(locMgr.markerFinish.position.latitude) lng:LOCATION_STR(locMgr.markerFinish.position.longitude)];
    }
    
    // 변경된 주소로 marker를 다시 찍고
    [self initStartEndPosition];
    
    // route 다시 그리기
    if (self->appDelegate.pstate == 240 && self->appDelegate.pstate2 == 10) {
        [self drawRoute:@"line=Y"];
    }
    else {
        if (!self->appDelegate.isDriverMode && self->appDelegate.ustate == 110) {
            [self drawRoute:@"line=Y"];
        }
    }
}

- (void) resetVehicleInfo
{
    selectedVechicleInfo = @"";
    [[[TXApp instance] getSettings] setFDKeychain:@"selectedVechicleInfo" value:selectedVechicleInfo];
}

- (NSString*) getVehicleInfo
{
    selectedVechicleInfo = [[[TXApp instance] getSettings] getFDKeychain:@"selectedVechicleInfo"];
    return selectedVechicleInfo;
}

- (void) setVehicleInfo:(NSDictionary*)dic
{
    selectedVechicleInfo = [NSString stringWithFormat:@"%@ %@ %@",
                            [Utils nullToString:dic[@"service_name"]],
                            [Utils nullToString:dic[@"vehicleno"]],
                            [Utils nullToString:dic[@"vehiclemodel"]]
                            ];
    
    [[[TXApp instance] getSettings] setFDKeychain:@"selectedVechicleInfo" value:selectedVechicleInfo];
}

- (void) didDone2:(NSInteger)_tag forTarget:(id)_target
{
    NSLog(@"didDone2:%ld",(long)_tag);
    
    [self cancelAllLoop];
    [self cancelTimeout];
    
    [self onPD035];
}

- (void) didDone210
{
#ifdef _DRIVER_MODE
    @try {
        NSArray *provider_vehicle = [self->appDelegate.dicDriver objectForKey:@"provider_vehicles"];
        
        if (![Utils isArray:provider_vehicle]) {
            provider_vehicle = vehicleList;
        }
        
        if (self->appDelegate.isDriverMode && self->appDelegate.isBecomeMode) {
            [self alertError:nil message:LocalizedStr(@"String.Driver.Permit.None.text")];
            [self releaseLockIt];
            return;
        }
        
        if (![Utils isArray:provider_vehicle]) {
            [self alertError:nil message:LocalizedStr(@"Driver.Vechile.Select.alert.NoVehicle.text")]; //__i18n
            [self releaseLockIt];
            return;
        }
        
        [self getVehicleCount];
        /*
        if ([dataArray count]) {
            [dataArray removeAllObjects];
        }
        
        for (NSDictionary* key in provider_vehicle) {
            if ([key[@"state"] intValue] == 1) {
                NSString *vehicle = [NSString stringWithFormat:@"%@ / %@ / %@",
                                     [Utils nullToString:key[@"service_name"]],
                                     [Utils nullToString:key[@"vehicleno"]],
                                     [Utils nullToString:key[@"vehiclemodel"]]
                                     ];
                [dataArray addObject:vehicle];
            }
        }
        */
        if (![dataArray count]) {
            [self alertError:nil message:LocalizedStr(@"Driver.Vechile.Select.alert.Wait.text")]; //__i18n
            [self releaseLockIt];
            return;
        }
        
        UIView *blockView = [[UIView alloc] initWithFrame:CGRectMake(0, kTopHeight, self.mapView_.bounds.size.width, self.mapView_.bounds.size.height-kTopHeight)];
        blockView.tag = 90;
        blockView.backgroundColor = HEXCOLOR(0x00000000);
        [self.mapView_ addSubview:blockView];
        if (!_vVehicleSelectView.subviews.count) {
            [self initVehicleSelect];
        }
        [_vVehicle bringSubviewToFront:blockView];
        
        [UIView animateWithDuration:0.5
                         animations:^{
                             CGRect frame = _vVehicle.frame;
                             frame.origin.y = self.mapView_.bounds.size.height - frame.size.height;
                             _vVehicle.frame = frame;
                             
                             blockView.backgroundColor = HEXCOLOR(0x000000CC);
                         }
                         completion:^(BOOL finished) {
                         }];
        return;
        
        // 차량 선택화면 show
        UITableView* mTableView = [[UITableView alloc] init];
        mTableView.delegate=self;
        mTableView.dataSource=self;
        
        mTableView.allowsMultipleSelection = NO;
        if ([dataArray count] == 1) {
            mTableView.frame = CGRectMake(0.f, 0.f, 320, 42.f);
        }
        else {
            mTableView.frame = CGRectMake(0.f, 0.f, 320, 84.f);
        }
        mTableView.contentMode = UIViewContentModeScaleAspectFit;
        
        if ([dataArray count] == 1) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
            
            if (indexPath != nil) {
                [mTableView selectRowAtIndexPath:indexPath
                                        animated:YES
                                  scrollPosition:UITableViewScrollPositionNone];
                //[mTableView tableView:mTableView didSelectRowAtIndexPath:indexPath];
                [mTableView.delegate tableView:mTableView didSelectRowAtIndexPath:indexPath];
            }
        }
        //            NSIndexPath *indexPath = nil;
        
        LGAlertView *alertView = [[LGAlertView alloc] initWithViewAndTitle:nil
                                                                   message:LocalizedStr(@"Driver.Vechile.Select.alert.text")
                                                                     style:LGAlertViewStyleAlert
                                                                      view:mTableView
                                                              buttonTitles:@[LocalizedStr(@"Button.OK")]
                                                         cancelButtonTitle:LocalizedStr(@"Button.CANCEL")
                                                    destructiveButtonTitle:nil
                                                             actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                                                                 NSLog(@"actionHandler, %@, %lu", title, (long unsigned)index);
                                                                 
                                                                 BOOL flag = NO;
                                                                 for(NSIndexPath* indexPath in [mTableView indexPathsForSelectedRows]) {
                                                                     NSLog(@"cell.tag:%@",[dataArray objectAtIndex:indexPath.row]);
                                                                     
                                                                     NSDictionary* key = [provider_vehicle objectAtIndex:indexPath.row];
                                                                     
                                                                     [[[TXApp instance] getSettings] setFDKeychain:SettingsConst.CryptoKeys.VSEQ value:[key valueForKey:@"vseq"]];
                                                                     
                                                                     [self setVehicleInfo:key];
                                                                     
                                                                     // 시험운행이면 alert를 띄운다.
                                                                     if ([[Utils nullToString:[[[TXApp instance] getSettings] getDriveMode]] isEqualToString:DRIVE_MODE_TRAINING]) {
                                                                         [Utils onErrorAlertViewTitle:LocalizedStr(@"Menu.Testdrive.request.title") title:LocalizedStr(@"String.Alert") timeout:5];
                                                                     }
                                                                     
                                                                     
                                                                     [self onPD015];
                                                                     flag = YES;
                                                                 }
                                                                 
                                                                 [mTableView reloadData];
                                                                 [mTableView setContentOffset:CGPointZero animated:YES];
                                                                 //mTableView.indexPathsForSelectedRows = nil;
                                                                 
                                                                 if (!flag) {
                                                                     [self releaseLockIt];
                                                                 }
                                                                 
                                                             }
                                                             cancelHandler:^(LGAlertView *alertView) {
                                                                 NSLog(@"cancelHandler");
                                                                 
                                                                 [self releaseLockIt];
                                                             }
                                                        destructiveHandler:^(LGAlertView *alertView) {
                                                            NSLog(@"destructiveHandler");
                                                        }];
        //alertView.heightMax = 256.f;
        [Utils initAlertButtonColor:alertView];
        alertView.windowLevel = LGAlertViewWindowLevelAboveStatusBar;
        [alertView showAnimated:YES completionHandler:nil];
    }
    @catch (NSException *exception) {
        NSLog(@"Exception: %@, %@", exception.name, exception.reason);
        [self alertError:nil message:LocalizedStr(@"Driver.Vechile.Select.alert.NoVehicle.text")]; //__i18n
        [self releaseLockIt];
    }
#endif
}


#ifdef _DRIVER_MODE

#if defined _CHACHA

#pragma mark - Driver ChaCha
- (void) sideMenuSelected:(NSInteger) tag
{
    if (tag == 0) {
        if (self->appDelegate.isBecomeMode) {
            // alert
            //[self alertError:LocalizedStr(@"String.Alert") message:LocalizedStr(@"String.Driver.Permit.None.text")];
            [self loadBecomeADriver];
            return;
        }
        
        TXProfileAccountVC *mvc = [[TXProfileAccountVC alloc] initWithNibName:@"TXProfileAccountVC" bundle:nil];
        [self pushViewController:mvc];
    }
    else if (tag == 1) {
        if (self->appDelegate.isBecomeMode) {
            // alert
            //[self alertError:LocalizedStr(@"String.Alert") message:LocalizedStr(@"String.Driver.Permit.None.text")];
            [self loadBecomeADriver];
            return;
        }
        
        TXVehicleVC *mvc = [[TXVehicleVC alloc] initWithNibName:@"TXVehicleVC" bundle:nil];
        [self pushViewController:mvc];
    }
    else if (tag == 2) {
        TXTripHistoryVC *mvc = [[TXTripHistoryVC alloc] initWithNibName:@"TXTripHistoryVC" bundle:nil];
        [self pushViewController:mvc];
    }
    else if (tag == 3) {
        TXEarningsVC *mvc = [[TXEarningsVC alloc] initWithNibName:@"TXEarningsVC" bundle:nil];
        [self pushViewController:mvc];
    }
    else if (tag == 4) {
        TXNewsVC *mvc = [[TXNewsVC alloc] initWithNibName:@"TXNewsVC" bundle:nil];
        [self pushViewController:mvc];
    }
    else if (tag == 5) {
        TXFaqVC *mvc = [[TXFaqVC alloc] initWithNibName:@"TXFaqVC" bundle:nil];
        [self pushViewController:mvc];
    }
    else if (tag == 6) {
        TXQnaVC *mvc = [[TXQnaVC alloc] initWithNibName:@"TXQnaVC" bundle:nil];
        [self pushViewController:mvc];
    }
    else if (tag == 7) {
        TXSettingsVC *mvc = [[TXSettingsVC alloc] initWithNibName:@"TXSettingsVC" bundle:nil];
        [self pushViewController:mvc];
    }
    else if (tag == 8) {
        if ([[Utils nullToString:[[[TXApp instance] getSettings] getDriveMode]] isEqualToString:DRIVE_MODE_TRAINING]) {
            
            if (self->appDelegate.pstate <= 210) {
                // test 운행종료
                [self onPP136];
            }
            else {
                //
                //if ([Utils isNotAllowedMenu]) {return;}
            }
        }
        else {
            if (self->appDelegate.pstate == 200) {
                // test 운행시작
                [self onPP116];
            }
            else if (self->appDelegate.pstate == 210) {
                // -> offline
                [self didDone2:0 forTarget:nil];
            }
            else {
                if ([Utils isNotAllowedMenu]) {return;}
            }
        }
    }
}

#endif


#else

#if defined _CHACHA
#pragma mark - Rider ChaCha
- (void) sideMenuSelected:(NSInteger) tag
{
    if (tag == 0) {
        TXAccountVC *mvc = [[TXAccountVC alloc] initWithNibName:@"TXAccountVC" bundle:nil];
        [self pushViewController:mvc];
    }
    else if (tag == 1) {
        TXPaymentVC *mvc = [[TXPaymentVC alloc] initWithNibName:@"TXPaymentVC" bundle:nil];
        [self pushViewController:mvc];
    }
    else if (tag == 2) {
        TXTripHistoryVC *mvc = [[TXTripHistoryVC alloc] initWithNibName:@"TXTripHistoryVC" bundle:nil];
        //TXOtherTripHistoryVC *mvc = [[TXOtherTripHistoryVC alloc] initWithNibName:@"TXOtherTripHistoryVC" bundle:nil];
        //mvc.isShare = NO;
        [self pushViewController:mvc];
    }
    else if (tag == 3) {
        TXFavoriteVC *mvc = [[TXFavoriteVC alloc] initWithNibName:@"TXFavoriteVC" bundle:nil];
        mvc._target = self.dstLocationTextField;
        mvc.delegate = self;
        [self pushViewController:mvc];
    }
    else if (tag == 4) {
        TXNewsVC *mvc = [[TXNewsVC alloc] initWithNibName:@"TXNewsVC" bundle:nil];
        [self pushViewController:mvc];
    }
    else if (tag == 5) {
        TXPromotionsVC *mvc = [[TXPromotionsVC alloc] initWithNibName:@"TXPromotionsVC" bundle:nil];
        [self pushViewController:mvc];
    }
    else if (tag == 6) {
        TXInviteVC *mvc = [[TXInviteVC alloc] initWithNibName:@"TXInviteVC" bundle:nil];
        [self pushViewController:mvc];
    }
    else if (tag == 7) {
        TXFaqVC *mvc = [[TXFaqVC alloc] initWithNibName:@"TXFaqVC" bundle:nil];
        [self pushViewController:mvc];
    }
    else if (tag == 8) {
        TXQnaVC *mvc = [[TXQnaVC alloc] initWithNibName:@"TXQnaVC" bundle:nil];
        [self pushViewController:mvc];
    }
    else if (tag == 9) {
        TXSettingsVC *mvc = [[TXSettingsVC alloc] initWithNibName:@"TXSettingsVC" bundle:nil];
        [self pushViewController:mvc];
    }
    else if (tag == 10) {
        //[self onPP114];
    }
    else if (tag == 11) {
        BOOL isInstalled = [[UIApplication sharedApplication] openURL:[NSURL URLWithString:_DRIVER_APP_NAME]];
        if (!isInstalled) {
            // 설치 되어 있지 않습니다! 앱스토어로 안내...
            if ([self->appDelegate.appstoredriverurl length]) {
#ifdef _DEBUG
                if(SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(@"11.0")) {
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self->appDelegate.appstoredriverurl] options:@{} completionHandler:^(BOOL success) {
                        if (success) {
                            NSLog(@"Opened url");
                            exit(0);
                        }
                    }];
                }
                else {
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self->appDelegate.appstoredriverurl]];
                    //exit(0);
                }
#else
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self->appDelegate.appstoredriverurl]];
                //exit(0);
#endif
                
            }
            else {
                [Utils onErrorAlertViewTitle:LocalizedStr(@"Menu.Settings.Driver.AppStore.text") title:@""];
            }
        }
    }
}

#endif

#endif

#pragma mark - Driver, Rider Menu
- (void) didSideMenuSelected:(NSInteger) tag
{
    NSLog(@"didSideMenuSelected:%ld",(long)tag);

    if (tag == 9090) {
        if (self->appDelegate.isDebugMode) {
            self.activity.hidden = NO;
        }
        else {
            self.activity.hidden = YES;
        }
    }
    
    [self sideMenuSelected:tag];
}

- (void) didVehicleSelected
{
    NSLog(@"didVehicleSelected");
    
    [self callLoop:@"PQ100" withCancel:YES andDirect:YES];
}

- (void) openShareTripView
{
    /*
#ifndef _DRIVER_MODE
    if (![[Utils nullToString:self->appDelegate.shareOseq] isEqualToString:@""]) {
        if (![self->appDelegate.mapVCBeforeVC isEqualToString:@"TXOtherTripHistoryVC"] || ![self->appDelegate.mapVCBeforeVC isEqualToString:@"TXOtherTripDetailVC"]) {
            TXOtherTripHistoryVC *mvc = [[TXOtherTripHistoryVC alloc] initWithNibName:@"TXOtherTripHistoryVC" bundle:nil];
            mvc.isShare = YES;
            [self pushViewController:mvc];
        }
        else {
            self->appDelegate.shareOseq = @"";
        }
    }
#endif
     */
}

#pragma mark Touch
/*
 - (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
 
 UITouch *touch = [[event allTouches] anyObject];
 CGPoint touchLocation = [touch locationInView:_vehicleView];
 
 if (!dragging && CGRectContainsPoint(self.view.frame, touchLocation) && touchLocation.y >= 0) {
 dragging = YES;
 oldX = touchLocation.x;
 oldY = touchLocation.y;
 //NSLog(@"touchesBegan oldY:%f",oldY);
 }
 }
 
 - (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
 
 UITouch *touch = [[event allTouches] anyObject];
 CGPoint touchLocation = [touch locationInView:_vehicleView];
 
 float yy = _vehicleView.frame.origin.y + touchLocation.y - oldY;
 
 //NSLog(@"%f,%d,%f",( self.view.frame.size.height - kOverlayHeight),yy,touchLocation.y);
 
 if (dragging &&
 yy >= (self.view.frame.size.height - kOverlayHeight) &&
 yy <= (self.view.frame.size.height - kTopOverlayHeight)) {
 
 if (!self->appDelegate.isDriverMode) {
 mImageFocus.hidden = YES;
 mImagePin.hidden = YES;
 }
 
 CGRect frame = _vehicleView.frame;
 frame.origin.y =  yy;
 _vehicleView.frame = frame;
 
 CGSize size = self.view.bounds.size;
 
 self.mapView_.padding = UIEdgeInsetsMake(0, 0, size.height - yy, 0);
 
 [self markerWillMove];
 }
 }
 */
//- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
//
//    // 드라이브 모드고 재스쳐에 의해 움직인거면 특정시간 후 현 위치로 이동한다.
//    if (self->appDelegate.isDriverMode && isMoveMapBYGesture) {
//#ifdef _DEBUG_ACTIVITY
//        NSString *data = [NSString stringWithFormat:@"panGestureHandler"];
//        [self performSelectorOnMainThread:@selector(debugNotification:) withObject:data waitUntilDone:false];
//#endif
//        [self updateMapMoveTimer];
//    }
//}


#pragma mark KVO
-(void)addObservers {
    [self.mapView_ addObserver:self forKeyPath:@"myLocation" options:NSKeyValueObservingOptionNew context: nil];
}

-(void)removeObservers {
    [self.mapView_ removeObserver:self forKeyPath:@"myLocation"];
}


#pragma mark Current Location
-(void)initAddressSrcDst {
#ifndef _DRIVER_MODE
    _srcLocationTextField.text = @"";
    _dstLocationTextField.text = @"";

    //_routeCancel.hidden = YES;
    
    lsrc = kCLLocationCoordinate2DInvalid;
    ldst = kCLLocationCoordinate2DInvalid;
    
    [Utils updateSrcPosition:LOCATION_STR(lsrc.latitude) lng:LOCATION_STR(lsrc.longitude)];
    [Utils updateDstPosition:LOCATION_STR(ldst.latitude) lng:LOCATION_STR(ldst.longitude)];
    
    // manually gps location update
    [self didUpdateLocations];
    
    // 현재 위치
    CLLocationCoordinate2D position = [Utils currentPositionCoor];
    
    // 시작점 표시
    [self getAddressFromLatLon:position];
    
    //[self routeImageDidLoad];
#endif
}

-(void)initAddressSrc {
#ifndef _DRIVER_MODE
    _srcLocationTextField.text = @"";
    
    //_routeCancel.hidden = YES;
    
    lsrc = kCLLocationCoordinate2DInvalid;
    
    [Utils updateSrcPosition:LOCATION_STR(lsrc.latitude) lng:LOCATION_STR(lsrc.longitude)];
    //[Utils updateCurrentPosition:LOCATION_STR(lsrc.latitude) lng:LOCATION_STR(lsrc.longitude)];
    
#endif
}

-(void)initAddressDst {
    
//    if ([_dstLocationTextField.text isEqualToString:@""]) {
//        return;
//    }
    _dstLocationTextField.text = @"";
    ldst = kCLLocationCoordinate2DInvalid;
    
    [Utils updateDstPosition:LOCATION_STR(ldst.latitude) lng:LOCATION_STR(ldst.longitude)];
    
    // manually gps location update
    [self didUpdateLocations];
}

-(IBAction)clearDsetAddressTextField {
    
    //if (!self->appDelegate.isDriverMode && self->appDelegate.ustate == 110) {
    if (!self->appDelegate.isDriverMode) {
        // 주소초기화
        // map clear & dst address
        [self initAddressDst];
        [Utils setStateUIInfo:UI_STATE_TRIP_LINE value:UI_STATE_STEP_WAIT oldvalue:nil];
        
        [self pushCpos:btnCpos];
        
        [Utils setStateUIInfo:UI_STATE_ESTIMATE value:UI_STATE_STEP_FIN oldvalue:nil];
        
        // 목적지 지우면 현재 driver정보로 표시한다.
//        NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
//        dic = [estimate_result mutableCopy];
//        [dic removeObjectForKey:@"estimate_driver"];
//        estimate_result = dic;
        estimate_result = nil;
        
        // map 초기화
        [self cleanTripMapOnly];
        [self cleanMapOnly];
    }
}

-(void)configureStyles {
    [super configureStyles];
    
}

-(void)configure {
    
    NSLog(@"configure");
    promoCode = [[[TXApp instance] getSettings] getFDKeychain:@"promoCode"];
    promoCode = [Utils nullToString:promoCode];
    uName = @"";
    
#ifndef _DRIVER_MODE
    [self getServiceType];
    if ([serviceType isEqualToString:@""]) {
        [self setServiceType:DEFAULT_SERVICE];
    }
#endif
    //    [appDelegate resetTripInfo];
    
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    [locMgr initMarker];
    //[self initMarker];
    
    [super configure];
    
    [super statusBarStyleDefault];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:@"NotificationCustomURLReceived"
                                                      object:nil
                                                       queue:[NSOperationQueue mainQueue]
                                                  usingBlock:^(NSNotification *notification) {
                                                      
                                                      if (![appDelegate isLoginState]) {
                                                          return;
                                                      }
                                                      // change to share view
                                                      [self openShareTripView];
                                                  }];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:@"NotificationBackgroundFetchReceived"
                                                      object:nil
                                                       queue:[NSOperationQueue mainQueue]
                                                  usingBlock:^(NSNotification *notification) {
                                                      
                                                      if (![appDelegate isLoginState]) {
                                                          return;
                                                      }
                                                      
                                                      if (self->appDelegate.isNetwork) {
                                                          
                                                          [self backgroudFetchLoop];
                                                      }
                                                      else {
                                                          
                                                      }
                                                      
                                                  }];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:@"NotificationNetworkReachableReceived"
                                                      object:nil
                                                       queue:[NSOperationQueue mainQueue]
                                                  usingBlock:^(NSNotification *notification) {
                                                      
                                                      if (![appDelegate isLoginState]) {
                                                          return;
                                                      }
                                                      
                                                      if (self->appDelegate.isNetwork) {
                                                          //#if !TARGET_IPHONE_SIMULATOR
//                                                          if ([CLLocationManager locationServicesEnabled]) {
//
//                                                              [self startUpdatingLocation];
//                                                          }
                                                          //#endif
                                                          
                                                          
                                                          
                                                          if (self->appDelegate.isDriverMode) {
                                                              if (self->appDelegate.pstate == 230 && self->appDelegate.pstate2 == 10) {
                                                                  [self cancelAllLoop];
                                                                  [self callLoop:@"PD211" withCancel:YES andDirect:YES];
                                                                  return;
                                                              }
                                                              else if (self->appDelegate.pstate == 240 && self->appDelegate.pstate2 == 10) {
                                                                  [self cancelAllLoop];
                                                                  [self callLoop:@"PD311" withCancel:YES andDirect:YES];
                                                                  return;
                                                              }
                                                          }
                                                          
                                                          [self checkLoop];
                                                      }
                                                      else {
                                                          
                                                      }
                                                      
                                                  }];
    
//    [[NSNotificationCenter defaultCenter] addObserverForName:@"NotificationChangeModeReceived"
//                                                      object:nil
//                                                       queue:[NSOperationQueue mainQueue]
//                                                  usingBlock:^(NSNotification *notification) {
//                                                      
//                                                      if (![appDelegate isLoginState]) {
//                                                          return;
//                                                      }
//                                                      
//#ifdef _DRIVER_MODE
//                                                      self->appDelegate.isDriverMode = YES;
//#endif
//                                                      
//                                                      if (!self->appDelegate.isDriverMode) {
//                                                          //self->appDelegate.isDriverMode = YES;
//                                                          [self changeMode];
//                                                          
//                                                          // updateUI
//                                                      }
//                                                      else {
//                                                          //self->appDelegate.isDriverMode = NO;
//                                                          [self changeMode];
//                                                          
//                                                          // updateUI
//                                                      }
//                                                      
//                                                      
//                                                      [menuVC dismissViewControllerAnimated:YES completion:nil];
//                                                      
//                                                  }];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:@"NotificationPushReceived"
                                                      object:nil
                                                       queue:[NSOperationQueue mainQueue]
                                                  usingBlock:^(NSNotification *notification) {
                                                      
                                                      if (![appDelegate isLoginState]) {
                                                          return;
                                                      }
                                                      
                                                      NSDictionary* dict = notification.userInfo;
                                                      //NSLog(@"NotificationPushReceived:%@",dict);
                                                      
                                                      if ([Utils isDictionary:[dict valueForKey:@"notification"]]) {
                                                          //get
                                                          if (self->appDelegate.isDriverMode) {
                                                              if (self->appDelegate.pstate == 240) {
                                                                  [self onPQ300];
                                                              }
                                                              else if (self->appDelegate.pstate > 220) {
                                                                  [self onPQ200];
                                                              }
                                                              else if (self->appDelegate.pstate >= 210) {
                                                                  [self onPQ100];
                                                              }
                                                          }
                                                          else {
                                                              if (self->appDelegate.ustate == 140) {
                                                                  [self onUQ300];
                                                              }
                                                              else if (self->appDelegate.ustate >= 120) {
                                                                  [self onUQ200];
                                                              }
                                                              else if (self->appDelegate.ustate >= 110) {
                                                                  [self onUQ002];
                                                              }
                                                          }
                                                          return;
                                                      }
                                                      else if ([Utils isDictionary:[dict valueForKey:@"aps.data"]]) {
                                                          dict = [dict valueForKey:@"aps.data"];
                                                          //                                                          NSString *jsonString = [dict valueForKey:@"data"];
                                                          //                                                          NSError *jsonError;
                                                          //                                                          NSData *objectData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
                                                          //                                                          dict = [NSJSONSerialization JSONObjectWithData:objectData
                                                          //                                                                                                               options:NSJSONReadingMutableContainers
                                                          //                                                                                                                 error:&jsonError];
                                                          
                                                          
                                                          NSString* _msg = [dict valueForKey:@"mid"];
                                                          // 채팅메시지
                                                          if ([_msg isEqualToString:@"UD355"] || [_msg isEqualToString:@"PD355"]) {
                                                              
                                                              // new badge
                                                              [self updateNewBadge:YES];
                                                              
                                                              [Utils updateBGChatInfo:dict[@"drive_message"][@"oseq"]];
                                                              
                                                              if ((self->appDelegate.isDriverMode && self->appDelegate.pstate > 220) || (!self->appDelegate.isDriverMode && self->appDelegate.ustate > 120)) {
                                                                  NSString* oseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.OSEQ];
                                                                  if ([dict[@"drive_message"][@"oseq"] isEqualToString:oseq]) {
                                                                      [self updateChatBadge:YES];
                                                                  }
                                                              }
                                                              __block NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
                                                              [notificationCenter postNotificationName:@"NotificationPushChat" object:nil userInfo:dict];
                                                              
                                                              // alert를 띄우는 경우
                                                              // 채팅화면인데 같은 oseq가 아니면
                                                              if ([self->appDelegate.mapVCBeforeVC isEqualToString:@"ChatView"]) {
                                                                  
                                                                  if (![self->appDelegate.chatOseq isEqualToString:dict[@"drive_message"][@"oseq"]]) {
                                                                      NSString *chat_name = @"";
                                                                      NSString *chat_body = LocalizedStr(@"Push.Alert.chat.text");

                                                                      if (self->appDelegate.isDriverMode) {
                                                                          chat_name = [NSString stringWithFormat:@"%@",dict[@"drive_message"][@"uname"]];
                                                                      }
                                                                      else {
                                                                          chat_name = [NSString stringWithFormat:@"%@",dict[@"drive_message"][@"pname"]];
                                                                      }
                                                                      chat_name = @"";
                                                                      // alert
                                                                      if([UIApplication sharedApplication].applicationState == UIApplicationStateActive) {
                                                                          // alert
                                                                          [Utils onErrorAlertViewTitle:chat_body title:chat_name];
                                                                          [self playTTS:chat_body vol:1.0f];
                                                                      }
                                                                      else {
                                                                          // local notification
                                                                          [Utils sendNotofication:chat_body title:chat_name];
                                                                      }
                                                                  }
                                                              }
                                                              // 채팅화면이 아니면
                                                              else {
                                                                  // alert
                                                                  NSString *chat_name = @"";
                                                                  NSString *chat_body = LocalizedStr(@"Push.Alert.chat.text");

                                                                  if (self->appDelegate.isDriverMode) {
                                                                      chat_name = [NSString stringWithFormat:@"%@",dict[@"drive_message"][@"uname"]];
                                                                  }
                                                                  else {
                                                                      chat_name = [NSString stringWithFormat:@"%@",dict[@"drive_message"][@"pname"]];
                                                                  }
                                                                  chat_name = @"";
                                                                  // alert
                                                                  if([UIApplication sharedApplication].applicationState == UIApplicationStateActive) {
                                                                      // alert
                                                                      [Utils onErrorAlertViewTitle:chat_body title:chat_name];
                                                                      [self playTTS:chat_body vol:1.0f];
                                                                  }
                                                                  else {
                                                                      // local notification
                                                                      [Utils sendNotofication:chat_body title:chat_name];
                                                                  }
                                                              }
                                                              
                                                              // toast
                                                              //[Utils onErrorAlert:LocalizedStr(@"Push.Alert.chat.text") kwtlong:YES];
                                                          }
                                                          // 공유메시지
                                                          else if ([_msg isEqualToString:@"UD362"]) {
                                                              // new badge
                                                              [self updateNewBadge:YES];
                                                              NSString* oseq = dict[@"drive_share"][@"oseq"];
                                                              
                                                              [Utils updateAlrim:CODE_TYPE_SHARE_TRIP code:CODE_ALRIM_SHARE_TRIP(oseq) value:CODE_FLAG_ON];
                                                              
                                                              // toast
                                                              [Utils onErrorAlert:LocalizedStr(@"Push.Alert.share.text") kwtlong:YES];
                                                          }
                                                          // order
                                                          else if ([_msg isEqualToString:@"PD151"] || [_msg isEqualToString:@"UD151"]) {
                                                              NSInteger state = [dict[@"drive_order"][@"state"] integerValue];
                                                              NSString* mode = [[[TXApp instance] getSettings] getDriveMode];
                                                              if ([[Utils nullToString:mode] isEqualToString:DRIVE_MODE_TRAINING]) {
                                                                  if (state == -25) {
                                                                      // order를 받지 않아서 test mode off처리한다.
                                                                      [self onPP136];
                                                                      return;
                                                                  }
                                                              }
                                                          }
                                                          
                                                          
                                                          if([UIApplication sharedApplication].applicationState == UIApplicationStateInactive) {
                                                          }
                                                          if (![[Utils nullToString:[dict objectForKey:@"error"]] isEqualToString:@""]) {
                                                              
                                                              BOOL __flag = NO;
                                                              NSDictionary *drive_order = dict[@"drive_order"];
                                                              NSDictionary *user_device = dict[@"user_device"];
                                                              if ([Utils isDictionary:drive_order] && [Utils isDictionary:user_device]) {
                                                                  
                                                                  NSString *ostate = [NSString stringWithFormat:@"%@",drive_order[@"state"]];
                                                                  NSString *ustate = [NSString stringWithFormat:@"%@",user_device[@"state"]];
                                                                  
                                                                  if ([ostate isEqualToString:@"10"] && [ustate isEqualToString:@"120"]) {
                                                                      if (self->appDelegate.isDriverMode && self->appDelegate.pstate <= 220) {
                                                                          // request이면 alert에서 승인을 때린다.
                                                                          __flag = YES;
                                                                          //                                                                          LGAlertView *alertView = [[LGAlertView alloc] initWithTitle:nil
                                                                          //                                                                                                                              message:[dict objectForKey:@"alert"]
                                                                          //                                                                                                                                style:LGAlertViewStyleAlert
                                                                          //                                                                                                                         buttonTitles:@[LocalizedStr(@"Button.OK")]
                                                                          //                                                                                                                    cancelButtonTitle:nil
                                                                          //                                                                                                               destructiveButtonTitle:nil
                                                                          //                                                                                                                        actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                                                                          //                                                                                                                            // 수락
                                                                          //                                                                                                                            //[self onPD110]; // order accept를 하지 말고 pickup request를 한다.
                                                                          //                                                                                                                            [self onPD220];
                                                                          //                                                                                                                        }
                                                                          //                                                                                                                        cancelHandler:^(LGAlertView *alertView) {
                                                                          //
                                                                          //                                                                                                                        }
                                                                          //                                                                                                                   destructiveHandler:^(LGAlertView *alertView) {
                                                                          //
                                                                          //                                                                                                                   }];
                                                                          //                                                                          [Utils initAlertButtonColor:alertView];
                                                                          //                                                                          [alertView showAnimated:YES completionHandler:^(void)
                                                                          //                                                                           {
                                                                          //
                                                                          //                                                                           }];
                                                                      }
                                                                  }
                                                              }
                                                              
                                                              if (!__flag) {
                                                                  [appDelegate onCommonErrorAlert:dict[@"error"] timeout:0];
                                                                  //[Utils onErrorAlertViewTitle:[dict objectForKey:@"alert"] title:@""];
                                                              }
                                                          }
                                                          /*
                                                           if  (self->appDelegate.isDriverMode) {
                                                           if (![[Utils nullToString:[dict objectForKey:@"alert"]] isEqualToString:@""]) {
                                                           [Utils onErrorAlertViewTitle:[dict objectForKey:@"alert"] title:@""];
                                                           }
                                                           }
                                                           */
                                                          
                                                          //get
                                                          if (self->appDelegate.isDriverMode) {
                                                              if (self->appDelegate.pstate == 240) {
                                                                  [self onPQ300Direct];
                                                              }
                                                              else if (self->appDelegate.pstate > 220) {
                                                                  [self onPQ200Direct];
                                                              }
                                                              else if (self->appDelegate.pstate >= 210) {
                                                                  [self onPQ100Direct];
                                                              }
                                                          }
                                                          else {
                                                              if (self->appDelegate.ustate == 140) {
                                                                  [self onUQ300];
                                                              }
                                                              else if (self->appDelegate.ustate >= 120) {
                                                                  [self onUQ200];
                                                              }
                                                              else if (self->appDelegate.ustate >= 110) {
                                                                  [self onUQ002];
                                                              }
                                                          }
                                                          
                                                          //get
                                                          //                                                          if (self->appDelegate.isDriverMode) {
                                                          //                                                              if (self->appDelegate.pstate == 240) {
                                                          //                                                                  [self onPQ300];
                                                          //                                                              }
                                                          //                                                              else if (self->appDelegate.pstate > 220) {
                                                          //                                                                  [self onPQ200];
                                                          //                                                              }
                                                          //                                                              else if (self->appDelegate.pstate >= 210) {
                                                          //                                                                  [self onPQ100];
                                                          //                                                              }
                                                          //                                                          }
                                                          //                                                          else {
                                                          //                                                              if (self->appDelegate.ustate == 140) {
                                                          //                                                                  [self onUQ300];
                                                          //                                                              }
                                                          //                                                              else if (self->appDelegate.ustate >= 120) {
                                                          //                                                                  [self onUQ200];
                                                          //                                                              }
                                                          //                                                              else if (self->appDelegate.ustate >= 110) {
                                                          //                                                                  [self onUQ002];
                                                          //                                                              }
                                                          //                                                          }
                                                          return;
                                                      }
                                                      else if ([Utils isDictionary:[dict valueForKey:@"aps"]]) {
                                                          NSString *jsonString = [dict valueForKey:@"gcm.data"];
                                                          NSError *jsonError;
                                                          NSData *objectData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
                                                          dict = [NSJSONSerialization JSONObjectWithData:objectData
                                                                                                 options:NSJSONReadingMutableContainers
                                                                                                   error:&jsonError];
                                                      }
                                                      
                                                      //                                                      NSLog(@"dict:%@",dict);
                                                      //                                                      NSLog(@"dict:%@",[dict valueForKey:@"mid"]);
                                                      NSString* _msg = [dict valueForKey:@"mid"];
                                                      if ([_msg isEqualToString:@"UD130"]) {
                                                          [self onUD130];
                                                      }
                                                      
                                                      if ([_msg isEqualToString:@"UD150"]) {
                                                          [self pushUD150];
                                                      }
                                                      else if ([_msg isEqualToString:@"UD151"]) {
                                                          [self pushUD151];
                                                      }
                                                      else if ([_msg isEqualToString:@"UD250"]) {
                                                          [self pushUD250];
                                                      }
                                                      else if ([_msg isEqualToString:@"UD253"]) {
                                                          [self pushUD253];
                                                      }
                                                      else if ([_msg isEqualToString:@"UD252"]) {
                                                          [self pushUD252];
                                                      }
                                                      else if ([_msg isEqualToString:@"UD254"]) {
                                                          [self pushUD254];
                                                      }
                                                      else if ([_msg isEqualToString:@"UD350"]) {
                                                          [self pushUD350];
                                                      }
                                                      else if ([_msg isEqualToString:@"UD352"]) {
                                                          [self pushUD352];
                                                      }
                                                      else if ([_msg isEqualToString:@"UD353"]) {
                                                          [self pushUD353];
                                                      }
                                                      else if ([_msg isEqualToString:@"UD354"]) {
                                                          [self pushUD354];
                                                      }
                                                      /// Driver
                                                      else if ([_msg isEqualToString:@"PD150"]) {
                                                          [self pushPD150];
                                                      }
                                                      else if ([_msg isEqualToString:@"PD151"]) {
                                                          [self pushPD151];
                                                      }
                                                      else if ([_msg isEqualToString:@"PD152"]) {
                                                          [self pushPD152];
                                                      }
                                                      else if ([_msg isEqualToString:@"PD251"]) {
                                                          [self pushPD251];
                                                      }
                                                      else if ([_msg isEqualToString:@"PD252"]) {
                                                          [self pushPD252];
                                                      }
                                                      else if ([_msg isEqualToString:@"PD351"]) {
                                                          [self pushPD351];
                                                      }
                                                      
                                                  }];
    
    
    [self initMapView];
#ifdef _DRIVER_MODE
    if (self->appDelegate.imgProfileDriver == nil) {
        [self onPP602];
    }
#else
    if (self->appDelegate.imgProfileRider == nil) {
        [self onUR601];
    }
#endif
    //self->appDelegate.isDriverMode = YES;
    //[self changeMode];
}

-(void)changeMode {
    [self cancelAllLoop];
    [self cancelTimeout];
    
    // install event
    [self configureEvent];
#ifdef _DRIVER_MODE
    
    //self.mapView_.myLocationEnabled = YES;
    
    if (self->appDelegate.pstate == 200) {
        [self onPQ200];
        [self onPP400];
        //[self callLoop:@"PQ002" withCancel:YES andDirect:YES];
    }
    else if (self->appDelegate.pstate == 230) {
        [self onPQ200];
        //[self callLoop:@"PQ200" withCancel:YES andDirect:YES];
    }
    else if (self->appDelegate.pstate == 240) {
        [self onPQ300];
        //[self callLoop:@"PQ300" withCancel:YES andDirect:YES];
    }
    else {
        [self onPQ100];
        //[self callLoop:@"PQ100" withCancel:YES andDirect:YES];
    }
    
#else
    
    //self.mapView_.myLocationEnabled = NO;
    
    if (self->appDelegate.ustate == 120) {
        [self->model UD131];
    }
    else if (self->appDelegate.ustate == 130) {
        [self onUQ200];
        //[self callLoop:@"UQ200" withCancel:YES andDirect:YES];
    }
    else if (self->appDelegate.ustate == 140) {
        [self onUQ300];
        //[self callLoop:@"UQ300" withCancel:YES andDirect:YES];
    }
    else {
        [self onUQ002];
        //[self callLoop:@"UQ002" withCancel:YES andDirect:YES];
    }
#endif
    
    //[self startSingleLocationRequest];
    
//    [self onUpdateUI];
//    [self onUpdateUI:@{@"result1":@"data1"}];
    
    //#if !TARGET_IPHONE_SIMULATOR
//    if ([CLLocationManager locationServicesEnabled]) {
//
//        [self startUpdatingLocation];
//    }
    //#endif
//    [self moveCposButton];
}

#ifdef _DRIVER_MODE
-(void)configureUpdatingDriver {
    // prefer
    [self onPP206];
    
    //[self callLoop:@"PQ100" withCancel:YES andDirect:YES];
}

-(void)configureEvent {
    
    // self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"btn_set_pickup_location"]];
    
    // event 등록, msg/timer
    _msgList = @[
                    @[@"UR110",@""], // login
//                    @[@"UR114",@""], // logout
                    
                    @[@"PP110",@""], // login
//                    @[@"PP114",@""], // logout
                    @[@"PD015",@""], // 차량선택
                    @[@"PD035",@""], // 차량해제
                    @[@"PP116",@""], // 연습시작
                    @[@"PP136",@""], // 연습종료
                    //                    @[@"PP200",@""], // 제공자상세 요청
                    @[@"PQ002",@"30"], // surge rate polling
                    
                    @[@"PQ100",@"10"], // order polling
                    
                    @[@"PP601",@""], // rider profile image
                    @[@"PP602",@""], // profile image
                    @[@"PP603",@""], // vehicle image
                    
                    @[@"UR200",@""], // profile rider
                    @[@"UR206",@""], // profile rider
                    
                    @[@"PP206",@""], // profile rider
                    
                    @[@"PD150",@"push"], // 220.10 or 취소사유
                    @[@"PD151",@"push"], // rider 목적지 변경
                    @[@"PD110",@""], // accept 220.20
                    @[@"PD130",@""], // cancel order
                    @[@"PD131",@""], // timeout order
                    @[@"PD152",@"push"], // 210
                    
                    @[@"PQ200",@"3"], // pickup 상태 조회 polling
                    @[@"PD220",@""], // 220.20이 되면 바로 올린다. pickup start -> 230.10
                    @[@"PD251",@""], // rider 목적지 변경
                    @[@"PD211",@"30"], // pickup step : 계산식에 의해 올리는 시간이 결정됨.
                    @[@"PD213",@""], // pickup arrive 230.20
                    @[@"PD252",@"push"], // cancel 210
                    @[@"PD232",@""], // cancel 210
                    
                    
                    @[@"PQ300",@"5"], // trip begine 240.10 polling
                    @[@"PD320",@""], // trip begine 240.10
                    @[@"PD351",@"push"], // rider 목적지 변경
                    @[@"PD310",@""], // driver 목적지 변경
                    @[@"PD311",@"30"], // trip step : 계산식에 의해 올리는 시간이 결정됨.
                    @[@"PD313",@""], // trip end 240.20
                    //                      @[@"PD421",@""], // charge rate 210
                    @[@"PD330",@""], // del trip 210
                    
                    @[@"PP400",@""], // 차량정보 loading
                    ];
    
    [self registerEventListeners];
    
    // 메시지 body정보
    settings  = [[TXApp instance] getSettings];
    NSString *userUid   = [settings getUserId];
    NSString *userTelno = [settings getUserTelno];
    
    user = [[TXUser alloc] init];
    user.uid = userUid;
    user.utelno = userTelno;
    
    //    self.view = self.mapView_;
    //[self setOverlayDriver];
    
    [self configureUpdatingDriver];
}
#else
-(void)configureUpdatingRider {
    // prefer
    [self onUR206];
    
    //payment
    [self->model requsetAPI:@"UR300" property:nil];
}

-(void)configureEvent {
    
    // event 등록, msg/timer
    _msgList = @[@[@"PP110",@""], // login
//                    @[@"PP114",@""], // logout
                    
                    @[@"UR110",@""], // login
//                    @[@"UR114",@""], // logout
                    @[@"UQ002",@"10"], // driver polling
                    @[@"UQ100",@"5"], // order polling
                    @[@"UD110",@""], // rider 목적지 변경
                    @[@"UQ101",@""], // rider 예상시간
                    @[@"UD120",@""], // order 120.10
                    @[@"UD112",@""], // pay_nonce
                    
                    @[@"UR601",@""], // rider profile image
                    @[@"UR602",@""], // profile image
                    @[@"UR603",@""], // vehicle image
                    
                    @[@"UR200",@""], // profile rider
                    @[@"UR206",@""], // profile prefer
                    @[@"UR300",@""], // payment
                    
                    //@[@"UD130",@""], // cancel order
                    @[@"UD131",@"3"], // order timeout
                    @[@"UD150",@"push"], // order accept 120.20
                    @[@"UD151",@"push"], // fail 110
                    
                    @[@"UQ200",@"5"], // pickup polling
                    @[@"UD210",@""], // rider 목적지 변경
                    //@[@"UD230",@""], // rider 취소
                    @[@"UD250",@"push"], // pickup start 130.10
                    @[@"UD253",@"push"], // pickup arrive 130.20
                    //                      @[@"UD230",@""], // del pickup 110
                    @[@"UD252",@"push"], // 110
                    @[@"UD254",@"push"], // 110
                    
                    @[@"UQ300",@"10"], // trip polling
                    @[@"UD310",@""], // rider 목적지 변경
                    @[@"UD350",@"push"], // trip begin 140.10
                    @[@"UD352",@"push"], // driver 목적지 변경
                    @[@"UD353",@"push"], // trip end 140.20
                    
                    @[@"UD354",@"push"], // fail 110
                 
                 @[@"UR303",@""], // my coupon
                    ];
    
    [self registerEventListeners];
    
    // 메시지 body정보
    settings  = [[TXApp instance] getSettings];
    NSString *userUid   = [settings getUserId];
    NSString *userTelno = [settings getUserTelno];
    
    user = [[TXUser alloc] init];
    user.uid = userUid;
    user.utelno = userTelno;
    
    //    self.view = self.mapView_;
    //[self setOverlay];
    
    //    mImagePin.hidden = NO;
    
    //[self callLoop:@"UQ002" withCancel:YES andDirect:YES];
    
    [self configureUpdatingRider];
    
}
#endif

-(void)saveProfileImage:(UIImage*)image withCode:(NSString*)code{
    
    // 운전자 모드일때
#ifdef _DRIVER_MODE
    // rider 요청일때 1301, driver요청일때 1101
    if ([code isEqualToString:@"PP601"]) {
        [appDelegate setImgProfileRider:image];
        [_btnRider setImage:image forState:UIControlStateNormal];
    }
    else if ([code isEqualToString:@"PP602"]) {
        [appDelegate setImgProfileDriver:image];
    }
#else
    // driver 요청일때 1301, rider요청일때 1101
    if ([code isEqualToString:@"UR601"]) {
        [appDelegate setImgProfileRider:image];
    }
    else if ([code isEqualToString:@"UR602"]) {
        [appDelegate setImgProfileDriver:image];
        _imgDriver.image = image;
        [_btnimgDriver setImage:image forState:UIControlStateNormal];
    }
    else if ([code isEqualToString:@"UR603"]) {
        [appDelegate setImgVehicleDriver:image];
        _imgVehicle.image = image;
        [_btnimgVehicle setImage:image forState:UIControlStateNormal];
    }
#endif
    // Store image
    //[self->appDelegate.cache storeObject:event forKey:request.reqUrl];
    
    // 드라이브 모드일때 230.20에서 대기시간 초과일때 우측 버튼은 취소버튼이기때문에 이미지 업데이트를 막는다.
    if (self->appDelegate.isDriverMode && self->_isEtTimerTimeout) {
        return;
    }
}


#pragma mark - SlideNavigationController Methods -

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    return YES;
}

- (BOOL)slideNavigationControllerShouldDisplayRightMenu
{
    return NO;
}

#pragma mark - 화면움직임 애니메이션
- (void)moveScreen
{
    //    NSDictionary = _etaView.frame.origin.y;
    
    [UIView animateWithDuration:1.0 animations:^{
        //_etaView.frame.origin.y
        mImagePin.alpha = 1.0f;
    }];
}

#pragma mark -
#pragma mark UIGestureRecognizerDelegate
#pragma mark -

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer{
    //default is no - if we dont set it to on then gesture is captured by GMSMapView and myGestureHandler never called
    return YES;
}

#pragma mark -
#pragma mark UIPanGestureRecognizer handler
#pragma mark -
-(void)panGestureHandler:(UIPanGestureRecognizer *)recognizer {
    
    if([recognizer state] == UIGestureRecognizerStatePossible)
    {
    }
    else if([recognizer state] == UIGestureRecognizerStateBegan)
    {
    }
    else if([recognizer state] == UIGestureRecognizerStateChanged)
    {
    }
    else if([recognizer state] == UIGestureRecognizerStateEnded)
    {
        INTULocationManager *locMgr = [INTULocationManager sharedInstance];
        //
        // 드라이브 모드고 재스쳐에 의해 움직인거면 특정시간 후 현 위치로 이동한다.
        if (self->appDelegate.isDriverMode && locMgr.isMoveMapBYGesture) {
            [self updateMapMoveTimer];
        }
    }
    else if([recognizer state] == UIGestureRecognizerStateCancelled)
    {
    }
    else if([recognizer state] == UIGestureRecognizerStateFailed)
    {
    }
    else{
        NSLog(@"ERROR:<%@ %@:(%d)> %s UNHANDLED STATE", self, [[NSString stringWithUTF8String:__FILE__] lastPathComponent], __LINE__, __PRETTY_FUNCTION__);
    }
    //-----------------------------------------------------------------------------------
    
}

-(void)pinchGestureHandler:(UIPinchGestureRecognizer *)recognizer {
    
    if([recognizer state] == UIGestureRecognizerStatePossible)
    {
    }
    else if([recognizer state] == UIGestureRecognizerStateBegan)
    {
    }
    else if([recognizer state] == UIGestureRecognizerStateChanged)
    {
    }
    else if([recognizer state] == UIGestureRecognizerStateEnded)
    {
        INTULocationManager *locMgr = [INTULocationManager sharedInstance];
        //
        // 드라이브 모드고 재스쳐에 의해 움직인거면 특정시간 후 현 위치로 이동한다.
        if (self->appDelegate.isDriverMode && locMgr.isMoveMapBYGesture) {
            [self updateMapMoveTimer];
        }
    }
    else if([recognizer state] == UIGestureRecognizerStateCancelled)
    {
    }
    else if([recognizer state] == UIGestureRecognizerStateFailed)
    {
    }
    else{
        NSLog(@"ERROR:<%@ %@:(%d)> %s UNHANDLED STATE", self, [[NSString stringWithUTF8String:__FILE__] lastPathComponent], __LINE__, __PRETTY_FUNCTION__);
    }
    //-----------------------------------------------------------------------------------
    
}

/*
-(void) onBackgroundTap {
    if (self->appDelegate.isDriverMode && self->appDelegate.pstate == 220 && self->appDelegate.pstate2 == 10) {
        // 수락
        //[self onPD110]; // order accept를 하지 말고 pickup request를 한다.
        [self onPD220];
    }
}
*/
#pragma mark - GMSMapViewDelegate

- (void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate {
    NSLog(@"didTapAtCoordinate map");
    /*
    if (self->appDelegate.isDriverMode && self->appDelegate.pstate == 220 && self->appDelegate.pstate2 == 10) {
        // 수락
        //[self onPD110]; // order accept를 하지 말고 pickup request를 한다.
        [self onPD220];
    }
     */
}

- (void)mapView:(GMSMapView *)mapView willMove:(BOOL)gesture
{
    //    [self recenterMarkerInMapView:mapView];
    //NSLog(@"willMove:%d",gesture);
    if (!gesture) {
        return;
    }
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    locMgr.isMoveMapBYGesture = gesture;
    
#ifdef _DEBUG_ACTIVITY
    NSString *data = [NSString stringWithFormat:@"willMove gesture :%d",gesture];
    [self performSelectorOnMainThread:@selector(debugNotification:) withObject:data waitUntilDone:false];
#endif
    
    // 교통정보
    if (!self->appDelegate.isKorea) {
        if (self->appDelegate.isDriverMode && ((self->appDelegate.pstate == 230 ||self->appDelegate.pstate == 240) && self->appDelegate.pstate2 == 10) && gesture == 1 ) {
            self.mapView_.trafficEnabled = YES;
        }
    }
    
    // map move timer 초기화
    if (self->appDelegate.isDriverMode && ((self->appDelegate.pstate == 230 ||self->appDelegate.pstate == 240) && self->appDelegate.pstate2 == 10) && gesture == 1 ) {
        [self stopMapMoveTimer];
    }
    
#ifdef _MAP_TOUCH_MOVE
#ifdef _DRIVER_MODE
    // pos image
    [self updateCposImage:NO];
#else
    if (!self->appDelegate.isDriverMode && self->appDelegate.ustate <= 110 && gesture == 1 &&
        ([_srcLocationTextField.text isEqualToString:@""] || [_dstLocationTextField.text isEqualToString:@""]) ) {
        
        isMapMoved = YES;

        [[NSOperationQueue mainQueue] addOperationWithBlock:^ {
            [UIView animateWithDuration:kOverlayMovingTime animations:^{
                // 상단
                CGRect rect1 = [super navigationView].frame;
                UIView* statusView = [super statusView];
                CGRect rect2 = self->_vTopMainNavi.frame;
                
                rect1.origin.y = -(rect1.size.height + kTopHeight + rect2.size.height);
                rect2.origin.y = -rect2.size.height;
                
                [super navigationView].frame = rect1;
                self->_vTopMainNavi.frame = rect2;
                statusView.alpha = 0.0f;
                
                //[[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationFade];
                
                // 하단.
                CGRect rect4 = self->_vAddress.frame;
                rect4.origin.y = self.view.frame.size.height;
                self->_vAddress.frame = rect4;
                
                // mylocation
                self->btnCpos.alpha = 0.0f;
                
                // favorite
                self->_vFavoriteHome.alpha = 0.0f;
                self->_vFavoriteWork.alpha = 0.0f;
                self->_vFavoriteLast.alpha = 0.0f;
                
                [self updateCposImage:NO];
            }];
        }];
    }
    else {
        [self updateCposImage:NO];
    }
#endif
#else
    // pos image
    [self updateCposImage:NO];
#endif
}


- (void)mapView:(GMSMapView *)mapView didChangeCameraPosition:(GMSCameraPosition *)position {
    //NSLog(@"geocode point (%f,%f)",position.target.latitude, position.target.longitude);

    /*
     CGPoint point = [mapView.projection pointForCoordinate:position.target];
     //    NSLog(@"point1:%@",NSStringFromCGPoint(point));
     point.x = point.x + mImageFocus.frame.size.width / 2;
     point.y = point.y + mImageFocus.frame.size.height;
     
     CGRect rect = mImageFocus.frame;
     rect.origin = point;
     */
    //[mImageFocus setFrame:rect];
}

- (void)mapView:(GMSMapView *)mapView idleAtCameraPosition:(GMSCameraPosition *)camera {
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    
#ifdef _MAP_TOUCH_MOVE
#ifdef _DRIVER_MODE
    // 드라이브 모드고 재스쳐에 의해 움직인거면 특정시간 후 현 위치로 이동한다.
    if (self->appDelegate.isDriverMode && locMgr.isMoveMapBYGesture) {
        [self setTimerMoveToCurrentPosition:5];
    }
    else {
        locMgr.isMoveMapBYGesture = NO;
    }
#else
    if (!self->appDelegate.isDriverMode && self->appDelegate.ustate <= 110 && locMgr.isMoveMapBYGesture &&
        ([_srcLocationTextField.text isEqualToString:@""] || [_dstLocationTextField.text isEqualToString:@""])) {
        
        [UIView animateWithDuration:kOverlayMovingTime animations:^{
            
            // 상단
            CGRect rect1 = [super navigationView].frame;
            UIView* statusView = [super statusView];
            CGRect rect2 = _vTopMainNavi.frame;
            
            rect1.origin.y = 0;
            rect2.origin.y = kTopHeight;
            
            [super navigationView].frame = rect1;
            _vTopMainNavi.frame = rect2;
            statusView.alpha = 1.0f;
            
            //[[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];
            
            // 하단.
            CGRect rect4 = _vAddress.frame;
            rect4.origin.y = (self.view.frame.size.height - kBottomSafeArea - _vAddress.frame.size.height);
            _vAddress.frame = rect4;
            
            // mylocation
            btnCpos.alpha = 1.0f;
            
            // favorite
            _vFavoriteHome.alpha = 1.0f;
            _vFavoriteWork.alpha = 1.0f;
            _vFavoriteLast.alpha = 1.0f;
        }];
    }
#endif
#else
    if (!self->appDelegate.isDriverMode && locMgr.isMoveMapBYGesture) {
        // 카메라 움직임으로 주소를 가져오지 않는다.
        return;
    }
#endif
    
    
//    // 드라이브 모드고 재스쳐에 의해 움직인거면 특정시간 후 현 위치로 이동한다.
//    if (self->appDelegate.isDriverMode && locMgr.isMoveMapBYGesture) {
//#ifdef _DEBUG_ACTIVITY
//        NSString *data = [NSString stringWithFormat:@"idleAtCameraPosition"];
//        [self performSelectorOnMainThread:@selector(debugNotification:) withObject:data waitUntilDone:false];
//#endif
//        [self updateMapMoveTimer];
//        //self->mapMoveTime = [NSDate date]; // 현재시간 보관
//        //[self setTimerMoveToCurrentPosition:5];
//    }
//    else {
//        //locMgr.isMoveMapBYGesture = NO;
//    }
    //locMgr.isMoveMapBYGesture = NO;
    
    // 카메라 움직임으로 주소를 가져오지 않는다.
    
#ifndef _DRIVER_MODE
    
    // 기사를 요청한 상태이거나 운행중이면 지도의 좌표를 바꾸지 않는다.
    if (self->appDelegate.ustate >= 120) {
        return;
    }
    // 주소가 모두 입력되어 있으면 주소변경을 하지 않느다.
    if (![_srcLocationTextField.text isEqualToString:@""] && ![_dstLocationTextField.text isEqualToString:@""]) {
        return;
    }
#ifdef _MAP_TOUCH_MOVE
#else
    // 출발지와 주소표시 정보가 같으면
    if ([Utils isEqualLocation:lsrc location:[Utils srcPositionCoor]]) {
        return;
    }
#endif
    // 출발지가 없으면 주소를 읽어온다. (라이더)
    [self getAddressFromLatLon:camera.target];
    
    NSNumber *myDoubleNumberLat = [NSNumber numberWithDouble:camera.target.latitude];
    NSNumber *myDoubleNumberLng = [NSNumber numberWithDouble:camera.target.longitude];

#ifdef _MAP_TOUCH_MOVE
    [Utils updateCurrentPosition:[myDoubleNumberLat stringValue] lng:[myDoubleNumberLng stringValue]];
#endif
    [Utils updateSrcPosition:[myDoubleNumberLat stringValue] lng:[myDoubleNumberLng stringValue]];
    
    lsrc = camera.target;

#ifdef _MAP_TOUCH_MOVE
#else
    // picup marker
    if (![Utils isEqualLocation:locMgr.markerStart.position location:lsrc]) {
        //            [locMgr clearMarker:locMgr.markerStart];
        //            locMgr.markerStart.map = nil;
        [locMgr updateStartMarker:lsrc];
//        locMgr.markerStart.position = lsrc;
//        if (locMgr.markerStart.map == nil) {
//            locMgr.markerStart.map = self.mapView_;
//        }
    }
#endif
    
    NSLog(@"geocode point (%f,%f)",camera.target.latitude, camera.target.longitude);

#endif
}


- (void)mapView:(GMSMapView *)mapView didLongPressAtCoordinate:(CLLocationCoordinate2D)coordinate {
    // On a long press, reverse geocode this location.
    NSLog(@"geocode point (%f,%f)",
          coordinate.latitude, coordinate.longitude);
    
}

-(void)mapView:(GMSMapView *)mapView didBeginDraggingMarker:(GMSMarker *)marker {
    //    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
}

- (void) mapView:(GMSMapView *)mapView didDragMarker:(GMSMarker *)_marker
{
    //self->pickupCoordinate = _marker.position;
}

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    //self.mapView.centerCoordinate = userLocation.location.coordinate;
    NSLog(@"startLocationUpdateSubscription point (%f,%f)",
          userLocation.coordinate.latitude, userLocation.coordinate.longitude);
    //  MKCoordinateRegion region=MKCoordinateRegionMakeWithDistance([userLocation coordinate], 100000, 100000);
    //  [self.mapView setRegion:region];
}

-(BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)_marker {
    NSLog(@"Did Tap Marker >>>>>>");
    [mapView setSelectedMarker:_marker];
#ifndef _DRIVER_MODE
    
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    // 110인 경우 위치 상세 조정
    if (!self->appDelegate.isDriverMode && self->appDelegate.ustate == 110) {
        if ([_marker isEqual:locMgr.markerStart]) {
            //
            [self selectPickupAddress:nil];
        }
        else if ([_marker isEqual:locMgr.markerFinish]) {
            //
            [self selectDestAddress:nil];
        }
        return NO;
    }
#endif
    // 기사요청일때
    /*
    if (self->appDelegate.isDriverMode && self->appDelegate.pstate == 220 && self->appDelegate.pstate2 == 10) {
        //[self onPD110]; // order accept를 하지 말고 pickup request를 한다.
        [self onPD220];
    }
     */
    return YES;
}

- (void)mapView:(GMSMapView *)mapView didTapInfoWindowOfMarker:(GMSMarker *)_marker {
    
}

- (void)mapView:(GMSMapView *)mapView didTapOverlay:(GMSOverlay *)overlay {
    // When a polygon is tapped, randomly change its fill color to a new hue.
    if ([overlay isKindOfClass:[GMSPolygon class]]) {
        GMSPolygon *polygon = (GMSPolygon *)overlay;
        //        NSString *title = [NSString stringWithFormat:LocalizedStr(@"Map.Driver.Surge.text"),polygon.title];
        TOAST_MSG(polygon.title);
        
        //        CGFloat hue = (((float)arc4random()/0x100000000)*1.0f);
        //        polygon.fillColor =
        //        [UIColor colorWithHue:hue saturation:1 brightness:1 alpha:0.5];
    }
}

#pragma mark CLLocationManager delegate

-(void)didUpdateLocations:(CLLocation*)location {
    static CLLocation *olocation = nil;

    INTULocationManager *locMgr = [INTULocationManager sharedInstance];

    if ([Utils isEqualLocation:olocation.coordinate location:location.coordinate]) {
        return;
    }
    
//    if (!locMgr.isMoveMapBYGesture) {
//        if (self->appDelegate.isDriverMode && self->appDelegate.pstate >= 210) {
//            [CATransaction begin];
//            //[CATransaction setAnimationDuration:(distance / (50 * 1000))];  // custom duration, 2m/sec 50km/sec 50 * 1000
//            [CATransaction setAnimationDuration:1];  // custom duration, 2m/sec 50km/sec 50 * 1000
//            
//            [self.mapView_ animateToLocation:location.coordinate];
//            
//            [CATransaction commit];
//        }
//    }
    
    if (!self->appDelegate.isDriverMode && self->appDelegate.ustate > 110) {
        //[self.locationMgr stopUpdatingLocation];
        [appDelegate forceCompleteRequest];
    
        //self->appDelegate.location = location;
        olocation = location;
        [self didUpdateLocations];
        return;
    }
    
    if (!locMgr.isMoveMapBYGesture) {
        if (self->appDelegate.isDriverMode && self->appDelegate.pstate >= 210) {
            [CATransaction begin];
            //[CATransaction setAnimationDuration:(distance / (50 * 1000))];  // custom duration, 2m/sec 50km/sec 50 * 1000
            [CATransaction setAnimationDuration:1];  // custom duration, 2m/sec 50km/sec 50 * 1000
            
            [self.mapView_ animateToLocation:location.coordinate];
            
            [CATransaction commit];
        }
    }

    //self->appDelegate.location = location;
    olocation = location;
    [self didUpdateLocations];
}

- (void)onETAPrint
{
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    if ([Utils isDictionary:locMgr.distancematrix]) {
        NSDictionary *dic = @{
                              @"edistance":locMgr.distancematrix[@"remain_distance"],
                              @"eduration":locMgr.distancematrix[@"remain_duration"]
                              };
        [self print_eta:dic toggle:0 api:YES];
    }
}
- (void)didUpdateLocations
{
    static CLLocationCoordinate2D olocation;
    
    CLLocationCoordinate2D currLocation = self->appDelegate.location.coordinate;// [Utils currentPositionCoor];
    NSNumber *myDoubleNumberLat = [NSNumber numberWithDouble:currLocation.latitude];
    NSNumber *myDoubleNumberLng = [NSNumber numberWithDouble:currLocation.longitude];
    
    if (![GoogleUtils isLocationValid:currLocation]) {
        return;
    }
    
    NSString *newposition = [NSString stringWithFormat:@"%@,%@",[myDoubleNumberLat stringValue], [myDoubleNumberLng stringValue]];
    
    if ([Utils isEqualLocation:olocation location:currLocation]) {
        return;
    }
    
    // 차량은 trip 중에 gps에 따라 움직인다
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    if (self->appDelegate.isDriverMode &&
        (self->appDelegate.pstate == 230 || self->appDelegate.pstate == 240) && self->appDelegate.pstate2 == 10 &&
        (locMgr.tripline == nil || locMgr.tripline.map == nil)
        ) {
        ldrv = [Utils getLocation:newposition];
    }
    
    // bgmode가 아니면 UI처리한다.
    //if (![bgTask running])
    {
        
        // driver일때 지도를 진행방향으로 돌린다.
        if (self->appDelegate.isDriverMode && ((self->appDelegate.pstate == 230 ||self->appDelegate.pstate == 240) && self->appDelegate.pstate2 == 10) )
        {
            //                CLLocationCoordinate2D locationPoint = self->appDelegate.location.coordinate;
            //                [self animateToBearing:locationPoint];
            
            CLLocation *currentLocation = self->appDelegate.location;
            [locMgr updateDriveMap:currentLocation dst:[Utils destinationPositionCoor]];
            //
            
            // bgmode가 아니면 UI처리한다.
            if (![bgTask running]) {
                [self onETAPrint];
            }
            
            //[self navigationTTS:locationPoint];// 네비게이션 TTS 주석
        }
        else if (self->appDelegate.isDriverMode && self->appDelegate.pstate == 230 && self->appDelegate.pstate2 == 20 ) {
            CLLocationDirection heading = GMSGeometryHeading(locMgr.markerDrive.position, self->appDelegate.location.coordinate);
            NSLog(@"heading:%f",heading);
            BOOL isChangedHeading = YES;
            if (heading<15) isChangedHeading = NO;
            isChangedHeading = NO;
            [locMgr moveDriveMarker:1.2 coor:self->appDelegate.location.coordinate head:isChangedHeading heading:heading];
        }
    }
    olocation = self->appDelegate.location.coordinate;
    [Utils updateCurrentPosition:[myDoubleNumberLat stringValue] lng:[myDoubleNumberLng stringValue]];
}

#pragma mark Util
-(void)showNewChat
{
    NSString* oseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.OSEQ];
    if ([Utils showAlrim:CODE_TYPE_CHAT code:oseq]) {
        [self updateChatBadge:YES];
    }
    else {
        [self updateChatBadge:NO];
    }
}

-(void)showAlrim
{
    if ([Utils showAlrim]) {
        [self updateNewBadge:YES];
    }
    else {
        [self updateNewBadge:NO];
    }
}

-(void)removeBadge
{
}

-(void)updateChatBadge:(BOOL)flag
{
    if (flag) {
#ifdef _DRIVER_MODE
//        _btnRider.badgeColor   = [UIColor redColor];
//        _btnRider.badgeOriginX = _btnRider.frame.size.width - _btnRider.badgeMinSize;
//        _btnRider.badgeOriginY = 0;
        
        _btnRiderName.badgeColor   = [UIColor redColor];
        _btnRiderName.badgeMinSize = 10;
        _btnRiderName.badgeOriginX = _btnRiderName.frame.size.width - _btnRiderName.badgeMinSize*2;
        _btnRiderName.badgeOriginY = _btnRider.frame.size.height/4 - 5;
#else
        _btnChat.badgeColor   = [UIColor redColor];
        _btnChat.badgeMinSize = 10;
        _btnChat.badgeOriginX = _btnChat.imageView.frame.origin.x + _btnChat.imageView.frame.size.width - _btnChat.badgeMinSize;
        _btnChat.badgeOriginY = _btnChat.imageView.frame.origin.y;
#endif
    }
    else {
#ifdef _DRIVER_MODE
        _btnRiderName.badgeColor   = [UIColor clearColor];
#else
        _btnChat.badgeColor   = [UIColor clearColor];
#endif
    }
#ifdef _DRIVER_MODE
    //[Utils setCircleImage:_btnRider];
#endif
}

-(void)updateNewBadge:(BOOL)flag
{
    
    return;
    /* not show menu
    UIButton *btnBadgeOrg;

#ifdef _DRIVER_MODE
    if (self->appDelegate.pstate == 200) {
        btnBadgeOrg = [_vTopDriverNavi200 viewWithTag:1101];
    }
    else if (self->appDelegate.pstate == 210) {
        btnBadgeOrg = [_vTopDriverNavi210 viewWithTag:1101];
    }
    else {
        btnBadgeOrg = [_vTopDriverNavi2X0 viewWithTag:1101];
    }
#else
    if (self->appDelegate.ustate <= 110 && ![_srcLocationTextField.text isEqualToString:@""] && [_dstLocationTextField.text isEqualToString:@""]) {
        btnBadgeOrg = [_vTopMainNavi viewWithTag:1101];
    }
    else {
        btnBadgeOrg = [_vTopRiderNavi viewWithTag:1101];
    }
#endif

    if (flag) {
        btnBadgeOrg.badgeColor   = [UIColor redColor];
    }
    else {
        btnBadgeOrg.badgeColor   = [UIColor clearColor];
    }
     */
}

- (void)moveCompassButton:(GMSMapView *) map{
    
    //    map.padding = UIEdgeInsetsMake (map.frame.size.height - 45, 0, 0, 40);
    //self.mapView_.padding = UIEdgeInsetsMake(_addrView.frame.origin.y + _addrView.frame.size.height + kBasicHeight, 0, _addrView.frame.origin.y + _addrView.frame.size.height + kBasicHeight, 0);
    /*
     for(UIView *view in [map subviews]){
     NSRange isRange = [view.description rangeOfString:@"GMSUISettingsView"];
     if(isRange.location != NSNotFound){
     for(UIView *subview in [view subviews]){
     NSRange isRange2 = [subview.description rangeOfString:@"GMSCompassButton"];
     if(isRange2.location != NSNotFound){
     CGRect frame = view.frame;
     frame.origin.y = 255;
     frame.origin.x = map.frame.size.width/2 - 10;
     [view setFrame:frame];
     }
     }
     }
     }
     */
}

- (void)setTimerMoveToCurrentPosition:(NSInteger)__timer
{
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    // 드라이브 모드고 재스쳐에 의해 움직인거면 특정시간 후 현 위치로 이동한다.
    self->mapTouchMoveTimer = [NSDate date]; // 현재시간 보관
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, __timer * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        if (locMgr.isMoveMapBYGesture) {
            NSDate *methodFinish = [NSDate date];
            if (self->mapTouchMoveTimer<0) {
                return;
            }
            
            NSTimeInterval executionTime = [methodFinish timeIntervalSinceDate:self->mapTouchMoveTimer];
            NSLog(@"executionTime = %f, %d, %@", executionTime, (int)(executionTime/60),self->mapMoveTimer);
            if (executionTime > 5) {
                
                if (self->appDelegate.isDriverMode && self->appDelegate.pstate == 220 && self->appDelegate.pstate2 == 10) {
                    return;
                }
                CLLocationCoordinate2D coord = [Utils currentPositionCoor];
                [self.mapView_ animateToLocation:coord];
                self->mapTouchMoveTimer = 0;
                locMgr.isMoveMapBYGesture = NO;
                [self updateCposImage:YES];
                self.mapView_.trafficEnabled = NO;
                
            }
        }
        
    });
    
}

-(void)getVehicleCount {
    if ([dataArray count]) {
        [dataArray removeAllObjects];
    }
    
    NSArray *provider_vehicle = [self->appDelegate.dicDriver objectForKey:@"provider_vehicles"];
    for (NSDictionary* key in provider_vehicle) {
        if ([key[@"state"] intValue] == 1) {
            NSString *vehicle = [NSString stringWithFormat:@"%@ / %@ / %@",
                                 [Utils nullToString:key[@"service_name"]],
                                 [Utils nullToString:key[@"vehicleno"]],
                                 [Utils nullToString:key[@"vehiclemodel"]]
                                 ];
            [dataArray addObject:vehicle];
        }
    }
}

-(void)getMarkerAddress:(CLLocationCoordinate2D)coor marker:(GMSMarker*)marker__
{
    [self getMarkerAddress:coor marker:marker__ textField:nil];
}

-(void)getMarkerAddressCallback:(id)address coor:(CLLocationCoordinate2D)coor marker:(GMSMarker*)marker__ textField:(UITextField*)textfield__
{
    BOOL isTripline__ = NO;

    NSLog(@"Geocoder result: %@,%d", address,(int)textfield__.tag);
    
    if (textfield__ != nil) {

        textfield__.text = address[@"fullAddress"];
        
#ifndef _DRIVER_MODE
        if ([textfield__ isEqual:_srcLocationTextField]) {
            
            if ([textfield__.text isEqualToString:@""]) {
                lsrc = kCLLocationCoordinate2DInvalid;
                textfield__.text = LocalizedStr(@"Address.None.text");
            }
            else {
                lsrc.latitude = [Utils formattedPositionCoor:coor.latitude];
                lsrc.longitude= [Utils formattedPositionCoor:coor.longitude];
            }
            
            [Utils updateSrcPosition:LOCATION_STR(lsrc.latitude) lng:LOCATION_STR(lsrc.longitude)];
            
            [Utils setStateUIInfo:UI_STATE_START_ADDR value:UI_STATE_STEP_FIN oldvalue:nil];
        }
        else {
            
            if ([textfield__.text isEqualToString:@""]) {
                ldst = kCLLocationCoordinate2DInvalid;
                textfield__.text = LocalizedStr(@"Address.None.text");
            }
            else {
                ldst.latitude = [Utils formattedPositionCoor:coor.latitude];
                ldst.longitude= [Utils formattedPositionCoor:coor.longitude];
            }
            
            [Utils updateDstPosition:LOCATION_STR(ldst.latitude) lng:LOCATION_STR(ldst.longitude)];
            
            [Utils setStateUIInfo:UI_STATE_END_ADDR value:UI_STATE_STEP_FIN oldvalue:nil];
        }
#else
        if ([textfield__ isEqual:_dstLocationTextField]) {
            
            if (self->appDelegate.pstate == 230) {
                lsrc.latitude = [Utils formattedPositionCoor:coor.latitude];
                lsrc.longitude= [Utils formattedPositionCoor:coor.longitude];
                
                [Utils updateSrcPosition:LOCATION_STR(lsrc.latitude) lng:LOCATION_STR(lsrc.longitude)];
                
                isTripline__ = YES;
                
#ifdef _DEBUG_ACTIVITY
                [self performSelectorOnMainThread:@selector(debugNotification:)
                                       withObject:[NSString stringWithFormat:@"drawRouteOnMAP getMarkerAddress230"]
                                    waitUntilDone:false];
#endif
                
                [self drawRoute];
            }
            else if (self->appDelegate.pstate == 240 && self->appDelegate.pstate2 == 10) {
                ldst.latitude = [Utils formattedPositionCoor:coor.latitude];
                ldst.longitude= [Utils formattedPositionCoor:coor.longitude];
                
                [Utils updateDstPosition:LOCATION_STR(ldst.latitude) lng:LOCATION_STR(ldst.longitude)];
                
                isTripline__ = YES;
                
#ifdef _DEBUG_ACTIVITY
                [self performSelectorOnMainThread:@selector(debugNotification:)
                                       withObject:[NSString stringWithFormat:@"drawRouteOnMAP getMarkerAddress240.10"]
                                    waitUntilDone:false];
#endif
                
                [self drawRoute];
            }
        }
        
        [Utils setStateUIInfo:UI_STATE_END_ADDR value:UI_STATE_STEP_FIN oldvalue:nil];
#endif
    }
    
    //marker__.snippet = [address.lines firstObject];
    //marker__.snippet = [Utils addressToString:address];//address.thoroughfare;
    if (!isTripline__) {
        marker__.position = coor;
    }
    
#if defined (_MAP_TOUCH_MOVE) && ! defined (_DRIVER_MODE)
    // 110인 경우 start marker를 보이지 않게 한다. imagepin을 사용하기 위해
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    if ([marker__ isEqual:locMgr.markerStart] && !self->appDelegate.isDriverMode && self->appDelegate.ustate <= 110 &&
        ([_srcLocationTextField.text isEqualToString:@""] || [_dstLocationTextField.text isEqualToString:@""])) {
        [locMgr clearMarker:marker__];
        
        [self showFocusPin];
        return;
    }
    else {
        [self hideFocusPin];
    }
#endif
    
    marker__.map = self.mapView_;
    
    //[self routeImageDidLoad];
    
    if ([textfield__ isEqual:_dstLocationTextField] && !self->appDelegate.isDriverMode && self->appDelegate.ustate == 140 && self->appDelegate.ustate2 == 10) {
        if (![_dstLocationTextField.text isEqualToString:@""]) {
            // fit
            // 라이더가 운행중일때 도착지가 변경되면 지도를 fit한다.
            [self updateVehicleFitBounds];
        }
    }
}
-(void)getMarkerAddress:(CLLocationCoordinate2D)coor marker:(GMSMarker*)marker__ textField:(UITextField*)textfield__
{
    if (![textfield__.text isEqualToString:@""] && [Utils pointCompare:coor.latitude comp:marker__.position.latitude] && [Utils pointCompare:coor.longitude comp:marker__.position.longitude]) {
        return;
    }
    if (![GoogleUtils isLocationValid:coor]) {
        return;
    }
    
#ifndef _DRIVER_MODE
    if ([textfield__ isEqual:_srcLocationTextField]) {
        if ([Utils checkStateUIInfo:UI_STATE_START_ADDR oldvalue:[Utils formattedPosition:coor]] == UI_STATE_STEP_RUN) {
            return;
        }
        if ([Utils checkStateUIInfo:UI_STATE_START_ADDR oldvalue:[Utils formattedPosition:coor]] == UI_STATE_STEP_FIN) {
            if (![textfield__.text isEqualToString:@""]) {
                return;
            }
        }
        [Utils setStateUIInfo:UI_STATE_START_ADDR value:UI_STATE_STEP_RUN oldvalue:[Utils formattedPosition:coor]];
        
        //_srcIndicator.hidden = NO;
    }
    else {
        if ([Utils checkStateUIInfo:UI_STATE_END_ADDR oldvalue:[Utils formattedPosition:coor]] == UI_STATE_STEP_RUN) {
            return;
        }
        if ([Utils checkStateUIInfo:UI_STATE_END_ADDR oldvalue:[Utils formattedPosition:coor]] == UI_STATE_STEP_FIN) {
            if (![textfield__.text isEqualToString:@""]) {
                return;
            }
            return;
        }
        [Utils setStateUIInfo:UI_STATE_END_ADDR value:UI_STATE_STEP_RUN oldvalue:[Utils formattedPosition:coor]];
        
        //_dstIndicator.hidden = NO;
    }
#else
    {
        if ([Utils checkStateUIInfo:UI_STATE_END_ADDR oldvalue:[Utils formattedPosition:coor]] == UI_STATE_STEP_RUN) {
            return;
        }
        if ([Utils checkStateUIInfo:UI_STATE_END_ADDR oldvalue:[Utils formattedPosition:coor]] == UI_STATE_STEP_FIN) {
            if (![textfield__.text isEqualToString:@""]) {
                return;
            }
            return;
        }
        [Utils setStateUIInfo:UI_STATE_END_ADDR value:UI_STATE_STEP_RUN oldvalue:[Utils formattedPosition:coor]];
        
        //_dstIndicator.hidden = NO;
    }
#endif
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    [locMgr geocodeAPICall:coor
                completion:^(NSDictionary *user, NSString* params, NSString *str, int status) {
                    
                    if (status==1) {
                        //NSAssert(![NSThread isMainThread], @"This shouldn't be called from the main thread");
                        //NSLog(@"responseDict:%@",user);
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            // Update the UI
                            [self getMarkerAddressCallback:user coor:coor marker:marker__ textField:textfield__];
                        });
                    }
                    else {
                        dispatch_async(dispatch_get_main_queue(), ^{
#ifdef _DRIVER_MODE
                            [Utils setStateUIInfo:UI_STATE_END_ADDR value:UI_STATE_STEP_FAIL oldvalue:nil];
#else
                            if ([textfield__ isEqual:_srcLocationTextField]) {
                                [Utils setStateUIInfo:UI_STATE_START_ADDR value:UI_STATE_STEP_FAIL oldvalue:nil];
                            }
                            else {
                                [Utils setStateUIInfo:UI_STATE_END_ADDR value:UI_STATE_STEP_FAIL oldvalue:nil];
                            }
#endif
                        });
                    }
                }];
    
}

-(NSString *)getDestinationAddress:(CLLocationCoordinate2D)coor
{
    if (![GoogleUtils isLocationValid:coor]) {
        return @"";
    }
    
    if ([Utils checkStateUIInfo:UI_STATE_END_ADDR oldvalue:[Utils formattedPosition:coor]] == UI_STATE_STEP_RUN) {
        return @"";
    }
    if ([Utils checkStateUIInfo:UI_STATE_END_ADDR oldvalue:[Utils formattedPosition:coor]] == UI_STATE_STEP_FIN) {
        return @"";
    }
    [Utils setStateUIInfo:UI_STATE_END_ADDR value:UI_STATE_STEP_RUN oldvalue:[Utils formattedPosition:coor]];
    
    // 목적지가 변경 되었습니다.
    //[self playTTS:LocalizedStr(@"Map.Driver.DST.Change.text") vol:1.0f];
    
    //    _dstIndicator.hidden = NO;
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    [locMgr geocodeAPICall:coor
                completion:^(NSDictionary *user, NSString* params, NSString *str, int status) {
                    
                    if (status==1) {
                        //NSAssert(![NSThread isMainThread], @"This shouldn't be called from the main thread");
                        //NSLog(@"responseDict:%@",user);
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            // Update the UI
                            _dstLocationTextField.text = user[@"fullAddress"];
                            ldst.latitude = [Utils formattedPositionCoor:coor.latitude];
                            ldst.longitude= [Utils formattedPositionCoor:coor.longitude];
                            
                            [Utils updateDstPosition:LOCATION_STR(ldst.latitude) lng:LOCATION_STR(ldst.longitude)];
                            
                            [Utils setStateUIInfo:UI_STATE_END_ADDR value:UI_STATE_STEP_FIN oldvalue:nil];
                            
#ifdef _DEBUG_ACTIVITY
                            [self performSelectorOnMainThread:@selector(debugNotification:)
                                                   withObject:[NSString stringWithFormat:@"drawRouteOnMAP getDestinationAddress"]
                                                waitUntilDone:false];
#endif
                            [self drawRoute];
                        });
                    }
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [Utils setStateUIInfo:UI_STATE_END_ADDR value:UI_STATE_STEP_FIN oldvalue:nil];
                    });
                    
                }];
    
    return @"";
}

#ifndef _DRIVER_MODE
-(void)getAddressFromLatLon:(CLLocationCoordinate2D)coor
{
    static CLLocationCoordinate2D oldcoor;
    
    if (![GoogleUtils isLocationValid:coor]) {
        return;
    }
    
    if ([Utils checkStateUIInfo:UI_STATE_START_ADDR oldvalue:[Utils formattedPosition:coor]] == UI_STATE_STEP_RUN) {
        return;
    }
//    if ([Utils checkStateUIInfo:UI_STATE_START_ADDR oldvalue:[Utils formattedPosition:coor]] == UI_STATE_STEP_FIN) {
//        return;
//    }
    [Utils setStateUIInfo:UI_STATE_START_ADDR value:UI_STATE_STEP_RUN oldvalue:[Utils formattedPosition:coor]];
    
    oldcoor = coor;
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    [locMgr geocodeAPICall:coor
                completion:^(NSDictionary *user, NSString* params, NSString *str, int status) {
                    
                    if (status==1) {
                        //NSAssert(![NSThread isMainThread], @"This shouldn't be called from the main thread");
                        //NSLog(@"responseDict:%@",user);
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            // Update the UI
                            _srcLocationTextField.text = user[@"fullAddress"];
                            if ([_srcLocationTextField.text isEqualToString:@""]) {
                                lsrc = kCLLocationCoordinate2DInvalid;
                                _srcLocationTextField.text = LocalizedStr(@"Address.None.text");
                            }
                            else {
                                lsrc.latitude = [Utils formattedPositionCoor:coor.latitude];
                                lsrc.longitude= [Utils formattedPositionCoor:coor.longitude];
                            }
                            
                            [Utils updateSrcPosition:LOCATION_STR(lsrc.latitude) lng:LOCATION_STR(lsrc.longitude)];
                        });
                    }
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [Utils setStateUIInfo:UI_STATE_START_ADDR value:UI_STATE_STEP_FIN oldvalue:nil];
                    });
                    
                }];
}
#endif

-(void)getAddress:(CLLocationCoordinate2D)coor select:(SEL)selector
{
    if (![GoogleUtils isLocationValid:coor]) {
        return;
    }
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    [locMgr geocodeAPICall:coor
                completion:^(NSDictionary *user, NSString* params, NSString *str, int status) {
                    
                    if (status==1) {
                        //NSAssert(![NSThread isMainThread], @"This shouldn't be called from the main thread");
                        //NSLog(@"responseDict:%@",user);
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            // Update the UI
                            
                            [self performSelector:selector withObject:user afterDelay:0.00001];
                        });
                    }
                    
                }];

}

/* map을 고정시키거나 움직이이 가능하게 한다 */
-(void)setMapGesture:(BOOL)flag {
    return;
    /*
     self.mapView_.settings.scrollGestures = flag;
     self.mapView_.settings.zoomGestures = flag;
     self.mapView_.settings.tiltGestures = flag;
     self.mapView_.settings.rotateGestures = flag;
     */
}
/*
 service = X;
 "service_name" = Witz;
 service = P;
 "service_name" = WitzPlus;
 service = L;
 "service_name" = WitzLuxury;
 service = A;
 "service_name" = WitzAgent;
 */
/*
-(UIImage*)getMarkerIcon:(NSString*)icon__ {
    UIImage *icon = nil;
    NSLog(@"getMarkerIcon:%@",icon__);
    if (icon__.length == 1) {
        //UIImage *img = [UIImage imageNamed:@"img_mapcar"];
        UIImage *img = [UIImage imageNamed:[NSString stringWithFormat:@"img_mapcar_%@",icon__]];
        icon = [Utils image:img scaledToSize:CGSizeMake(kDriverMarkerSizeW, kDriverMarkerSizeH)];
        //icon = [Utils image:img scaledToSize:CGSizeMake(img.size.width, img.size.height)];
        self->car_normal = icon;
    }
    else if ([icon__ isEqualToString:@"route_start"])
        icon = [Utils image:[UIImage imageNamed:@"icon_pin_start"] scaledToSize:CGSizeMake(kMarkerSizeW, kMarkerSizeH)];
    else if ([icon__ isEqualToString:@"route_end"])
        icon = [Utils image:[UIImage imageNamed:@"icon_pin_end"] scaledToSize:CGSizeMake(kMarkerSizeW, kMarkerSizeH)];
    else if ([icon__ isEqualToString:@"pickup"])
        icon = [Utils image:[UIImage imageNamed:@"icon_pin_start"] scaledToSize:CGSizeMake(kMarkerSizeW, kMarkerSizeH)];
    else {
        NSLog(@"getMarkerIcon ERROR:%@",icon__);
    }
    return icon;
}
*/

-(void)hideDriveMarker {
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    [locMgr clearMarker:locMgr.markerDrive];
}

-(void)clearMarker:(GMSMarker*)marker__ {
    if (self->appDelegate.isDriverMode && self->appDelegate.pstate > 210) {
        INTULocationManager *locMgr = [INTULocationManager sharedInstance];
        if ([marker__ isEqual:locMgr.markerDrive]) return;
    }
    
    if (marker__.map != nil) {
        marker__.map = nil;
        marker__.icon = nil;
        marker__.position = kCLLocationCoordinate2DInvalid;
    }
}

-(void)cleanTripMapOnly {
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    if (
        locMgr.tripline != nil || locMgr.tripline.map != nil) {
        locMgr.tripline.map = nil;
    }
    
    // bearing오류를 수정한다
    if (self->appDelegate.isDriverMode && self->appDelegate.pstate <= 210) {
        // breaing 0
        if (self.mapView_.camera.bearing != 0)
            [self updateMapHeading];
    }
    else if (!self->appDelegate.isDriverMode && self->appDelegate.ustate <= 110) {
        // breaing 0
        if (self.mapView_.camera.bearing != 0)
            [self updateMapHeading];
    }
}

-(void)cleanMapOnly {
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    //[locMgr clearMarker:locMgr.markerStart];
    locMgr.markerStart.icon  = [locMgr getMarkerIcon:@"route_start"];
    [locMgr clearMarker:locMgr.markerFinish];
    
    [self pushCpos:btnCpos];
    //CLLocationCoordinate2D coordinate = [Utils srcPositionCoor];
    CLLocationCoordinate2D coordinate = [Utils currentPositionCoor];
    NSLog(@"coordinate:%f,%f",coordinate.latitude,coordinate.longitude);
    self.mapView_.camera = [GMSCameraPosition cameraWithTarget:coordinate
                                                          zoom:[self getZoomByState]];
}

-(void)initStartEndPosition {
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    
#ifndef _DRIVER_MODE
    // 출발점
    if ([_srcLocationTextField.text isEqualToString:@""]) {
        
        __block LGAlertView *alertView = [[LGAlertView alloc] initWithTitle:nil
                                                                    message:LocalizedStr(@"Map.Rider.Request.SrcAddress.Fail.text")
                                                                      style:LGAlertViewStyleAlert
                                                               buttonTitles:@[LocalizedStr(@"Button.OK")]
                                                          cancelButtonTitle:nil
                                                     destructiveButtonTitle:nil
                                                              actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                                                              }
                                                              cancelHandler:^(LGAlertView *alertView) {
                                                                  NSLog(@"cancelHandler");
                                                              }
                                                         destructiveHandler:^(LGAlertView *alertView) {
                                                             NSLog(@"destructiveHandler");
                                                         }];
        [Utils initAlertButtonColor:alertView];
        [alertView showAnimated:YES completionHandler:^(void)
         {
             dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                 [alertView dismissAnimated:YES completionHandler:nil];
             });
         }];
        
        return;
    }
    
    CLLocationCoordinate2D coordinate = [Utils srcPositionCoor];
    
    if (![Utils isEqualLocation:locMgr.markerStart.position location:coordinate]) {
        [locMgr clearMarker:locMgr.markerStart];
        locMgr.markerStart.position = coordinate;
        locMgr.markerStart.map = self.mapView_;
        
        lsrc = locMgr.markerStart.position;
    }
#endif

    // 도착점
    if ([_dstLocationTextField.text isEqualToString:@""]) {
        return;
    }
    
    CLLocationCoordinate2D coordinate2 = [Utils destinationPositionCoor];
    if (![Utils isEqualLocation:locMgr.markerFinish.position location:coordinate2]) {
        [locMgr clearMarker:locMgr.markerFinish];
        [locMgr updateFinishMarker:coordinate2];
        
        ldst = locMgr.markerFinish.position;
    }
}

-(void)updateStartPosition:(NSString*)lsrc__  {
    CLLocationCoordinate2D position = [Utils getLocation:lsrc__];
    
    if (![GoogleUtils isLocationValid:position]) {
        return;
    }
    
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    if (![Utils isEqualLocation:locMgr.markerStart.position location:position] || ![GoogleUtils isLocationValid:lsrc]) {
#ifdef _DRIVER_MODE
        // 픽업지
        [Utils updateSrcPosition:LOCATION_STR(position.latitude) lng:LOCATION_STR(position.longitude)];
        [self getMarkerAddress:position marker:locMgr.markerStart textField:_dstLocationTextField];
        
#else
        // 출발점
        [Utils updateSrcPosition:LOCATION_STR(position.latitude) lng:LOCATION_STR(position.longitude)];
        [self getMarkerAddress:position marker:locMgr.markerStart textField:_srcLocationTextField];
#endif
    }
}

-(void)updateFinishPosition:(NSString*)ldst__  {
    CLLocationCoordinate2D position = [Utils getLocation:ldst__];
    
    if (![GoogleUtils isLocationValid:position]) {
        return;
    }
    
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    // 도착점
    if (![Utils isEqualLocation:locMgr.markerFinish.position location:position] || ![GoogleUtils isLocationValid:ldst]) {
        [Utils updateDstPosition:LOCATION_STR(position.latitude) lng:LOCATION_STR(position.longitude)];
        [self getMarkerAddress:position marker:locMgr.markerFinish textField:_dstLocationTextField];
    }
}

-(void)updateDrivePosition:(NSString*)service__ uname:(NSString*)uname__ lsrc:(NSString*)lsrc__  {
    
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    // 드리이브 위치
    if ([Utils isEqualLocation:locMgr.markerDrive.position location:[Utils getLocation:lsrc__]] && locMgr.markerDrive.icon != nil) {
        if (locMgr.markerDrive.map != nil) {
            return;
        }
    }
    
    if (locMgr.markerDrive.icon == nil) {
        locMgr.markerDrive.icon = [locMgr getMarkerIcon:service__];
        //locMgr.markerDrive.snippet = [Utils nameOfuname:uname__];
    }
    
    //[self updateDrivePosition:lsrc__ animation:NO];
    [self updateDrivePosition:lsrc__ animation:YES];
}

-(void)updateDrivePosition:(NSString*)lsrc__  animation:(BOOL)animation{
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    // 드리이브 위치
    if ([Utils isEqualLocation:locMgr.markerDrive.position location:[Utils getLocation:lsrc__]]) {
        if (locMgr.markerDrive.map != nil) {
            return;
        }
    }
    
    //locMgr.markerDrive.position = [self getLocation:[self currentPosition]];
    locMgr.markerDrive.map = self.mapView_;

    ldrv = [Utils getLocation:lsrc__];
    NSLog(@"ldrv----------- %f,%f",ldrv.latitude,ldrv.longitude);
    
    // 차령이동 - 애니메이션
    [self animateToNextCoord:locMgr.markerDrive nextLocation:ldrv];
    
    // 지도 이동 -> 라이더는 차량만 움직이고 map을 움직이지 않는다.
    //[self.mapView_ animateToLocation:ldrv];
}

-(void)updateDrivePosition  {
    NSString* __lsrc = @"";
    
    NSDictionary *dic = nil;
    if (self->appDelegate.isDriverMode) {
        dic = [self->appDelegate.dicDriver objectForKey:@"provider_device"];
    }
    else {
        dic = [self->appDelegate.dicRider objectForKey:@"provider_device"];
    }
    
    if ([Utils isDictionary:dic]) {
        __lsrc = [Utils nullTolocation:[dic objectForKey:@"location"]];
    }

    if (![__lsrc isEqualToString:@""]) {
        [self updateDriveMarker];
        
        [self updateDrivePosition:__lsrc animation:YES];
    }
}

-(void)updateDrivePosition:(CLLocationCoordinate2D)lsrc__  {
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    // 드리이브 위치
    if ([Utils isEqualLocation:locMgr.markerDrive.position location:lsrc__] && locMgr.markerDrive.icon != nil) {
        if (locMgr.markerDrive.map != nil) {
            return;
        }
    }
    
    if (locMgr.markerDrive.icon == nil || locMgr.markerDrive.map == nil) {
        [self updateDriveMarker];
        locMgr.markerDrive.map = self.mapView_;
        locMgr.markerDrive.groundAnchor = CGPointMake(0.5, 0.5);
    }
    
    // 차령이동 - 애니메이션
    // Use CATransaction to set a custom duration for this animation. By default, changes to the
    // position are already animated, but with a very short default duration. When the animation is
    // complete, trigger another animation step.
    // 축적에 따라서 속도 계산해야 한다.
    [CATransaction begin];
    //[CATransaction setAnimationDuration:(distance / (50 * 1000))];  // custom duration, 2m/sec 50km/sec 50 * 1000
    [CATransaction setAnimationDuration:3];  // custom duration, 2m/sec 50km/sec 50 * 1000
    
    locMgr.markerDrive.position = lsrc__;
    
    // 차량을 따서서 position을 움직인다.
    if (!locMgr.isMoveMapBYGesture && self->appDelegate.isDriverMode) {
        // 위치로 이동
        CGFloat currentZoom = self.mapView_.camera.zoom;
        GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:lsrc__.latitude
                                                                longitude:lsrc__.longitude
                                                                     zoom:currentZoom];
        
        [self.mapView_ animateToCameraPosition:camera];
    }
    [CATransaction commit];
}

-(void)getRoute:(NSString*)lsrc__ dst:(NSString*)ldst__ {
    static NSString *__ssrc;
    static NSString *__sdst;
    
    // 주기적으로 지도를 업데이트 하지 않고 그려진 지도에서 위치가 벗어나면 요청하도록 변경. -> locationUpdate에서 한다. 여기서 지도 뱡향도 바꾸어야 한다.
    //return;
    // -> matrix에서 거리와 시간을 계산한다.
    
    if ([lsrc__ isEqualToString:@""] || [ldst__ isEqualToString:@""]) return;
    
    CLLocationCoordinate2D __lsrc = [Utils getLocation:lsrc__];
    CLLocationCoordinate2D __ldst = [Utils getLocation:ldst__];
    
    if (![Utils isLocationValid:__lsrc.latitude long:__lsrc.longitude]) {
        return;
    }
    if (![Utils isLocationValid:__ldst.latitude long:__ldst.longitude]) {
        return;
    }
    if ([lsrc__ isEqualToString:ldst__]) {
        return;
    }
    if ([lsrc__ isEqualToString:__ssrc] && [ldst__ isEqualToString:__sdst]) {
        return;
    }
    
    if ([Utils checkStateUIInfo:UI_STATE_TRIP_MATRIX oldvalue:[NSString stringWithFormat:@"%@,%@",lsrc__,ldst__]] == UI_STATE_STEP_RUN) {
        return;
    }
    if ([Utils checkStateUIInfo:UI_STATE_TRIP_MATRIX oldvalue:[NSString stringWithFormat:@"%@,%@",lsrc__,ldst__]] == UI_STATE_STEP_FIN) {
        return;
    }
    [Utils setStateUIInfo:UI_STATE_TRIP_MATRIX value:UI_STATE_STEP_RUN oldvalue:[NSString stringWithFormat:@"%@,%@",lsrc__,ldst__]];
    
    __ssrc = lsrc__;
    __sdst = ldst__;
    
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    [locMgr requestDirection:__lsrc
                      endPos:__ldst
                andWaypoints:kCLLocationCoordinate2DInvalid
                    andParam:nil
                  completion:^(NSDictionary* paramDic, int status) {
                      
                      dispatch_async(dispatch_get_main_queue(), ^{
                          if (status==1) {
                              //NSAssert(![NSThread isMainThread], @"This should be called from the main thread");
                              
                              NSDictionary *legs = [locMgr distanceOffeature];
                              int total_distance = [legs[@"distance"] intValue];
                              int total_duration = [legs[@"duration"] intValue];
                              locMgr.distancematrix = @{
                                                        @"remain_distance":[NSString stringWithFormat:@"%d", total_distance],
                                                        @"remain_duration":[NSString stringWithFormat:@"%d", total_duration]
                                                        };
//                              if (locMgr.distancematrix == nil) {
//
//                              }
//                              else {
//                                  [locMgr.distancematrix setValue:[NSString stringWithFormat:@"%d", total_distance] forKey:@"remain_distance"];
//                                  [locMgr.distancematrix setValue:[NSString stringWithFormat:@"%d", total_duration] forKey:@"remain_duration"];
//                              }
                              
                              //ROUTE_DELETE;
                              //if (!self->appDelegate.isDriverMode && self->appDelegate.ustate <= 110) {
                                  [self print_eta:legs toggle:0 api:YES];
                              //}
                              
                              if ( locMgr.mapType == CMapTypeTmaps) {
                                  [self updateReportApi:@"direction-tmap"];
                              }
                              else if (locMgr.mapType == CMapTypeGoogleMaps) {
                                  [self updateReportApi:@"distance-google"];
                              }
                              
                              // step을 올린다.
                              if (self->appDelegate.isDriverMode) {
                                  if (self->appDelegate.pstate == 230) {
                                      [self onPD211BYDirect];
                                  }
                                  else if (self->appDelegate.pstate == 240) {
                                      [self onPD311BYDirect];
                                  }
                              }
                          }
                          [Utils setStateUIInfo:UI_STATE_TRIP_MATRIX value:UI_STATE_STEP_FIN oldvalue:nil];
                      });
                      
                  }];
//    if (locMgr.isKorea) {
//        [self->googleReqMgr sendTmapDirectionsMatrixByCoordinatesAsync:__lsrc.latitude startLongitude:__lsrc.longitude
//                                                           endLocation:[Utils formattedPosition:__ldst]
//                                                                sensor:YES optional:@"line=N&fit=N"];
//    }
//    else {
//        [self->googleReqMgr sendDirectionsMatrixByCoordinatesAsync:__lsrc.latitude startLongitude:__lsrc.longitude
//                                                       endLocation:[Utils formattedPosition:__ldst]
//                                                            sensor:YES optional:@"line=N&fit=N"];
//
//    }
}

/*
-(void)getRoute:(NSString*)lsrc__ dst:(NSString*)ldst__ {
    static NSString *__ssrc;
    static NSString *__sdst;

    
    // 주기적으로 지도를 업데이트 하지 않고 그려진 지도에서 위치가 벗어나면 요청하도록 변경. -> locationUpdate에서 한다. 여기서 지도 뱡향도 바꾸어야 한다.
    //return;
    // -> matrix에서 거리와 시간을 계산한다.
    
    if ([lsrc__ isEqualToString:@""] || [ldst__ isEqualToString:@""]) return;
    
    CLLocationCoordinate2D __lsrc = [Utils getLocation:lsrc__];
    CLLocationCoordinate2D __ldst = [Utils getLocation:ldst__];
    
    if (![Utils isLocationValid:__lsrc.latitude long:__lsrc.longitude]) {
        return;
    }
    if (![Utils isLocationValid:__ldst.latitude long:__ldst.longitude]) {
        return;
    }
    if ([lsrc__ isEqualToString:ldst__]) {
        return;
    }
    if ([lsrc__ isEqualToString:__ssrc] && [ldst__ isEqualToString:__sdst]) {
        return;
    }
    
    if ([Utils checkStateUIInfo:UI_STATE_TRIP_MATRIX oldvalue:[NSString stringWithFormat:@"%@,%@",lsrc__,ldst__]] == UI_STATE_STEP_RUN) {
        return;
    }
    if ([Utils checkStateUIInfo:UI_STATE_TRIP_MATRIX oldvalue:[NSString stringWithFormat:@"%@,%@",lsrc__,ldst__]] == UI_STATE_STEP_FIN) {
        return;
    }
    [Utils setStateUIInfo:UI_STATE_TRIP_MATRIX value:UI_STATE_STEP_RUN oldvalue:[NSString stringWithFormat:@"%@,%@",lsrc__,ldst__]];
    
    __ssrc = lsrc__;
    __sdst = ldst__;
    if (self->appDelegate.isKorea) {

        [self->googleReqMgr sendTmapDirectionsMatrixByCoordinatesAsync:__lsrc.latitude startLongitude:__lsrc.longitude
                                                           endLocation:[Utils formattedPosition:__ldst]
                                                                sensor:YES optional:@"line=N&fit=N"];
    }
    else {
        [self->googleReqMgr sendDirectionsMatrixByCoordinatesAsync:__lsrc.latitude startLongitude:__lsrc.longitude
                                                       endLocation:[Utils formattedPosition:__ldst]
                                                            sensor:YES optional:@"line=N&fit=N"];
        
    }
}
*/
- (BOOL)drawRoute
{
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    if (self->appDelegate.isDriverMode) {
        if (self->appDelegate.pstate == 230 && self->appDelegate.pstate2 == 10) {
            if (![_dstLocationTextField.text isEqualToString:@""] &&
                locMgr.tripline.map == nil) {
                // 라인이 없으면 trip line 그린다.
                return [self drawRouteOnMAP:[Utils currentPosition]
                                        dst:[Utils srcPosition] param:@"fit=N&line=Y"];
            }
            else {
                return [self drawRouteOnMAP:[Utils currentPosition]
                                        dst:[Utils srcPosition] param:@"line=Y"];
            }
        }
        else if (self->appDelegate.pstate == 240 && self->appDelegate.pstate2 == 10) {
            if (![_dstLocationTextField.text isEqualToString:@""] &&
                locMgr.tripline.map == nil) {
                // trip line 그린다.
                return [self drawRouteOnMAP:[Utils formattedPosition:ldrv]
                                        dst:[Utils destinationPosition] param:@"fit=N&line=Y"];
            }
            else {
                // trip line 그린다.
                return [self drawRouteOnMAP:[Utils formattedPosition:ldrv]
                                        dst:[Utils destinationPosition] param:@"line=Y"];
            }
        }
    }
    else  {
        if (self->appDelegate.ustate <= 110) {
            // trip line 그린다.
            return [self drawRouteOnMAP:[Utils srcPosition]
                                    dst:[Utils destinationPosition] param:@"fit=Y&line=Y&cmd=UQ101"];
        }
    }
    return YES;
}

- (void)drawRoute:(NSString*)param
{
    if (self->appDelegate.isDriverMode) {
        if (self->appDelegate.pstate == 240 && self->appDelegate.pstate2 == 10) {
            // trip line 그린다.
            [self drawRouteOnMAP:[Utils currentPosition]
                             dst:[Utils destinationPosition] param:param];
        }
    }
    else  {
        if (self->appDelegate.ustate <= 110) {
            // trip line 그린다.
            [self drawRouteOnMAP:[Utils srcPosition]
                             dst:[Utils destinationPosition] param:param];
        }
    }
}

-(BOOL)drawRouteOnMAP:(NSString*)lsrc__ dst:(NSString*)ldst__ param:(NSString*)param{
    
    NSLog(@"drawRouteOnMAP:%@/%@",lsrc__,ldst__);
    //NSLog(@"drawRouteOnMAP:%@",ldst__);
    [appDelegate debugActivityState];
    
    if ([lsrc__ isEqualToString:@""] || [ldst__ isEqualToString:@""]) return NO;
    
    CLLocationCoordinate2D __lsrc = [Utils getLocation:lsrc__];
    CLLocationCoordinate2D __ldst = [Utils getLocation:ldst__];
    
    CLLocationDistance distance = GMSGeometryDistance(__lsrc, __ldst);
    // 10m 이내면 조회해지 않는다.
    if (distance <= 10) {
        NSLog(@"drawRouteOnMAP:1:%d",(int)distance);
        return NO;
    }
    if (![GoogleUtils isLocationValid:__lsrc]) {
        NSLog(@"drawRouteOnMAP:2");
        return NO;
    }
    if (![GoogleUtils isLocationValid:__ldst]) {
        NSLog(@"drawRouteOnMAP:3");
        return NO;
    }
    // 출도착이 같으면 조회하지 않는다.
    if ([Utils isEqualLocation:__lsrc location:__ldst]) {
        NSLog(@"drawRouteOnMAP:4");
        return NO;
    }
    //
//    if ([Utils checkStateUIInfo:UI_STATE_TRIP_LINE oldvalue:@"UI_STATE_TRIP_LINE"] == UI_STATE_STEP_RUN) {
//        return NO;
//    }
//    if ([Utils checkStateUIInfo:UI_STATE_TRIP_LINE oldvalue:@"UI_STATE_TRIP_LINE"] == UI_STATE_STEP_FIN) {
//        [Utils sendNotofication:[NSString stringWithFormat:@"drawRouteOnMAP fin error"]];
//        return NO;
//    }
//
//    [Utils setStateUIInfo:UI_STATE_TRIP_LINE value:UI_STATE_STEP_RUN oldvalue:@"UI_STATE_TRIP_LINE"];
    
//    static BOOL isRun = NO;
//
//    if (isRun) {
//        NSLog(@"drawRouteOnMAP:5");
//        return YES;
//    }
//
//    isRun = YES;
    //__weak __typeof(self) weakSelf = self;
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    [locMgr requestDirection:__lsrc
                      endPos:__ldst
                andWaypoints:kCLLocationCoordinate2DInvalid
                    andParam:(NSMutableDictionary*)[GoogleUtils getDataOfQueryString:param]
                  completion:^(NSDictionary* paramDic, int status) {
                      
                      dispatch_async(dispatch_get_main_queue(), ^{
                          NSLog(@"drawRouteOnMAP:9 %d",status);
                          if (status==1) {
                              //NSAssert(![NSThread isMainThread], @"This should be called from the main thread");
                              
                              INTULocationManager *locMgr = [INTULocationManager sharedInstance];
                              BOOL isMapRedraw = [[Utils nullToString:paramDic[@"line"]] isEqualToString:@"Y"];
                              // line은 110, 230, 240일때 그린다.
                              // 110은 라인만 다시 그린다.
                              
                              // 230,240은 경로를 벗어나면 다시 그린다.
                              if (!self->appDelegate.isDriverMode && self->appDelegate.ustate <= 110) {
                                  isMapRedraw = YES;
                              }
                              
                              CLLocationCoordinate2D scoordinate;
                              CLLocationCoordinate2D ecoordinate;
                              
                              // 찍은 주소로 처리됨.
                              scoordinate = [Utils srcPositionCoor];
                              ecoordinate = [Utils destinationPositionCoor];
                              
                              //NSLog(@"drawRouteOnMAP:10 %@",[Utils getLocationByString:scoordinate]);
                              //NSLog(@"drawRouteOnMAP:10 %@",[Utils getLocationByString:ecoordinate]);
                              // 마커 clear
//                              locMgr.markerStart.icon  = [locMgr getMarkerIcon:@"route_start"];
//                              locMgr.markerFinish.icon  = [locMgr getMarkerIcon:@"route_end"];
                              [locMgr clearMarker:locMgr.markerStart];
                              [locMgr clearMarker:locMgr.markerFinish];
                              //NSLog(@"drawRouteOnMAP:10 %@",[Utils getLocationByString:locMgr.markerStart.position]);
                              //NSLog(@"drawRouteOnMAP:10 %@",[Utils getLocationByString:locMgr.markerFinish.position]);
                              
                              // 지도에 표시
                              if (self->appDelegate.isDriverMode && self->appDelegate.pstate == 230 && self->appDelegate.pstate2 == 10 ) {
                                  [locMgr showStartMarker:scoordinate];
                              }
                              else if (self->appDelegate.isDriverMode && self->appDelegate.pstate == 240 && self->appDelegate.pstate2 == 10 ) {
                                  [locMgr showFinishMarker:ecoordinate];
                              }
                              else {
                                  [locMgr showStartMarker:scoordinate];
                                  [locMgr showFinishMarker:ecoordinate];
                              }
                              
                              //NSLog(@"drawRouteOnMAP:10 %@",[Utils getLocationByString:locMgr.markerStart.position]);
                              //NSLog(@"drawRouteOnMAP:10 %@",[Utils getLocationByString:locMgr.markerFinish.position]);
                              [self updateDriveMarker];
                              //NSLog(@"drawRouteOnMAP:10 %@",[Utils getLocationByString:locMgr.markerDrive.position]);
                              //NSLog(@"drawRouteOnMAP:10 %@",[Utils getLocationByString:locMgr.markerPickup.position]);
                              //[locMgr updateDriveMarker:self->appDelegate.location];
                              
                              // 출발점, 도착점
                              // ready 일때 출발지, 목적지의 line을 그린다.
                              BOOL isFit = [[Utils nullToString:paramDic[@"fit"]] isEqualToString:@"Y"];
                              
                              //NSLog(@"drawRouteOnMAP:9 %d/%d/%d",status,isMapRedraw,isFit);
                              
                              //dispatch_async(dispatch_get_main_queue(), ^{
                                  if (isMapRedraw) {
                                      [locMgr drawTripLine];
                                      //[self reDrawTripLine];
                                  }
                                  //[self updateStartFinish:scoordinate finish:ecoordinate fit:isFit];
                                  if (isFit) {
                                      [self updateMapFitBounds:nil];
                                  }
                              //});
                              
                              //ROUTE_DELETE;
                              if (!self->appDelegate.isDriverMode && self->appDelegate.ustate <= 110) {
                                  NSDictionary *dic = [locMgr distanceOffeature];
                                  //dispatch_async(dispatch_get_main_queue(), ^{
                                      [self print_eta:dic toggle:0 api:YES];
                                  //});

                                  // tmap의 distancedㅘ duration을 서버에 전송한다.
                                  NSString *duration = DURATION_TEXT(dic[@"eduration"]);
                                  [self onUQ101:dic[@"edistance"] duration:duration];
                              }
                              
                              if ( locMgr.mapType == CMapTypeTmaps) {
                                  [self updateReportApi:@"direction-tmap"];
                              }
                              else if (locMgr.mapType == CMapTypeGoogleMaps) {
                                  [self updateReportApi:@"direction-google"];
                              }
                              
                              // step을 올린다.
                              if (self->appDelegate.isDriverMode && isMapRedraw) {
                                  if (self->appDelegate.pstate == 230) {
                                      [self onPD211BYDirect];
                                  }
                                  else if (self->appDelegate.pstate == 240) {
                                      [self onPD311BYDirect];
                                  }
                              }
                              
//                              BOOL isUQ101 = [[Utils nullToString:paramDic[@"cmd"]] isEqualToString:@"UQ101"];
//                              if (isUQ101) {
//                                  driver_step[@"edistance"]); // mile, km 5.5km
//                                  NSString *durt = DURATION_TEXT(driver_step[@"eduration"])
//                                  [self onUQ101:];
//                              }
                          }
                          
                          [Utils setStateUIInfo:UI_STATE_TRIP_LINE value:UI_STATE_STEP_FIN oldvalue:@""];
                          
                          [appDelegate updateActivityState:self];
                          [appDelegate debugActivityState];
                          [self onUpdateUI];
                          [self onUpdateUI:@{@"result1":@"data1"}];
                      });
                      
                  }];
    
    return YES;
}
/*
-(void)reDrawTripLine
{
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    if ([locMgr.paths count]) {
        stepsIndex = 0;
        
        [self pushCpos:btnCpos];
        [self animateToBearing:self->appDelegate.location.coordinate];
    }
}
*/
#pragma mark - mapMoveTimer
- (void)startMapMoveTimer {
    // 타이머 셋팅 시작 - 개별 선택된 셀의 타이머가 따로 작동한다
    [self stopMapMoveTimer];
    
    mapMoveTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(mapMovetimerMethod:) userInfo:nil repeats:YES];
    self->mapMoveTime = [NSDate date]; // 현재시간 보관
    
//#ifdef _DEBUG_ACTIVITY
//    NSString *data = [NSString stringWithFormat:@"startMapMoveTimer"];
//    [self performSelectorOnMainThread:@selector(debugNotification:) withObject:data waitUntilDone:false];
//#endif
}

- (void)updateMapMoveTimer {
    if (mapMoveTimer != nil) {
        self->mapMoveTime = [NSDate date]; // 현재시간 보관
//#ifdef _DEBUG_ACTIVITY
//        NSString *data = [NSString stringWithFormat:@"updateMapMoveTimer"];
//        [self performSelectorOnMainThread:@selector(debugNotification:) withObject:data waitUntilDone:false];
//#endif
    }
    else {
        [self startMapMoveTimer];
    }
}


- (void)stopMapMoveTimer {
    if (mapMoveTimer != nil) {
        [mapMoveTimer invalidate];
        mapMoveTimer = nil;
        
        //TXButton* btnCpos = [self.mapView_ viewWithTag:801];
        [btnCpos setTitle:@"" forState:UIControlStateNormal];
//#ifdef _DEBUG_ACTIVITY
//        NSString *data = [NSString stringWithFormat:@"stopMapMoveTimer"];
//        [self performSelectorOnMainThread:@selector(debugNotification:) withObject:data waitUntilDone:false];
//#endif
    }
    self->mapMoveTime = [NSDate date]; // 현재시간 보관
    

}

-(void)mapMovetimerMethod:(NSTimer *)timer
{
    //TXButton* btnCpos = [self.mapView_ viewWithTag:801];
    
    if ([btnCpos.currentBackgroundImage isEqual:[UIImage imageNamed:@"btn_location_on"]]) {
        //
        [self stopMapMoveTimer];
        return;
    }
    
    NSDate *methodFinish = [NSDate date];
    
    NSTimeInterval executionTime = [methodFinish timeIntervalSinceDate:self->mapMoveTime];
    NSLog(@"executionTime = %f, %d, %@", executionTime, (int)(executionTime/60),self->mapMoveTime);
//#ifdef _DEBUG_ACTIVITY
//    NSString *data = [NSString stringWithFormat:@"mapMove %.1f/%d/%ld",self->appDelegate.location.speed,locMgr.isMoveMapBYGesture,(long)executionTime];
//    [self performSelectorOnMainThread:@selector(debugNotification:) withObject:data waitUntilDone:false];
//#endif
    if (executionTime > 0) {
        [btnCpos setTitle:[NSString stringWithFormat:@"%.0f",(_MAP_TOUCH_MOVE_TIME_OUT-executionTime)] forState:UIControlStateNormal];
    }
    else {
        [btnCpos setTitle:@"" forState:UIControlStateNormal];
    }
    if (executionTime >= _MAP_TOUCH_MOVE_TIME_OUT) {
        
        if (self->appDelegate.isDriverMode && self->appDelegate.pstate == 220 && self->appDelegate.pstate2 == 10) {
            return;
        }
//        CLLocationCoordinate2D coord = [Utils getLocation:[Utils currentPosition]];
//        [self.mapView_ animateToLocation:coord];
        [self pushCpos:btnCpos];
        [self stopMapMoveTimer];
        
        INTULocationManager *locMgr = [INTULocationManager sharedInstance];
        locMgr.isMoveMapBYGesture = NO;
        [self updateCposImage:YES];
        [btnCpos setTitle:@"" forState:UIControlStateNormal];
        if (!self->appDelegate.isKorea) {
            self.mapView_.trafficEnabled = NO;
        }
        
    }
}

#pragma mark - Button - CurrentPosition

-(void)showCposButton {
    
    //TXButton* btnCpos = [self.mapView_ viewWithTag:801];
    if (btnCpos != nil) {
        return;
    }
    
#ifdef _DRIVER_MODE
    
    btnCpos = [[TXButton alloc]
               initWithFrame:CGRectMake(self.mapView_.bounds.size.width - kBasicMargin - 54,
                                        kBasicMargin,
                                        54, 54)];
    
#else
    
#ifdef _WITZM
    btnCpos = [[TXButton alloc]
               initWithFrame:CGRectMake(self.mapView_.bounds.size.width - kBasicMargin - 54,
                                        kBasicMargin,
                                        54, 54)];
#elif defined _CHACHA
    btnCpos = [[TXButton alloc]
               initWithFrame:CGRectMake(0,
                                        0,
                                        54, 54)];
#endif
    
#endif
    
    //[btnNavi setBackgroundColor:colorFromRGB(102, 0, 102, 1)];
    [btnCpos setTitleColor:UIColorDefault2 forState:UIControlStateNormal];
    [btnCpos addTarget:self action:@selector(pushCpos:) forControlEvents:UIControlEventTouchUpInside];
    [btnCpos setTag:801];
    btnCpos.hidden = YES;
    
    [self.mapView_ addSubview:btnCpos];
    
    CLLocationCoordinate2D cloc = [Utils getLocation:[Utils currentPosition]];
    CLLocationCoordinate2D coor = [self.mapView_.projection coordinateForPoint:self.mapView_.center];
    
    if (cloc.longitude == coor.latitude && cloc.longitude == coor.longitude) {
        [self updateCposImage:YES];
    }
    else {
        [self updateCposImage:NO];
    }
}

-(void)moveCposButton {
    return;
    
}

-(void)updateCposImage:(BOOL)isMyLoc
{
    //TXButton* btnCpos = [self.mapView_ viewWithTag:801];
//#ifndef _DRIVER_MODE
//    TXButton* btnCpos2 = [_vDriveStatus viewWithTag:801];
//    if (isMyLoc) {
//        [btnCpos2 setImage:[UIImage imageNamed:@"btn_location_on"] forState:UIControlStateNormal];
//    }
//    else {
//        [btnCpos2 setImage:[UIImage imageNamed:@"btn_location"] forState:UIControlStateNormal];
////        if (self->appDelegate.isDriverMode && self->appDelegate.pstate == 240 && self->appDelegate.pstate2 == 10) {
////            [self setTimerMoveToCurrentPosition:10];
////        }
//    }
//#endif
    if (isMyLoc) {
        [btnCpos setTitle:@"" forState:UIControlStateNormal];
        
//        [btnCpos setImage:[UIImage imageNamed:@"btn_location_on"] forState:UIControlStateNormal];
        
        [btnCpos setBackgroundImage:[UIImage imageNamed:@"btn_location_on"] forState:UIControlStateNormal];
        
        INTULocationManager *locMgr = [INTULocationManager sharedInstance];
        locMgr.isMoveMapBYGesture = NO;
    }
    else {
        [btnCpos setTitle:@"" forState:UIControlStateNormal];
        
//        [btnCpos setImage:[UIImage imageNamed:@"btn_location"] forState:UIControlStateNormal];
        
        [btnCpos setBackgroundImage:[UIImage imageNamed:@"btn_location"] forState:UIControlStateNormal];
//        if (self->appDelegate.isDriverMode && self->appDelegate.pstate == 240 && self->appDelegate.pstate2 == 10) {
//            [self setTimerMoveToCurrentPosition:10];
//        }
    }
}

#pragma mark Rider API Call

-(void)onPP110Auto {
    
    if ([Utils checkStateUIInfo:UI_STATE_PP110 oldvalue:@"PP110"] == UI_STATE_STEP_RUN) {
        return;
    }
    [Utils setStateUIInfo:UI_STATE_PP110 value:UI_STATE_STEP_RUN oldvalue:@"PP110"];
    
    user.utelno = [settings getUserTelno];
    user.atoken = [[[TXApp instance] getSettings] getUserToken];
    NSLog(@"didBecomeActive:PP110");
    
    // uistate 초기화, 화면을 다시 그리기 위해서
    //self->appDelegate.uistate = 0;
    //self->appDelegate.uistate2 = 0;
    [self showBusyIndicator:@"Authenticating driver ... "];
    [self->model PP110Auto:user];
}

-(void)onPP110 {
    
    if ([Utils checkStateUIInfo:UI_STATE_PP110 oldvalue:@"PP110"] == UI_STATE_STEP_RUN) {
        return;
    }
    [Utils setStateUIInfo:UI_STATE_PP110 value:UI_STATE_STEP_RUN oldvalue:@"PP110"];
    
    self->appDelegate.imgVehicleDriver = nil;
    self->appDelegate.imgProfileRider = nil;
    
    //[self startLocationUpdateSubscription];
    // 주소초기화
    //    _srcLocationTextField.text = @"";
    _dstLocationTextField.text = @"";
    //_routeCancel.hidden = YES;
    lsrc = kCLLocationCoordinate2DInvalid;
    ldst = kCLLocationCoordinate2DInvalid;
    
    [Utils updateSrcPosition:LOCATION_STR(lsrc.latitude) lng:LOCATION_STR(lsrc.longitude)];
    [Utils updateDstPosition:LOCATION_STR(ldst.latitude) lng:LOCATION_STR(ldst.longitude)];
    
    [self showBusyIndicator:@"Authenticating driver ... "];
    [self->model PP110:user];
}


-(void)onPD015 {

    self->appDelegate.isMyCancel = NO;
    NSString *pseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.PSEQ];
    NSString *vseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.VSEQ];
    
    NSDictionary *propertyMap = @{
                                  @"pseq" : pseq,
                                  @"vseq" : vseq,
                                  };
    // where want to go
    if ([GoogleUtils isLocationValid:ldst]) {
        propertyMap = @{
                        @"ldst" : [Utils formattedPosition:ldst],
                        @"vseq" : vseq,
                        };
    }
    
    [self showBusyIndicator:@"Setting Vehicle ... "];
    [self->model PD015:propertyMap];
}

-(void)onPD035 {
    self->appDelegate.isMyCancel = NO;
    //selectedVechicleInfo = @"";
    //[[[TXApp instance] getSettings] setFDKeychain:@"selectedVechicleInfo" value:@""];
    NSString *pseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.PSEQ];
    NSString *vseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.VSEQ];
    
    NSDictionary *propertyMap = @{
                                  @"pseq" : pseq,
                                  @"vseq" : vseq,
                                  };
    
    [self showBusyIndicator:@"Offline Vehicle ... "];
    [self->model PD035:propertyMap];
}

-(void)onPP116 {

    self->appDelegate.isMyCancel = NO;
    NSString *pseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.PSEQ];
    
    NSDictionary *dic = @{
                          @"pseq" : pseq,
                          };
    
    // 상태 초기화
    [appDelegate resetState];
    //[[[TXApp instance] getSettings] setDriveMode:DRIVE_MODE_TRAINING];
    [self showBusyIndicator:@"Setting Vehicle ... "];
    [self->model requsetAPI:API_FUNC(NSStringFromSelector(_cmd)) property:dic];
}

-(void)onPP136 {
    [self cancelAllLoop];
    [self cancelTimeout];
    
    // 상태 초기화
    [appDelegate resetState];
    self->appDelegate.isMyCancel = NO;
    //selectedVechicleInfo = @"";
    //[[[TXApp instance] getSettings] setFDKeychain:@"selectedVechicleInfo" value:@""];
    NSString *pseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.PSEQ];
    
    NSDictionary *dic = @{
                          @"pseq" : pseq,
                          };
    
    //[[[TXApp instance] getSettings] setDriveMode:DRIVE_MODE_DRIVING];
    [self showBusyIndicator:@"Offline Vehicle ... "];
    [self->model requsetAPI:API_FUNC(NSStringFromSelector(_cmd)) property:dic];
}

-(void)onPP114 {
    
    [self showBusyIndicator:@"Requesting LogOut ... "];
    [self->model PP114];
}

-(void)onPQ002 {
    
    if (self->appDelegate.isDriverMode && self->appDelegate.pstate != 200) {
        
        return;
    }
    if (self->appDelegate.isDriverMode && self->appDelegate.isBecomeMode) {
        
        return;
    }
    
    if (self->appDelegate.isDriverMode && self->appDelegate.pstate == 200) {
        
        if([UIApplication sharedApplication].applicationState != UIApplicationStateActive) {
            [self callLoop:@"PQ002" withCancel:YES andDirect:NO];
            return;
        }
    }
    
    ////[self showBusyIndicator:@"Inquery Driver ... "];
    [self->model PQ002];
}

-(void)onPQ100 {
    
    if (self->appDelegate.isDriverMode && self->appDelegate.pstate == 200) {
        [self callLoop:@"PQ100" withCancel:YES andDirect:NO];
        return;
    }
    
    
    if ([Utils checkStateUIInfo:UI_STATE_PQ100 oldvalue:@"PQ100"] == UI_STATE_STEP_RUN) {
        return;
    }
    [Utils setStateUIInfo:UI_STATE_PQ100 value:UI_STATE_STEP_RUN oldvalue:@"PQ100"];
    
    ////[self showBusyIndicator:@"Inquery Driver ... "];
    [self->model PQ100];
    
}

-(void)onPQ100Direct {
    
    if ([Utils checkStateUIInfo:UI_STATE_PQ100 oldvalue:@"PQ100"] == UI_STATE_STEP_RUN) {
        return;
    }
    [Utils setStateUIInfo:UI_STATE_PQ100 value:UI_STATE_STEP_RUN oldvalue:@"PQ100"];
    
    ////[self showBusyIndicator:@"Inquery Driver ... "];
    [self->model PQ100];
    
}

-(void)onPD130:(NSString*)rsncode {
    if (self->appDelegate.isDriverMode && self->appDelegate.ustate == 220 && self->appDelegate.ustate2 == 20) {
        return;
    }
    if (self->appDelegate.isDriverMode && self->appDelegate.ustate == 230 && self->appDelegate.ustate2 == 10) {
        return;
    }
    
    
    /***/
    isRunTimeout_ = NO;
    
    etTime = -99;
    // timer kill
    [animationTimer invalidate];
    animationTimer = nil;
    [self.view.layer removeAllAnimations];
    [appDelegate stopSound];
    
    //self.mapView_.myLocationEnabled = YES;
    [self cancelTimeout];
    [self cancelAllLoop];
    
    [self cancelTimeout];
    [self showBusyIndicator:@"Canceling Order ... "];
    [self->model PD130:rsncode];
}

-(void)onPD131 {
    if (self->appDelegate.isDriverMode && self->appDelegate.ustate == 220 && self->appDelegate.ustate2 == 20) {
        return;
    }
    if (self->appDelegate.isDriverMode && self->appDelegate.ustate == 230 && self->appDelegate.ustate2 == 10) {
        return;
    }
    
    if ([Utils checkStateUIInfo:UI_STATE_PD131 oldvalue:@"PD131"] == UI_STATE_STEP_RUN) {
        return;
    }
    [Utils setStateUIInfo:UI_STATE_PD131 value:UI_STATE_STEP_RUN oldvalue:@"PD131"];
    
    [self cancelTimeout];
    isRunTimeout_ = NO;
    //[self showBusyIndicator:@"Inquery Driver ... "];
    [self->model PD131];
}

-(void)onPD220 {
    
    /***/
    isRunTimeout_ = NO;
    
    etTime = -99;
    [appDelegate stopSound];
    
    //self.mapView_.myLocationEnabled = YES;
    [self cancelTimeout];
    [self cancelAllLoop];
    
    /***/
    [self PD220Delay];
}

-(void)PD220Delay {
    NSString* pseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.PSEQD];
    NSString* oseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.OSEQ];
    
    // pickup 시작
    NSString* _ldst = [Utils currentPosition];
    
    NSLog(@"trace1114");
    NSDictionary *estimate = [self getDistanceNDuration:_ldst];
    [self showBusyIndicator:@"Picking Up ... "];
    
    NSDictionary *propertyMap = @{
                                  @"pseq" : pseq,
                                  @"oseq" : oseq,
                                  @"eduration" : estimate[@"eduration"],
                                  @"edistance" : estimate[@"edistance"]
                                  };
    
    //                [self showBusyIndicator:@"Picking Up ... "];
    [self->model PD220:propertyMap];
}

-(void)onPD211 {
    static NSString* clat = @"";
    static NSString* clng = @"";
    
    // manually gps location update
    [self didUpdateLocations];
    
    if ([Utils checkStateUIInfo:UI_STATE_PD211 oldvalue:@"PD211"] == UI_STATE_STEP_RUN) {
        return;
    }
    
    NSLog(@"trace1115");
    // 남은거리,남은시간
    NSDictionary *estimate = [self getDistanceNDuration:[Utils srcPosition]];
    if ([estimate[@"eduration"] intValue] == 0 && [estimate[@"edistance"] intValue] == 0) {
        [Utils setStateUIInfo:UI_STATE_PQ200 value:UI_STATE_STEP_FIN oldvalue:nil];
        [self callLoop:@"PQ200" withCancel:NO andDirect:YES];
        NSLog(@"trace1117");
        return;
    }
    [Utils setStateUIInfo:UI_STATE_PD211 value:UI_STATE_STEP_RUN oldvalue:@"PD211"];
    NSLog(@"trace1116");
    NSString* plat = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.LAT];
    NSString* plng = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.LNG];
    
    // step 은 시작마다 그냥 올리도록 수정함.
    //    if (![Utils isLocationValid:[plat doubleValue] long:[plng doubleValue]]) {
    //        return;
    //    }
    //    if ([clat isEqualToString:plat] && [clng isEqualToString:plng]) {
    //        return;
    //    }
    clat = plat;
    clng = plng;
    
    NSString* pseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.PSEQD];
    NSString* oseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.OSEQ];
    
    NSDictionary *propertyMap = @{
                                  @"pseq" : pseq,
                                  @"oseq" : oseq,
                                  @"tseq" : [GoogleUtils nowTimestamp],
                                  @"navi" : [self getReportApi],
                                  @"eduration" : estimate[@"eduration"],
                                  @"edistance" : estimate[@"edistance"]
                                  };
    
    self->_lastReportTime = [[NSDate date] timeIntervalSince1970];
    
    ////[self showBusyIndicator:@"Inquery Driver ... "];
    [self->model PD211:propertyMap];
    [self initReportApi];
}

-(void)onPD211BYDirect {
    
    if ([Utils checkStateUIInfo:UI_STATE_PD211 oldvalue:@"PD211"] == UI_STATE_STEP_RUN) {
        return;
    }
    
    NSLog(@"trace1116");
    // 남은거리,남은시간
    NSDictionary *estimate = [self getDistanceNDuration:[Utils srcPosition]];
    if ([estimate[@"eduration"] intValue] == 0 && [estimate[@"edistance"] intValue] == 0) {
        [self callLoop:@"PQ200" withCancel:NO andDirect:YES];
        return;
    }
    [Utils setStateUIInfo:UI_STATE_PD211 value:UI_STATE_STEP_RUN oldvalue:@"PD211"];
    
    // manually gps location update
    [self didUpdateLocations];
    
    //    NSString* plat = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.LAT];
    //    NSString* plng = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.LNG];
    
    // step 은 시작마다 그냥 올리도록 수정함.
    //    if (![Utils isLocationValid:[plat doubleValue] long:[plng doubleValue]]) {
    //        return;
    //    }
    
    NSString* pseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.PSEQD];
    NSString* oseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.OSEQ];
    
    NSDictionary *propertyMap = @{
                                  @"pseq" : pseq,
                                  @"oseq" : oseq,
                                  @"tseq" : [GoogleUtils nowTimestamp],
                                  @"navi" : [self getReportApi],
                                  @"eduration" : estimate[@"eduration"],
                                  @"edistance" : estimate[@"edistance"]
                                  };
    
    self->_lastReportTime = [[NSDate date] timeIntervalSince1970];
    //[self showBusyIndicator:@"Inquery Driver ... "];
    [self->model PD211:propertyMap];
    [self initReportApi];
}

-(void)onPQ200 {
    
    // manually gps location update
    [self didUpdateLocations];
    
    // 상태가 맞지 않으면 이전 요청을 한다.
    if (self->appDelegate.isDriverMode && self->appDelegate.pstate < 230) {
        [self callLoop:@"PQ100" withCancel:YES andDirect:NO];
        return;
    }

    // step중에는 처리하지 않는다.
    if (self->appDelegate.isDriverMode && (self->appDelegate.pstate == 230 || self->appDelegate.pstate == 240) && self->appDelegate.pstate2 == 10) {
        // step정보가 존재하면 처리하지 않는다.
        INTULocationManager *locMgr = [INTULocationManager sharedInstance];
        if (locMgr.distancematrix != nil) {
            [self callLoop:@"PQ200" withCancel:NO andDirect:NO];
            //[self callLoop:@"PD211" withCancel:NO andDirect:YES];
            return;
        }
    }
    
    // cancel되면 하지않는다.
    if (self->appDelegate.isDriverMode && (self->appDelegate.pstate == 230 || self->appDelegate.pstate == 240) && self->appDelegate.pstate2 <= -10) {
        return;
    }

    if ([Utils checkStateUIInfo:UI_STATE_PQ200 oldvalue:@"PQ200"] == UI_STATE_STEP_RUN) {
        return;
    }
    [Utils setStateUIInfo:UI_STATE_PQ200 value:UI_STATE_STEP_RUN oldvalue:@"PQ200"];

    ////[self showBusyIndicator:@"Inquery Driver ... "];
    [self->model PQ200];
}

-(void)onPQ200Direct {
    
    // manually gps location update
    [self didUpdateLocations];
    
    if ([Utils checkStateUIInfo:UI_STATE_PQ200 oldvalue:@"PQ200"] == UI_STATE_STEP_RUN) {
        return;
    }
    [Utils setStateUIInfo:UI_STATE_PQ200 value:UI_STATE_STEP_RUN oldvalue:@"PQ200"];
    
    ////[self showBusyIndicator:@"Inquery Driver ... "];
    [self->model PQ200];
}

-(void)onPQ300 {
    
    // manually gps location update
    [self didUpdateLocations];
    
    // 상태가 맞지 않으면 이전 요청을 한다.
    if (self->appDelegate.isDriverMode && self->appDelegate.pstate < 230) {
        [self callLoop:@"PQ100" withCancel:YES andDirect:NO];
        return;
    }
    
    // step중에는 처리하지 않는다.
    if (self->appDelegate.isDriverMode && (self->appDelegate.pstate == 230 || self->appDelegate.pstate == 240) && self->appDelegate.pstate2 == 10) {
        //
        [self callLoop:@"PQ300" withCancel:NO andDirect:NO];
        //        [self callLoop:@"PD311" withCancel:NO andDirect:YES];
        return;
    }
    // cancel되면 하지않는다.
    if (self->appDelegate.isDriverMode && (self->appDelegate.pstate == 230 || self->appDelegate.pstate == 240) && self->appDelegate.pstate2 <= -10) {
        return;
    }
    // 240.20에는 조회하지 않는다.
    if (self->appDelegate.isDriverMode && self->appDelegate.pstate == 240 && self->appDelegate.pstate2 == 20) {
        //
        [self callLoop:@"PQ300" withCancel:NO andDirect:NO];
        //        [self callLoop:@"PD311" withCancel:NO andDirect:YES];
        return;
    }
    
    if ([Utils checkStateUIInfo:UI_STATE_PQ300 oldvalue:@"PQ300"] == UI_STATE_STEP_RUN) {
        return;
    }
    [Utils setStateUIInfo:UI_STATE_PQ300 value:UI_STATE_STEP_RUN oldvalue:@"PQ300"];
    
    ////[self showBusyIndicator:@"Inquery Driver ... "];
    [self->model PQ300];
    
}

-(void)onPQ300Direct {
    
    // manually gps location update
    [self didUpdateLocations];
    
    if ([Utils checkStateUIInfo:UI_STATE_PQ300 oldvalue:@"PQ300"] == UI_STATE_STEP_RUN) {
        return;
    }
    [Utils setStateUIInfo:UI_STATE_PQ300 value:UI_STATE_STEP_RUN oldvalue:@"PQ300"];
    
    ////[self showBusyIndicator:@"Inquery Driver ... "];
    [self->model PQ300];
    
}

-(void)onPD213 {
    
    [self showBusyIndicator:@"Pickup Arrived ... "];
    [self->model PD213];
}

-(void)onPD232:(NSString*)rsncode {
    
    [self showBusyIndicator:@"Canceling Pickup ... "];
    [self->model PD232:rsncode];
}

-(void)onPD320 {
    
    // 작업이 오래 걸리는 API를 백그라운드 스레드에서 실행한다.
    NSString* pseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.PSEQD];
    NSString* oseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.OSEQ];
    
    // trip 시작
//    NSString* dlat = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.DLAT];
//    NSString* dlng = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.DLNG];
//    NSString* _ldst = [NSString stringWithFormat:@"%@,%@",dlat,dlng];
    
    // trip 진행시 차량의 위치를 출발지로 한다.
    NSDictionary *dic = [self->appDelegate.dicDriver objectForKey:@"provider_device"];
    if ([Utils isDictionary:dic]) {
        ldrv = [Utils getLocation:[Utils nullTolocation:[dic objectForKey:@"location"]]];
    }
    
    NSDictionary *estimate = [self getDistanceNDuration:[Utils destinationPosition]];
    
    NSDictionary *propertyMap = @{
                                  @"pseq" : pseq,
                                  @"oseq" : oseq,
                                  //                                  @"tseq" : [GoogleUtils nowTimestamp],
                                  @"eduration" : estimate[@"eduration"],
                                  @"edistance" : estimate[@"edistance"]
                                  };
    
    [self cancelAllLoop];
    [self cancelTimeout];
    
    //            [self showBusyIndicator:@"Start Trip ... "];
    [self showBusyIndicator:@"Start Trip ... "];
    [self->model PD320:propertyMap];
}

-(void)onPD310 {
    
    NSString* pseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.PSEQD];
    NSString* oseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.OSEQ];
    
    NSString* dlat = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.DLAT];
    NSString* dlng = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.DLNG];
    
    NSString* _ldst = [NSString stringWithFormat:@"%@,%@",dlat,dlng];
    
    NSDictionary *propertyMap = @{
                                  @"pseq" : pseq,
                                  @"oseq" : oseq,
                                  @"ldst" : _ldst,
                                  };
    
    //[self showBusyIndicator:@"Inquery Driver ... "];
    [self->model PD310:propertyMap];
}

-(void)onPD311 {
    static NSString* clat = @"";
    static NSString* clng = @"";
    
    // manually gps location update
    [self didUpdateLocations];
    
    if ([Utils checkStateUIInfo:UI_STATE_PD311 oldvalue:@"PD311"] == UI_STATE_STEP_RUN) {
        return;
    }
    // 남은거리,남은시간
    NSDictionary *estimate = [self getDistanceNDuration:[Utils destinationPosition]];
    if ([estimate[@"eduration"] intValue] == 0 && [estimate[@"edistance"] intValue] == 0) {
        [Utils setStateUIInfo:UI_STATE_PQ300 value:UI_STATE_STEP_FIN oldvalue:nil];
        [self callLoop:@"PQ300" withCancel:NO andDirect:YES];
        return;
    }
    [Utils setStateUIInfo:UI_STATE_PD311 value:UI_STATE_STEP_RUN oldvalue:@"PD311"];
    
    NSString* plat = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.LAT];
    NSString* plng = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.LNG];
    
    // step 은 시작마다 그냥 올리도록 수정함.
    //    if (![Utils isLocationValid:[plat doubleValue] long:[plng doubleValue]]) {
    //        return;
    //    }
    //    if ([clat isEqualToString:plat] && [clng isEqualToString:plng]) {
    //        return;
    //    }
    clat = plat;
    clng = plng;
    
    NSString* pseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.PSEQD];
    NSString* oseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.OSEQ];
    
    NSDictionary *propertyMap = @{
                                  @"pseq" : pseq,
                                  @"oseq" : oseq,
                                  @"tseq" : [GoogleUtils nowTimestamp],
                                  @"navi" : [self getReportApi],
                                  @"eduration" : estimate[@"eduration"],
                                  @"edistance" : estimate[@"edistance"]
                                  };
    
    self->_lastReportTime = [[NSDate date] timeIntervalSince1970];
    
    ////[self showBusyIndicator:@"Inquery Driver ... "];
    [self->model PD311:propertyMap];
    [self initReportApi];
}

-(void)onPD311BYDirect {
    // manually gps location update
    [self didUpdateLocations];
    
    //    NSString* plat = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.LAT];
    //    NSString* plng = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.LNG];
    
    // step 은 시작마다 그냥 올리도록 수정함.
    //    if (![Utils isLocationValid:[plat doubleValue] long:[plng doubleValue]]) {
    //        return;
    //    }
    
    NSString* pseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.PSEQD];
    NSString* oseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.OSEQ];
    
    // 남은거리,남은시간
    NSDictionary *estimate = [self getDistanceNDuration:[Utils destinationPosition]];
    if ([estimate[@"eduration"] intValue] == 0 && [estimate[@"edistance"] intValue] == 0) {
        [Utils setStateUIInfo:UI_STATE_PQ300 value:UI_STATE_STEP_FIN oldvalue:nil];
        [self callLoop:@"PQ300" withCancel:NO andDirect:YES];
        return;
    }
    
    NSDictionary *propertyMap = @{
                                  @"pseq" : pseq,
                                  @"oseq" : oseq,
                                  @"tseq" : [GoogleUtils nowTimestamp],
                                  @"navi" : [self getReportApi],
                                  @"eduration" : estimate[@"eduration"],
                                  @"edistance" : estimate[@"edistance"]
                                  };
    
    self->_lastReportTime = [[NSDate date] timeIntervalSince1970];
    ////[self showBusyIndicator:@"Inquery Driver ... "];
    [self->model PD311:propertyMap];
    [self initReportApi];
}

-(BOOL)onPD313 {
    
    [self showBusyIndicator:@"End Trip ... "];
    if ([self->model PD313]) {
        return YES;
    }
    else {
        [self hideBusyIndicator];
        return NO;
    }
}

-(void)onPD330:(NSString*)rsncode {
    
    [self showBusyIndicator:@"Canceling Trip ... "];
    [self->model PD330:rsncode];
    
}

-(void)pushPD150 {
    
    if (!isRunTimeout_) {

        // 220.10 PD110하지 않으면 timeout처리
        [self cancelTimeout];
        [self callLoop:@"PQ100" withCancel:YES andDirect:YES];
        //        [self callLoop:@"PD131" withCancel:NO andDirect:NO];
        isRunTimeout_ = YES;
    }
    
}

-(void)pushPD151 {
    
    // 목적지 업데이트 __DEBUG
    [self cancelTimeout];
    [self callLoop:@"PQ100" withCancel:YES andDirect:YES];
}

// 주문취소
-(void)pushPD152 {
    
//    [self isChangedCancelAlert:self->appDelegate.pstate];
    
    [self cancelTimeout];
    [self callLoop:@"PQ100" withCancel:YES andDirect:YES];
}

-(void)pushPD251 {
    
    // 목적지 업데이트 __DEBUG
    [self cancelTimeout];
    [self callLoop:@"PQ200" withCancel:YES andDirect:YES];
}

-(void)pushPD252 {
    
    [self cancelTimeout];
    [self callLoop:@"PQ100" withCancel:YES andDirect:YES];
}

-(void)pushPD351 {
    
    // 목적지 업데이트 __DEBUG
    [self cancelTimeout];
    [self callLoop:@"PQ300" withCancel:YES andDirect:YES];
}

-(void)onPP400 {
    
    NSString* pseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.PSEQ];
    
    NSDictionary *_propertyMap = @{
                                   @"pseq" : pseq
                                   };
    
    [self showBusyIndicator:@"Loading info ... "];
    
    [self->model requsetAPI:API_FUNC(NSStringFromSelector(_cmd)) property:_propertyMap];
}

#pragma mark Rider API Call

-(void)onUD120 {
#ifndef _DRIVER_MODE
    if ([Utils checkStateUIInfo:UI_STATE_UD120 oldvalue:@"UD120"] == UI_STATE_STEP_RUN) {
        return;
    }
    
    NSString *code = @""; //[[self->appDelegate.dicRider valueForKey:@"user"] valueForKey:@"code"];
    NSString *cseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.CSEQ];
    //NSString *lat = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.LAT];
    //NSString *lng = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.LNG];
    NSString *uname = @""; // input.text
    NSString *fare_rate = @"";
    NSString *lsrcLoc = @"";
    NSString *ldstLoc = @"";
    
    promoCode = [[[TXApp instance] getSettings] getFDKeychain:@"promoCode"];//
    if (![promoCode isEqualToString:@""]) {
        code = promoCode;
    }
    if (![uName isEqualToString:@""]) {
        uname = uName;
    }
    
    if ([[Utils nullToString:cseq] isEqualToString:@""]) {
        // 카드가 없으면 요청불가
        [self alertError:@"" message:LocalizedStr(@"Map.Payment.Require.alert.text")];
        return;
    }
    
    if ([serviceType isEqualToString:@""]) {
        // 차량선택이 없으면 주문할 수 없다.
        [self alertError:@"" message:LocalizedStr(@"Map.Address.Vehicle.Not.text")];
        return;
    }
    NSString *estkey = [NSString stringWithFormat:@"estimate_%@",[serviceType lowercaseString]];
    fare_rate = [[self->appDelegate.drive_estimates objectForKey:estkey] objectForKey:@"fare_rate"];
    
    if (![_srcLocationTextField.text isEqualToString:@""]) {
        lsrcLoc = [Utils formattedPosition:lsrc];
        [Utils updateSrcPosition:LOCATION_STR(lsrc.latitude) lng:LOCATION_STR(lsrc.longitude)];
    }
    else {
        // 출발지가 없으면 주문할 수 없다.
        [self alertError:@"" message:LocalizedStr(@"Map.Address.SRC.Not.text")];
        return;
    }
    
    if (![GoogleUtils isLocationValid:lsrc]) {
        // 출발지가 없으면 주문할 수 없다.
        [self alertError:@"" message:LocalizedStr(@"Map.Address.SRC.Not.text")];
        return;
    }
    
    if (![_dstLocationTextField.text isEqualToString:@""]) {
        ldstLoc = [Utils formattedPosition:ldst];
        [Utils updateDstPosition:LOCATION_STR(ldst.latitude) lng:LOCATION_STR(ldst.longitude)];
    }
    else {
        // 도착지가 없으면 주문할 수 없다.
        [self alertError:@"" message:LocalizedStr(@"Map.Address.DST.Not.text")];
        return;
    }
    
    if (![GoogleUtils isLocationValid:ldst]) {
        // 도착지가 없으면 주문할 수 없다.
        //[self alertError:@"" message:LocalizedStr(@"Map.Address.DST.Not.text")];
        LGAlertView *alertView = [[LGAlertView alloc] initWithTitle:nil
                                                            message:LocalizedStr(@"Map.Address.DST.Not.text")
                                                              style:LGAlertViewStyleAlert
                                                       buttonTitles:@[LocalizedStr(@"Button.OK")]
                                                  cancelButtonTitle:nil
                                             destructiveButtonTitle:nil
                                                      actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                                                          [self selectDestAddress:nil];
                                                      }
                                                      cancelHandler:^(LGAlertView *alertView) {
                                                      }
                                                 destructiveHandler:^(LGAlertView *alertView) {
                                                 }];
        [Utils initAlertButtonColor:alertView];
        [alertView showAnimated:YES completionHandler:nil];
        return;
    }
    
    //[self cancelAllLoop];
    //[self cancelTimeout];
    
    NSDictionary *propertyMap = @{
                                  @"service" : serviceType,
                                  @"uname" : uname,
                                  @"cseq" : cseq,
                                  @"code" : code,
                                  @"fare_rate" : fare_rate,
                                  @"lsrc" : lsrcLoc,
                                  @"ldst" : ldstLoc,
                                  };
    
    //    promoCode = @"";
    //    uName = @"";
    
    //[[[TXApp instance] getSettings] setFDKeychain:@"promoCode" value:promoCode];
    //[[[TXApp instance] getSettings] setFDKeychain:@"serviceType" value:serviceType];
    
    [Utils setStateUIInfo:UI_STATE_UD120 value:UI_STATE_STEP_RUN oldvalue:@"UD120"];
    
    [self showBusyIndicator:@"Requesting Cue ... "];
    [self->model UD120:propertyMap];
    
    // 취소되어도 맵을 다시 그리지 않는다.
    [Utils setStateUIInfo:UI_STATE_TRIP_LINE value:UI_STATE_STEP_FIN oldvalue:@""];
#endif
}

-(void)onUR110Auto {
    
    if ([Utils checkStateUIInfo:UI_STATE_UR110 oldvalue:@"UR110"] == UI_STATE_STEP_RUN) {
        return;
    }
    [Utils setStateUIInfo:UI_STATE_UR110 value:UI_STATE_STEP_RUN oldvalue:@"UR110"];
    
    //[self removeBlackLayer];
    
    user.utelno = [settings getUserTelno];
    user.atoken = [[[TXApp instance] getSettings] getUserToken];
    
    // uistate 초기화, 화면을 다시 그리기 위해서
    //self->appDelegate.uistate = 0;
    //self->appDelegate.uistate2 = 0;
    [self showBusyIndicator:@"Authenticating ... "];
    [self->model UR110Auto:user];
}

-(void)onUR110 {
    
    if ([Utils checkStateUIInfo:UI_STATE_UR110 oldvalue:@"UR110"] == UI_STATE_STEP_RUN) {
        return;
    }
    [Utils setStateUIInfo:UI_STATE_UR110 value:UI_STATE_STEP_RUN oldvalue:@"UR110"];
    
    //[self cancelRequestMonitoring:nil];
    //[self startSingleLocationRequestAndMoveCamera];
    //    self.mapView_.myLocationEnabled = NO;
    
    //[self removeBlackLayer];
    self->appDelegate.imgVehicleDriver = nil;
    NSString *userUid   = [settings getUserId];
    NSString *userPwd = [settings getPassword];
    
    user.uid = userUid;
    user.pwd = userPwd;
    
    //[self initAddressTextField];
    //[self initTripView];
    
    [self showBusyIndicator:@"Authenticating ... "];
    [self->model UR110:user];
}

-(void)onUQ101 {
    
    NSString* useq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.USEQ];
    
    NSString *__lsrc = [Utils formattedPosition:lsrc];
    NSString *__ldst = [Utils formattedPosition:ldst];
    
    //NSDictionary *user = [self->appDelegate.dicRider objectForKey:@"user"];
    
    NSDictionary *propertyMap = @{
                                  @"useq" : useq,
                                  @"lsrc" : __lsrc,
                                  @"ldst" : __ldst,
                                  };
    
    [self showBusyIndicator:@"Inquery Driver ... "];
    [self->model UQ101:propertyMap];
}

-(void)onUQ101:(NSString*)trip_distance duration:(NSString*)trip_duration {
    
    NSString* useq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.USEQ];
    
    NSString *__lsrc = [Utils formattedPosition:lsrc];
    NSString *__ldst = [Utils formattedPosition:ldst];
    
    //NSDictionary *user = [self->appDelegate.dicRider objectForKey:@"user"];
    
    NSDictionary *propertyMap = @{
                                  @"useq" : useq,
                                  @"lsrc" : __lsrc,
                                  @"ldst" : __ldst,
                                  @"trip_distance" : trip_distance,
                                  @"trip_duration" : trip_duration,
                                  };
    
    [self showBusyIndicator:@"Inquery Driver ... "];
    [self->model UQ101:propertyMap];
}

-(void)onUD130 {
    
    self->appDelegate.isMyCancel = YES;
    [self showBusyIndicator:@"Canceling Cue ... "];
    [self->model UD130];
}

-(void)onUQ002 {
    
    //[self showBusyIndicator:@"Inquery Driver ... "];
    [self->model UQ002];
    
}

-(void)onUQ100 {
    
    //[self showBusyIndicator:@"Inquery Driver ... "];
    [self->model UQ100];
}

-(void)onUD131 {
    
    return;
    
    //    if ([Utils checkStateUIInfo:UI_STATE_UD131 oldvalue:@"UD131"] == UI_STATE_STEP_RUN) {
    //        return;
    //    }
    //    [Utils setStateUIInfo:UI_STATE_UD131 value:UI_STATE_STEP_RUN oldvalue:@"UD131"];
    //
    //    self->appDelegate.isMyCancel = YES;
    //    [self showBusyIndicator:@"Canceling Cue ... timeout"];
    //    [self->model UD131];
}

-(void)onUQ200 {
    
    ////[self showBusyIndicator:@"Inquery Driver ... "];
    [self->model UQ200];
    
}

-(void)onUD110 {
    
    //[self showBusyIndicator:@"Inquery Driver ... "];
    [self->model UD110];
}


-(void)onUD210 {
    
    //[self showBusyIndicator:@"Inquery Driver ... "];
    [self->model UD210];
}

-(void)onUD230 {
    self->appDelegate.isMyCancel = YES;
    [self showBusyIndicator:@"Canceling Cue ... no trip"];
    [self->model UD230];
}

-(void)onUQ300 {
    
    // 140.20에는 조회하지 않는다.
    if (!self->appDelegate.isDriverMode && ((self->appDelegate.ustate == 140 && self->appDelegate.ustate2 == 20) || (self->appDelegate.ustate == 130 && self->appDelegate.ustate2 <= -10) || (self->appDelegate.ustate == 140 && self->appDelegate.ustate2 <= -10))) {
        return;
    }
    ////[self showBusyIndicator:@"Inquery Driver ... "];
    [self->model UQ300];
    
}

-(void)onUD310 {
    
    //[self showBusyIndicator:@"Inquery Driver ... "];
    [self->model UD310];
}

-(void)onUR303 {
    
    // 3. call api
    [self showBusyIndicator:@"Requesting ... "];
    [self->model requsetAPI:@"UR303" property:nil];

}

-(void)pushUD150 {
    
    [self cancelTimeout];
    
    [self onUR602]; // driver profile
    [self callLoop:@"UQ002" withCancel:YES andDirect:YES];
}

-(void)pushUD151 {
    
    [self cancelTimeout];
    [self callLoop:@"UQ002" withCancel:YES andDirect:YES];
}

-(void)pushUD250 {
    
    [self cancelTimeout];
    [self callLoop:@"UQ200" withCancel:YES andDirect:YES];
}

-(void)pushUD253 {
    
    [self cancelTimeout];
    [self callLoop:@"UQ200" withCancel:YES andDirect:YES];
}

-(void)pushUD252 {
    
    [self cancelTimeout];
    [self callLoop:@"UQ002" withCancel:YES andDirect:YES];
}

-(void)pushUD254 {
    
    [self cancelTimeout];
    [self callLoop:@"UQ002" withCancel:YES andDirect:YES];
}

-(void)pushUD350 {
    
    [self cancelTimeout];
    [self callLoop:@"UQ300" withCancel:YES andDirect:YES];
}

-(void)pushUD352 {
    
    // 목적지 업데이트 __DEBUG
    [self cancelTimeout];
    [self callLoop:@"UQ300" withCancel:YES andDirect:YES];
    
}

-(void)pushUD353 {
    
    [self cancelTimeout];
    
}

-(void)pushUD354 {
    
    [self cancelTimeout];
    [self callLoop:@"UQ002" withCancel:YES andDirect:YES];
}

#pragma mark - IBAction

#ifdef _DRIVER_MODE

-(IBAction)onMusic:(id)sender {
    
    UIButton *btn = (UIButton*)sender;
    
    if (self->appDelegate.player == nil) {
        SpotifyLoginController *vc = [[SpotifyLoginController alloc] init];
        [self.navigationController pushViewController:vc animated:YES];
        
        return;
    }
    
    if (btn.tag == 1) {
        
        SpotifyViewController *vc = [[SpotifyViewController alloc] init];
        [self.navigationController pushViewController:vc animated:YES];
        
    }
    else if (btn.tag == 10) {
        
        // pause상태, play로 변한다.
        if (self->appDelegate.player.playbackState.isPlaying)
        {
            //pause로 바꾼다.
            [self->appDelegate.player setIsPlaying:NO callback:nil];
            [self didChangeUISpotify:NO];
        }
        else{
            //play로 바꾼다.
            [self->appDelegate.player setIsPlaying:YES callback:nil];
            [self didChangeUISpotify:YES];
        }
    } else if (btn.tag == 2) {
        if (self->appDelegate.player.metadata.nextTrack) {
            // next
            [self->appDelegate.player skipNext:nil];
            [self didChangeUISpotify:YES];
        }
        else {
            // 처음으로 돌아간다.
            
        }
    }
}
#endif

-(IBAction)onCall:(id)sender {
    
    NSString* acall = @"";
    if (self->appDelegate.isDriverMode) {
        acall = [Utils acall:self->appDelegate.dicDriver];
    }
    else {
        acall = [Utils acall:self->appDelegate.dicRider];
    }
    
    if ([acall isEqualToString:@"no"]) {
        if (self->appDelegate.isDriverMode)
            [self alertError:@"Error" message:@"Failed Call to Rider"];
        else
            [self alertError:@"Error" message:@"Failed Call to Driver"];
    }
    else if (![acall isEqualToString:@""] && acall != nil) {
        //        NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt://%@",acall]];
        //        if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        //            [[UIApplication sharedApplication] openURL:phoneUrl];
        //        }
        //
        NSURL *phoneUrl = [NSURL URLWithString:[@"telprompt://" stringByAppendingString:acall]];
        NSURL *phoneFallbackUrl = [NSURL URLWithString:[@"tel://" stringByAppendingString:acall]];
        
        if ([UIApplication.sharedApplication canOpenURL:phoneUrl]) {
            [UIApplication.sharedApplication openURL:phoneUrl];
        } else if ([UIApplication.sharedApplication canOpenURL:phoneFallbackUrl]) {
            [UIApplication.sharedApplication openURL:phoneFallbackUrl];
        } else
        {
            [self alertError:@"Error" message:@"Call facility is not available!!!"];
        }
    }
    else
        [self alertError:@"Error" message:@"Call facility is not available!!!"];
}



-(IBAction)onSMS:(id)sender {
    
    NSString* acall = @"";
    if (self->appDelegate.isDriverMode) {
        acall = [Utils acall:self->appDelegate.dicDriver];
    }
    else {
        acall = [Utils acall:self->appDelegate.dicRider];
    }
    
    if ([acall isEqualToString:@"no"]) {
        if (self->appDelegate.isDriverMode)
            [self alertError:@"Error" message:@"Failed Call to Rider"];
        else
            [self alertError:@"Error" message:@"Failed Call to Driver"];
    }
    else if (![acall isEqualToString:@""] && acall != nil) {
        //        NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"sms://%@",acall]];
        //        if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        //            [[UIApplication sharedApplication] openURL:phoneUrl];
        //        }
        
        NSURL *phoneUrl = [NSURL URLWithString:[@"sms://" stringByAppendingString:acall]];
        NSURL *phoneFallbackUrl = [NSURL URLWithString:[@"sms://" stringByAppendingString:acall]];
        
        if ([UIApplication.sharedApplication canOpenURL:phoneUrl]) {
            [UIApplication.sharedApplication openURL:phoneUrl];
        } else if ([UIApplication.sharedApplication canOpenURL:phoneFallbackUrl]) {
            [UIApplication.sharedApplication openURL:phoneFallbackUrl];
        } else
        {
            [self alertError:@"Error" message:@"Sms facility is not available!!!"];
        }
    }
    else
        [self alertError:@"Error" message:@"Sms facility is not available!!!"];
}

-(IBAction)onCancel {
    
    //self->appDelegate.isMyCancel = YES;
    if (self->appDelegate.isDriverMode) {
        //
        if (self->appDelegate.pstate == 230 || self->appDelegate.pstate == 240) {
            if (vcCancel == nil) {
                vcCancel = [[TXCancelVC alloc] initWithNibName:@"TXCancelVC" bundle:nil step:1 state:self->appDelegate.pstate];
                vcCancel.view.frame = self.view.frame;
                [self pushViewController:vcCancel];
            }
        }
    }
    else {
        if (self->appDelegate.ustate == 130) {
            if (vcCancel == nil) {
                vcCancel = [[TXCancelVC alloc] initWithNibName:@"TXCancelVC" bundle:nil step:1 state:self->appDelegate.ustate];
                vcCancel.view.frame = self.view.frame;
                [self pushViewController:vcCancel];
            }
        }
    }
}

// Add a UIButton in Interface Builder to call this function
/*
 - (IBAction)getCurrentPlace:(UIButton *)sender {
 [_placesClient currentPlaceWithCallback:^(GMSPlaceLikelihoodList *placeLikelihoodList, NSError *error){
 if (error != nil) {
 NSLog(@"Pick Place error %@", [error localizedDescription]);
 return;
 }
 
 if (placeLikelihoodList != nil) {
 GMSPlace *place = [[[placeLikelihoodList likelihoods] firstObject] place];
 if (place != nil) {
 NSLog(@"place.name:%@",place.name);
 NSLog(@"address:%@",[[place.formattedAddress componentsSeparatedByString:@", "]
 componentsJoinedByString:@"\n"]);
 }
 }
 else {
 NSLog(@"No place selected");
 }
 }];
 [self performSelector:@selector(getCurrentPlace:) withObject:@{@"param1":@"param1"} afterDelay:10];
 }
 */

-(NSInteger)getZoomByState {
    NSInteger zoom = kZoomLevel110;
    
    if (self->appDelegate.isDriverMode) {
        if (self->appDelegate.pstate == 200) {
            zoom = kZoomLevel200;
        }
        else if (self->appDelegate.pstate == 210) {
            zoom = kZoomLevel210;
        }
        else if (self->appDelegate.pstate == 230 && self->appDelegate.pstate2 == 10) {
            zoom = kZoomLevel23010;
        }
        else if (self->appDelegate.pstate == 230 && self->appDelegate.pstate2 == 20) {
            zoom = kZoomLevel23020;
        }
        else if (self->appDelegate.pstate == 240 && self->appDelegate.pstate2 == 10) {
            zoom = kZoomLevel24010;
        }
        else if (self->appDelegate.pstate == 240 && self->appDelegate.pstate2 == 20) {
            zoom = kZoomLevel24020;
        }
        else {
            zoom = kZoomLevel200;
        }
    }
    else {
        if (self->appDelegate.ustate == 100) {
            zoom = kZoomLevel100;
        }
        else if (self->appDelegate.ustate == 110) {
            zoom = kZoomLevel110;
        }
        else if (self->appDelegate.ustate == 130 && self->appDelegate.ustate2 == 10) {
            zoom = kZoomLevel13010;
        }
        else if (self->appDelegate.ustate == 130 && self->appDelegate.ustate2 == 20) {
            zoom = kZoomLevel13020;
        }
        else if (self->appDelegate.ustate == 140 && self->appDelegate.ustate2 == 10) {
            zoom = kZoomLevel14010;
        }
        else if (self->appDelegate.ustate == 140 && self->appDelegate.ustate2 == 20) {
            zoom = kZoomLevel14020;
        }
        else {
            zoom = kZoomLevel200;
        }
    }
    
    return zoom;
}

-(void)resetLocationNPosition
{
//    NSInteger zoom = [self getZoomByState];
//    // camera
//    //CGFloat currentZoom = self.mapView_.camera.zoom;
//    CLLocationCoordinate2D position = [Utils currentPositionCoor];
//    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:position.latitude
//                                                            longitude:position.longitude
//                                                                 zoom:zoom];
//    
//    [self.mapView_ animateToCameraPosition:camera];
//    //self.mapView_.camera = camera;
    
    [self pushCpos:btnCpos];
    
    [self hideBusyIndicator];
}

/* google zoom 
 1: 세계
 5: 대륙
 10: 도시
 15: 거리
 20: 건물
 */
-(IBAction)pushCpos:(id)sender {
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    NSLog(@"GMSCameraPosition1:%.6f,%.6f",locMgr.currentLocation.coordinate.latitude,locMgr.currentLocation.coordinate.longitude);
    NSLog(@"GMSCameraPosition2:%.6f,%.6f",locMgr.realTimeCLocation.coordinate.latitude,locMgr.realTimeCLocation.coordinate.longitude);
    NSLog(@"GMSCameraPosition3:%.6f,%.6f",self->appDelegate.location.coordinate.latitude,self->appDelegate.location.coordinate.longitude);
    if (self->appDelegate.isDriverMode) {
    }
    else {
        if (self->appDelegate.ustate <= 110 && locMgr.tripline.map != nil) {
            [self updateMapFitBounds:nil];
            return;
        }
        // 운행중일때 차량과 목적지 fit
        else if (self->appDelegate.ustate == 140 && self->appDelegate.ustate2 == 10) {
            
            if ([Utils isLocationValid:[Utils destinationPositionCoor]]) {
                [self updateVehicleFitBounds];
                return;
            }
        }
        // 픽업중일때 차량과 출발지
        else if (self->appDelegate.ustate == 130) {
        //else if (self->appDelegate.ustate == 130 && self->appDelegate.ustate2 == 10) {
            
            if ([Utils isLocationValid:[Utils srcPositionCoor]]) {
                [self updateVehicleFitBounds];
                return;
            }
        }
    }
    
    if (!self->appDelegate.isDriverMode && self->appDelegate.ustate == 140) {
        // 라이더 pickup, trip중
        // 차량의 위치로 간다.
        [self.mapView_ animateToLocation:locMgr.markerDrive.position];
    }
    else {
        //CLLocation *location = self.mapView_.myLocation;
        CLLocation *location = self->appDelegate.location;

        NSLog(@"GMSCameraPosition:%.6f,%.6f",location.coordinate.latitude,location.coordinate.longitude);
#ifndef _DRIVER_MODE
        if (!self->appDelegate.isDriverMode && self->appDelegate.ustate <= 110 && ![_srcLocationTextField.text isEqualToString:@""] && [_dstLocationTextField.text isEqualToString:@""]) {
            if (sender != nil) {
//                // 픽업지 주소 엡데이트
//                //[self getAddressFromLatLon:location.coordinate];
//                [self updateStartPosition:[Utils getLocationByString:location.coordinate]];
//
                //src marker를 현재 위치로 이동
                [Utils updateSrcPosition:LOCATION_STR(location.coordinate.latitude) lng:LOCATION_STR(location.coordinate.longitude)];
                NSLog(@"coordinate:%f,%f",location.coordinate.latitude,location.coordinate.longitude);
                [self.mapView_ animateToLocation:location.coordinate];
                
                //self->appDelegate.location = location;
                NSString *lat = LOCATION_STR(location.coordinate.latitude);
                NSString *lng = LOCATION_STR(location.coordinate.longitude);
                [Utils updateCurrentPosition:lat lng:lng];
            }
            else {
                if ([Utils isLocationValid:lsrc]) {
                    NSLog(@"coordinate:%f,%f",lsrc.latitude,lsrc.longitude);
                    [self.mapView_ animateToLocation:lsrc];
                }
                else {
//                    // 픽업지 주소 엡데이트
//                    //[self getAddressFromLatLon:location.coordinate];
//                    [self updateStartPosition:[Utils getLocationByString:location.coordinate]];
                    
                    //src marker를 현재 위치로 이동
                    [Utils updateSrcPosition:LOCATION_STR(location.coordinate.latitude) lng:LOCATION_STR(location.coordinate.longitude)];
                    NSLog(@"coordinate:%f,%f",location.coordinate.latitude,location.coordinate.longitude);
                    [self.mapView_ animateToLocation:location.coordinate];
                    
                    //self->appDelegate.location = location;
                    NSString *lat = LOCATION_STR(location.coordinate.latitude);
                    NSString *lng = LOCATION_STR(location.coordinate.longitude);
                    [Utils updateCurrentPosition:lat lng:lng];
                }
            }
        }
        else
#endif

            if (location) {
                //            self->appDelegate.location = location;
                
/*
                if (!self->appDelegate.isDriverMode && self->appDelegate.ustate == 110 && sender != nil) {
                    //src marker 이동
                    [Utils updateSrcPosition:LOCATION_STR(location.coordinate.latitude) lng:LOCATION_STR(location.coordinate.longitude)];
                }
*/
                [CATransaction begin];
                [CATransaction setAnimationDuration:0.5];
                
                // 운행중일때는 zoom을 처리한다.
                GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:location.coordinate.latitude
                                                                        longitude:location.coordinate.longitude
                                                                             zoom:[self getZoomByState]
                                                                          bearing:self.mapView_.camera.bearing
                                                                     viewingAngle:0];
                
                [self.mapView_ animateToCameraPosition:camera];
                
                [CATransaction commit];
                
                if (self->appDelegate.isDriverMode && (self->appDelegate.pstate == 230 || self->appDelegate.pstate == 240) && self->appDelegate.pstate2 == 10) {
                    [self stopMapMoveTimer];
                }
                
                //self->appDelegate.location = location;
                NSString *lat = LOCATION_STR(location.coordinate.latitude);
                NSString *lng = LOCATION_STR(location.coordinate.longitude);
                [Utils updateCurrentPosition:lat lng:lng];
            }
    }
    [self updateCposImage:YES];
//#endif
}

#ifdef _DRIVER_MODE
-(BOOL)isInstalledTMAP {
    NSString *__mapName = LocalizedStr(@"Menu.Settings.Driver.Map.List1.text");
    CMMapApp mapApp = CMMapAppTmap;
    
    BOOL installed = [CMMapLauncher isMapAppInstalled:mapApp];
    if (installed) {
        [[[TXApp instance] getSettings] setFDKeychain:@"MAP" value:LocalizedStr(@"Menu.Settings.Driver.Map.List1.text")];
        return YES;
    }
    else {
        [[[TXApp instance] getSettings] setFDKeychain:@"MAP" value:@""];
        LGAlertView *alertView = [[LGAlertView alloc] initWithTitle:nil
                                                            message:[NSString stringWithFormat:@"%@ %@",__mapName, LocalizedStr(@"Menu.Settings.Driver.Map.NotExist.text")]
                                                              style:LGAlertViewStyleAlert
                                                       buttonTitles:@[LocalizedStr(@"Button.OK")]
                                                  cancelButtonTitle:LocalizedStr(@"Button.CANCEL")
                                             destructiveButtonTitle:nil
                                                      actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                                                          //TMAPSTORE_URL
                                                          [[UIApplication sharedApplication] openURL: [NSURL URLWithString:TMAPSTORE_URL]];
                                                      }
                                                      cancelHandler:^(LGAlertView *alertView) {
                                                      }
                                                 destructiveHandler:^(LGAlertView *alertView) {
                                                 }];
        [Utils initAlertButtonColor:alertView];
        [alertView showAnimated:YES completionHandler:nil];
    }
    
    return NO;
}
#endif

-(IBAction)pushNavi {
#ifdef _DRIVER_MODE
    /*
     Apple Maps — CMMapAppAppleMaps
     Citymapper — CMMapAppCitymapper
     Google Maps — CMMapAppGoogleMaps
     Navigon — CMMapAppNavigon
     The Transit App — CMMapAppTheTransitApp
     Waze — CMMapAppWaze
     Yandex Navigator — CMMapAppYandex
     CMMapAppTmap,           // Tmap Navigator
     */
    
    
#ifdef _WITZM
    if (![self isInstalledTMAP]) {
        return;
    }
#elif defined _CHACHA
    if (![self isInstalledTMAP]) {
        return;
    }
#endif
    
    NSString *mapName = [[[TXApp instance] getSettings] getFDKeychain:@"MAP"];
    CMMapApp mapApp = CMMapAppNone;
    if ([mapName isEqualToString:LocalizedStr(@"Menu.Settings.Driver.Map.List3.text")]) {
        mapApp = CMMapAppAppleMaps;
    }
    else if ([mapName isEqualToString:LocalizedStr(@"Menu.Settings.Driver.Map.List2.text")]) {
        mapApp = CMMapAppWaze;
    }
    else {
        if (self->appDelegate.isKorea) {
            if (![[Utils nullToString:mapName] isEqualToString:@""]) {
                mapApp = CMMapAppTmap;
            }
        }
        else {
            if (![[Utils nullToString:mapName] isEqualToString:@""]) {
                mapApp = CMMapAppGoogleMaps;
            }
        }
    }
    if (mapApp == CMMapAppNone) {
        [self onClickNavi];
        return;
    }
    BOOL installed = [CMMapLauncher isMapAppInstalled:mapApp];
    if (installed) {
        NSDictionary *__user = [self->appDelegate.dicDriver objectForKey:@"user"];
        NSString *mapDescName = [Utils nameOfuname:[__user objectForKey:@"name"]];
        NSString *mapDestAddr = @"";
        
        CLLocationCoordinate2D coordinate;
        
        if (self->appDelegate.pstate < 240) {
            coordinate = [Utils srcPositionCoor];
            mapDestAddr = _dstLocationTextField.text;
        }
        else {
            coordinate = [Utils destinationPositionCoor];
            mapDestAddr = _dstLocationTextField.text;
        }
        
        if (![GoogleUtils isLocationValid:coordinate]) {
            LGAlertView *alertView = [[LGAlertView alloc] initWithTitle:LocalizedStr(@"Map.Address.DST.Not.text")
                                                                message:nil
                                                                  style:LGAlertViewStyleAlert
                                                           buttonTitles:@[LocalizedStr(@"Button.OK")]
                                                      cancelButtonTitle:nil
                                                 destructiveButtonTitle:nil
                                                          actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                                                          }
                                                          cancelHandler:^(LGAlertView *alertView) {
                                                          }
                                                     destructiveHandler:^(LGAlertView *alertView) {
                                                     }];
            [Utils initAlertButtonColor:alertView];
            [alertView showAnimated:YES completionHandler:nil];
            
            
            return;
        }
        
        if (mapApp == CMMapAppTmap) {
#ifdef _WITZM
            //[TMapTapi invokeRoute:mapDestAddr coordinate:coordinate];
            [CMMapLauncher launchMapApp:mapApp
                        forDirectionsTo:[CMMapPoint mapPointWithAddress:mapDestAddr
                                                             coordinate:coordinate]];
#elif defined _CHACHA
            //[TMapTapi invokeRoute:mapDestAddr coordinate:coordinate];
            [CMMapLauncher launchMapApp:mapApp
                        forDirectionsTo:[CMMapPoint mapPointWithAddress:mapDestAddr
                                                             coordinate:coordinate]];
#endif
        }
        else if (mapApp == CMMapAppGoogleMaps) {
            [CMMapLauncher launchMapApp:mapApp
                        forDirectionsTo:[CMMapPoint mapPointWithCoordinate:coordinate]];
        }
        else if (mapApp == CMMapAppTmap) {
            [CMMapLauncher launchMapApp:mapApp
                        forDirectionsTo:[CMMapPoint mapPointWithAddress:mapDestAddr
                                                             coordinate:coordinate]];
        }
        else {
            [CMMapLauncher launchMapApp:mapApp
                        forDirectionsTo:[CMMapPoint mapPointWithName:mapDescName
                                                          coordinate:coordinate]];
        }
        
        return;
    }
    else {
        LGAlertView *alertView = [[LGAlertView alloc] initWithTitle:nil
                                                            message:[NSString stringWithFormat:@"%@ %@",mapName, LocalizedStr(@"Menu.Settings.Driver.Map.NotExist.text")]
                                                              style:LGAlertViewStyleAlert
                                                       buttonTitles:@[LocalizedStr(@"Button.OK")]
                                                  cancelButtonTitle:nil
                                             destructiveButtonTitle:nil
                                                      actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                                                      }
                                                      cancelHandler:^(LGAlertView *alertView) {
                                                      }
                                                 destructiveHandler:^(LGAlertView *alertView) {
                                                 }];
        [Utils initAlertButtonColor:alertView];
        [alertView showAnimated:YES completionHandler:nil];
    }
#endif
}

- (void)onClickNavi
{
    dataArray = (NSMutableArray*)@[LocalizedStr(@"Menu.Settings.Driver.Map.List1.text"),
                                   LocalizedStr(@"Menu.Settings.Driver.Map.List2.text"),
                                   LocalizedStr(@"Menu.Settings.Driver.Map.List3.text")];
    
    UITableView* mTableView = [[UITableView alloc] init];
    mTableView.delegate=self;
    mTableView.dataSource=self;
    
    mTableView.allowsMultipleSelection = NO;
    mTableView.frame = CGRectMake(0.f, 0.f, 320, 124.f);
    mTableView.contentMode = UIViewContentModeScaleAspectFit;
    mTableView.scrollEnabled = NO;
    
    LGAlertView *alertView = [[LGAlertView alloc] initWithViewAndTitle:LocalizedStr(@"Menu.Settings.Driver.title1.text1")
                                                               message:nil
                                                                 style:LGAlertViewStyleAlert
                                                                  view:mTableView
                                                          buttonTitles:@[LocalizedStr(@"Button.OK")]
                                                     cancelButtonTitle:LocalizedStr(@"Button.CANCEL")
                                                destructiveButtonTitle:nil
                                                         actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                                                             NSLog(@"actionHandler, %@, %lu", title, (long unsigned)index);
                                                             
                                                             for(NSIndexPath* indexPath in [mTableView indexPathsForSelectedRows]) {
                                                                 NSLog(@"cell.tag:%@",[dataArray objectAtIndex:indexPath.row]);
                                                                 
                                                                 //[propertyMap setValue:dataArray[indexPath.row] forKey:@"gender"];
                                                                 //tf.text = dataArray[indexPath.row];
                                                                 
                                                                 [[[TXApp instance] getSettings] setFDKeychain:@"MAP" value:[dataArray objectAtIndex:indexPath.row]];
                                                                 
                                                                 [self pushNavi];
                                                             }
                                                         }
                                                         cancelHandler:^(LGAlertView *alertView) {
                                                             NSLog(@"cancelHandler");
                                                         }
                                                    destructiveHandler:^(LGAlertView *alertView) {
                                                        NSLog(@"destructiveHandler");
                                                    }];
    //alertView.heightMax = 256.f;
    //alertView.heightMax = 156.f;
    //alertView.cancelButtonBackgroundColor = [UIColor redColor];
    [Utils initAlertButtonColor:alertView];
    alertView.windowLevel = LGAlertViewWindowLevelAboveStatusBar;
    [alertView showAnimated:YES completionHandler:nil];
    
}

-(IBAction)onNaviButtonClick:(id)sender {
    UIButton *btn = (UIButton*)sender;
    NSLog(@"onNaviButtonClick:%ld",(long)btn.tag);
    if (btn.tag == 1101 || btn.tag == 2100) {
        [(MenuNavigationController *)self.navigationController performSelector:@selector(showMenu)];
        
        //[[SlideNavigationController sharedInstance] toggleLeftMenu];
        
        //[self pushViewController:menuVC];
    }
    // < confirmation
    else if (btn.tag == 1100 || btn.tag == 1109) {
        //[self downVehicleView];
        //[self hideVehicleView];
#ifdef _DRIVER_MODE
        
#else
        if (!self->appDelegate.isDriverMode) {
            // back 키를 눌러 차량요청을 취소했다.
            if (self->appDelegate.ustate <= 110) {
                // 맵을 다시 그리기 위해 상태를 바꿔준다
                [self initAddressDst];
                self->appDelegate.ustate2 = -2;
                [self updteOrderSelect];
                [self onUpdateUI];
            }
            else {
                // rider cancel
                if (self->appDelegate.ustate < 120) {
                    return;
                }
                
                // 정말 취소하시겠습니까?
                [self onCancel];
            }
        }
#endif
    }
    else if (btn.tag == 1300 || btn.tag == 1301 || btn.tag == 2400) {
        
        if (self->appDelegate.isDriverMode) {
            if (self->appDelegate.pstate < 220) {
                return;
            }
            // __DEBUG
            if (self->appDelegate.pstate == 220 && self->appDelegate.pstate2 == 10) {
                [self onCancel];
                //[self onPD130];
                return;
            }
            else if (self->appDelegate.pstate == 220 && self->appDelegate.pstate2 == 20) {
                [self onCancel];
                //[self onPD130];
                return;
            }
            else if (self->appDelegate.pstate == 230 && self->appDelegate.pstate2 == 20) {
                // 도착시간예정시각 7분을 지나면 상대방 사진이 아니고 cancel 버튼이다.
                //                if (self->_isEtTimerTimeout == YES) {
                //                    [self onCancel];
                //                    //[self onPD232];
                //                    return;
                //                }
                //            if (도착시간예정시각 7분을 지나면) {
                //                [self onPD232];
                //                return;
                //            }
            }
            
            [self openChatView];
            
            //            TXProfileVC *mvc = [[TXProfileVC alloc] initWithNibName:@"TXProfileVC" bundle:nil];
            //            [self pushViewController:mvc];
        }
        else {
            if (self->appDelegate.ustate < 120) {
                return;
            }
            
            if (self->appDelegate.ustate == 120) {
                // 정말 취소하시겠습니까?
                [self onCancel];
                //[self onUD130];
                return;
            }
            else if (self->appDelegate.ustate == 130 && self->appDelegate.ustate2 == 10) {
                // driver 사진 __DEBUG
                [self onUR602];
            }
            else if (self->appDelegate.ustate == 130 && self->appDelegate.ustate2 == 20) {
                //            if (7분을 지나면) { // __DEBUG
                //                [self onUD230];
                //                return;
                //            }
            }
            
            if (!self->appDelegate.isDriverMode && self->appDelegate.ustate > 120) {
                [self openChatView];
            }
            
            //            TXProfileVC *mvc = [[TXProfileVC alloc] initWithNibName:@"TXProfileVC" bundle:nil];
            //            [self pushViewController:mvc];
        }
        
    }
    else if (btn.tag == 1400) {
        if (!self->appDelegate.isDriverMode && self->appDelegate.ustate > 120) {
            //            TXProfileVC *mvc = [[TXProfileVC alloc] initWithNibName:@"TXProfileVC" bundle:nil];
            //            [self pushViewController:mvc];
            
            [self openChatView];
        }
    }
    else if (btn.tag == 1410) {
        // 이력 공유
#ifndef _DRIVER_MODE
        if (!self->appDelegate.isDriverMode && self->appDelegate.ustate > 120) {
            //[self openPeoplePicker];
            //            [self sendShareTrip:@"" first:@"" middle:@"" lastName:@""];
            TXShareTripVC *mvc = [[TXShareTripVC alloc] initWithNibName:@"TXShareTripVC" bundle:nil];
            [self pushViewController:mvc];
        }
#endif
    }
    else if (btn.tag == 1900) {
        // QnA
        //TXNewsVC *mvc = [[TXNewsVC alloc] initWithNibName:@"TXNewsVC" bundle:nil];
        TXQnaVC *mvc = [[TXQnaVC alloc] initWithNibName:@"TXQnaVC" bundle:nil];
        [self pushViewController:mvc];
    }
    else if (btn.tag == 3000) {
        // prefer
        TXPreferVC *mvc = [[TXPreferVC alloc] initWithNibName:@"TXPreferVC" bundle:nil];
        [self pushViewController:mvc];
    }
#ifdef _DRIVER_MODE
    else if (btn.tag == 2500) {
        // vehicle close
        UIView *blockView = [self.mapView_ viewWithTag:90];
        [self releaseLockIt];
        
        [UIView animateWithDuration:0.5
                         animations:^{
                             CGRect frame = _vVehicle.frame;
                             frame.origin.y = self.mapView_.bounds.size.height + frame.size.height;
                             _vVehicle.frame = frame;
                             
                             blockView.backgroundColor = HEXCOLOR(0x00000000);
                         }
                         completion:^(BOOL finished) {
                             [blockView removeFromSuperview];
                         }];
        
    }
    else if (btn.tag == 2501) {
        NSArray *provider_vehicle = [self->appDelegate.dicDriver objectForKey:@"provider_vehicles"];
        if (![Utils isArray:provider_vehicle]) {
            provider_vehicle = vehicleList;
        }
        
        NSDictionary* key = nil;
        for (UIView *aView in _vVehicleSelectView.subviews) {
            UIButton *button = [aView viewWithTag:11];
            if (button) {
                key = [provider_vehicle objectAtIndex:aView.tag];
            }
        }
        if (key == nil) {
            btn.tag = 2500;
            [self onNaviButtonClick:btn];
            return;
        }
        
        [[[TXApp instance] getSettings] setFDKeychain:SettingsConst.CryptoKeys.VSEQ value:[key valueForKey:@"vseq"]];
        
        [self setVehicleInfo:key];
        
        // 시험운행이면 alert를 띄운다.
        if ([[Utils nullToString:[[[TXApp instance] getSettings] getDriveMode]] isEqualToString:DRIVE_MODE_TRAINING]) {
            [Utils onErrorAlertViewTitle:LocalizedStr(@"Menu.Testdrive.request.title") title:LocalizedStr(@"String.Alert") timeout:5];
        }
        
        [self onPD015];
        
        // vehicle close
        UIView *blockView = [self.mapView_ viewWithTag:90];
        
        [UIView animateWithDuration:0.5
                         animations:^{
                             CGRect frame = _vVehicle.frame;
                             frame.origin.y = self.mapView_.bounds.size.height + frame.size.height;
                             _vVehicle.frame = frame;
                             
                             blockView.backgroundColor = HEXCOLOR(0x00000000);
                         }
                         completion:^(BOOL finished) {
                             [blockView removeFromSuperview];
                         }];
    }
#endif
    NSLog(@"onBack2");
    //[self dismissViewControllerAnimated:YES completion:nil];


}

-(IBAction)openChatView
{
    NSString *groupId = @"1";
    NSString *chatUserName = @"";
    NSString *srcName = @"";
    NSString *dstName = @"";
    NSString *srcId = @"";
    NSString *dstId = @"";
    
    NSString* useq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.USEQ];
    NSString* pseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.PSEQ];
    NSString* oseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.OSEQ];
    NSDictionary *item = [[NSDictionary alloc] initWithObjectsAndKeys:useq, @"useq",pseq, @"pseq", nil];
    
    if (self->appDelegate.isDriverMode) {
        //        NSDictionary *__src = [self->appDelegate.dicDriver objectForKey:@"provider_device"];
        //        srcName = [Utils nameOfuname:[__src objectForKey:@"name"]];
        
        srcName = [Utils getUserName];
        srcId = [NSString stringWithFormat:@"pseq,%@",item[@"pseq"]];
        
        NSDictionary *__dst = [self->appDelegate.dicDriver objectForKey:@"user_device"];
        dstName = [Utils nameOfuname:[__dst objectForKey:@"name"]];
        dstId = [NSString stringWithFormat:@"useq,%@",item[@"useq"]];
        
        chatUserName = [NSString stringWithFormat:LocalizedStr(@"Chat.Head.title"), dstName];
    }
    else {
        //        NSDictionary *__src = [self->appDelegate.dicRider objectForKey:@"user_device"];
        //        srcName = [Utils nameOfuname:[__src objectForKey:@"name"]];
        
        srcName = [Utils getUserName];
        srcId = [NSString stringWithFormat:@"useq,%@",item[@"useq"]];
        
        NSDictionary *__dst = [self->appDelegate.dicRider objectForKey:@"provider_device"];
        dstName = [Utils nameOfuname:[__dst objectForKey:@"name"]];
        dstId = [NSString stringWithFormat:@"pseq,%@",item[@"pseq"]];
        
        chatUserName = [NSString stringWithFormat:LocalizedStr(@"Chat.Head.title"), dstName];
    }
    
    chatUserName = [Utils trimStringOnly:chatUserName];
    UIImage *simage = self->appDelegate.imgProfileRider;
    UIImage *dimage = self->appDelegate.imgProfileDriver;
    if (simage == nil) {
        simage = [UIImage imageNamed:@"ic_user_white"];
    }
    if (dimage == nil) {
        dimage = [UIImage imageNamed:@"ic_user_white"];
    }
    
    NSDictionary *dic = @{@"srcName":[Utils trimStringOnly:srcName], // 본인
                          @"dstName":[Utils trimStringOnly:dstName], // 상태방
                          @"srcId":srcId, // 본인
                          @"dstId":dstId, // 상태방
                          @"useq":useq, // 본인
                          @"pseq":pseq, // 상태방
                          @"oseq":oseq,
                          @"srcImage":simage,
                          @"dstImage":dimage,
                          @"headerView":@"1"
                          };
    
    chatView = [[ChatView alloc] initWith:groupId description:chatUserName recent:dic];
    [self.navigationController pushViewController:chatView animated:YES];
    
    [self updateChatBadge:NO];
    [Utils updateAlrim:CODE_TYPE_CHAT code:CODE_ALRIM_CHAT(oseq) value:CODE_FLAG_OFF];
    //[Utils deleteAlrim:CODE_ALRIM_NEWCHAT];
}

-(void)editProfile:(id)sender {
    //[[SlideNavigationController sharedInstance] toggleLeftMenu];
    
    //[self pushViewController:menuVC];
    
    //[self pushViewController:[self vcFromName:@"MenuViewController"]];
    
}

-(IBAction)onEnterPromo:(id)sender {
#ifndef _DRIVER_MODE
    
    [self onUR303];
    /*
    TXCouponVC *vc = [[TXCouponVC alloc] initWithNibName:@"TXCouponVC" bundle:nil target:promoCode label:_lbCouponInfo];
    vc.view.frame = self.view.frame;
    [self pushViewController:vc];
     */
#endif
}

-(IBAction)onPaymentPressed:(id)sender {
#ifndef _DRIVER_MODE
    TXPaymentVC *firstVC = [[TXPaymentVC alloc] initWithNibName:@"TXPaymentVC" bundle:nil];
    firstVC.view.frame = self.view.frame;
    [self pushViewController:firstVC];
#endif
}

-(IBAction)onRequestRider:(id)sender {
    
#ifdef _WITZM
    [self onRequestRiderStep:sender];
#elif defined _CHACHA
#ifndef _DRIVER_MODE
    
#ifdef _MAP_TOUCH_MOVE
    if (isMapMoved) {
        [self onRequestRiderStep:sender];
        return;
    }
    isMapMoved = YES;
#endif
    
    TXDestinationsVC *result = [[TXDestinationsVC alloc] initWithNibName:@"TXDestinationsVC" bundle:nil];
    result._target = self.srcLocationTextField;
    result._step = 420;
    if ([GoogleUtils isLocationValid:lsrc]) {
        result.destPosition = lsrc;
    }
    else {
        result.destPosition = kCLLocationCoordinate2DInvalid;
    }
    result.delegate = self;
    [self presentViewController:result animated:YES completion:nil];
    //[self pushViewController:result];
#endif
#endif
}

-(IBAction)onRequestRiderStep:(id)sender {
#ifndef _DRIVER_MODE
    self->appDelegate.isMyCancel = NO;
    
    if (!self->appDelegate.isNetwork) {
        //return;
    }
    
    NSString* no = [self getAvailableCardNo];
    if (self->appDelegate.ustate == 100) {
        no = @"";
    }
    if ([no isEqualToString:@""]) {
        LGAlertView *alertView = [[LGAlertView alloc] initWithTitle:nil
                                                            message:LocalizedStr(@"Map.Payment.Require.alert.text")
                                                              style:LGAlertViewStyleAlert
                                                       buttonTitles:@[LocalizedStr(@"Map.Payment.Require.OK.text")]
                                                  cancelButtonTitle:LocalizedStr(@"Map.Payment.Require.NOK.text")
                                             destructiveButtonTitle:nil
                                                      actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                                                          //[self removeEventListeners];
                                                          
#if defined (_CHACHA) || defined (_WITZM)
                                                          //TXCardVC *firstVC = [[TXCardVC alloc] initWithNibName:@"TXCardVC" bundle:nil];
                                                          TXAskCardNumberVC *firstVC = [[TXAskCardNumberVC alloc] initWithNibName:@"TXAskCardNumberVC" bundle:nil];
#else
                                                          TXAskCardNumberVC *firstVC = [[TXAskCardNumberVC alloc] initWithNibName:@"TXAskCardNumberVC" bundle:nil];
#endif
                                                          firstVC.isInAPP = YES;
                                                          [self pushViewController:firstVC];
                                                      }
                                                      cancelHandler:^(LGAlertView *alertView) {
                                                      }
                                                 destructiveHandler:^(LGAlertView *alertView) {
                                                     NSLog(@"destructiveHandler");
                                                 }];
        //[alertView setButtonAtIndex:0 enabled:NO];
        //alertView.titleFont = [UIFont systemFontOfSize:14];
        alertView.buttonsFont = [UIFont systemFontOfSize:12];
        alertView.cancelButtonFont = [UIFont systemFontOfSize:12];
        
        alertView.buttonsTitleColor = UIColorDefault;
        alertView.buttonsTitleColorHighlighted = UIColorDefault;
        alertView.buttonsBackgroundColorHighlighted = HEXCOLOR(0x7777770A);
        
#ifdef _WITZM
        alertView.cancelButtonTitleColor = [UIColor redColor];
        alertView.cancelButtonTitleColorHighlighted = [UIColor redColor];
#elif defined _CHACHA
        alertView.cancelButtonTitleColor = [UIColor blackColor];
        alertView.cancelButtonTitleColorHighlighted = [UIColor blackColor];
#endif
        
        alertView.cancelButtonBackgroundColorHighlighted = HEXCOLOR(0x7777770A);
        
        [alertView showAnimated:YES completionHandler:nil];
        
        return;
    }
    else {
        
    }
    
/*
    LGAlertView *alertView = [[LGAlertView alloc] initWithTitle:nil
                                                        message:LocalizedStr(@"Map.Vechile.Request.Alert.text")
                                                          style:LGAlertViewStyleAlert
                                                   buttonTitles:@[LocalizedStr(@"Button.OK")]
                                              cancelButtonTitle:LocalizedStr(@"Button.CANCEL")
                                         destructiveButtonTitle:nil
                                                  actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                                                      
                                                      // request
                                                      [self onUD120];
                                                      
                                                  }
                                                  cancelHandler:^(LGAlertView *alertView) {
                                                  }
                                             destructiveHandler:^(LGAlertView *alertView) {
                                             }];
    [Utils initAlertButtonColor:alertView];
    [alertView showAnimated:YES completionHandler:nil];
*/
    [self onUD120];
#endif
}
-(NSString*)getAvailableCardNo
{
    NSArray *cards = [self->appDelegate.dicRider valueForKey:@"user_payments"];
    NSString* no     = @"";
    
    if ([Utils isArray:cards]) {
        
        // 사용가능 카드를 찾는다.
        for (NSDictionary* key in cards) {
            //NSString* cseq   = [key valueForKey:@"cseq"];
            int mode   = [key[@"mode"] intValue];
            int state   = [key[@"state"] intValue];
            
            if (mode == 1 && state == 1) {
                NSString *cardNo = [Utils trimStringOnly:key[@"no"]];
                if ([cardNo length] > 4) {
                    NSInteger lastIdx = [cardNo length] - 4;
                    cardNo = [cardNo substringFromIndex : lastIdx];
                }
//                no = [NSString stringWithFormat:@"%@ %@ %@",
//                      LocalizedStr(@"Menu.Payment.Detail.title"),
//                      key[@"name"],
//                      cardNo
//                      ];
                no = [NSString stringWithFormat:@"xxxx-%@",
                      cardNo
                      ];
                [[[TXApp instance] getSettings] setFDKeychain:SettingsConst.CryptoKeys.CSEQ value:key[@"cseq"]];
                break;
            }
        }
    }
    return no;
}

- (void)sessionOut {
    
    if (isLogout) {
        return;
    }
    
    [self cancelAllLoop];
    [self cancelTimeout];
    
    if (self->appDelegate.isDriverMode) {
        [self->model PP114:NO];
    }
    else {
        // 메시지 body정보
        NSString *userUid   = [settings getUserId];
        NSString *userTelno = [settings getUserTelno];
        
        TXUser *__user = [[TXUser alloc] init];
        __user.uid = userUid;
        __user.utelno = userTelno;
        
        [self->model UR114:__user withSession:NO];
    }
}

- (void)clearDestAddress:(id)sender {
    [self clearDsetAddressTextField];

    [appDelegate debugActivityState];
    [Utils setStateUIInfo:UI_STATE_TRIP_LINE value:UI_STATE_STEP_WAIT oldvalue:nil];
    // 130.10,20, 140.10 취소였는지 확인
    if ((self->appDelegate.ustate == 130 || self->appDelegate.ustate == 140) && self->appDelegate.ustate2 <= -10) {
        NSLog(@"-=-=-=-=-=-=1");
    }
    else {
        NSLog(@"-=-=-=-=-=-=2");
        self->appDelegate.ustate2 = -1;
    }
    
    [self onUpdateUI];
    
//    if ((self->appDelegate.ustate == 130 || self->appDelegate.ustate == 140) && self->appDelegate.ustate2 <= -10) {
//        self->appDelegate.uistate2 = self->appDelegate.ustate2;
//    }
    
}

- (void)reStore111:(id)sender {
#ifndef _DRIVER_MODE
    promoCode = [[[TXApp instance] getSettings] getFDKeychain:@"promoCode"];
    if (![promoCode isEqualToString:@""]) {
        //_lbCouponInfo.text = promoCode;
        _lbCouponInfo.text = LocalizedStr(@"Map.Vechile.Request.CouponCode.text");
    }
    [self getServiceType];
    
    // 110 상태에서 출/도착지가 있으면 UI가 바뀐다.
    if(!self->appDelegate.isDriverMode &&
       self->appDelegate.ustate == 110 &&
       ![_srcLocationTextField.text isEqualToString:@""] &&
       ![_dstLocationTextField.text isEqualToString:@""]
       ) {
        //drawLineTime = [[NSDate date] dateByAddingTimeInterval:-7];
        
        [Utils setStateUIInfo:UI_STATE_TRIP_LINE value:UI_STATE_STEP_WAIT oldvalue:nil];
        // 230.10,20, 240.10 취소였는지 확인
        if (self->appDelegate.ustate == 230 || (self->appDelegate.ustate == 240 && self->appDelegate.ustate2 == 10)) {
            
        }
        else {
            self->appDelegate.ustate2 = -3;
        }
        //[self updteOrderSelect];
        [self updateUIServiceType];
        [self onUpdateUI];
        
        // ETA
        _lbTopTitle.text = [NSString stringWithFormat:@"%@ ...", LocalizedStr(@"Map.Drive.Pickup.ETA.text")];
        
        // route 그리기 : 차량요청
        [self drawRoute];
        
        // request driver
        //[self performSelector:@selector(onRequestRider:) withObject:nil afterDelay:2];
    }
    [self pushCpos:btnCpos];
#endif
}

- (void)dismissVcCancel:(NSNotification *)notification {
    NSLog(@"dismissVcCancel");
    if (vcCancel != nil) {
        if (self->appDelegate.isDriverMode) {
            self->appDelegate.pstateold = 0;
        }
        else {
            self->appDelegate.ustateold = 0;
        }
        //dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            //[self removeTempVC];
            [self.navigationController popToRootViewControllerAnimated:YES];
            vcCancel = nil;
            
            if (self->appDelegate.isDriverMode) {
                if (notification == nil) {
                    return;
                }
                NSDictionary* dict = notification.userInfo;
                if ([Utils isDictionary:dict]) {
                    NSString* cancel = [dict valueForKey:@"cancel"];
                    if ([cancel isEqualToString:@"1"]) {
                        // testdrive인 경우 재시도 확인 창
                        [self testModeRetryAlert];
                    }
                }
            }
        //});
    }
    
    // 취소된 후 결제화면은 놔둬야 한다.
    if ((self->appDelegate.ustate == 130 || self->appDelegate.ustate == 140) && self->appDelegate.ustate2 <= -10) {
        return;
    }
    else if ((self->appDelegate.pstate == 230 || self->appDelegate.pstate == 240) && self->appDelegate.pstate2 <= -10) {
        return;
    }
    
    if (vcCharge != nil) {
        [self.navigationController popToRootViewControllerAnimated:YES];
        vcCharge = nil;
//        [vcCharge dismissViewControllerAnimated:YES completion:^(){
//            vcCharge = nil;
//            NSLog(@"vcCharge1");
//        }];
//        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
//            NSLog(@"vcCharge1");
//            vcCharge = nil;
//        });
    }
}

- (void)logout:(id)sender {
    
    [self applicationWillTerminate];
    [self cancelAllLoop];
    [self cancelTimeout];
    //    [self removeObservers];
    [[TXCallModel instance] onCleanSession];
    [Utils resetStateUIInfo];
    [self stopETClock];
    [self removeEventListeners];
    
    [self removeObservers];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [self removeFromParentViewController];

    // sendder가 nil이 아니면 화면을 로그아웃시킨다.
    if(sender != nil) {
        
        isLogout = YES;
        
        [[TXCallModel instance] onCleanSession];
        
        //[RedisSingleton redisDisConnect];
        
        NSLog(@"Log out");

        TXLauncherVC *firstVC = [[TXLauncherVC alloc] initWithNibName:@"TXLauncherVC" bundle:nil flag:YES];
        
        [appDelegate resetRootVC:firstVC];
    }
}

-(void)onUR114 {
    
    [self cancelAllLoop];
    [self cancelTimeout];
    
    [self showBusyIndicator:@"Requesting LogOut ... "];
    [self->model UR114:user];
}

-(void)onUR200 {
    
    //[self showBusyIndicator:@"Requesting LogOut ... "];
    //[self->model UR200];
    [self->model requsetAPI:@"UR200" property:nil];
}

-(void)onUR206 {
    
    if (self->appDelegate.isDriverMode) return;
    
    if (![Utils isArray:self->appDelegate.dicRider[@"user_matchs"]] && self->appDelegate.ustate <= 110) {
        //[self showBusyIndicator:@"Requesting LogOut ... "];
        
#ifdef _WITZM
        [self->model requsetAPI:@"UR206" property:nil];
#elif defined _CHACHA
#endif
    }
}

-(void)onPP206 {
    
    if (![Utils isArray:self->appDelegate.dicDriver[@"provider_matchs"]] && self->appDelegate.pstate <= 210) {
        //[self showBusyIndicator:@"Requesting LogOut ... "];
        
#ifdef _WITZM
        [self->model requsetAPI:@"PP206" property:nil];
#elif defined _CHACHA
#endif
    }
}

-(void)onUR601 {
    if (self->appDelegate.imgProfileRider != nil) {
        return;
    }
    if (self->appDelegate.pictureSizeRider == nil) {
        return;
    }
    if ([Utils checkStateUIInfo:UI_STATE_UR601 oldvalue:@"UR601"] == UI_STATE_STEP_RUN) {
        return;
    }
    if ([Utils checkStateUIInfo:UI_STATE_UR601 oldvalue:@"UR601"] == UI_STATE_STEP_FIN) {
        return;
    }
    [Utils setStateUIInfo:UI_STATE_UR601 value:UI_STATE_STEP_RUN oldvalue:@"UR601"];
    
    //[self showBusyIndicator:@"Requesting LogOut ... "];
    [self->model UR601];
}

-(void)onUR602 {
    if (self->appDelegate.imgProfileDriver != nil) {
        return;
    }
    if (self->appDelegate.pictureSizeDriver == nil) {
        return;
    }
    if ([Utils checkStateUIInfo:UI_STATE_UR602 oldvalue:@"UR602"] == UI_STATE_STEP_RUN) {
        return;
    }
    if ([Utils checkStateUIInfo:UI_STATE_UR602 oldvalue:@"UR602"] == UI_STATE_STEP_FIN) {
        return;
    }
    [Utils setStateUIInfo:UI_STATE_UR602 value:UI_STATE_STEP_RUN oldvalue:@"UR602"];
    
    //[self showBusyIndicator:@"Requesting LogOut ... "];
    [self->model UR602];
}

-(void)onUR603 {
    
    if (self->appDelegate.imgVehicleDriver != nil) {
        return;
    }
    NSDictionary *__vehicle;
    if (self->appDelegate.isDriverMode) {
        __vehicle = [self->appDelegate.dicDriver objectForKey:@"provider_device"];
    }
    else {
        __vehicle = [self->appDelegate.dicRider objectForKey:@"provider_device"];
    }
    NSString *vseq;
    if ([Utils isDictionary:__vehicle]) {
        vseq = [Utils nullToString:__vehicle[@"vseq"]];
    }
    
    if ([[Utils nullToString:vseq] isEqualToString:@""]) {
        return;
    }
    NSDictionary *_propertyMap = @{
                                   @"vseq" : vseq,
                                   @"name" : @"vehicle"
                                   };
    
    //    [self showBusyIndicator:@"Loading info ... "];
    [self->model requsetAPI:API_FUNC(NSStringFromSelector(_cmd)) property:_propertyMap];
}

-(void)onPP601 {
    if (self->appDelegate.imgProfileRider != nil) {
        return;
    }
    if (self->appDelegate.pictureSizeRider == nil) {
        return;
    }
    if ([Utils checkStateUIInfo:UI_STATE_PP601 oldvalue:@"PP601"] == UI_STATE_STEP_RUN) {
        return;
    }
    if ([Utils checkStateUIInfo:UI_STATE_PP601 oldvalue:@"PP601"] == UI_STATE_STEP_FIN) {
        return;
    }
    [Utils setStateUIInfo:UI_STATE_PP601 value:UI_STATE_STEP_RUN oldvalue:@"PP601"];
    
    //[self showBusyIndicator:@"Requesting LogOut ... "];
    [self->model PP601];
}


-(void)onPP602 {
    
    if (self->appDelegate.imgProfileDriver != nil) {
        return;
    }
    if (self->appDelegate.pictureSizeDriver == nil) {
        return;
    }
    if ([Utils checkStateUIInfo:UI_STATE_PP602 oldvalue:@"PP602"] == UI_STATE_STEP_RUN) {
        return;
    }
    if ([Utils checkStateUIInfo:UI_STATE_PP602 oldvalue:@"PP602"] == UI_STATE_STEP_FIN) {
        return;
    }
    [Utils setStateUIInfo:UI_STATE_PP602 value:UI_STATE_STEP_RUN oldvalue:@"PP602"];
    
    //[self showBusyIndicator:@"Requesting LogOut ... "];
    [self->model PP602];
}

-(void)onPP603 {
    
    if (self->appDelegate.imgVehicleDriver != nil) {
        return;
    }
    NSDictionary *__vehicle;
    if (self->appDelegate.isDriverMode) {
        __vehicle = [self->appDelegate.dicDriver objectForKey:@"provider_device"];
    }
    else {
        __vehicle = [self->appDelegate.dicRider objectForKey:@"provider_device"];
    }
    NSString *vseq;
    if ([Utils isDictionary:__vehicle]) {
        vseq = [Utils nullToString:__vehicle[@"vseq"]];
    }
    
    if ([[Utils nullToString:vseq] isEqualToString:@""]) {
        return;
    }
    NSDictionary *_propertyMap = @{
                                   @"vseq" : vseq,
                                   @"name" : @"vehicle"
                                   };
    
    //    [self showBusyIndicator:@"Loading info ... "];
    [self->model requsetAPI:API_FUNC(NSStringFromSelector(_cmd)) property:_propertyMap];
}

// This function converts decimal degrees to radians
-(double) _deg2rad:(double) deg
{
    return (deg * M_PI/180.0);
}
// This function converts radians to decimal degrees
-(double) _rad2deg:(double) rad
{
    return (rad * 180.0 / M_PI);
}

/**
 * 두 지점간의 거리 계산
 *
 * @param lat1 지점 1 위도
 * @param lon1 지점 1 경도
 * @param lat2 지점 2 위도
 * @param lon2 지점 2 경도
 * @param unit 거리 표출단위
 */
// unit : km, m
-(double) geoDistance:(CLLocationCoordinate2D)loc1 loc2:(CLLocationCoordinate2D)loc2 unit:(NSString*) unit
{
    double lat1 = loc1.latitude;
    double lon1 = loc1.longitude;
    double lat2 = loc2.latitude;
    double lon2 = loc2.longitude;
    
    double theta = lon1 - lon2;
    double dist = sin([self _deg2rad:lat1]) * sin([self _deg2rad:lat2]) + cos([self _deg2rad:lat1]) * cos([self _deg2rad:lat2]) * cos([self _deg2rad:theta]);
    dist = acos(dist);
    dist = [self _rad2deg:dist];
    double miles = dist * 60 * 1.1515;
    
    if ([unit isEqualToString:@"K"]) {
        return (miles * 1.609344);
    } else {
        return (miles * 1609.344);
    }
}

-(void)initRiderProfileImage {
    if (self->appDelegate.imgProfileRider != nil) {
        self->appDelegate.imgProfileRider = nil;
        self->appDelegate.pictureSizeRider = nil;
        self->appDelegate.dicRider = nil;
        self->appDelegate.dicDrive = nil;
        
        [[[TXApp instance] getSettings] setFDKeychain:SettingsConst.CryptoKeys.PSEQD value:@"0"];
        [[[TXApp instance] getSettings] setFDKeychain:SettingsConst.CryptoKeys.USEQD value:@"0"];
    }
}

-(void)initDriderProfileImage {
    if (self->appDelegate.imgProfileDriver != nil) {
        self->appDelegate.imgProfileDriver = nil;
        self->appDelegate.pictureSizeDriver = nil;
        self->appDelegate.dicDriver = nil;
        self->appDelegate.dicDrive = nil;
        
        [[[TXApp instance] getSettings] setFDKeychain:SettingsConst.CryptoKeys.PSEQD value:@"0"];
        [[[TXApp instance] getSettings] setFDKeychain:SettingsConst.CryptoKeys.USEQD value:@"0"];
    }
}

-(void)hideFocusPin {
    
    if (mImagePin.hidden) return;
    
    //mImageFocus.hidden = YES;
    mImagePin.hidden = YES;
    
    //mImageFocus.alpha = 0;
    mImagePin.alpha = 0;
}

-(void)showFocusPin {
    
    if (![self canShowPin]) {
        return;
    }
    if (!mImagePin.hidden) return;
    
    //mImageFocus.hidden = NO;
    mImagePin.hidden = NO;
    
    //mImageFocus.alpha = 1;
    mImagePin.alpha = 1;
    
#if defined (_MAP_TOUCH_MOVE) && ! defined (_DRIVER_MODE)
    // 110인 경우 start marker를 보이지 않게 한다. imagepin을 사용하기 위해
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    if (!self->appDelegate.isDriverMode && self->appDelegate.ustate <= 110 &&
        ([_srcLocationTextField.text isEqualToString:@""] || [_dstLocationTextField.text isEqualToString:@""])) {
        [locMgr clearMarker:locMgr.markerStart];
        
        isMapMoved = NO;
    }
#endif
}

-(BOOL)canShowPin {
    
    if (!self->appDelegate.isDriverMode && self->appDelegate.ustate <= 110 && [_dstLocationTextField.text isEqualToString:@""]) {
        return YES;
    }
    
    return NO;
}

- (void)hideAllVehicle {
    if (![self.markers count]) {
        return;
    }
    
    for (id key in self.markers) {
        GMSMarker *_marker = (GMSMarker *)self.markers[key];
        _marker.map = nil;
        _marker = nil;
    }
    
    [self.markers removeAllObjects];
}
// for driver mode
-(void)isChangedCancelAlert:(int)__state
{
    BOOL __flag = NO;
    
    if (!self->appDelegate.isDriverMode) {
        return;
    }
    
    [self hideMarker];
    [self cleanTripMapOnly];
    //[self cleanMapOnly];

    if (self->appDelegate.isMyCancel) {
        [self cancelTimeout];
        
        self->appDelegate.pstateold = 0;
        self->appDelegate.isMyCancel = NO;
        
        // request close
        if (vcMapCall != nil) {
            [vcMapCall dismissViewControllerAnimated:YES completion:^(){
                vcMapCall = nil;
                // open canceled UI
            }];
        }
        return;
    }
    
    // 상대방에서 취소한 경우
    if (__state == 220) {
        // 취소상태로 변경됨. 취소 alert
        __flag = YES;
        //NSLog(@"----------------------");
    }
    else if (__state == 230) {
        // 취소상태로 변경됨. 취소 alert
        __flag = YES;
        //NSLog(@"----------------------");
        if (vcCancel != nil) {
            [self.navigationController popToRootViewControllerAnimated:YES];
            vcCancel = nil;
//            [vcCancel dismissViewControllerAnimated:YES completion:^(){
//                vcCancel = nil;
//                NSLog(@"vcCancel0");
//            }];
//            [self.navigationController dismissViewControllerAnimated:YES completion:^(){
//                vcCancel = nil;
//            }];
//            [vcCancel removeFromParentViewController];
//            vcCancel = nil;
        }
        
        if (vcCancel == nil) {
            //NSLog(@"----------------------");
            // push alert만 출력하고 cancel ui는 보이지 않는다.
//            vcCancel = [[TXCancelVC alloc] initWithNibName:@"TXCancelVC" bundle:nil step:2 state:__state];
//            vcCancel.view.frame = self.view.frame;
//            [self pushViewController:vcCancel];
            
            // 라이더 취소시 알림음과 진동처리
            [appDelegate playSound:SOUND_NOTI vibrate:YES];
            // tts
            NSLog(@"appDelegate playSound");
            
            // 노티 얼러트
            [self changePickupCancelNotofication];
        }
    }
    
    if (!__flag) {
        return;
    }
    
    [self cancelTimeout];
    self->appDelegate.pstateold = 0;
    
    // 상태 초기화
    [appDelegate resetTripInfo];
    [self pushCpos:btnCpos];
    [self initAddressDst];
    // request close
    if (vcMapCall != nil) {
        [vcMapCall dismissViewControllerAnimated:YES completion:^(){
            vcMapCall = nil;
            // open canceled UI
        }];
    }
    else {
        // open canceled UI
    }
}

-(void)isNotPikcupedAlert:(int)__state
{
    BOOL __flag = NO;
    NSString *__alert = @"";
    NSLog(@"----------------------");
    if (self->appDelegate.isDriverMode) {
        return;
    }

    if ((!self->appDelegate.isDriverMode && self->appDelegate.ustate <= 110)) {
        [self pushCpos:btnCpos];
    }
    
    if (self->appDelegate.isMyCancel) {
        [self cancelTimeout];
        
        //        self->appDelegate.ustateold = 0;
        //        self->appDelegate.isMyCancel = NO;
        //        _vehicleView.userInteractionEnabled = YES;
        NSLog(@"----------------------");
        // request close
        if (vcMapCall != nil) {
            [vcMapCall dismissViewControllerAnimated:YES completion:^(){
                vcMapCall = nil;
                // open canceled UI
            }];
        }
        return;
    }
    // 상대방에서 취소한 경우
    if (__state == 120) {
        // 취소상태로 변경됨. 취소 alert
        __flag = YES;
        __alert = LocalizedStr(@"Map.Rider.Request.Driver.NOT.text");
        //NSLog(@"----------------------");
    }
    else if (__state == 130) {
        // 취소상태로 변경됨. 취소 alert
        __flag = YES;
        __alert = LocalizedStr(@"Map.Rider.Pickup.Driver.Cancel.text");
        //NSLog(@"----------------------");
        if (vcCancel != nil) {
            [self.navigationController popToRootViewControllerAnimated:YES];
            vcCancel = nil;
//            [vcCancel dismissViewControllerAnimated:YES completion:^(){
//                vcCancel = nil;
//            }];
//            [vcCancel removeFromParentViewController];
//            vcCancel = nil;
        }
        
        if (vcCancel == nil) {
            //NSLog(@"----------------------");
            vcCancel = [[TXCancelVC alloc] initWithNibName:@"TXCancelVC" bundle:nil step:2 state:__state];
            vcCancel.view.frame = self.view.frame;
            [self pushViewController:vcCancel];
        }
    }
    else if (__state == 140) {
        // 취소상태로 변경됨. 취소 alert
        __flag = YES;
        __alert = LocalizedStr(@"Map.Rider.Trip.Driver.Cancel.text");
        NSLog(@"----------------------");
        if (vcCancel != nil) {
            [self.navigationController popToRootViewControllerAnimated:YES];
            vcCancel = nil;
//            [vcCancel dismissViewControllerAnimated:YES completion:^(){
//                vcCancel = nil;
//            }];
//            [vcCancel removeFromParentViewController];
//            vcCancel = nil;
        }
        
        if (vcCancel == nil) {
            NSLog(@"----------------------");
            vcCancel = [[TXCancelVC alloc] initWithNibName:@"TXCancelVC" bundle:nil step:3 state:__state];
            vcCancel.view.frame = self.view.frame;
            [self pushViewController:vcCancel];
        }
    }
    
    if (!__flag) {
        return;
    }
    
    [self cancelTimeout];
    
    // 상태 초기화
    [appDelegate resetTripInfo];
    //[self initAddressDst];
    //[self etaDriverType220_10:selectedVechicleInfo str5:@" "];
    
    //_vehicleView.userInteractionEnabled = YES;
    //self->appDelegate.ustateold = 0;
    
    // request close
    if (vcMapCall != nil) {
        [vcMapCall dismissViewControllerAnimated:YES completion:^(){
            vcMapCall = nil;
            // open canceled UI
        }];
    }
    else {
        // open canceled UI
    }
    
    return;
}

-(void)onUpdateUI:(NSDictionary *)param{
    
    if (self->appDelegate.isDriverMode) {
        if (!(self->appDelegate.pstate == 220 && self->appDelegate.pstate2 == 10)) {
            [Utils setStateUIInfo:UI_STATE_PD220 value:UI_STATE_STEP_FIN oldvalue:nil];
            etTime = -99;
        }
    }
    
    // view가 바뀌었으면 UI를 갱신하지 않는다.
    if (_isViewChanged) {
        return;
    }
    
    //NSLog(@"updateUI");
#ifdef _DRIVER_MODE
    if (self->appDelegate.isDriverMode) {
        //*****************************************************************
        // 상태이상.
        if (self->appDelegate.pstate == 220 && self->appDelegate.pstate2 < 0) {
            [self onPD131];
        }
        else if (self->appDelegate.pstate == 0) {
            if (!isLogout) {
                // login
                [self onPP110Auto];
            }
        }
        
        //*****************************************************************
        // ET timer
        if (!((self->appDelegate.pstate == 230 && self->appDelegate.pstate2 == 20) ||
              (self->appDelegate.pstate == 220 && self->appDelegate.pstate2 == 10) ||
              (self->appDelegate.pstate == 240 && self->appDelegate.pstate2 == 10)
              )) {
            
            
            if (!((self->appDelegate.pstate == 230) ||
                  (self->appDelegate.pstate == 240)
                  )) {
                [self stopETClock];
            }
        }
    }
#else
    {
        // 상태이상.
        if (self->appDelegate.ustate == 120 && self->appDelegate.ustate2 < 0) {
            [self onUD131];
        }

        //*****************************************************************
        // ET timer
        if (!(self->appDelegate.ustate == 130 && self->appDelegate.ustate2 == 20)) {
            
            [self stopETClock];
        }
    }
#endif
}

-(void)onChangeUI {
    
    if (self->appDelegate.isDriverMode) {
        
    }
    else {
        
    }
    NSLog(@"changeUI");
}


/*
 출발지
 _step : 110(주소검색)
 _step : 120(지도)
 _step : 111(favorite 주소검색)
 _step : 121(favorite 지도)
 도착지
 _step : 210(주소검색)
 _step : 220(지도)
 _step : 211(favorite 주소검색)
 _step : 221(favorite 지도)
 */
-(IBAction)selectDestAddress:(id)sender {
#ifdef _DRIVER_MODE
    NSDictionary *dic = [self->appDelegate.dicDriver objectForKey:@"provider_device"];
    if ([Utils isDictionary:dic]) {
        int ldcnt = [dic[@"ldcnt"] intValue];
        int ldmax = [dic[@"ldmax"] intValue];
        
        if (ldcnt >= ldmax) {
            return;
        }
    }
#else
    // 110에서 차량 부르기전에는 주소를 고칠수 없다.
    if (!self->appDelegate.isDriverMode && self->appDelegate.ustate == 110 && ![_srcLocationTextField.text isEqualToString:@""] && ![_dstLocationTextField.text isEqualToString:@""]) {
//        return;
    }
#endif
    TXDestinationsVC *result = [[TXDestinationsVC alloc] initWithNibName:@"TXDestinationsVC" bundle:nil];
    result._target = self.dstLocationTextField;
    result._step = 210;
    if ([GoogleUtils isLocationValid:ldst]) {
        result.destPosition = ldst;
    }
    else {
        result.destPosition = kCLLocationCoordinate2DInvalid;
    }
    result.delegate = self;
    [self pushViewController:result];
    
}

#ifndef _DRIVER_MODE
-(IBAction)selectFavoriteDestAddress:(id)sender {
    UIButton *btn = (UIButton*)sender;
    
    // 110에서 차량 부르기전에는 주소를 고칠수 없다.
    if (!self->appDelegate.isDriverMode && self->appDelegate.ustate == 110 && ![_srcLocationTextField.text isEqualToString:@""] && ![_dstLocationTextField.text isEqualToString:@""]) {
        return;
    }
    /*
    if (btn.tag == 26) {
#ifndef _DRIVER_MODE
        
#ifdef _WITZM
#elif defined _CHACHA
#else
        TXFavoriteVC *result = [[TXFavoriteVC alloc] initWithNibName:@"TXFavoriteVC" bundle:nil];
        result._target = self.dstLocationTextField;
        result.delegate = self;
        [self pushViewController:result];
#endif
        
#endif
        
#ifdef _WITZM
#elif defined _CHACHA
#else
        return;
#endif
        
    }
     */
    CLLocationCoordinate2D ldst__;
    NSString *tag = [NSString stringWithFormat:@"%ld",(long)btn.tag];
    ldst__ = [Utils getLocation:dicFavorite[tag][@"location"]];
    
    if ([GoogleUtils isLocationValid:ldst__]) {
        TXDestinationsVC *result = [[TXDestinationsVC alloc] initWithNibName:@"TXDestinationsVC" bundle:nil];
        result._target = self.dstLocationTextField;
        result._step = 220;
        result.destPosition = ldst__;
        result.delegate = self;
        [self pushViewController:result];
    }
    else {
        // 설정된 정보가 없다.
        NSString *type__  = @"1";
        NSString *place__ = @"";
        if (btn.tag == 24) {
            place__ = LocalizedStr(@"Map.Search.Address.HOME.text");
            type__ = @"1";
        }
        else if (btn.tag == 25) {
            place__ = LocalizedStr(@"Map.Search.Address.WORK.text");
            type__ = @"2";
        }
        
        NSDictionary *items__ = @{@"place":place__, @"type":type__};
        TXDestinationsVC *result = [[TXDestinationsVC alloc] initWithNibName:@"TXDestinationsVC" bundle:nil];
        result._target = items__;
        result._step = 310;
        result.destPosition = kCLLocationCoordinate2DInvalid;
        result.delegate = self;
        [self pushViewController:result];
    }
}

-(IBAction)selectPickupAddress:(id)sender {
    
    // 110에서 차량 부르기전에는 주소를 고칠수 없다.
    if (!self->appDelegate.isDriverMode && self->appDelegate.ustate == 110 && ![_srcLocationTextField.text isEqualToString:@""] && ![_dstLocationTextField.text isEqualToString:@""]) {
//        return;
    }
    
    //
    if (!self->appDelegate.isDriverMode && self->appDelegate.ustate >= 130) {
        // alert
        
        __block LGAlertView *alertView = [[LGAlertView alloc] initWithTitle:nil
                                                                    message:LocalizedStr(@"Map.Drive.OnPickup.NoChange.SRC.text")
                                                                      style:LGAlertViewStyleAlert
                                                               buttonTitles:@[LocalizedStr(@"Button.OK")]
                                                          cancelButtonTitle:nil
                                                     destructiveButtonTitle:nil
                                                              actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                                                              }
                                                              cancelHandler:^(LGAlertView *alertView) {
                                                              }
                                                         destructiveHandler:^(LGAlertView *alertView) {
                                                         }];
        
        [Utils initAlertButtonColor:alertView];
        alertView.windowLevel = LGAlertViewWindowLevelAboveStatusBar;
        [alertView showAnimated:YES completionHandler:nil];
        
        return;
    }
    TXDestinationsVC *result = [[TXDestinationsVC alloc] initWithNibName:@"TXDestinationsVC" bundle:nil];
    result._target = self.srcLocationTextField;
    result._step = 110;
    if ([GoogleUtils isLocationValid:lsrc]) {
        result.destPosition = lsrc;
    }
    else {
        result.destPosition = kCLLocationCoordinate2DInvalid;
    }
    result.delegate = self;
    [self pushViewController:result];
}
#endif

-(void)keyTyped:(id)sender {
    
    UITextField *field = (UITextField *) sender;
    NSString *typedText = field.text;
    int length = (int)typedText.length;
    char lastChar = [typedText characterAtIndex:length - 1];
    
    
    if(lastChar == ' ' || isdigit(lastChar)) {
        
//        [self->googleReqMgr sendPlaceAutocompleteAsync:field.text sensor:YES optional:nil];
    }
    
}

-(void)testModeRetryAlert
{
    // testdrive
    if ([[Utils nullToString:[[[TXApp instance] getSettings] getDriveMode]] isEqualToString:DRIVE_MODE_TRAINING]) {
        [self onPP136];
        //alert
        
        LGAlertView *alertView = [[LGAlertView alloc] initWithTitle:nil
                                                            message:LocalizedStr(@"Menu.Testdrive.end.Header.title")
                                                              style:LGAlertViewStyleAlert
                                                       buttonTitles:@[LocalizedStr(@"Button.OK")]
                                                  cancelButtonTitle:LocalizedStr(@"Button.TESTDRIVE")
                                             destructiveButtonTitle:nil
                                                      actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                                                          
                                                      }
                                                      cancelHandler:^(LGAlertView *alertView) {
                                                          [self onPP116];
                                                      }
                                                 destructiveHandler:^(LGAlertView *alertView) {
                                                     
                                                 }];
        [Utils initAlertButtonColor:alertView];
        [alertView showAnimated:YES completionHandler:^(void)
         {
             
         }];
    }
}

#pragma mark - End of Actions

-(GMSMarker*) addMarker:(CLLocationCoordinate2D) position withTaxi:(TXTaxi*)taxi {
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    
    NSString *service = [taxi.provider objectForKey:@"service"];
    
    GMSMarker *carMarker = [GMSMarker markerWithPosition:position];
    carMarker.draggable = NO;
    carMarker.icon = [locMgr getMarkerIcon:service];
    //carMarker.groundAnchor = CGPointMake(0.5f, 0.5f); //마커 회전
    carMarker.flat = YES;
    //carMarker.title = @"Driver";
#ifdef _DEBUG
    carMarker.snippet = [Utils nameOfuname:[taxi.provider valueForKey:@"name"]];
#endif
    return (GMSMarker*)carMarker;
}

- (UIImage *)imageFromView:(UIView *) view
{
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        UIGraphicsBeginImageContextWithOptions(view.frame.size, NO, [[UIScreen mainScreen] scale]);
    } else {
        UIGraphicsBeginImageContext(view.frame.size);
    }
    [view.layer renderInContext: UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

-(void) searchLocation {
    
}


- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context {
    if (!firstLocationUpdate_) {
        firstLocationUpdate_ = YES;
        
        CLLocation *location = [object valueForKeyPath:keyPath];
        self.mapView_.settings.myLocationButton = NO;
        self.mapView_.camera = [GMSCameraPosition cameraWithTarget:location.coordinate
                                                              zoom:[self getZoomByState]];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)catchNetworkError:(TXResponseDescriptor *)descriptor {
    if(!descriptor.success && descriptor.rlt == -999999 && ![descriptor.error isEqualToString:TXEvents.HTTPREQUESTCOMPLETED]) {
        return YES;
    }
    return NO;
}

- (void)updateMapHeading {
    [self.mapView_ animateToBearing:0];
}
/*
- (void)updateMapFitBounds:(NSArray*)coord__ {
    GMSCoordinateBounds *bounds;
    for (NSValue *aValue in coord__) {
        CLLocationCoordinate2D coord = [aValue MKCoordinateValue];
        if (bounds == nil) {
            bounds = [[GMSCoordinateBounds alloc] initWithCoordinate:coord
                                                          coordinate:coord];
        }
        bounds = [bounds includingCoordinate:coord];
        
        //NSLog(@"updateMapFitBounds--->%f,%f",coord.latitude, coord.longitude);
    }
    //CGFloat currentZoom = self.mapView_.camera.zoom;
    
    [CATransaction begin];
    [CATransaction setAnimationDuration:0.5];
    
    GMSCameraUpdate *update = [GMSCameraUpdate fitBounds:bounds withPadding:kBasicMapPadding];
    //GMSCameraUpdate *update = [GMSCameraUpdate fitBounds:bounds];
    [self.mapView_ animateWithCameraUpdate:update];
    
    [CATransaction commit];
}
*/
- (void)updateMapFitBounds:(NSArray *)coord__
{
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    GMSCoordinateBounds *bounds = nil;
    NSInteger __pathCount = [locMgr.tripline.path count];
    for (int i =0; i<__pathCount ; i++) {
        CLLocationCoordinate2D coord = [locMgr.tripline.path coordinateAtIndex:i];
        if (bounds == nil) {
            bounds = [[GMSCoordinateBounds alloc] initWithCoordinate:coord
                                                          coordinate:coord];
        }
        bounds = [bounds includingCoordinate:coord];
    }
    
    if ([GoogleUtils isArray:coord__] && [coord__ count]) {
        for (NSValue *aValue in coord__) {
            CLLocationCoordinate2D coord = [aValue MKCoordinateValue];
            if (bounds == nil) {
                bounds = [[GMSCoordinateBounds alloc] initWithCoordinate:coord
                                                              coordinate:coord];
            }
            bounds = [bounds includingCoordinate:coord];
        }
    }
    //CGFloat currentZoom = self.mapView.camera.zoom;
    
    [CATransaction begin];
    [CATransaction setAnimationDuration:0.5];
    
    NSLog(@"GMSCameraPosition5: %f,%f,%f,%f", bounds.northEast.latitude, bounds.southWest.longitude, bounds.southWest.latitude, bounds.northEast.longitude);
    
    GMSCameraUpdate *update = [GMSCameraUpdate fitBounds:bounds withPadding:kBasicMapPadding];
    //GMSCameraUpdate *update = [GMSCameraUpdate fitBounds:bounds];
    [self.mapView_ animateWithCameraUpdate:update];
    
    [CATransaction commit];
}

// 차량과 목적지사이의 fit처리
- (void)updateVehicleFitBounds {
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    
    //NSValue *sValue1 = [NSValue valueWithMKCoordinate:[self srcPositionCoor]];//]locMgr.markerDrive.position];
    NSValue *sValue1 = [NSValue valueWithMKCoordinate:locMgr.markerDrive.position];
    NSValue *eValue1 = [NSValue valueWithMKCoordinate:[Utils destinationPositionCoor]];//locMgr.markerFinish.position];
    //    CLLocationDistance distance = GMSGeometryDistance([self srcPositionCoor], [self destinationPositionCoor]);
    
    if (![Utils isLocationValid:locMgr.markerDrive.position]) {
        return;
    }
    
    if (!self->appDelegate.isDriverMode && self->appDelegate.ustate == 130) {
        sValue1 = [NSValue valueWithMKCoordinate:locMgr.markerDrive.position];
        eValue1 = [NSValue valueWithMKCoordinate:[Utils srcPositionCoor]];
        
        if (![Utils isLocationValid:[Utils srcPositionCoor]]) {
            return;
        }
        
//        NSLog(@"--->%f,%f",locMgr.markerDrive.position.latitude, locMgr.markerDrive.position.longitude);
//        NSLog(@"--->%f,%f",[Utils srcPositionCoor].latitude,[Utils srcPositionCoor].longitude);
        
    }
    else {
        if (![Utils isLocationValid:[Utils destinationPositionCoor]]) {
            return;
        }
        
//        NSLog(@"D--->%f,%f",locMgr.markerDrive.position.latitude, locMgr.markerDrive.position.longitude);
//        NSLog(@"D--->%f,%f",[Utils srcPositionCoor].latitude,[Utils destinationPositionCoor].longitude);
    }
    
    NSString *theLocation1 = [NSString stringWithFormat:@"%@,%@",
                              [Utils formattedPosition:locMgr.markerDrive.position],
                              [Utils formattedPosition:locMgr.markerFinish.position]];
    
    if ([Utils checkStateUIInfo:UI_STATE_RIDER_FIT oldvalue:theLocation1] == UI_STATE_STEP_RUN) {
        return;
    }
    if ([Utils checkStateUIInfo:UI_STATE_RIDER_FIT oldvalue:theLocation1] == UI_STATE_STEP_FIN) {
        return;
    }
    [Utils setStateUIInfo:UI_STATE_RIDER_FIT value:UI_STATE_STEP_RUN oldvalue:theLocation1];
    
    NSArray* coord__ = [[NSArray alloc] initWithObjects:sValue1, eValue1, nil];
    
#ifdef _DRIVER_MODE
    [self updateMapFitBounds:coord__];
#else
    CLLocationDistance distance = GMSGeometryDistance([sValue1 MKCoordinateValue], [eValue1 MKCoordinateValue]);
    NSLog(@"distance:%f",distance);
    // 1000m 이내면 fit 하지 않고 zoom level을 조정한다.
    if (distance <= 1000) {
        [CATransaction begin];
        [CATransaction setAnimationDuration:0.5];
        
        GMSCameraPosition *camera;
        // driver 인 경우 rider
        if (self->appDelegate.isDriverMode) {
            camera = [GMSCameraPosition cameraWithLatitude:[eValue1 MKCoordinateValue].latitude
                                                 longitude:[eValue1 MKCoordinateValue].longitude
                                                      zoom:[self getZoomByState]
                                                   bearing:self.mapView_.camera.bearing
                                              viewingAngle:0];
        }
        // rider 인 경우 driver
        else {
            camera = [GMSCameraPosition cameraWithLatitude:[sValue1 MKCoordinateValue].latitude
                                                 longitude:[sValue1 MKCoordinateValue].longitude
                                                      zoom:[self getZoomByState]
                                                   bearing:self.mapView_.camera.bearing
                                              viewingAngle:0];
        }
        // 운행중일때는 zoom을 처리한다.
        
        [self.mapView_ animateToCameraPosition:camera];
        
        [CATransaction commit];
    }
    else {
        [self updateMapFitBounds:coord__];
    }
#endif
    
    if (!self->appDelegate.isDriverMode && self->appDelegate.ustate == 140) {
        //
        [locMgr updateStartMarker:[Utils srcPositionCoor]];
        
        [locMgr updateFinishMarker:[Utils destinationPositionCoor]];
    }
    
    [Utils setStateUIInfo:UI_STATE_RIDER_FIT value:UI_STATE_STEP_WAIT oldvalue:nil];
}

-(void)changeDestinationNotofication
{
    if (self->appDelegate.isDriverMode && self->appDelegate.pstate == 240 && self->appDelegate.pstate2 == 10) {
        // bg이고 목적지가 변경되었습으면 noti를 날린다.
        if ([UIApplication sharedApplication].applicationState == UIApplicationStateBackground) {
            [Utils sendNotofication:LocalizedStr(@"Map.Driver.DST.Change.text")];
            [self playTTS:LocalizedStr(@"Map.Driver.DST.Change.text") vol:1.0];
            [self performSelector:@selector(changeDestinationNotofication) withObject:nil afterDelay:5];
        }
    }
}

-(void)changePickupNotofication
{
    if (self->appDelegate.isDriverMode && self->appDelegate.pstate == 230 && self->appDelegate.pstate2 == 10) {
        // bg이고 목적지가 변경되었습으면 noti를 날린다.
        if ([UIApplication sharedApplication].applicationState == UIApplicationStateBackground) {
            [Utils sendNotofication:LocalizedStr(@"Map.Driver.SRC.Change.text")];
            [self playTTS:LocalizedStr(@"Map.Driver.SRC.Change.text") vol:1.0];
            [self performSelector:@selector(changePickupNotofication) withObject:nil afterDelay:5];
        }
    }
}

-(void)changePickupCancelNotofication
{
    //if (self->appDelegate.isDriverMode && self->appDelegate.pstate == 230 && self->appDelegate.pstate2 == 10)
    {
        // bg이고 목적지가 변경되었습으면 noti를 날린다.
        if ([UIApplication sharedApplication].applicationState == UIApplicationStateBackground) {
            [Utils sendNotofication:LocalizedStr(@"TTS.230.Pickup.Cancel.text")];
            [self playTTS:LocalizedStr(@"TTS.230.Pickup.Cancel.text") vol:1.0];
            [self performSelector:@selector(changePickupCancelNotofication) withObject:nil afterDelay:5];
        }
    }
}

/*
// 0:수정없음 1:출발 2:도착 3:출도착
- (NSInteger)changedAddress {
    
    BOOL __isSrc = NO;
    BOOL __isDst = NO;
    
    NSDictionary *pdevice = nil;
    
    if (self->appDelegate.isDriverMode) {
        if(self->appDelegate.pstate == 240 && self->appDelegate.pstate2 == 20) {
            return 0;
        }
        else if(self->appDelegate.pstate == 240) {
            pdevice = [self->appDelegate.dicDriver objectForKey:@"drive_trip"];
        }
        else if(self->appDelegate.pstate == 230) {
            pdevice = [self->appDelegate.dicDriver objectForKey:@"drive_pickup"];
        }
//        else if(self->appDelegate.pstate == 220) {
//            pdevice = [self->appDelegate.dicDrive objectForKey:@"drive_order"];
//        }
        else {
            return 0;
        }
    }
    else {
        if(self->appDelegate.ustate == 140) {
            pdevice = [self->appDelegate.dicRider objectForKey:@"drive_trip"];
        }
        else if(self->appDelegate.ustate == 130) {
            pdevice = [self->appDelegate.dicRider objectForKey:@"drive_pickup"];
        }
//        else if(self->appDelegate.ustate == 120) {
//            pdevice = [self->appDelegate.dicRider objectForKey:@"drive_order"];
//        }
        else {
            return 0;
        }
    }
    if (![Utils isDictionary:pdevice]) {
        return 0;
    }
    
#ifdef _DRIVER_MODE
    if (self->appDelegate.pstate == 230) {
        // lsrc가 존재하면
        if (![[Utils nullToString:[pdevice objectForKey:@"lsrc"]] isEqualToString:@""]) {
            // 위치
            CLLocationCoordinate2D position = [Utils getLocation:[pdevice objectForKey:@"lsrc"]];
            
            // 이전 위치와 다르면
            if (![Utils isEqualLocation:lsrc location:position]) {
                __isSrc = YES;
            }
        }
        return __isSrc;
    }
#else
    // lsrc가 존재하면
    if (self->appDelegate.ustate == 140 && self->appDelegate.ustate2 == 10) {
        if (![[Utils nullToString:[pdevice objectForKey:@"begin_location"]] isEqualToString:@""]) {
            // 위치
            CLLocationCoordinate2D position = [Utils getLocation:[pdevice objectForKey:@"begin_location"]];
            
            // 이전 위치와 다르면
            if (![Utils isEqualLocation:lsrc location:position]) {
                __isSrc = YES;
            }
        }
    }
    else {
        if (![[Utils nullToString:[pdevice objectForKey:@"lsrc"]] isEqualToString:@""]) {
            // 위치
            CLLocationCoordinate2D position = [Utils getLocation:[pdevice objectForKey:@"lsrc"]];
            
            // 이전 위치와 다르면
            if (![Utils isEqualLocation:lsrc location:position]) {
                __isSrc = YES;
            }
        }
    }
    
    
#endif
    
    // ldst가 존재하면
    if (![[Utils nullToString:[pdevice objectForKey:@"ldst"]] isEqualToString:@""]) {
        // 위치
        CLLocationCoordinate2D position = [Utils getLocation:[pdevice objectForKey:@"ldst"]];
        
        // 이전 위치와 다르면
        if (![Utils isEqualLocation:ldst location:position]) {
            __isDst = YES;
        }
    }
    
    if (__isSrc && __isDst) {
        return 3;
    }
    else if (__isDst) {
        return 2;
    }
    else if (__isSrc) {
        return 1;
    }
    return 0;
}
*/
- (void)updateDriveMarker
{
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    if (locMgr.markerDrive.map == nil || [Utils nullToImage:locMgr.markerDrive.icon]) {
        //NSDictionary *pvehicle = [self->appDelegate.dicRider objectForKey:@"provider_vehicle"];
        NSDictionary *pvehicle;
        if (self->appDelegate.isDriverMode) {
            pvehicle = [self->appDelegate.dicDriver objectForKey:@"provider_device"];
        }
        else {
            pvehicle = [self->appDelegate.dicRider objectForKey:@"provider_device"];
        }
        
        NSString *service = [pvehicle objectForKey:@"service"];
        locMgr.markerDrive.icon = [locMgr getMarkerIcon:service];
    }
}

- (void)updateDriveStepRider:(NSDictionary*)step__ {
    
    if (step__ == nil || step__ == (id)[NSNull null]) {
        return;
    }
    
    if (self->appDelegate.ustate == 140 && self->appDelegate.ustate2 == 10) {
        //
        NSDictionary *pdevice = [self->appDelegate.dicRider objectForKey:@"drive_trip"];
        
        // ldst가 존재하면
        if (![[Utils nullToString:[pdevice objectForKey:@"ldst"]] isEqualToString:@""] && [_dstLocationTextField.text isEqualToString:@""]) {
            //[self getRoute:[Utils currentPosition] dst:[Utils destinationPosition]];
            self->_lastRequestGoogleTrip = [[NSDate date] timeIntervalSince1970];
            
            NSArray* loc = [[pdevice objectForKey:@"ldst"] componentsSeparatedByString:@","];
            
            // 위치
            CLLocationCoordinate2D position;
            position.latitude  = [Utils formattedPositionCoor:[[loc objectAtIndex:0] doubleValue]];
            position.longitude = [Utils formattedPositionCoor:[[loc objectAtIndex:1] doubleValue]];
            
            [Utils setStateUIInfo:UI_STATE_END_ADDR value:UI_STATE_STEP_FIN oldvalue:nil];
            // 목적지 업데이트
            [self getDestinationAddress:position];
        }
    }
    
    NSArray* loc = [[step__ objectForKey:@"nlocation"] componentsSeparatedByString:@","];
    
    // 위치
    ldrv.latitude  = [Utils formattedPositionCoor:[[loc objectAtIndex:0] doubleValue]];
    ldrv.longitude = [Utils formattedPositionCoor:[[loc objectAtIndex:1] doubleValue]];
    
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    // 차령이동
    [self animateToNextCoord:locMgr.markerDrive nextLocation:ldrv];
    
    // 도착점
    //    [self updateFinishPosition:dstGeoStr];
    // eta업데이트
    // ETA
    [self print_eta:step__ toggle:0 api:NO];
}

#ifndef _DRIVER_MODE
- (void)blockSrcAddress {
    UIButton *btn1 = [_vAddress viewWithTag:10];
    
    if (btn1.enabled) {
        btn1.enabled = NO;
        _srcLocationTextField.enabled = NO;
    }
}

- (void)unblockSrcAddress {
    UIButton *btn1 = [_vAddress viewWithTag:10];
    
    if (!btn1.enabled) {
        btn1.enabled = YES;
        _srcLocationTextField.enabled = YES;
    }
}
#endif

- (void)blockDestinationAddress {
    UIButton *btn2 = [_vAddress viewWithTag:11];
    
    if (btn2.enabled) {
        btn2.enabled = NO;
        _dstLocationTextField.enabled = NO;
    }
}

- (void)unblockDestinationAddress {
    UIButton *btn2 = [_vAddress viewWithTag:11];
    
    if (!btn2.enabled) {
        btn2.enabled = YES;
        _dstLocationTextField.enabled = YES;
    }
}

-(void)tripEstimateUQ101:(NSDictionary*)result {
    
#ifndef _DRIVER_MODE
    // 목적지를 입력하면 현재 가까운 드라이버의 예상금액을 표시하지 않고 목적지에 대한 예상금액을 표시해야 한다.
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    dic = [result mutableCopy];
    dic[@"estimate_driver"] = [[NSArray alloc] initWithObjects:@"", nil];
    estimate_result = dic;
    
    [self initOrderSelect];
    [self updateUIServiceType];
    
#ifdef _WITZM
    [_btnRequest setTitleColor:UIColorDefault forState:UIControlStateNormal];
#elif defined _CHACHA
    [_btnRequest setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
#endif

    _btnRequest.enabled = YES;
#endif
}


-(void)onFail:(id)object error:(TXError *)error {
    
}

-(void)onEvent:(TXEvent *)event eventParams:(id)subscriptionParams {
    
    // logout 되었으면 메시지처리를 하지 않는다.
    //    utelno
    if (isLogout) {
        [[TXCallModel instance] onCleanSession];
        [Utils resetStateUIInfo];
        [self stopETClock];
        [self removeEventListeners];
        NSLog(@"STOP..");
        return;
    }
    
    [self hideBusyIndicator];
    
    NSLog(@"event.name:%@",event.name);
    
    // 최초 상태면 모든 loop를 clear한다.
    NSString* lastCode = @"";
    // profile image면 lopp 체크를 하지 않는다.
    if(![event.name isEqualToString:@"UR601"] &&
       ![event.name isEqualToString:@"UR602"] &&
       ![event.name isEqualToString:@"UR603"] &&
       ![event.name isEqualToString:@"PP601"] &&
       ![event.name isEqualToString:@"PP602"] &&
       ![event.name isEqualToString:@"PP603"]
       ) { // profile image
        lastCode = [self checkLoop];
        //NSLog(@"lastCode:%@",lastCode);
    }
    
    TXResponseDescriptor * descriptor = [event getEventProperty:TXEvents.Params.DESCRIPTOR];
    TXRequestObj *request = [event getEventProperty:TXEvents.Params.REQUEST];
    
    BOOL isNetworkError = [self catchNetworkError:descriptor];
    if (isNetworkError) {
        //[self alertError:@"Error" message:@"The network connection was lost."];
        //[self updateNetworkMonitor:NO];
    }
    else {
        //[self updateNetworkMonitor:YES];
    }
    
    if([event.name isEqualToString:@"UR110"]) { // 로그인
        [Utils setStateUIInfo:UI_STATE_UR110 value:UI_STATE_STEP_FIN oldvalue:nil];
        
        if(descriptor.success == true) {
            isLogout = NO;
            self->appDelegate.isDriverMode = NO;
#ifdef _DRIVER_MODE
            self->appDelegate.isDriverMode = YES;
#endif
            
//            if (!isBecomeActive) {
//                [self changeMode];
//            }
            [self onUR601];
            
            self->appDelegate.uistate  = 0;
            self->appDelegate.uistate2 = 0;
            
        } else {
            if (descriptor.rlt == -100) {
                
            }
            else if (descriptor.rlt != -1000 && descriptor.rlt != -999999) {
                //에러주석[self alertError:@"Error" message:descriptor.error];
            }
        }
        isBecomeActive = NO;
    }
    /*
    else if([event.name isEqualToString:@"UR114"]) { // 로그아웃
        
        if(descriptor.success == true) {
            isLogout = YES;
            
            [[TXCallModel instance] onCleanSession];
            
            [self cancelAllLoop];
            [self cancelTimeout];
            
            //remove old marker
            [self hideAllVehicle];
            
            // remove focus
            [self hideFocusPin];
            
            //[self resetDriverSlider]; // __DEBUG
            //[self onPP110];
            return;
            
        } else {
            if (descriptor.rlt != -1000 && descriptor.rlt != -999999) {
                [self alertError:@"Error" message:@"Failed to logout"];
            }
        }
        return;
    }
     */
    if([event.name isEqualToString:@"UR200"]) {
        
        if(descriptor.success == true) {
//            NSString     *response = [[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding];
//            NSDictionary *result   = getJSONObj(response);
            
            if (self->appDelegate.isDriverMode) {
                //                UILabel *lb = [_etaView viewWithTag:104];
                //                [lb setText:[Utils nameOfuname:[[result valueForKey:@"user"] valueForKey:@"name"]]];
                //                lb.hidden = NO;
            }
        } else {
            //에러주석[self alertError:@"Error" message:descriptor.error];
        }
    }
    else if([event.name isEqualToString:@"UR206"]) {
        
        if(descriptor.success == true) {
            NSString     *response = [[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding];
            NSDictionary *result   = getJSONObj(response);
            
            [self updatePrefer:result[@"user_matchs"]];
        } else {
            //에러주석[self alertError:@"Error" message:descriptor.error];
        }
    }
    else if([event.name isEqualToString:@"UR300"]) {
        
        if(descriptor.success == true) {
            NSString     *response = [[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding];
            NSDictionary *result   = getJSONObj(response);
            
            NSArray *tripData              = [result valueForKey:@"user_payments"];
            
            self->items = [NSMutableArray arrayWithCapacity:[tripData count]];
            
            for (NSDictionary* p in tripData) {
                [items addObject:p];
            }
            
            if ([self->items count]) {
                // 1. 정보 업데이트
                NSMutableDictionary *dic = [self->appDelegate.dicRider mutableCopy];
                [dic setObject:[result valueForKey:@"user_payments"] forKey:@"user_payments"];
                self->appDelegate.dicRider = dic;
            }
            else {
                NSMutableDictionary *dic = [self->appDelegate.dicRider mutableCopy];
                [dic removeObjectForKey:@"user_payments"];
                self->appDelegate.dicRider = dic;
            }
#ifndef _DRIVER_MODE
            NSString* no = [self getAvailableCardNo];
            if ([no isEqualToString:@""]) {
                no = @"-";
            }
            _lbCardInfo.text = no;
#endif
        } else {
            //    [self alertError:@"Error" message:@"Failed to get profile image"];
        }
    }
    else if([event.name isEqualToString:@"UR601"] ||
            [event.name isEqualToString:@"UR602"] ||
            [event.name isEqualToString:@"PP601"] ||
            [event.name isEqualToString:@"PP602"]
            ) { // profile image
        
        if([event.name isEqualToString:@"UR601"])
        {
            [Utils setStateUIInfo:UI_STATE_UR601 value:UI_STATE_STEP_FIN oldvalue:nil];
        }
        else if([event.name isEqualToString:@"UR602"])
        {
            [Utils setStateUIInfo:UI_STATE_UR602 value:UI_STATE_STEP_FIN oldvalue:nil];
        }
        else if([event.name isEqualToString:@"PP601"])
        {
            [Utils setStateUIInfo:UI_STATE_PP601 value:UI_STATE_STEP_FIN oldvalue:nil];
        }
        else if([event.name isEqualToString:@"PP602"])
        {
            [Utils setStateUIInfo:UI_STATE_PP602 value:UI_STATE_STEP_FIN oldvalue:nil];
        }
        
        // 드라이브모드고 account정보에서 호출된 정보면 무시한다. account를 수정할 수 도 있기때문에 아직 반영되지 않은 이미지므로 처리하면 않된다.
        if (self->appDelegate.isDriverMode &&
            ([self->appDelegate.mapVCBeforeVC isEqualToString:@"TXBecomeFirstVC"] ||
             [self->appDelegate.mapVCBeforeVC isEqualToString:@"TXBecomeVC"] ||
             [self->appDelegate.mapVCBeforeVC isEqualToString:@"TXProfileAccountVC"])) {
                return;
            }

        NSDictionary *param = [Utils getDataOfQueryString:request.reqUrl];
        
        if(descriptor.success == true) {
            NSString     *response = [[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding];
            NSDictionary *result   = getJSONObj(response);
            
            
            NSString *pictureData              = [result valueForKey:@"picture"];
            
            if ([pictureData length]) {
                
                //                NSData *data = [[NSData alloc] initWithBase64EncodedString:pictureData options:0];
                //                UIImage *image = [[UIImage alloc] initWithData:data];
                UIImage *image = [Utils decodeBase64ToImage:pictureData];
                // Store image
                //[self->appDelegate.cache storeObject:event forKey:request.reqUrl];
                
                if ([param valueForKey:@"name"]) {
                    if ([param[@"name"] isEqualToString:@"picture"]) {
                        [self saveProfileImage:image withCode:event.name];
                    }
                }
                else {
                    [self saveProfileImage:image withCode:event.name];
                }
                
            }
            else {
                NSLog(@"ic_user_white%d:%@",__LINE__,event.name);
                //[self saveProfileImage:[UIImage imageNamed:@"img_driver_small"] withCode:event.name];
                if([event.name isEqualToString:@"UR601"]) {
                    //[self saveProfileImage:[UIImage imageNamed:@"ic_user_white"] withCode:event.name];

#ifdef _DRIVER_MODE
                    [_btnRider setImage:[UIImage imageNamed:@"ic_user_white"] forState:UIControlStateNormal];
#endif
                }
                else {
                    //[self saveProfileImage:[UIImage imageNamed:@"img_driver_small"] withCode:event.name];
                    
#ifndef _DRIVER_MODE
                    _imgDriver.image = [UIImage imageNamed:@"img_driver_small"];
                    [_btnimgDriver setImage:[UIImage imageNamed:@"img_driver_small"] forState:UIControlStateNormal];
                    //[self shadowProfileImage:[UIImage imageNamed:@"img_driver_small"]];
#endif
                }
            }
            
        } else {
//            NSLog(@"ic_user_white%d:%@",__LINE__,event.name);
//            //[self saveProfileImage:[UIImage imageNamed:@"img_driver_small"] withCode:event.name];
//            if([event.name isEqualToString:@"UR601"]) {
//                [self saveProfileImage:[UIImage imageNamed:@"ic_user_white"] withCode:event.name];
//            }
//            else {
//                [self saveProfileImage:[UIImage imageNamed:@"img_driver_small"] withCode:event.name];
//            }
            if (descriptor.rlt != -1000 && descriptor.rlt != -999999) {
                //에러주석[self alertError:@"Error" message:descriptor.error];
            }
        }
    }
    else if([event.name isEqualToString:@"UR603"] || [event.name isEqualToString:@"PP603"]) { // vehicle image
        
        if(descriptor.success == true) {
            NSString     *response = [[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding];
            NSDictionary *result   = getJSONObj(response);
            
            
            NSString *pictureData              = [result valueForKey:@"picture"];
            
            if ([pictureData length]) {
                UIImage *image = [Utils decodeBase64ToImage:pictureData];
                // Store image
                //[self->appDelegate.cache storeObject:event forKey:request.reqUrl];
                
                [self saveProfileImage:image withCode:event.name];
            }
            else {
                NSLog(@"ic_user_white%d:%@",__LINE__,event.name);
                //[self saveProfileImage:[UIImage imageNamed:@"img_car_small"] withCode:event.name];
#ifndef _DRIVER_MODE
                _imgVehicle.image = [UIImage imageNamed:@"img_car_small"];
                [_btnimgVehicle setImage:[UIImage imageNamed:@"img_car_small"] forState:UIControlStateNormal];
#endif
            }
            
        } else {
            NSLog(@"ic_user_white%d:%@",__LINE__,event.name);
//            [self saveProfileImage:[UIImage imageNamed:@"img_car_small"] withCode:event.name];
            if (descriptor.rlt != -1000 && descriptor.rlt != -999999) {
                //에러주석[self alertError:@"Error" message:descriptor.error];
            }
        }
    }
#ifndef _DRIVER_MODE
    else if([event.name isEqualToString:@"UR303"]) { // my coupon조회
        
        if (![self->appDelegate.mapVCBeforeVC isEqualToString:@"TXPromotionsVC"]) {
            if(descriptor.success == true) {
                NSString     *response = [[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding];
                NSDictionary *result   = getJSONObj(response);
                
                NSArray *tripData              = [result valueForKey:@"user_codes"];
                
                dataArray = [NSMutableArray arrayWithCapacity:[tripData count]+1];
                
                //[dataArray addObject:LocalizedStr(@"Map.Vechile.Request.CouponCode.NoSelect.text")];
                [dataArray addObject:[NSNull null]];
                for (NSDictionary* p in tripData) {
                    NSString *state = [NSString stringWithFormat:@"%@",[p valueForKey:@"state"]];
                    if ([state isEqualToString:@"0"]) {
                        [dataArray addObject:p];
                    }
                }
                
                // 차량 선택화면 show
                UITableView* mTableView = [[UITableView alloc] init];
                mTableView.delegate=self;
                mTableView.dataSource=self;
                mTableView.allowsMultipleSelection = NO;
                mTableView.frame = CGRectMake(0.f, 0.f, 320, 176.f);
                mTableView.contentMode = UIViewContentModeScaleAspectFit;
                
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
                if (indexPath != nil) {
                    [mTableView selectRowAtIndexPath:indexPath
                                            animated:YES
                                      scrollPosition:UITableViewScrollPositionNone];
                    //[mTableView.delegate tableView:mTableView didSelectRowAtIndexPath:indexPath];
                }
                
                NSString *okbtn = LocalizedStr(@"Button.OK");
                if ([dataArray count] == 1) {
                    
                    mTableView.frame = CGRectMake(0.f, 0.f, 320, 150.f);
                    
                    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 40, 320, 110)];
                    
                    view.backgroundColor = [UIColor whiteColor];
                    
                    UILabel *lb = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, 110)];
                    lb.translatesAutoresizingMaskIntoConstraints = NO;
                    [lb setLineBreakMode:NSLineBreakByWordWrapping];
                    [lb setNumberOfLines:0];
                    [lb setTextAlignment:NSTextAlignmentCenter];
                    [lb setTextColor:HEXCOLOR(0x333333FF)];
                    lb.minimumScaleFactor = 0.5f;
                    lb.adjustsFontSizeToFitWidth = YES;
                    [lb setFont:[UIFont fontWithName:@"AppleSDGothicNeo-Regular" size:16]];
                    lb.text = LocalizedStr(@"Map.Vechile.Request.CouponCode.NoSelect.bottom.text");
                    
                    [view addSubview:lb];
                    [mTableView addSubview:view];
                    
                    okbtn = LocalizedStr(@"Map.Vechile.Request.CouponCode.BTN.book");
                }
                
                
                LGAlertView *alertView = [[LGAlertView alloc] initWithViewAndTitle:nil
                                                                           message:LocalizedStr(@"Map.Vechile.Request.CouponCode.text")
                                                                             style:LGAlertViewStyleAlert
                                                                              view:mTableView
                                                                      buttonTitles:@[okbtn]
                                                                 cancelButtonTitle:LocalizedStr(@"Button.CANCEL")
                                                            destructiveButtonTitle:nil
                                                                     actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                                                                         NSLog(@"actionHandler, %@, %lu", title, (long unsigned)index);
                                                                         
                                                                         if ([dataArray count] == 1) {
                                                                             TXPromotionsVC *mvc = [[TXPromotionsVC alloc] initWithNibName:@"TXPromotionsVC" bundle:nil];
                                                                             [self pushViewController:mvc];
                                                                         }
                                                                         else {
                                                                             for(NSIndexPath* indexPath in [mTableView indexPathsForSelectedRows]) {
                                                                                 NSLog(@"cell.tag:%@",[dataArray objectAtIndex:indexPath.row]);
                                                                                 
                                                                                 if (indexPath.row == 0) {
                                                                                     // code 초기화
                                                                                     promoCode = @"";
                                                                                     [[[TXApp instance] getSettings] setFDKeychain:@"promoCode" value:promoCode];
                                                                                     _lbCouponInfo.text = LocalizedStr(@"Map.Vechile.Request.CouponCode.title");
                                                                                 }
                                                                                 else {
                                                                                     NSDictionary *p = [dataArray objectAtIndex:indexPath.row];
                                                                                     
                                                                                     promoCode = [p valueForKey:@"code"];
                                                                                     [[[TXApp instance] getSettings] setFDKeychain:@"promoCode" value:promoCode];
                                                                                     //_lbCouponInfo.text = promoCode;
                                                                                     _lbCouponInfo.text = LocalizedStr(@"Map.Vechile.Request.CouponCode.text");
                                                                                 }
                                                                             }
                                                                         }
                                                                     }
                                                                     cancelHandler:^(LGAlertView *alertView) {
                                                                         NSLog(@"cancelHandler");
                                                                         
                                                                     }
                                                                destructiveHandler:^(LGAlertView *alertView) {
                                                                    NSLog(@"destructiveHandler");
                                                                }];
                [Utils initAlertButtonColor:alertView];
                alertView.windowLevel = LGAlertViewWindowLevelAboveStatusBar;
                [alertView showAnimated:YES completionHandler:nil];
                
                
                
            } else {
                //    [self alertError:@"Error" message:@"Failed to get profile image"];
            }
        }
        
    }
#endif
    else if([event.name isEqualToString:@"UQ002"]) { // 가능한 차량 조회 polling
        
        if (self->appDelegate.ustate == 0) {
            // 로그인 해야 한다.
            [self onUR110];
            return;
        }
        else if(descriptor.success == true) {
            
            NSString     *response = [[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding];
            NSDictionary *result   = getJSONObj(response);
            
            NSArray *driver = [result objectForKey:@"provider_devices"];
            
            // 예상금액
            //estimate_result = result;
            
            //NSLog(@"self.markers:%@",self.markers);
            //Marker
            NSDictionary *oldMarkers = [self.markers copy];
            self.markers = [[NSMutableDictionary alloc] init];
            for (id key in driver) {
                TXTaxi *taxi = [[TXTaxi alloc] init];
                taxi.provider = key;
                
                NSString *location = [taxi.provider valueForKey:@"location"];
                CLLocationCoordinate2D new_position = [Utils getLocation:location];
                CLLocationCoordinate2D old_position = kCLLocationCoordinate2DInvalid;
                
                GMSMarker *_marker;
                if ([oldMarkers.allKeys containsObject:[taxi.provider valueForKey:@"vseq"]]) {
                    _marker = [oldMarkers objectForKey:[taxi.provider valueForKey:@"vseq"]];
                    old_position = _marker.position;
                } else {
                    //Create new Marker
                    
                    _marker = [self addMarker:new_position withTaxi:taxi];
                    old_position = new_position;
                }
                _marker.userData = taxi;
                [self animateToNextCoord:_marker nextLocation:new_position];
                _marker.map = self.mapView_; // 무조건 차량을 보여준다 -> 차량을 보여 주지 않는다.
                [self.markers setObject:_marker forKey:[taxi.provider valueForKey:@"vseq"]];
            }
            //remove old marker
            for (id key in oldMarkers) {
                GMSMarker *_marker = (GMSMarker *)oldMarkers[key];
                if (!([self.markers.allValues containsObject:_marker])) {
                    _marker.map = nil;
                    _marker = nil;
                }
            }
            //NSLog(@"self.markers:%@",self.markers);
            //[self onUpdateUI:@{@"result1":@"data1"}];
            
        } else {
            if (descriptor.rlt != -1000 && descriptor.rlt != -999999) {
                //에러주석[self alertError:@"Error" message:descriptor.error];
                //[self alertError:@"Error" message:@"Network is not stabled!"];
            }
            
        }
        [self callLoop:event.name withCancel:YES andDirect:NO];
    }
    else if([event.name isEqualToString:@"UD120"]) { // 요청
        
        [Utils setStateUIInfo:UI_STATE_UD120 value:UI_STATE_STEP_FIN oldvalue:@""];
        
        if(descriptor.success == true) {
            
            [self initStartEndPosition];
            
            NSString     *response = [[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding];
            NSDictionary *responseObj   = getJSONObj(response);
            
            BOOL isPaymentCard = NO;
            NSString*   pay_token = [Utils nullToString:[responseObj objectForKey:SettingsConst.CryptoKeys.PAY_TOKEN]];
            if (![pay_token isEqualToString:@""]) {
                //[self cancelAllLoop];
                // paytoken처리
                
                //NSString* oseq   = [[responseObj valueForKey:@"provider_device"] valueForKey:@"oseq"];
                //                NSString* vseq   = [[responseObj valueForKey:@"provider_device"] valueForKey:@"vseq"];
                //                //NSString* pseq   = [[responseObj valueForKey:@"provider_device"] valueForKey:@"pseq"];
                //
                //                [[[TXApp instance] getSettings] setFDKeychain:SettingsConst.CryptoKeys.OSEQ value:oseq];
                //                //[[[TXApp instance] getSettings] setFDKeychain:SettingsConst.CryptoKeys.PSEQ value:pseq];
                //                [[[TXApp instance] getSettings] setFDKeychain:SettingsConst.CryptoKeys.VSEQ value:vseq];
                
                id   user_payment = [self->appDelegate.dicRider objectForKey:@"user_payments"];
                
                for (NSDictionary* key in user_payment) {
                    NSString* no     = [key valueForKey:@"no"];
                    //NSString* cvv    = [key valueForKey:@"cvv"];
                    NSString* expire = [key valueForKey:@"expire"];
                    NSString* mode   = [key valueForKey:@"mode"];
                    NSString* state  = [key valueForKey:@"state"];
                    NSString* cseq   = [key valueForKey:@"cseq"];
                    
                    NSLog(@"key:%@",key);
                    
                    //선택된 카드의 인증을 한다.
                    if ([mode isEqualToString:@"1"] && [state isEqualToString:@"1"] && ![no isEqualToString:@""] && expire && ![expire isEqualToString:@""]) {
                        
                        [[[TXApp instance] getSettings] setFDKeychain:SettingsConst.CryptoKeys.CSEQ value:cseq];
                        
                        [[[TXApp instance] getSettings] setFDKeychain:SettingsConst.CryptoKeys.PAY_TOKEN value:pay_token];
                        
                        // pay_nonce호출처리한다.
                        
                        isPaymentCard = YES;
                        /*

                        NSString *month = [expire substringToIndex:2];
                        NSString *year  = [expire substringFromIndex:3];
                        //NSString *month = [NSString stringWithFormat:@"20%@",[expire substringFromIndex:2]];
                        NSLog(@"%@,%@",year,month);
                        NSDictionary *propertyMap = @{
                                                      @"clientToken" : pay_token,
                                                      @"cvv"  : cvv,
                                                      @"cardNumber" : no,
                                                      @"month" : month,
                                                      @"year" : year,
                                                      @"mid" : @"UD112",
                                                      @"cseq" : cseq,
                                                      @"oseq" : oseq,
                                                      };

                        [self->model BTREE:propertyMap];

                         */
                        break;
                    }
                }
                
            }
            else {
                isPaymentCard = YES;
            }
            
            if (isPaymentCard) {
                [self callLoop:@"UQ100" withCancel:YES andDirect:NO];
                [self callLoop:@"UD131" withCancel:NO andDirect:NO];
#ifndef _DRIVER_MODE
                vcMapCall = [[TXMapCallVC alloc] initWithNibName:@"TXMapCallVC" bundle:nil];
                vcMapCall.view.frame = self.view.frame;
                [vcMapCall initAddress:_srcLocationTextField.text dest:_dstLocationTextField.text];
                
                if ([self.presentedViewController isKindOfClass:TXDestinationsVC.class]) {
                    [self.presentedViewController dismissViewControllerAnimated:NO completion:nil];
                }
            
                [self presentViewController:vcMapCall animated:YES completion:nil];
#endif
            }
            else {
                // __DEBUG
                /*
                 [self callLoop:@"UQ100" withCancel:YES andDirect:NO];
                 [self callLoop:@"UD131" withCancel:NO andDirect:NO];
                 [self showWatingAnimation];
                 */
//                [self showVehicleView];
                [self alertError:@"" message:LocalizedStr(@"Map.Rider.Request.Card.Cancel.text")]; //i18n
            }
            
        } else {
            // 요청전 정보를 초기화 하지 않는다.
#ifndef _DRIVER_MODE
            // 카드등록으로 이동
            //if (descriptor.orgrlt == -111) {
            if (descriptor.orgrlt == -115) {
                _isViewChanged = YES;
                TXPaymentVC *firstVC = [[TXPaymentVC alloc] initWithNibName:@"TXPaymentVC" bundle:nil];
                [self pushViewController:firstVC];
                //
                //                return;
                
                [self callLoop:@"UQ002" withCancel:YES andDirect:NO];
            }
            else {
                [self callLoop:@"UQ002" withCancel:YES andDirect:NO];
            }
            
            if (descriptor.rlt != -1000 && descriptor.rlt != -999999) {
                //에러주석[self alertError:@"Error" message:descriptor.error];
            }
#endif
        }
    }
    else if([event.name isEqualToString:@"UD112"]) { // pay_nonce
        
        if(descriptor.success == true) {
            
            //            NSString     *response = [[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding];
            //            NSDictionary *responseObj   = getJSONObj(response);
            
            //            NSString* oseq   = [[responseObj valueForKey:@"provider_device"] valueForKey:@"oseq"];
            //            NSString* vseq   = [[responseObj valueForKey:@"provider_device"] valueForKey:@"vseq"];
            //            NSString* pseq   = [[responseObj valueForKey:@"provider_device"] valueForKey:@"pseq"];
            
            //            [[[TXApp instance] getSettings] setFDKeychain:SettingsConst.CryptoKeys.OSEQ value:oseq];
            //            [[[TXApp instance] getSettings] setFDKeychain:SettingsConst.CryptoKeys.PSEQ value:pseq];
            //            [[[TXApp instance] getSettings] setFDKeychain:SettingsConst.CryptoKeys.VSEQ value:vseq];
            
            [self callLoop:@"UQ100" withCancel:YES andDirect:YES];
            [self callLoop:@"UD131" withCancel:NO andDirect:NO];
            
        } else {
            [self callLoop:@"UQ002" withCancel:YES andDirect:NO];
            if (descriptor.rlt != -1000 && descriptor.rlt != -999999) {
                //에러주석[self alertError:@"Error" message:descriptor.error];
            }
        }
    }
    else if([event.name isEqualToString:@"UD110"]) { // 목적지 주소 변경
        
        if(descriptor.success == true) {
            
            // 목적지 UI 표시 (google 주소)
            //[self updateDriveDestination];
            [self updateRealStartEndAddress:YES];
        } else {
            [self updateRealStartEndAddress:NO];
            if (descriptor.rlt != -1000 && descriptor.rlt != -999999) {
                //에러주석[self alertError:@"Error" message:descriptor.error];
            }
        }
    }
    else if([event.name isEqualToString:@"UQ101"]) { // 예상시간
        
        if(descriptor.success == true) {
            
            NSString     *response = [[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding];
            NSDictionary *responseObj   = getJSONObj(response);
            
            // 가격 "fare_min":"5.50" "fare_max":"8.25" vehicle view에 노출한다.
            // __DEBUG
            
            // 예상금액
            //[self showBusyIndicator:@"Canceling Order ... "];
            [self tripEstimateUQ101:responseObj];
            [self hideBusyIndicator];
            
        } else {
            if (descriptor.rlt != -1000 && descriptor.rlt != -999999) {
                //에러주석[self alertError:@"Error" message:descriptor.error];
            }
        }
    }
    else if([event.name isEqualToString:@"UQ100"]) { // 요청상태조회
        
        if(descriptor.success == true) {
            
            // 목적지 UI 표시 (google 주소)
            //[self updateDriveDestination];
            
            [self callLoop:@"UQ100" withCancel:YES andDirect:NO lastCode:lastCode];
        } else {
            if (descriptor.rlt != -1000 && descriptor.rlt != -999999) {
                //에러주석[self alertError:@"Error" message:descriptor.error];
            }
        }
    }
    else if([event.name isEqualToString:@"UD130"]) { // 요청취소
        
        if(descriptor.success == true) {
            [self cancelTimeout];
            [self pushCpos:nil];// 취소되면 목적지를 지우지 않는다.
        } else {
            if (descriptor.rlt != -1000 && descriptor.rlt != -999999) {
                //에러주석[self alertError:@"Error" message:descriptor.error];
            }
        }
    }
    else if([event.name isEqualToString:@"UD131"]) { // 요청timeout
        
        [Utils setStateUIInfo:UI_STATE_UD131 value:UI_STATE_STEP_FIN oldvalue:nil];
        
        if(descriptor.success == true) {
            [self pushCpos:nil];// 취소되면 목적지를 지우지 않는다.
        } else {
            if (descriptor.rlt != -1000 && descriptor.rlt != -999999) {
                //에러주석[self alertError:@"Error" message:descriptor.error];
            }
        }
    }
    // 130
    else if([event.name isEqualToString:@"UQ200"]) { // pickup polling
        
        if(descriptor.success == true) {
            // 지도에 위치 표시
            //[self updateDriveMarker];
        } else {
            if (descriptor.rlt != -1000 && descriptor.rlt != -999999) {
                //에러주석[self alertError:@"Error" message:descriptor.error];
            }
        }
    }
    else if([event.name isEqualToString:@"UD210"]) { // 목적지 변경
        
        if(descriptor.success == true) {
            // 지도에 목적지 정보 변경
            //[self updateDriveDestination];
            [self updateRealStartEndAddress:YES];
        } else {
            [self updateRealStartEndAddress:NO];
            if (descriptor.rlt != -1000 && descriptor.rlt != -999999) {
                //에러주석[self alertError:@"Error" message:descriptor.error];
            }
        }
    }
    else if([event.name isEqualToString:@"UD230"]) { // 사용자 취소 (5분경과)
        
        if(descriptor.success == true) {
            // 110 처리됨 초기화 해야 함.
            //[self initTripViewIncludeAddress];
        } else {
            if (descriptor.rlt != -1000 && descriptor.rlt != -999999) {
                //에러주석[self alertError:@"Error" message:descriptor.error];
            }
        }
    }
    
    // 140
    else if([event.name isEqualToString:@"UQ300"]) { // trip polling
        
        if(descriptor.success == true) {
            // 지도에 위치 표시
            /*
             "drive_trip_step" =     {
             edistance = "0.00";
             eduration = "0.00";
             ndistance = "0.2";
             nlocation = "37.629444,126.834267";
             ntime = 15;
             oseq = 3213;
             };
             */
            NSDictionary *dic = [self->appDelegate.dicRider objectForKey:@"drive_trip_step"];
            
            //[self updateDriveMarker];
            [self updateDriveStepRider:dic];
            
        } else {
            if (descriptor.rlt != -1000 && descriptor.rlt != -999999) {
                //에러주석[self alertError:@"Error" message:descriptor.error];
            }
        }
    }
    else if([event.name isEqualToString:@"UD310"]) { // rider 목적지 변경
        
        if(descriptor.success == true) {
            // 지도에 목적지 정보 변경
            //[self updateDriveDestination];
            [self updateRealStartEndAddress:YES];
            [self pushCpos:btnCpos];
        } else {
            [self updateRealStartEndAddress:NO];
            if (descriptor.rlt != -1000 && descriptor.rlt != -999999) {
                //에러주석[self alertError:@"Error" message:descriptor.error];
            }
        }
    }
    //************************************************************************************************************************
    else if([event.name isEqualToString:@"PP110"]) { // driver login
        
        [Utils setStateUIInfo:UI_STATE_PP110 value:UI_STATE_STEP_FIN oldvalue:nil];
        if(descriptor.success == true) {
            isLogout = NO;
            self->appDelegate.isDriverMode = YES;
#ifdef _DRIVER_MODE
            self->appDelegate.isDriverMode = YES;
#endif
            
//            if (!isBecomeActive) {
//                [self changeMode];
//            }
            [self onPP602];
            
            NSDictionary *dic = [self->appDelegate.dicDriver objectForKey:@"provider_device"];
            if (![Utils isDictionary:dic]) {
                dic = [self->appDelegate.dicDriver objectForKey:@"provider_device"];
            }
            if ([Utils isDictionary:dic]) {
                
                NSArray *provider_vehicles = [self->appDelegate.dicDriver objectForKey:@"provider_vehicles"];
                if (![Utils isDictionary:dic]) {
                    provider_vehicles = [self->appDelegate.dicDriver objectForKey:@"provider_vehicles"];
                }
                NSString *vseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.VSEQ];
                for(NSDictionary *vehicle in provider_vehicles) {
                    if ([vehicle[@"vseq"] isEqualToString:vseq] ) {
                        [self setVehicleInfo:vehicle];
                        break;
                    }
                }
                
                int state = [dic[@"state"] intValue];
                if (state > 230) {
                    [self onPQ300];
                }
                else if (state >= 220) {
                    [self onPQ200];
                }
            }
            self->appDelegate.uistate  = 0;
            self->appDelegate.uistate2 = 0;
            
        } else {
            if (descriptor.rlt == -100) {
                
            }
            else if (descriptor.rlt != -1000 && descriptor.rlt != -999999) {
                //에러주석[self alertError:@"Error" message:descriptor.error];
            }
        }
        isBecomeActive = NO;
    }
    /*
    else if([event.name isEqualToString:@"PP114"]) { // logout
        
        if(descriptor.success == true) {
            // sider 초기화
            //self.slideView.tag = 0;
            
            isLogout = YES;
            [[TXCallModel instance] onCleanSession];
            
        } else {
            if (descriptor.rlt != -1000 && descriptor.rlt != -999999) {
                //에러주석[self alertError:@"Error" message:descriptor.error];
            }
        }
     return;
    }
     */
    else if([event.name isEqualToString:@"PD015"]) { // vehicle등록
        
        if(descriptor.success == true) {
            //
            [self callLoop:@"PQ100" withCancel:YES andDirect:NO];
        } else {
            [self resetVehicleInfo];
            //[self etaDriverType220_10:@" " str5:@""];
            if (descriptor.rlt != -1000 && descriptor.rlt != -999999) {
                //에러주석[self alertError:@"Error" message:descriptor.error];
            }
        }
    }
    else if([event.name isEqualToString:@"PD035"]) { // vehicle해제
        
        if(descriptor.success == true) {
            // sider 초기화
            //self.slideView.tag = 0;
            
            //selectedVechicleInfo = @"";
            //[[[TXApp instance] getSettings] setFDKeychain:@"selectedVechicleInfo" value:@""];
            //[self etaDriverType220_10:@" " str5:@""];
            //
            //self->appDelegate.isDriverMode = NO;
            //[self clearMode];
            //[self onUR110];
        } else {
            if (descriptor.rlt != -1000 && descriptor.rlt != -999999) {
                //에러주석[self alertError:@"Error" message:descriptor.error];
            }
        }
    }
#ifdef _DRIVER_MODE
    else if([event.name isEqualToString:@"PP116"]) { // 시험운행등록
        
        if(descriptor.success == true) {
            [[[TXApp instance] getSettings] setDriveMode:DRIVE_MODE_TRAINING];
            //
//            self->appDelegate.uistate = 0;
//            self->appDelegate.uistate2 = 0;
            
            [self callLoop:@"PQ100" withCancel:YES andDirect:YES];
            [super statusBarStyleTestMode];
            
            
            [self configureUpdatingDriver];
            // ui refresh
            //self->appDelegate.pstate2 = -30;
        } else {
            [[[TXApp instance] getSettings] setDriveMode:DRIVE_MODE_DRIVING];
            
            [self resetVehicleInfo];
            
            //[self etaDriverType220_10:@" " str5:@""];
            if (descriptor.rlt != -1000 && descriptor.rlt != -999999) {
                //에러주석[self alertError:@"Error" message:descriptor.error];
            }
        }
    }
    else if([event.name isEqualToString:@"PP136"]) { // 시험운행해제
        
        if(descriptor.success == true) {
            [[[TXApp instance] getSettings] setDriveMode:DRIVE_MODE_DRIVING];
            [super statusBarStyleWhite];
            // sider 초기화
            //self.slideView.tag = 0;
            
            [self resetVehicleInfo];
            
            //[self etaDriverType220_10:@" " str5:@""];
            
            //
//            self->appDelegate.uistate = 0;
//            self->appDelegate.uistate2 = 0;
            [Utils setStateUIInfo:UI_STATE_PQ100 value:UI_STATE_STEP_FIN oldvalue:nil];
            //[self callLoop:@"PQ100" withCancel:YES andDirect:NO];
            [self onPP110];
            [self configureUpdatingDriver];
            return;
            
            //
            //self->appDelegate.isDriverMode = NO;
            //[self clearMode];
            //[self onUR110];
        } else {
            if (descriptor.rlt != -1000 && descriptor.rlt != -999999) {
                //에러주석[self alertError:@"Error" message:descriptor.error];
            }
        }
    }
#endif
    else if([event.name isEqualToString:@"PP206"]) {
        
        if(descriptor.success == true) {
            NSString     *response = [[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding];
            NSDictionary *result   = getJSONObj(response);
            
            [self updatePrefer:result[@"provider_matchs"]];

        } else {
            //에러주석[self alertError:@"Error" message:descriptor.error];
        }
    }
    else if([event.name isEqualToString:@"PQ002"]) { // surge rate
        
        if(descriptor.success == true) {
            //
            if (self->appDelegate.pstate == 200) {
                
                [self resetPolygon];
                
                //                query/surge 색상 표현 범위  None: <1.5, Yellow: 1.5~2.0, Orange: 2.0~2.5,Red: >2.5
                //                transparancy 70%
                
                NSString     *response = [[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding];
                NSDictionary *responseObj   = getJSONObj(response);
                
                NSArray *countries = responseObj[@"drive_surge"][@"features"];
                
                for (NSDictionary *__country in countries) {
                    
                    NSDictionary *geometry = __country[@"geometry"];
                    if (![Utils isDictionary:geometry]) {
                        continue;
                    }
                    
                    if ([geometry[@"type"] isEqualToString:@"Polygon"] || [geometry[@"type"] isEqualToString:@"MultiPolygon"]) {
                        NSString *title = __country[@"properties"][@"name"];
                        float fare_rate = [__country[@"properties"][@"fare_rate"] floatValue];
                        
                        // 0 이면 ploygon, >0면 hole
                        int polyCount = 0;
                        
                        GMSMutablePath *path = [GMSMutablePath path];
                        GMSMutablePath *hole = [GMSMutablePath path];
                        for (NSArray *polygonData in geometry[@"coordinates"]) {
                            
                            NSInteger coordIdx = [polygonData count];
                            
                            for (int i = 0; i < coordIdx; i++)
                            {
                                CLLocationCoordinate2D coord = CLLocationCoordinate2DMake([polygonData[i][1] doubleValue], [polygonData[i][0] doubleValue]);
                                if (polyCount == 0) {
                                    [path addCoordinate:coord];
                                }
                                else {
                                    [hole addCoordinate:coord];
                                }
                            }
                            polyCount++;
                        }
                        [self drawPolygon:path hole:hole title:title fare:fare_rate];
                    }
                    else {
                        NSLog(@"Unsupported type: %@", geometry[@"type"]);
                    }
                }
                
                //                [self cancelTimeout];
                //                [self callLoop:@"PQ002" withCancel:NO andDirect:YES];
            }
            
        } else {
            if (descriptor.rlt != -1000 && descriptor.rlt != -999999) {
                //에러주석[self alertError:@"Error" message:descriptor.error];
            }
        }
        [self callLoop:event.name withCancel:YES andDirect:NO];
    }
    else if([event.name isEqualToString:@"PQ100"]) { // order polling
        
        [Utils setStateUIInfo:UI_STATE_PQ100 value:UI_STATE_STEP_FIN oldvalue:nil];
        
        if(descriptor.success == true) {
            //
            if (_isDriveMode) {
                //
                if (self->appDelegate.pstate < 230 && self->appDelegate.pstate > 210) {
                    //[self onUR200]; // 탑승자 정보 조회
                    //[self onPD501]; // 탑승자 정보 조회
                    
                    [self cancelTimeout];
                    //[self callLoop:@"PD220" withCancel:NO andDirect:YES];
                }
                else {
                    [self callLoop:@"PQ100" withCancel:YES andDirect:NO];
                }
                _isDriveMode = NO;
                
            }
            else {
                //                if (self->appDelegate.pstate < 230) {
                //                    [self callLoop:@"PQ100" withCancel:YES andDirect:NO];
                //                }
                //                else {
                //                    [self callLoop:@"PD220" withCancel:YES andDirect:YES];
                //                }
                
                if (self->appDelegate.pstate == 220 && self->appDelegate.pstate2 == 20) {
                    [self callLoop:@"PD220" withCancel:YES andDirect:YES];
                }
                else if (self->appDelegate.pstate < 230) {
                    [self callLoop:@"PQ100" withCancel:YES andDirect:NO];
                }
            }
            
        } else {
            if (descriptor.rlt != -1000 && descriptor.rlt != -999999) {
                //에러주석[self alertError:@"Error" message:descriptor.error];
            }
        }
    }
    else if([event.name isEqualToString:@"PD110"]) { // order accept
        
        if(descriptor.success == true) {
            //
            //[self onUR200]; // 탑승자 정보 조회
            //[self onUR601]; // 탑승자 정보 조회
            [self onUR601]; // 탑승자 정보 조회
            
            //[self hidePickupMarker];
            [self cleanTripMapOnly];
            [self cleanMapOnly];
            
            [self cancelTimeout];
            [self callLoop:@"PD220" withCancel:NO andDirect:YES];
        } else {
            if (descriptor.rlt != -1000 && descriptor.rlt != -999999) {
                //에러주석[self alertError:@"Error" message:descriptor.error];
            }
        }
    }
    else if([event.name isEqualToString:@"PD130"]) { // 요청취소
        
        if(descriptor.success == true) {
            [self cancelTimeout];
            isRunTimeout_ = NO;
            
            // 상태 초기화
            [appDelegate resetTripInfo];
            // testdrive인 경우 재시도 확인 창
            [self testModeRetryAlert];
            
            // 110 처리됨 초기화 해야 함.
            //[self initTripViewIncludeAddress];
        } else {
            if (descriptor.rlt != -1000 && descriptor.rlt != -999999) {
                //에러주석[self alertError:@"Error" message:descriptor.error];
            }
        }
    }
    else if([event.name isEqualToString:@"PD330"]) { // trip del
        
        if(descriptor.success == true) {
            // 상태 초기화
            if ((self->appDelegate.pstate == 230 || self->appDelegate.pstate == 240) && self->appDelegate.pstate2 <= -10) {
                // cancel인 경우 초기화 하지 않는다.
            }
            else {
                [appDelegate resetTripInfo];
            }
            
            [self cancelTimeout];
            isRunTimeout_ = NO;
            
            // testdrive인 경우 재시도 확인 창
            //[self testModeRetryAlert];
            
            // 110 처리됨 초기화 해야 함.
            //[self initTripViewIncludeAddress];
            
        } else {
            if (descriptor.rlt != -1000 && descriptor.rlt != -999999) {
                //에러주석[self alertError:@"Error" message:descriptor.error];
            }
        }
    }
    else if([event.name isEqualToString:@"PD232"]) { // 요청취소
        
        if(descriptor.success == true) {
            [self cancelTimeout];
            isRunTimeout_ = NO;
            
            // 상태 초기화
            if ((self->appDelegate.pstate == 230 || self->appDelegate.pstate == 240) && self->appDelegate.pstate2 <= -10) {
                // cancel인 경우 초기화 하지 않는다.
            }
            else {
                [appDelegate resetTripInfo];
            }
            
            // testdrive인 경우 재시도 확인 창
            //[self testModeRetryAlert];
            
            // 110 처리됨 초기화 해야 함.
            //[self initTripViewIncludeAddress];
        } else {
            if (descriptor.rlt != -1000 && descriptor.rlt != -999999) {
                //에러주석[self alertError:@"Error" message:descriptor.error];
            }
        }
    }
    else if([event.name isEqualToString:@"PD131"]) { // 요청timeout
        
        [Utils setStateUIInfo:UI_STATE_PD131 value:UI_STATE_STEP_FIN oldvalue:nil];
        
        if(descriptor.success == true) {
            NSLog(@"timeout");
            
            // 상태 초기화
            [appDelegate resetTripInfo];
            isRunTimeout_ = NO;
            //[self cleanEtaType];
            //[self etaDriverType220_10:@" " str5:@" "];
            
            // testdrive인 경우 재시도 확인 창
            [self testModeRetryAlert];
        } else {
            if (descriptor.rlt != -1000 && descriptor.rlt != -999999) {
                //에러주석[self alertError:@"Error" message:descriptor.error];
            }
        }
    }
    else if([event.name isEqualToString:@"PQ200"]) { // pickup 상태조회
        
        [Utils setStateUIInfo:UI_STATE_PQ200 value:UI_STATE_STEP_FIN oldvalue:nil];
        
        if(descriptor.success == true) {
            [self callLoop:@"PQ200" withCancel:YES andDirect:NO lastCode:lastCode];
        } else {
            if (descriptor.rlt != -1000 && descriptor.rlt != -999999) {
                //에러주석[self alertError:@"Error" message:descriptor.error];
            }
        }
    }
    else if([event.name isEqualToString:@"PQ300"]) { // trip 상태조회
        
        [Utils setStateUIInfo:UI_STATE_PQ300 value:UI_STATE_STEP_FIN oldvalue:nil];
        
        if(descriptor.success == true) {
            [self callLoop:@"PQ300" withCancel:YES andDirect:NO lastCode:lastCode];
        } else {
            if (descriptor.rlt != -1000 && descriptor.rlt != -999999) {
                //에러주석[self alertError:@"Error" message:descriptor.error];
            }
        }
    }
    else if([event.name isEqualToString:@"PD220"]) { // pickup request
        
        if(descriptor.success == true) {
            // 지도를 그린다.
            //[self drawRoute];
            //[self updateTripLineClock];
            
            //[self callLoop:@"PD211" withCancel:YES andDirect:NO];
        } else {
            if (descriptor.rlt != -1000 && descriptor.rlt != -999999) {
                //에러주석[self alertError:@"Error" message:descriptor.error];
            }
        }
    }
    else if([event.name isEqualToString:@"PD211"]) { // pickup step
        // timer 시간 초기화
        self->_lastReportTime = [[NSDate date] timeIntervalSince1970];
        [Utils setStateUIInfo:UI_STATE_PD211 value:UI_STATE_STEP_FIN oldvalue:nil];
        
        if(descriptor.success == true) {
            // 지도 업데이트 __DEBUG
            //[self callLoop:@"PD211" withCancel:NO andDirect:NO];
            
        } else {
            if (descriptor.rlt != -1000 && descriptor.rlt != -999999) {
                //에러주석[self alertError:@"Error" message:descriptor.error];
            }
        }
    }
    else if([event.name isEqualToString:@"PD213"]) { // pickup arrive
#ifdef _DRIVER_MODE
        if(descriptor.success == true) {
            //[self hidePickupMarker]; // 탑승자의 pickup위치
            self->_MAX_etTimer = 1;
            [self stopETClock];
            
        } else {
            [self releaseLockIt];
            if (descriptor.rlt != -1000 && descriptor.rlt != -999999) {
                //에러주석[self alertError:@"Error" message:descriptor.error];
            }
        }
#endif
    }
    else if([event.name isEqualToString:@"PD320"]) { // trip begine
#ifdef _DRIVER_MODE
        if(descriptor.success == true) {
            // 지도를 그린다.
            [self stopETClock];
            
            //[self drawRoute];
            //[self updateTripLineClock];
            
            //[self callLoop:@"PD311" withCancel:YES andDirect:NO];
            //[self callLoop:@"PD311" withCancel:YES andDirect:NO lastCode:lastCode];
        } else {
            [self releaseLockIt];
            
            if (descriptor.rlt != -1000 && descriptor.rlt != -999999) {
                //에러주석[self alertError:@"Error" message:descriptor.error];
            }
        }
#endif
    }
    else if([event.name isEqualToString:@"PD310"]) { // 목적지 변경
        
        if(descriptor.success == true) {
            // 목적지 변경
            //[self updateDriveDestination];
            [self updateRealStartEndAddress:YES];
            // 현 위치로 이동
//            isMoveMapBYGesture = NO;
//            [self updateMapMoveTimer];
//            [self setTimerMoveToCurrentPosition:5];
            
        } else {
            [self updateRealStartEndAddress:NO];
            if (descriptor.rlt != -1000 && descriptor.rlt != -999999) {
                //에러주석[self alertError:@"Error" message:descriptor.error];
            }
        }
    }
    else if([event.name isEqualToString:@"PD311"]) { // trip step
        
        // timer 시간 초기화
        self->_lastReportTime = [[NSDate date] timeIntervalSince1970];
        [Utils setStateUIInfo:UI_STATE_PD311 value:UI_STATE_STEP_FIN oldvalue:nil];
        
        if(descriptor.success == true) {
            // 지도 업데이트 __DEBUG
            //[self callLoop:@"PD311" withCancel:NO andDirect:NO];
            
        } else {
            if (descriptor.rlt != -1000 && descriptor.rlt != -999999) {
                //에러주석[self alertError:@"Error" message:descriptor.error];
            }
        }
    }
    else if([event.name isEqualToString:@"PD313"]) { // trip end
#ifdef _DRIVER_MODE
        if(descriptor.success == true) {
            // 지도 업데이트 __DEBUG
            //[self callLoop:@"PD311" withCancel:NO andDirect:YES];
        } else {
            [self releaseLockIt];
            
            if (descriptor.rlt != -1000 && descriptor.rlt != -999999) {
                //에러주석[self alertError:@"Error" message:descriptor.error];
            }
        }
#endif
    }
    else if([event.name isEqualToString:@"PP400"]) {
#ifdef _DRIVER_MODE
        if(descriptor.success == true) {
            NSString     *response = [[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding];
            NSDictionary *result   = getJSONObj(response);

            NSArray *vehicle              = [result valueForKey:@"provider_vehicles"];
            if ([Utils isArray:vehicle]) {
                
                NSMutableArray *vlists = [[NSMutableArray alloc] init];
                for(NSDictionary *dic in vehicle) {
                    if ([dic[@"state"] intValue] >= 0) {
                        [vlists addObject:dic];
                    }
                }
                vehicleList = (NSArray*)vlists;
                [self initVehicleSelect];
            }
            [self.tableView reloadData];
        }
        else {
            if (descriptor.rlt != -1000 && descriptor.rlt != -999999) {
                //에러주석[self alertError:@"Error" message:descriptor.error];
            }
        }
#endif
    }
    
    if ([event.name isEqualToString:@"UR601"] ||
        [event.name isEqualToString:@"UR602"] ||
        [event.name isEqualToString:@"UR603"] ||
        [event.name isEqualToString:@"PP601"] ||
        [event.name isEqualToString:@"PP602"] ||
        [event.name isEqualToString:@"PP603"] ) {
        return;
    }
    
    [appDelegate updateActivityState:self];
    [appDelegate debugActivityState];
    [self onUpdateUI];
    [self onUpdateUI:@{@"result1":@"data1"}];
}

-(void) callLoop:(NSString*)_code withCancel:(BOOL)cancel andDirect:(BOOL)direct lastCode:(NSString*)lastCode{
    if (![lastCode isEqualToString:@""] && ![_code isEqualToString:lastCode]) {
        return;
    }
    [self callLoop:_code withCancel:cancel andDirect:direct];
}

-(void) callLoop:(NSString*)_msg withCancel:(BOOL)cancel andDirect:(BOOL)direct{
    // 모든 runloop를 cancel한다.
    if (cancel) {
        [self cancelAllLoop];
    }
    
    if (!self->appDelegate.isNetwork) {
        direct = NO;
    }
    
    // 해당하는 timer를 작동시킨다.
    for (NSArray *msg in _msgList) {
        if ([[msg objectAtIndex:0] isEqualToString:_msg])
        {
            //NSLog(@"start loop:%@",_msg);
            if (direct) {
                [self runLoop:_msg afterDelay:0.001];
            }
            else
                [self runLoop:_msg afterDelay:[[msg objectAtIndex:1] integerValue]];
            
            return;
        }
    }
}

-(void)clearPolygon
{
    if (self->appDelegate.isDriverMode && self->appDelegate.pstate != 200) {
        [self resetPolygon];
    }
}

-(void)resetPolygon
{
    if ([overlays count]) {
        for (GMSPolygon *poly in overlays) {
            poly.map = nil;
        }
        [overlays removeAllObjects];
    }
}

-(void)drawPolygon:(GMSPath*)path hole:(GMSPath*)hole title:(NSString*)title fare:(float)fare_rate
{
    // Create the first polygon.
    GMSPolygon *polygon = [[GMSPolygon alloc] init];
    polygon.path = path;
    if ([hole count]) {
        polygon.holes = @[ hole ];
    }
    
    //polygon.title = [NSString stringWithFormat:@"%d",fare_rate];
    polygon.title = [NSString stringWithFormat:LocalizedStr(@"Map.Driver.Surge.text"),title,fare_rate];
    
    //NSLog(@"fare_rate:%f, %@",fare_rate,title);
    if (fare_rate < 1.5) {
        polygon.fillColor = UIColorWhite;
    }
    else if (fare_rate >= 1.5 && fare_rate < 2.0) {
        polygon.fillColor = UIColorYellow;
    }
    else if (fare_rate >= 2.0 && fare_rate < 2.5) {
        polygon.fillColor = UIColorOrange;
    }
    else if (fare_rate >= 2.5 && fare_rate < 3.0) {
        polygon.fillColor = UIColorRed;
    }
    else if (fare_rate >= 3.0 && fare_rate < 5.0) {
        polygon.fillColor = UIColorDarkRed;
    }
    
    //polygon.strokeColor = [UIColor blackColor];
    polygon.strokeColor = [UIColor clearColor];
    polygon.strokeWidth = 2;
    polygon.tappable = YES;
    polygon.map = self.mapView_;
    
    [overlays addObject:polygon];
}

/*
- (void)updateClock {
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HH:mm"];
    NSString *ETA = [formatter stringFromDate:[NSDate date]];
    [formatter setDateFormat:@"a"];
    NSString *APM = [formatter stringFromDate:[NSDate date]];
    
    if (self->appDelegate.isDriverMode && self->appDelegate.pstate <= 210) {
        //        [self etaDriverType0:@"" str2:ETA str3:APM str4:@"" str5:@"" onlyleft:YES];
    }
    
    //[self performSelector:@selector(updateClock) withObject:nil afterDelay:1];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [self performSelectorInBackground:@selector(updateClock) withObject:nil];
    });
}

- (void)cancelUpdateClock {
    [NSObject cancelPreviousPerformRequestsWithTarget:self
                                             selector:@selector(updateClock)
                                               object:nil];
}
*/

// 남은거리와 시간 for step
- (NSDictionary*)getDistanceNDuration:(NSString*)ldst__ {
    // 현재 정보가 있으면 현재 정보를 보낸다.
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    if (locMgr.distancematrix != nil) {
        NSDictionary *dic = @{
                              @"edistance":locMgr.distancematrix[@"remain_distance"],
                              @"eduration":locMgr.distancematrix[@"remain_duration"]
                              };
        return dic;
    }
    
#ifdef _DEBUG_ACTIVITY
    [self performSelectorOnMainThread:@selector(debugNotification:)
                           withObject:[NSString stringWithFormat:@"drawRouteOnMAP getDistanceNDuration"]
                        waitUntilDone:false];
#endif
    
    // 현재 정보가 없으면 지도를 조회한다.
    NSString *param = [NSString stringWithFormat:@"fit=Y&line=Y"];
    NSLog(@"trace1113");
    [self drawRouteOnMAP:[Utils formattedPosition:ldrv]
                     dst:ldst__ param:param];
    
    return @{@"edistance" : @"", @"eduration" : @""};
}

// 이동위치 및 시간을 업데이트하고 보고를 하기 위한 timer
- (void)updateTripClock {
    
    /*
     "drive_pickup_step" =     {
     edistance = "1.00";
     eduration = "1.00";
     ndistance = "0.2";// 이 거리(mi)를 지났을때 step을 올려라 nlocation와 현재 gps의 위치를 가지고 거리의 차이를 보고 올린다. ntime중에 하나만 보낸다.
     nlocation = "37.48283692518338,126.9446668401361"; // api올릴때 위치
     ntime = 15초; // 이 시간이 지났을때 step을 올려라, 이전 step을 올린 시간과 차리가 15가 넘으면 올린다.
     oseq = 3223;
     };
     */
    
    if (self->appDelegate.isDriverMode && (self->appDelegate.pstate == 230 || self->appDelegate.pstate == 240) && self->appDelegate.pstate2 == 10) {
        NSDictionary *drive_step = nil;
        
        if (self->appDelegate.pstate == 230) {
            drive_step = [self->appDelegate.dicDriver objectForKey:@"drive_pickup_step"];
        }
        else if (self->appDelegate.pstate == 240) {
            drive_step = [self->appDelegate.dicDriver objectForKey:@"drive_trip_step"];
        }
        
        NSString *nlocation = [drive_step objectForKey:@"nlocation"];
        NSString *ndistance = [drive_step objectForKey:@"ndistance"];
        int ntime = [[drive_step objectForKey:@"ntime"] intValue];
        
        //CLLocationCoordinate2D cloc = [self getLocation:[self currentPosition]];
        
        //double dist = [self geoDistance:[self getLocation:nlocation] loc2:cloc unit:@"K"];
        //double dist = [self geoDistance:[self getLocation:nlocation] loc2:cloc unit:@"m"];
        
        CLLocationCoordinate2D cloc = [Utils getLocation:[Utils currentPosition]];
        CLLocationCoordinate2D dloc = [Utils getLocation:nlocation];
        
        CLLocation *locA = [[CLLocation alloc] initWithLatitude:cloc.latitude longitude:cloc.longitude];
        CLLocation *locB = [[CLLocation alloc] initWithLatitude:dloc.latitude longitude:dloc.longitude];
        
        double dist = [locA distanceFromLocation:locB];// meter
        
        //int timestamp = (int)(([[NSDate date] timeIntervalSince1970] - self->_lastReportTime)/60); //분
        int timestamp = (int)(([[NSDate date] timeIntervalSince1970] - self->_lastReportTime)); // 초
        //NSLog(@"timestamp:%d",timestamp);
        //NSTimeInterval executionTime = [[NSDate date] timeIntervalSinceDate:self->_lastReportTime];
        //int timestamp = (int)(executionTime);
        
        //int timestamp = [[NSDate date] timeIntervalSince1970] - self->_lastReportTime;
        BOOL __isReport = NO;
        
        // 최초 요청인 경우 초기화
        if (!self->_lastReportTime) {
            self->_lastReportTime = [[NSDate date] timeIntervalSince1970];
            self->_lastRequestGoogleTrip = [[NSDate date] timeIntervalSince1970];
            timestamp = 0;
        }
        // 거리 체크 ndistance -> mi
        //NSLog(@"dist:%f,%f",[ndistance doubleValue], [Utils meterTomi:dist]);
        if ([ndistance doubleValue] < [Utils meterTomi:dist] &&
            [GoogleUtils isLocationValid:cloc] &&
            [GoogleUtils isLocationValid:dloc]) {
            // 보고를 위해 보고용 array에 추가한다. - 네트웍 오류인 경우 처리를 위해, 앱이 종료되는건 어쩔수 없음. bg인 경우도 처리되어야 함.
            // add array 좌표
            // self->_ndistance를 현재 위치로 업데이트 한다.
            self->_ndistance = [Utils formattedPosition:cloc];
            
            __isReport = YES;
        }
        // 시간체크
        else if (ntime < timestamp) {
            // 보고를 위해 보고용 array에 추가한다. - 네트웍 오류인 경우 처리를 위해, 앱이 종료되는건 어쩔수 없음. bg인 경우도 처리되어야 함.
            // add array 좌표
            // self->_lastReportTime를 현재 시간으로 업데이트 한다.
            //self->_lastReportTime = [[NSDate date] timeIntervalSince1970];
            //self->_lastReportTime = [NSDate date];
            
            __isReport = YES;
        }
        //NSLog(@"__isReport:%d",__isReport);
        //        NSLog(@"%d:current position:%@, ntime:%d, timestamp:%d",__isReport,[self currentPosition],ntime, timestamp);
        //        NSLog(@"%d:current position:%@, ndistance:%@/%f",__isReport,nlocation,ndistance,dist);
        
        // google api 요청시간 체크
        timestamp = (int)(([[NSDate date] timeIntervalSince1970] - self->_lastRequestGoogleTrip)); // 초
        if (60 < timestamp) {
#ifndef _ETA_LOCAL
            // matrix 보고용
            [self getRoute:[Utils currentPosition] dst:[Utils destinationPosition]];
#endif
            self->_lastRequestGoogleTrip = [[NSDate date] timeIntervalSince1970];
        }
        
        if (__isReport) {
            self->_lastReportTime = [[NSDate date] timeIntervalSince1970];
            
            if (self->appDelegate.pstate == 230) {
                // onPD211
                [self onPD211];
            }
            else {
                // onPD311
                [self onPD311];
            }
        }
        
        // 사용하지 않음. 폴링주기에 따라서 판단하도록 함.
        //[self performSelector:@selector(updateTripClock) withObject:nil afterDelay:10];
    }
}

-(void)dismissAlertView:(LGAlertView*)alert
{
    [alert dismissAnimated:YES completionHandler:nil];
    //[alert dismissWithClickedButtonIndex:0 animated:YES];
}

- (void)cancelUpdateTripClock {
    if (!self->_lastReportTime) {
        return;
    }
    self->_lastReportTime = 0;
    self->_lastRequestGoogleTrip = 0;
    
    //return; // 사용하지 않음. 폴링주기에 따라서 판단하도록 함.
    
    if (self->appDelegate.isDriverMode && (self->appDelegate.pstate == 230 || self->appDelegate.pstate == 240) && self->appDelegate.pstate2 == 10) {
        return;
    }
    
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self
                                             selector:@selector(updateTripClock)
                                               object:nil];
}


- (BOOL)checkETClock {
    
    if (self->_MAX_etTimer > 0) {
        return YES;
    }
    return NO;
}

- (void)startETClock:(float)mt withDate:(NSString*)update_date{
    
    if ([self checkETClock]) {
        return;
    }
    //[self cleanEtaType];
    if(!mt) return; // 0이면 timer를 걸지 않는다.
    self->_MAX_etTimer = mt;
    self->_isEtTimerTimeout = NO;
    self->etTimer = StringFormat2Date(update_date);
    
    NSLog(@"etTimr:%@",self->etTimer);
    [self updateETClock];
}

- (void)startETClock:(float)mt {
    
    if ([self checkETClock]) {
        return;
    }
    if(!mt) return; // 0이면 timer를 걸지 않는다.
    self->_MAX_etTimer = mt;
    self->_isEtTimerTimeout = NO;
    self->etTimer = [NSDate date];
    [self updateETClock];
}

- (void)stopETClock {
    
    if (![self checkETClock]) {
        return;
    }
    self->_MAX_etTimer = 0;
    self->_isEtTimerTimeout = NO;
    self->etTimer = [NSDate date];
    [self cancelUpdateETClock];
//    [self etaDriverType_ETA:@" " eta:@" " str5:@" "];
    
#ifdef _DRIVER_MODE
    if (self->appDelegate.isDriverMode && self->appDelegate.pstate == 230 && self->appDelegate.pstate2 == 20) {
        //_lbTopTitle.text = @"";
    }
    _lbTopTitle.textColor = UIColorDefault2;
#endif
}

// for 230 대기시간 출력
-(void)print_wait230:(int)__tm {
    dispatch_async(dispatch_get_main_queue(), ^{
        if (__tm >= self->_MAX_etTimer) {
            _lbTopTitle.textColor = [UIColor redColor];
        }
        else if (__tm>=0) {
            // 대기시간 처리
            _lbTopTitle.textColor = UIColorDefault2;
        }
        _lbTopTitle.text = [NSString stringWithFormat:@"%@ %d %@",LocalizedStr(@"Map.Drive.Pickup.ET.text"),__tm,LocalizedStr(@"String.Minute")];
    });
}

// timer에 따른 처리
- (void)updateETClock {
    
    NSDate *methodFinish = [NSDate date];
    NSTimeInterval executionTime = [methodFinish timeIntervalSinceDate:self->etTimer];
    
#ifdef _WITZM
#elif defined _CHACHA
#else
    if (self->appDelegate.isKorea) {
        executionTime -= 16*60*60;
    }
#endif
    //NSLog(@"executionTime = %f, %d, %@", executionTime, (int)(executionTime/60),self->etTimer);
    //NSLog(@"executionTime = %f, %d", executionTime, (int)(executionTime/60));
    
    if (executionTime < 0) {
        return;
    }

    if (self->appDelegate.isDriverMode && self->appDelegate.pstate == 230 && self->appDelegate.pstate2 == 20) {
        
        int __tm = (int)(executionTime/60);
        
        // ETA 시간이 초과 되었다.
        if (__tm >= self->_MAX_etTimer) {
            // 사용하지 않는다.
            //self->_isEtTimerTimeout = YES;
            
            //NSLog(@"self->_isEtTimerTimeout:%d",self->_isEtTimerTimeout);
            
            //--[self etaDriverType230_20:uname eta:LocalizedStr(@"Map.Drive.Pickup.ET.text") str5:[NSString stringWithFormat:@" %d %@",__tm,LocalizedStr(@"String.Minute")]];
            [self print_wait230:__tm];
        }
        else if (__tm>=0) {
            // 대기시간 처리
            //[self etaDriverType_ETA:uname eta:LocalizedStr(@"Map.Drive.Pickup.ET.text") str5:[NSString stringWithFormat:@" %d %@",__tm,LocalizedStr(@"String.Minute")]];
            [self print_wait230:__tm];
        }
        else {
            //[self etaDriverType_ETA:uname eta:@" " str5:@" "];
        }
    }
    else if (self->appDelegate.isDriverMode && (self->appDelegate.pstate == 230 || self->appDelegate.pstate == 240) && self->appDelegate.pstate2 == 10) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self updateTripClock];
        });
    }
    
    // 60초 시간 보정
    int __tm = (int)((int)executionTime%60);
    __tm = 60 -__tm;
    //[self performSelector:@selector(updateETClock) withObject:nil afterDelay:__tm];
    
    if (self->appDelegate.isDriverMode && (self->appDelegate.pstate == 230 || self->appDelegate.pstate == 240) && self->appDelegate.pstate2 == 10) {
        __tm = 2;
    }
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, __tm * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [self performSelectorInBackground:@selector(updateETClock) withObject:nil];
    });
}

- (void)cancelUpdateETClock {
    [NSObject cancelPreviousPerformRequestsWithTarget:self
                                             selector:@selector(updateETClock)
                                               object:nil];
}


- (void)runLoop:(NSString*)midx afterDelay:(NSInteger)delay__{
    
    //NSLog(@"add? runLoop:%@,%@",midx,LOOP_MAP_KEY(midx));
    if ([self->appDelegate.loopArray valueForKey:LOOP_MAP_KEY(midx)]) {
        return;
    }
    [self->appDelegate.loopArray setValue:midx forKey:LOOP_MAP_KEY(midx)];
    
    //NSLog(@"add runLoop:%@,%@",midx,LOOP_MAP_KEY(midx));
    SEL selector = NSSelectorFromString([NSString stringWithFormat:@"on%@", midx]);
    [self performSelector:selector withObject:nil afterDelay:delay__];
    
    //    @autoreleasepool {
    //        SEL selector = NSSelectorFromString([NSString stringWithFormat:@"on%@", midx]);
    //        NSLog(@"(A) %@", [NSThread currentThread]);
    //
    //        // (1) ↓runUntilDate: でメソッドの実行時間までランループの終了を遅延させる
    //        [self performSelector:selector withObject:nil afterDelay:delay__];
    //        [[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:delay__]]; // ←これがないとダメ。
    //
    //        // (2) ↓dispatch_after()だと待たなくても別のスレッド上で遅延実行される。
    ////        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 5 * NSEC_PER_SEC), dispatch_get_current_queue(), ^{
    ////            [self selectorFromBackground2];
    ////        });
    //    }
    
    //        UIApplication*   app = [UIApplication sharedApplication];
    //
    //    id<NSObject,NSCopying> token = [app bk_performInBackgroundAfterDelay:delay__ usingBlock:^(id sender){
    //#pragma clang diagnostic push
    //#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
    //        [self performSelector:selector];
    //#pragma clang diagnostic pop
    //    }];
    //    NSLog(@"token:%@",token);
    
    //    double delayInSeconds = delay__;
    //    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    //    dispatch_after(popTime, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0),
    //                   ^(void){
    //                       //your code here
    //                       SEL selector = NSSelectorFromString([NSString stringWithFormat:@"on%@", midx]);
    //                       [self performSelector:selector withObject:nil afterDelay:0.1];
    //                       //[self performSelectorInBackground:selector withObject:nil];
    //                   });
    
    //    [[EDQueue sharedInstance] enqueueWithData:[NSString stringWithFormat:@"on%@", midx] forTask:@"success"];
    
    //    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, delay__ * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
    ////    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
    //    SEL selector = NSSelectorFromString([NSString stringWithFormat:@"on%@", midx]);
    //        NSTimer* t = [NSTimer scheduledTimerWithTimeInterval:delay__ target:self  selector:selector userInfo:nil repeats:NO];
    //
    //        [[NSRunLoop currentRunLoop] addTimer:t forMode:NSDefaultRunLoopMode];
    //
    //        [[NSRunLoop currentRunLoop] run];
    //    });
    
    //    dispatch_queue_t myQueue = dispatch_queue_create("My Queue",NULL);
    //    dispatch_async(myQueue, ^{
    //        // Perform long running process
    //        SEL selector = NSSelectorFromString([NSString stringWithFormat:@"on%@", midx]);
    //        [self performSelector:selector withObject:nil afterDelay:delay__];
    //
    //        dispatch_async(dispatch_get_main_queue(), ^{
    //            // Update the UI
    //
    //        });
    //    });
    
    
    //    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, delay__ * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
    //        SEL selector = NSSelectorFromString([NSString stringWithFormat:@"on%@", midx]);
    //        [self performSelectorInBackground:selector withObject:nil];
    //    });
    
    //[self performSelector:selector withObject:nil afterDelay:delay__];
    
    //    NSArray *tasks = @[[SomeTask class]];
    //    // Run the scheduler every 5 minutes
    //    [SLNScheduler setMinimumBackgroundFetchInterval:60 * 5];
    //    // Add the tasks
    //    [SLNScheduler scheduleTasks:tasks];
}

- (void)cancelLoop:(NSString*)midx {
    
    if ([self->appDelegate.loopArray valueForKey:LOOP_MAP_KEY(midx)]) {
        [self->appDelegate.loopArray removeObjectForKey:LOOP_MAP_KEY(midx)];
        //NSLog(@"remove runLoop:%@",midx);
    }
    
    //NSLog(@"cancelLoop:%@",midx);
    SEL selector = NSSelectorFromString([NSString stringWithFormat:@"on%@", midx]);
    
    [NSRunLoop cancelPreviousPerformRequestsWithTarget:self selector:selector object:nil];
}

- (void)cancelAllLoop {
    
    for (NSArray *msg in _msgList) {
        // timeout은 cancel loop에서 제외한다.
        // 직접 cancel해야 한다.
        if ([[msg objectAtIndex:0] isEqualToString:@"UD131_1"] ) {
            continue;
        }
        else if ([[msg objectAtIndex:0] isEqualToString:@"PD131"] ) {
            continue;
        }
        [self cancelLoop:[msg objectAtIndex:0]];
    }
}

- (void)cancelTimeout {
    
    for (NSArray *msg in _msgList) {
        if ([[msg objectAtIndex:0] isEqualToString:@"UD131_1"] ) {
            [self cancelLoop:[msg objectAtIndex:0]];
            [Utils setStateUIInfo:UI_STATE_UD131 value:UI_STATE_STEP_FIN oldvalue:nil];
        }
        else if ([[msg objectAtIndex:0] isEqualToString:@"PD131"] ) {
            [self cancelLoop:[msg objectAtIndex:0]];
            [Utils setStateUIInfo:UI_STATE_PD131 value:UI_STATE_STEP_FIN oldvalue:nil];
        }
    }
}

- (void)backgroudFetchLoop {
    
    NSString *midx = @"";
    if (self->appDelegate.isDriverMode) {
        // Driver
        if (self->appDelegate.pstate == 200) {
            midx = @"PQ002";
        }
        else if (self->appDelegate.pstate == 210) {
            midx = @"PQ100";
        }
        else if (self->appDelegate.pstate == 220) {
            midx = @"PQ100";
        }
        else if (self->appDelegate.pstate == 230) {
            midx = @"PQ200";
        }
        else if (self->appDelegate.pstate == 240 && self->appDelegate.pstate2 == 0) {
            midx = @"PQ300";
        }
    }
    else {
        // Rider
        if (self->appDelegate.ustate <= 110) {
            midx = @"UQ002";
        }
        else if (self->appDelegate.ustate == 130 && self->appDelegate.ustate2 == 0) {
            midx = @"UQ200";
        }
        else if (self->appDelegate.ustate == 130 && self->appDelegate.ustate2 == 10) {
            midx = @"UQ200";
        }
        else if (self->appDelegate.ustate == 130 && self->appDelegate.ustate2 == 20) {
            midx = @"UQ300";
        }
        else if (self->appDelegate.ustate == 140) {
            midx = @"UQ300";
        }
    }
    
    [self->model requestSync:midx];
    [self callLoop:midx withCancel:YES andDirect:YES];
    
}

// 현재 상태에서 timer를 살려두어야 하는것을 정의한다.
- (NSString*)checkLoop {
    
    if (self->appDelegate.isDriverMode) {
        
        if (self->appDelegate.pstate == 200) {
            [self cancelAllLoop];
            [self callLoop:@"PQ002" withCancel:YES andDirect:YES];
            return @"PQ002";
        }
        else if (self->appDelegate.pstate == 210) {
            [self cancelAllLoop];
            [self callLoop:@"PQ100" withCancel:YES andDirect:YES];
            return @"PQ100";
        }
        else if (self->appDelegate.pstate == 220 && self->appDelegate.pstate2 == 10) {
            [self pushPD150];
            //            if (!isRunTimeout_) {
            //                [self callLoop:@"PD131" withCancel:NO andDirect:NO];
            //                isRunTimeout_ = YES;
            //            }
            return @"";
        }
        else if (self->appDelegate.pstate == 220 && self->appDelegate.pstate2 == 20) {
            [self cancelAllLoop];
            [self callLoop:@"PQ100" withCancel:YES andDirect:YES];
            return @"PQ100";
        }
        else if (self->appDelegate.pstate == 230) {
            [self cancelAllLoop];
            [self callLoop:@"PQ200" withCancel:YES andDirect:YES];
            return @"PQ200";
        }
        //        else if (self->appDelegate.pstate == 230 && self->appDelegate.pstate2 == 10) {
        //            [self cancelAllLoop];
        //            //[self callLoop:@"PQ200" withCancel:YES andDirect:YES];
        //            [self callLoop:@"PD211" withCancel:NO andDirect:NO];
        //            return @"PQ200";
        //        }
        else if (self->appDelegate.pstate == 240 && self->appDelegate.pstate2 == 0) {
            [self cancelAllLoop];
            [self callLoop:@"PQ300" withCancel:YES andDirect:YES];
            return @"PQ300";
        }
        else if (self->appDelegate.pstate == 240 && self->appDelegate.pstate2 == 10) {
            [self cancelAllLoop];
            [self callLoop:@"PQ300" withCancel:YES andDirect:YES];
            //
            //            [self callLoop:@"PD311" withCancel:NO andDirect:NO];
            return @"PQ300";
        }
        else if ((self->appDelegate.pstate == 240 && self->appDelegate.pstate2 == 20) || (self->appDelegate.pstate == 230 && self->appDelegate.pstate2 <= -10) || (self->appDelegate.pstate == 240 && self->appDelegate.pstate2 <= -10)) {
            [self cancelAllLoop];
            return @"";
        }
    }
    else {
        // Rider
        if (self->appDelegate.ustate <= 110) {
            //self->appDelegate.ustate2 = 0;
            [self cancelAllLoop];
            [self callLoop:@"UQ002" withCancel:YES andDirect:YES];
            return @"UQ002";
        }
        else if (self->appDelegate.ustate == 130 && self->appDelegate.ustate2 == 0) {
            [self cancelTimeout];
            [self cancelAllLoop];
            [self onUR602];
            [self callLoop:@"UQ200" withCancel:YES andDirect:YES];
            return @"UQ200";
        }
        else if (self->appDelegate.ustate == 130 && self->appDelegate.ustate2 == 10) {
            [self cancelTimeout];
            [self cancelAllLoop];
            [self callLoop:@"UQ200" withCancel:YES andDirect:NO];
            return @"UQ200";
        }
        else if (self->appDelegate.ustate == 130 && self->appDelegate.ustate2 == 20) {
            [self cancelTimeout];
            [self cancelAllLoop];
            [self callLoop:@"UQ300" withCancel:YES andDirect:NO];
            return @"UQ300";
        }
        else if (self->appDelegate.ustate == 140 && self->appDelegate.ustate2 == 0) {
            //[self cancelTimeout];
            [self cancelAllLoop];
            [self onUR602];
            [self callLoop:@"UQ300" withCancel:YES andDirect:YES];
            return @"UQ300";
        }
        else if (self->appDelegate.ustate == 140 && self->appDelegate.ustate2 == 10) {
            //[self cancelTimeout];
            [self cancelAllLoop];
            [self callLoop:@"UQ300" withCancel:YES andDirect:NO];
            return @"UQ300";
        }
        else if ((self->appDelegate.ustate == 140 && self->appDelegate.ustate2 == 20) || (self->appDelegate.ustate == 130 && self->appDelegate.ustate2 <= -10) || (self->appDelegate.ustate == 140 && self->appDelegate.ustate2 <= -10)) {
            [self cancelTimeout];
            [self cancelAllLoop];
            [self callLoop:@"UQ300" withCancel:YES andDirect:NO];
            return @"UQ300";
        }
    }
    
    
    return @"";
}

- (void)animateToNextCoord:(GMSMarker *)_marker nextLocation:(CLLocationCoordinate2D) coord {
    CLLocationCoordinate2D previous = _marker.position;
    
    if (_marker.map == nil) {
        
        TXTaxi *taxi = _marker.userData;
        if (taxi != nil) {
            NSString *service = [taxi.provider objectForKey:@"service"];
            
            // 차량을 보여주지 않는다.
            if ([serviceType isEqualToString:service]) {
                //                _marker.map = self.mapView_;
            }
        }
        else {
            NSLog(@"No TXTaxi");
            //_marker.map = self.mapView_;
        }
    }
    
    // 이동 위치가 없으면 업데이트 하지 않는다.
    
    if ([Utils pointCompare:coord.latitude comp:_marker.position.latitude] && [Utils pointCompare:coord.longitude comp:_marker.position.longitude]) {
        _marker.position = coord;
        return;
    }
    
    if (![GoogleUtils isLocationValid:_marker.position]) {
        previous = coord;
    }
    if (![GoogleUtils isLocationValid:coord]) {
        return;
    }
    
    //    NSLog(@"----------------animateToNextCoord: %f,%f",_marker.position.latitude,_marker.position.longitude);
    //    NSLog(@"----------------animateToNextCoord: %f,%f",coord.latitude,coord.longitude);
    CLLocationDirection heading = GMSGeometryHeading(previous, coord);
    CLLocationDistance distance = GMSGeometryDistance(previous, coord);
    
    // 10도 이상 heading과 거리 10 m이상 일때만처리한다.
    if (!self->appDelegate.isDriverMode && (distance < 10 || heading < 10))
    {
        if (![GoogleUtils isLocationValid:_marker.position]) {
            _marker.position = coord;
        }
        return;
    }
    NSLog(@"----------------animateToNextCoord: %f,%f,%f",distance,heading,[self DegreeBearing:previous locationB:coord]);
    // Use CATransaction to set a custom duration for this animation. By default, changes to the
    // position are already animated, but with a very short default duration. When the animation is
    // complete, trigger another animation step.
    // 축적에 따라서 속도 계산해야 한다.
    [CATransaction begin];
    //[CATransaction setAnimationDuration:(distance / (50 * 1000))];  // custom duration, 2m/sec 50km/sec 50 * 1000
    [CATransaction setAnimationDuration:10];  // custom duration, 2m/sec 50km/sec 50 * 1000
    
    //    __weak TXMapVC *weakSelf = self;
    //    [CATransaction setCompletionBlock:^{
    //        [weakSelf animateToNextCoord:marker];
    //    }];
    
    _marker.position = coord;
    _marker.groundAnchor = CGPointMake(0.5, 0.5);
    
    [CATransaction commit];
    
    // 각도 20, -값은 반대로, 회전 3초
    // If this marker is flat, implicitly trigger a change in rotation, which will finish quickly.
    //    if (_marker.flat) {
    _marker.rotation = heading;
    //    }
    
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];

    // 차량을 따서서 position을 움직인다.
    if (!locMgr.isMoveMapBYGesture && self->appDelegate.isDriverMode) {
        // 위치로 이동
        CGFloat currentZoom = self.mapView_.camera.zoom;
        GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:coord.latitude
                                                                longitude:coord.longitude
                                                                     zoom:currentZoom];
        
        [self.mapView_ animateToCameraPosition:camera];
    }
}

-(double) DegreeBearing:(CLLocationCoordinate2D) A locationB: (CLLocationCoordinate2D)B{
    double dlon = [self ToRad:(B.longitude - A.longitude)];
    double dPhi = log(tan([self ToRad:B.latitude] / 2 + M_PI / 4) / tan([self ToRad:A.latitude] / 2 + M_PI / 4));
    if  (fabs(dlon) > M_PI){
        dlon = (dlon > 0) ? (dlon - 2*M_PI) : (2*M_PI + dlon);
    }
    return [self ToBearing:(atan2(dlon, dPhi))];
}

-(double) ToRad: (double)degrees{
    return degrees*(M_PI/180);
}

-(double) ToBearing:(double)radians{
    return fmod(([self ToDegrees:radians] + 360.0f) , 360);
}

-(double) ToDegrees: (double)radians{
    return radians * 180 / M_PI;
}
/*
 -(void)propertiesOnGeometry:(CLLocationCoordinate2D)coordinate {
 
 CLLocationCoordinate2D bestPoint = [self nearestPolylineLocationToCoordinate:coordinate];
 
 for (NSDictionary *step in features) {
 NSDictionary *geometry   = [step objectForKey:@"geometry"];
 NSDictionary *properties = [step objectForKey:@"properties"];
 
 if ([[geometry objectForKey:@"type"] isEqualToString:@"LineString"]) {
 NSArray *coordinates = [geometry objectForKey:@"coordinates"];
 
 int i;
 NSInteger coordIdx = [coordinates count];
 
 for (i = 0; i < coordIdx; i++)
 {
 CLLocationCoordinate2D coord = CLLocationCoordinate2DMake([coordinates[i][1] doubleValue], [coordinates[i][0] doubleValue]);
 if  ([self isEqualLocation:bestPoint location:coord]) {
 
 if ([Utils isDictionary:properties]) {
 NSString *description = properties[@"description"];
 if (![[Utils nullToString:description] isEqualToString:@""]) {
 // TTS
 [self playTTS:description vol:0.5f];
 }
 }
 
 return;
 }
 }
 }
 }
 }
 */
/*
 void TRDrawLineWithArrow(CGContextRef CXT, CGPoint FROMPOINT, CGPoint TOPOINT, CGFloat WIDTH, CGFloat ARROWSIZEMULTIPLE)
 {
 CGFloat         rise = TOPOINT.y - FROMPOINT.y;
 CGFloat         run = TOPOINT.x - FROMPOINT.x;
 
 // trig
 CGFloat         length = sqrt(rise*rise + run+run);
 CGFloat         angle = atan2(rise, run);
 
 // the length of our arrowhead
 CGFloat         arrowLen = WIDTH*ARROWSIZEMULTIPLE;
 
 // push graphics context
 CGContextSaveGState(CXT);
 
 // transform context according to line's origin and angle
 CGContextTranslateCTM(CXT, FROMPOINT.x, FROMPOINT.y);
 CGContextRotateCTM(CXT, angle);
 
 // draw straight line
 CGContextMoveToPoint(CXT, 0, -WIDTH/2.);
 CGContextAddLineToPoint(CXT, 0, WIDTH/2.);
 CGContextAddLineToPoint(CXT, length-arrowLen, WIDTH/2.);
 
 // draw arrowhead
 CGContextAddLineToPoint(CXT, length-arrowLen, (WIDTH*ARROWSIZEMULTIPLE)/2.);
 CGContextAddLineToPoint(CXT, length, 0);
 CGContextAddLineToPoint(CXT, length-arrowLen, -(WIDTH*ARROWSIZEMULTIPLE)/2.);
 
 CGContextAddLineToPoint(CXT, length-arrowLen, -WIDTH/2.);
 CGContextAddLineToPoint(CXT, 0, -WIDTH/2.);
 
 // fill the path
 CGContextFillPath(CXT);
 
 // pop graphics context
 CGContextRestoreGState(CXT);
 }
 */

/*
 "start_location" : {
 "lat" : 0,
 "lng" : 0
 },
 "end_location" : {
 "lat" : 0,
 "lng" : 0
 },
 "html_instructions" : "",
 "maneuver" : "turn-right",
 */
/*
-(void)findLocationIndexBySteps:(CLLocationCoordinate2D)coordinate {
    static double distanse_gap = 0;
    
    NSInteger typeIndex = 0;
    double distance = 0;
    double bestDistance = DBL_MAX; // 50m
    //NSDictionary *properties;
    CLLocationCoordinate2D bestCoordinate;
    NSInteger stepCount = [steps count];
    
    if (geomIndex == 0) {
        distanse_gap = 0;
    }
    
    // over
    if (geomIndex<0) {
        distanse_gap = 0;
        return;
    }
    
    if (![Utils isArray:steps] || !stepCount || stepCount < (geomIndex+1)) {
        distanse_gap = 0;
        return;
    }
    
    NSInteger geometryindex = 0;
    
    for (; geometryindex < stepCount ; geometryindex++) {
        NSDictionary *step = steps[geometryindex];
        if ([Utils isDictionary:step]) {
            NSDictionary *start_location    = [step objectForKey:@"start_location"];
            //            NSDictionary *end_location      = [step objectForKey:@"end_location"];
            //            NSString *html_instructions     = [step objectForKey:@"html_instructions"];
            //            NSString *maneuver              = [step objectForKey:@"maneuver"];
            
            // 시작점
            CLLocationCoordinate2D coord = CLLocationCoordinate2DMake([start_location[@"lat"] doubleValue], [start_location[@"lng"] doubleValue]);
            distance = GMSGeometryDistance(coordinate, coord);
            
            // 현재위치를 찾는다.
            if (distance < bestDistance) {
                bestDistance = distance;
                bestCoordinate = coord;
                
                typeIndex = geometryindex;
            }
        }
    }
    
    geomIndex = typeIndex;
    NSLog(@"---------------------geomIndex:%ld",(long)geomIndex);
}

-(void)navigationTTS:(CLLocationCoordinate2D)coordinate
{
    static double distanse_gap = 0;
    
    //double bestDistance = 10; // 50m
    
    if (![Utils isArray:steps]) {
        distanse_gap=0;
        return;
    }
    
    NSInteger stepCount = [steps count];
    if (!stepCount) {
        distanse_gap=0;
        return;
    }
    
    if (geomIndex >= stepCount) {
        geomIndex=0;
        distanse_gap=0;
        return;
    }
    
    NSMutableDictionary *step = [steps[geomIndex] mutableCopy];
    if ([Utils isDictionary:step]) {
        NSDictionary *start_location    = [step objectForKey:@"start_location"];
        NSDictionary *end_location      = [step objectForKey:@"end_location"];
        NSString *html_instructions     = [step objectForKey:@"html_instructions"];
        NSString *maneuver              = [step objectForKey:@"maneuver"];
        
        // 시작점
        CLLocationCoordinate2D coord1 = CLLocationCoordinate2DMake([start_location[@"lat"] doubleValue], [start_location[@"lng"] doubleValue]);
        double distance1 = GMSGeometryDistance(coordinate, coord1);
        
        // 끝점
        CLLocationCoordinate2D coord2 = CLLocationCoordinate2DMake([end_location[@"lat"] doubleValue], [end_location[@"lng"] doubleValue]);
        double distance2 = GMSGeometryDistance(coordinate, coord2);
        
        // 끝점이 2m 이내면 다음 스텝으로 넘어간다.
        if (distance2 < 10 && !step[@"distance2_10"]) {
            
            geomIndex++;
            distanse_gap = 0;
            step[@"distance2_10"] = @"1";
            steps[geomIndex] = step;
            // TTS
            [self playTTS:maneuver vol:0.5f];
            
            return;
        }
        
        // 시작점이 10m 이내면
        if (distance1 < 50 && !step[@"distance1_50"]) {
            
            step[@"distance1_50"] = @"1";
            steps[geomIndex] = step;
            
            // TTS
            [self playTTS:[Utils stringByStrippingHTML:html_instructions] vol:0.5f];
        }
        // 끝점이 10m 이내면
        else if (distance2 < 120 && !step[@"distance2_120"]) {
            
            step[@"distance2_120"] = @"1";
            steps[geomIndex] = step;
            // TTS
            [self playTTS:maneuver vol:0.5f];
        }
        else {
            
        }
        
        // 지나쳐서 멀어지는 경우 위치를 다시 찾아야 함.
        if (distance2 > distanse_gap && distanse_gap != 0) {
            //geomIndex++;
            distanse_gap = 0;
            [self findLocationIndexBySteps:[Utils currentPositionCoor]];
        }
        else {
            distanse_gap = distance2;
        }
    }
}
*/
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - UITableView DataSource Methods
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    //[cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [tableView setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [tableView setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
    
    UIView *seperatorView = [[UIView alloc] initWithFrame:CGRectMake(0, cell.frame.size.height - 1, cell.frame.size.width, 1)];
    seperatorView.backgroundColor = UIColorTableSeperator;
    [cell addSubview:seperatorView];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    // This will create a "invisible" footer
    return 0.01f;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [UIView new];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [dataArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    cell.tag = indexPath.row;
    int tag = (int)indexPath.row+100;
    //[self changeCell:tableView withIndexPath:indexPath andTag:tag];
    [self addCell:tableView withCell:cell withIndexPath:indexPath andTag:tag];
    //cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    
    NSString *lb1 = @"";
    if (indexPath.row == 0) {
        lb1 = LocalizedStr(@"Map.Vechile.Request.CouponCode.NoSelect.text");
    }
    else {
        NSDictionary *p = [dataArray objectAtIndex:indexPath.row];
        
        // 1:% 0:won
        if ([[Utils nullToString:p[@"discounttype"]] isEqualToString:@"1"]) {
            lb1 = [NSString stringWithFormat:@"%@ %@%%",
                   [p valueForKey:@"title"],
                   CURRENCY_FORMAT2([p valueForKey:@"discount"])
                   ];
        }
        else {
            lb1 = [NSString stringWithFormat:@"%@ %@",
                   [p valueForKey:@"title"],
                   CURRENCY_FORMAT([p valueForKey:@"discount"])
                   ];
        }
    }
    
    
    cell.textLabel.text = [NSString stringWithFormat:@"      %@",lb1];
    
    return cell;
}

#pragma mark - UITableView Delegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self changeCell:tableView withIndexPath:indexPath andTag:((int)indexPath.row+100)];
    
    //cell.accessoryView.hidden = NO;
}
- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self changeCell:tableView withIndexPath:indexPath andTag:((int)indexPath.row+100)];
    
    //cell.accessoryView.hidden = YES;
}

-(void)addCell:(UITableView*)tableView withCell:(UITableViewCell*)cell withIndexPath:(NSIndexPath*)indexPath andTag:(int)tag{
    
    UIImageView *preView = [cell.contentView viewWithTag:tag];
    [preView removeFromSuperview];
    
    UILabel *preLabel = [cell.contentView viewWithTag:(tag+100)];
    [preLabel removeFromSuperview];
    
    NSInteger _h = cell.frame.size.height;
    NSInteger _x = 16;
    UIImageView* imv = [[UIImageView alloc]initWithFrame:CGRectMake(cell.frame.size.width - _x*4 ,_h/2-_x/2, _x, _x)];
    imv.tag = tag;
    
    /*
    UIImage *cellImage;
    
    if([[tableView indexPathsForSelectedRows] containsObject:indexPath]) {
        cellImage = [UIImage imageNamed:@"form_radio_chk"];
    }
    else {
        cellImage = [UIImage imageNamed:@"form_radio_normal"];
    }
    imv.image=cellImage;
    [cell.contentView addSubview:imv];
     */
}

-(void)changeCell:(UITableView*)tableView withIndexPath:(NSIndexPath*)indexPath andTag:(int)tag{
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    UIImageView *preView = [cell.contentView viewWithTag:tag];
    [preView removeFromSuperview];
    
    UILabel *preLabel = [cell.contentView viewWithTag:(tag+100)];
    [preLabel removeFromSuperview];
    
    /*
    NSInteger _h = cell.frame.size.height;
    NSInteger _x = 16;
    
    UIImageView* imv = [[UIImageView alloc]initWithFrame:CGRectMake(cell.frame.size.width - _x*4 ,_h/2-_x/2, _x, _x)];
    //    UIImageView* imv = [[UIImageView alloc]initWithFrame:CGRectMake(0,_h/2-_x/2, _x, _x)];
    imv.tag = tag;
    UIImage *cellImage;
    
    if([[tableView indexPathsForSelectedRows] containsObject:indexPath]) {
        cellImage = [UIImage imageNamed:@"form_radio_chk"];
    }
    else {
        cellImage = [UIImage imageNamed:@"form_radio_normal"];
    }
    imv.image=cellImage;
    [cell.contentView addSubview:imv];
     */
}

#pragma mark -
#pragma mark Validator
#pragma mark -
-(void) resignKeyboard
{
    [self.view endEditing:YES];
}

- (IBAction)validateAction:(id)sender
{
}

#pragma ValidatorDelegate - Delegate methods

- (void) preValidation
{
}

- (void)onSuccess
{
}

- (void)onFailure:(Rule *)failedRule
{
    
    //    NSMutableDictionary *options = [@{kCRToastNotificationTypeKey               : @(CRToastTypeNavigationBar),
    //                                      kCRToastNotificationPresentationTypeKey   : @(CRToastPresentationTypeCover),
    //                                      kCRToastUnderStatusBarKey                 : @(YES),
    //                                      kCRToastTextKey                           : failedRule.failureMessage,
    //                                      kCRToastFontKey                           : [UIFont systemFontOfSize:14],
    //                                      kCRToastTextAlignmentKey                  : @(NSTextAlignmentCenter),
    //                                      kCRToastTimeIntervalKey                   : @(2),
    //                                      kCRToastAnimationInTypeKey                : @(CRToastAnimationTypeGravity),
    //                                      kCRToastAnimationOutTypeKey               : @(CRToastAnimationTypeGravity),
    //                                      kCRToastAnimationInDirectionKey           : @(CRToastAnimationDirectionTop),
    //                                      kCRToastAnimationOutDirectionKey          : @(CRToastAnimationDirectionTop),
    //                                      kCRToastNotificationPreferredPaddingKey   : @(15)} mutableCopy];
    //    [CRToastManager showNotificationWithOptions:options
    //                                completionBlock:^{
    //                                    NSLog(@"Completed");
    //                                }];
    
    [WToast showWithText:failedRule.failureMessage duration:kWTShort roundedCorners:NO gravity:kWTGravityTop];
}

#pragma mark - Spotify AppDelegate
- (void)didChangePositionSpotify:(NSTimeInterval)position {
}

#ifdef _DRIVER_MODE
- (void)didChangeUISpotify:(BOOL)isPlay {
    
    // pause상태, play 상태 변한다.
    UIButton *btn = [self.vToolBar viewWithTag:10];
    if (isPlay)
    {
        [btn setImage:[UIImage imageNamed:@"music_pause"] forState:UIControlStateNormal];
    }
    else{
        [btn setImage:[UIImage imageNamed:@"music_play"] forState:UIControlStateNormal];
    }
}
#endif


#pragma mark - Debug View
#ifdef _DEBUG_ACTIVITY
- (void)debugNotification:(NSDictionary *)object
{
    //[[NSNotificationCenter defaultCenter] postNotificationName:[object objectForKey:@"name"] object:[object objectForKey:@"data"]];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"EDQueueJobDidSucceed" object:object];
}

- (void)receivedNotification:(NSNotification *)notification
{
    NSString *userInfo = notification.object;
    if (![[Utils nullToString:userInfo] isEqualToString:@""]) {
        if (self->appDelegate.isDriverMode) {
            self.activity.text = [NSString stringWithFormat:@"%@%d,%d %@\n", self.activity.text, self->appDelegate.pstate, self->appDelegate.pstate2, userInfo];
        }
        else {
            self.activity.text = [NSString stringWithFormat:@"%@%d,%d %@\n", self.activity.text, self->appDelegate.ustate, self->appDelegate.ustate2, userInfo];
        }
        
        [self.activity scrollRangeToVisible:NSMakeRange([self.activity.text length], 0)];
    }
}

#endif

//Method writes a string to a text file
-(void) writeToDebugFile{
#ifdef _DEBUG_ACTIVITY
    NSString* oseq = @"";
    if (self->appDelegate.isDriverMode) {
        oseq = self->appDelegate.dicDriver[@"provider_state"];
    }
    else {
        oseq = self->appDelegate.dicRider[@"user_state"];
    }
    if ([oseq isEqualToString:@""]) return;
    
    //get the documents directory:
    NSArray *paths = NSSearchPathForDirectoriesInDomains
    (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    //make a file name to write the data to using the documents directory:
    NSString *fileName = [NSString stringWithFormat:@"%@/%@.txt",
                          documentsDirectory,oseq];
    //save content to the documents directory
    //    [self.activity.text writeToFile:fileName
    //              atomically:NO
    //                encoding:NSStringEncodingConversionAllowLossy
    //                   error:nil];
    
    NSString *documentTXTPath = [documentsDirectory stringByAppendingPathComponent:fileName];
    NSString *savedString = self.activity.text;
    NSFileHandle *myHandle = [NSFileHandle fileHandleForWritingAtPath:documentTXTPath];
    [myHandle seekToEndOfFile];
    [myHandle writeData:[savedString dataUsingEncoding:NSUTF8StringEncoding]];
    
    self.activity.text = @"";
#endif
}

@end
