//
//  TXChargeVC.m
//  Taxi
//

//

#import "TXCancelVC.h"
#import "utils.h"
#import "converter.h"
#import "LGAlertView.h"

//#import "NSString+FontAwesome.h"
@import IQKeyboardManager;

#import "Validator.h"
#import "WToast.h"
//#import "CRToast.h"


@interface TXCancelVC () <UIGestureRecognizerDelegate,UITableViewDelegate,UITableViewDataSource> {
    int userId;
    NSArray *_msgList;
    
    NSArray *dataArray;
    LGAlertView *alertView;
}

@end

@implementation TXCancelVC {
}

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil step:(NSInteger)__step state:(NSInteger)__state {
    
    if(self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        _step = __step;
        _state= __state;
    }
    
    return self;
}

-(void)viewDidLoad {
    [super viewDidLoad];
    
    [[IQKeyboardManager sharedManager] setEnable:NO];
    
    alertView = nil;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    // Disable iOS 7 back gesture
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
        self.navigationController.interactivePopGestureRecognizer.delegate = self;
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    // XXX_canceled
    if (!appDelegate.isDriverMode && _step != 1)
    {
        NSString* str= LocalizedStr(@"Map.Rider.Pickup.Canceled.BTN.text");
        
//        _btnCancel.titleLabel.numberOfLines = 1;
//        _btnCancel.titleLabel.adjustsFontSizeToFitWidth = YES;
//        _btnCancel.titleLabel.lineBreakMode = NSLineBreakByClipping;
        
        [_btnCancel setTitle:str forState:UIControlStateNormal];
        
        CGSize maximumSize = CGSizeMake(300, 9999);
        UIFont *myFont = [UIFont fontWithName:@"HelveticaNeue-Medium" size:18.0f];
        CGRect textRect = [str boundingRectWithSize:maximumSize
                                            options:NSStringDrawingUsesLineFragmentOrigin| NSStringDrawingUsesFontLeading
                                         attributes:@{NSFontAttributeName:myFont}
                                            context:nil];
        
        UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2 - textRect.size.width/2,
                                                                  _btnCancel.frame.origin.y + _btnCancel.frame.size.height,
                                                                  textRect.size.width,
                                                                  1)];
        bgView.backgroundColor = [UIColor blackColor];
        [self.view addSubview:bgView];
    }
    
    
}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self removeEventListeners];
    
    // Enable iOS 7 back gesture
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
        self.navigationController.interactivePopGestureRecognizer.delegate = nil;
    }
    
    if (alertView) {
        [alertView dismissAnimated:YES completionHandler:nil];
        alertView = nil;
    }
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    return NO;
}

#pragma mark - Configure

-(void)configureStyles {
    [super configureStyles];

    [super navigationType01X:[UIImage imageNamed:@"btn_back"] centerText:@""];
}

-(void)configure {
    [super configure];
    
    // 2. init event
    _msgList = @[
                 
                 @[@"UD130",@""], // rider cancel order
                 @[@"UD230",@""], // rider cancel pickup
                 
                 @[@"PD130",@""], // driver cancel order
                 @[@"PD232",@""], // driver cancel pickup
                 @[@"PD330",@""], // driver cancel trip
                 ];
    
    [self registerEventListeners];
    
    //self.view.backgroundColor = UIColorLabelText;
    _lbTitle.textColor = [UIColor blackColor];
    _lbDetail.textColor = HEXCOLOR(0x666666ff);
    _lbTitle.text = @"";
    _lbDetail.text= @"";
    
    if (appDelegate.isDriverMode) {
        _imgIcon.image = [UIImage imageNamed:@"img_cnaceltrip"];
        // 본인 cancel
        if (_step == 1) {
            if (appDelegate.pstate == 220) {
                _lbTitle.text = LocalizedStr(@"Map.Rider.Pickup.Cancel.title");
                _lbDetail.text = LocalizedStr(@"Map.Driver.Request.Driver.Cancel.text");
            }
            else if (appDelegate.pstate == 230 && appDelegate.pstate2 == 10) {
                _lbTitle.text = LocalizedStr(@"Map.Rider.Pickup.Cancel.title");
                _lbDetail.text = LocalizedStr(@"Map.Driver.Pickup.Cancel.title");
            }
            else if (appDelegate.pstate == 230 && appDelegate.pstate2 == 20) {
                _lbTitle.text = LocalizedStr(@"Map.Rider.Pickup.Cancel.title");
                _lbDetail.text = LocalizedStr(@"Map.Driver.Pickup.Cancel.title");
            }
            else if (appDelegate.pstate == 240 && appDelegate.pstate2 == 10) {
                _lbTitle.text = LocalizedStr(@"Map.Rider.Pickup.Cancel.title");
                _lbDetail.text = LocalizedStr(@"Map.Driver.onTrip.Cancel.text");
            }
        }
        else {
            if (_state == 220) {
                _lbTitle.text = LocalizedStr(@"Map.Rider.Pickup.Cancel.title");
                _lbDetail.text = LocalizedStr(@"Map.Driver.Request.Rider.Cancel.text");
            }
            else if (_state == 230) {
                _lbTitle.text = LocalizedStr(@"Map.Rider.Pickup.Cancel.title");
                _lbDetail.text = LocalizedStr(@"Map.Driver.Request.Rider.Cancel.text");
            }
        }
        
        _btnCancel.hidden = YES;
    }
    else {
        // pickup_cancel
        if (_step == 1) {
            _imgIcon.image = [UIImage imageNamed:@"img_cnaceltrip"];
            _lbTitle.text = LocalizedStr(@"Map.Rider.Pickup.Cancel.title");
            _lbDetail.text = LocalizedStr(@"Map.Rider.Pickup.Cancel.text");
            _btnCancel.hidden = YES;
        }
        // pickup_canceled
        else if (_step == 2) {
            _imgIcon.image = [UIImage imageNamed:@"img_sorry"];
            _lbTitle.text = LocalizedStr(@"Map.Rider.Pickup.Canceled.title");
            _lbDetail.text = LocalizedStr(@"Map.Rider.Pickup.Canceled.text");
            _btnCancel.hidden = NO;
        }
        // trip_canceled
        else if (_step == 3) {
            _imgIcon.image = [UIImage imageNamed:@"img_sorry"];
            _lbTitle.text = LocalizedStr(@"Map.Rider.Pickup.Canceled.title");
            _lbDetail.text = LocalizedStr(@"Map.Rider.Pickup.Canceled.text");
            _btnCancel.hidden = NO;
        }
    }
    
    [_lbDetail sizeToFit];
    
#ifdef _WITZM
    [_btnSubmit setTitleColor:UIColorDefault forState:UIControlStateNormal];
#elif defined _CHACHA
    [_btnSubmit setTitleColor:UIColorButtonText forState:UIControlStateNormal];
#endif

    _btnSubmit.backgroundColor = UIColorDefault2;
    CGRect frame = _btnSubmit.frame;
    frame.origin.y -= kBottomBottonSafeArea;
    _btnSubmit.frame = frame;
    
    // pickup_cancel
    if (_step == 1) {
        [_btnSubmit setTitle:LocalizedStr(@"Map.Rider.Pickup.Cancel.BTN.text") forState:UIControlStateNormal];
    }
    // XXX_canceled
    else {
        if (appDelegate.isDriverMode) {
            [_btnSubmit setTitle:LocalizedStr(@"Button.OK") forState:UIControlStateNormal];

            // 5초후 자동 닫힘
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [self onNaviButtonClick:_btnSubmit];
            });
        }
        else {
            [_btnSubmit setTitle:LocalizedStr(@"Map.Rider.Pickup.Canceled.BTN.Submit.text") forState:UIControlStateNormal];
        }
        
    }
}

-(void) configureConfig {

    
}


-(void) registerEventListeners {
    for (NSArray *msg in _msgList) {
        [self->model addEventListener:self forEvent:[msg objectAtIndex:0] eventParams:nil];
    }
}

-(void) removeEventListeners {
    for (NSArray *msg in _msgList) {
        [self->model removeEventListener:self forEvent:[msg objectAtIndex:0]];
    }
}

#pragma mark - Action
-(IBAction)onNaviButtonClick:(id)sender {
    UIButton *btn = (UIButton*)sender;
    NSLog(@"onNaviButtonClick:%ld",(long)btn.tag);
    //[super onNaviButtonClick:sender];
    
    if ([self onCanceled]) return;
    
    // back arrow
    if (btn.tag == 1100) {
        
        if (_step != 1) {
            // pre order map
            [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_110 object:nil userInfo:nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_DISSMISS_CANCEL object:nil userInfo:@{@"cancel":@"1"}];
        }
        else {
            [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_DISSMISS_CANCEL object:nil userInfo:nil];
        }
        //[self.navigationController popViewControllerAnimated:YES];
    }
    else if (btn.tag == 2200) {
        
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_110 object:nil userInfo:nil];
        // pre order map
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_DISSMISS_CANCEL object:nil userInfo:@{@"cancel":@"1"}];
        //[self.navigationController popViewControllerAnimated:YES];
    }
    // cancel reason alert
    else if (btn.tag == 2300) {
        
        // pickup_cancel
        if (_step == 1) {
            [self onCancel:sender];
        }
        // XXX_canceled
        else {
            if (!appDelegate.isDriverMode) {
                // request again
                [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_111 object:nil userInfo:nil];
                [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_DISSMISS_CANCEL object:nil userInfo:@{@"cancel":@"1"}];
            }
            else {
                [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_DISSMISS_CANCEL object:nil userInfo:nil];
            }
            //[self.navigationController popViewControllerAnimated:YES];
        }
    }
}

-(BOOL)onCanceled {
    if ((appDelegate.isDriverMode && appDelegate.pstate < 220) || (!appDelegate.isDriverMode && appDelegate.ustate < 120)) {
        //[self.navigationController popViewControllerAnimated:YES];
        //[self dismissViewControllerAnimated:YES completion:^(){}];
        
        if (!appDelegate.isDriverMode) {
            // request again
            [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_111 object:nil userInfo:nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_DISSMISS_CANCEL object:nil userInfo:@{@"cancel":@"1"}];
        }
        else {
            [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_DISSMISS_CANCEL object:nil userInfo:nil];
        }
        
        return YES;
    }
    return NO;
}
-(IBAction)onCancel:(id)sender {
    
    NSString *alertmsg = @" ";
    if (appDelegate.isDriverMode) {
        if (appDelegate.pstate == 220) {
            alertmsg = LocalizedStr(@"Map.Driver.Request.Driver.Cancel.text");
        }
        if (appDelegate.pstate == 230 && appDelegate.pstate2 == 10) {
            alertmsg = LocalizedStr(@"Map.Driver.Pickup.Cancel.title");
        }
        else if (appDelegate.pstate == 230 && appDelegate.pstate2 == 20) {
            alertmsg = @"";
        }
        else if (appDelegate.pstate == 240) {
            alertmsg = LocalizedStr(@"Map.Driver.onTrip.Cancel.text");
        }
    }
    else {
        if (appDelegate.ustate == 120) {
            alertmsg = LocalizedStr(@"Map.Rider.Request.Cancel.text");
        }
        else if (appDelegate.ustate == 130) {
            alertmsg = LocalizedStr(@"Map.Rider.Pickup.Cancel.text");
        }
        else if (appDelegate.ustate == 140) {
            return;
        }
    }
    
    if (![alertmsg isEqualToString:@""]) {
        
        if ([alertmsg isEqualToString:@" "]) {
            [self dismissViewControllerAnimated:YES completion:nil];
            return;
        }
        
        appDelegate.isMyCancel = YES;
        if (appDelegate.isDriverMode) {
            if (appDelegate.pstate == 220) {
                [self onPD130:@""];
            }
            else if (appDelegate.pstate == 230 && appDelegate.pstate2 == 10) {
                [self onPD232:@""];
            }
            else if (appDelegate.pstate == 240) {
                [self onPD330:@""];
            }
        }
        else {
            if (appDelegate.ustate == 120) {
                [self onUD130];
            }
            else if (appDelegate.ustate == 130) {
                [self onUD230];
            }
        }
        
        return;
    }
    
    // 취소사유 appDelegate.pstate : 230.20
    dataArray = (NSMutableArray*)@[LocalizedStr(@"Map.Drive.Rider.Cancel.text1"),
                                   LocalizedStr(@"Map.Drive.Rider.Cancel.text2"),
                                   LocalizedStr(@"Map.Drive.Rider.Cancel.text3"),
                                   LocalizedStr(@"Map.Drive.Rider.Cancel.text4"),
                                   LocalizedStr(@"Map.Drive.Rider.Cancel.text5"),
                                   LocalizedStr(@"Map.Drive.Rider.Cancel.text6"),
                                   LocalizedStr(@"Map.Drive.Rider.Cancel.text7")];
    
    // 취소사유 선택화면 show
    UITableView* mTableView = [[UITableView alloc] init];
    mTableView.delegate=self;
    mTableView.dataSource=self;
    
    mTableView.allowsMultipleSelection = NO;
    mTableView.frame = CGRectMake(0.f, 0.f, 320, 260.f);
    mTableView.contentMode = UIViewContentModeScaleAspectFit;
    
    //            NSIndexPath *indexPath = nil;
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    
    if (indexPath != nil) {
        [mTableView selectRowAtIndexPath:indexPath
                                animated:YES
                          scrollPosition:UITableViewScrollPositionNone];
        //[mTableView tableView:mTableView didSelectRowAtIndexPath:indexPath];
        [mTableView.delegate tableView:mTableView didSelectRowAtIndexPath:indexPath];
    }
    
    alertView = [[LGAlertView alloc] initWithViewAndTitle:LocalizedStr(@"Map.Drive.Rider.Cancel.title")
                                                               message:@""
                                                                 style:LGAlertViewStyleAlert
                                                                  view:mTableView
                                                          buttonTitles:@[LocalizedStr(@"Button.OK")]
                                                     cancelButtonTitle:LocalizedStr(@"Button.CANCEL")
                                                destructiveButtonTitle:nil
                                                         actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                                                             NSLog(@"actionHandler, %@, %lu", title, (long unsigned)index);
                                                             
                                                             BOOL flag = NO;
                                                             NSString *rsncode = @"";
                                                             for(NSIndexPath* indexPath in [mTableView indexPathsForSelectedRows]) {
                                                                 NSLog(@"cell.tag:%@",[dataArray objectAtIndex:indexPath.row]);
                                                                 //rsncode = [dataArray objectAtIndex:indexPath.row];
                                                                 rsncode = [NSString stringWithFormat:@"%d",(int)(indexPath.row+1)];
                                                                 flag = YES;
                                                                 break;
                                                             }
                                                             
                                                             if (!flag) {
                                                                 return;
                                                             }
                                                             appDelegate.isMyCancel = YES;
                                                             if (appDelegate.isDriverMode) {
                                                                 if (appDelegate.pstate == 220 && appDelegate.pstate2 == 20) {
                                                                     [self onPD130:rsncode];
                                                                 }
                                                                 if (appDelegate.pstate == 230) {
                                                                     [self onPD232:rsncode];
                                                                 }
                                                                 else if (appDelegate.pstate == 240 && appDelegate.pstate2 == 10) {
                                                                     [self onPD330:rsncode];
                                                                 }
                                                             }
                                                             else {
                                                                 if (appDelegate.ustate == 120 && appDelegate.ustate2 == 20) {
                                                                     [self onUD130];
                                                                 }
                                                                 if (appDelegate.ustate == 130) {
                                                                     [self onUD230];
                                                                 }
                                                             }
                                                             
                                                             
                                                             [mTableView reloadData];
                                                             [mTableView setContentOffset:CGPointZero animated:YES];
                                                             //mTableView.indexPathsForSelectedRows = nil;
                                                             
                                                         }
                                                         cancelHandler:^(LGAlertView *alertView) {
                                                             NSLog(@"cancelHandler");
                                                         }
                                                    destructiveHandler:^(LGAlertView *alertView) {
                                                        NSLog(@"destructiveHandler");
                                                    }];
    //alertView.heightMax = 256.f;
    [Utils initAlertButtonColor:alertView];
    alertView.windowLevel = LGAlertViewWindowLevelAboveStatusBar;
    [alertView showAnimated:YES completionHandler:nil];
    
}

#pragma mark - onEvent
-(void)onUD130 {
    
    [self showBusyIndicator:@"Canceling Cue ... "];
    [self->model UD130];
}

-(void)onUD230 {
    
    [self showBusyIndicator:@"Canceling Cue ... "];
    [self->model UD230];
}


-(void)onPD330:(NSString*)rsncode {
    
    [self showBusyIndicator:@"Canceling Trip ... "];
    [self->model PD330:rsncode];
    
}

-(void)onPD232:(NSString*)rsncode {
    
    [self showBusyIndicator:@"Canceling Pickup ... "];
    [self->model PD232:rsncode];
}

-(void)onPD130:(NSString*)rsncode {
    
    [self showBusyIndicator:@"Canceling Order ... "];
    [self->model PD130:rsncode];
}


#pragma mark - onEvent
-(void)onEvent:(TXEvent *)event eventParams:(id)subscriptionParams {
    
    TXResponseDescriptor * descriptor = [event getEventProperty:TXEvents.Params.DESCRIPTOR];
    //TXRequestObj *request = [event getEventProperty:TXEvents.Params.REQUEST];
    
    if([event.name isEqualToString:@"PD130"] ||
       [event.name isEqualToString:@"PD232"]) { // 요청취소
        
        if(descriptor.success == true) {
            [self removeEventListeners];
            
            if ((appDelegate.pstate == 230 || appDelegate.pstate == 240) && appDelegate.pstate2 <= -10) {
                //
                [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_DISSMISS_CANCEL object:nil userInfo:@{@"cancel":@"1"}];
            }
            else {
                // 상태 초기화
                [appDelegate resetTripInfo];
                
                [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_DISSMISS_CANCEL object:nil userInfo:@{@"cancel":@"1"}];
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    NSLog(@"NOTIFICATION_DISSMISS_CANCEL");
                    //[self hideBusyIndicator];
                    //[self.navigationController popViewControllerAnimated:YES];
                });
            }
            
            
            //[self.navigationController popViewControllerAnimated:YES];
            //[self dismissViewControllerAnimated:YES completion:nil];
        } else {
            if (descriptor.rlt != -1000 && descriptor.rlt != -999999) {
                //에러주석[self alertError:@"Error" message:descriptor.error];
            }
            [self hideBusyIndicator];
        }
    }
    else if([event.name isEqualToString:@"PD330"]) { // trip del
        
        if(descriptor.success == true) {
            // 대기상태이 전이
            [self removeEventListeners];
            
            if ((appDelegate.pstate == 230 || appDelegate.pstate == 240) && appDelegate.pstate2 <= -10) {
                //
                [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_DISSMISS_CANCEL object:nil userInfo:@{@"cancel":@"1"}];
            }
            else {
                // 상태 초기화
                [appDelegate resetTripInfo];
                
                [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_DISSMISS_CANCEL object:nil userInfo:@{@"cancel":@"1"}];
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    NSLog(@"NOTIFICATION_DISSMISS_CANCEL");
                    //[self hideBusyIndicator];
                    //[self.navigationController popViewControllerAnimated:YES];
                });
            }
            
            
            //[self.navigationController popViewControllerAnimated:YES];
            //[self dismissViewControllerAnimated:YES completion:nil];
        } else {
            if (descriptor.rlt != -1000 && descriptor.rlt != -999999) {
                //에러주석[self alertError:@"Error" message:descriptor.error];
            }
            [self hideBusyIndicator];
        }
    }
    else if([event.name isEqualToString:@"UD130"]) { // del request
        
        if(descriptor.success == true) {
            // updateActivityState에서 110의 상태로 돌아간다.
            [self removeEventListeners];
            
            if ((appDelegate.ustate == 130 || appDelegate.ustate == 140) && appDelegate.ustate2 <= -10) {
                //
                [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_DISSMISS_CANCEL object:nil userInfo:@{@"cancel":@"1"}];
            }
            else {
                // 상태 초기화
                [appDelegate resetTripInfo];
                
                [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_DISSMISS_CANCEL object:nil userInfo:@{@"cancel":@"1"}];
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    NSLog(@"NOTIFICATION_DISSMISS_CANCEL");
                    //[self hideBusyIndicator];
                    //[self.navigationController popViewControllerAnimated:YES];
                });
            }
            
            
            //[self.navigationController popViewControllerAnimated:YES];
            //[self dismissViewControllerAnimated:YES completion:nil];
        } else {
            if (descriptor.rlt != -1000 && descriptor.rlt != -999999) {
                //에러주석[self alertError:@"Error" message:descriptor.error];
            }
            [self hideBusyIndicator];
        }
    }
    else if([event.name isEqualToString:@"UD230"]) { // del pickup
        
        if(descriptor.success == true) {
            // updateActivityState에서 110의 상태로 돌아간다.
            [self removeEventListeners];
            
            if ((appDelegate.ustate == 130 || appDelegate.ustate == 140) && appDelegate.ustate2 <= -10) {
                //
                [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_110 object:nil userInfo:nil];
                [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_DISSMISS_CANCEL object:nil userInfo:@{@"cancel":@"1"}];
            }
            else {
                // 상태 초기화
                [appDelegate resetTripInfo];
                
                [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_110 object:nil userInfo:nil];
                [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_DISSMISS_CANCEL object:nil userInfo:@{@"cancel":@"1"}];
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    NSLog(@"NOTIFICATION_DISSMISS_CANCEL");
                    //[self hideBusyIndicator];
                    //[self.navigationController popViewControllerAnimated:YES];
                });
            }
            
            
            //[self.navigationController popViewControllerAnimated:YES];
            //[self dismissViewControllerAnimated:YES completion:nil];
        } else {
            if (descriptor.rlt != -1000 && descriptor.rlt != -999999) {
                //에러주석[self alertError:@"Error" message:descriptor.error];
            }
            [self hideBusyIndicator];
        }
    }
    else {
        [self hideBusyIndicator];
    }
}

#pragma mark - UITableView DataSource Methods
#pragma mark - UITableView DataSource Methods
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [tableView setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [tableView setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
    }

    if ([dataArray count] != (indexPath.row+1)) {
        UIView *seperatorView = [[UIView alloc] initWithFrame:CGRectMake(10, cell.frame.size.height - 1, cell.frame.size.width - 10*2, 1)];
        seperatorView.backgroundColor = UIColorTableSeperator;
        [cell addSubview:seperatorView];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    // This will create a "invisible" footer
    return 0.01f;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [UIView new];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [dataArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    cell.tag = indexPath.row;
    int tag = (int)indexPath.row+100;
    //[self changeCell:tableView withIndexPath:indexPath andTag:tag];
    [self addCell:tableView withCell:cell withIndexPath:indexPath andTag:tag];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    cell.textLabel.text = [NSString stringWithFormat:@"      %@",[dataArray objectAtIndex:indexPath.row]];
    
    return cell;
}

#pragma mark - UITableView Delegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self onCanceled]) return;
    [self changeCell:tableView withIndexPath:indexPath andTag:((int)indexPath.row+100)];
    
    //cell.accessoryView.hidden = NO;
}
- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self onCanceled]) return;
    [self changeCell:tableView withIndexPath:indexPath andTag:((int)indexPath.row+100)];
    
    //cell.accessoryView.hidden = YES;
}

-(void)addCell:(UITableView*)tableView withCell:(UITableViewCell*)cell withIndexPath:(NSIndexPath*)indexPath andTag:(int)tag{
    
    UIImageView *preView = [cell.contentView viewWithTag:tag];
    [preView removeFromSuperview];
    
    UILabel *preLabel = [cell.contentView viewWithTag:(tag+100)];
    [preLabel removeFromSuperview];
    
    NSInteger _h = cell.frame.size.height;
    NSInteger _x = 16;
    UIImageView* imv = [[UIImageView alloc]initWithFrame:CGRectMake(cell.frame.size.width - _x*4 ,_h/2-_x/2, _x, _x)];
    imv.tag = tag;
    UIImage *cellImage;
    
    if([[tableView indexPathsForSelectedRows] containsObject:indexPath]) {
        cellImage = [UIImage imageNamed:@"form_radio_chk"];
    }
    else {
        cellImage = [UIImage imageNamed:@"form_radio_normal"];
    }
    imv.image=cellImage;
    [cell.contentView addSubview:imv];
}

-(void)changeCell:(UITableView*)tableView withIndexPath:(NSIndexPath*)indexPath andTag:(int)tag{
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    UIImageView *preView = [cell.contentView viewWithTag:tag];
    [preView removeFromSuperview];
    
    UILabel *preLabel = [cell.contentView viewWithTag:(tag+100)];
    [preLabel removeFromSuperview];
    
    NSInteger _h = cell.frame.size.height;
    NSInteger _x = 16;
    UIImageView* imv = [[UIImageView alloc]initWithFrame:CGRectMake(cell.frame.size.width - _x*4 ,_h/2-_x/2, _x, _x)];
    //    UIImageView* imv = [[UIImageView alloc]initWithFrame:CGRectMake(0,_h/2-_x/2, _x, _x)];
    imv.tag = tag;
    UIImage *cellImage;
    
    if([[tableView indexPathsForSelectedRows] containsObject:indexPath]) {
        cellImage = [UIImage imageNamed:@"form_radio_chk"];
    }
    else {
        cellImage = [UIImage imageNamed:@"form_radio_normal"];
    }
    imv.image=cellImage;
    [cell.contentView addSubview:imv];
}

@end
