//
//  TXProfileVC.m
//  Taxi
//

//

#import "TXMapCallVC.h"
#import "utils.h"
#import "LGAlertView.h"
#import "PulsingHaloLayer.h"
#import <GoogleMaps/GoogleMaps.h>

@interface TXMapCallVC () <UITableViewDelegate,UITableViewDataSource>{
    int userId;
    NSArray *_msgList;
    
    NSArray *dataArray;
    
    NSTimer *waitTimer;
    NSInteger waitTimerTime;
    BOOL isPause;
}
@property (nonatomic, weak) PulsingHaloLayer *halo;
@property (strong, nonatomic) IBOutlet UIView *alertView;

@end

@implementation TXMapCallVC {
}

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    
    if(self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {

    }
    
    return self;
}

-(void)viewDidLoad {
    [super viewDidLoad];
    [self hideBusyIndicator];
    isPause = NO;
    
    //_lbTimer.text = @"";
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    [self startTimer];
    
#ifdef _DRIVER_MODE
    
#ifdef _WITZM
    UIView *v = [self.view viewWithTag:300];
    v.hidden = YES;
#elif defined _CHACHA
    UIView *v = [self.view viewWithTag:300];
    v.hidden = YES;
#endif
    
#else
    
#ifdef _WITZM
#elif defined _CHACHA
#endif
    
#endif

}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self removeEventListeners];
    [self stopTimer];
//    [self.halo removeFromSuperlayer];
//    self.halo = nil;
}

-(void)dealloc {
    [self stopTimer];
}

#pragma mark - Configure

-(void)configureStyles {
    [super configureStyles];
}

-(void)configure {
    [super configure];
    
    UIView *n = [super navigationView];
    //NSInteger _y = n.frame.origin.y + n.frame.size.height;
    n.hidden = YES;
    
    // 2. init event
    _msgList = @[
                 
                 @[@"UD130",@""], // rider cancel
                 @[@"UD131",@""], // order timeout
                 @[@"UD230",@""], // rider cancel pickup
                 
                 @[@"PD130",@""], // driver cancel order
                 @[@"PD232",@""], // driver cancel pickup
                 @[@"PD330",@""], // driver cancel trip
                 
                 @[@"PD131",@""], // timeout order
                 @[@"PD220",@""], // 220.20이 되면 바로 올린다. pickup start -> 230.10
                 
                 ];
    
    [self registerEventListeners];
    
    CGRect frame = _btnCancel.frame;
    frame.origin.y = kTopHeight + 10;
    _btnCancel.frame = frame;
    
    _lbMatch.textColor = HEXCOLOR(0x333333ff);
    _lbTimer.textColor = HEXCOLOR(0x333333ff);
    
    _lbPickup.textColor = UIColorDefault;
    _lbPickupAddr.textColor = [UIColor blackColor];
    
    _lbDest.textColor = UIColorDefault;
    _lbDestAddr.textColor = [UIColor blackColor];
    
    vLine.backgroundColor = UIColorDefault;
    
    _lbBottomDesc.backgroundColor = UIColorDefault;
    frame = _lbBottomDesc.frame;
    frame.origin.y -= kBottomBottonSafeArea;
    _lbBottomDesc.frame = frame;
    
    if (appDelegate.isDriverMode) {
        _lbPickup.text = LocalizedStr(@"Map.Driver.Request.Pickup.text");
        _lbDest.text   = LocalizedStr(@"Map.Driver.Request.Destination.text");
        _lbMatch.text  = LocalizedStr(@"Map.Driver.Request.Timer.text");
        
        vLine.hidden = YES;
        _lbBottomDesc.hidden = YES;
    }
    else {
        _lbPickup.text = LocalizedStr(@"Map.Rider.Request.Pickup.text");
        _lbDest.text   = LocalizedStr(@"Map.Rider.Request.Destination.text");
        _lbMatch.text  = LocalizedStr(@"Map.Rider.Request.Timer.text");
        
        _lbBottomDesc.text  = LocalizedStr(@"Map.Rider.Request.Bottom.Desc.text");
    }
    
    
    UIButton *btn = [self.view viewWithTag:1100];
    btn.hidden = NO;

    UIButton *btnClose = [self.view viewWithTag:2100];
    [btnClose setImage:nil forState:UIControlStateNormal];
    btnClose.hidden = NO;
    btnClose = [self.view viewWithTag:2101];
    btnClose.hidden = NO;
    btnClose.layer.borderColor            = UIColorLabelTextColor.CGColor;
    btnClose.layer.borderWidth            = 1.0f;
    btnClose.layer.cornerRadius           = 4.0f;
    //            _btnClose.backgroundColor               = HEXCOLOR(0xf7f7f7FF);
    //            _btnClose.tintColor                     = HEXCOLOR(0xd42327FF);
    [btnClose setImage:nil forState:UIControlStateNormal];
    [btnClose setTitle:LocalizedStr(@"Button.CANCEL") forState:UIControlStateNormal];

}

-(void) configureConfig {
    
    
}

-(void) registerEventListeners {
    for (NSArray *msg in _msgList) {
        [self->model addEventListener:self forEvent:[msg objectAtIndex:0] eventParams:nil];
    }
}

-(void) removeEventListeners {
    for (NSArray *msg in _msgList) {
        [self->model removeEventListener:self forEvent:[msg objectAtIndex:0]];
    }
}

- (void) initAddress:(NSString*)__srcaddr dest:(NSString*)__dstaddr;
{
    _lbPickupAddr.text = __srcaddr;
    _lbDestAddr.text = __dstaddr;
}

- (void) initLocation:(NSString*)__srcaddr dest:(NSString*)__dstaddr;
{
    [self getAddress:[Utils getLocation:__srcaddr] target:_lbPickupAddr];
    [self getAddress:[Utils getLocation:__dstaddr] target:_lbDestAddr];
}

#pragma mark - waitTimer
// http://stockengineer.tistory.com/226
- (void)startTimer {
    // 타이머 셋팅 시작 - 개별 선택된 셀의 타이머가 따로 작동한다
    [self stopTimer];
    
    waitTimerTime = 0;
    [self printTimer];
    waitTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timerMethod:) userInfo:nil repeats:YES];
    
    self.halo = [PulsingHaloLayer layer];
    
    // setup
    self.halo.position =vHalo.center;
    
    self.halo.haloLayerNumber = 3; // 총 나타나는 수
    self.halo.radius = vHalo.frame.size.width/2; // 원의 반지름
    self.halo.animationDuration = 2.5; // 원 표시 지속시간
    self.halo.useTimingFunction = NO;
    //self.halo.repeatCount = 1;
    self.halo.fromValueForRadius = 0.10; // 시작 원 지름
    self.halo.keyTimeForHalfOpacity = 0.2;
    //        halo.backgroundColor = 1;
    
    UIColor *color = UIColorDefault;
    [self.halo setBackgroundColor:color.CGColor];
    [vHalo.layer addSublayer:self.halo];
    
    [self.halo start];
    
    if (appDelegate.isDriverMode) {
        [appDelegate loopSound:SOUND_ORDER];
    }

}

- (void)pauseTimer {
    isPause = YES;
}

- (void)stopTimer {
    if (waitTimer != nil) {
        [waitTimer invalidate];
        waitTimer = nil;
    }
    [self printTimer];
    //waitTimerTime = 0;
    
    if (appDelegate.isDriverMode) {
        [appDelegate stopSound];
    }
}

-(void)timerMethod:(NSTimer *)timer
{
    if (isPause) {
        return;
    }
    waitTimerTime++;
    
    // 타이머 종료 시, 알람메시지를 띄운다
    if( waitTimerTime == (appDelegate.isDriverMode?_RESPONSE_TIME_OUT:_REQUEST_TIME_OUT))
    {
        // 타이머를 종료한다
        [waitTimer invalidate];
        waitTimer = nil;
        [timer invalidate];
        timer = nil;
        
        [self stopTimer];
        // timeout
        
        if (appDelegate.isDriverMode) {
            [self onPD131];
        }
        else {
            [self onUD131];
        }
        //[self dismissViewControllerAnimated:YES completion:nil];
        return;
    }
    [self printTimer];
}

-(void)printTimer {
    // 화면 재로드
    NSInteger t = (appDelegate.isDriverMode?_RESPONSE_TIME_OUT:_REQUEST_TIME_OUT) - waitTimerTime;
    //_lbTimer.text = [NSString stringWithFormat:@"%02d:%02d",(int)(t/60), (int)(t%60)];
    _lbTimer.text = [NSString stringWithFormat:@"%02d",(int)(t%60)];
}

#pragma mark - Action
-(IBAction)onNaviButtonClick:(id)sender {
    UIButton *btn = (UIButton*)sender;
    NSLog(@"onNaviButtonClick");
    //[super onNaviButtonClick:sender];
    
    if (btn.tag == 2100 || btn.tag == 2101) {
        // 취소 버튼을 막는다.
        UIButton *btnClose = [self.view viewWithTag:2101];
        btnClose.enabled = NO;
        btnClose = [self.view viewWithTag:2100];
        btnClose.enabled = NO;
        
        [self removeEventListeners];
        
        [self stopTimer];
        //[self.navigationController popViewControllerAnimated:YES];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else if (btn.tag == 2200) {
        if (appDelegate.isDriverMode) {
            // 수락
            //[self onPD110]; // order accept를 하지 말고 pickup request를 한다.
            // 취소 버튼을 막는다.
            UIButton *btnClose = [self.view viewWithTag:2101];
            btnClose.enabled = NO;
            btnClose = [self.view viewWithTag:2100];
            btnClose.enabled = NO;
            [self onPD220];
        }
    }
}

-(void)getAddress:(CLLocationCoordinate2D)coor target:(UILabel*)__label
{
    if (![GoogleUtils isLocationValid:coor]) {
        return;
    }
    
    NSString *theLocation = [NSString stringWithFormat:@"latitude: %f longitude: %f", coor.latitude, coor.longitude];
    NSLog(@"theLocation:%@",theLocation);
    
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    [locMgr geocodeAPICall:coor
                completion:^(NSDictionary *user, NSString* params, NSString *str, int status) {
                    
                    if (status==1) {
                        //NSAssert(![NSThread isMainThread], @"This shouldn't be called from the main thread");
                        //NSLog(@"responseDict:%@",user);
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            // Update the UI
                            __label.text = user[@"fullAddress"];
                        });
                    }
                }];
    

    return;
}

-(IBAction)onCancel:(id)sender {
    
    NSString *alertmsg = @" ";
    if (appDelegate.isDriverMode) {
        if (appDelegate.pstate == 220) {
            alertmsg = LocalizedStr(@"Map.Driver.Request.Driver.Cancel.text");
        }
        if (appDelegate.pstate == 230 && appDelegate.pstate2 == 10) {
            alertmsg = LocalizedStr(@"Map.Driver.Pickup.Cancel.title");
        }
        else if (appDelegate.pstate == 230 && appDelegate.pstate2 == 20) {
            alertmsg = @"";
        }
        else if (appDelegate.pstate == 240) {
            alertmsg = LocalizedStr(@"Map.Driver.onTrip.Cancel.text");
        }
    }
    else {
        if (appDelegate.ustate == 120) {
            alertmsg = LocalizedStr(@"Map.Rider.Request.Cancel.text");
        }
        else if (appDelegate.ustate == 130) {
            alertmsg = LocalizedStr(@"Map.Rider.Pickup.Cancel.text");
        }
        else if (appDelegate.ustate == 140) {
            return;
        }
    }
    
    if (![alertmsg isEqualToString:@""]) {
        
        if ([alertmsg isEqualToString:@" "]) {
            [self dismissViewControllerAnimated:YES completion:nil];
            return;
        }
        
        LGAlertView *alertView = [[LGAlertView alloc] initWithTitle:nil
                                                            message:alertmsg
                                                              style:LGAlertViewStyleAlert
                                                       buttonTitles:@[LocalizedStr(@"Button.OK")]
                                                  cancelButtonTitle:LocalizedStr(@"Button.CANCEL")
                                             destructiveButtonTitle:nil
                                                      actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                                                          NSLog(@"actionHandler, %@, %lu", title, (long unsigned)index);
                                                          
                                                          // 취소 버튼을 막는다.
                                                          UIButton *btnClose = [self.view viewWithTag:2101];
                                                          btnClose.enabled = NO;
                                                          btnClose = [self.view viewWithTag:2100];
                                                          btnClose.enabled = NO;
                                                          
                                                          appDelegate.isMyCancel = YES;
                                                          if (appDelegate.isDriverMode) {
                                                              if (appDelegate.pstate == 220) {
                                                                  [self onPD130:@""];
                                                              }
                                                              else if (appDelegate.pstate == 230 && appDelegate.pstate2 == 10) {
                                                                  [self onPD232:@""];
                                                              }
                                                              else if (appDelegate.pstate == 240) {
                                                                  [self onPD330:@""];
                                                              }
                                                          }
                                                          else {
                                                              if (appDelegate.ustate == 120) {
                                                                  [self onUD130];
                                                              }
                                                              else if (appDelegate.ustate == 130) {
                                                                  [self onUD230];
                                                              }
                                                          }
                                                      }
                                                      cancelHandler:^(LGAlertView *alertView) {
                                                      }
                                                 destructiveHandler:^(LGAlertView *alertView) {
                                                 }];
        [Utils initAlertButtonColor:alertView];
        [alertView showAnimated:YES completionHandler:nil];
        
        return;
    }
    
    // 취소사유 appDelegate.pstate : 230.20
    dataArray = (NSMutableArray*)@[LocalizedStr(@"Map.Drive.Rider.Cancel.text1"),
                                   LocalizedStr(@"Map.Drive.Rider.Cancel.text2"),
                                   LocalizedStr(@"Map.Drive.Rider.Cancel.text3"),
                                   LocalizedStr(@"Map.Drive.Rider.Cancel.text4"),
                                   LocalizedStr(@"Map.Drive.Rider.Cancel.text5"),
                                   LocalizedStr(@"Map.Drive.Rider.Cancel.text6"),
                                   LocalizedStr(@"Map.Drive.Rider.Cancel.text7")];
    
    // 취소사유 선택화면 show
    UITableView* mTableView = [[UITableView alloc] init];
    mTableView.delegate=self;
    mTableView.dataSource=self;
    
    mTableView.allowsMultipleSelection = NO;
    mTableView.frame = CGRectMake(0.f, 0.f, 320, 260.f);
    mTableView.contentMode = UIViewContentModeScaleAspectFit;
    
    //            NSIndexPath *indexPath = nil;
    
    LGAlertView *alertView = [[LGAlertView alloc] initWithViewAndTitle:LocalizedStr(@"Map.Drive.Rider.Cancel.title")
                                                               message:@""
                                                                 style:LGAlertViewStyleAlert
                                                                  view:mTableView
                                                          buttonTitles:@[LocalizedStr(@"Button.OK")]
                                                     cancelButtonTitle:LocalizedStr(@"Button.CANCEL")
                                                destructiveButtonTitle:nil
                                                         actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                                                             NSLog(@"actionHandler, %@, %lu", title, (long unsigned)index);
                                                             
                                                             BOOL flag = NO;
                                                             NSString *rsncode = @"";
                                                             for(NSIndexPath* indexPath in [mTableView indexPathsForSelectedRows]) {
                                                                 NSLog(@"cell.tag:%@",[dataArray objectAtIndex:indexPath.row]);
                                                                 //rsncode = [dataArray objectAtIndex:indexPath.row];
                                                                 rsncode = [NSString stringWithFormat:@"%d",(int)(indexPath.row+1)];
                                                                 flag = YES;
                                                                 break;
                                                             }
                                                             
                                                             if (!flag) {
                                                                 return;
                                                             }
                                                             appDelegate.isMyCancel = YES;
                                                             if (appDelegate.isDriverMode) {
                                                                 if (appDelegate.pstate == 220 && appDelegate.pstate2 == 20) {
                                                                     [self onPD130:rsncode];
                                                                 }
                                                                 if (appDelegate.pstate == 230) {
                                                                     [self onPD232:rsncode];
                                                                 }
                                                                 else if (appDelegate.pstate == 240 && appDelegate.pstate2 == 10) {
                                                                     [self onPD330:rsncode];
                                                                 }
                                                             }
                                                             else {
                                                                 if (appDelegate.ustate == 120 && appDelegate.ustate2 == 20) {
                                                                     [self onUD130];
                                                                 }
                                                                 if (appDelegate.ustate == 130) {
                                                                     [self onUD230];
                                                                 }
                                                             }
                                                             
                                                             
                                                             [mTableView reloadData];
                                                             [mTableView setContentOffset:CGPointZero animated:YES];
                                                             //mTableView.indexPathsForSelectedRows = nil;
                                                             
                                                         }
                                                         cancelHandler:^(LGAlertView *alertView) {
                                                             NSLog(@"cancelHandler");
                                                         }
                                                    destructiveHandler:^(LGAlertView *alertView) {
                                                        NSLog(@"destructiveHandler");
                                                    }];
    //alertView.heightMax = 256.f;
    [Utils initAlertButtonColor:alertView];
    alertView.windowLevel = LGAlertViewWindowLevelAboveStatusBar;
    [alertView showAnimated:YES completionHandler:nil];
    
}

#pragma mark - onEvent

-(void)onUD130 {
    
    [self showBusyIndicator:@"Canceling Cue ... "];
    [self->model UD130];
}

-(void)onUD131 {
    
    if ([Utils checkStateUIInfo:UI_STATE_UD131 oldvalue:@"UD131"] == UI_STATE_STEP_RUN) {
        return;
    }
    [Utils setStateUIInfo:UI_STATE_UD131 value:UI_STATE_STEP_RUN oldvalue:@"UD131"];
    
    appDelegate.isMyCancel = YES;
    [self showBusyIndicator:@"Canceling Cue ... timeout"];
    [self->model UD131];
}

-(void)onUD230 {
    
    [self showBusyIndicator:@"Canceling Cue ... "];
    [self->model UD230];
}


-(void)onPD330:(NSString*)rsncode {
    
    [self showBusyIndicator:@"Canceling Trip ... "];
    [self->model PD330:rsncode];
    
}

-(void)onPD232:(NSString*)rsncode {
    
    [self showBusyIndicator:@"Canceling Pickup ... "];
    [self->model PD232:rsncode];
}

-(void)onPD130:(NSString*)rsncode {
    
    [self showBusyIndicator:@"Canceling Order ... "];
    [self->model PD130:rsncode];
}

-(void)onPD131 {
    if (appDelegate.isDriverMode && appDelegate.ustate == 220 && appDelegate.ustate2 == 20) {
        return;
    }
    if (appDelegate.isDriverMode && appDelegate.ustate == 230 && appDelegate.ustate2 == 10) {
        return;
    }
    
    if ([Utils checkStateUIInfo:UI_STATE_PD131 oldvalue:@"PD131"] == UI_STATE_STEP_RUN) {
        return;
    }
    [Utils setStateUIInfo:UI_STATE_PD131 value:UI_STATE_STEP_RUN oldvalue:@"PD131"];
    
    appDelegate.isMyCancel = YES;
    [self showBusyIndicator:@"Canceling Cue ... timeout"];
    [self->model PD131];
}

-(void)onPD110 {
    
    [appDelegate stopSound];
    
    [self showBusyIndicator:@"Acceptting Order ... "];
    
    // pd220으로 대체
    //[self->model PD110];
}

-(void)onPD220 {
    [self showBusyIndicator:@"Picking Up ... "];
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_DISSMISS_CALL object:nil userInfo:nil];
    // 요청을 하면 타이머는 잠시 멈춘다.
    [self pauseTimer];
}

#pragma mark - onEvent
-(void)onEvent:(TXEvent *)event eventParams:(id)subscriptionParams {
    
    TXResponseDescriptor * descriptor = [event getEventProperty:TXEvents.Params.DESCRIPTOR];
    //TXRequestObj *request = [event getEventProperty:TXEvents.Params.REQUEST];
    
    [self hideBusyIndicator];

    if([event.name isEqualToString:@"PD130"] ||
            [event.name isEqualToString:@"PD232"]) { // 요청취소
        
        if(descriptor.success == true) {
            [self removeEventListeners];

            // 상태 초기화
            [appDelegate resetTripInfo];
            //
            [self stopTimer];
            
            //[self.navigationController popViewControllerAnimated:YES];
            [self dismissViewControllerAnimated:YES completion:nil];
        } else {
            if (descriptor.rlt != -1000 && descriptor.rlt != -999999) {
                //에러주석[self alertError:@"Error" message:descriptor.error];
            }
        }
    }
    else if([event.name isEqualToString:@"UD131"]) { // 요청timeout
        
        [Utils setStateUIInfo:UI_STATE_UD131 value:UI_STATE_STEP_FIN oldvalue:nil];
        
        if(descriptor.success == true) {
            [self removeEventListeners];

            // 상태 초기화
            [appDelegate resetTripInfo];
            //
            [self stopTimer];

            //[self.navigationController popViewControllerAnimated:YES];
            [self dismissViewControllerAnimated:YES completion:nil];
        } else {
            if (descriptor.rlt != -1000 && descriptor.rlt != -999999) {
                //에러주석[self alertError:@"Error" message:descriptor.error];
            }
        }
    }
    else if([event.name isEqualToString:@"PD330"]) { // trip del
        
        if(descriptor.success == true) {
            // 대기상태이 전이
            [self removeEventListeners];

            // 상태 초기화
            [appDelegate resetTripInfo];
            //
            [self stopTimer];
            
            //[self.navigationController popViewControllerAnimated:YES];
            [self dismissViewControllerAnimated:YES completion:nil];
        } else {
            if (descriptor.rlt != -1000 && descriptor.rlt != -999999) {
                //에러주석[self alertError:@"Error" message:descriptor.error];
            }
        }
    }
    else if([event.name isEqualToString:@"UD130"] || [event.name isEqualToString:@"UD230"]) { // del pickup
        
        if(descriptor.success == true) {
            // updateActivityState에서 110의 상태로 돌아간다.
            [self removeEventListeners];

            
            // 상태 초기화
            [appDelegate resetTripInfo];
            //
            [self stopTimer];
            
            //[self.navigationController popViewControllerAnimated:YES];
            [self dismissViewControllerAnimated:YES completion:nil];
        } else {
            if (descriptor.rlt != -1000 && descriptor.rlt != -999999) {
                //에러주석[self alertError:@"Error" message:descriptor.error];
            }
        }
    }
    else if([event.name isEqualToString:@"PD220"]) { // pickup request
        
        if(descriptor.success == true) {
            // 지도를 그린다.
            //[self updateTripLineClock];
            //
            [self stopTimer];
            
            //[self callLoop:@"PD211" withCancel:YES andDirect:NO];
            [self dismissViewControllerAnimated:YES completion:nil];
        } else {
            if (descriptor.rlt != -1000 && descriptor.rlt != -999999) {
                //에러주석[self alertError:@"Error" message:descriptor.error];
            }
        }
    }
}

#pragma mark - UITableView DataSource Methods
#pragma mark - UITableView DataSource Methods
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [tableView setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [tableView setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
    
    UIView *seperatorView = [[UIView alloc] initWithFrame:CGRectMake(10, cell.frame.size.height - 1, cell.frame.size.width - 10*2, 1)];
    seperatorView.backgroundColor = UIColorTableSeperator;
    [cell addSubview:seperatorView];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    // This will create a "invisible" footer
    return 0.01f;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [UIView new];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [dataArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    cell.tag = indexPath.row;
    int tag = (int)indexPath.row+100;
    //[self changeCell:tableView withIndexPath:indexPath andTag:tag];
    [self addCell:tableView withCell:cell withIndexPath:indexPath andTag:tag];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    cell.textLabel.text = [NSString stringWithFormat:@"      %@",[dataArray objectAtIndex:indexPath.row]];
    
    return cell;
}

#pragma mark - UITableView Delegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self changeCell:tableView withIndexPath:indexPath andTag:((int)indexPath.row+100)];
    
    //cell.accessoryView.hidden = NO;
}
- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self changeCell:tableView withIndexPath:indexPath andTag:((int)indexPath.row+100)];
    
    //cell.accessoryView.hidden = YES;
}

-(void)addCell:(UITableView*)tableView withCell:(UITableViewCell*)cell withIndexPath:(NSIndexPath*)indexPath andTag:(int)tag{
    
    UIImageView *preView = [cell.contentView viewWithTag:tag];
    [preView removeFromSuperview];
    
    UILabel *preLabel = [cell.contentView viewWithTag:(tag+100)];
    [preLabel removeFromSuperview];
    
    NSInteger _h = cell.frame.size.height;
    NSInteger _x = 16;
    UIImageView* imv = [[UIImageView alloc]initWithFrame:CGRectMake(cell.frame.size.width - _x*4 ,_h/2-_x/2, _x, _x)];
    imv.tag = tag;
    UIImage *cellImage;
    
    if([[tableView indexPathsForSelectedRows] containsObject:indexPath]) {
        cellImage = [UIImage imageNamed:@"form_radio_chk"];
    }
    else {
        cellImage = [UIImage imageNamed:@"form_radio_normal"];
    }
    imv.image=cellImage;
    [cell.contentView addSubview:imv];
}

-(void)changeCell:(UITableView*)tableView withIndexPath:(NSIndexPath*)indexPath andTag:(int)tag{
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    UIImageView *preView = [cell.contentView viewWithTag:tag];
    [preView removeFromSuperview];
    
    UILabel *preLabel = [cell.contentView viewWithTag:(tag+100)];
    [preLabel removeFromSuperview];
    
    NSInteger _h = cell.frame.size.height;
    NSInteger _x = 16;
    UIImageView* imv = [[UIImageView alloc]initWithFrame:CGRectMake(cell.frame.size.width - _x*4 ,_h/2-_x/2, _x, _x)];
    //    UIImageView* imv = [[UIImageView alloc]initWithFrame:CGRectMake(0,_h/2-_x/2, _x, _x)];
    imv.tag = tag;
    UIImage *cellImage;
    
    if([[tableView indexPathsForSelectedRows] containsObject:indexPath]) {
        cellImage = [UIImage imageNamed:@"form_radio_chk"];
    }
    else {
        cellImage = [UIImage imageNamed:@"form_radio_normal"];
    }
    imv.image=cellImage;
    [cell.contentView addSubview:imv];
}

@end
