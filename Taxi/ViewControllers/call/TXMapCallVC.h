//
//  TXNewsDetailVC.h
//  TXNewsDetailVC
//

//

#import <UIKit/UIKit.h>
#import "TXButton.h"
#import "TXUserVC.h"

@interface TXMapCallVC : TXUserVC {
    
    IBOutlet UIView          *vHalo;
    
    IBOutlet UIView          *vLine;
}
@property (nonatomic, weak) IBOutlet UIImageView *beaconView;
@property (nonatomic, strong) IBOutlet UILabel *lbMatch;
@property (nonatomic, strong) IBOutlet UILabel *lbTimer;
@property (nonatomic, strong) IBOutlet UILabel *lbPickup;
@property (nonatomic, strong) IBOutlet UILabel *lbPickupAddr;
@property (nonatomic, strong) IBOutlet UILabel *lbDest;
@property (nonatomic, strong) IBOutlet UILabel *lbDestAddr;
@property (nonatomic, strong) IBOutlet UILabel *lbBottomDesc;
@property (nonatomic, strong) IBOutlet UIButton *btnCancel;

- (void) initAddress:(NSString*)__srcaddr dest:(NSString*)__dstaddr;
- (void) initLocation:(NSString*)__srcaddr dest:(NSString*)__dstaddr;

@end

