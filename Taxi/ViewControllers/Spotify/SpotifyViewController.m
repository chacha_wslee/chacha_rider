/*
 Copyright 2015 Spotify AB

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */

#import "SpotifyViewController.h"
#import <SpotifyAuthentication/SpotifyAuthentication.h>
#import <SpotifyMetadata/SpotifyMetadata.h>
#import <AVFoundation/AVFoundation.h>

@interface SpotifyViewController () <SPTAudioStreamingDelegate>

@property (weak, nonatomic) IBOutlet UILabel *trackTitle;
@property (weak, nonatomic) IBOutlet UILabel *artistTitle;
@property (weak, nonatomic) IBOutlet UIImageView *coverView;
//@property (weak, nonatomic) IBOutlet UIImageView *coverView2;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinner;
@property (weak, nonatomic) IBOutlet UISlider *progressSlider;
@property (weak, nonatomic) IBOutlet UILabel *playbackSourceTitle;

@property (nonatomic, strong) SPTAudioStreamingController *player;

@property IBOutlet UIButton* nextButton;
@property IBOutlet UIButton* prevButton;

@end

@implementation SpotifyViewController

-(void)viewDidLoad {
    [super viewDidLoad];
    self.trackTitle.text = @"";
    self.artistTitle.text = @"";
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateUI)
                                                 name:@"NotificationSpotifyUpdateReceived"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(closeSession)
                                                 name:@"NotificationSpotifyCloseSessionReceived"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(audioStreaming:)
                                                 name:@"NotificationSpotifyErrorReceived"
                                               object:nil];
    
    
    appDelegate.spotifyDelegate = self;
    
    // Customization for play slider
    self.progressSlider.minimumTrackTintColor = UIColorDefault;
    //    UIImage *sliderMinTrackImage = [UIImage imageNamed: @"slider_min"];
    //    [self.playSlider setMinimumTrackImage:sliderMinTrackImage forState:UIControlStateNormal];
    self.progressSlider.maximumTrackTintColor = HEXCOLOR(0x666666FF);
//    UIImage *playimage = [[UIImage imageNamed:@"ico_radio_playcursor"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
//    [self.progressSlider setThumbImage:playimage forState:UIControlStateNormal];
    
    
//    if (appDelegate.player != nil) {
//        
//        [self didChangeUISpotify:NO];
//        [self updateUI];
//        //[self didChangePositionSpotify:self.player.playbackState.position];
//        self.progressSlider.value = self.player.playbackState.position/appDelegate.player.metadata.currentTrack.duration;
//    }
    
    CALayer *maskLayer = [CALayer layer];
    maskLayer.frame = self.coverView.bounds;
    maskLayer.shadowRadius = 5;
    maskLayer.shadowPath = CGPathCreateWithRoundedRect(CGRectInset(self.coverView.bounds, 10, 10), 20, 20, nil);
    maskLayer.shadowOpacity = 1;
    maskLayer.shadowOffset = CGSizeZero;
    maskLayer.shadowColor = [UIColor blackColor].CGColor;
    
//    CALayer *sublayer = [CALayer layer];
//    sublayer.frame = self.coverView.bounds;
//    sublayer.backgroundColor = [UIColor blueColor].CGColor;
//    sublayer.shadowOffset = CGSizeMake(0, 3);
//    sublayer.shadowRadius = 15.0;
//    sublayer.shadowColor = [UIColor blackColor].CGColor;
//    sublayer.shadowOpacity = 0.8;
//    sublayer.borderColor = [UIColor blackColor].CGColor;
//    sublayer.borderWidth = 2.0;
//    sublayer.cornerRadius = 10.0;
    //[self.view.layer addSublayer:sublayer];
    
    self.coverView.layer.mask = maskLayer;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (appDelegate.player != nil) {
        
        [self didChangeUISpotify2];
        [self updateUI];
        
        if (appDelegate.player.playbackState.isPlaying)
        {
            self.progressSlider.value = self.player.playbackState.position/appDelegate.player.metadata.currentTrack.duration;
        }
        else {
            self.progressSlider.value = appDelegate.playerDuration/appDelegate.player.metadata.currentTrack.duration;
            
        }
    }
    else {
        //[self.navigationController popViewControllerAnimated:YES];
    }
    
    appDelegate.spotifyDelegate = self;
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    appDelegate.spotifyDelegate = nil;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


- (BOOL)prefersStatusBarHidden {
    return YES;
}

-(void)configureStyles {
    [super configureStyles];
    
//    if (appDelegate.player == nil) {
//        [super navigationType01X:[UIImage imageNamed:@"btn_back"] centerText:@"Spotify"];
//    }
//    else {
//        [super navigationType01X:[UIImage imageNamed:@"btn_back"] centerText:appDelegate.player.metadata.currentTrack.name];
//    }
}

-(void)configure {
    [super configure];
    
    UIView *n = [super navigationView];
    n.hidden = YES;
}

#pragma mark - Action
-(IBAction)onNaviButtonClick:(id)sender {
    UIButton *btn = (UIButton*)sender;
    NSLog(@"onNaviButtonClick");
    //[super onNaviButtonClick:sender];
    
    if (btn.tag == 1100) {
        //[self.navigationController popViewControllerAnimated:YES];
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}

#pragma mark - Actions

-(IBAction)rewind:(id)sender {
    [appDelegate.player skipPrevious:nil];
    [self didChangeUISpotify:YES];
}

-(IBAction)playPause:(id)sender {
    
    //UIButton *btn = (UIButton*)sender;

    if (appDelegate.player.playbackState.isPlaying)
    {
        [appDelegate.player setIsPlaying:NO callback:nil];
        [self didChangeUISpotify:NO];
    }
    else{
        [appDelegate.player setIsPlaying:YES callback:nil];
        [self didChangeUISpotify:YES];
    }
    
//    [appDelegate.player setIsPlaying:!appDelegate.player.playbackState.isPlaying callback:nil];
}

-(IBAction)fastForward:(id)sender {
    [appDelegate.player skipNext:nil];
    [self didChangeUISpotify:YES];
}

- (IBAction)seekValueChanged:(id)sender {
    appDelegate.isChangingProgress = NO;
    NSUInteger dest = appDelegate.player.metadata.currentTrack.duration * self.progressSlider.value;
    [appDelegate.player seekTo:dest callback:nil];
}

- (IBAction)logoutClicked:(id)sender {
    if (appDelegate.player) {
        [appDelegate.player logout];
    } else {
        //[self.navigationController popViewControllerAnimated:YES];
    }
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)proggressTouchDown:(id)sender {
    appDelegate.isChangingProgress = YES;
}

#pragma mark - Logic


- (UIImage *)applyBlurOnImage: (UIImage *)imageToBlur
                   withRadius: (CGFloat)blurRadius {
    
    CIImage *originalImage = [CIImage imageWithCGImage: imageToBlur.CGImage];
    CIFilter *filter = [CIFilter filterWithName: @"CIGaussianBlur"
                                  keysAndValues: kCIInputImageKey, originalImage,
                        @"inputRadius", @(blurRadius), nil];
    
    CIImage *outputImage = filter.outputImage;
    CIContext *context = [CIContext contextWithOptions:nil];
    
    CGImageRef outImage = [context createCGImage: outputImage
                                        fromRect: [outputImage extent]];
    
    UIImage *ret = [UIImage imageWithCGImage: outImage];
    
    CGImageRelease(outImage);
    
    return ret;
}

-(void)updateUI {
    SPTAuth *auth = [SPTAuth defaultInstance];
    
    if (appDelegate.player.metadata == nil || appDelegate.player.metadata.currentTrack == nil) {
        self.coverView.image = nil;
        //self.coverView2.image = nil;
        return;
    }
    
    [self.spinner startAnimating];
    
    self.nextButton.enabled = appDelegate.player.metadata.nextTrack != nil;
    self.prevButton.enabled = appDelegate.player.metadata.prevTrack != nil;
    self.trackTitle.text = appDelegate.player.metadata.currentTrack.name;
    //[super navigationType01X:nil centerText:appDelegate.player.metadata.currentTrack.name];
    self.artistTitle.text = appDelegate.player.metadata.currentTrack.artistName;
    self.playbackSourceTitle.text = appDelegate.player.metadata.currentTrack.playbackSourceName;
    
    [SPTTrack trackWithURI: [NSURL URLWithString:appDelegate.player.metadata.currentTrack.uri]
               accessToken:auth.session.accessToken
                    market:nil
                  callback:^(NSError *error, SPTTrack *track) {
                      
                      NSURL *imageURL = track.album.largestCover.imageURL;
                      if (imageURL == nil) {
                          NSLog(@"Album %@ doesn't have any images!", track.album);
                          self.coverView.image = nil;
                          //self.coverView2.image = nil;
                          return;
                      }
                      
                      //[self didChangeUISpotify2];
                      
                      // Pop over to a background queue to load the image over the network.
                      dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                          NSError *error = nil;
                          UIImage *image = nil;
                          NSData *imageData = [NSData dataWithContentsOfURL:imageURL options:0 error:&error];
                          
                          if (imageData != nil) {
                              image = [UIImage imageWithData:imageData];
                          }
                          
                          
                          // …and back to the main queue to display the image.
                          dispatch_async(dispatch_get_main_queue(), ^{
                              [self.spinner stopAnimating];
                              self.coverView.image = image;
                              if (image == nil) {
                                  NSLog(@"Couldn't load cover image with error: %@", error);
                                  return;
                              }
                          });
                          
                          // Also generate a blurry version for the background
//                          UIImage *blurred = [self applyBlurOnImage:image withRadius:10.0f];
                          dispatch_async(dispatch_get_main_queue(), ^{
                              //self.coverView2.image = blurred;
                          });
                      });
                      
                  }];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self handleNewSession];
}

-(void)handleNewSession {
    
    NSError *error = [appDelegate handleNewSession];
    if (error != nil) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error init" message:[error description] preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:alert animated:YES completion:nil];
        [self closeSession];
    }
}

- (void)closeSession {
    NSError *error = nil;
    if (![appDelegate.player stopWithError:&error]) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error deinit" message:[error description] preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:alert animated:YES completion:nil];
    }
    [SPTAuth defaultInstance].session = nil;
    appDelegate.player = nil;
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)audioStreaming:(NSNotification *)notification {
    
    NSError *error = notification.userInfo[@"error"];
    NSLog(@"didReceiveError: %zd %@", error.code, error.localizedDescription);
    
    if (error.code == SPErrorNeedsPremium) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Premium account required" message:@"Premium account is required to showcase application functionality. Please login using premium account." preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction* action){
            
            [self closeSession];
            //[[NSNotificationCenter defaultCenter] postNotificationName:@"NotificationSpotifyCloseSessionReceived" object:nil userInfo:nil];
            
        }]];
        [self presentViewController:alert animated:YES completion:nil];
        
    }
}

#pragma mark - Spotify AppDelegate
- (void)didChangePositionSpotify:(NSTimeInterval)position {
    if (appDelegate.isChangingProgress) {
        return;
    }
    self.progressSlider.value = position/appDelegate.player.metadata.currentTrack.duration;
}

- (void)didChangeUISpotify:(BOOL)isPlay {
    
    // pause상태, play 상태 변한다.
    UIButton *btn = [self.view viewWithTag:10];
    if (isPlay)
    {
        [btn setImage:[UIImage imageNamed:@"spotify_pause"] forState:UIControlStateNormal];
    }
    else{
        [btn setImage:[UIImage imageNamed:@"spotify_play"] forState:UIControlStateNormal];
    }
}

- (void)didChangeUISpotify2 {
    
    // pause상태, play 상태 변한다.
    UIButton *btn = [self.view viewWithTag:10];
    if (appDelegate.player.playbackState.isPlaying)
    {
        [btn setImage:[UIImage imageNamed:@"spotify_pause"] forState:UIControlStateNormal];
    }
    else{
        [btn setImage:[UIImage imageNamed:@"spotify_play"] forState:UIControlStateNormal];
    }
}


@end
