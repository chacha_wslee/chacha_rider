//
//  MenuViewController.m
//  SlideMenu
//

//

#import "TXVehicleVC.h"
#import "TXVehicleCell.h"
#import "RTLabel.h"
#import "LGAlertView.h"
#import "TXBecomeVehicleVC.h"
#import "TXModifyVehicleVC.h"

@import IQKeyboardManager;

@interface TXVehicleVC() {
    NSArray *items;
    NSArray *_msgList;
}

@end

@implementation TXVehicleVC {
    //NSMutableDictionary *propertyMap;
}
@synthesize cellIdentifier;



-(void)viewDidLoad {
    
    [super viewDidLoad];
    
    [[IQKeyboardManager sharedManager] setEnable:NO];
    
    NSDictionary *user;
    if (appDelegate.isDriverMode) {
        user = [appDelegate.dicDriver objectForKey:@"provider"];
    }
    else {
        user = [appDelegate.dicRider objectForKey:@"user"];
    }
    
    //NSString* _name = [Utils nameToString:[user objectForKey:@"name"]];
    
    [super configureBottomList];
    UIView *n = [super navigationView];
    NSInteger _y = n.frame.origin.y + n.frame.size.height;
    
    if (appDelegate.isBecomeMode) {
        self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, _y, self.view.bounds.size.width, self.view.bounds.size.height-_y)
                                                      style:UITableViewStylePlain];
    }
    else {
        self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, _y, self.view.bounds.size.width, self.view.bounds.size.height-_y-kBottomButtonHeight)
                                                      style:UITableViewStylePlain];
    }
                      
    self.tableView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:self.tableView];
    
    UINib *nib = [UINib nibWithNibName:@"TXVehicleCell" bundle:nil];
    self.cellIdentifier = @"TXVehicleCell";
    [self.tableView registerNib:nib forCellReuseIdentifier:self.cellIdentifier];
    
    self.tableView.separatorColor = [UIColor colorWithRed:150/255.0f green:161/255.0f blue:177/255.0f alpha:1.0f];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.opaque = NO;
    self.tableView.backgroundColor = [UIColor clearColor];
    
    UILabel *label = (UILabel*)[[super navigationView] viewWithTag:1111];
    label.tag = 2001;
    label.text = LocalizedStr(@"Menu.Driver.Vehicle.Header.text1");
    
//    self.tableView.tableHeaderView = ({
//        
//        UIView *view = [Utils addLabelView:self.view underView:nil dim:0];
//        
//        UILabel *label = [view viewWithTag:11];
//        label.text = LocalizedStr(@"Menu.Driver.Vehicle.Header.text1");
//        label.tag = 2001;
//        //[label sizeToFit];
//        view.autoresizingMask = UIViewAutoresizingFlexibleHeight;
//        
//        view;
//    });
    
//    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, kBasicHeight + kBasicMargin*2)];
    self.tableView.tableFooterView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    
    if (!appDelegate.isBecomeMode) {
#ifndef _CHACHA
        UIButton *btn = [Utils addBottomButton:LocalizedStr(@"Menu.Driver.Vehicle.BTN.add") target:self selector:@selector(addButtonPressed:) color:YES];
        [self.view addSubview:btn];
#endif
//    [self.tableView.tableFooterView addSubview:({
//        UIView *view = [Utils addButtonBasic:self.view underView:nil dim:kBasicMargin];
//        
//        view.backgroundColor = UIColorLabelBG;
//        UIButton *button = [view viewWithTag:11];
//        button.tag = 999;
//        [button setTitle:LocalizedStr(@"Menu.Driver.Vehicle.BTN.add") forState:UIControlStateNormal];
//        [button addTarget:self action:@selector(addButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
//        view.autoresizingMask = UIViewAutoresizingFlexibleHeight;
//        
//        view;
//    })];
    }
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    self.tableView.scrollsToTop = YES;
    //[self.tableView setSeparatorInset:UIEdgeInsetsZero];
    
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self registerEventListeners];
    
    //propertyMap = [[NSMutableDictionary alloc] init];
    
    // 3. call api
    [self onPP400];
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self removeEventListeners];
}

#pragma mark - Configure

-(void)configureStyles {
    [super configureStyles];
    
    if (appDelegate.isDriverMode) {
        [super navigationType01X:[UIImage imageNamed:@"btn_back"] centerText:LocalizedStr(@"Menu.Driver.Vehicle.Detail.title")];
    }
    else {
        [super navigationType01X:[UIImage imageNamed:@"btn_back"] centerText:LocalizedStr(@"Menu.Driver.Vehicle.Detail.title")];
    }
}

-(void)configure {
    [super configure];
    
    
    // 2. init event
    _msgList = @[
                 @[@"PP400",@""], // list
                 @[@"PP430",@""], // delete
                 ];
}

-(void) registerEventListeners {
    for (NSArray *msg in _msgList) {
        [self->model addEventListener:self forEvent:[msg objectAtIndex:0] eventParams:nil];
    }
}

-(void) removeEventListeners {
    for (NSArray *msg in _msgList) {
        [self->model removeEventListener:self forEvent:[msg objectAtIndex:0]];
    }
}

#pragma mark - Action
-(IBAction)onNaviButtonClick:(id)sender {
    UIButton *btn = (UIButton*)sender;
    NSLog(@"onNaviButtonClick");
    //[super onNaviButtonClick:sender];
    
    if (btn.tag == 1100) {
        [self removeEventListeners];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark - IBAction
-(IBAction)addButtonPressed:(id)sender {
    UIButton *btn = (UIButton*)sender;
    
    if (btn.tag == 999) {
        [self removeEventListeners];
        
        appDelegate.becomePseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.PSEQ];
        appDelegate.becomeVseq = @"";
        
        TXBecomeVehicleVC *vc = [[TXBecomeVehicleVC alloc] initWithNibName:@"TXBecomeVehicleVC" bundle:[NSBundle mainBundle] isnew:YES];
        [self.navigationController pushViewController:vc animated:YES];
    }
}

-(IBAction)modifyPressed:(id)sender {
    UIButton *btn = (UIButton*)sender;
    
    appDelegate.becomePseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.PSEQ];
    appDelegate.becomeVseq = [NSString stringWithFormat:@"%ld",(long)btn.tag];
    
    TXModifyVehicleVC *vc = [[TXModifyVehicleVC alloc] initWithNibName:@"TXModifyVehicleVC" bundle:[NSBundle mainBundle]];

    for(NSDictionary *dic in self->items) {
        if ([dic[@"vseq"] isEqualToString:appDelegate.becomeVseq]) {
            
            vc.items = dic;
            //vc.title = [Utils nullToString:dic[@"vehiclemodel"]];
            break;
            
        }
    }
    
    [self.navigationController pushViewController:vc animated:YES];
}

-(IBAction)deletePressed:(id)sender {
    UIButton *btn = (UIButton*)sender;
    
    LGAlertView *alertView = [[LGAlertView alloc] initWithTitle:@""
                                message:LocalizedStr(@"Menu.Driver.Vehicle.BTN.delete.text")
                                  style:LGAlertViewStyleAlert
                           buttonTitles:@[LocalizedStr(@"Button.OK")]
                      cancelButtonTitle:LocalizedStr(@"Button.CANCEL")
                 destructiveButtonTitle:nil
                          actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                              NSLog(@"actionHandler, %@, %lu", title, (long unsigned)index);
                              
                              [self showBusyIndicator:@"deletting ... "];
                              [self onPP430:[NSString stringWithFormat:@"%d",(int)btn.tag]];
                          }
                          cancelHandler:^(LGAlertView *alertView) {
                          }
                     destructiveHandler:^(LGAlertView *alertView) {
                     }];
    [Utils initAlertButtonColor:alertView];
    [alertView showAnimated:YES completionHandler:nil];
    
}

#pragma mark - UITableView Delegate & Datasrouce -
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [tableView setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [tableView setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
    
    UIView *seperatorView = [[UIView alloc] initWithFrame:CGRectMake(10, cell.frame.size.height - 1, cell.frame.size.width-10*2, 1)];
    seperatorView.backgroundColor = UIColorTableSeperator;
    [cell addSubview:seperatorView];
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 82;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self->items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TXVehicleCell *cell = [tableView dequeueReusableCellWithIdentifier:self.cellIdentifier];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    NSDictionary *dic = self->items[indexPath.row];
    [cell bindData:dic];
    
//    if ([dic[@"mode"] intValue] == 1 && [dic[@"state"] intValue] == 1) {
//        ((UIImageView *)[cell.contentView viewWithTag:900]).image = [UIImage imageNamed:@"form_radio_chk"];
//        [[[TXApp instance] getSettings] setFDKeychain:SettingsConst.CryptoKeys.CSEQ value:[dic valueForKey:@"cseq"]];
//    }
//    else {
//        ((UIImageView *)[cell.contentView viewWithTag:900]).image = [UIImage imageNamed:@"form_radio_normal"];
//    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - onEvent
-(void)onPP400 {
    NSString *pseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.PSEQ];
    
    NSDictionary *_propertyMap = @{
                                   @"pseq" : pseq
                                   };
    
    [self showBusyIndicator:@"Loading info ... "];
    [self->model requsetAPI:API_FUNC(NSStringFromSelector(_cmd)) property:_propertyMap];
}

-(void)onPP430:(NSString*)vseq {
    
    NSString *pseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.PSEQ];
    
    NSDictionary *propertyMap = @{
                                  @"pseq" : pseq,
                                  @"vseq" : vseq,
                                  };
    
    [self showBusyIndicator:@"Delete Vehicle ... "];
    [self->model requsetAPI:API_FUNC(NSStringFromSelector(_cmd)) property:propertyMap];
}

-(void)onEvent:(TXEvent *)event eventParams:(id)subscriptionParams {
    
    TXResponseDescriptor * descriptor = [event getEventProperty:TXEvents.Params.DESCRIPTOR];
    TXRequestObj *request = [event getEventProperty:TXEvents.Params.REQUEST];
    
    [self hideBusyIndicator];
    if([event.name isEqualToString:@"PP400"]) {
        
        if(descriptor.success == true) {
            NSString     *response = [[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding];
            NSDictionary *result   = getJSONObj(response);
            [self hideBusyIndicator];
            
            NSArray *vehicle              = [result valueForKey:@"provider_vehicles"];
            if ([Utils isArray:vehicle]) {
                
                NSMutableArray *vlists = [[NSMutableArray alloc] init];
                for(NSDictionary *dic in vehicle) {
                    if ([dic[@"state"] intValue] >= 0) {
                        [vlists addObject:dic];
                    }
                }
                self->items = (NSArray*)vlists;
            }
            [self.tableView reloadData];
        }
        else {
            [self hideBusyIndicator];
            [self alertError:@"Error" message:descriptor.error];
        }
    }
    else if([event.name isEqualToString:@"PP430"]) {
        
        if(descriptor.success == true) {
//            NSString     *response = [[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding];
//            NSDictionary *result   = getJSONObj(response);
            [self hideBusyIndicator];
            
            [self onPP400];
            
//            NSArray *vehicle              = [result valueForKey:@"provider_vehicles"];
//            if ([Utils isArray:vehicle]) {
//                
//                NSMutableArray *vlists = [[NSMutableArray alloc] init];
//                for(NSDictionary *dic in vehicle) {
//                    if ([dic[@"state"] intValue] >= 0) {
//                        [vlists addObject:dic];
//                    }
//                }
//                self->items = (NSArray*)vlists;
//            }
//            [self.tableView reloadData];
        }
        else {
            [self hideBusyIndicator];
            [self alertError:@"Error" message:descriptor.error];
        }
    }
}


@end
