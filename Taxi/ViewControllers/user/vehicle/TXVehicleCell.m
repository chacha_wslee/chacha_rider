//
//  TXPaymentCell.m
//  TXPaymentCell
//

//
#import "TXAppDelegate.h"
#import "TXVehicleCell.h"
#import "utils.h"
#import "utilities.h"

@implementation TXVehicleCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
    }
    return self;
}

- (void)bindData:(NSDictionary*)dic
{
    TXAppDelegate *appDelegate = (TXAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    // color
    //---------------------------------------------------------------------------------------------------------
    self.lb1.textColor = HEXCOLOR(0x333333FF);
    self.lb2.textColor = HEXCOLOR(0x999999FF);
    self.lb3.textColor = UIColorDefault;
    self.btn1.tintColor = UIColorRedColor;
    self.btn2.tintColor = [UIColor blackColor];
    
    // text
    //---------------------------------------------------------------------------------------------------------
    self.lb1.text = [Utils nullToString:dic[@"vehiclemodel"]];
    self.lb2.text = [Utils nullToString:dic[@"vehicleno"]];
    if ([dic[@"state"] intValue]==1) {
        self.lb3.text = @"";
        self.btn1.hidden = YES;
        if (appDelegate.isDriverMode && appDelegate.pstate == 200) {
            self.btn2.enabled = YES;
        }
        else {
            self.btn2.enabled = NO;
        }
    }
    else {
        self.lb3.text = LocalizedStr(@"Menu.Driver.Vehicle.List.text1");
        [self.btn1 setTitle:LocalizedStr(@"Menu.Driver.Vehicle.List.text2") forState:UIControlStateNormal];
    }
    
    [self.btn2 setTitle:LocalizedStr(@"Menu.Driver.Vehicle.List.text3") forState:UIControlStateNormal];
    
    self.btn1.tag = [dic[@"vseq"] integerValue];
    self.btn2.tag = [dic[@"vseq"] integerValue];
    
    if (appDelegate.isDriverMode && appDelegate.pstate == 200) {
        self.btn1.enabled = YES;
    }
    else {
        self.btn1.enabled = NO;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

-(void)stateChanged
{
    
}

@end
