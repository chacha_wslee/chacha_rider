//
//  TXProfileVC.m
//  Taxi
//

//

//#import "TXBecomeFinishVC.h"
#import "TXAboutVC.h"
#import "TXModifyVehicleVC.h"
#import "LGAlertView.h"
#import "TOCropViewController.h"
//#import "EXPhotoViewer.h"
#import "TXVehicleVC.h"
#import "utilities.h"

#import "Validator.h"
#import "WToast.h"
//#import "CRToast.h"

#define MaxProcessCnt 1
#define NSProcessPct [NSString stringWithFormat:@"%d%@",(int)(processPct*100/MaxProcessCnt),LocalizedStr(@"Menu.BecomeADriver.Header.text2")]

@interface TXModifyVehicleVC () <UITextFieldDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIActionSheetDelegate, TOCropViewControllerDelegate, UIViewControllerTransitioningDelegate, ValidatorDelegate>{
    int userId;
    NSArray *_msgList;
    UILabel *lbProcessInfo;
    int processPct;
    
    NSArray *dataArray;
    
    BOOL isUpdateImage1;
    BOOL isUpdateImage2;
    BOOL isUpdateImage3;
    BOOL isUpdateImage4;
    
    NSInteger imageLoadingMax;
    NSInteger imageLoadingCnt;
    
    id observer1;
    id observer2;
    id observer3;
}
@property (nonatomic, assign) TOCropViewCroppingStyle croppingStyle; //The cropping style
@property (strong, nonatomic) LGAlertView *securityAlertView;
@end

@implementation TXModifyVehicleVC {
    NSMutableDictionary *propertyMap;
    NSMutableDictionary *propertyMap2;
    int _alertIdx;
}


-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    
    if(self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        self->_alertIdx = 1;
    }
    
    return self;
}

-(void)viewDidLoad {
    [super viewDidLoad];
    
    [[IQKeyboardManager sharedManager] setEnable:NO];
    
    observer1 = [[NSNotificationCenter defaultCenter] addObserverForName:@"NotificationPhotoAlbum"
                                                                  object:nil
                                                                   queue:[NSOperationQueue mainQueue]
                                                              usingBlock:^(NSNotification *notification) {
                                                                  
                                                                  [self openPhotoAlbum];
                                                                  
                                                              }];
    observer2 = [[NSNotificationCenter defaultCenter] addObserverForName:@"NotificationShowCamera"
                                                                  object:nil
                                                                   queue:[NSOperationQueue mainQueue]
                                                              usingBlock:^(NSNotification *notification) {
                                                                  
                                                                  [self showCamera];
                                                                  
                                                              }];
//    observer3 = [[NSNotificationCenter defaultCenter] addObserverForName:@"NotificationEditPhoto"
//                                                                  object:nil
//                                                                   queue:[NSOperationQueue mainQueue]
//                                                              usingBlock:^(NSNotification *notification) {
//                                                                  
//                                                                  NSLog(@"notification:%@",notification);
//                                                                  [self editPhoto];
//                                                                  
//                                                              }];
    
    isUpdateImage1 = NO;
    isUpdateImage2 = NO;
    isUpdateImage3 = NO;
    isUpdateImage4 = NO;
    imageLoadingMax = 0;
    imageLoadingCnt = 0;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self checkProcess:@""];
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    //[self removeEventListeners];
}

#pragma mark - Configure

-(void)configureStyles {
    [super configureStyles];
    
//    if (appDelegate.isDriverMode) {
//        [super navigationType01X:[UIImage imageNamed:@"btn_back"] centerText:LocalizedStr(@"Menu.Account.title")];
//    }
//    else {
//        [super navigationType01X:[UIImage imageNamed:@"btn_back"] centerText:LocalizedStr(@"Menu.Account.title")];
//    }
//
    if (![[Utils nullToString:self.items[@"vehiclemodel"]] isEqualToString:@""]) {
        //[super navigationType01X:[UIImage imageNamed:@"btn_back"] centerText:self.items[@"vehiclemodel"]];
        //[super navigationType01X:[UIImage imageNamed:@"btn_back"] centerText:self.items[@"vehiclemodel"]];
        [super navigationType01X:[UIImage imageNamed:@"btn_back"] centerText:LocalizedStr(@"Menu.BecomeADriver.Vehicle.title")];
    }
    else {
        //[super navigationType01X:[UIImage imageNamed:@"btn_back"] centerText:LocalizedStr(@"Menu.BecomeADriver.Vehicle.title")];
        if (appDelegate.becomeVseq) {
            [super navigationType01X:[UIImage imageNamed:@"btn_back"] centerText:LocalizedStr(@"Menu.BecomeADriver.Vehicle.title")];
        }
        else {
            [super navigationType01X:[UIImage imageNamed:@"btn_back"] centerText:LocalizedStr(@"Menu.BecomeADriver.Vehicle.title")];
        }
    }
    
    //[super navigationType11X:[UIImage imageNamed:@"btn_back"] centerText:LocalizedStr(@"Menu.BecomeADriver.Vehicle.title") rightText:LocalizedStr(@"Intro.Next.title")];
    [self nextButtonOn:NO];
}

- (void)nextButtonOn:(BOOL)on
{
    //return;
    
    UIButton *btn = [super.view viewWithTag:999];
    btn.enabled = on;
    if (on)
        [btn setTitleColor:UIColorButtonText forState:UIControlStateNormal];
    else
        [btn setTitleColor:UIColorBasicTextOff forState:UIControlStateNormal];
}

-(void)configure {
    [super configure];
    [self configureConfig];
    self->userId = [[[[self->model getApp] getSettings] getUserId] intValue];
    
    [super configureBottomList];
    UIView *n = [super navigationView];
    NSInteger _y = n.frame.origin.y + n.frame.size.height;
    
    //------------------------------------------------------------------------------------------------------------------------
    propertyMap = [[NSMutableDictionary alloc] init];
    propertyMap2 = [[NSMutableDictionary alloc] init];
    //------------------------------------------------------------------------------------------------------------------------
//    UIView *labelView = [Utils addDoubleLabelView:self.view underView:nil dim:0];
//    CGRect rect = labelView.frame;
//    rect.origin.y = kTopHeight + kNaviHeight;
//    labelView.frame = rect;
//    UILabel *label = [labelView viewWithTag:11];
//    label.text = LocalizedStr(@"Menu.BecomeADriver.Header.text1");
    
    UIView *bgview;
    if (![[Utils nullToString:self.items[@"vehiclemodel"]] isEqualToString:@""]) {
        bgview = [Utils addViewButtonText:LocalizedStr(@"Intro.Save.title") target:self selector:@selector(onNaviButtonClick:) color:YES];
    }
    else {
        if (appDelegate.becomeVseq) {
            bgview = [Utils addViewButtonText:LocalizedStr(@"Intro.Save.title") target:self selector:@selector(onNaviButtonClick:) color:YES];
        }
        else {
            bgview = [Utils addViewButtonText:LocalizedStr(@"Intro.Next.title") target:self selector:@selector(onNaviButtonClick:) color:YES];
        }
    }
    
    [self.view addSubview:bgview];
    
    lbProcessInfo = (UILabel*)[bgview viewWithTag:998];
    processPct = 0;
    lbProcessInfo.text = NSProcessPct;
    //[self.view addSubview:labelView];
    
    //------------------------------------------------------------------------------------------------------------------------
    //((UILabel *)[self.vbody viewWithTag:11011]).text = LocalizedStr(@"Menu.BecomeADriver.Vehicle.List.text1");
    ((UILabel *)[self.vbody viewWithTag:11111]).text = LocalizedStr(@"Menu.BecomeADriver.Vehicle.List.text2");
    
    [_btnForm setTitleColor:UIColorButtonText forState:UIControlStateNormal];
    _btnForm.backgroundColor = UIColorDefault;
    _btnForm.layer.cornerRadius = 3;
    [_btnForm setTitle:LocalizedStr(@"Menu.BecomeADriver.Vehicle.BTN.text") forState:UIControlStateNormal];
    [_btnForm addTarget:self action:@selector(inspectionButtonPressed:) forControlEvents:UIControlEventTouchUpInside];

    //------------------------------------------------------------------------------------------------------------------------
    self.myScrollView.backgroundColor = [UIColor whiteColor];
    self.vbody.backgroundColor = UIColorLabelBG;
    self.vbody.frame = CGRectMake(self.vbody.frame.origin.x, self.vbody.frame.origin.y, self.view.frame.size.width, self.vbody.frame.size.height);
    
    self.myScrollView.frame = CGRectMake(0,
                                         _y,
                                         self.view.frame.size.width,
                                         self.view.frame.size.height - _y - kBottomButtonHeight);
    
    self.myScrollView.contentSize = CGSizeMake(self.vbody.frame.size.width, self.vbody.frame.size.height);
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0, 0.0, 0, 0.0);
    self.myScrollView.contentInset = contentInsets;
    self.myScrollView.scrollIndicatorInsets = contentInsets;
    self.myScrollView.scrollsToTop = YES;
    [self.myScrollView addSubview:self.vbody];
    //------------------------------------------------------------------------------------------------------------------------
    // 2. init event
    _msgList = @[
                    @[@"PP410",@""], // modify
                    @[@"PP629",@""], // modify
                    
//                    @[@"PP411",@""], // modify
//                    @[@"PP412",@""], // modify
//                    @[@"PP413",@""], // modify
//                    @[@"PP414",@""], // modify
                    
                    @[@"PP420",@""], // save
                    
                    @[@"PP400",@""], // loading
                    @[@"PP606",@""], // loading
                    
//                    @[@"PP401",@""], // loading
//                    @[@"PP402",@""], // loading
//                    @[@"PP403",@""], // loading
//                    @[@"PP404",@""], // loading
                    ];
    
    [self registerEventListeners];
    
    if (appDelegate.isDriverMode) {
        [self configureDriver];
    }
    else {
        [self configureRider];
    }
}

-(void) configureConfig {

    
}

-(void) configureDriver {
    // 1. init var
    
    if (![appDelegate.becomePseq isEqualToString:@""] && ![appDelegate.becomeVseq isEqualToString:@""]) {
        // 조회
        [self onPP400];
    }
}

-(void) configureRider {
    // 1. init var
    if (![appDelegate.becomePseq isEqualToString:@""] && ![appDelegate.becomeVseq isEqualToString:@""]) {
        // 조회
        [self onPP400];
    }
}

-(void) registerEventListeners {
    for (NSArray *msg in _msgList) {
        [self->model addEventListener:self forEvent:[msg objectAtIndex:0] eventParams:nil];
    }
}

-(void) removeEventListeners {
    for (NSArray *msg in _msgList) {
        [self->model removeEventListener:self forEvent:[msg objectAtIndex:0]];
    }
    [[NSNotificationCenter defaultCenter] removeObserver:observer1];
    observer1 = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:observer2];
    observer2 = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:observer3];
    observer3 = nil;
    
//    [[NSNotificationCenter defaultCenter] removeObserver:self
//                                                    name:@"NotificationPhotoAlbum"
//                                                  object:nil];
//    [[NSNotificationCenter defaultCenter] removeObserver:self
//                                                    name:@"NotificationShowCamera"
//                                                  object:nil];
//    [[NSNotificationCenter defaultCenter] removeObserver:self
//                                                    name:@"NotificationEditPhoto"
//                                                  object:nil];
}

-(void) checkProcess:(NSString*)state {
    processPct = (int)[propertyMap count];
    if((int)(processPct*100/MaxProcessCnt) != 100) {
        lbProcessInfo.text = NSProcessPct;
    }
    else {
        if ([state isEqualToString:@"1"]) {
            lbProcessInfo.text = NSProcessPct;
        }
        else if ([state isEqualToString:@""]) {
        }
        else {
            lbProcessInfo.text = LocalizedStr(@"Menu.BecomeADriver.Vehicle.Header.text2");
        }
    }
    
    //if (processPct == MaxProcessCnt && _btnAgree.tag == 1) {
    if (processPct == MaxProcessCnt) {
        [self nextButtonOn:YES];
    }
    else {
        [self nextButtonOn:NO];
    }
    
}

-(void)updateTrimString:(NSString*)str key:(NSString*)key
{
    NSString *trimmedString = [Utils trimStringOnly:str];
    
    if ([trimmedString isEqualToString:@""]) {
        [propertyMap removeObjectForKey:key];
    }
    else {
        [propertyMap setValue:str forKey:key];
    }
}

-(void)updateLabel:(NSInteger)tag flag:(BOOL)flag
{
    if (flag) {
        ((UILabel *)[self.vbody viewWithTag:tag]).textColor = [UIColor blackColor];
    }
    else {
        ((UILabel *)[self.vbody viewWithTag:tag]).textColor = UIColorRedColor;
        lbProcessInfo.textColor = UIColorRedColor;
    }
}

#pragma mark - Action
-(IBAction)onNaviButtonClick:(id)sender {
    UIButton *btn = (UIButton*)sender;
    NSLog(@"onNaviButtonClick");
    //[super onNaviButtonClick:sender];
    
    if (btn.tag == 1100 || btn.tag == 1101) {
        [self removeEventListeners];
        [self.navigationController popViewControllerAnimated:YES];
    }
    else if(btn.tag == 1300 || btn.tag == 999) {
        
        if (![appDelegate.becomeVseq isEqualToString:@""]) {
            [self onPP410];
        }
        else {
//            [self onPP420];
        }
        
    }
}


#pragma mark - IBAction

-(IBAction)inspectionButtonPressed:(id)sender {
    //UIButton *btn = (UIButton*)sender;
#ifndef _CHACHA
    NSCharacterSet *set = [NSCharacterSet URLQueryAllowedCharacterSet];
    NSString* content = [INSPECTION_URL stringByAddingPercentEncodingWithAllowedCharacters:set];
    [[UIApplication sharedApplication] openURL: [NSURL URLWithString:content]];
    //[[UIApplication sharedApplication] openURL: [NSURL URLWithString:INSPECTION_URL]];
#endif
}

-(IBAction)onPhoto:(id)sender {
    
    self->_alertIdx = (int)((UIButton*)sender).tag;
    NSLog(@"_alertIdx:%d",self->_alertIdx);
    NSArray *items = @[LocalizedStr(@"Menu.Driver.Profile.Image.album")];
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        items = @[LocalizedStr(@"Menu.Driver.Profile.Image.album"), LocalizedStr(@"Menu.Driver.Profile.Image.camera"), LocalizedStr(@"Menu.Driver.Profile.Image.edit")];
    }
    
    LGAlertView *alertView = [[LGAlertView alloc] initWithTitle:nil
                                                        message:LocalizedStr(@"Menu.Driver.Profile.Image.title2")
                                                          style:LGAlertViewStyleActionSheet
                                                   buttonTitles:items
                                              cancelButtonTitle:LocalizedStr(@"Button.CANCEL")
                                         destructiveButtonTitle:nil
                                                  actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                                                      NSLog(@"actionHandler, %@, %lu", title, (long unsigned)index);
                                                      
                                                      if (index == 0) {
                                                          [self openPhotoAlbum];
                                                      }
                                                      else if (index == 1) {
                                                          [self showCamera];
                                                      }
                                                      else if (index == 2) {
                                                          UIImageView *imageView;
                                                          if (self->_alertIdx == 11012 || self->_alertIdx == 11013) {
                                                              imageView = (UIImageView *)[self.vbody viewWithTag:11015];
                                                          }
                                                          else if (self->_alertIdx == 11212 || self->_alertIdx == 11213) {
                                                              imageView = (UIImageView *)[self.vbody viewWithTag:11215];
                                                          }
                                                          else if (self->_alertIdx == 11312 || self->_alertIdx == 11313) {
                                                              imageView = (UIImageView *)[self.vbody viewWithTag:11315];
                                                          }
                                                          else if (self->_alertIdx == 11412 || self->_alertIdx == 11413) {
                                                              imageView = (UIImageView *)[self.vbody viewWithTag:11415];
                                                          }
                                                          
                                                          if (imageView.image) {
                                                              TOCropViewController *cropController = [[TOCropViewController alloc] initWithCroppingStyle:self.croppingStyle image:imageView.image];
                                                              cropController.delegate = self;
                                                              [self presentViewController:cropController animated:YES completion:nil];
                                                          }
                                                      }
                                                  }
                                                  cancelHandler:^(LGAlertView *alertView) {
                                                      NSLog(@"cancelHandler");
                                                  }
                                             destructiveHandler:^(LGAlertView *alertView) {
                                                 NSLog(@"destructiveHandler");
                                             }];
    [Utils initAlertButtonColor:alertView];
    [alertView showAnimated:YES completionHandler:nil];
    
}

-(IBAction)onClick:(id)sender {
    
    UIButton *btn = (UIButton*)sender;
    
    NSInteger tag = (btn.tag / 100) % 100 + 1 - 10;
    
    //NSLog(@"tag:%ld",(long)tag);

    self->_alertIdx = 1;
    [self popupInput:(int)tag];
    
}



-(void)popupInput:(int)tag
{
    NSString *__title = [NSString stringWithFormat:@"Menu.BecomeADriver.Vehicle.List.text%d",tag];
    if (tag == 12) {
        __title = [NSString stringWithFormat:@"Menu.BecomeADriver.Vehicle.List3.text1"];
    }
    else if (tag == 13) {
        __title = [NSString stringWithFormat:@"Menu.BecomeADriver.Vehicle.List3.text2"];
    }
    else if (tag == 22) {
        __title = [NSString stringWithFormat:@"Menu.BecomeADriver.Vehicle.List4.text1"];
    }
    else if (tag == 23) {
        __title = [NSString stringWithFormat:@"Menu.BecomeADriver.Vehicle.List4.text2"];
    }
    else if (tag == 32) {
        __title = [NSString stringWithFormat:@"Menu.BecomeADriver.Vehicle.List5.text1"];
    }
    else if (tag == 33) {
        __title = [NSString stringWithFormat:@"Menu.BecomeADriver.Vehicle.List5.text2"];
    }
    
    __title = LocalizedStr(__title);
    
    NSString *__subtitle = nil;
//    if (tag == 1 || tag == 2) {
//        __subtitle = [NSString stringWithFormat:@"Menu.BecomeADriver.Vehicle.List.text%d.input",tag];
//        __subtitle = LocalizedStr(__subtitle);
//    }
    
    LGAlertView *alertView = [[LGAlertView alloc] initWithTextFieldsAndTitle:__title
                                                                     message:__subtitle
                                                          numberOfTextFields:1
                                                      textFieldsSetupHandler:^(UITextField *textField, NSUInteger index)
                              {
                                  if (index == 0) {
                                      textField.placeholder = @"";
                                      textField.text = @"";
                                  }
                                  
                                  textField.tag = index;
                                  //textField.delegate = self;
                                  textField.enablesReturnKeyAutomatically = YES;
                                  textField.autocapitalizationType = NO;
                                  textField.autocorrectionType = NO;
                                  textField.clearButtonMode = UITextFieldViewModeNever;
                                  textField.textAlignment = NSTextAlignmentCenter;
                                  
                                  if (tag == 5 || tag == 13 || tag == 14 || tag == 15 ) {
                                      [textField setKeyboardType:UIKeyboardTypeNumberPad];
                                  }
                              }
                                                                buttonTitles:@[LocalizedStr(@"Button.OK")]
                                                           cancelButtonTitle:LocalizedStr(@"Button.CANCEL")
                                                      destructiveButtonTitle:nil
                                                               actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                                                                   UITextField *secondTextField = alertView.textFieldsArray[index];
                                                                   //promoCode = secondTextField.text;
                                                                   
                                                                   NSLog(@"actionHandler, %@, %lu, %@", title, (long unsigned)index, secondTextField.text);
                                                                   
                                                                   int __tag = tag * 100 + 1000 + 12 + 10000 - 100;
                                                                   
                                                                   ((UILabel *)[self.vbody viewWithTag:__tag]).text = secondTextField.text;
                                                                   
                                                                   if (tag == 2) {
                                                                       [self updateTrimString:secondTextField.text key:@"vehiclemodel"];
                                                                   }
                                                                   
                                                                   [self checkProcess:@""];
                                                               }
                                                               cancelHandler:^(LGAlertView *alertView) {
                                                                   NSLog(@"cancelHandler");
                                                                   
                                                               }
                                                          destructiveHandler:^(LGAlertView *alertView) {
                                                              NSLog(@"destructiveHandler");
                                                          }];
    //[alertView setButtonAtIndex:0 enabled:NO];
    [Utils initAlertButtonColor:alertView];
    [alertView showAnimated:YES completionHandler:nil];
    
}

#pragma mark - Send Event
-(void)onPP400 {
    
    NSDictionary *_propertyMap = @{
                                   @"pseq" : appDelegate.becomePseq,
                                   @"vseq" : appDelegate.becomeVseq
                                   };
    
    [self showBusyIndicator:@"Loading info ... "];
    
    [self->model requsetAPI:API_FUNC(NSStringFromSelector(_cmd)) property:_propertyMap];
}

-(void)onPP410 {
    
    NSDictionary *_propertyMap = @{
                                   @"pseq":appDelegate.becomePseq,
                                   @"vseq":appDelegate.becomeVseq,
                                   @"vehiclemodel":propertyMap[@"vehiclemodel"]
                                   };
     
    [self showBusyIndicator:@"Update ... "];
    [self->model requsetAPI:API_FUNC(NSStringFromSelector(_cmd)) property:_propertyMap];
}

-(void)onPP420 {
    
    NSDictionary *_propertyMap = @{
                                   @"pseq":appDelegate.becomePseq,
                                   @"vehiclemodel":propertyMap[@"vehiclemodel"]
                                   };
    
    [self showBusyIndicator:@"Create ... "];
    [self->model requsetAPI:API_FUNC(NSStringFromSelector(_cmd)) property:_propertyMap];
}


#pragma mark - onEvent
-(void)onEvent:(TXEvent *)event eventParams:(id)subscriptionParams {
    
    TXResponseDescriptor * descriptor = [event getEventProperty:TXEvents.Params.DESCRIPTOR];
    TXRequestObj *request = [event getEventProperty:TXEvents.Params.REQUEST];
    
    //[self hideBusyIndicator];
    if([event.name isEqualToString:@"PP400"]) {
        
        if(descriptor.success == true) {
            NSString     *response = [[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding];
            NSDictionary *result   = getJSONObj(response);
            
            
            NSDictionary *vehicle              = [result valueForKey:@"provider_vehicle"];
            if ([Utils isDictionary:vehicle]) {
                appDelegate.becomeVseq = vehicle[@"vseq"];

                ((UILabel *)[self.vbody viewWithTag:11112]).text = [Utils nullToString:vehicle[@"vehiclemodel"]];
                
                [self updateLabel:11111 flag:[vehicle[@"vehicle_flag"] boolValue]];
                [propertyMap setObject:vehicle[@"vehiclemodel"] forKey:@"vehiclemodel"];
                
                [self checkProcess:vehicle[@"state"]];
            }
        }
        else {
            [self alertError:@"Error" message:descriptor.error];
        }
        [self hideBusyIndicator];
    }
    else if([event.name isEqualToString:@"PP420"] || [event.name isEqualToString:@"PP410"]) {
        
        if (!isUpdateImage1 && !isUpdateImage2 && !isUpdateImage3 && !isUpdateImage4) {
            [self hideBusyIndicator];
        }
        
        if(descriptor.success == true) {
            NSString     *response = [[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding];
            NSDictionary *result   = getJSONObj(response);
            
            
            NSArray *vehicles               = [result valueForKey:@"provider_vehicles"];
            NSDictionary *vehicle           = nil;
            if([event.name isEqualToString:@"PP420"]) {
                if ([vehicles isKindOfClass:[NSArray class]] && [vehicles count]) {
                    vehicle = vehicles[0];
                }
            }
            else if([event.name isEqualToString:@"PP410"]) {
                vehicle              = [result valueForKey:@"provider_vehicle"];
            }
            
            if (vehicle != nil) {
                appDelegate.becomeVseq = vehicle[@"vseq"];
                
                // 페이지 이동
                if (appDelegate.becomeVseq) {
                    [self removeEventListeners];
                    [self.navigationController popViewControllerAnimated:YES];
                }
                else {
                    //TXBecomeFinishVC *vc = [[TXBecomeFinishVC alloc] initWithNibName:@"TXBecomeFinishVC" bundle:[NSBundle mainBundle]];
                    TXAboutVC *vc = [[TXAboutVC alloc] initWithNibName:@"TXAboutVC" bundle:[NSBundle mainBundle] step:3];
                    [self.navigationController pushViewController:vc animated:YES];
                }
                
            }
        }
        else {
            [self alertError:@"Error" message:descriptor.error];
        }
    }
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker  dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Image Picker Delegate -
//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    TOCropViewController *cropController = [[TOCropViewController alloc] initWithCroppingStyle:self.croppingStyle image:image];
    cropController.delegate = self;
    
    //If profile picture, push onto the same navigation stack
    if (self.croppingStyle == TOCropViewCroppingStyleCircular) {
        [picker pushViewController:cropController animated:YES];
    }
    else { //otherwise dismiss, and then present from the main controller
        [picker dismissViewControllerAnimated:YES completion:^{
            [self presentViewController:cropController animated:YES completion:nil];
        }];
    }
}

#pragma mark - Cropper Delegate -
- (void)cropViewController:(TOCropViewController *)cropViewController didCropToImage:(UIImage *)image withRect:(CGRect)cropRect angle:(NSInteger)angle
{
    [self updateImageViewWithImage:image fromCropViewController:cropViewController];
}

- (void)cropViewController:(TOCropViewController *)cropViewController didCropToCircularImage:(UIImage *)image withRect:(CGRect)cropRect angle:(NSInteger)angle
{
    [self updateImageViewWithImage:image fromCropViewController:cropViewController];
}

- (void)updateImageViewWithImage:(UIImage *)croppedImage fromCropViewController:(TOCropViewController *)cropViewController
{
    if (self->_alertIdx == 11013 || self->_alertIdx == 11012) {
        UIImageView *imageView = (UIImageView *)[self.vbody viewWithTag:11015];
        if ([imageView.image isEqual:croppedImage]) {
            [cropViewController.presentingViewController dismissViewControllerAnimated:YES completion:nil];
            return;
        }
    }
    else if (self->_alertIdx == 11213 || self->_alertIdx == 11212) {
        UIImageView *imageView = (UIImageView *)[self.vbody viewWithTag:11215];
        if ([imageView.image isEqual:croppedImage]) {
            [cropViewController.presentingViewController dismissViewControllerAnimated:YES completion:nil];
            return;
        }
    }
    else if (self->_alertIdx == 11313 || self->_alertIdx == 11312) {
        UIImageView *imageView = (UIImageView *)[self.vbody viewWithTag:11315];
        if ([imageView.image isEqual:croppedImage]) {
            [cropViewController.presentingViewController dismissViewControllerAnimated:YES completion:nil];
            return;
        }
    }
    else if (self->_alertIdx == 11413 || self->_alertIdx == 11412) {
        UIImageView *imageView = (UIImageView *)[self.vbody viewWithTag:11415];
        if ([imageView.image isEqual:croppedImage]) {
            [cropViewController.presentingViewController dismissViewControllerAnimated:YES completion:nil];
            return;
        }
    }

    //---------------------------------------------------------------------------------------------------------------------------------------------
    //self.imageView.image = croppedImage;
    //[self shadowProfileImage:croppedImage];
    UIImage *image = [Utils resizeImage:croppedImage];
    
    NSString* picture = [Utils encodeToBase64String:image];
    //NSLog(@"picture:%d",(int)[picture length]);
    
    if (self->_alertIdx == 11013 || self->_alertIdx == 11012) {
        UIButton *btn = (UIButton *)[self.vbody viewWithTag:11012];
        [btn setImage:nil forState:UIControlStateNormal];
        UIImageView *imageView = (UIImageView *)[self.vbody viewWithTag:11015];
        imageView.image = image;
        [propertyMap setObject:picture forKey:@"vehicle"];
        isUpdateImage1 = YES;
    }
    else if (self->_alertIdx == 11213 || self->_alertIdx == 11212) {
        UIButton *btn = (UIButton *)[self.vbody viewWithTag:11212];
        [btn setImage:nil forState:UIControlStateNormal];
        UIImageView *imageView = (UIImageView *)[self.vbody viewWithTag:11215];
        imageView.image = image;
        [propertyMap setObject:picture forKey:@"registration"];
        isUpdateImage2 = YES;
    }
    else if (self->_alertIdx == 11313 || self->_alertIdx == 11312) {
        UIButton *btn = (UIButton *)[self.vbody viewWithTag:11312];
        [btn setImage:nil forState:UIControlStateNormal];
        UIImageView *imageView = (UIImageView *)[self.vbody viewWithTag:11315];
        imageView.image = image;
        [propertyMap setObject:picture forKey:@"insurance"];
        isUpdateImage3 = YES;
    }
    else if (self->_alertIdx == 11413 || self->_alertIdx == 11412) {
        UIButton *btn = (UIButton *)[self.vbody viewWithTag:11412];
        [btn setImage:nil forState:UIControlStateNormal];
        UIImageView *imageView = (UIImageView *)[self.vbody viewWithTag:11415];
        imageView.image = image;
        [propertyMap2 setObject:picture forKey:@"inspection"];
        isUpdateImage4 = YES;
    }
    
    [self checkProcess:@"1"];
    
    [cropViewController.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - Private methods

- (void)showCamera
{
    self.croppingStyle = TOCropViewCroppingStyleDefault;
    
    UIImagePickerController *standardPicker = [[UIImagePickerController alloc] init];
    standardPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    standardPicker.allowsEditing = NO;
    standardPicker.delegate = self;
    [self presentViewController:standardPicker animated:YES completion:nil];

}

- (void)openPhotoAlbum
{
    self.croppingStyle = TOCropViewCroppingStyleDefault;
    
    UIImagePickerController *standardPicker = [[UIImagePickerController alloc] init];
    standardPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    standardPicker.allowsEditing = NO;
    standardPicker.delegate = self;
    [self presentViewController:standardPicker animated:YES completion:nil];

}

- (void)editPhoto
{
    UIImageView *imageView;
    if (self->_alertIdx == 11012 || self->_alertIdx == 11013) {
        imageView = (UIImageView *)[self.vbody viewWithTag:11015];
    }
    else if (self->_alertIdx == 11212 || self->_alertIdx == 11213) {
        imageView = (UIImageView *)[self.vbody viewWithTag:11215];
    }
    else if (self->_alertIdx == 11312 || self->_alertIdx == 11313) {
        imageView = (UIImageView *)[self.vbody viewWithTag:11315];
    }
    else if (self->_alertIdx == 11412 || self->_alertIdx == 11413) {
        imageView = (UIImageView *)[self.vbody viewWithTag:11415];
    }
    
    if (imageView.image) {
        TOCropViewController *cropController = [[TOCropViewController alloc] initWithCroppingStyle:self.croppingStyle image:imageView.image];
        cropController.delegate = self;
        
        [self presentViewController:cropController animated:YES completion:nil];
    }
    
}

#pragma mark - UITextField Delegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (self->_alertIdx == 3) {
        UITextField *secondTextField = _securityAlertView.textFieldsArray[(textField.tag == 1 ? 2 : 1)];
        
        NSMutableString *currentString = textField.text.mutableCopy;
        
        [currentString replaceCharactersInRange:range withString:string];
        
        [_securityAlertView setButtonAtIndex:0 enabled:(currentString.length > 2 && secondTextField.text.length > 2)];
    }
    else {
        UITextField *secondTextField = _securityAlertView.textFieldsArray[(textField.tag == 0 ? 1 : 0)];
        
        NSMutableString *currentString = textField.text.mutableCopy;
        
        [currentString replaceCharactersInRange:range withString:string];
        
        [_securityAlertView setButtonAtIndex:0 enabled:(currentString.length > 2 && secondTextField.text.length > 2)];
    }
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField.tag < 1)
        [_securityAlertView.textFieldsArray[(textField.tag + 1)] becomeFirstResponder];
    else
    {
        if ([_securityAlertView isButtonEnabledAtIndex:0])
            [_securityAlertView dismissAnimated:YES completionHandler:nil];
        else
            [textField resignFirstResponder];
    }
    
    return YES;
}

#pragma mark -
#pragma mark Validator
#pragma mark -
-(void) resignKeyboard
{
    [self.view endEditing:YES];
}

- (IBAction)validateAction:(id)sender
{
}

#pragma ValidatorDelegate - Delegate methods

- (void) preValidation
{
}

- (void)onSuccess
{
}

- (void)onFailure:(Rule *)failedRule
{
    
//    NSMutableDictionary *options = [@{kCRToastNotificationTypeKey               : @(CRToastTypeNavigationBar),
//                                      kCRToastNotificationPresentationTypeKey   : @(CRToastPresentationTypeCover),
//                                      kCRToastUnderStatusBarKey                 : @(YES),
//                                      kCRToastTextKey                           : failedRule.failureMessage,
//                                      kCRToastFontKey                           : [UIFont systemFontOfSize:14],
//                                      kCRToastTextAlignmentKey                  : @(NSTextAlignmentCenter),
//                                      kCRToastTimeIntervalKey                   : @(2),
//                                      kCRToastAnimationInTypeKey                : @(CRToastAnimationTypeGravity),
//                                      kCRToastAnimationOutTypeKey               : @(CRToastAnimationTypeGravity),
//                                      kCRToastAnimationInDirectionKey           : @(CRToastAnimationDirectionTop),
//                                      kCRToastAnimationOutDirectionKey          : @(CRToastAnimationDirectionTop),
//                                      kCRToastNotificationPreferredPaddingKey   : @(15)} mutableCopy];
//    [CRToastManager showNotificationWithOptions:options
//                                completionBlock:^{
//                                    NSLog(@"Completed");
//                                }];
    
    [WToast showWithText:failedRule.failureMessage duration:kWTShort roundedCorners:NO gravity:kWTGravityTop];
}


@end
