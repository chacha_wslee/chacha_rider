//
//  MenuViewController.h
//  SlideMenu
//

//

#import <UIKit/UIKit.h>
#import "TXBaseViewController.h"

@protocol VehicleSelectDelegate <NSObject>
@required
- (void) didVehicleSelected;
@end


@interface TXVehicleVC : TXBaseViewController <UITableViewDelegate, UITableViewDataSource> {

}

@property (nonatomic, strong) NSString *cellIdentifier;
@property (nonatomic, strong) IBOutlet UITableView *tableView;

@property (nonatomic, assign) id<VehicleSelectDelegate> delegate;


@end
