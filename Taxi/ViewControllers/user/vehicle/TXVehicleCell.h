//
//  TXPaymentCell.h
//  TXPaymentCell
//

//

#import <UIKit/UIKit.h>

@interface TXVehicleCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *lb1;
@property (nonatomic, weak) IBOutlet UILabel *lb2;
@property (nonatomic, weak) IBOutlet UILabel *lb3;

@property (nonatomic, weak) IBOutlet UIButton *btn1;
@property (nonatomic, weak) IBOutlet UIButton *btn2;

- (void)bindData:(NSDictionary*)dic;

@end
