//
//  TXChargeVC.m
//  Taxi
//

//

#import "TXChargeVC.h"
#import "utils.h"
#import "converter.h"
#import "LGAlertView.h"

//#import "NSString+FontAwesome.h"
#import <HCSStarRatingView/HCSStarRatingView.h>

#import "Validator.h"
#import "WToast.h"
//#import "CRToast.h"

@interface TXChargeVC () <UIGestureRecognizerDelegate, ValidatorDelegate> {
    int userId;
    NSArray *_msgList;
    
    BOOL isCanceled;
#ifdef _WITZM
#elif defined _CHACHA
#else
    PPiFlatSegmentedControl *segmented;
#endif
}

@property (strong, nonatomic) IBOutlet HCSStarRatingView *ratingView;
@property (strong, nonatomic) LGAlertView *securityAlertView;
@end

@implementation TXChargeVC {
    NSInteger rating;
    NSInteger tip;
}

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    
    if(self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        rating = 5;
    }
    
    return self;
}

-(void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    CGRect frame = _vPay.frame;
    frame.origin.y = _vBg.frame.origin.y + 50 - frame.size.height;//67;//_lbBottom.frame.origin.y - 20 - frame.size.height;
    _vPay.frame = frame;
    _btnPayAdd.tag = 200;
    
    NSString* oseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.OSEQ];
    if ([[Utils nullToString:oseq] isEqualToString:@""]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_DISSMISS_CANCEL object:nil userInfo:nil];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            NSLog(@"NOTIFICATION_DISSMISS_CANCEL");
            //[self.navigationController popToRootViewControllerAnimated:YES];
        });
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    // Disable iOS 7 back gesture
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
        self.navigationController.interactivePopGestureRecognizer.delegate = self;
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self removeEventListeners];
    
    // Enable iOS 7 back gesture
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
        self.navigationController.interactivePopGestureRecognizer.delegate = nil;
    }
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    return NO;
}

#pragma mark - Configure

-(void)configureStyles {
    [super configureStyles];
    
//    if (appDelegate.isDriverMode) {
//        [super navigationType01X:[UIImage imageNamed:@"btn_back"] centerText:@"Rating & Tip"];
//    }
//    else {
//        [super navigationType01X:[UIImage imageNamed:@"btn_back"] centerText:@"Rating"];
//    }
    //[super navigationType01X:nil centerText:LocalizedStr(@"Map.Drive.Rating.title")];
}

-(void)configure {
    [super configure];
    
    isCanceled = NO;
    if (appDelegate.isDriverMode) {
        if ((appDelegate.pstate == 230 && appDelegate.pstate2 <= -10) || (appDelegate.pstate == 240 && appDelegate.pstate2 <= -10)) {
            isCanceled = YES;
        }
    }
    else {
        if ((appDelegate.ustate == 130 && appDelegate.ustate2 <= -10) || (appDelegate.ustate == 140 && appDelegate.ustate2 <= -10)) {
            isCanceled = YES;
        }
    }
    
    UIView *n = [super navigationView];
    //NSInteger _y = n.frame.origin.y + n.frame.size.height;
    n.hidden = YES;
    
    // 2. init event
    _msgList = @[
                 @[@"PP601",@""], // rider profile
                 @[@"UR602",@""], // driver profile
                 
                 @[@"UQ300",@""], // driver profile
                 @[@"PQ300",@""], // driver profile
                 
                 @[@"UD421",@""], // rider rate
                 @[@"PD421",@""], // driver rate
//                 @[@"UD423",@""], // rider tip
                 @[@"UD413",@""], // rider tip
                 
                 @[@"UD410",@""], // cancel confirm
                 @[@"PD410",@""], // cancel confirm
                 ];
    
    [self registerEventListeners];
    
    _imgProfile.image = nil;
    
    [self showBusyIndicator:@"Requesting Charge ... "];
    if (appDelegate.isDriverMode) {
        [self configureDriver];
        [self->model PQ300];
        //[self configureRider2];
    }
    else {
        [self configureRider];
        [self->model UQ300];
        //[self configureDriver2];
    }
    
    // 취소인 경우에 대한 처리가 필요
    if (isCanceled) {
        _lbPayTitle.text= LocalizedStr(@"CHARGE_Rating_Cancel.PayTitle.text");
        
#ifndef _DRIVER_MODE
        _vRate.hidden = YES;
#endif
        _ratingView.hidden = YES;
        _ratingView.value = 0;
        rating = 0;
    }
    else {
        _lbPayTitle.text= LocalizedStr(@"CHARGE_Rating_Open.PayTitle.text");
        
#ifndef _DRIVER_MODE
        _ratingView.value = 0;
        rating = 0;
#endif
    }
//    }
    
    
    _imgPattern.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"img_fare_line"]];
    
    _vBg.backgroundColor = UIColorDefault;
    //self.view.backgroundColor = UIColorLabelText;
    _lbName.textColor = HEXCOLOR(0x4a4a4aff);
    _lbGrade.textColor = UIColorDefault;
    _lbVehicle.textColor = [UIColor blackColor];
    //[Utils layerButton:_btnSubmit];
    
    CGRect frame = _btnSubmit.frame;
    frame.origin.y -= kBottomBottonSafeArea;
    _btnSubmit.frame = frame;
    
    [_btnSubmit setTitleColor:UIColorButtonText forState:UIControlStateNormal];
    _btnSubmit.backgroundColor = UIColorDefault2;
    [_btnSubmit setTitle:LocalizedStr(@"Map.Payment.BTN.submit") forState:UIControlStateNormal];
    
    [_btnSubmit sendSubviewToBack:_vPay];
    
//#ifdef _CHACHA
    self.view.backgroundColor = HEXCOLOR(0xeeeeeeff);
#ifdef _DRIVER_MODE
    _lbBottom.text = LocalizedStr(@"DRIVER.CHARGE_Rating_Open.Bottom.text");
#else
    _lbBottom.text = LocalizedStr(@"CHARGE_Rating_Open.Bottom.text");
#endif
    _lbBottom.textColor = UIColorGray;
    frame = _lbBottom.frame;
    frame.origin.y -= kBottomBottonSafeArea;
    _lbBottom.frame = frame;
    
    _lbDate.textColor = UIColorGray;
    _lbPayTitle.textColor = HEXCOLOR(0x333333FF);
    _lbPay.textColor = UIColorLabelTextColor;
    
    [_btnPayAdd setTitleColor:UIColorDefault2 forState:UIControlStateNormal];
    [_btnPayAdd setTitle:LocalizedStr(@"CHARGE_Rating_Open.BTN.Show.text") forState:UIControlStateNormal];
    
    _lbPayTitle01.textColor = HEXCOLOR(0x333333FF);
    _lbPayTitle02.textColor = HEXCOLOR(0x666666FF);
#ifdef _DRIVER_MODE
    _lbPayTitle1.text = LocalizedStr(@"CHARGE_Rating_Open.PayTitle2.text");
    _lbPayTitle1.textColor = HEXCOLOR(0x111111FF);
    _lbPayTitle4.text = LocalizedStr(@"CHARGE_Rating_Open.PayTitle3.text");
    _lbPayTitle4.textColor = HEXCOLOR(0x111111FF);
    _lbPayTitle5.text = LocalizedStr(@"CHARGE_Rating_Open.PayTitle6.text");
    _lbPayTitle5.textColor = HEXCOLOR(0x111111FF);
    
    _lbPay1.textColor = HEXCOLOR(0x111111FF);
    _lbPay4.textColor = HEXCOLOR(0x111111FF);
    _lbPay5.textColor = HEXCOLOR(0x111111FF);
    
//    _lbPayTitle4.text = LocalizedStr(@"CHARGE_Rating_Open.PayTitle2.text");
//    _lbPayTitle4.textColor = HEXCOLOR(0x111111FF);
//    _lbPayTitle5.text = LocalizedStr(@"CHARGE_Rating_Open.PayTitle3.text");
//    _lbPayTitle5.textColor = HEXCOLOR(0x111111FF);
//
//    _lbPay4.textColor = HEXCOLOR(0x111111FF);
//    _lbPay5.textColor = HEXCOLOR(0x111111FF);
#else
    _lbPayTitle1.text = LocalizedStr(@"CHARGE_Rating_Open.PayTitle1.text");
    _lbPayTitle1.textColor = HEXCOLOR(0x111111FF);
    _lbPayTitle2.text = LocalizedStr(@"CHARGE_Rating_Open.PayTitle2.text");
    _lbPayTitle2.textColor = HEXCOLOR(0x111111FF);
    _lbPayTitle3.text = LocalizedStr(@"CHARGE_Rating_Open.PayTitle3.text");
    _lbPayTitle3.textColor = HEXCOLOR(0x111111FF);
    _lbPayTitle4.text = LocalizedStr(@"CHARGE_Rating_Open.PayTitle4.text");
    _lbPayTitle4.textColor = HEXCOLOR(0x111111FF);
    _lbPayTitle5.text = LocalizedStr(@"CHARGE_Rating_Open.PayTitle5.text");
    _lbPayTitle5.textColor = HEXCOLOR(0x111111FF);
    
    _lbPay1.textColor = HEXCOLOR(0x111111FF);
    _lbPay2.textColor = HEXCOLOR(0x111111FF);
    _lbPay3.textColor = HEXCOLOR(0x111111FF);
    _lbPay4.textColor = HEXCOLOR(0x111111FF);
    _lbPay5.textColor = UIColorDefault;
    
    // vpay
    _vRate.frame = self.view.frame;
    [_vRate viewWithTag:10].backgroundColor = HEXCOLOR(0xc8c8c8FF);
    //_vRate.backgroundColor = HEXCOLOR(0xc8c8c8);
    _lbRateText.text = LocalizedStr(@"CHARGE_Rating_Open.Rate.Desc.text");
    _lbRateText.textColor = HEXCOLOR(0x111111FF);
    _lbRateText2.text = LocalizedStr(@"CHARGE_Rating_Open.Rate.Desc2.text");
    _lbRateText2.textColor = HEXCOLOR(0x111111FF);
    
    [_btnRateSubmit setTitleColor:UIColorButtonText forState:UIControlStateNormal];
    _btnRateSubmit.backgroundColor = UIColorDefault;
    [_btnRateSubmit setTitle:LocalizedStr(@"CHARGE_Rating_Open.Rate.BTN.text") forState:UIControlStateNormal];
    
    [self.view addSubview:_vRate];
    
    //_ratingView.enabled = NO;
#endif
    
    
//#endif
}

-(void) configureConfig {

    
}

-(void) configureRider {
    // 1. init var
    _lbDate.text    = @"";
    _lbPay.text     = @"";
    _lbName.text    = @"";
    _lbGrade.text   = @"";
    _lbPoint.text   = @"";
    _lbVehicle.text = @"";
    _lbPayTitle01.text = @"";
    _lbPayTitle02.text = @"";
    
    _lbGrade.hidden = YES;
    _lbVehicle.hidden = YES;
//    CGRect rect = _lbVehicle.frame;
//    rect.origin.y = _lbGrade.frame.origin.y;
//    _lbVehicle.frame = rect;
    
    // 3. call api
    if (appDelegate.imgProfileDriver != nil) {
        [self saveProfileImage:appDelegate.imgProfileDriver withCode:@"PP602"];
    }
    [self->model PP602];
    /*
//#ifndef _CHACHA
    // 4. settup
    
//    NSString *itemString1 = [NSString stringWithFormat:@"   %@",LocalizedStr(@"Map.Payment.Tip.text1")];
//    NSString *itemString2 = [NSString stringWithFormat:@"%@",LocalizedStr(@"Map.Payment.Tip.text5")];
    
    // Do any additional setup after loading the view, typically from a nib.
    NSArray *items = @[[[PPiFlatSegmentItem alloc] initWithTitle:LocalizedStr(@"Map.Payment.Tip.text1") andIcon:nil],
                       [[PPiFlatSegmentItem alloc] initWithTitle:LocalizedStr(@"Map.Payment.Tip.text2") andIcon:nil],
                       [[PPiFlatSegmentItem alloc] initWithTitle:LocalizedStr(@"Map.Payment.Tip.text3") andIcon:nil],
                       [[PPiFlatSegmentItem alloc] initWithTitle:LocalizedStr(@"Map.Payment.Tip.text4") andIcon:nil],
                       [[PPiFlatSegmentItem alloc] initWithTitle:LocalizedStr(@"Map.Payment.Tip.text5") andIcon:nil]];
    
    UIView *v = [self.view viewWithTag:20];
    segmented=[[PPiFlatSegmentedControl alloc] initWithFrame:v.frame
                                                       items:items
                                                iconPosition:IconPositionRight
                                           andSelectionBlock:^(NSUInteger segmentIndex) {
                                               NSLog(@"segmentIndex:%lu",(unsigned long)segmentIndex);
                                               
                                               if (segmentIndex == 0) {
                                                   tip = 0;
                                                   [segmented setItems:items];
                                               }
                                               else if (segmentIndex == 1) {
                                                   tip = 1;
                                                   [segmented setItems:items];
                                               }
                                               else if (segmentIndex == 2) {
                                                   tip = 3;
                                                   [segmented setItems:items];
                                               }
                                               else if (segmentIndex == 3) {
                                                   tip = 5;
                                                   [segmented setItems:items];
                                               }
                                               else if (segmentIndex == 4) {
                                                   [self onOtherTip];
                                               }
                                           }
                                              iconSeparation:5];
    
    segmented.color=UIColorDefault2;
    segmented.layer.cornerRadius = 20;
    //segmented.borderWidth=0.5;
    //segmented.borderColor= [UIColor colorWithRed:0.0f/255.0 green:141.0f/255.0 blue:147.0f/255.0 alpha:1];
    segmented.selectedColor=UIColorDefault;//[UIColor colorWithRed:0.0f/255.0 green:141.0f/255.0 blue:147.0f/255.0 alpha:1];
    segmented.textAttributes=@{NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue-Bold" size:14],
                               NSForegroundColorAttributeName:[UIColor whiteColor]};
    segmented.selectedTextAttributes=@{NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue-Bold" size:14],
                                       NSForegroundColorAttributeName:[UIColor whiteColor]};
    [self.view addSubview:segmented];
//#endif
     */
}

-(void) configureRider2 {
    
    NSDictionary *user = [appDelegate.dicRider objectForKey:@"provider_device"];
    NSDictionary *charge = [appDelegate.dicRider objectForKey:@"drive_charge"];
    NSDictionary *trip = [appDelegate.dicRider objectForKey:@"drive_trip"];
    
    if ([Utils isDictionary:charge]) {
        //_lbPay.text     = CURRENCY_FORMAT(charge[@"fare_trip"]);
        NSInteger sum   = [charge[@"fare_sum"] intValue];
        NSInteger extra = [charge[@"fare_extra"] intValue];
        NSInteger discount = [charge[@"fare_discount"] intValue];
        NSInteger payed = [charge[@"fare_payed"] intValue];
        
        NSInteger pay1 = sum+extra;
        NSInteger pay2 = round((sum+extra)*0.05);
        NSInteger pay3 = round((sum+extra)*0.95);
        NSInteger pay4 = discount;
        
        NSString *p1 = [NSString stringWithFormat:@"%ld",(long)pay1];
        NSString *p2 = [NSString stringWithFormat:@"%ld",(long)pay2];
        NSString *p3 = [NSString stringWithFormat:@"%ld",(long)pay3];
        NSString *p4 = [NSString stringWithFormat:@"%ld",(long)pay4];
        NSString *p  = [NSString stringWithFormat:@"%ld",(long)payed];
        
        _lbPay1.text     = CURRENCY_FORMAT(p1);
        _lbPay2.text     = CURRENCY_FORMAT(p2);
        _lbPay3.text     = CURRENCY_FORMAT(p3);
        _lbPay4.text     = CURRENCY_FORMAT(p4);
        _lbPay5.text     = CURRENCY_FORMAT(p);
        _lbPay.text      = CURRENCY_FORMAT(p);
    }
    else {
        _lbPay1.text     = CURRENCY_FORMAT(@"0");
        _lbPay2.text     = CURRENCY_FORMAT(@"0");
        _lbPay3.text     = CURRENCY_FORMAT(@"0");
        _lbPay4.text     = CURRENCY_FORMAT(@"0");
        _lbPay5.text     = CURRENCY_FORMAT(@"0");
        _lbPay.text      = CURRENCY_FORMAT(@"0");
    }
    
    if (isCanceled) {
        NSDictionary *pickup = [appDelegate.dicRider objectForKey:@"drive_pickup"];
        
        _lbDate.text    = StringFormatDate2Date2(pickup[@"reg_date"]);
        
        _lbPayTitle01.text    = StringFormatDate2DateYYYYMM(pickup[@"reg_date"]);
        _lbPayTitle02.text    = StringFormatDate2DateHHMM(pickup[@"reg_date"]);
    }
    else {
        _lbDate.text    = StringFormatDate2Date2(trip[@"reg_date"]);
        
        _lbPayTitle01.text    = StringFormatDate2DateYYYYMM(trip[@"reg_date"]);
        _lbPayTitle02.text    = StringFormatDate2DateHHMM(trip[@"reg_date"]);
    }
    
    
    //_lbGrade.text = [Utils labelStringUserInfo:[user objectForKey:@"trips"] rate:[user objectForKey:@"rate"]];
    _lbName.text = [Utils nameToString:[user objectForKey:@"name"]];
#ifdef _DRIVER_MODE
    _lbName.text = [Utils nameToString:[user objectForKey:@"name"]];
#else
    _lbName.text = [NSString stringWithFormat:@"%@ / %@", [Utils nameToString:[user objectForKey:@"name"]], [Utils nameToString:[user objectForKey:@"service_name"]]];
    _lbName.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:14];
#endif
    
}

-(void) configureDriver {
    // 1. init var
    _lbDate.text    = @"";
    _lbPay.text     = @"";
    _lbPayTitle.text     = @"";
    _lbName.text    = @"";
    _lbGrade.text   = @"";
    _lbPoint.text   = @"";
    _lbVehicle.text = @"";
    _lbPayTitle01.text = @"";
    _lbPayTitle02.text = @"";
    
    [_btnPayAdd setTitle:@"" forState:UIControlStateNormal];
    
    // 3. call api
    //[self->model UR601];
    if (appDelegate.imgProfileRider != nil) {
        [self saveProfileImage:appDelegate.imgProfileRider withCode:@"PP601"];
    }

    [self->model PP601];
}

// for driver
-(void) configureDriver2 {
    NSDictionary *user = [appDelegate.dicDriver objectForKey:@"user_device"];
    NSDictionary *charge = [appDelegate.dicDriver objectForKey:@"drive_charge"];
    NSDictionary *trip = [appDelegate.dicDriver objectForKey:@"drive_trip"];
    
    if ([Utils isDictionary:charge]) {
        //_lbPay.text     = CURRENCY_FORMAT(charge[@"fare_trip"]);
        NSInteger sum   = [charge[@"fare_sum"] intValue];
        NSInteger extra = [charge[@"fare_extra"] intValue];
        
        NSInteger pay1 = sum+extra;
        NSInteger pay2 = round((sum+extra)*0.05);
        NSInteger pay3 = round((sum+extra)*0.75);
        NSInteger pay4 = round((sum+extra)*0.20);
        
        NSString *p1 = [NSString stringWithFormat:@"%ld",(long)pay1];
        NSString *p2 = [NSString stringWithFormat:@"%ld",(long)pay2];
        NSString *p3 = [NSString stringWithFormat:@"%ld",(long)pay3];
        NSString *p4 = [NSString stringWithFormat:@"%ld",(long)pay4];
//        NSString *p  = [NSString stringWithFormat:@"%ld",(long)extra];
        
        _lbPay1.text     = CURRENCY_FORMAT(p2);
        _lbPay4.text     = CURRENCY_FORMAT(p3);
        _lbPay5.text     = CURRENCY_FORMAT(p4);
        _lbPay.text      = CURRENCY_FORMAT(p1);
        
//        _lbPay4.text     = CURRENCY_FORMAT(p2);
//        _lbPay5.text     = CURRENCY_FORMAT(p3);
//        _lbPay.text      = CURRENCY_FORMAT(p1);
    }
    else {
        _lbPay1.text     = CURRENCY_FORMAT(@"0");
        _lbPay4.text     = CURRENCY_FORMAT(@"0");
        _lbPay5.text     = CURRENCY_FORMAT(@"0");
        _lbPay.text      = CURRENCY_FORMAT(@"0");
    }
    
    if (isCanceled) {
        NSDictionary *pickup = [appDelegate.dicDriver objectForKey:@"drive_pickup"];
        
        _lbDate.text    = StringFormatDate2Date2(pickup[@"reg_date"]);
        _lbPayTitle01.text    = StringFormatDate2DateYYYYMM(pickup[@"reg_date"]);
        _lbPayTitle02.text    = StringFormatDate2DateHHMM(pickup[@"reg_date"]);
    }
    else {
        _lbDate.text    = StringFormatDate2Date2(trip[@"reg_date"]);
        _lbPayTitle01.text    = StringFormatDate2DateYYYYMM(trip[@"reg_date"]);
        _lbPayTitle02.text    = StringFormatDate2DateHHMM(trip[@"reg_date"]);
    }
    
    //_lbPayTitle.text= LocalizedStr(@"CHARGE_Rating_Open.PayTitle.text");
    [_btnPayAdd setTitle:LocalizedStr(@"CHARGE_Rating_Open.BTN.Show.text") forState:UIControlStateNormal];
    
    //_lbGrade.text = [Utils labelStringUserInfo:[user objectForKey:@"trips"] rate:[user objectForKey:@"rate"]];
    _lbName.text = [Utils nameToString:[user objectForKey:@"name"]];
    
}

-(void) registerEventListeners {
    for (NSArray *msg in _msgList) {
        [self->model addEventListener:self forEvent:[msg objectAtIndex:0] eventParams:nil];
    }
}

-(void) removeEventListeners {
    for (NSArray *msg in _msgList) {
        [self->model removeEventListener:self forEvent:[msg objectAtIndex:0]];
    }
}

#pragma mark - Action
-(IBAction)onNaviButtonClick:(id)sender {
    UIButton *btn = (UIButton*)sender;
    NSLog(@"onNaviButtonClick");
    //[super onNaviButtonClick:sender];
    
    if (btn.tag == 1100) {
//        [appDelegate resetState2];
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_DISSMISS_CANCEL object:nil userInfo:nil];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            //[self.navigationController popViewControllerAnimated:YES];
            NSLog(@"NOTIFICATION_DISSMISS_CANCEL");
            //[self.navigationController popToRootViewControllerAnimated:YES];
        });
    }
    else if (_btnPayAdd.tag == 201) {
        _btnPayAdd.userInteractionEnabled = NO;
        // show pay off
        [UIView animateWithDuration:0.5
                         animations:^{
                             CGRect frame = _vPay.frame;
#ifdef _DRIVER_MODE
                             frame.origin.y = _vBg.frame.origin.y + 50 - frame.size.height;//67;//_lbBottom.frame.origin.y - 20 - frame.size.height;
#else
                             frame.origin.y = _vBg.frame.origin.y + 50 - frame.size.height;//67;//_lbBottom.frame.origin.y - 20 - frame.size.height;
#endif
                             _vPay.frame = frame;
                             
                             _imgGrade.transform = CGAffineTransformMakeRotation(0);
                         }
                         completion:^(BOOL finished) {
                             _btnPayAdd.userInteractionEnabled = YES;
                         }];
        _btnPayAdd.tag = 200;
    }
    else if (_btnPayAdd.tag == 200) {
        _btnPayAdd.userInteractionEnabled = NO;
        // show pay on
        [UIView animateWithDuration:0.5
                         animations:^{
                             CGRect frame = _vPay.frame;
#ifdef _DRIVER_MODE
                             frame.origin.y = _vBg.frame.origin.y - 100 - 120 + 40;//- 120;//67;//_lbBottom.frame.origin.y - 20 - frame.size.height;
#else
                             frame.origin.y = _vBg.frame.origin.y - 100;//67;//_lbBottom.frame.origin.y - 20 - frame.size.height;
#endif
                             _vPay.frame = frame;
                             
                             //_imgGrade.transform = CGAffineTransformMakeRotation(0);
                             _imgGrade.transform = CGAffineTransformMakeRotation(M_PI_2*2);
                         }
                         completion:^(BOOL finished) {
                             _btnPayAdd.userInteractionEnabled = YES;
                         }];
        _btnPayAdd.tag = 201;
    }
}
/*
//#ifndef _CHACHA
-(void)onOtherTip {
    
    LGAlertView *alertView = [[LGAlertView alloc] initWithTextFieldsAndTitle:nil
                                                                     message:LocalizedStr(@"Map.Payment.Tip.Other.title")
                                                          numberOfTextFields:1
                                                      textFieldsSetupHandler:^(UITextField *textField, NSUInteger index)
                              {
                                  if (index == 0) {
                                      textField.placeholder = @"";
                                      textField.text = @"";
                                  }
                                  
                                  textField.tag = index;
                                  //textField.delegate = self;
                                  textField.enablesReturnKeyAutomatically = YES;
                                  textField.autocapitalizationType = NO;
                                  textField.autocorrectionType = NO;
                                  textField.clearButtonMode = UITextFieldViewModeNever;
                                  textField.textAlignment = NSTextAlignmentCenter;
                                  textField.keyboardType = UIKeyboardTypeNumberPad;
                              }
                                                                buttonTitles:@[LocalizedStr(@"Button.OK")]
                                                           cancelButtonTitle:LocalizedStr(@"Button.CANCEL")
                                                      destructiveButtonTitle:nil
                                                               actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                                                                   UITextField *secondTextField = alertView.textFieldsArray[index];
                                                                   //promoCode = secondTextField.text;
                                                                   
                                                                   [self resignKeyboard];
                                                                   
                                                                   Validator *validator = [[Validator alloc] init];
                                                                   validator.delegate   = self;
                                                                   
                                                                   // 이름
                                                                   [validator putRule:[Rules minNum:0 withFailureString:LocalizedStr(@"Validator.Tip.text") forTextField:secondTextField]];
                                                                   [validator putRule:[Rules maxNum:999 withFailureString:LocalizedStr(@"Validator.Tip.text") forTextField:secondTextField]];
                                                                   [validator putRule:[Rules checkIfNumericWithFailureString:LocalizedStr(@"Validator.Tip.text") forTextField:secondTextField]];
                                                                   
                                                                   if (![validator validateForResult]) {
                                                                       [alertView.firstButton setSelected:NO];
                                                                       return;
                                                                   }
                                                                   
                                                                   NSLog(@"actionHandler, %@, %lu, %@", title, (long unsigned)index, secondTextField.text);
                                                                   NSLog(@"tip:%f",[secondTextField.text doubleValue]);
                                                                   tip = [secondTextField.text integerValue];
                                                                   NSLog(@"tip:%ld",(long)tip);
                                                                   NSString *tipStr = [NSString stringWithFormat:LocalizedStr(@"String.Currency"),secondTextField.text];
                                                                   
                                                                   NSArray *items = @[[[PPiFlatSegmentItem alloc] initWithTitle:LocalizedStr(@"Map.Payment.Tip.text1") andIcon:nil],
                                                                                      [[PPiFlatSegmentItem alloc] initWithTitle:LocalizedStr(@"Map.Payment.Tip.text2") andIcon:nil],
                                                                                      [[PPiFlatSegmentItem alloc] initWithTitle:LocalizedStr(@"Map.Payment.Tip.text3") andIcon:nil],
                                                                                      [[PPiFlatSegmentItem alloc] initWithTitle:LocalizedStr(@"Map.Payment.Tip.text4") andIcon:nil],
                                                                                      [[PPiFlatSegmentItem alloc] initWithTitle:tipStr andIcon:nil]];
                                                                   [segmented setItems:items];
                                                                   
                                                                   [alertView dismissAnimated:YES completionHandler:nil];
                                                               }
                                                               cancelHandler:^(LGAlertView *alertView) {
                                                                   [alertView dismissAnimated:YES completionHandler:nil];
                                                                   
                                                               }
                                                          destructiveHandler:^(LGAlertView *alertView) {
                                                              [alertView dismissAnimated:YES completionHandler:nil];
                                                          }];
    //[alertView setButtonAtIndex:0 enabled:NO];
    [Utils initAlertButtonColor:alertView];
    [alertView showAnimated:YES completionHandler:nil];
    [alertView setDismissOnAction:NO];
}
#endif
*/
-(IBAction)onClickFareExtra {
    
    LGAlertView *alertView = [[LGAlertView alloc] initWithTextFieldsAndTitle:nil
                                                                     message:LocalizedStr(@"Map.Payment.Pay.Add.title")
                                                          numberOfTextFields:1
                                                      textFieldsSetupHandler:^(UITextField *textField, NSUInteger index)
                              {
                                  if (index == 0) {
                                      textField.placeholder = @"";
                                      textField.text = @"";
                                  }
                                  
                                  textField.tag = index;
                                  //textField.delegate = self;
                                  textField.enablesReturnKeyAutomatically = YES;
                                  textField.autocapitalizationType = NO;
                                  textField.autocorrectionType = NO;
                                  textField.clearButtonMode = UITextFieldViewModeNever;
                                  textField.textAlignment = NSTextAlignmentCenter;
                                  textField.keyboardType = UIKeyboardTypeNumberPad;
                              }
                                                                buttonTitles:@[LocalizedStr(@"Button.OK")]
                                                           cancelButtonTitle:LocalizedStr(@"Button.CANCEL")
                                                      destructiveButtonTitle:nil
                                                               actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                                                                   UITextField *secondTextField = alertView.textFieldsArray[index];
                                                                   //promoCode = secondTextField.text;
                                                                   
                                                                   [self resignKeyboard];
                                                                   
                                                                   Validator *validator = [[Validator alloc] init];
                                                                   validator.delegate   = self;
                                                                   
                                                                   // 이름
                                                                   [validator putRule:[Rules minNum:500 withFailureString:LocalizedStr(@"Validator.Extra.text") forTextField:secondTextField]];
                                                                   [validator putRule:[Rules maxNum:50000 withFailureString:LocalizedStr(@"Validator.Extra.text") forTextField:secondTextField]];
                                                                   [validator putRule:[Rules checkIfNumericWithFailureString:LocalizedStr(@"Validator.Extra.text") forTextField:secondTextField]];
                                                                   
                                                                   if (![validator validateForResult]) {
                                                                       [alertView.firstButton setSelected:NO];
                                                                       return;
                                                                   }
                                                                   
                                                                   NSLog(@"actionHandler, %@, %lu, %@", title, (long unsigned)index, secondTextField.text);
                                                                   NSLog(@"tip:%f",[secondTextField.text doubleValue]);
                                                                   tip = [secondTextField.text integerValue];
                                                                   NSLog(@"tip:%ld",(long)tip);
                                                                   NSString *tipStr = CURRENCY_FORMAT2(secondTextField.text);
                                                                   _lbPay.text = tipStr;
                                                                   
                                                                   [alertView dismissAnimated:YES completionHandler:nil];
                                                               }
                                                               cancelHandler:^(LGAlertView *alertView) {
                                                                   [alertView dismissAnimated:YES completionHandler:nil];
                                                                   
                                                               }
                                                          destructiveHandler:^(LGAlertView *alertView) {
                                                              [alertView dismissAnimated:YES completionHandler:nil];
                                                          }];
    //[alertView setButtonAtIndex:0 enabled:NO];
    [Utils initAlertButtonColor:alertView];
    [alertView showAnimated:YES completionHandler:nil];
    [alertView setDismissOnAction:NO];
}

#pragma mark - onEvent
-(void)onPD410 {
    
    NSString* oseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.OSEQ];
    NSDictionary *propertyMap = @{
                                  @"oseq" : oseq,
                                  };
    
    [self showBusyIndicator:@"Requesting Rate ... "];
    [self->model PD410:propertyMap];
}

-(void)onUD410 {
    
    NSString* oseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.OSEQ];
    //NSLog(@"tip:%f",tip);
    NSDictionary *propertyMap = @{
                                  @"oseq" : oseq,
                                  };
    
    [self showBusyIndicator:@"Requesting Rate ... "];
    [self->model UD410:propertyMap];
}

-(void)onPD421 {
    
    NSString* pseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.PSEQD];
    NSString* oseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.OSEQ];
    
    NSDictionary *propertyMap = @{
                                  @"pseq" : pseq,
                                  @"oseq" : oseq,
                                  @"rating" : [NSString stringWithFormat:@"%d",(int)rating],
                                  @"fare_extra" : [NSString stringWithFormat:@"%ld",(long)tip],
//                                  @"note" : _tfComment.text,
                                  };
    
    [self showBusyIndicator:@"Requesting Rate ... "];
    [self->model PD421:propertyMap];
}

-(void)onUD421 {
    
    NSString* useq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.USEQD];
    NSString* oseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.OSEQ];
    //NSLog(@"tip:%f",tip);
    NSDictionary *propertyMap = @{
                                  @"useq" : useq,
                                  @"oseq" : oseq,
                                  @"rating" : [NSString stringWithFormat:@"%d",(int)rating],
//                                  @"driver" : @"favorite", // __DEBUG 여기는 뭐가 들어가나? favorite를 고정값으로...
//                                  @"note" : _tfComment.text,
                                  @"tip_payed" : [NSString stringWithFormat:@"%ld",(long)tip],
                                  };
    
    [self showBusyIndicator:@"Requesting Rate ... "];
    [self->model UD421:propertyMap];
}




-(void)onUD423 {
    
    [self showBusyIndicator:@"Requesting Charge Tip ... "];
    [self->model UD423];
}

#pragma mark - IBAction
-(IBAction)onSubmit:(id)sender {
    
    if (isCanceled) {
        if (appDelegate.isDriverMode) {
            [self onPD410];
        }
        else {
            [self onUD410];
        }
    }
    else {
        if (appDelegate.isDriverMode) {
            [self onPD421];
        }
        else {
            [self onUD421];
        }
    }
}

#ifndef _DRIVER_MODE
-(IBAction)onRateSubmit:(id)sender {
    
    if (rating <= 0) {
        return;
    }
    [UIView animateWithDuration:0.4
                     animations:^{
                         CGRect rect = _vRate.frame;
                         rect.origin.y = self.view.frame.size.height;
                         _vRate.frame = rect;
                     }
                     completion:^(BOOL finished){
                         [_vRate removeFromSuperview];
                     }];
    
}
#endif

-(IBAction)onComment:(id)sender {
    CGRect textViewFrame = CGRectMake(20.0f, 20.0f, 280.0f, 124.0f);
    UITextView *textView = [[UITextView alloc] initWithFrame:textViewFrame];
    //textView.returnKeyType = UIReturnKeyDone;
    //textView.backgroundColor = [UIColor clearColor];
    [textView setFont:[UIFont systemFontOfSize:15]];
    textView.userInteractionEnabled = YES;
    
    if ([_tfComment.text length]>0) {
        textView.text = _tfComment.text;
    }
    
    //textView.delegate = self; //텍스트 뷰 델리게이트
    
    //CALayer 클래스 이용 텍스트 뷰 커스터마이징, 레이어 프로퍼티 사용을 위해 <QuartzCore/QuartzCore.h> 임포트 할 것.
    //[[textView layer] setBorderColor:[[UIColor grayColor] CGColor]]; //border color
    //[[textView layer] setBackgroundColor:[[UIColor whiteColor] CGColor]]; //background color
    //[[textView layer] setBorderWidth:0.5]; // border width
    //[[textView layer] setCornerRadius:5]; // radius of rounded corners
    //[textView setClipsToBounds: YES]; //clip text within the bounds
    
    LGAlertView *alertView = [[LGAlertView alloc] initWithViewAndTitle:@"Comment"
                                                               message:@""
                                                                 style:LGAlertViewStyleAlert
                                                                  view:textView
                                                          buttonTitles:@[LocalizedStr(@"Button.OK")]
                                                     cancelButtonTitle:LocalizedStr(@"Button.CANCEL")
                                                destructiveButtonTitle:nil
                                                         actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                                                             NSLog(@"actionHandler, %@, %lu", title, (long unsigned)index);
                                                             NSLog(@"text:%@",textView.text);
                                                             _tfComment.text = textView.text;
                                                             
                                                             //[propertyMap setValue:textView.text forKey:@"profile"];
                                                             
                                                         }
                                                         cancelHandler:^(LGAlertView *alertView) {
                                                             NSLog(@"cancelHandler");
                                                         }
                                                    destructiveHandler:^(LGAlertView *alertView) {
                                                        NSLog(@"destructiveHandler");
                                                    }];
    
    //alertView.heightMax = 256.f;
    [Utils initAlertButtonColor:alertView];
    alertView.windowLevel = LGAlertViewWindowLevelAboveStatusBar;
    [alertView showAnimated:YES completionHandler:^(void)
     {
         [textView becomeFirstResponder];
     }];
}

- (IBAction)didChangeValue:(HCSStarRatingView *)sender {
    rating = sender.value;
    NSLog(@"Changed rating to %.1f", sender.value);
}

#ifndef _DRIVER_MODE
- (IBAction)didChangeRateValue:(HCSStarRatingView *)sender {
    rating = sender.value;
    _ratingView.value = rating;
    NSLog(@"Changed rating to %.1f", sender.value);
}
#endif

#pragma mark - onEvent
-(void)onEvent:(TXEvent *)event eventParams:(id)subscriptionParams {
    
    TXResponseDescriptor * descriptor = [event getEventProperty:TXEvents.Params.DESCRIPTOR];
    TXRequestObj *request = [event getEventProperty:TXEvents.Params.REQUEST];
    
    [self hideBusyIndicator];
    if([event.name isEqualToString:@"UQ300"] || [event.name isEqualToString:@"PQ300"]) { // trip polling
        
        if(descriptor.success == true) {
            
            // 상대방을 봐야 한다.
            if (appDelegate.isDriverMode) {
                // 3. call api
                [self->model UR601];
                [self configureDriver2];
            }
            else {
                // 3. call api
                [self->model PP602];
                [self configureRider2];
            }
            
        } else {
            if (descriptor.rlt != -1000 && descriptor.rlt != -999999) {
                //에러주석[self alertError:@"Error" message:descriptor.error];
            }
        }
    }
    else if([event.name isEqualToString:@"UR602"] || [event.name isEqualToString:@"PP601"]) {
    
        if(descriptor.success == true) {
            NSString     *response = [[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding];
            NSDictionary *result   = getJSONObj(response);
            
            NSString *pictureData              = [result valueForKey:@"picture"];
            
            if ([pictureData length]) {
                
//                NSData *data = [[NSData alloc] initWithBase64EncodedString:pictureData options:0];
//                UIImage *image = [[UIImage alloc] initWithData:data];
                UIImage *image = [Utils decodeBase64ToImage:pictureData];
                // Store image
                //[appDelegate.cache storeObject:event forKey:request.reqUrl];
                
                [self saveProfileImage:image withCode:event.name];
            }
            else {
                [self saveProfileImage:[UIImage imageNamed:@"ic_user_white"] withCode:event.name];
            }
            
        } else {
            //[self saveProfileImage:[UIImage imageNamed:@"ic_user_white"] withCode:event.name];
        }
    }
    else if([event.name isEqualToString:@"UD421"] || [event.name isEqualToString:@"UD410"]) { // charge rate
        
        if(descriptor.success == true) {
            // 별점 처리 후 tip 과금처리
            
            
#ifdef _WITZM
            [appDelegate resetState2];
            
            // 상태 초기화
            [appDelegate resetTripInfo];
            [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_DISSMISS_CANCEL object:nil userInfo:nil];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                //[self.navigationController popViewControllerAnimated:YES];
                NSLog(@"NOTIFICATION_DISSMISS_CANCEL");
                //[self.navigationController popToRootViewControllerAnimated:YES];
            });
#elif defined _CHACHA
            [appDelegate resetState2];
            
            // 상태 초기화
            [appDelegate resetTripInfo];
            [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_DISSMISS_CANCEL object:nil userInfo:nil];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                //[self.navigationController popViewControllerAnimated:YES];
                NSLog(@"NOTIFICATION_DISSMISS_CANCEL");
                //[self.navigationController popToRootViewControllerAnimated:YES];
            });
#else
            NSString     *response = [[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding];
            NSDictionary *responseObj   = getJSONObj(response);
            
            NSString*   pay_token = [responseObj objectForKey:SettingsConst.CryptoKeys.PAY_TOKEN];
            if (![pay_token isKindOfClass:[NSNull class]] && [pay_token length] > 10) {
                // paytoken처리
                
                [self showBusyIndicator:@"Requesting Charge ... "];
                
                NSString* oseq   = [[responseObj valueForKey:@"drive_charge"] valueForKey:@"oseq"];
                NSString* vseq   = [[responseObj valueForKey:@"drive_charge"] valueForKey:@"vseq"];
                //NSString* pseq   = [[responseObj valueForKey:@"provider_device"] valueForKey:@"pseq"];
                
                [[[TXApp instance] getSettings] setFDKeychain:SettingsConst.CryptoKeys.OSEQ value:oseq];
                //[[[TXApp instance] getSettings] setFDKeychain:SettingsConst.CryptoKeys.PSEQ value:pseq];
                [[[TXApp instance] getSettings] setFDKeychain:SettingsConst.CryptoKeys.VSEQ value:vseq];
                
                id   user_payment = [appDelegate.dicRider objectForKey:@"user_payments"]; // __DEBUG
                
                
                for (NSDictionary* key in user_payment) {
                    NSString* mode    = [key valueForKey:@"mode"];
                    NSString* state   = [key valueForKey:@"state"];
                    
                    //NSLog(@"key:%@",key);
                    
                    //선택된 카드의 인증을 한다.
                    if ([mode isEqualToString:@"1"] && [state isEqualToString:@"1"]) {
                        
                        NSString* no     = [key valueForKey:@"no"];
                        NSString* cvv    = [key valueForKey:@"cvv"];
                        NSString* expire = [key valueForKey:@"expire"];
                        NSString* cseq   = [key valueForKey:@"cseq"];
                        
                        [[[TXApp instance] getSettings] setFDKeychain:SettingsConst.CryptoKeys.CSEQ value:cseq];
                        [[[TXApp instance] getSettings] setFDKeychain:SettingsConst.CryptoKeys.PAY_TOKEN value:pay_token];
                        
                        // pay_nonce호출처리한다.
                        
                        NSString *month  = [expire substringToIndex:2];
                        NSString *year   = [expire substringFromIndex:3];
                        //NSString *month = [NSString stringWithFormat:@"20%@",[expire substringFromIndex:2]];
                        NSLog(@"%@,%@",year,month);
                        NSDictionary *propertyMap = @{
                                                      @"clientToken" : pay_token,
                                                      @"cardNumber" : no,
                                                      @"cvv"  : cvv,
                                                      @"month" : month,
                                                      @"year" : year,
                                                      @"mid" : @"UD413",
                                                      @"cseq" : cseq,
                                                      @"oseq" : oseq,
                                                      @"tip" : [NSString stringWithFormat:@"%ld",(long)tip],
                                                      };
                        
                        [self->model BTREE:propertyMap];
                        break;
                    }
                }
            }
            else {
                [appDelegate resetState2];
                
                // 상태 초기화
                [appDelegate resetTripInfo];
                [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_DISSMISS_CANCEL object:nil userInfo:nil];
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    //[self.navigationController popViewControllerAnimated:YES];
                    NSLog(@"NOTIFICATION_DISSMISS_CANCEL");
                    //[self.navigationController popToRootViewControllerAnimated:YES];
                });
            }
#endif
            
        } else {
//            if (descriptor.rlt != -1000 && descriptor.rlt != -999999) {
//                [self alertError:@"Error" message:descriptor.error];
//            }
            
            [appDelegate resetState2];
            
            // 상태 초기화
            [appDelegate resetTripInfo];
            [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_DISSMISS_CANCEL object:nil userInfo:nil];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                //[self.navigationController popViewControllerAnimated:YES];
                NSLog(@"NOTIFICATION_DISSMISS_CANCEL");
                //[self.navigationController popToRootViewControllerAnimated:YES];
            });
        }
    }
    else if([event.name isEqualToString:@"UD413"]) { // pay_nonce
        
        if(descriptor.success == true) {
            // 결제 완료처리 후 updateActivityState에서 상태 변경
            [appDelegate resetState2];
            
            // 상태 초기화
            [appDelegate resetTripInfo];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_DISSMISS_CANCEL object:nil userInfo:nil];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                //[self.navigationController popViewControllerAnimated:YES];
                NSLog(@"NOTIFICATION_DISSMISS_CANCEL");
                //[self.navigationController popToRootViewControllerAnimated:YES];
            });
        } else {
            if (descriptor.rlt != -1000 && descriptor.rlt != -999999) {
                //에러주석[self alertError:@"Error" message:descriptor.error];
            }
        }
    }
    else if([event.name isEqualToString:@"PD421"] || [event.name isEqualToString:@"PD410"]) { // charge rate
        
        // 상태 초기화
        [appDelegate resetTripInfo];
        
        if(descriptor.success == true) {
            [appDelegate resetState2];

            [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_DISSMISS_CANCEL object:nil userInfo:nil];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                //[self.navigationController popViewControllerAnimated:YES];
                NSLog(@"NOTIFICATION_DISSMISS_CANCEL");
                //[self.navigationController popToRootViewControllerAnimated:YES];
            });
        } else {
//            if (descriptor.rlt != -1000 && descriptor.rlt != -999999) {
//                [self alertError:@"Error" message:descriptor.error];
//            }
            
            // 저장실패면 화면 고정(화면 바꾸지 않는다)
//            [appDelegate resetState2];

//            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}

-(void)saveProfileImage:(UIImage*)image withCode:(NSString*)code{
    [self shadowProfileImage:image];
}

-(UIImage *)makeRoundedImage:(UIImage *) image
                      radius: (float) radius;
{
    CALayer *imageLayer = [CALayer layer];
    imageLayer.frame = CGRectMake(0, 0, image.size.width, image.size.height);
    imageLayer.contents = (id) image.CGImage;
    
    imageLayer.masksToBounds = YES;
    imageLayer.cornerRadius = radius;
    
    UIGraphicsBeginImageContext(image.size);
    [imageLayer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *roundedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return roundedImage;
}

-(void)shadowProfileImage:(UIImage*)image {
    _imgProfile.image = image;
    [Utils setCircleImage:_imgProfile];
    return;

//    CALayer* containerLayer = [CALayer layer];
//    containerLayer.shadowColor = [UIColor blackColor].CGColor;
//    containerLayer.shadowRadius = 10.f;
//    containerLayer.shadowOffset = CGSizeMake(0.f, 5.f);
//    containerLayer.shadowOpacity = 1.f;
    /*
    CALayer *sublayer = [CALayer layer];
    sublayer.backgroundColor = [UIColor blueColor].CGColor;
    sublayer.shadowOffset = CGSizeMake(0, 3);
    sublayer.shadowRadius = 15.0;
    sublayer.shadowColor = [UIColor blackColor].CGColor;
    sublayer.shadowOpacity = 0.8;
    sublayer.frame = CGRectMake(30, 30, 128, 128);
    sublayer.borderColor = [UIColor blackColor].CGColor;
    sublayer.borderWidth = 2.0;
    sublayer.cornerRadius = 10.0;
    [self.view.layer addSublayer:sublayer];
    
    CALayer *imageLayer = [CALayer layer];
    imageLayer.frame = sublayer.bounds;
    imageLayer.cornerRadius = 10.0;
    imageLayer.contents = (id) [UIImage imageNamed:@"BattleMapSplashScreen.png"].CGImage;
    imageLayer.masksToBounds = YES;
    [sublayer addSublayer:imageLayer];
    */
    /*
    UIImageView *pImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 200, 80, 80)];
    pImageView.frame = _imgProfile.frame;
    pImageView.image = image;
    pImageView.layer.cornerRadius = pImageView.frame.size.width / 2;
    pImageView.layer.borderWidth = 2.0f;
    pImageView.layer.borderColor = [UIColor whiteColor].CGColor;
    pImageView.clipsToBounds = YES;
    //pImageView.contentMode = UIViewContentModeScaleAspectFit;
    
    //pImageView.center = self.view.center;
    //pImageView.layer.cornerRadius = roundf(pImageView.frame.size.width/2.0);
    pImageView.layer.masksToBounds = YES;
    
    [containerLayer addSublayer:pImageView.layer];
    
    //[self.view addSubview:pImageView];
    [self.view.layer addSublayer:containerLayer];
     */
}

#pragma mark -
#pragma mark Validator
#pragma mark -
-(void) resignKeyboard
{
    [self.view endEditing:YES];
}

- (IBAction)validateAction:(id)sender
{
}

#pragma ValidatorDelegate - Delegate methods

- (void) preValidation
{
}

- (void)onSuccess
{
    
    
}

- (void)onFailure:(Rule *)failedRule
{
    
//    NSMutableDictionary *options = [@{kCRToastNotificationTypeKey               : @(CRToastTypeNavigationBar),
//                                      kCRToastNotificationPresentationTypeKey   : @(CRToastPresentationTypeCover),
//                                      kCRToastUnderStatusBarKey                 : @(YES),
//                                      kCRToastTextKey                           : failedRule.failureMessage,
//                                      kCRToastFontKey                           : [UIFont systemFontOfSize:14],
//                                      kCRToastTextAlignmentKey                  : @(NSTextAlignmentCenter),
//                                      kCRToastTimeIntervalKey                   : @(2),
//                                      kCRToastAnimationInTypeKey                : @(CRToastAnimationTypeGravity),
//                                      kCRToastAnimationOutTypeKey               : @(CRToastAnimationTypeGravity),
//                                      kCRToastAnimationInDirectionKey           : @(CRToastAnimationDirectionTop),
//                                      kCRToastAnimationOutDirectionKey          : @(CRToastAnimationDirectionTop),
//                                      kCRToastNotificationPreferredPaddingKey   : @(15)} mutableCopy];
//    [CRToastManager showNotificationWithOptions:options
//                                completionBlock:^{
//                                    NSLog(@"Completed");
//                                }];
    
    [WToast showWithText:failedRule.failureMessage duration:kWTShort roundedCorners:NO gravity:kWTGravityTop];
}

@end
