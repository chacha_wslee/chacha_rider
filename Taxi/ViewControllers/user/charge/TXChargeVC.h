//
//  TXChargeVC.h
//  Taxi
//

//

#import "TXUserVC.h"
#ifdef _WITZM
#elif defined _CHACHA
#else
#import "PPiFlatSegmentedControl.h"
#endif

@interface TXChargeVC : TXUserVC {
}

@property (nonatomic, strong) IBOutlet UIView* vBg;
@property (nonatomic, strong) IBOutlet UIView* vPay;

@property (nonatomic, strong) IBOutlet UIImageView* imgGrade;
@property (nonatomic, strong) IBOutlet UIImageView* imgProfile;

@property (nonatomic, strong) IBOutlet UIImageView* imgPattern;

@property (nonatomic, strong) IBOutlet UILabel* lbDate;
@property (nonatomic, strong) IBOutlet UILabel* lbPayTitle;

@property (nonatomic, strong) IBOutlet UILabel* lbPayTitle01;
@property (nonatomic, strong) IBOutlet UILabel* lbPayTitle02;

@property (nonatomic, strong) IBOutlet UILabel* lbPayTitle1;
@property (nonatomic, strong) IBOutlet UILabel* lbPayTitle2;
@property (nonatomic, strong) IBOutlet UILabel* lbPayTitle3;
@property (nonatomic, strong) IBOutlet UILabel* lbPayTitle4;
@property (nonatomic, strong) IBOutlet UILabel* lbPayTitle5;

@property (nonatomic, strong) IBOutlet UILabel* lbPay1;
@property (nonatomic, strong) IBOutlet UILabel* lbPay2;
@property (nonatomic, strong) IBOutlet UILabel* lbPay3;
@property (nonatomic, strong) IBOutlet UILabel* lbPay4;
@property (nonatomic, strong) IBOutlet UILabel* lbPay5;

@property (nonatomic, strong) IBOutlet UILabel* lbPay;
@property (nonatomic, strong) IBOutlet UIButton* btnPayAdd;

@property (nonatomic, strong) IBOutlet UILabel* lbName;
@property (nonatomic, strong) IBOutlet UILabel* lbGrade;
@property (nonatomic, strong) IBOutlet UILabel* lbPoint;
@property (nonatomic, strong) IBOutlet UILabel* lbVehicle;

@property (nonatomic, strong) IBOutlet UITextField* tfComment;

@property (nonatomic, strong) IBOutlet UILabel* lbBottom;

@property (nonatomic, strong) IBOutlet TXButton* btnSubmit;

// rate UI
@property (nonatomic, strong) IBOutlet UIView* vRate;
@property (nonatomic, strong) IBOutlet UILabel* lbRateText;
@property (nonatomic, strong) IBOutlet UILabel* lbRateText2;
@property (nonatomic, strong) IBOutlet TXButton* btnRateSubmit;

//
@property (nonatomic, assign) BOOL isManual;
@end
