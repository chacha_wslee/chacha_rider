//
//  TXChargeVC.m
//  Taxi
//

//

#import "TXChargeViewVC.h"
#import "utils.h"
#import "converter.h"
#import "LGAlertView.h"

//#import "NSString+FontAwesome.h"
#import <HCSStarRatingView/HCSStarRatingView.h>

#import "Validator.h"
#import "WToast.h"
//#import "CRToast.h"

@interface TXChargeViewVC () <UIGestureRecognizerDelegate, ValidatorDelegate> {
    int userId;
    NSArray *_msgList;
    
    BOOL isCanceled;
#ifdef _WITZM
#elif defined _CHACHA
#else
    PPiFlatSegmentedControl *segmented;
#endif
}

@property (strong, nonatomic) IBOutlet HCSStarRatingView *ratingView;
@property (strong, nonatomic) LGAlertView *securityAlertView;
@end

@implementation TXChargeViewVC {
    NSInteger rating;
    NSInteger tip;
}

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    
    if(self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        rating = 5;
    }
    
    return self;
}

-(void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    CGRect frame = _vPay.frame;
    frame.origin.y = _vBg.frame.origin.y + 50 - frame.size.height;//67;//_lbBottom.frame.origin.y - 20 - frame.size.height;
    _vPay.frame = frame;
    _btnPayAdd.tag = 200;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    return NO;
}

#pragma mark - Configure

-(void)configureStyles {
    [super configureStyles];
    
//    if (appDelegate.isDriverMode) {
//        [super navigationType01X:[UIImage imageNamed:@"btn_back"] centerText:@"Rating & Tip"];
//    }
//    else {
//        [super navigationType01X:[UIImage imageNamed:@"btn_back"] centerText:@"Rating"];
//    }
    //[super navigationType01X:nil centerText:LocalizedStr(@"Map.Drive.Rating.title")];
}

-(void)configure {
    [super configure];
    
    isCanceled = NO;
    
    NSDictionary *drive_pickup = [_chargeDic objectForKey:@"drive_pickup"];
    NSDictionary *drive_trip   = [_chargeDic objectForKey:@"drive_trip"];
    NSDictionary *drive_rating = [_chargeDic objectForKey:@"drive_rating"];
    
    
    // pickup 취소
    if ([Utils isDictionary:drive_pickup]) {
        // 취소
        if ([drive_pickup[@"state"] intValue]<0) {
            isCanceled = YES;
        }
    }
    // trip 취소
    if ([Utils isDictionary:drive_trip]) {
        // 취소
        if ([drive_trip[@"state"] intValue]<0) {
            isCanceled = YES;
        }
    }
    
    UIView *n = [super navigationView];
    //NSInteger _y = n.frame.origin.y + n.frame.size.height;
    n.hidden = YES;
    
    // 2. init event
    _msgList = @[
                 @[@"UR601",@""], // rider profile
                 @[@"UR602",@""], // driver profile
                 
                 @[@"PP601",@""], // rider profile
                 @[@"PP602",@""], // driver profile
                 ];
    
//    [self registerEventListeners];

    _imgProfile.image = nil;
    if (![[_chargeDic objectForKey:@"picImage"] isEqual:[NSNull null]]) {
        [self shadowProfileImage:_chargeDic[@"picImage"]];
    }
    else {
        [self shadowProfileImage:[UIImage imageNamed:@"ic_user_white"]];
    }
    
    //[self showBusyIndicator:@"Requesting Charge ... "];
    if (appDelegate.isDriverMode) {
        // 3. call api
        //[self->model UR601];
        [self configureDriver];
        [self configureDriver2];
    }
    else {
        // 3. call api
        //[self->model PP602];
        [self configureRider];
        [self configureRider2];
    }
    
    // 취소인 경우에 대한 처리가 필요
    if (isCanceled) {
        _lbPayTitle.text= LocalizedStr(@"CHARGE_Rating_Cancel.PayTitle.text");

        _ratingView.hidden = YES;
        _ratingView.value = 0;
        rating = 0;
    }
    else {
        _lbPayTitle.text= LocalizedStr(@"CHARGE_Rating_Open.PayTitle.text");
        
#ifndef _DRIVER_MODE
        _ratingView.hidden = NO;
        _ratingView.value = 0;
        rating = 0;
        
        // rating
        if ([Utils isDictionary:drive_rating]) {
            _ratingView.value = [drive_rating[@"rate"] intValue];
            rating = [drive_rating[@"rate"] intValue];
        }
#endif
        
    }
//    }
    
    
    _imgPattern.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"img_fare_line"]];
    
    _vBg.backgroundColor = UIColorDefault;
    //self.view.backgroundColor = UIColorLabelText;
    _lbName.textColor = HEXCOLOR(0x4a4a4aff);
    _lbGrade.textColor = UIColorDefault;
    _lbVehicle.textColor = [UIColor blackColor];
    //[Utils layerButton:_btnSubmit];
    
    CGRect frame = _btnSubmit.frame;
    frame.origin.y -= kBottomBottonSafeArea;
    _btnSubmit.frame = frame;
    
    [_btnSubmit setTitleColor:UIColorButtonText forState:UIControlStateNormal];
    _btnSubmit.backgroundColor = UIColorDefault2;
    [_btnSubmit setTitle:LocalizedStr(@"Map.Payment.BTN.submit") forState:UIControlStateNormal];
    
    [_btnSubmit sendSubviewToBack:_vPay];
    
//#ifdef _CHACHA
    self.view.backgroundColor = HEXCOLOR(0xeeeeeeff);
#ifdef _DRIVER_MODE
    _lbBottom.text = LocalizedStr(@"DRIVER.CHARGE_Rating_Open.Bottom.text");
#else
    _lbBottom.text = LocalizedStr(@"CHARGE_Rating_Open.Bottom.text");
#endif
    _lbBottom.textColor = UIColorGray;
    
    frame = _lbBottom.frame;
    frame.origin.y -= kBottomBottonSafeArea;
    _lbBottom.frame = frame;
    
    _lbDate.textColor = UIColorGray;
    _lbPayTitle.textColor = HEXCOLOR(0x333333FF);
    _lbPay.textColor = UIColorLabelTextColor;
    
    [_btnPayAdd setTitleColor:UIColorDefault2 forState:UIControlStateNormal];
    [_btnPayAdd setTitle:LocalizedStr(@"CHARGE_Rating_Open.BTN.Show.text") forState:UIControlStateNormal];
    
    _lbPayTitle01.textColor = HEXCOLOR(0x333333FF);
    _lbPayTitle02.textColor = HEXCOLOR(0x666666FF);
#ifdef _DRIVER_MODE
    _lbPayTitle1.text = LocalizedStr(@"CHARGE_Rating_Open.PayTitle2.text");
    _lbPayTitle1.textColor = HEXCOLOR(0x111111FF);
    _lbPayTitle4.text = LocalizedStr(@"CHARGE_Rating_Open.PayTitle3.text");
    _lbPayTitle4.textColor = HEXCOLOR(0x111111FF);
    _lbPayTitle5.text = LocalizedStr(@"CHARGE_Rating_Open.PayTitle6.text");
    _lbPayTitle5.textColor = HEXCOLOR(0x111111FF);
    
    _lbPay1.textColor = HEXCOLOR(0x111111FF);
    _lbPay4.textColor = HEXCOLOR(0x111111FF);
    _lbPay5.textColor = HEXCOLOR(0x111111FF);
    
//    _lbPayTitle4.text = LocalizedStr(@"CHARGE_Rating_Open.PayTitle2.text");
//    _lbPayTitle4.textColor = HEXCOLOR(0x111111FF);
//    _lbPayTitle5.text = LocalizedStr(@"CHARGE_Rating_Open.PayTitle3.text");
//    _lbPayTitle5.textColor = HEXCOLOR(0x111111FF);
//
//    _lbPay4.textColor = HEXCOLOR(0x111111FF);
//    _lbPay5.textColor = HEXCOLOR(0x111111FF);
#else
    _lbPayTitle1.text = LocalizedStr(@"CHARGE_Rating_Open.PayTitle1.text");
    _lbPayTitle1.textColor = HEXCOLOR(0x111111FF);
    _lbPayTitle2.text = LocalizedStr(@"CHARGE_Rating_Open.PayTitle2.text");
    _lbPayTitle2.textColor = HEXCOLOR(0x111111FF);
    _lbPayTitle3.text = LocalizedStr(@"CHARGE_Rating_Open.PayTitle3.text");
    _lbPayTitle3.textColor = HEXCOLOR(0x111111FF);
    _lbPayTitle4.text = LocalizedStr(@"CHARGE_Rating_Open.PayTitle4.text");
    _lbPayTitle4.textColor = HEXCOLOR(0x111111FF);
    _lbPayTitle5.text = LocalizedStr(@"CHARGE_Rating_Open.PayTitle5.text");
    _lbPayTitle5.textColor = HEXCOLOR(0x111111FF);
    
    _lbPay1.textColor = HEXCOLOR(0x111111FF);
    _lbPay2.textColor = HEXCOLOR(0x111111FF);
    _lbPay3.textColor = HEXCOLOR(0x111111FF);
    _lbPay4.textColor = HEXCOLOR(0x111111FF);
    _lbPay5.textColor = UIColorDefault;
    
    //_ratingView.enabled = NO;
#endif
    
    
//#endif
}

-(void) configureConfig {

    
}

-(void) configureRider {
    // 1. init var
    _lbDate.text    = @"";
    _lbPay.text     = @"";
    _lbName.text    = @"";
    _lbGrade.text   = @"";
    _lbPoint.text   = @"";
    _lbVehicle.text = @"";
    _lbPayTitle01.text = @"";
    _lbPayTitle02.text = @"";
    
    _lbGrade.hidden = YES;
    _lbVehicle.hidden = YES;
//    CGRect rect = _lbVehicle.frame;
//    rect.origin.y = _lbGrade.frame.origin.y;
//    _lbVehicle.frame = rect;
}

-(void) configureRider2 {
    
    NSDictionary *user   = [_chargeDic objectForKey:@"drive_driver"];
    NSDictionary *charge = [_chargeDic objectForKey:@"drive_charge"];
    NSDictionary *pickup = [_chargeDic objectForKey:@"drive_pickup"];
    NSDictionary *trip   = [_chargeDic objectForKey:@"drive_trip"];
    NSString *service_name = @"";
    
    if ([Utils isDictionary:charge]) {
        //_lbPay.text     = CURRENCY_FORMAT(charge[@"fare_trip"]);
        NSInteger sum   = [charge[@"fare_sum"] intValue];
        NSInteger extra = [charge[@"fare_extra"] intValue];
        NSInteger discount = [charge[@"fare_discount"] intValue];
        NSInteger payed = [charge[@"fare_payed"] intValue];
        
        NSInteger pay1 = sum+extra;
        NSInteger pay2 = round((sum+extra)*0.05);
        NSInteger pay3 = round((sum+extra)*0.95);
        NSInteger pay4 = discount;
        
        NSString *p1 = [NSString stringWithFormat:@"%ld",(long)pay1];
        NSString *p2 = [NSString stringWithFormat:@"%ld",(long)pay2];
        NSString *p3 = [NSString stringWithFormat:@"%ld",(long)pay3];
        NSString *p4 = [NSString stringWithFormat:@"%ld",(long)pay4];
        NSString *p  = [NSString stringWithFormat:@"%ld",(long)payed];
        
        _lbPay1.text     = CURRENCY_FORMAT(p1);
        _lbPay2.text     = CURRENCY_FORMAT(p2);
        _lbPay3.text     = CURRENCY_FORMAT(p3);
        _lbPay4.text     = CURRENCY_FORMAT(p4);
        _lbPay5.text     = CURRENCY_FORMAT(p);
        _lbPay.text      = CURRENCY_FORMAT(p);
    }
    else {
        _lbPay1.text     = CURRENCY_FORMAT(@"0");
        _lbPay2.text     = CURRENCY_FORMAT(@"0");
        _lbPay3.text     = CURRENCY_FORMAT(@"0");
        _lbPay4.text     = CURRENCY_FORMAT(@"0");
        _lbPay5.text     = CURRENCY_FORMAT(@"0");
        _lbPay.text      = CURRENCY_FORMAT(@"0");
    }
    
    if (isCanceled) {
        NSDictionary *pickup = [_chargeDic objectForKey:@"drive_pickup"];
        
        _lbDate.text    = StringFormatDate2Date2(pickup[@"reg_date"]);
        
        _lbPayTitle01.text    = StringFormatDate2DateYYYYMM(pickup[@"reg_date"]);
        _lbPayTitle02.text    = StringFormatDate2DateHHMM(pickup[@"reg_date"]);
    }
    else {
        _lbDate.text    = StringFormatDate2Date2(trip[@"reg_date"]);
        
        _lbPayTitle01.text    = StringFormatDate2DateYYYYMM(trip[@"reg_date"]);
        _lbPayTitle02.text    = StringFormatDate2DateHHMM(trip[@"reg_date"]);
    }
    
    if ([Utils isDictionary:pickup]) {
        service_name = pickup[@"service_name"];
    }
    else if ([Utils isDictionary:trip]) {
        service_name = trip[@"service_name"];
    }
    
    //_lbGrade.text = [Utils labelStringUserInfo:[user objectForKey:@"trips"] rate:[user objectForKey:@"rate"]];
    _lbName.text = [Utils nameToString:[user objectForKey:@"name"]];
#ifdef _DRIVER_MODE
    _lbName.text = [Utils nameToString:[user objectForKey:@"name"]];
#else
    _lbName.text = [NSString stringWithFormat:@"%@ / %@", [Utils nameToString:[user objectForKey:@"name"]], [Utils nameToString:service_name]];
    _lbName.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:14];
#endif
    
}

-(void) configureDriver {
    // 1. init var
    _lbDate.text    = @"";
    _lbPay.text     = @"";
    _lbPayTitle.text     = @"";
    _lbName.text    = @"";
    _lbGrade.text   = @"";
    _lbPoint.text   = @"";
    _lbVehicle.text = @"";
    _lbPayTitle01.text = @"";
    _lbPayTitle02.text = @"";
    
    [_btnPayAdd setTitle:@"" forState:UIControlStateNormal];
}

// for driver
-(void) configureDriver2 {
    NSDictionary *user   = [_chargeDic objectForKey:@"drive_rider"];
    NSDictionary *charge = [_chargeDic objectForKey:@"drive_charge"];
    NSDictionary *trip   = [_chargeDic objectForKey:@"drive_trip"];
    
    if ([Utils isDictionary:charge]) {
        //_lbPay.text     = CURRENCY_FORMAT(charge[@"fare_trip"]);
        NSInteger sum   = [charge[@"fare_sum"] intValue];
        NSInteger extra = [charge[@"fare_extra"] intValue];
        
        NSInteger pay1 = sum+extra;
        NSInteger pay2 = round((sum+extra)*0.05);
        NSInteger pay3 = round((sum+extra)*0.75);
        NSInteger pay4 = round((sum+extra)*0.20);
        
        NSString *p1 = [NSString stringWithFormat:@"%ld",(long)pay1];
        NSString *p2 = [NSString stringWithFormat:@"%ld",(long)pay2];
        NSString *p3 = [NSString stringWithFormat:@"%ld",(long)pay3];
        NSString *p4 = [NSString stringWithFormat:@"%ld",(long)pay4];
//        NSString *p  = [NSString stringWithFormat:@"%ld",(long)extra];
        
        _lbPay1.text     = CURRENCY_FORMAT(p2);
        _lbPay4.text     = CURRENCY_FORMAT(p3);
        _lbPay5.text     = CURRENCY_FORMAT(p4);
        _lbPay.text      = CURRENCY_FORMAT(p1);
        
//        _lbPay4.text     = CURRENCY_FORMAT(p2);
//        _lbPay5.text     = CURRENCY_FORMAT(p3);
//        _lbPay.text      = CURRENCY_FORMAT(p1);
    }
    else {
        _lbPay1.text     = CURRENCY_FORMAT(@"0");
        _lbPay4.text     = CURRENCY_FORMAT(@"0");
        _lbPay5.text     = CURRENCY_FORMAT(@"0");
        _lbPay.text      = CURRENCY_FORMAT(@"0");
    }
    
    if (isCanceled) {
        NSDictionary *pickup = [_chargeDic objectForKey:@"drive_pickup"];
        
        _lbDate.text    = StringFormatDate2Date2(pickup[@"reg_date"]);
        _lbPayTitle01.text    = StringFormatDate2DateYYYYMM(pickup[@"reg_date"]);
        _lbPayTitle02.text    = StringFormatDate2DateHHMM(pickup[@"reg_date"]);
    }
    else {
        _lbDate.text    = StringFormatDate2Date2(trip[@"reg_date"]);
        _lbPayTitle01.text    = StringFormatDate2DateYYYYMM(trip[@"reg_date"]);
        _lbPayTitle02.text    = StringFormatDate2DateHHMM(trip[@"reg_date"]);
    }
    
    //_lbPayTitle.text= LocalizedStr(@"CHARGE_Rating_Open.PayTitle.text");
    [_btnPayAdd setTitle:LocalizedStr(@"CHARGE_Rating_Open.BTN.Show.text") forState:UIControlStateNormal];
        
    //_lbGrade.text = [Utils labelStringUserInfo:[user objectForKey:@"trips"] rate:[user objectForKey:@"rate"]];
    _lbName.text = [Utils nameToString:[user objectForKey:@"name"]];
    
}

-(void) registerEventListeners {
    for (NSArray *msg in _msgList) {
        [self->model addEventListener:self forEvent:[msg objectAtIndex:0] eventParams:nil];
    }
}

-(void) removeEventListeners {
    for (NSArray *msg in _msgList) {
        [self->model removeEventListener:self forEvent:[msg objectAtIndex:0]];
    }
}

#pragma mark - Action
-(IBAction)onNaviButtonClick:(id)sender {
    UIButton *btn = (UIButton*)sender;
    NSLog(@"onNaviButtonClick");
    //[super onNaviButtonClick:sender];
    
    if (btn.tag == 1100) {
//        [appDelegate resetState2];
        [self.navigationController popToRootViewControllerAnimated:YES];
        /*
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_DISSMISS_CANCEL object:nil userInfo:nil];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            //[self.navigationController popViewControllerAnimated:YES];
            NSLog(@"NOTIFICATION_DISSMISS_CANCEL");
            [self.navigationController popToRootViewControllerAnimated:YES];
        });
         */
    }
    else if (_btnPayAdd.tag == 201) {
        _btnPayAdd.userInteractionEnabled = NO;
        // show pay off
        [UIView animateWithDuration:0.5
                         animations:^{
                             CGRect frame = _vPay.frame;
#ifdef _DRIVER_MODE
                             frame.origin.y = _vBg.frame.origin.y + 50 - frame.size.height;//67;//_lbBottom.frame.origin.y - 20 - frame.size.height;
#else
                             frame.origin.y = _vBg.frame.origin.y + 50 - frame.size.height;//67;//_lbBottom.frame.origin.y - 20 - frame.size.height;
#endif
                             _vPay.frame = frame;
                             
                             _imgGrade.transform = CGAffineTransformMakeRotation(0);
                         }
                         completion:^(BOOL finished) {
                             _btnPayAdd.userInteractionEnabled = YES;
                         }];
        _btnPayAdd.tag = 200;
    }
    else if (_btnPayAdd.tag == 200) {
        _btnPayAdd.userInteractionEnabled = NO;
        // show pay on
        [UIView animateWithDuration:0.5
                         animations:^{
                             CGRect frame = _vPay.frame;
#ifdef _DRIVER_MODE
                             frame.origin.y = _vBg.frame.origin.y - 100 - 120 + 40;//- 120;//67;//_lbBottom.frame.origin.y - 20 - frame.size.height;
#else
                             frame.origin.y = _vBg.frame.origin.y - 100;//67;//_lbBottom.frame.origin.y - 20 - frame.size.height;
#endif
                             _vPay.frame = frame;
                             
                             //_imgGrade.transform = CGAffineTransformMakeRotation(0);
                             _imgGrade.transform = CGAffineTransformMakeRotation(M_PI_2*2);
                         }
                         completion:^(BOOL finished) {
                             _btnPayAdd.userInteractionEnabled = YES;
                         }];
        _btnPayAdd.tag = 201;
    }
}


#pragma mark - onEvent

#pragma mark - IBAction
-(IBAction)onSubmit:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - onEvent
-(void)onEvent:(TXEvent *)event eventParams:(id)subscriptionParams {
    
    TXResponseDescriptor * descriptor = [event getEventProperty:TXEvents.Params.DESCRIPTOR];
    TXRequestObj *request = [event getEventProperty:TXEvents.Params.REQUEST];
    
    [self hideBusyIndicator];
    if([event.name isEqualToString:@"UR601"] ||
       [event.name isEqualToString:@"UR602"] ||
       [event.name isEqualToString:@"PP601"] ||
       [event.name isEqualToString:@"PP602"]
       ) {
    
        if(descriptor.success == true) {
            NSString     *response = [[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding];
            NSDictionary *result   = getJSONObj(response);
            
            NSString *pictureData              = [result valueForKey:@"picture"];
            
            if ([pictureData length]) {
                
//                NSData *data = [[NSData alloc] initWithBase64EncodedString:pictureData options:0];
//                UIImage *image = [[UIImage alloc] initWithData:data];
                UIImage *image = [Utils decodeBase64ToImage:pictureData];
                // Store image
                //[appDelegate.cache storeObject:event forKey:request.reqUrl];
                
                [self saveProfileImage:image withCode:event.name];
            }
            else {
                [self saveProfileImage:[UIImage imageNamed:@"ic_user_white"] withCode:event.name];
            }
            
        } else {
            //[self saveProfileImage:[UIImage imageNamed:@"ic_user_white"] withCode:event.name];
        }
    }
}

-(void)saveProfileImage:(UIImage*)image withCode:(NSString*)code{
    
    [self shadowProfileImage:image];
}

-(UIImage *)makeRoundedImage:(UIImage *) image
                      radius: (float) radius;
{
    CALayer *imageLayer = [CALayer layer];
    imageLayer.frame = CGRectMake(0, 0, image.size.width, image.size.height);
    imageLayer.contents = (id) image.CGImage;
    
    imageLayer.masksToBounds = YES;
    imageLayer.cornerRadius = radius;
    
    UIGraphicsBeginImageContext(image.size);
    [imageLayer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *roundedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return roundedImage;
}

-(void)shadowProfileImage:(UIImage*)image {
    _imgProfile.image = image;
    [Utils setCircleImage:_imgProfile];
}

#pragma mark -
#pragma mark Validator
#pragma mark -
-(void) resignKeyboard
{
    [self.view endEditing:YES];
}

- (IBAction)validateAction:(id)sender
{
}

#pragma ValidatorDelegate - Delegate methods

- (void) preValidation
{
}

- (void)onSuccess
{
    
    
}

- (void)onFailure:(Rule *)failedRule
{
    [WToast showWithText:failedRule.failureMessage duration:kWTShort roundedCorners:NO gravity:kWTGravityTop];
}

@end
