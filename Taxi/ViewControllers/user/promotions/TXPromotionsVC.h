//
//  MenuViewController.h
//  SlideMenu
//

//

#import <UIKit/UIKit.h>
#import "TXBaseViewController.h"

@interface TXPromotionsVC : TXBaseViewController <UITableViewDelegate, UITableViewDataSource> {

}

@property (nonatomic, strong) NSString *cellIdentifierAdd;
@property (nonatomic, strong) NSString *cellIdentifierMy;
@property (nonatomic, strong) NSString *cellIdentifierBook;
@property (nonatomic, strong) IBOutlet UITableView *tableView;

@property (nonatomic, strong) IBOutlet UIView *tableHeader;
@property (nonatomic, strong) IBOutlet UIButton *btnBook;
@property (nonatomic, strong) IBOutlet UIButton *btnMy;

@property (assign, nonatomic) BOOL isMyCoupon;

@end
