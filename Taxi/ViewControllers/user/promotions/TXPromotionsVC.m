//
//  MenuViewController.m
//  SlideMenu
//

//
#import "UITableView+UITableViewAddition.h"
#import "TXPromotionsVC.h"
#import "TXPromotionsCell.h"
#import "RTLabel.h"
#import "LGAlertView.h"
#import "converter.h"

#import "Validator.h"
#import "WToast.h"

@import IQKeyboardManager;

@interface TXPromotionsVC() <ValidatorDelegate>{
    NSMutableArray *items;
    NSArray *myitems;
    NSArray *_msgList;
}

@end

@implementation TXPromotionsVC {
    //NSMutableDictionary *propertyMap;
}
@synthesize cellIdentifierAdd;
@synthesize cellIdentifierMy;
@synthesize cellIdentifierBook;

-(void)viewDidLoad {
    
    [super viewDidLoad];
    
    [[IQKeyboardManager sharedManager] setEnable:NO];
    
//    NSArray *user;
//    if (appDelegate.isDriverMode) {
////        user = [appDelegate.dicDriver objectForKey:@"provider_code"];
//    }
//    else {
//        user = [appDelegate.dicRider objectForKey:@"user_codes"];
//    }
//    // ? 없으면 없다고 보여준다. 화면에
//    if (![user count]) {
//        //
//        self->items = [NSMutableArray arrayWithCapacity:0];
//    }
//    else {
//        self->items = [NSMutableArray arrayWithCapacity:1];
//        [items addObject:user];
//    }
    
    [super configureBottomList];
    UIView *n = [super navigationView];
    NSInteger _y = n.frame.origin.y + n.frame.size.height;
    
    // table header select
    CGRect rect = self.tableHeader.frame;
    rect.origin.x = 0;
    rect.origin.y = _y;
    rect.size.width = self.view.bounds.size.width;
    self.tableHeader.frame = rect;
    _y = self.tableHeader.frame.origin.y + self.tableHeader.frame.size.height;
    [self.view addSubview:self.tableHeader];
    
    //
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, _y, self.view.bounds.size.width, self.view.bounds.size.height-_y)
                                                  style:UITableViewStylePlain];
    self.tableView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:self.tableView];
    
    self.cellIdentifierMy = @"TXPromotionsMyCell";
    [self.tableView registerNib:[UINib nibWithNibName:@"TXPromotionsMyCell" bundle:nil] forCellReuseIdentifier:self.cellIdentifierMy];

    self.cellIdentifierAdd = @"TXPromotionsAddCell";
    [self.tableView registerNib:[UINib nibWithNibName:@"TXPromotionsAddCell" bundle:nil] forCellReuseIdentifier:self.cellIdentifierAdd];
    
    self.cellIdentifierBook = @"TXPromotionsBookCell";
    [self.tableView registerNib:[UINib nibWithNibName:@"TXPromotionsBookCell" bundle:nil] forCellReuseIdentifier:self.cellIdentifierBook];
    
    self.tableView.separatorColor = [UIColor clearColor];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.opaque = NO;
    self.tableView.backgroundColor = [UIColor clearColor];
    
    UILabel *label = (UILabel*)[[super navigationView] viewWithTag:1111];
    label.tag = 2001;
    //label.text = LocalizedStr(@"Menu.Promotion.Header.title");
    
//    self.tableView.tableHeaderView = ({
//        
//        UIView *view = [Utils addLabelView:self.view underView:nil dim:0];
//        
//        UILabel *label = [view viewWithTag:11];
//        label.text = LocalizedStr(@"Menu.Promotion.Header.title");
//        label.tag = 2001;
//        //[label sizeToFit];
//        label.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
//        
//        view;
//    });
    
    //self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, kBasicHeight + kBasicMargin*2)];
//    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, kBasicHeight + kBasicMargin*2 - kBottomButtonHeight)];
    self.tableView.tableFooterView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
//    [self.tableView.tableFooterView addSubview:({
//        UIView *view = [Utils addButtonBasic:self.view underView:nil dim:kBasicMargin];
//        
//        view.backgroundColor = UIColorLabelBG;
//        UIButton *button = [view viewWithTag:11];
//        button.tag = 999;
//        [button setTitle:LocalizedStr(@"Menu.Promotion.BTN.add") forState:UIControlStateNormal];
//        [button addTarget:self action:@selector(addButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
//        //view.autoresizingMask = UIViewAutoresizingFlexibleHeight;
//        
//        view;
//    })];
    /*
    UIButton *btn = [Utils addBottomButton:LocalizedStr(@"Menu.Promotion.BTN.add") target:self selector:@selector(addButtonPressed:) color:YES];
    [self.view addSubview:btn];
    //[self.tableView setSeparatorInset:UIEdgeInsetsZero];
     */
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self removeEventListeners];
}

-(void)dealloc {
    [self removeEventListeners];
}

#pragma mark - Configure

-(void)configureStyles {
    [super configureStyles];
    
    if (appDelegate.isDriverMode) {
        [super navigationType01X:[UIImage imageNamed:@"btn_back"] centerText:LocalizedStr(@"Menu.Promotion.title")];
    }
    else {
        [super navigationType01X:[UIImage imageNamed:@"btn_back"] centerText:LocalizedStr(@"Menu.Promotion.title")];
    }
}

-(void)configure {
    [super configure];
    
    // 2. init event
    _msgList = @[
                 @[@"UR303",@""], // rider code query
                 @[@"UR323",@""], // rider code reg
                 
                 @[@"UR304",@""], // rider book query
                 @[@"UR324",@""], // rider book down
                 ];
    
    [self registerEventListeners];
    
    [self toggleSelectTable:NO];
}

-(void) registerEventListeners {
    for (NSArray *msg in _msgList) {
        [self->model addEventListener:self forEvent:[msg objectAtIndex:0] eventParams:nil];
    }
}

-(void) removeEventListeners {
    for (NSArray *msg in _msgList) {
        [self->model removeEventListener:self forEvent:[msg objectAtIndex:0]];
    }
}

#pragma mark - Action
-(IBAction)onNaviButtonClick:(id)sender {
    UIButton *btn = (UIButton*)sender;
    NSLog(@"onNaviButtonClick:%ld",btn.tag);
    //[super onNaviButtonClick:sender];
    
    if (btn.tag == 1100) {
        [self removeEventListeners];
        [self.navigationController popViewControllerAnimated:YES];
    }
    else if (btn.tag == 700) { // book
        btn.tag = 770;
        [self toggleSelectTable:NO];
    }
    else if (btn.tag == 701) { // my
        btn.tag = 771;
        [self toggleSelectTable:YES];
    }
}

- (void) resetBtn
{
    if (self.isMyCoupon) {
        _btnBook.tag = 700;
    }
    else {
        _btnMy.tag = 701;
    }
    
    NSLog(@"----------------------------------->");
    [self hideBusyIndicator];
}

- (void)toggleSelectTable:(BOOL)flag {
    [self showBusyIndicator:@"Requesting ... "];
    self.isMyCoupon = flag;
    if (flag) { // my
        UIView *line1 = [self.tableHeader viewWithTag:710];
        line1.backgroundColor = HEXCOLOR(0xd8d8d8FF);
        
        UIView *line2 = [self.tableHeader viewWithTag:711];
        line2.backgroundColor = HEXCOLOR(0x333333FF);
        
        
        // 3. call api
        [self showBusyIndicator:@"Requesting ... "];
        [self->model requsetAPI:@"UR303" property:nil];
    }
    else { // book
        UIView *line1 = [self.tableHeader viewWithTag:710];
        line1.backgroundColor = HEXCOLOR(0x333333FF);
        
        UIView *line2 = [self.tableHeader viewWithTag:711];
        line2.backgroundColor = HEXCOLOR(0xd8d8d8FF);
        
        [self showBusyIndicator:@"Requesting ... "];
        [self->model requsetAPI:@"UR304" property:nil];
    }
}

#pragma mark - IBAction
-(void)addButtonPressed:(NSDictionary*)dic {

    LGAlertView *alertView = [[LGAlertView alloc] initWithTextFieldsAndTitle:LocalizedStr(@"Menu.Promotion.Add.title")
                                                                     message:nil
                                                          numberOfTextFields:1
                                                      textFieldsSetupHandler:^(UITextField *textField, NSUInteger index)
                              {
                                  if (index == 0) {
                                      textField.placeholder = @"";
                                      textField.text = @"";
                                  }
                                  
                                  textField.tag = index;
//                                  textField.delegate = self;
                                  textField.enablesReturnKeyAutomatically = YES;
                                  textField.autocapitalizationType = NO;
                                  textField.autocorrectionType = NO;
                                  textField.clearButtonMode = UITextFieldViewModeNever;
                                  textField.textAlignment = NSTextAlignmentCenter;
                              }
                                                                buttonTitles:@[LocalizedStr(@"Button.OK")]
                                                           cancelButtonTitle:LocalizedStr(@"Button.CANCEL")
                                                      destructiveButtonTitle:nil
                                                               actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                                                                   UITextField *secondTextField = alertView.textFieldsArray[index];
                                                                   
                                                                   [self resignKeyboard];
                                                                   Validator *validator = [[Validator alloc] init];
                                                                   validator.delegate   = self;
                                                                   //
                                                                   [validator putRule:[Rules checkRange:NSMakeRange(4, 20) withFailureString:LocalizedStr(@"Validator.Coupon.text1") forTextField:secondTextField]];
                                                                   //[validator putRule:[Rules checkIfAlphaNumericWithFailureString:LocalizedStr(@"Validator.Coupon.text2") forTextField:secondTextField]];
                                                                   
                                                                   if (![validator validateForResult]) {
                                                                       [alertView.firstButton setSelected:NO];
                                                                       return;
                                                                   }
                                                                   
                                                                   // send
                                                                   [self->model UR323:secondTextField.text];
                                                                   
                                                                   [alertView dismissAnimated:YES completionHandler:nil];
                                                                   //NSLog(@"actionHandler, %@, %lu, %@", title, (long unsigned)index, secondTextField.text);
                                                                   
                                                               }
                                                               cancelHandler:^(LGAlertView *alertView) {
                                                                   [alertView dismissAnimated:YES completionHandler:nil];
                                                                   
                                                               }
                                                          destructiveHandler:^(LGAlertView *alertView) {
                                                              
                                                          }];
    //[alertView setButtonAtIndex:0 enabled:NO];
    [Utils initAlertButtonColor:alertView];
    [alertView showAnimated:YES completionHandler:nil];
    alertView.windowLevel = LGAlertViewWindowLevelBelowStatusBar;
    [alertView setDismissOnAction:NO];
}


#pragma mark - UITableView Delegate & Datasrouce -
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [tableView setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [tableView setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
    UIView *seperatorView = [[UIView alloc] initWithFrame:CGRectMake(10, cell.frame.size.height - 1, cell.frame.size.width - 10*2, 1)];
    seperatorView.backgroundColor = UIColorTableSeperator;
    [cell addSubview:seperatorView];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    //
    
    if (self.isMyCoupon) {
        return 130;
    }
    else {
        if (indexPath.row==0) {
            return 124;
        }
        else {
            return 130;
        }
    }
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self->items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = @"";
    
    if (self.isMyCoupon) {
        //Get the top cell
        CellIdentifier = @"TXPromotionsMyCell";
    } else {
        if (indexPath.row==0) {
            CellIdentifier = @"TXPromotionsAddCell";
        }
        else {
            CellIdentifier = @"TXPromotionsBookCell";
        }
    }
    
	TXPromotionsCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];

    if (self.isMyCoupon) {
        NSDictionary *dic = self->items[indexPath.row];
        
        NSString *lb1 = [NSString stringWithFormat:@"%@",
                         [dic valueForKey:@"title"]
                         ];
        NSString *lb2 = [NSString stringWithFormat:LocalizedStr(@"Menu.Promotion.Type0.text"),
                         CURRENCY_FORMAT2([dic valueForKey:@"discount"])
                         ];
        // 1:% 0:won
        if ([[Utils nullToString:dic[@"discounttype"]] isEqualToString:@"1"]) {
            lb2 = [NSString stringWithFormat:LocalizedStr(@"Menu.Promotion.Type1.text"),
                   CURRENCY_FORMAT2([dic valueForKey:@"discount"])
                   ];
        }
        NSString *lb3 = [NSString stringWithFormat:LocalizedStr(@"Menu.Promotion.Period.text"),
                         StringFormatCouponEndDate([dic valueForKey:@"end_date"])
                         ];
        
        ((UILabel *)[cell.contentView viewWithTag:901]).text = lb1;
        ((RTLabel *)[cell.contentView viewWithTag:902]).text = lb2;
        ((RTLabel *)[cell.contentView viewWithTag:903]).text = lb3;
        ((RTLabel *)[cell.contentView viewWithTag:903]).textColor = HEXCOLOR(0x333333FF);
    }
    else {
        if (indexPath.row!=0) {
            NSDictionary *dic = self->items[indexPath.row];
            
            NSString *lb1 = [NSString stringWithFormat:@"%@",
                             [dic valueForKey:@"title"]
                             ];
            NSString *lb2 = [NSString stringWithFormat:LocalizedStr(@"Menu.Promotion.Type0.text"),
                             CURRENCY_FORMAT2([dic valueForKey:@"discount"])
                             ];
            // 1:% 0:won
            if ([[Utils nullToString:dic[@"discounttype"]] isEqualToString:@"1"]) {
                lb2 = [NSString stringWithFormat:LocalizedStr(@"Menu.Promotion.Type1.text"),
                       CURRENCY_FORMAT2([dic valueForKey:@"discount"])
                       ];
            }
            
            NSString *lb3 = [NSString stringWithFormat:LocalizedStr(@"Menu.Promotion.Remain.text"),
                             [dic valueForKey:@"remain"]
                             ];
            
            ((UILabel *)[cell.contentView viewWithTag:901]).text = lb1;
            ((RTLabel *)[cell.contentView viewWithTag:902]).text = lb2;
            ((RTLabel *)[cell.contentView viewWithTag:903]).text = lb3;
            ((RTLabel *)[cell.contentView viewWithTag:903]).textColor = HEXCOLOR(0x333333FF);
        }
    }
    
    
    //cell.button.tag = [[dic valueForKey:@"cseq"] integerValue];
    cell.button.tag = indexPath.row + 1000;
    
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSDictionary *dic = self->items[indexPath.row];
    if (self.isMyCoupon) {
        //
        NSString *message = [NSString stringWithFormat:@"<font size=20>%@</font>\n<font size=14>%@</font>\n\n<font size=20>%@</font>\n<font size=14>%@</font>",
                             LocalizedStr(@"Menu.Promotion.Alert.Condition.text"),
                             dic[@"use_condition"],
                             LocalizedStr(@"Menu.Promotion.Alert.Period.text"),
                             dic[@"use_period"]];
        LGAlertView *alertView = [[LGAlertView alloc] initWithTitle:dic[@"title"]
                                                            message:message
                                                              style:LGAlertViewStyleAlert
                                                       buttonTitles:@[LocalizedStr(@"Button.OK")]
                                                  cancelButtonTitle:nil
                                             destructiveButtonTitle:nil
                                                      actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                                                          
                                                          
                                                      }
                                                      cancelHandler:^(LGAlertView *alertView) {
                                                      }
                                                 destructiveHandler:^(LGAlertView *alertView) {
                                                 }];
        [Utils initAlertButtonColor:alertView];
        alertView.messageTextAlignment = NSTextAlignmentLeft;
        [alertView showAnimated:YES completionHandler:nil];
    } else {
        if (indexPath.row==0) {
            // add
            [self addButtonPressed:dic];
        }
        else {
            NSString *message = [NSString stringWithFormat:@"<font size=20>%@</font>\n<font size=14>%@</font>\n\n<font size=20>%@</font>\n<font size=14>%@</font>",
                                 LocalizedStr(@"Menu.Promotion.Alert.Condition.text"),
                                 dic[@"use_condition"],
                                 LocalizedStr(@"Menu.Promotion.Alert.Period.text"),
                                 dic[@"use_period"]];
            LGAlertView *alertView = [[LGAlertView alloc] initWithTitle:dic[@"title"]
                                                                message:message
                                                                  style:LGAlertViewStyleAlert
                                                           buttonTitles:@[LocalizedStr(@"Menu.Promotion.BTN.download")]
                                                      cancelButtonTitle:LocalizedStr(@"Button.CANCEL")
                                                 destructiveButtonTitle:nil
                                                          actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                                                              
                                                              // download UR324
                                                              [self showBusyIndicator:@"Requesting ... "];
                                                              // send
                                                              [self->model requsetAPI:@"UR324" property:@{@"code":dic[@"code"]}];
                                                              
                                                          }
                                                          cancelHandler:^(LGAlertView *alertView) {
                                                          }
                                                     destructiveHandler:^(LGAlertView *alertView) {
                                                     }];
            [Utils initAlertButtonColor:alertView];
            alertView.messageTextAlignment = NSTextAlignmentLeft;
            [alertView showAnimated:YES completionHandler:nil];

        }
    }
    //NSDictionary *dic = self->items[indexPath.row];
    //NSDictionary* drive_trip    = [dic valueForKey:@"drive_trip"]; //trip_duration/trip_distance, begin_location, end_location, update_date
/*
    TXTripDetailVC *vc = [[TXTripDetailVC alloc] initWithNibName:@"TXTripDetailVC" bundle:[NSBundle mainBundle]];
    vc.tripData = dic;
    [self.navigationController pushViewController:vc animated:YES];
*/
}



#pragma mark - onEvent
-(void)onEvent:(TXEvent *)event eventParams:(id)subscriptionParams {
    
    TXResponseDescriptor * descriptor = [event getEventProperty:TXEvents.Params.DESCRIPTOR];
    TXRequestObj *request = [event getEventProperty:TXEvents.Params.REQUEST];
    
    if([event.name isEqualToString:@"UR303"]) {
        
        if(descriptor.success == true) {
            NSString     *response = [[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding];
            NSDictionary *result   = getJSONObj(response);
            
            NSArray *tripData              = [result valueForKey:@"user_codes"];
            
            [self->items removeAllObjects];
            self->items = [NSMutableArray arrayWithCapacity:[tripData count]];
            [self.tableView setContentOffset:CGPointZero animated:NO];

            for (NSDictionary* p in tripData) {
                [items addObject:p];
            }
            
            //if ([self->items count])
            {
                [self.tableView reloadDataAndWait:^{
                    //call the required method here
                    [self performSelector:@selector(resetBtn) withObject:nil afterDelay:0.5];
                    
                }];
                //[self.tableView reloadData];
            }
            
        } else {
        //    [self alertError:@"Error" message:@"Failed to get profile image"];
            [self performSelector:@selector(resetBtn) withObject:nil afterDelay:0.5];
            
        }
    }
    else if([event.name isEqualToString:@"UR304"]) {
        
        if(descriptor.success == true) {
            NSString     *response = [[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding];
            NSDictionary *result   = getJSONObj(response);
            
            NSArray *tripData              = [result valueForKey:@"user_coupons"];
            
            [self->items removeAllObjects];
            self->items = [NSMutableArray arrayWithCapacity:[tripData count]+1];
            [self.tableView setContentOffset:CGPointZero animated:NO];
            
            // 쿠폰등록 첫번째에 추가
            [items addObject:[NSArray array]];
            for (NSDictionary* p in tripData) {
                [items addObject:p];
            }
            
            //if ([self->items count])
            {
                [self.tableView reloadDataAndWait:^{
                    //call the required method here
                    [self performSelector:@selector(resetBtn) withObject:nil afterDelay:0.5];
                }];
                //[self.tableView reloadData];
            }
            
        } else {
            //    [self alertError:@"Error" message:@"Failed to get profile image"];
            [self performSelector:@selector(resetBtn) withObject:nil afterDelay:0.5];
        }
    }
    else if([event.name isEqualToString:@"UR323"]) {
        
        if(descriptor.success == true) {
            //NSString     *response = [[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding];
            //NSDictionary *result   = getJSONObj(response);
            
            NSString *msgtitle = LocalizedStr(@"Menu.Promotion.Book.DN.alert");
            if ([event.name isEqualToString:@"UR323"]) {
                msgtitle = LocalizedStr(@"Menu.Promotion.Book.Add.alert");
            }
            LGAlertView *alertView = [[LGAlertView alloc] initWithTitle:nil
                                                                message:msgtitle
                                                                  style:LGAlertViewStyleAlert
                                                           buttonTitles:@[LocalizedStr(@"Menu.Promotion.BTN.close")]
                                                      cancelButtonTitle:nil
                                                 destructiveButtonTitle:nil
                                                          actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                                                              
                                                          }
                                                          cancelHandler:^(LGAlertView *alertView) {
                                                          }
                                                     destructiveHandler:^(LGAlertView *alertView) {
                                                     }];
            [Utils initAlertButtonColor:alertView];
            [alertView showAnimated:YES completionHandler:nil];
            
            [self performSelector:@selector(resetBtn) withObject:nil afterDelay:0.5];
            
        } else {
            //    [self alertError:@"Error" message:@"Failed to get profile image"];
            [self performSelector:@selector(resetBtn) withObject:nil afterDelay:0.5];
        }
    }
    else if([event.name isEqualToString:@"UR324"]) {
        
        if(descriptor.success == true) {
            NSString     *response = [[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding];
            NSDictionary *result   = getJSONObj(response);
            
            NSString *msgtitle = LocalizedStr(@"Menu.Promotion.Book.DN.alert");
            if ([event.name isEqualToString:@"UR323"]) {
                msgtitle = LocalizedStr(@"Menu.Promotion.Book.Add.alert");
            }
            LGAlertView *alertView = [[LGAlertView alloc] initWithTitle:nil
                                                                message:msgtitle
                                                                  style:LGAlertViewStyleAlert
                                                           buttonTitles:@[LocalizedStr(@"Menu.Promotion.BTN.close")]
                                                      cancelButtonTitle:nil
                                                 destructiveButtonTitle:nil
                                                          actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                                                              
                                                          }
                                                          cancelHandler:^(LGAlertView *alertView) {
                                                          }
                                                     destructiveHandler:^(LGAlertView *alertView) {
                                                     }];
            [Utils initAlertButtonColor:alertView];
            [alertView showAnimated:YES completionHandler:nil];
            
            NSArray *tripData              = [result valueForKey:@"user_coupons"];
            
            [self->items removeAllObjects];
            self->items = [NSMutableArray arrayWithCapacity:[tripData count]+1];
            [self.tableView setContentOffset:CGPointZero animated:NO];
            
            // 쿠폰등록 첫번째에 추가
            [items addObject:[NSArray array]];
            for (NSDictionary* p in tripData) {
                [items addObject:p];
            }
            
            //if ([self->items count])
            {
                [self.tableView reloadDataAndWait:^{
                    //call the required method here
                    [self performSelector:@selector(resetBtn) withObject:nil afterDelay:0.5];
                }];
                //[self.tableView reloadData];
            }
            
        } else {
            //    [self alertError:@"Error" message:@"Failed to get profile image"];
            [self performSelector:@selector(resetBtn) withObject:nil afterDelay:0.5];
        }
    }
    //[self hideBusyIndicator];
}

#pragma mark -
#pragma mark Validator
#pragma mark -
-(void) resignKeyboard
{
    [self.view endEditing:YES];
}

- (IBAction)validateAction:(id)sender
{
}

#pragma ValidatorDelegate - Delegate methods

- (void) preValidation
{
}

- (void)onSuccess
{
}

- (void)onFailure:(Rule *)failedRule
{
    
    //    NSMutableDictionary *options = [@{kCRToastNotificationTypeKey               : @(CRToastTypeNavigationBar),
    //                                      kCRToastNotificationPresentationTypeKey   : @(CRToastPresentationTypeCover),
    //                                      kCRToastUnderStatusBarKey                 : @(YES),
    //                                      kCRToastTextKey                           : failedRule.failureMessage,
    //                                      kCRToastFontKey                           : [UIFont systemFontOfSize:14],
    //                                      kCRToastTextAlignmentKey                  : @(NSTextAlignmentCenter),
    //                                      kCRToastTimeIntervalKey                   : @(2),
    //                                      kCRToastAnimationInTypeKey                : @(CRToastAnimationTypeGravity),
    //                                      kCRToastAnimationOutTypeKey               : @(CRToastAnimationTypeGravity),
    //                                      kCRToastAnimationInDirectionKey           : @(CRToastAnimationDirectionTop),
    //                                      kCRToastAnimationOutDirectionKey          : @(CRToastAnimationDirectionTop),
    //                                      kCRToastNotificationPreferredPaddingKey   : @(15)} mutableCopy];
    //    [CRToastManager showNotificationWithOptions:options
    //                                completionBlock:^{
    //                                    NSLog(@"Completed");
    //                                }];
    
    [WToast showWithText:failedRule.failureMessage duration:kWTShort roundedCorners:NO gravity:kWTGravityTop];
}


@end
