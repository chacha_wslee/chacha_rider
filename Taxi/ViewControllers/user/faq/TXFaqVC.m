//
//  MenuViewController.m
//  SlideMenu
//

//

#import "TXFaqVC.h"
#import "TXFaqDetailVC.h"

@import IQKeyboardManager;

@interface TXFaqVC() {
    NSArray *items;
    NSArray *_msgList;
}

@end

@implementation TXFaqVC {
    NSMutableDictionary *propertyMap;
}
@synthesize cellIdentifier;


-(void)viewDidLoad {
    
    [super viewDidLoad];
    
    [[IQKeyboardManager sharedManager] setEnable:NO];
    
    [super configureBottomList];
    UIView *n = [super navigationView];
    NSInteger _y = n.frame.origin.y + n.frame.size.height;
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, _y, self.view.bounds.size.width, self.view.bounds.size.height-_y)
                                                  style:UITableViewStylePlain];
    self.tableView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:self.tableView];
    
    UINib *nib = [UINib nibWithNibName:@"TXFaqCell" bundle:nil];
    self.cellIdentifier = @"TXFaqCell";
    [self.tableView registerNib:nib forCellReuseIdentifier:self.cellIdentifier];
    
    self.tableView.separatorColor = [UIColor clearColor];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.opaque = NO;
    self.tableView.backgroundColor = [UIColor clearColor];
//    self.tableView.tableHeaderView = ({
//        
//        UIView *view = [Utils addLabelView:self.view underView:nil dim:0];
//        
//        UILabel *label = [view viewWithTag:11];
//        //        label.text = [NSString stringWithFormat:@"CREDIT CARD LIST"];
//        label.tag = 2001;
//        //[label sizeToFit];
//        label.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
//        
//        view;
//    });
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    self.tableView.scrollsToTop = YES;
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self removeEventListeners];
}

#pragma mark - Configure

-(void)configureStyles {
    [super configureStyles];
    
    if (appDelegate.isDriverMode) {
        [super navigationType01X:[UIImage imageNamed:@"btn_back"] centerText:LocalizedStr(@"Menu.Faq.title")];
    }
    else {
        [super navigationType01X:[UIImage imageNamed:@"btn_back"] centerText:LocalizedStr(@"Menu.Faq.title")];
    }
}

-(void)configure {
    [super configure];
    
    // 2. init event
    _msgList = @[
                 @[@"UR501",@""], // rider
                 @[@"PP501",@""], // driver
                 ];
    
    [self registerEventListeners];
    
    propertyMap = [[NSMutableDictionary alloc] init];
    
    if (appDelegate.isDriverMode) {
        [self configureDriver];
    }
    else {
        [self configureRider];
    }
    
}

-(void) configureDriver {
    
    // 3. call api
    [self showBusyIndicator:@"Requesting ... "];
    [self->model PP501:@""];
}

-(void) configureRider {

    // 3. call api
    [self showBusyIndicator:@"Requesting ... "];
    [self->model UR501:@""];
}

-(void) registerEventListeners {
    for (NSArray *msg in _msgList) {
        [self->model addEventListener:self forEvent:[msg objectAtIndex:0] eventParams:nil];
    }
}

-(void) removeEventListeners {
    for (NSArray *msg in _msgList) {
        [self->model removeEventListener:self forEvent:[msg objectAtIndex:0]];
    }
}

#pragma mark - Action
-(IBAction)onNaviButtonClick:(id)sender {
    UIButton *btn = (UIButton*)sender;
    NSLog(@"onNaviButtonClick");
    //[super onNaviButtonClick:sender];
    
    if (btn.tag == 1100) {
        [self removeEventListeners];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark - UITableView Delegate & Datasrouce -
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [tableView setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [tableView setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
    UIView *seperatorView = [[UIView alloc] initWithFrame:CGRectMake(10, cell.frame.size.height - 1, cell.frame.size.width - 10*2, 1)];
    seperatorView.backgroundColor = UIColorTableSeperator;
    [cell addSubview:seperatorView];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}


- (CGFloat)calculateHeightForConfiguredSizingCell:(UITableViewCell *)sizingCell {
    [sizingCell layoutIfNeeded];
    
    CGSize size = [sizingCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    return size.height;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static UITableViewCell *cell = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        cell = [self.tableView dequeueReusableCellWithIdentifier:@"TXFaqCell"];
    });
    
    return [self calculateHeightForConfiguredSizingCell:cell];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return self->items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:self.cellIdentifier];
	
    // driver
    if (appDelegate.isDriverMode) {
        NSDictionary *dic = self->items[indexPath.row];
        NSString* category  = [dic valueForKey:@"category"];
        
        ((UILabel *)[cell.contentView viewWithTag:10]).text = category;
    }
    else {
        NSDictionary *dic = self->items[indexPath.row];
        NSString* category  = [dic valueForKey:@"category"];
        
        ((UILabel *)[cell.contentView viewWithTag:10]).text = category;
    }
    
    if (self->items.count == indexPath.row) {
        ((UIView *)[cell.contentView viewWithTag:1]).backgroundColor = [UIColor clearColor];
    }
    else {
        ((UIView *)[cell.contentView viewWithTag:1]).backgroundColor = UIColorBasicBack;
    }

	
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSDictionary *dic = self->items[indexPath.row];
    NSArray* list  = [dic valueForKey:@"list"];
    //NSDictionary* drive_trip    = [dic valueForKey:@"drive_trip"]; //trip_duration/trip_distance, begin_location, end_location, update_date
    
    TXFaqDetailVC *vc = [[TXFaqDetailVC alloc] initWithNibName:@"TXFaqDetailVC" bundle:[NSBundle mainBundle]];
    vc.tripData = list;
    vc.ctype = [dic valueForKey:@"ctype"];
    vc.subtitle = [dic valueForKey:@"category"];
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - onEvent
-(void)onEvent:(TXEvent *)event eventParams:(id)subscriptionParams {
    
    TXResponseDescriptor * descriptor = [event getEventProperty:TXEvents.Params.DESCRIPTOR];
    TXRequestObj *request = [event getEventProperty:TXEvents.Params.REQUEST];
    
    if([event.name isEqualToString:@"UR501"]) {
        
        if(descriptor.success == true) {
            NSString     *response = [[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding];
            NSDictionary *result   = getJSONObj(response);
            
            self->items = [result valueForKey:@"faq_users"];
            
            [self.tableView reloadData];
            
        } else {
            //에러주석[self alertError:@"Error" message:descriptor.error];
        }
    }
    else if([event.name isEqualToString:@"PP501"]) {
        
        if(descriptor.success == true) {
            NSString     *response = [[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding];
            NSDictionary *result   = getJSONObj(response);
            
            self->items = [result valueForKey:@"faq_providers"];
            
            [self.tableView reloadData];
            
        } else {
            //에러주석[self alertError:@"Error" message:descriptor.error];
        }
    }
    [self hideBusyIndicator];
}



@end
