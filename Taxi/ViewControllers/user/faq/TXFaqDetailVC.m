//
//  MenuViewController.m
//  SlideMenu
//
//  Created by Aryan Gh on 4/24/13.
//  Copyright (c) 2013 Aryan Ghassemi. All rights reserved.
//

#import "TXFaqDetailVC.h"
#import "TXFaqDetailCell.h"
#import "LGAlertView.h"
#import "utilities.h"

@import UITextView_Placeholder;

static NSString *CellIdentifier = @"CellIdentifier";

@interface TXFaqDetailVC() <UITextFieldDelegate, UITextViewDelegate>{
    NSMutableArray *items;
    NSArray *_msgList;
}

@end

@implementation TXFaqDetailVC {
    NSMutableDictionary *propertyMap;
}
@synthesize cellIdentifier;
@synthesize tripData, ctype;
@synthesize subtitle;


-(void)viewDidLoad {
    
    [super viewDidLoad];
    
    [[IQKeyboardManager sharedManager] setEnable:NO];
    
    [super configureBottomLine];
    UIView *n = [super navigationView];
    NSInteger _y = n.frame.origin.y + n.frame.size.height;
    
    UIButton *btn = [Utils addBottomButton:LocalizedStr(@"Menu.Faq.Detail.BTN.help") target:self selector:@selector(addButtonPressed:) color:YES];
    btn.tag = 999;
    [self.view addSubview:btn];
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, _y, self.view.bounds.size.width, self.view.bounds.size.height-_y - btn.frame.size.height)
                                                  style:UITableViewStylePlain];
    self.tableView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:self.tableView];
    
    self.cellIdentifier = @"CellIdentifier";
    [self.tableView registerClass:[TXFaqDetailCell class] forCellReuseIdentifier:self.cellIdentifier];
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    
    self.tableView.estimatedRowHeight = 44.0;
    
//    self.cellIdentifier = @"RJTableViewCell";
//    UINib *nib = [UINib nibWithNibName:self.cellIdentifier bundle:nil];
//    [self.tableView registerNib:nib forCellReuseIdentifier:self.cellIdentifier];
    
    self.tableView.separatorColor = [UIColor clearColor];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.opaque = NO;
    self.tableView.backgroundColor = [UIColor clearColor];
//    self.tableView.tableHeaderView = ({
//        
//        UIView *view = [Utils addLabelView:self.view underView:nil dim:0];
//        
//        UILabel *label = [view viewWithTag:11];
//        //        label.text = [NSString stringWithFormat:@"CREDIT CARD LIST"];
//        label.tag = 2001;
//        //[label sizeToFit];
//        label.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
//        
//        view;
//    });
    
//    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, kBasicHeight + kBasicMargin*2)];
    self.tableView.tableFooterView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
//    [self.tableView.tableFooterView addSubview:({
//        UIView *view = [Utils addButtonBasic:self.view underView:nil dim:kBasicMargin];
//        
//        view.backgroundColor = UIColorLabelBG;
//        UIButton *button = [view viewWithTag:11];
//        button.tag = 999;
//        [button setTitle:LocalizedStr(@"Menu.Faq.Detail.BTN.help") forState:UIControlStateNormal];
//        [button addTarget:self action:@selector(addButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
//        //view.autoresizingMask = UIViewAutoresizingFlexibleHeight;
//        
//        view;
//    })];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    self.tableView.scrollsToTop = YES;
}


-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(contentSizeCategoryChanged:)
                                                 name:UIContentSizeCategoryDidChangeNotification
                                               object:nil];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIContentSizeCategoryDidChangeNotification
                                                  object:nil];
}

// This method is called when the Dynamic Type user setting changes (from the system Settings app)
- (void)contentSizeCategoryChanged:(NSNotification *)notification
{
    [self.tableView reloadData];
}

#pragma mark - Configure

-(void)configureStyles {
    [super configureStyles];
    
    if (appDelegate.isDriverMode) {
        [super navigationType01X:[UIImage imageNamed:@"btn_back"] centerText:self.subtitle];
    }
    else {
        [super navigationType01X:[UIImage imageNamed:@"btn_back"] centerText:self.subtitle];
    }
}

-(void)configure {
    [super configure];
    
    // 2. init event
    _msgList = @[
                 @[@"UR522",@""], // modify
                 @[@"PP522",@""], // modify
                 ];
    
    [self registerEventListeners];
}

-(void) registerEventListeners {
    for (NSArray *msg in _msgList) {
        [self->model addEventListener:self forEvent:[msg objectAtIndex:0] eventParams:nil];
    }
}

-(void) removeEventListeners {
    for (NSArray *msg in _msgList) {
        [self->model removeEventListener:self forEvent:[msg objectAtIndex:0]];
    }
}

#pragma mark - Action
-(IBAction)onNaviButtonClick:(id)sender {
    UIButton *btn = (UIButton*)sender;
    NSLog(@"onNaviButtonClick");
    //[super onNaviButtonClick:sender];
    
    if (btn.tag == 1100) {
        [self removeEventListeners];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark - IBAction
-(IBAction)addButtonPressed:(id)sender {
    //UIButton *btn = (UIButton*)sender;
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(20.0f, 20.0f, 280.0f, 240.0f)];
    
    UIView *line1 = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0, 320.0f, 1)];
    line1.backgroundColor = [UIColor colorWithWhite:0.97 alpha:1.f];
    
    UITextField *tf = [[UITextField alloc] initWithFrame:CGRectMake(15.0f, 1.0f, 260.0f, 38)];
    tf.textColor = [UIColor blackColor];
    tf.font = [UIFont systemFontOfSize:14.f];
    tf.clearButtonMode = UITextFieldViewModeNever;
    tf.placeholder = LocalizedStr(@"Menu.Faq.Detail.Alert.text1.plcaholder");
    
    UIView *line2 = [[UIView alloc] initWithFrame:CGRectMake(10.0f, 39, 320.0f, 1)];
    line2.backgroundColor = [UIColor colorWithWhite:0.97 alpha:1.f];
    
    CGRect textViewFrame = CGRectMake(10.0f, 40.0f, 260.0f, 180.0f);
    UITextView *textView = [[UITextView alloc] initWithFrame:textViewFrame];
    //textView.returnKeyType = UIReturnKeyDone;
    //textView.backgroundColor = [UIColor clearColor];
    [textView setFont:[UIFont systemFontOfSize:14]];
    textView.userInteractionEnabled = YES;
    textView.placeholder = LocalizedStr(@"Menu.Faq.Detail.Alert.text2.plcaholder");
    textView.placeholderColor = [UIColor lightGrayColor]; // optional
    
    textView.delegate = self; //텍스트 뷰 델리게이트
    
    [view addSubview:line1];
    [view addSubview:tf];
    [view addSubview:line2];
    [view addSubview:textView];
    
    //CALayer 클래스 이용 텍스트 뷰 커스터마이징, 레이어 프로퍼티 사용을 위해 <QuartzCore/QuartzCore.h> 임포트 할 것.
    //[[textView layer] setBorderColor:[[UIColor grayColor] CGColor]]; //border color
    //[[textView layer] setBackgroundColor:[[UIColor whiteColor] CGColor]]; //background color
    //[[textView layer] setBorderWidth:0.5]; // border width
    //[[textView layer] setCornerRadius:5]; // radius of rounded corners
    //[textView setClipsToBounds: YES]; //clip text within the bounds
    
    LGAlertView *alertView = [[LGAlertView alloc] initWithViewAndTitle:LocalizedStr(@"Menu.Faq.Detail.Alert.title")
                                                               message:nil
                                                                 style:LGAlertViewStyleAlert
                                                                  view:view
                                                          buttonTitles:@[LocalizedStr(@"Button.OK")]
                                                     cancelButtonTitle:LocalizedStr(@"Button.CANCEL")
                                                destructiveButtonTitle:nil
                                                         actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                                                             NSLog(@"actionHandler, %@, %lu", title, (long unsigned)index);
                                                             NSLog(@"text:%@",textView.text);
                                                             //tf.text = textView.text;
                                                             
                                                             NSString *__title = [Utils trimString:tf.text];
                                                             __title = tf.text;
                                                             NSString *__content = [Utils trimString:textView.text];
                                                             __content = textView.text;
                                                             
                                                             if (![__title isEqualToString:@""] && ![__content isEqualToString:@""]) {
                                                                 if (appDelegate.isDriverMode) {
                                                                     [self onPP522:__title content:__content];
                                                                 }
                                                                 else {
                                                                     [self onUR522:__title content:__content];
                                                                 }
                                                             }
                                                             
                                                         }
                                                         cancelHandler:^(LGAlertView *alertView) {
                                                             NSLog(@"cancelHandler");
                                                         }
                                                    destructiveHandler:^(LGAlertView *alertView) {
                                                        NSLog(@"destructiveHandler");
                                                    }];
    
    //alertView.heightMax = 256.f;
    [Utils initAlertButtonColor:alertView];
    alertView.windowLevel = LGAlertViewWindowLevelAboveStatusBar;
    //alertView.separatorsColor = [UIColor redColor];
    [alertView showAnimated:YES completionHandler:^(void)
     {
         //[textView becomeFirstResponder];
         [tf becomeFirstResponder];
     }];
}

#pragma mark - Send Event
-(void)onUR522:(NSString*)title content:(NSString*)content {
    //useq,qtype,title,content
    
    NSString* useq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.USEQ];
    //NSString* qtype = self.tripData[0][@"qtype"];
    NSDictionary *_propertyMap = @{
                                   @"useq" : useq,
                                   @"ctype" : ctype,
                                   @"title" : title,
                                   @"content" : content
                                   };
    
    [self showBusyIndicator:@"Upload ... "];
    [self->model requsetAPI:API_FUNC(NSStringFromSelector(_cmd)) property:_propertyMap];
}

-(void)onPP522:(NSString*)title content:(NSString*)content {
    //useq,qtype,title,content
    
    NSString* pseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.PSEQ];
    //NSString* qtype = self.tripData[0][@"qtype"];
    NSDictionary *_propertyMap = @{
                                   @"pseq" : pseq,
                                   @"ctype" : ctype,
                                   @"title" : title,
                                   @"content" : content
                                   };
    
    [self showBusyIndicator:@"Upload ... "];
    [self->model requsetAPI:API_FUNC(NSStringFromSelector(_cmd)) property:_propertyMap];
}

#pragma mark - onEvent
-(void)onEvent:(TXEvent *)event eventParams:(id)subscriptionParams {
    
    TXResponseDescriptor * descriptor = [event getEventProperty:TXEvents.Params.DESCRIPTOR];
    //TXRequestObj *request = [event getEventProperty:TXEvents.Params.REQUEST];
    
    [self hideBusyIndicator];
    if([event.name isEqualToString:@"UR522"] || [event.name isEqualToString:@"PP522"]) {
        
        if(descriptor.success == true) {
//            NSString     *response = [[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding];
//            NSDictionary *result   = getJSONObj(response);
            
            
            LGAlertView* alertView = [[LGAlertView alloc] initWithTitle:nil
                                        message:LocalizedStr(@"Menu.Faq.Detail.Alert.complete")
                                          style:LGAlertViewStyleAlert
                                   buttonTitles:@[LocalizedStr(@"Button.OK")]
                              cancelButtonTitle:nil
                         destructiveButtonTitle:nil
                                  actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                                      //NSLog(@"actionHandler, %@, %lu", title, (long unsigned)index);
                                      
                                      
                                  }
                                  cancelHandler:^(LGAlertView *alertView) {
                                  }
                             destructiveHandler:^(LGAlertView *alertView) {
                             }];
            [Utils initAlertButtonColor:alertView];
            [alertView showAnimated:YES completionHandler:nil];
            
        }
        else {
            [self hideBusyIndicator];
            //에러주석[self alertError:@"Error" message:descriptor.error];
        }
    }
}

#pragma mark - UITextView Delegate Methods

//캐릭터가 텍스트 뷰에 표시되기 직전 아래 메소드가 호출된다
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    
    
//    //newLineCharacterSet이 있으면 done button이 호출됨. 따라서 키보드가 사라짐.
//    NSCharacterSet *doneButtonCharacterSet = [NSCharacterSet newlineCharacterSet];
//    NSRange replacementTextRange = [text rangeOfCharacterFromSet:doneButtonCharacterSet];
//    NSUInteger location = replacementTextRange.location;
//    
//    //텍스트가 190자가 넘지 않도록 제한
//    if (textView.text.length + text.length > 190){
//        if (location != NSNotFound){
//            [textView resignFirstResponder];
//        }
//        return NO;
//    }
//    
//    else if (location != NSNotFound){
//        if([text isEqualToString:@"\n"]) {
//            return YES;
//        }
//        [textView resignFirstResponder];
//        return NO;
//    }
    
    return YES;
}

#pragma mark - UITableView Delegate & Datasrouce -

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [tableView setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [tableView setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
    UIView *seperatorView = [[UIView alloc] initWithFrame:CGRectMake(10, cell.frame.size.height - 1, cell.frame.size.width - 10*2, 1)];
    seperatorView.backgroundColor = UIColorTableSeperator;
    [cell addSubview:seperatorView];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return [self.tripData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TXFaqDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:self.cellIdentifier];
    
    // Configure the cell for this indexPath
    [cell updateFonts];
    
    NSDictionary *dic = self.tripData[indexPath.row];
    
    cell.titleLabel.text =  [dic valueForKey:@"title"];
    //cell.dateLabel.text =   StringFormatDate2Date([dic valueForKey:@"reg_date"]); // __DEBUG
    cell.dateLabel.text =   @"";
    //cell.dateLabel.text =   [dic valueForKey:@"title"];
    cell.bodyLabel.text = [dic valueForKey:@"content"];
    
    // Make sure the constraints have been added to this cell, since it may have just been created from scratch
    [cell setNeedsUpdateConstraints];
    [cell updateConstraintsIfNeeded];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


@end
