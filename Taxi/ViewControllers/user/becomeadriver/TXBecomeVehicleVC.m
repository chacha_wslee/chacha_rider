//
//  TXProfileVC.m
//  Taxi
//

//

//#import "TXBecomeFinishVC.h"
#import "TXAboutVC.h"
#import "TXBecomeVehicleVC.h"
#import "LGAlertView.h"
#import "TOCropViewController.h"
//#import "EXPhotoViewer.h"
#import "TXVehicleVC.h"
#import "utilities.h"

#import "Validator.h"
#import "WToast.h"
//#import "CRToast.h"

#define MaxProcessCnt 1
#define NSProcessPct [NSString stringWithFormat:@"%d%@",(int)(processPct*100/MaxProcessCnt),LocalizedStr(@"Menu.BecomeADriver.Header.text2")]

@interface TXBecomeVehicleVC () <UITextFieldDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIActionSheetDelegate, TOCropViewControllerDelegate, UIViewControllerTransitioningDelegate, ValidatorDelegate>{
    int userId;
    NSArray *_msgList;
    UILabel *lbProcessInfo;
    int processPct;
    
    NSArray *dataArray;
    
    BOOL isUpdateImage1;
    BOOL isUpdateImage2;
    BOOL isUpdateImage3;
    BOOL isUpdateImage4;
    
    NSInteger imageLoadingMax;
    NSInteger imageLoadingCnt;
    
    id observer1;
    id observer2;
    id observer3;
}
@property (nonatomic, assign) TOCropViewCroppingStyle croppingStyle; //The cropping style
@property (strong, nonatomic) LGAlertView *securityAlertView;
@end

@implementation TXBecomeVehicleVC {
    NSMutableDictionary *propertyMap;
    NSMutableDictionary *propertyMap2;
    int _alertIdx;
}


-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil isnew:(BOOL)isnew{
    
    if(self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        self->_alertIdx = 1;
        self.isNewVehicle = isnew;
    }
    
    return self;
}

-(void)viewDidLoad {
    [super viewDidLoad];
    
    [[IQKeyboardManager sharedManager] setEnable:NO];
    
    observer1 = [[NSNotificationCenter defaultCenter] addObserverForName:@"NotificationPhotoAlbum"
                                                                  object:nil
                                                                   queue:[NSOperationQueue mainQueue]
                                                              usingBlock:^(NSNotification *notification) {
                                                                  
                                                                  [self openPhotoAlbum];
                                                                  
                                                              }];
    observer2 = [[NSNotificationCenter defaultCenter] addObserverForName:@"NotificationShowCamera"
                                                                  object:nil
                                                                   queue:[NSOperationQueue mainQueue]
                                                              usingBlock:^(NSNotification *notification) {
                                                                  
                                                                  [self showCamera];
                                                                  
                                                              }];
//    observer3 = [[NSNotificationCenter defaultCenter] addObserverForName:@"NotificationEditPhoto"
//                                                                  object:nil
//                                                                   queue:[NSOperationQueue mainQueue]
//                                                              usingBlock:^(NSNotification *notification) {
//                                                                  
//                                                                  NSLog(@"notification:%@",notification);
//                                                                  [self editPhoto];
//                                                                  
//                                                              }];
    
    isUpdateImage1 = NO;
    isUpdateImage2 = NO;
    isUpdateImage3 = NO;
    isUpdateImage4 = NO;
    imageLoadingMax = 0;
    imageLoadingCnt = 0;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self checkProcess];
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    //[self removeEventListeners];

}


#pragma mark - Configure

-(void)configureStyles {
    [super configureStyles];
    
//    if (appDelegate.isDriverMode) {
//        [super navigationType01X:[UIImage imageNamed:@"btn_back"] centerText:LocalizedStr(@"Menu.Account.title")];
//    }
//    else {
//        [super navigationType01X:[UIImage imageNamed:@"btn_back"] centerText:LocalizedStr(@"Menu.Account.title")];
//    }
//
    if (_isNewVehicle) {
        [super navigationType01X:[UIImage imageNamed:@"btn_back"] centerText:LocalizedStr(@"Menu.BecomeADriver.Vehicle.title")];
        //[super navigationType11X:[UIImage imageNamed:@"btn_back"] centerText:LocalizedStr(@"Menu.BecomeADriver.Vehicle.title") rightText:LocalizedStr(@"Intro.Save.title")];
    }
    else {
        [super navigationType01X:[UIImage imageNamed:@"btn_back"] centerText:LocalizedStr(@"Menu.BecomeADriver.Vehicle.title")];
        //[super navigationType11X:[UIImage imageNamed:@"btn_back"] centerText:LocalizedStr(@"Menu.BecomeADriver.Vehicle.title") rightText:LocalizedStr(@"Intro.Next.title")];
    }
    [self nextButtonOn:NO];
}

- (void)nextButtonOn:(BOOL)on
{
    UIButton *btn = [super.view viewWithTag:999];
    btn.enabled = on;
    if (on)
        [btn setTitleColor:UIColorButtonText forState:UIControlStateNormal];
    else
        [btn setTitleColor:UIColorBasicTextOff forState:UIControlStateNormal];
}

-(void)configure {
    [super configure];
    [self configureConfig];
    self->userId = [[[[self->model getApp] getSettings] getUserId] intValue];
    
    [super configureBottomList];
    UIView *n = [super navigationView];
    NSInteger _y = n.frame.origin.y + n.frame.size.height;
    
    //------------------------------------------------------------------------------------------------------------------------
    propertyMap = [[NSMutableDictionary alloc] init];
    propertyMap2 = [[NSMutableDictionary alloc] init];
    //------------------------------------------------------------------------------------------------------------------------
//    UIView *labelView = [Utils addDoubleLabelView:self.view underView:nil dim:0];
//    CGRect rect = labelView.frame;
//    rect.origin.y = kTopHeight + kNaviHeight;
//    labelView.frame = rect;
//    UILabel *label = [labelView viewWithTag:11];
//    label.text = LocalizedStr(@"Menu.BecomeADriver.Header.text1");
//    lbProcessInfo = [labelView viewWithTag:12];
    
    UIView *bgview;
    if (_isNewVehicle) {
        bgview = [Utils addViewButtonText:LocalizedStr(@"Intro.Save.title") target:self selector:@selector(onNaviButtonClick:) color:YES];
    }
    else {
        bgview = [Utils addViewButtonText:LocalizedStr(@"Intro.Next.title") target:self selector:@selector(onNaviButtonClick:) color:YES];
    }
    
    [self.view addSubview:bgview];
    
    lbProcessInfo = (UILabel*)[bgview viewWithTag:998];
    processPct = 0;
    lbProcessInfo.text = NSProcessPct;
//    [self.view addSubview:labelView];
    //------------------------------------------------------------------------------------------------------------------------
    //((UILabel *)[self.vbody viewWithTag:11011]).text = LocalizedStr(@"Menu.BecomeADriver.Vehicle.List.text1");
    ((UILabel *)[self.vbody viewWithTag:11111]).text = LocalizedStr(@"Menu.BecomeADriver.Vehicle.List.text2");
    //((UILabel *)[self.vbody viewWithTag:11211]).text = LocalizedStr(@"Menu.BecomeADriver.Vehicle.List.text3");
    //((UILabel *)[self.vbody viewWithTag:11311]).text = LocalizedStr(@"Menu.BecomeADriver.Vehicle.List.text4");
    //((UILabel *)[self.vbody viewWithTag:11411]).text = LocalizedStr(@"Menu.BecomeADriver.Vehicle.List.text5");
    
//    UIView *lb = (UIView *)[self.vbody viewWithTag:11410];
//    UIView *view = [Utils addButtonBasic:self.view underView:lb dim:kBasicMargin];
//    
//    view.backgroundColor = UIColorLabelBG;
//    UIButton *button = [view viewWithTag:11];
//    button.tag = 999;
    
    //------------------------------------------------------------------------------------------------------------------------
    self.myScrollView.backgroundColor = [UIColor whiteColor];
    self.vbody.backgroundColor = UIColorLabelBG;
    self.vbody.frame = CGRectMake(self.vbody.frame.origin.x, self.vbody.frame.origin.y, self.view.frame.size.width, self.vbody.frame.size.height);
    
    self.myScrollView.frame = CGRectMake(0,
                                         _y,
                                         self.view.frame.size.width,
                                         self.view.frame.size.height - _y - kBottomButtonHeight);
    
    self.myScrollView.contentSize = CGSizeMake(self.vbody.frame.size.width, self.vbody.frame.size.height);
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0, 0.0, 0, 0.0);
    self.myScrollView.contentInset = contentInsets;
    self.myScrollView.scrollIndicatorInsets = contentInsets;
    self.myScrollView.scrollsToTop = YES;
    [self.myScrollView addSubview:self.vbody];
    //------------------------------------------------------------------------------------------------------------------------
    // 2. init event
    _msgList = @[
                    @[@"PP410",@""], // modify
                    @[@"PP629",@""], // modify
                    
//                    @[@"PP411",@""], // modify
//                    @[@"PP412",@""], // modify
//                    @[@"PP413",@""], // modify
//                    @[@"PP414",@""], // modify
                    
                    @[@"PP420",@""], // save
                    
                    @[@"PP400",@""], // loading
                    @[@"PP606",@""], // loading
                    
//                    @[@"PP401",@""], // loading
//                    @[@"PP402",@""], // loading
//                    @[@"PP403",@""], // loading
//                    @[@"PP404",@""], // loading
                    ];
    
    [self registerEventListeners];
    
    if (appDelegate.isDriverMode) {
        [self configureDriver];
    }
    else {
        [self configureRider];
    }
}

-(void) configureConfig {

    
}

-(void) configureDriver {
    if (_isNewVehicle) {
        return;
    }
    
    // 1. init var
    //if (![appDelegate.becomePseq isEqualToString:@""] && ![appDelegate.becomeVseq isEqualToString:@""]) {
    if (![appDelegate.becomePseq isEqualToString:@""]) {
        // 조회
        [self onPP400];
    }
}

-(void) configureRider {
    // 1. init var
    //if (![appDelegate.becomePseq isEqualToString:@""] && ![appDelegate.becomeVseq isEqualToString:@""]) {
    if (![appDelegate.becomePseq isEqualToString:@""]) {
        // 조회
        [self onPP400];
    }
}

-(void) registerEventListeners {
    for (NSArray *msg in _msgList) {
        [self->model addEventListener:self forEvent:[msg objectAtIndex:0] eventParams:nil];
    }
}

-(void) removeEventListeners {
    for (NSArray *msg in _msgList) {
        [self->model removeEventListener:self forEvent:[msg objectAtIndex:0]];
    }
    
    [[NSNotificationCenter defaultCenter] removeObserver:observer1];
    observer1 = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:observer2];
    observer2 = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:observer3];
    observer3 = nil;
    
//    [[NSNotificationCenter defaultCenter] removeObserver:self
//                                                    name:@"NotificationPhotoAlbum"
//                                                  object:nil];
//    [[NSNotificationCenter defaultCenter] removeObserver:self
//                                                    name:@"NotificationShowCamera"
//                                                  object:nil];
//    [[NSNotificationCenter defaultCenter] removeObserver:self
//                                                    name:@"NotificationEditPhoto"
//                                                  object:nil];
}

-(void) checkProcess {
    processPct = (int)[propertyMap count];
    lbProcessInfo.text = NSProcessPct;
    
    //if (processPct == MaxProcessCnt && _btnAgree.tag == 1) {
    if (processPct == MaxProcessCnt) {
        [self nextButtonOn:YES];
    }
    else {
        [self nextButtonOn:NO];
    }
    
}

-(void)updateTrimString:(NSString*)str key:(NSString*)key
{
    NSString *trimmedString = [Utils trimStringOnly:str];
    
    if ([trimmedString isEqualToString:@""]) {
        [propertyMap removeObjectForKey:key];
    }
    else {
        [propertyMap setValue:str forKey:key];
    }
}

#pragma mark - Action
-(IBAction)onNaviButtonClick:(id)sender {
    UIButton *btn = (UIButton*)sender;
    NSLog(@"onNaviButtonClick");
    //[super onNaviButtonClick:sender];
    
    if (btn.tag == 1100 || btn.tag == 1101) {
        [self removeEventListeners];
        [self.navigationController popViewControllerAnimated:YES];
    }
    else if(btn.tag == 1300 || btn.tag == 999) {
        
        if (![appDelegate.becomeVseq isEqualToString:@""]) {
            [self onPP410];
        }
        else {
            [self onPP420];
        }
        
    }
}


#pragma mark - IBAction

-(IBAction)inspectionButtonPressed:(id)sender {
    //UIButton *btn = (UIButton*)sender;
#ifndef _CHACHA
    NSCharacterSet *set = [NSCharacterSet URLQueryAllowedCharacterSet];
    NSString* content = [INSPECTION_URL stringByAddingPercentEncodingWithAllowedCharacters:set];
    [[UIApplication sharedApplication] openURL: [NSURL URLWithString:content]];
    //[[UIApplication sharedApplication] openURL: [NSURL URLWithString:INSPECTION_URL]];
#endif
}

-(IBAction)onPhoto:(id)sender {
    
    self->_alertIdx = (int)((UIButton*)sender).tag;
    NSLog(@"_alertIdx:%d",self->_alertIdx);
    
    NSArray *items = @[LocalizedStr(@"Menu.Driver.Profile.Image.album")];
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        items = @[LocalizedStr(@"Menu.Driver.Profile.Image.album"), LocalizedStr(@"Menu.Driver.Profile.Image.camera"), LocalizedStr(@"Menu.Driver.Profile.Image.edit")];
    }
    
    LGAlertView *alertView = [[LGAlertView alloc] initWithTitle:nil
                                                        message:LocalizedStr(@"Menu.Driver.Profile.Image.title2")
                                                          style:LGAlertViewStyleActionSheet
                                                   buttonTitles:items
                                              cancelButtonTitle:LocalizedStr(@"Button.CANCEL")
                                         destructiveButtonTitle:nil
                                                  actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                                                      NSLog(@"actionHandler, %@, %lu", title, (long unsigned)index);
                                                      
                                                      if (index == 0) {
                                                          [self openPhotoAlbum];
                                                      }
                                                      else if (index == 1) {
                                                          [self showCamera];
                                                      }
                                                      else if (index == 2) {
                                                          UIImageView *imageView;
                                                          if (self->_alertIdx == 11012 || self->_alertIdx == 11013) {
                                                              imageView = (UIImageView *)[self.vbody viewWithTag:11015];
                                                          }
                                                          else if (self->_alertIdx == 11212 || self->_alertIdx == 11213) {
                                                              imageView = (UIImageView *)[self.vbody viewWithTag:11215];
                                                          }
                                                          else if (self->_alertIdx == 11312 || self->_alertIdx == 11313) {
                                                              imageView = (UIImageView *)[self.vbody viewWithTag:11315];
                                                          }
                                                          else if (self->_alertIdx == 11412 | self->_alertIdx == 11413) {
                                                              imageView = (UIImageView *)[self.vbody viewWithTag:11415];
                                                          }
                                                          
                                                          if (imageView.image) {
                                                              TOCropViewController *cropController = [[TOCropViewController alloc] initWithCroppingStyle:self.croppingStyle image:imageView.image];
                                                              cropController.delegate = self;
                                                              [self presentViewController:cropController animated:YES completion:nil];
                                                          }
                                                      }
                                                  }
                                                  cancelHandler:^(LGAlertView *alertView) {
                                                      NSLog(@"cancelHandler");
                                                  }
                                             destructiveHandler:^(LGAlertView *alertView) {
                                                 NSLog(@"destructiveHandler");
                                             }];
    [Utils initAlertButtonColor:alertView];
    [alertView showAnimated:YES completionHandler:nil];
}

-(IBAction)onClick:(id)sender {
    
    UIButton *btn = (UIButton*)sender;
    
    if (btn.tag == 11012) {
        self->_alertIdx = (int)btn.tag;
        if ([propertyMap valueForKey:@"vehicle"]) {
            
//            UIImageView *imageView = (UIImageView *)[self.vbody viewWithTag:11015];
            //[EXPhotoViewer showImageFrom:imageView tag:btn.tag];
//            if (imageView.image) {
//                TOCropViewController *cropController = [[TOCropViewController alloc] initWithCroppingStyle:self.croppingStyle image:imageView.image];
//                cropController.delegate = self;
//                [self presentViewController:cropController animated:YES completion:nil];
//            }
//            else {
                [self onPhoto:sender];
//            }
        }
        else {
            [self onPhoto:btn];
        }
        return;
    }
    else if (btn.tag == 11212) {
        self->_alertIdx = (int)btn.tag;
        if ([propertyMap valueForKey:@"registration"]) {
            
//            UIImageView *imageView = (UIImageView *)[self.vbody viewWithTag:11215];
            //[EXPhotoViewer showImageFrom:imageView tag:btn.tag];
//            if (imageView.image) {
//                TOCropViewController *cropController = [[TOCropViewController alloc] initWithCroppingStyle:self.croppingStyle image:imageView.image];
//                cropController.delegate = self;
//                [self presentViewController:cropController animated:YES completion:nil];
//            }
//            else {
                [self onPhoto:sender];
//            }
        }
        else {
            [self onPhoto:btn];
        }
        return;
    }
    else if (btn.tag == 11312) {
        self->_alertIdx = (int)btn.tag;
        if ([propertyMap valueForKey:@"insurance"]) {
            
//            UIImageView *imageView = (UIImageView *)[self.vbody viewWithTag:11315];
            //[EXPhotoViewer showImageFrom:imageView tag:btn.tag];
//            if (imageView.image) {
//                TOCropViewController *cropController = [[TOCropViewController alloc] initWithCroppingStyle:self.croppingStyle image:imageView.image];
//                cropController.delegate = self;
//                [self presentViewController:cropController animated:YES completion:nil];
//            }
//            else {
                [self onPhoto:sender];
//            }
        }
        else {
            [self onPhoto:btn];
        }
        return;
    }
    else if (btn.tag == 11412) {
        self->_alertIdx = (int)btn.tag;
        if ([propertyMap2 valueForKey:@"inspection"]) {
            
//            UIImageView *imageView = (UIImageView *)[self.vbody viewWithTag:11415];
            //[EXPhotoViewer showImageFrom:imageView tag:btn.tag];
//            if (imageView.image) {
//                TOCropViewController *cropController = [[TOCropViewController alloc] initWithCroppingStyle:self.croppingStyle image:imageView.image];
//                cropController.delegate = self;
//                [self presentViewController:cropController animated:YES completion:nil];
//            }
//            else {
                [self onPhoto:sender];
//            }
        }
        else {
            [self onPhoto:btn];
        }
        return;
    }
    
    NSInteger tag = (btn.tag / 100) % 100 + 1 - 10;
    
    //NSLog(@"tag:%ld",tag);

    self->_alertIdx = 1;
    [self popupInput:(int)tag];
    
}



-(void)popupInput:(int)tag
{
    NSString *__title = [NSString stringWithFormat:@"Menu.BecomeADriver.Vehicle.List.text%d",tag];
    __title = LocalizedStr(__title);
    
    NSString *__subtitle = nil;
//    if (tag == 1 || tag == 2) {
//        __subtitle = [NSString stringWithFormat:@"Menu.BecomeADriver.Vehicle.List.text%d.input",tag];
//        __subtitle = LocalizedStr(__subtitle);
//    }
    
    LGAlertView *alertView = [[LGAlertView alloc] initWithTextFieldsAndTitle:__title
                                                                     message:__subtitle
                                                          numberOfTextFields:1
                                                      textFieldsSetupHandler:^(UITextField *textField, NSUInteger index)
                              {
                                  if (index == 0) {
                                      textField.placeholder = @"";
                                      textField.text = @"";
                                  }
                                  
                                  textField.tag = index;
                                  //textField.delegate = self;
                                  textField.enablesReturnKeyAutomatically = YES;
                                  textField.autocapitalizationType = NO;
                                  textField.autocorrectionType = NO;
                                  textField.clearButtonMode = UITextFieldViewModeNever;
                                  textField.textAlignment = NSTextAlignmentCenter;
                                  
                                  if (tag == 5 || tag == 13 || tag == 14 || tag == 15 ) {
                                      [textField setKeyboardType:UIKeyboardTypeNumberPad];
                                  }
                              }
                                                                buttonTitles:@[LocalizedStr(@"Button.OK")]
                                                           cancelButtonTitle:LocalizedStr(@"Button.CANCEL")
                                                      destructiveButtonTitle:nil
                                                               actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                                                                   UITextField *secondTextField = alertView.textFieldsArray[index];
                                                                   //promoCode = secondTextField.text;
                                                                   
                                                                   NSLog(@"actionHandler, %@, %lu, %@", title, (long unsigned)index, secondTextField.text);
                                                                   
                                                                   int __tag = tag * 100 + 1000 + 12 + 10000 - 100;
                                                                   
                                                                   
                                                                   
                                                                   [self resignKeyboard];
                                                                   Validator *validator = [[Validator alloc] init];
                                                                   validator.delegate   = self;
                                                                   //
                                                                   [validator putRule:[Rules checkRange:NSMakeRange(3, 10) withFailureString:LocalizedStr(@"Validator.PlateNo.text") forTextField:secondTextField]];
                                                                   
                                                                   if (![validator validateForResult]) {
                                                                       [alertView.firstButton setSelected:NO];
                                                                       return;
                                                                   }
                                                                   
                                                                   ((UITextField *)[self.vbody viewWithTag:__tag]).text = secondTextField.text;
                                                                   
                                                                   if (tag == 2) {
                                                                       [self updateTrimString:secondTextField.text key:@"vehiclemodel"];
                                                                   }
                                                                   
                                                                   [self checkProcess];
                                                                   
                                                                   [alertView dismissAnimated:YES completionHandler:nil];
                                                               }
                                                               cancelHandler:^(LGAlertView *alertView) {
                                                                   [alertView dismissAnimated:YES completionHandler:nil];
                                                                   
                                                               }
                                                          destructiveHandler:^(LGAlertView *alertView) {
                                                              [alertView dismissAnimated:YES completionHandler:nil];
                                                          }];
    //[alertView setButtonAtIndex:0 enabled:NO];
    [Utils initAlertButtonColor:alertView];
    [alertView showAnimated:YES completionHandler:nil];
    [alertView setDismissOnAction:NO];
}

#pragma mark - Send Event
-(void)onPP400 {
    
    NSDictionary *_propertyMap = @{
                                   @"pseq" : appDelegate.becomePseq
                                   };
    
    [self showBusyIndicator:@"Loading info ... "];
    
    [self->model requsetAPI:API_FUNC(NSStringFromSelector(_cmd)) property:_propertyMap];
}

-(void)onPP401:(NSString*)picture {
    
    NSDictionary *_propertyMap = @{
                                   @"pseq" : appDelegate.becomePseq,
                                   @"vseq" : appDelegate.becomeVseq,
                                   @"name" : @"vehicle",
                                   @"vehicle" : picture
                                   };
    
    [self showBusyIndicator:@"Loading ... "];
    [self->model requsetAPI:@"PP606" property:_propertyMap];
}
-(void)onPP402:(NSString*)picture {
    
    NSDictionary *_propertyMap = @{
                                   @"pseq" : appDelegate.becomePseq,
                                   @"vseq" : appDelegate.becomeVseq,
                                   @"name" : @"registration",
                                   @"registration" : picture
                                   };
    
    [self showBusyIndicator:@"Loading ... "];
    [self->model requsetAPI:@"PP606" property:_propertyMap];
}
-(void)onPP403:(NSString*)picture {
    
    NSDictionary *_propertyMap = @{
                                   @"pseq" : appDelegate.becomePseq,
                                   @"vseq" : appDelegate.becomeVseq,
                                   @"name" : @"insurance",
                                   @"insurance" : picture
                                   };
    
    [self showBusyIndicator:@"Loading ... "];
    [self->model requsetAPI:@"PP606" property:_propertyMap];
}
-(void)onPP404:(NSString*)picture {
    
    NSDictionary *_propertyMap = @{
                                   @"pseq" : appDelegate.becomePseq,
                                   @"vseq" : appDelegate.becomeVseq,
                                   @"name" : @"inspection",
                                   @"inspection" : picture
                                   };
    
    [self showBusyIndicator:@"Loading ... "];
    [self->model requsetAPI:@"PP606" property:_propertyMap];
}
-(void)onPP410 {
    
    NSDictionary *_propertyMap = @{
                                   @"pseq":appDelegate.becomePseq,
                                   @"vseq":appDelegate.becomeVseq,
                                   @"vehiclemodel":propertyMap[@"vehiclemodel"]
                                   };
     
    [self showBusyIndicator:@"Update ... "];
    [self->model requsetAPI:API_FUNC(NSStringFromSelector(_cmd)) property:_propertyMap];
}

-(void)onPP420 {
    
    NSDictionary *_propertyMap = @{
                                   @"pseq":appDelegate.becomePseq,
                                   @"vehiclemodel":propertyMap[@"vehiclemodel"]
                                   };
    
    [self showBusyIndicator:@"Create ... "];
    [self->model requsetAPI:API_FUNC(NSStringFromSelector(_cmd)) property:_propertyMap];
}

-(void)onPP411 {
    
//    UIImageView *imageView = (UIImageView *)[self.vbody viewWithTag:11015];
//    NSString* picture = [Utils encodeToBase64String:imageView.image];
    NSString* picture = [propertyMap objectForKey:@"vehicle"];
    NSDictionary *_propertyMap = @{
                                   @"pseq" : appDelegate.becomePseq,
                                   @"vseq" : appDelegate.becomeVseq,
                                   @"name" : @"vehicle",
                                   @"vehicle" : picture
                                   };
    
    [self showBusyIndicator:@"Update ... "];
    [self->model requsetAPI:@"PP629" property:_propertyMap];
}

-(void)onPP412 {
    
//    UIImageView *imageView = (UIImageView *)[self.vbody viewWithTag:11215];
//    NSString* picture = [Utils encodeToBase64String:imageView.image];
    NSString* picture = [propertyMap objectForKey:@"registration"];
    NSDictionary *_propertyMap = @{
                                   @"pseq" : appDelegate.becomePseq,
                                   @"vseq" : appDelegate.becomeVseq,
                                   @"name" : @"registration",
                                   @"registration" : picture
                                   };
    
    //[self showBusyIndicator:@"Loading ... "];
    [self->model requsetAPI:@"PP629" property:_propertyMap];
}

-(void)onPP413 {
    
//    UIImageView *imageView = (UIImageView *)[self.vbody viewWithTag:11315];
//    NSString* picture = [Utils encodeToBase64String:imageView.image];
    NSString* picture = [propertyMap objectForKey:@"insurance"];
    NSDictionary *_propertyMap = @{
                                   @"pseq" : appDelegate.becomePseq,
                                   @"vseq" : appDelegate.becomeVseq,
                                   @"name" : @"insurance",
                                   @"insurance" : picture
                                   };
    
    //[self showBusyIndicator:@"Loading ... "];
    [self->model requsetAPI:@"PP629" property:_propertyMap];
}

-(void)onPP414 {
    
//    UIImageView *imageView = (UIImageView *)[self.vbody viewWithTag:11415];
//    NSString* picture = [Utils encodeToBase64String:imageView.image];
    NSString* picture = [propertyMap2 objectForKey:@"inspection"];
    NSDictionary *_propertyMap = @{
                                   @"pseq" : appDelegate.becomePseq,
                                   @"vseq" : appDelegate.becomeVseq,
                                   @"name" : @"inspection",
                                   @"inspection" : picture
                                   };
    
    //[self showBusyIndicator:@"Loading ... "];
    [self->model requsetAPI:@"PP629" property:_propertyMap];
}



#pragma mark - onEvent
-(void)onEvent:(TXEvent *)event eventParams:(id)subscriptionParams {
    
    TXResponseDescriptor * descriptor = [event getEventProperty:TXEvents.Params.DESCRIPTOR];
    TXRequestObj *request = [event getEventProperty:TXEvents.Params.REQUEST];
    
    //[self hideBusyIndicator];
    if([event.name isEqualToString:@"PP400"]) {
        
        if(descriptor.success == true) {
            NSString     *response = [[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding];
            NSDictionary *result   = getJSONObj(response);
            
            
            NSArray *vehicle              = [result valueForKey:@"provider_vehicles"];
            if ([vehicle isKindOfClass:[NSArray class]] && [vehicle count]) {
                appDelegate.becomeVseq = vehicle[0][@"vseq"];

                propertyMap[@"vehiclemodel"] = vehicle[0][@"vehiclemodel"];
                
                ((UITextField *)[self.vbody viewWithTag:11112]).text = propertyMap[@"vehiclemodel"];
                
                if (![vehicle[0][@"vehicle"] intValue] || ![vehicle[0][@"registration"] intValue] || ![vehicle[0][@"insurance"] intValue] || ![vehicle[0][@"inspection"] intValue]) {
                    [self hideBusyIndicator];
                }
                
                imageLoadingMax = 0;
                imageLoadingCnt = 0;
                
                if ([vehicle[0][@"vehicle"] intValue]) {
                    imageLoadingMax++;
                    [self onPP401:vehicle[0][@"vehicle"]];
                }
                if ([vehicle[0][@"registration"] intValue]) {
                    imageLoadingMax++;
                    [self onPP402:vehicle[0][@"registration"]];
                }
                if ([vehicle[0][@"insurance"] intValue]) {
                    imageLoadingMax++;
                    [self onPP403:vehicle[0][@"insurance"]];
                }
                if ([vehicle[0][@"inspection"] intValue]) {
                    imageLoadingMax++;
                    [self onPP404:vehicle[0][@"inspection"]];
                }
                
                if (!imageLoadingMax) {
                    [self hideBusyIndicator];
                }
            }
            else {
                [self hideBusyIndicator];
            }
            
            [self checkProcess];
        }
        else {
            [self hideBusyIndicator];
            [self alertError:@"Error" message:descriptor.error];
        }
    }
    //else if([event.name isEqualToString:@"PP401"] || [event.name isEqualToString:@"PP402"] || [event.name isEqualToString:@"PP403"] || [event.name isEqualToString:@"PP404"]) {
    else if([event.name isEqualToString:@"PP606"]) {
        imageLoadingCnt++;
        
        NSDictionary *param;
//        if (request.body && ([request.body isKindOfClass:[NSData class]] && ((NSData *)request.body).length > 0)) {
//            param = [Utils getDataOfQueryString:request.body];
//        }
        if (![Utils isDictionary:param]) {
            param = [Utils getDataOfQueryString:request.reqUrl];
        }
        
        if(descriptor.success == true) {
            NSString     *response = [[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding];
            NSDictionary *result   = getJSONObj(response);
            
            NSString *pictureData              = nil;
            
            pictureData              = [result valueForKey:@"picture"];
            
            if ([pictureData length]) {
                
//                NSData *data = [[NSData alloc] initWithBase64EncodedString:pictureData options:0];
//                UIImage *image = [[UIImage alloc] initWithData:data];
                UIImage *image = [Utils decodeBase64ToImage:pictureData];
                // Store image
                //[appDelegate.cache storeObject:event forKey:request.reqUrl];
                
                if ([param[@"name"] isEqualToString:@"vehicle"]) {
                    UIButton *btn = (UIButton *)[self.vbody viewWithTag:11012];
                    [btn setImage:nil forState:UIControlStateNormal];
                    UIImageView *imageView = (UIImageView *)[self.vbody viewWithTag:11015];
                    imageView.image = image;
                    [propertyMap setObject:pictureData forKey:@"vehicle"];
                    isUpdateImage1 = NO;
                }
                else if ([param[@"name"] isEqualToString:@"registration"]) {
                    UIButton *btn = (UIButton *)[self.vbody viewWithTag:11212];
                    [btn setImage:nil forState:UIControlStateNormal];
                    UIImageView *imageView = (UIImageView *)[self.vbody viewWithTag:11215];
                    imageView.image = image;
                    [propertyMap setObject:pictureData forKey:@"registration"];
                    isUpdateImage2 = NO;
                }
                else if ([param[@"name"] isEqualToString:@"insurance"]) {
                    UIButton *btn = (UIButton *)[self.vbody viewWithTag:11312];
                    [btn setImage:nil forState:UIControlStateNormal];
                    UIImageView *imageView = (UIImageView *)[self.vbody viewWithTag:11315];
                    imageView.image = image;
                    [propertyMap setObject:pictureData forKey:@"insurance"];
                    isUpdateImage3 = NO;
                }
                else if ([param[@"name"] isEqualToString:@"inspection"]) {
                    UIButton *btn = (UIButton *)[self.vbody viewWithTag:11412];
                    [btn setImage:nil forState:UIControlStateNormal];
                    UIImageView *imageView = (UIImageView *)[self.vbody viewWithTag:11415];
                    imageView.image = image;
                    [propertyMap2 setObject:pictureData forKey:@"inspection"];
                    isUpdateImage4 = NO;
                }
            }
            else {
                [self hideBusyIndicator];
            }
            
            if (imageLoadingMax <= imageLoadingCnt)
            {
                [self hideBusyIndicator];
            }
            
            [self checkProcess];
        }
        else {
            [self hideBusyIndicator];
            [self alertError:@"Error" message:descriptor.error];
        }
    }
    else if([event.name isEqualToString:@"PP420"] || [event.name isEqualToString:@"PP410"]) {
        if (!isUpdateImage1 && !isUpdateImage2 && !isUpdateImage3) {
            [self hideBusyIndicator];
        }
        
        if(descriptor.success == true) {
            NSString     *response = [[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding];
            NSDictionary *result   = getJSONObj(response);
            
            
            NSArray *vehicles               = [result valueForKey:@"provider_vehicles"];
            NSDictionary *vehicle           = [result valueForKey:@"provider_vehicle"];
            
            if([event.name isEqualToString:@"PP420"]) {
                if ([Utils isArray:vehicles]) {
                    vehicle = vehicles[0];
                }
                else if ([Utils isDictionary:vehicle]) {
                    vehicle               = [result valueForKey:@"provider_vehicle"];
                }
            }
            else if([event.name isEqualToString:@"PP410"]) {
                vehicle              = [result valueForKey:@"provider_vehicle"];
            }
            
            if (vehicle != nil) {
                appDelegate.becomeVseq = vehicle[@"vseq"];
                
                imageLoadingMax = 0;
                imageLoadingCnt = 0;
                
                if (isUpdateImage1) {
                    imageLoadingMax++;
                    [self onPP411];
                }
                if (isUpdateImage2) {
                    imageLoadingMax++;
                    [self onPP412];
                }
                if (isUpdateImage3) {
                    imageLoadingMax++;
                    [self onPP413];
                }
                if (isUpdateImage4) {
                    imageLoadingMax++;
                    [self onPP414];
                }
                
                if (!imageLoadingMax) {
                    // 페이지 이동
                    if (_isNewVehicle) {
                        // 이전으로 이동
                        [self removeEventListeners];
                        [self.navigationController popViewControllerAnimated:YES];
                    }
                    else {
                        //[[[TXApp instance] getSettings] setFDKeychain:@"BCD_STEP" value:@"3"];
                        //TXBecomeFinishVC *vc = [[TXBecomeFinishVC alloc] initWithNibName:@"TXBecomeFinishVC" bundle:[NSBundle mainBundle]];
                        TXAboutVC *vc = [[TXAboutVC alloc] initWithNibName:@"TXAboutVC" bundle:[NSBundle mainBundle] step:3];
                        [self.navigationController pushViewController:vc animated:YES];
                    }
                    
                }
            }
        }
        else {
            [self alertError:@"Error" message:descriptor.error];
        }
    }
    //else if([event.name isEqualToString:@"PP411"] || [event.name isEqualToString:@"PP412"] || [event.name isEqualToString:@"PP413"] || [event.name isEqualToString:@"PP414"]) {
    else if([event.name isEqualToString:@"PP629"]) {
        imageLoadingCnt++;
        
        NSDictionary *param;
//        if (request.body && ([request.body isKindOfClass:[NSData class]] && ((NSData *)request.body).length > 0)) {
//            param = [Utils getDataOfQueryString:request.body];
//        }
        if (![Utils isDictionary:param]) {
            param = [Utils getDataOfQueryString:request.reqUrl];
        }
        
        if(descriptor.success == true) {
            NSString     *response = [[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding];
            NSDictionary *result   = getJSONObj(response);
            
            
            NSDictionary *profile              = [result valueForKey:@"provider_device"];
            NSString *pseq = profile[@"pseq"];
            
            if ([pseq length]) {
//                NSLog(@"%@ OK",event.name);

                if ([param[@"name"] isEqualToString:@"vehicle"]) {
                    isUpdateImage1 = NO;
                }
                else if ([param[@"name"] isEqualToString:@"registration"]) {
                    isUpdateImage2 = NO;
                }
                else if ([param[@"name"] isEqualToString:@"insurance"]) {
                    isUpdateImage3 = NO;
                }
                else if ([param[@"name"] isEqualToString:@"inspection"]) {
                    isUpdateImage4 = NO;
                }
                
                if (imageLoadingMax <= imageLoadingCnt)
                {
                    [self hideBusyIndicator];
                    // 페이지 이동
                    if (_isNewVehicle) {
                        // 이전으로 이동
                        [self removeEventListeners];
                        [self.navigationController popViewControllerAnimated:YES];
                    }
                    else {
                        //[[[TXApp instance] getSettings] setFDKeychain:@"BCD_STEP" value:@"3"];
                        //TXBecomeFinishVC *vc = [[TXBecomeFinishVC alloc] initWithNibName:@"TXBecomeFinishVC" bundle:[NSBundle mainBundle]];
                        TXAboutVC *vc = [[TXAboutVC alloc] initWithNibName:@"TXAboutVC" bundle:[NSBundle mainBundle] step:3];
                        [self.navigationController pushViewController:vc animated:YES];
                    }
                }
            }
            else {
                if (imageLoadingMax <= imageLoadingCnt)
                {
                    [self hideBusyIndicator];
                    // 페이지 이동
                    if (_isNewVehicle) {
                        // 이전으로 이동
                        [self removeEventListeners];
                        [self.navigationController popViewControllerAnimated:YES];
                    }
                    else {
                        //[[[TXApp instance] getSettings] setFDKeychain:@"BCD_STEP" value:@"3"];
                        //TXBecomeFinishVC *vc = [[TXBecomeFinishVC alloc] initWithNibName:@"TXBecomeFinishVC" bundle:[NSBundle mainBundle]];
                        TXAboutVC *vc = [[TXAboutVC alloc] initWithNibName:@"TXAboutVC" bundle:[NSBundle mainBundle] step:3];
                        [self.navigationController pushViewController:vc animated:YES];
                    }
                }
            }
        }
        else {
            [self hideBusyIndicator];
            [self alertError:@"Error" message:descriptor.error];
        }
    }
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker  dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Image Picker Delegate -
//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    TOCropViewController *cropController = [[TOCropViewController alloc] initWithCroppingStyle:self.croppingStyle image:image];
    cropController.delegate = self;
    
    //If profile picture, push onto the same navigation stack
    if (self.croppingStyle == TOCropViewCroppingStyleCircular) {
        [picker pushViewController:cropController animated:YES];
    }
    else { //otherwise dismiss, and then present from the main controller
        [picker dismissViewControllerAnimated:YES completion:^{
            [self presentViewController:cropController animated:YES completion:nil];
        }];
    }
}

#pragma mark - Cropper Delegate -
- (void)cropViewController:(TOCropViewController *)cropViewController didCropToImage:(UIImage *)image withRect:(CGRect)cropRect angle:(NSInteger)angle
{
    [self updateImageViewWithImage:image fromCropViewController:cropViewController];
}

- (void)cropViewController:(TOCropViewController *)cropViewController didCropToCircularImage:(UIImage *)image withRect:(CGRect)cropRect angle:(NSInteger)angle
{
    [self updateImageViewWithImage:image fromCropViewController:cropViewController];
}

- (void)updateImageViewWithImage:(UIImage *)croppedImage fromCropViewController:(TOCropViewController *)cropViewController
{
    if (self->_alertIdx == 11013 || self->_alertIdx == 11012) {
        UIImageView *imageView = (UIImageView *)[self.vbody viewWithTag:11015];
        if ([imageView.image isEqual:croppedImage]) {
            [cropViewController.presentingViewController dismissViewControllerAnimated:YES completion:nil];
            return;
        }
    }
    else if (self->_alertIdx == 11213 || self->_alertIdx == 11212) {
        UIImageView *imageView = (UIImageView *)[self.vbody viewWithTag:11215];
        if ([imageView.image isEqual:croppedImage]) {
            [cropViewController.presentingViewController dismissViewControllerAnimated:YES completion:nil];
            return;
        }
    }
    else if (self->_alertIdx == 11313 || self->_alertIdx == 11312) {
        UIImageView *imageView = (UIImageView *)[self.vbody viewWithTag:11315];
        if ([imageView.image isEqual:croppedImage]) {
            [cropViewController.presentingViewController dismissViewControllerAnimated:YES completion:nil];
            return;
        }
    }
    else if (self->_alertIdx == 11413 || self->_alertIdx == 11412) {
        UIImageView *imageView = (UIImageView *)[self.vbody viewWithTag:11415];
        if ([imageView.image isEqual:croppedImage]) {
            [cropViewController.presentingViewController dismissViewControllerAnimated:YES completion:nil];
            return;
        }
    }
    
    //---------------------------------------------------------------------------------------------------------------------------------------------
    //self.imageView.image = croppedImage;
    //[self shadowProfileImage:croppedImage];
    UIImage *image = [Utils resizeImage:croppedImage];
    NSString* picture = [Utils encodeToBase64String:image];
    
    if (self->_alertIdx == 11013 || self->_alertIdx == 11012) {
        UIButton *btn = (UIButton *)[self.vbody viewWithTag:11012];
        [btn setImage:nil forState:UIControlStateNormal];
        UIImageView *imageView = (UIImageView *)[self.vbody viewWithTag:11015];
        imageView.image = image;
        [propertyMap setObject:picture forKey:@"vehicle"];
        isUpdateImage1 = YES;
    }
    else if (self->_alertIdx == 11213 || self->_alertIdx == 11212) {
        UIButton *btn = (UIButton *)[self.vbody viewWithTag:11212];
        [btn setImage:nil forState:UIControlStateNormal];
        UIImageView *imageView = (UIImageView *)[self.vbody viewWithTag:11215];
        imageView.image = image;
        [propertyMap setObject:picture forKey:@"registration"];
        isUpdateImage2 = YES;
    }
    else if (self->_alertIdx == 11313 || self->_alertIdx == 11312) {
        UIButton *btn = (UIButton *)[self.vbody viewWithTag:11312];
        [btn setImage:nil forState:UIControlStateNormal];
        UIImageView *imageView = (UIImageView *)[self.vbody viewWithTag:11315];
        imageView.image = image;
        [propertyMap setObject:picture forKey:@"insurance"];
        isUpdateImage3 = YES;
    }
    else if (self->_alertIdx == 11413 || self->_alertIdx == 11412) {
        UIButton *btn = (UIButton *)[self.vbody viewWithTag:11412];
        [btn setImage:nil forState:UIControlStateNormal];
        UIImageView *imageView = (UIImageView *)[self.vbody viewWithTag:11415];
        imageView.image = image;
        [propertyMap2 setObject:picture forKey:@"inspection"];
        isUpdateImage4 = YES;
    }
    
    [self checkProcess];
    
    [cropViewController.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - Private methods

- (void)showCamera
{
    self.croppingStyle = TOCropViewCroppingStyleDefault;
    
    UIImagePickerController *standardPicker = [[UIImagePickerController alloc] init];
    standardPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    standardPicker.allowsEditing = NO;
    standardPicker.delegate = self;
    [self presentViewController:standardPicker animated:YES completion:nil];

}

- (void)openPhotoAlbum
{
    self.croppingStyle = TOCropViewCroppingStyleDefault;
    
    UIImagePickerController *standardPicker = [[UIImagePickerController alloc] init];
    standardPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    standardPicker.allowsEditing = NO;
    standardPicker.delegate = self;
    [self presentViewController:standardPicker animated:YES completion:nil];

}

- (void)editPhoto
{
    UIImageView *imageView;
    if (self->_alertIdx == 11012 || self->_alertIdx == 11013) {
        imageView = (UIImageView *)[self.vbody viewWithTag:11015];
    }
    else if (self->_alertIdx == 11212 || self->_alertIdx == 11213) {
        imageView = (UIImageView *)[self.vbody viewWithTag:11215];
    }
    else if (self->_alertIdx == 11312 || self->_alertIdx == 11313) {
        imageView = (UIImageView *)[self.vbody viewWithTag:11315];
    }
    else if (self->_alertIdx == 11412 || self->_alertIdx == 11413) {
        imageView = (UIImageView *)[self.vbody viewWithTag:11415];
    }
    
    if (imageView.image) {
        TOCropViewController *cropController = [[TOCropViewController alloc] initWithCroppingStyle:self.croppingStyle image:imageView.image];
        cropController.delegate = self;
        
        
        [self presentViewController:cropController animated:YES completion:nil];
    }
    
}

#pragma mark - UITextField Delegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (self->_alertIdx == 3) {
        UITextField *secondTextField = _securityAlertView.textFieldsArray[(textField.tag == 1 ? 2 : 1)];
        
        NSMutableString *currentString = textField.text.mutableCopy;
        
        [currentString replaceCharactersInRange:range withString:string];
        
        [_securityAlertView setButtonAtIndex:0 enabled:(currentString.length > 2 && secondTextField.text.length > 2)];
    }
    else {
        UITextField *secondTextField = _securityAlertView.textFieldsArray[(textField.tag == 0 ? 1 : 0)];
        
        NSMutableString *currentString = textField.text.mutableCopy;
        
        [currentString replaceCharactersInRange:range withString:string];
        
        [_securityAlertView setButtonAtIndex:0 enabled:(currentString.length > 2 && secondTextField.text.length > 2)];
    }
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField.tag < 1)
        [_securityAlertView.textFieldsArray[(textField.tag + 1)] becomeFirstResponder];
    else
    {
        if ([_securityAlertView isButtonEnabledAtIndex:0])
            [_securityAlertView dismissAnimated:YES completionHandler:nil];
        else
            [textField resignFirstResponder];
    }
    
    return YES;
}


#pragma mark -
#pragma mark Validator
#pragma mark -
-(void) resignKeyboard
{
    [self.view endEditing:YES];
}

- (IBAction)validateAction:(id)sender
{
}

#pragma ValidatorDelegate - Delegate methods

- (void) preValidation
{
}

- (void)onSuccess
{
}

- (void)onFailure:(Rule *)failedRule
{
    
//    NSMutableDictionary *options = [@{kCRToastNotificationTypeKey               : @(CRToastTypeNavigationBar),
//                                      kCRToastNotificationPresentationTypeKey   : @(CRToastPresentationTypeCover),
//                                      kCRToastUnderStatusBarKey                 : @(YES),
//                                      kCRToastTextKey                           : failedRule.failureMessage,
//                                      kCRToastFontKey                           : [UIFont systemFontOfSize:14],
//                                      kCRToastTextAlignmentKey                  : @(NSTextAlignmentCenter),
//                                      kCRToastTimeIntervalKey                   : @(2),
//                                      kCRToastAnimationInTypeKey                : @(CRToastAnimationTypeGravity),
//                                      kCRToastAnimationOutTypeKey               : @(CRToastAnimationTypeGravity),
//                                      kCRToastAnimationInDirectionKey           : @(CRToastAnimationDirectionTop),
//                                      kCRToastAnimationOutDirectionKey          : @(CRToastAnimationDirectionTop),
//                                      kCRToastNotificationPreferredPaddingKey   : @(15)} mutableCopy];
//    [CRToastManager showNotificationWithOptions:options
//                                completionBlock:^{
//                                    NSLog(@"Completed");
//                                }];
    
    [WToast showWithText:failedRule.failureMessage duration:kWTShort roundedCorners:NO gravity:kWTGravityTop];
}

@end
