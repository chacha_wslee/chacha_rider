//
//  TXProfileVC.m
//  Taxi
//

//

#import "TXBecomeFinishVC.h"
#import "LGAlertView.h"
#import "RTLabel.h"
#import "utilities.h"

@interface TXBecomeFinishVC ()

@property (strong, nonatomic) IBOutlet RTLabel *lbalert;
@property (strong, nonatomic) IBOutlet RTLabel *lbalert2;

@end

@implementation TXBecomeFinishVC

-(void)viewDidLoad {
    
    [super viewDidLoad];
    
    [[IQKeyboardManager sharedManager] setEnable:NO];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [super configureBottomLine];
    UIView *line = (UIView*)[[super navigationView] viewWithTag:1500];
    CGRect rect = line.frame;
    rect.size.height = 3;
    line.frame = rect;
    line.backgroundColor = HEXCOLOR(0x111111FF);
    UIView *n = [super navigationView];
    NSInteger _y = n.frame.origin.y + n.frame.size.height;
    _y = _y + 20;
    
    self.baseView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 0)];
    
    // text
    self.lbalert = [[RTLabel alloc] initWithFrame:CGRectMake(kBasicMargin, 0, self.baseView.frame.size.width - kBasicMargin*2, 0)];
    [self.lbalert setTextColor:UIColorLabelText];
    [self.lbalert sizeToFit];
    self.lbalert.backgroundColor = [UIColor clearColor];
    self.lbalert.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    [self.lbalert setFont:[UIFont fontWithName:@"AppleSDGothicNeo-Medium" size:22]];
    self.lbalert.textColor = [UIColor blackColor];
    self.lbalert.text = LocalizedStr(@"Menu.BecomeADriver.Finish.List.text1");
    [self.baseView addSubview:self.lbalert];
    
    // text
    self.lbalert2 = [[RTLabel alloc] initWithFrame:CGRectMake(kBasicMargin, 0, self.baseView.frame.size.width - kBasicMargin*2, 0)];
    [self.lbalert2 setTextColor:UIColorLabelText];
    [self.lbalert2 sizeToFit];
    self.lbalert2.backgroundColor = [UIColor clearColor];
    self.lbalert2.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    [self.lbalert2 setFont:[UIFont fontWithName:@"AppleSDGothicNeo-Regular" size:14]];
    self.lbalert2.textColor = HEXCOLOR(0x666666FF);
    self.lbalert2.text = LocalizedStr(@"Menu.BecomeADriver.Finish.List.text2");
    [self.baseView addSubview:self.lbalert2];
    
    // become a drive를 완료하면 상태를 바꾼다.
    appDelegate.isBecomeMode = NO;
    
    self.myScrollView.backgroundColor = [UIColor whiteColor];
    self.baseView.backgroundColor = [UIColor whiteColor];
    
    self.myScrollView.frame = CGRectMake(0, _y, self.view.frame.size.width, self.view.frame.size.height - _y - kBottomButtonHeight);
    
    self.myScrollView.contentSize = CGSizeMake(self.baseView.frame.size.width, self.baseView.frame.size.height);
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0, 0.0, 0, 0.0);
    self.myScrollView.contentInset = contentInsets;
    self.myScrollView.scrollIndicatorInsets = contentInsets;
    self.myScrollView.scrollsToTop = YES;
    [self.myScrollView addSubview:self.baseView];
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    // 1
    CGSize optimumSize = [_lbalert optimumSize];
    _lbalert.frame = CGRectMake(_lbalert.frame.origin.x, _lbalert.frame.origin.y, _lbalert.frame.size.width,optimumSize.height);
    
    // 2
    optimumSize = [_lbalert2 optimumSize];
    _lbalert2.frame = CGRectMake(_lbalert2.frame.origin.x, _lbalert.frame.origin.y + _lbalert.frame.size.height + 20, _lbalert2.frame.size.width,optimumSize.height);

    CGRect rect = self.baseView.frame;
    rect.size.height = (_lbalert2.frame.origin.y + _lbalert2.frame.size.height) + 20;
    self.baseView.frame = rect;
    
    self.myScrollView.contentSize = CGSizeMake(self.baseView.frame.size.width, self.baseView.frame.size.height);
}

#pragma mark - Configure

-(void)configureStyles {
    [super configureStyles];
    
    //[super navigationType11X:[UIImage imageNamed:@"btn_back"] centerText:LocalizedStr(@"Menu.BecomeADriver.Finish.title") rightText:nil];
    //[super navigationType111:[UIImage imageNamed:@"btn_back"] centerText:LocalizedStr(@"Menu.BecomeADriver.Finish.title") rightImage:[UIImage imageNamed:@"ic_x_black"]];
    [super navigationType01X:[UIImage imageNamed:@"btn_back"] centerText:LocalizedStr(@"Menu.BecomeADriver.Finish.title")];
    
    [self nextButtonOn:YES];
}

- (void)nextButtonOn:(BOOL)on
{
    UIButton *btn = [super.view viewWithTag:999];
    btn.enabled = on;
    if (on)
        [btn setTitleColor:UIColorButtonText forState:UIControlStateNormal];
    else
        [btn setTitleColor:UIColorBasicTextOff forState:UIControlStateNormal];
}

-(void)configure {
    [super configure];
    [self configureConfig];
    UIView *n = [super navigationView];
    [n setBackgroundColor:[UIColor whiteColor]];
    
    UIButton *btn = [Utils addBottomButton:LocalizedStr(@"Button.OK") target:self selector:@selector(onNaviButtonClick:) color:YES];
    [self.view addSubview:btn];
}

-(void) configureConfig {

    
}

#pragma mark - Action
-(IBAction)onNaviButtonClick:(id)sender {
    UIButton *btn = (UIButton*)sender;
    NSLog(@"onNaviButtonClick");
    //[super onNaviButtonClick:sender];
    
    if (btn.tag == 1100 || btn.tag == 1101) {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else if (btn.tag == 999) {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}

-(IBAction)onBackClick
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
