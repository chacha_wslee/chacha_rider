//
//  TXProfileVC.h
//  Taxi
//

//

#import "TXUserVC.h"
#import "REFormattedNumberField.h"

@interface TXBecomeVC : TXUserVC {

}

@property (strong, nonatomic) IBOutlet UIScrollView *myScrollView;
@property (strong, nonatomic) IBOutlet UIView *vbody;

@property (nonatomic, strong) IBOutlet UIImageView* imgProfile;
@property (nonatomic, strong) IBOutlet UIImageView* imgDriver;

@property (nonatomic, strong) IBOutlet UIButton* btnAgree;

@end
