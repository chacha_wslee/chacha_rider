//
//  TXProfileVC.h
//  Taxi
//

//

#import "TXUserVC.h"
#import "REFormattedNumberField.h"

@interface TXBecomeVehicleVC : TXUserVC {

}

@property (strong, nonatomic) IBOutlet UIScrollView *myScrollView;
@property (strong, nonatomic) IBOutlet UIView *vbody;

@property (nonatomic, strong) IBOutlet UIImageView* imgProfile;
@property (nonatomic, strong) IBOutlet UIImageView* imgDriver;

@property (nonatomic, strong) IBOutlet UIButton* btnForm;

@property (nonatomic, strong) IBOutlet UIButton* btnAgree;

@property (nonatomic, assign) BOOL isNewVehicle;

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil isnew:(BOOL)isnew;
    
@end
