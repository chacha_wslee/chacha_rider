//
//  TXProfileVC.m
//  Taxi
//

//

#import "TXBecomeVC.h"
#import "LGAlertView.h"
//#import "PECropViewController.h"
#import "TOCropViewController.h"
//#import "EXPhotoViewer.h"
#import "TXBecomeVehicleVC.h"
#import "utilities.h"

#import "Validator.h"
#import "WToast.h"
//#import "CRToast.h"

#define MaxProcessCnt 6
#define NSProcessPct [NSString stringWithFormat:@"%d%@",(int)(processPct*100/MaxProcessCnt),LocalizedStr(@"Menu.BecomeADriver.Header.text2")]

@interface TXBecomeVC () <UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIActionSheetDelegate, TOCropViewControllerDelegate, UIViewControllerTransitioningDelegate,ValidatorDelegate>{
    int userId;
    NSArray *_msgList;
    UILabel *lbProcessInfo;
    int processPct;
    
    NSString *fname;
    NSString *lname;
    
    NSArray *dataArray;
    
    BOOL isUpdateImage1;
    BOOL isUpdateImage2;
    BOOL isUpdateImage3;
    
    NSInteger imageLoadingMax;
    NSInteger imageLoadingCnt;
    
    id observer1;
    id observer2;
    id observer3;
} 
@property (nonatomic, assign) TOCropViewCroppingStyle croppingStyle; //The cropping style
@property (strong, nonatomic) LGAlertView *securityAlertView;
@end

@implementation TXBecomeVC {
    NSMutableDictionary *propertyMap;
    int _alertIdx;
}


-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    
    if(self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        self->_alertIdx = 1;
    }
    
    return self;
}

-(void)viewDidLoad {
    [super viewDidLoad];
    
    [[IQKeyboardManager sharedManager] setEnable:NO];
    
    observer1 = [[NSNotificationCenter defaultCenter] addObserverForName:@"NotificationPhotoAlbum"
                                                                  object:nil
                                                                   queue:[NSOperationQueue mainQueue]
                                                              usingBlock:^(NSNotification *notification) {
                                                                  
                                                                  [self openPhotoAlbum];
                                                                  
                                                              }];
    observer2 = [[NSNotificationCenter defaultCenter] addObserverForName:@"NotificationShowCamera"
                                                                  object:nil
                                                                   queue:[NSOperationQueue mainQueue]
                                                              usingBlock:^(NSNotification *notification) {
                                                                  
                                                                  [self showCamera];
                                                                  
                                                              }];
//    observer3 = [[NSNotificationCenter defaultCenter] addObserverForName:@"NotificationEditPhoto"
//                                                                  object:nil
//                                                                   queue:[NSOperationQueue mainQueue]
//                                                              usingBlock:^(NSNotification *notification) {
//                                                                  
//                                                                  NSLog(@"notification:%@",notification);
//                                                                  [self editPhoto];
//                                                                  
//                                                              }];
    
    isUpdateImage1 = NO;
    isUpdateImage2 = NO;
    isUpdateImage3 = NO;
    imageLoadingMax = 0;
    imageLoadingCnt = 0;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self checkProcess];
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    //[self removeEventListeners];

}

#pragma mark - Configure

-(void)configureStyles {
    [super configureStyles];
    
//    if (appDelegate.isDriverMode) {
//        [super navigationType01X:[UIImage imageNamed:@"btn_back"] centerText:LocalizedStr(@"Menu.Account.title")];
//    }
//    else {
//        [super navigationType01X:[UIImage imageNamed:@"btn_back"] centerText:LocalizedStr(@"Menu.Account.title")];
//    }
//    
    //[super navigationType11X:[UIImage imageNamed:@"btn_back"] centerText:LocalizedStr(@"Menu.BecomeADriver.title") rightText:LocalizedStr(@"Intro.Next.title")];
    [super navigationType01X:[UIImage imageNamed:@"btn_back"] centerText:LocalizedStr(@"Menu.BecomeADriver.title")];
    [self nextButtonOn:NO];
}

- (void)nextButtonOn:(BOOL)on
{
    UIButton *btn = [super.view viewWithTag:999];
    btn.enabled = on;
    if (on)
        [btn setTitleColor:UIColorButtonText forState:UIControlStateNormal];
    else
        [btn setTitleColor:UIColorBasicTextOff forState:UIControlStateNormal];
}

-(void)configure {
    [super configure];
    [super configureAccount];
    [self configureConfig];
    
    UIView *n = [super navigationView];
    NSInteger _y = n.frame.origin.y + n.frame.size.height;
    
    self->userId = [[[[self->model getApp] getSettings] getUserId] intValue];
    
    UIButton *button = [[super navigationView] viewWithTag:9999];
    _imgProfile = [[super navigationView] viewWithTag:9998];
    [button addTarget:self action:@selector(onPhoto:) forControlEvents:UIControlEventTouchUpInside];

    _imgProfile.layer.cornerRadius = _imgProfile.frame.size.width / 2;
    _imgProfile.clipsToBounds = YES;
    _imgProfile.contentMode = UIViewContentModeScaleAspectFill;
    _imgProfile.layer.masksToBounds = YES;
    //_imgProfile.alpha = 0.7;
    
    if (appDelegate.isDriverMode) {
        _imgProfile.image = appDelegate.imgProfileDriver;
        if (appDelegate.imgProfileDriver == nil) {
            [self saveProfileImage:[UIImage imageNamed:@"ic_user_white"] withCode:@"PP605"];
        }
    }
    
    //------------------------------------------------------------------------------------------------------------------------
    propertyMap = [[NSMutableDictionary alloc] init];
    //------------------------------------------------------------------------------------------------------------------------
//    UIView *labelView = [Utils addDoubleLabelView:self.view underView:nil dim:0];
//    CGRect rect = labelView.frame;
//    rect.origin.y = kTopHeight + kNaviHeight;
//    labelView.frame = rect;
//    UILabel *label = [labelView viewWithTag:11];
//    label.text = LocalizedStr(@"Menu.BecomeADriver.Header.text1");
//    lbProcessInfo = [labelView viewWithTag:12];
    UIView *bgview;
    bgview = [Utils addViewButtonText:LocalizedStr(@"Intro.Next.title") target:self selector:@selector(onNaviButtonClick:) color:YES];
    [self.view addSubview:bgview];
    
    lbProcessInfo = (UILabel*)[bgview viewWithTag:998];
    processPct = 0;
    lbProcessInfo.text = NSProcessPct;
//    [self.view addSubview:labelView];
    //------------------------------------------------------------------------------------------------------------------------
    ((UILabel *)[self.vbody viewWithTag:11011]).text = LocalizedStr(@"Menu.BecomeADriver.List.text1");
    ((UILabel *)[self.vbody viewWithTag:11211]).text = LocalizedStr(@"Menu.BecomeADriver.List.text3");
    ((UILabel *)[self.vbody viewWithTag:11311]).text = LocalizedStr(@"Menu.BecomeADriver.List.text4");
    ((UILabel *)[self.vbody viewWithTag:11711]).text = LocalizedStr(@"Menu.BecomeADriver.List.text17");
    ((UILabel *)[self.vbody viewWithTag:11811]).text = LocalizedStr(@"Menu.BecomeADriver.List.text8");
    
    ((UILabel *)[self.vbody viewWithTag:13011]).hidden = YES;
    ((UILabel *)[self.vbody viewWithTag:13011]).text = LocalizedStr(@"Menu.BecomeADriver.footer.text");
    ((UILabel *)[self.vbody viewWithTag:13011]).textColor = UIColorDefault;
    
    //[(UIImageView *)[self.vbody viewWithTag:11515] setClipsToBounds:YES];
    //[(UIImageView *)[self.vbody viewWithTag:11615] setClipsToBounds:YES];
    //[(UIImageView *)[self.vbody viewWithTag:12715] setClipsToBounds:YES];
    //------------------------------------------------------------------------------------------------------------------------
    self.myScrollView.backgroundColor = [UIColor whiteColor];
    self.vbody.backgroundColor = UIColorLabelBG;
    self.vbody.frame = CGRectMake(self.vbody.frame.origin.x, self.vbody.frame.origin.y, self.view.frame.size.width, self.vbody.frame.size.height);
    self.myScrollView.frame = CGRectMake(0,
                                         _y,
                                         self.view.frame.size.width,
                                         self.view.frame.size.height - _y - kBottomButtonHeight);
    
    self.myScrollView.contentSize = CGSizeMake(self.vbody.frame.size.width, self.vbody.frame.size.height);
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0, 0.0, 0, 0.0);
    self.myScrollView.contentInset = contentInsets;
    self.myScrollView.scrollIndicatorInsets = contentInsets;
    self.myScrollView.scrollsToTop = YES;
    [self.myScrollView addSubview:self.vbody];
    //------------------------------------------------------------------------------------------------------------------------
    // 2. init event
    _msgList = @[
                    @[@"PP300",@""], // loading
                    @[@"PP310",@""], // modify
                    @[@"PP320",@""], // save
                    
                    @[@"PP605",@""], // loading
                    @[@"PP628",@""], // image save
                    
//                    @[@"PP311",@""], // modify
//                    @[@"PP312",@""], // modify
//                    @[@"PP313",@""], // modify
                    
//                    @[@"PP301",@""], // loading
//                    @[@"PP302",@""], // loading
//                    @[@"PP303",@""], // loading
                    ];
    
    [self registerEventListeners];
    
    if (appDelegate.isDriverMode) {
        [self configureDriver];
    }
}

-(void) configureConfig {

    
}

-(void) configureDriver {
    // 1. init var
    
    //NSDictionary *provider = [appDelegate.dicDriver objectForKey:@"provider"];
    NSString *pseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.PSEQ];
    
    //    REFormattedNumberField *txtNum = [[REFormattedNumberField alloc] initWithFrame:CGRectZero];
    //    txtNum.format = @"+X (XXX) XXX-XXXX";
    //    txtNum.text = user[@"utelno"];
    
//    ((UILabel *)[self.vbody viewWithTag:11212]).text = provider[@"uid"];
//    ((UILabel *)[self.vbody viewWithTag:11312]).text = provider[@"utelno"];//txtNum.text;
    
    // 4. settup
    if (pseq != nil && ![pseq isEqualToString:@""]) {
        appDelegate.becomePseq = pseq;
        // 조회
        [self onPP300:pseq];
    }
    else if (![appDelegate.becomePseq isEqualToString:@""]) {
        // 조회
        [self onPP300:appDelegate.becomePseq];
    }
    
}


-(void) registerEventListeners {
    for (NSArray *msg in _msgList) {
        [self->model addEventListener:self forEvent:[msg objectAtIndex:0] eventParams:nil];
    }
}

-(void) removeEventListeners {
    for (NSArray *msg in _msgList) {
        [self->model removeEventListener:self forEvent:[msg objectAtIndex:0]];
    }
    
    [[NSNotificationCenter defaultCenter] removeObserver:observer1];
    observer1 = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:observer2];
    observer2 = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:observer3];
    observer3 = nil;
    
//    [[NSNotificationCenter defaultCenter] removeObserver:self
//                                                    name:@"NotificationPhotoAlbum"
//                                                  object:nil];
//    [[NSNotificationCenter defaultCenter] removeObserver:self
//                                                    name:@"NotificationShowCamera"
//                                                  object:nil];
//    [[NSNotificationCenter defaultCenter] removeObserver:self
//                                                    name:@"NotificationEditPhoto"
//                                                  object:nil];
}

-(void) checkProcess {
    processPct = (int)[propertyMap count];
    lbProcessInfo.text = NSProcessPct;
    
    //if (processPct == MaxProcessCnt && _btnAgree.tag == 1) {
    if (processPct == MaxProcessCnt) {
        [self nextButtonOn:YES];
    }
    else {
        [self nextButtonOn:NO];
    }
    
}

-(void)updateTrimString:(NSString*)str key:(NSString*)key
{
    NSString *trimmedString = [Utils trimStringOnly:str];
    
    if ([trimmedString isEqualToString:@""]) {
        [propertyMap removeObjectForKey:key];
    }
    else {
        [propertyMap setValue:str forKey:key];
    }
}

#pragma mark - Action
-(IBAction)onNaviButtonClick:(id)sender {
    UIButton *btn = (UIButton*)sender;
    NSLog(@"onNaviButtonClick");
    //[super onNaviButtonClick:sender];
    
    if (btn.tag == 1100 || btn.tag == 1101) {
        [self removeEventListeners];
        [self.navigationController popViewControllerAnimated:YES];
    }
    else if(btn.tag == 999) {
        if (![appDelegate.becomePseq isEqualToString:@""]) {
            [self onPP310];
        }
        else {
            [self onPP320];
        }
    }
}


#pragma mark - IBAction
-(IBAction)onPhoto:(id)sender {
    
    self->_alertIdx = (int)((UIButton*)sender).tag;
    NSLog(@"_alertIdx:%d",self->_alertIdx);
    NSArray *items = @[LocalizedStr(@"Menu.Driver.Profile.Image.album")];
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        items = @[LocalizedStr(@"Menu.Driver.Profile.Image.album"), LocalizedStr(@"Menu.Driver.Profile.Image.camera"), LocalizedStr(@"Menu.Driver.Profile.Image.edit")];
    }
    
    LGAlertView *alertView = [[LGAlertView alloc] initWithTitle:nil
                                                        message:LocalizedStr(@"Menu.Driver.Profile.Image.title2")
                                                          style:LGAlertViewStyleActionSheet
                                                   buttonTitles:items
                                              cancelButtonTitle:LocalizedStr(@"Button.CANCEL")
                                         destructiveButtonTitle:nil
                                                  actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                                                      NSLog(@"actionHandler, %@, %lu", title, (long unsigned)index);
                                                      
                                                      if (index == 0) {
                                                          [self openPhotoAlbum];
                                                      }
                                                      else if (index == 1) {
                                                          [self showCamera];
                                                      }
                                                      else if (index == 2) {
                                                          UIImageView *imageView;
                                                          if (self->_alertIdx == 9999) {
                                                              imageView = (UIImageView *)[[super navigationView] viewWithTag:9998];
                                                          }
                                                          
                                                          if (imageView.image) {
                                                              TOCropViewController *cropController = [[TOCropViewController alloc] initWithCroppingStyle:self.croppingStyle image:imageView.image];
                                                              cropController.delegate = self;
                                                              [self presentViewController:cropController animated:YES completion:nil];
                                                          }
                                                      }
                                                  }
                                                  cancelHandler:^(LGAlertView *alertView) {
                                                      NSLog(@"cancelHandler");
                                                  }
                                             destructiveHandler:^(LGAlertView *alertView) {
                                                 NSLog(@"destructiveHandler");
                                             }];
    [Utils initAlertButtonColor:alertView];
    [alertView showAnimated:YES completionHandler:nil];
}

-(IBAction)onClickAgree:(id)sender {
    
    UIButton *btn = (UIButton*)sender;
    if (btn.tag == 1) {
        [btn setImage:[UIImage imageNamed:@"form_radio_normal"] forState:UIControlStateNormal];
        btn.tag = 0;
    }
    else {
        [btn setImage:[UIImage imageNamed:@"form_radio_chk"] forState:UIControlStateNormal];
        btn.tag = 1;
    }
}

-(IBAction)onClick:(id)sender {
    
    UIButton *btn = (UIButton*)sender;
    
    if (btn.tag == 9999) {
        self->_alertIdx = (int)btn.tag;
        if ([propertyMap valueForKey:@"picture"]) {
            [self onPhoto:sender];
        }
        else {
            [self onPhoto:btn];
        }
        return;
    }
    
    NSInteger tag = (btn.tag / 100) % 100 + 1 - 10;
    
    NSLog(@"tag:%ld",(long)tag);
    //------------------------------------------------------------------------------------------------
//    if (tag == 1) {
//        TOAST_MSG(@"Menu.Account.List.none.edit");
//    }
//    else
    if (tag == 3) {
        TOAST_MSG(@"Menu.Account.List.none.edit");
    }
    else if (tag == 4) {
        TOAST_MSG(@"Menu.Account.List.none.edit");
    }
    else if (tag == 8) {
//        TOAST_MSG(@"Menu.Account.List.none.edit");
//        return;
        
        int __tag = (int)tag * 100 + 1000 + 12 + 10000 - 100;
        UILabel *tf = (UILabel *)[self.vbody viewWithTag:__tag];
        
        UIDatePicker *datePicker = [UIDatePicker new];
        datePicker.datePickerMode = UIDatePickerModeDate;
        datePicker.frame = CGRectMake(0.f, 0.f, datePicker.frame.size.width, 160.f);
        NSLog(@"tf.text:%@",tf.text);
        if ([tf.text length]>0) {
            NSString *calDate = tf.text;
            NSDate *date = [[NSDate alloc] init];
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
            
            [dateFormatter setDateFormat:DATE_FORMAT_YYYYMMDD];
            
            date = [dateFormatter dateFromString:calDate];
            
            [datePicker setDate:date];
        }
        
        LGAlertView *alertView = [[LGAlertView alloc] initWithViewAndTitle:LocalizedStr(@"Menu.BecomeADriver.List.text17")
                                                                   message:nil
                                                                     style:LGAlertViewStyleAlert
                                                                      view:datePicker
                                                              buttonTitles:@[@"OK"]
                                                         cancelButtonTitle:@"Cancel"
                                                    destructiveButtonTitle:nil
                                                             actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                                                                 NSLog(@"actionHandler, %@, %lu", title, (long unsigned)index);
                                                                 
                                                                 NSDateFormatter __autoreleasing *dateFormat = [[NSDateFormatter alloc] init];
                                                                 [dateFormat setTimeZone:[NSTimeZone systemTimeZone]];
                                                                 [dateFormat setDateFormat:DATE_FORMAT_YYYYMMDD];
                                                                 NSString *dateString = [dateFormat stringFromDate:datePicker.date];
                                                                 NSLog(@"dateString:%@",dateString);
                                                                 
                                                                 int __tag = (int)tag * 100 + 1000 + 12 + 10000 - 100;
                                                                 UILabel *tf = (UILabel *)[self.vbody viewWithTag:__tag];
                                                                 tf.text = dateString;
                                                                 
                                                                 [dateFormat setDateFormat:DATE_FORMAT_API];
                                                                 dateString = [dateFormat stringFromDate:datePicker.date];
                                                                 
                                                                 [self updateTrimString:dateString key:@"ssn_dob"];
                                                                 
                                                                 [self checkProcess];
                                                                 
                                                                 [alertView dismissAnimated:YES completionHandler:nil];
                                                             }
                                                             cancelHandler:^(LGAlertView *alertView) {
                                                                 [alertView dismissAnimated:YES completionHandler:nil];
                                                             }
                                                        destructiveHandler:^(LGAlertView *alertView) {
                                                            [alertView dismissAnimated:YES completionHandler:nil];
                                                        }];
        
        //alertView.heightMax = 256.f;
        //alertView.windowLevel = LGAlertViewWindowLevelAboveStatusBar;
        //alertView.separatorsColor = [UIColor redColor];
        [Utils initAlertButtonColor:alertView];
        [alertView showAnimated:YES completionHandler:^(void)
         {
         }];
        [alertView setDismissOnAction:NO];

    }
    else {
        self->_alertIdx = 1;
        [self popupInput:(int)tag];
    }
}

// 1 9
-(void)popupInput:(int)tag
{
    NSString *__title = [NSString stringWithFormat:@"Menu.BecomeADriver.List.text%d",tag];
    if (tag == 9) {
        __title = [NSString stringWithFormat:@"Menu.BecomeADriver.List.text8"];
    }
    __title = LocalizedStr(__title);
    
    NSString *__subtitle = nil;
    if (tag == 1 || tag == 2) {
        __subtitle = [NSString stringWithFormat:@"Menu.BecomeADriver.List.text%d.input",tag];
        __subtitle = LocalizedStr(__subtitle);
    }
    
    LGAlertView *alertView = [[LGAlertView alloc] initWithTextFieldsAndTitle:__title
                                                                     message:__subtitle
                                                          numberOfTextFields:1
                                                      textFieldsSetupHandler:^(UITextField *textField, NSUInteger index)
                              {
                                  if (index == 0) {
                                      textField.placeholder = @"";
                                      textField.text = @"";
                                  }
                                  
                                  textField.tag = index;
                                  //textField.delegate = self;
                                  textField.enablesReturnKeyAutomatically = YES;
                                  textField.autocapitalizationType = NO;
                                  textField.autocorrectionType = NO;
                                  textField.clearButtonMode = UITextFieldViewModeNever;
                                  textField.textAlignment = NSTextAlignmentCenter;
                                  
                                  if (tag == 5 || tag == 12 ) {
                                      [textField setKeyboardType:UIKeyboardTypeNumberPad];
                                  }
                              }
                                                                buttonTitles:@[LocalizedStr(@"Button.OK")]
                                                           cancelButtonTitle:LocalizedStr(@"Button.CANCEL")
                                                      destructiveButtonTitle:nil
                                                               actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                                                                   UITextField *secondTextField = alertView.textFieldsArray[index];
                                                                   //promoCode = secondTextField.text;
                                                                   
                                                                   NSLog(@"actionHandler, %@, %lu, %@", title, (long unsigned)index, secondTextField.text);
                                                                   
                                                                   int __tag = tag * 100 + 1000 + 12 + 10000 - 100;
                                                                   
                                                                   [self resignKeyboard];
                                                                   Validator *validator = [[Validator alloc] init];
                                                                   validator.delegate   = self;
                                                                   if (tag == 1 || tag == 2) {
                                                                       //
                                                                       [validator putRule:[Rules minLength:2 withFailureString:LocalizedStr(@"Validator.Name.text") forTextField:secondTextField]];
                                                                       //[validator putRule:[Rules checkIfAlphaNumericWithFailureString:LocalizedStr(@"Validator.Name.text") forTextField:secondTextField]];
                                                                   }
                                                                   else if (tag == 3) {
                                                                       // 이메일
                                                                       [validator putRule:[Rules minLength:8 withFailureString:LocalizedStr(@"Validator.Email.text1") forTextField:secondTextField]];
                                                                       [validator putRule:[Rules checkIsValidEmailWithFailureString:LocalizedStr(@"Validator.Email.text2") forTextField:secondTextField]];
                                                                   }
                                                                   else if (tag == 4) {
                                                                       // 전화번호
                                                                       [validator putRule:[Rules checkRange:NSMakeRange(8, 14) withFailureString:LocalizedStr(@"Validator.Telno.text") forTextField:secondTextField]];
                                                                       //[validator putRule:[Rules checkValidCountryCodeUSWithFailureString:LocalizedStr(@"Validator.Telno.text") forTextField:secondTextField]];
                                                                   }
                                                                   else if (tag == 9) {
                                                                       // 운전면허번호
                                                                       [validator putRule:[Rules minLength:4 withFailureString:LocalizedStr(@"Validator.LicenseNo.text") forTextField:secondTextField]];
                                                                       //[validator putRule:[Rules checkIfAlphaNumericWithFailureString:LocalizedStr(@"Validator.Address.text") forTextField:secondTextField]];
                                                                   }
                                                                   
                                                                   if (![validator validateForResult]) {
                                                                       [alertView.firstButton setSelected:NO];
                                                                       return;
                                                                   }
                                                                   
                                                                   ((UILabel *)[self.vbody viewWithTag:__tag]).text = secondTextField.text;
                                                                   
                                                                   if (tag == 1) {
                                                                       [self updateTrimString:secondTextField.text key:@"fname"];
                                                                   }
                                                                   else if (tag == 9) {
                                                                       [self updateTrimString:secondTextField.text key:@"licenseno"];
                                                                   }
                                                                   
                                                                   [self checkProcess];
                                                                   
                                                                   [alertView dismissAnimated:YES completionHandler:nil];
                                                               }
                                                               cancelHandler:^(LGAlertView *alertView) {
                                                                   [alertView dismissAnimated:YES completionHandler:nil];
                                                                   
                                                               }
                                                          destructiveHandler:^(LGAlertView *alertView) {
                                                              [alertView dismissAnimated:YES completionHandler:nil];
                                                          }];
    //[alertView setButtonAtIndex:0 enabled:NO];
    [Utils initAlertButtonColor:alertView];
    [alertView showAnimated:YES completionHandler:nil];
    [alertView setDismissOnAction:NO];
}

#pragma mark - Send Event
-(void)onPP300:(NSString*)pseq {
    
    NSDictionary *_propertyMap = @{
                                   @"pseq" : pseq
                                   };
    
    [self showBusyIndicator:@"Loading info ... "];
    [self->model requsetAPI:API_FUNC(NSStringFromSelector(_cmd)) property:_propertyMap];
}
-(void)onPP605:(NSString*)pseq picture:(NSString*)picture {
    
    NSDictionary *_propertyMap = @{
                                   @"pseq" : pseq,
                                   @"name" : @"picture",
                                   @"picture" : picture
                                   };
    
    [self showBusyIndicator:@"Loading ... "];
    [self->model requsetAPI:@"PP605" property:_propertyMap];
}

-(void)onPP310 {
    
    //NSString *name = [NSString stringWithFormat:@"%@;%@",[propertyMap objectForKey:@"fname"],[propertyMap objectForKey:@"lname"]];
    
    TXSettings  *settings  = [[TXApp instance] getSettings];
    NSString *userPwd = [settings getPassword];
    
    NSDictionary *_propertyMap = @{
                                   @"pseq":appDelegate.becomePseq,
                                   @"uid":propertyMap[@"uid"],
                                   @"utelno":propertyMap[@"utelno"],
                                   @"pwd":userPwd,
                                   @"name":propertyMap[@"fname"],
                                   @"ssn_dob":propertyMap[@"ssn_dob"],
                                   @"licenseno":propertyMap[@"licenseno"]
                                   };
     
    [self showBusyIndicator:@"Update ... "];
    [self->model requsetAPI:API_FUNC(NSStringFromSelector(_cmd)) property:_propertyMap];
}

-(void)onPP320 {
    
    TXSettings  *settings  = [[TXApp instance] getSettings];
    NSString *userPwd = [settings getPassword];
    
    NSDictionary *_propertyMap = @{
                                    @"uid":propertyMap[@"uid"],
                                    @"utelno":propertyMap[@"utelno"],
                                    @"pwd":userPwd,
                                    @"name":propertyMap[@"fname"],
                                    @"licenseno":propertyMap[@"licenseno"],
                                    @"ssn_dob":propertyMap[@"ssn_dob"]
                                    };
    
    [self showBusyIndicator:@"Create ... "];
    [self->model requsetAPI:API_FUNC(NSStringFromSelector(_cmd)) property:_propertyMap];
}

-(void)onPP311 {
    
    if (!isUpdateImage1) {
        return;
    }
    NSString* pseq = appDelegate.becomePseq;
    
    //UIImageView *imageView = (UIImageView *)[self.vbody viewWithTag:11515];
    UIImageView *imageView = (UIImageView *)[[super navigationView] viewWithTag:9998];
    NSString* picture = [Utils encodeToBase64String:imageView.image];
    
    NSDictionary *_propertyMap = @{
                                   @"pseq" : pseq,
                                   @"name" : @"picture",
                                   @"picture" : picture,
                                   };
    
    [self showBusyIndicator:@"Update Picture ... "];
    [self->model requsetAPI:@"PP628" property:_propertyMap];
}

-(void)onPP312 {
    if (!isUpdateImage2) {
        return;
    }
    NSString* pseq = appDelegate.becomePseq;
    
    UIImageView *imageView = (UIImageView *)[self.vbody viewWithTag:11615];
    NSString* picture = [Utils encodeToBase64String:imageView.image];
    
    NSDictionary *_propertyMap = @{
                                   @"pseq" : pseq,
                                   @"name" : @"driver",
                                   @"driver" : picture,
                                   };
    
    [self showBusyIndicator:@"Update Driver ... "];
    [self->model requsetAPI:@"PP628" property:_propertyMap];
}
-(void)onPP313 {
    if (!isUpdateImage3) {
        return;
    }
    NSString* pseq = appDelegate.becomePseq;
    
    UIImageView *imageView = (UIImageView *)[self.vbody viewWithTag:12715];
    NSString* picture = [Utils encodeToBase64String:imageView.image];
    
    NSDictionary *_propertyMap = @{
                                   @"pseq" : pseq,
                                   @"name" : @"taxpayer",
                                   @"taxpayer" : picture,
                                   };
    
    [self showBusyIndicator:@"Update Driver ... "];
    [self->model requsetAPI:@"PP628" property:_propertyMap];
}

#pragma mark - onEvent
-(void)onEvent:(TXEvent *)event eventParams:(id)subscriptionParams {
    
    TXResponseDescriptor * descriptor = [event getEventProperty:TXEvents.Params.DESCRIPTOR];
    TXRequestObj *request = [event getEventProperty:TXEvents.Params.REQUEST];
    
    //[self hideBusyIndicator];
    if([event.name isEqualToString:@"PP300"]) {
        
        if(descriptor.success == true) {
            NSString     *response = [[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding];
            NSDictionary *result   = getJSONObj(response);
            
            BOOL isNewDriver = NO;
            NSDictionary *profile              = [result valueForKey:@"provider_profile"];
            if (![Utils isDictionary:profile]) {
                profile              = [appDelegate.dicDriver valueForKey:@"provider"];
                isNewDriver = YES;
            }
            
            propertyMap[@"uid"] = profile[@"uid"];
            propertyMap[@"utelno"] = profile[@"utelno"];
            propertyMap[@"ssn_dob"] = profile[@"ssn_dob"];
            propertyMap[@"licenseno"] = profile[@"licenseno"];
            propertyMap[@"fname"] = profile[@"name"];
            
            // 값이 빠져있는 경우 보정
            if ([[Utils nullToString:propertyMap[@"uid"]] isEqualToString:@""]) {
                TXSettings  *settings  = [[TXApp instance] getSettings];
                NSString *userUid   = [settings getUserId];
                NSString *userTel = [settings getUserTelno];
                
                propertyMap[@"uid"] = userUid;
                propertyMap[@"utelno"] = userTel;
                propertyMap[@"fname"] = [[[TXApp instance] getSettings] getFDKeychain:@"SIGNUP_NAME"];
            }
            
            ((UILabel *)[self.vbody viewWithTag:11012]).text = [Utils nullToString:propertyMap[@"fname"]];
            ((UILabel *)[self.vbody viewWithTag:11212]).text = [Utils nullToString:propertyMap[@"uid"]];
            ((UILabel *)[self.vbody viewWithTag:11312]).text = [Utils nullToString:propertyMap[@"utelno"]];
            
            ((UILabel *)[self.vbody viewWithTag:11812]).text = [Utils nullToString:propertyMap[@"licenseno"]];
            
            NSString* dateString = @"";
            if (![[Utils nullToString:propertyMap[@"ssn_dob"]] isEqualToString:@""]) {
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
                [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
                [dateFormatter setDateFormat:DATE_FORMAT_API];
                NSDate *date = [dateFormatter dateFromString:propertyMap[@"ssn_dob"]];
                [dateFormatter setDateFormat:DATE_FORMAT_YYYYMMDD];
                dateString = [dateFormatter stringFromDate:date];
            }
            ((UILabel *)[self.vbody viewWithTag:11712]).text = [Utils nullToString:dateString];
        
            if (isNewDriver) {
                if (![profile[@"picture"] intValue]) {
                    [self hideBusyIndicator];
                }
            }
            else {
                if (![profile[@"picture"] intValue]) {
                    [self hideBusyIndicator];
                }
            }
            
            imageLoadingMax = 0;
            imageLoadingCnt = 0;
            if ([profile[@"picture"] intValue]) {
                imageLoadingMax++;
                [self onPP605:profile[@"pseq"] picture:profile[@"picture"]];
            }
            [self checkProcess];
        }
        else {
            [self hideBusyIndicator];
            [self alertError:@"Error" message:descriptor.error];
        }
    }
    else if([event.name isEqualToString:@"PP605"]) {
        imageLoadingCnt++;
        
        NSDictionary *param;
//        if (request.body && ([request.body isKindOfClass:[NSData class]] && ((NSData *)request.body).length > 0)) {
//            param = [Utils getDataOfQueryString:request.body];
//        }
        if (![Utils isDictionary:param]) {
            param = [Utils getDataOfQueryString:request.reqUrl];
        }
        
        if(descriptor.success == true) {
            NSString     *response = [[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding];
            NSDictionary *result   = getJSONObj(response);
            
            NSString *pictureData              = [result valueForKey:@"picture"];
            
            if ([pictureData length]) {
                
//                NSData *data = [[NSData alloc] initWithBase64EncodedString:pictureData options:0];
//                UIImage *image = [[UIImage alloc] initWithData:data];
                UIImage *image = [Utils decodeBase64ToImage:pictureData];
                // Store image
                //[appDelegate.cache storeObject:event forKey:request.reqUrl];
                
                if ([param[@"name"] isEqualToString:@"picture"]) {
                    UIImageView *imageView = (UIImageView *)[[super navigationView] viewWithTag:9998];
                    imageView.image = image;
                    
//                    UIButton *btn = (UIButton *)[self.vbody viewWithTag:11512];
//                    [btn setImage:nil forState:UIControlStateNormal];
//                    UIImageView *imageView = (UIImageView *)[self.vbody viewWithTag:11515];
//                    imageView.image = image;
                    [propertyMap setObject:image forKey:@"picture"];
                    
                    _imgProfile.alpha = 1.0;
                    [self saveProfileImage:image withCode:event.name];
                }
            }
            if (imageLoadingMax <= imageLoadingCnt)
            {
                [self hideBusyIndicator];
            }
            
            [self checkProcess];
        }
        else {
            [self hideBusyIndicator];
            [self alertError:@"Error" message:descriptor.error];
        }
    }
    else if([event.name isEqualToString:@"PP320"] || [event.name isEqualToString:@"PP310"]) {
        [self hideBusyIndicator];
        if(descriptor.success == true) {
            NSString     *response = [[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding];
            NSDictionary *result   = getJSONObj(response);
            
            
            NSDictionary *profile              = [result valueForKey:@"provider_profile"];
            NSString *pseq = profile[@"pseq"];
            
            if ([pseq length]) {
                appDelegate.becomePseq = pseq;
                [self onPP311];
                
                [Utils setBCDSTEP:@"2"];
                TXBecomeVehicleVC *vc = [[TXBecomeVehicleVC alloc] initWithNibName:@"TXBecomeVehicleVC" bundle:[NSBundle mainBundle] isnew:NO];
                [self.navigationController pushViewController:vc animated:YES];
            }
        }
        else {
            [self alertError:@"Error" message:descriptor.error];
        }
    }
    else if([event.name isEqualToString:@"PP628"]) {
        NSDictionary *param;
//        if (request.body && ([request.body isKindOfClass:[NSData class]] && ((NSData *)request.body).length > 0)) {
//            param = [Utils getDataOfQueryString:request.body];
//        }
        if (![Utils isDictionary:param]) {
            param = [Utils getDataOfQueryString:request.reqUrl];
        }
        
        [self hideBusyIndicator];
        if(descriptor.success == true) {
            NSString     *response = [[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding];
            NSDictionary *result   = getJSONObj(response);
            
            
            NSDictionary *profile              = [result valueForKey:@"provider_device"];
            NSString *pseq = profile[@"pseq"];
            
            if ([pseq length]) {
                if ([param[@"name"] isEqualToString:@"picture"]) {
                    isUpdateImage1 = NO;
                }
                
                //[self onPP312];
            }
        }
        else {
            [self alertError:@"Error" message:descriptor.error];
        }
    }
}

-(void)shadowProfileImage:(UIImage*)image {
    _imgProfile.image = image;
    _imgProfile.alpha = 1.0;
}

-(void)saveProfileImage:(UIImage*)image withCode:(NSString*)code{
    
    [self shadowProfileImage:image];
}

-(UIImage *)makeRoundedImage:(UIImage *) image
                      radius: (float) radius;
{
    CALayer *imageLayer = [CALayer layer];
    imageLayer.frame = CGRectMake(0, 0, image.size.width, image.size.height);
    imageLayer.contents = (id) image.CGImage;
    
    imageLayer.masksToBounds = YES;
    imageLayer.cornerRadius = radius;
    
    UIGraphicsBeginImageContext(image.size);
    [imageLayer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *roundedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return roundedImage;
}


#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker  dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Image Picker Delegate -
//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    TOCropViewController *cropController = [[TOCropViewController alloc] initWithCroppingStyle:self.croppingStyle image:image];
    cropController.delegate = self;
    
    //If profile picture, push onto the same navigation stack
    if (self.croppingStyle == TOCropViewCroppingStyleCircular) {
        [picker pushViewController:cropController animated:YES];
    }
    else { //otherwise dismiss, and then present from the main controller
        [picker dismissViewControllerAnimated:YES completion:^{
            [self presentViewController:cropController animated:YES completion:nil];
        }];
    }
}

#pragma mark - Cropper Delegate -
- (void)cropViewController:(TOCropViewController *)cropViewController didCropToImage:(UIImage *)image withRect:(CGRect)cropRect angle:(NSInteger)angle
{
    [self updateImageViewWithImage:image fromCropViewController:cropViewController];
}

- (void)cropViewController:(TOCropViewController *)cropViewController didCropToCircularImage:(UIImage *)image withRect:(CGRect)cropRect angle:(NSInteger)angle
{
    [self updateImageViewWithImage:image fromCropViewController:cropViewController];
}

- (void)updateImageViewWithImage:(UIImage *)croppedImage fromCropViewController:(TOCropViewController *)cropViewController
{
    if (self->_alertIdx == 9999) {
        UIImageView *imageView = (UIImageView *)[[super navigationView] viewWithTag:9998];
        if ([imageView.image isEqual:croppedImage]) {
            [cropViewController.presentingViewController dismissViewControllerAnimated:YES completion:nil];
            return;
        }
    }
    
    //---------------------------------------------------------------------------------------------------------------------------------------------
    //self.imageView.image = croppedImage;
    //[self shadowProfileImage:croppedImage];
    UIImage *image = [Utils resizeImage:croppedImage];
    
    if (self->_alertIdx == 9999) {
        UIImageView *imageView = (UIImageView *)[[super navigationView] viewWithTag:9998];
        imageView.image = image;
        [propertyMap setObject:image forKey:@"picture"];
        isUpdateImage1 = YES;
    }
    
    [self checkProcess];
    
    [cropViewController.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - Private methods

- (void)showCamera
{
    self.croppingStyle = TOCropViewCroppingStyleDefault;
    
    UIImagePickerController *standardPicker = [[UIImagePickerController alloc] init];
    standardPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    standardPicker.allowsEditing = NO;
    standardPicker.delegate = self;
    [self presentViewController:standardPicker animated:YES completion:nil];

}

- (void)openPhotoAlbum
{
    self.croppingStyle = TOCropViewCroppingStyleDefault;
    
    UIImagePickerController *standardPicker = [[UIImagePickerController alloc] init];
    standardPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    standardPicker.allowsEditing = NO;
    standardPicker.delegate = self;
    [self presentViewController:standardPicker animated:YES completion:nil];

}

- (void)editPhoto
{
    UIImageView *imageView;
    if (self->_alertIdx == 9999) {
        imageView = (UIImageView *)[[super navigationView] viewWithTag:9998];
    }
    
    if (imageView.image) {
        TOCropViewController *cropController = [[TOCropViewController alloc] initWithCroppingStyle:self.croppingStyle image:imageView.image];
        cropController.delegate = self;
        
        
        [self presentViewController:cropController animated:YES completion:nil];
    }
    
}

#pragma mark - UITextField Delegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (self->_alertIdx == 3) {
        UITextField *secondTextField = _securityAlertView.textFieldsArray[(textField.tag == 1 ? 2 : 1)];
        
        NSMutableString *currentString = textField.text.mutableCopy;
        
        [currentString replaceCharactersInRange:range withString:string];
        
        [_securityAlertView setButtonAtIndex:0 enabled:(currentString.length > 2 && secondTextField.text.length > 2)];
    }
    else {
        UITextField *secondTextField = _securityAlertView.textFieldsArray[(textField.tag == 0 ? 1 : 0)];
        
        NSMutableString *currentString = textField.text.mutableCopy;
        
        [currentString replaceCharactersInRange:range withString:string];
        
        [_securityAlertView setButtonAtIndex:0 enabled:(currentString.length > 2 && secondTextField.text.length > 2)];
    }
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField.tag < 1)
        [_securityAlertView.textFieldsArray[(textField.tag + 1)] becomeFirstResponder];
    else
    {
        if ([_securityAlertView isButtonEnabledAtIndex:0])
            [_securityAlertView dismissAnimated:YES completionHandler:nil];
        else
            [textField resignFirstResponder];
    }
    
    return YES;
}

#pragma mark - UIDatePicker Delegate

//listen to changes in the date picker and just log them
- (void) datePickerDateChanged:(UIDatePicker *)paramDatePicker{
    NSLog(@"Selected date = %@", paramDatePicker.date);
    NSDateFormatter __autoreleasing *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setTimeZone:[NSTimeZone systemTimeZone]];
    //[dateFormat setDateFormat:DATE_FORMAT_YYYYMMDDHH24MISS];
    //dateFormat.dateStyle = NSDateFormatterMediumStyle;
    //NSString *dateString =  [dateFormat stringFromDate:paramDatePicker.date];
    
    [dateFormat setDateFormat:DATE_FORMAT_YYYYMMDD];
    NSString *dateString = [dateFormat stringFromDate:paramDatePicker.date];
    NSLog(@"dateString:%@",dateString);
    
    UITextField *tf = (UITextField*)[self.view viewWithTag:103];
    tf.text = dateString;
    
}

#pragma mark - UITextView Delegate Methods

//캐릭터가 텍스트 뷰에 표시되기 직전 아래 메소드가 호출된다
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    
    
    //newLineCharacterSet이 있으면 done button이 호출됨. 따라서 키보드가 사라짐.
    NSCharacterSet *doneButtonCharacterSet = [NSCharacterSet newlineCharacterSet];
    NSRange replacementTextRange = [text rangeOfCharacterFromSet:doneButtonCharacterSet];
    NSUInteger location = replacementTextRange.location;
    
    //텍스트가 190자가 넘지 않도록 제한
    if (textView.text.length + text.length > 190){
        if (location != NSNotFound){
            [textView resignFirstResponder];
        }
        return NO;
    }
    
    else if (location != NSNotFound){
        if([text isEqualToString:@"\n"]) {
            return YES;
        }
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}

#pragma mark - UITableView DataSource Methods
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [tableView setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [tableView setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
    
    UIView *seperatorView = [[UIView alloc] initWithFrame:CGRectMake(0, cell.frame.size.height - 1, cell.frame.size.width, 1)];
    seperatorView.backgroundColor = UIColorTableSeperator;
    [cell addSubview:seperatorView];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    // This will create a "invisible" footer
    return 0.01f;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [UIView new];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [dataArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    cell.tag = indexPath.row;
    int tag = (int)indexPath.row+100;
    //[self changeCell:tableView withIndexPath:indexPath andTag:tag];
    [self addCell:tableView withCell:cell withIndexPath:indexPath andTag:tag];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    cell.textLabel.text = [NSString stringWithFormat:@"      %@",[dataArray objectAtIndex:indexPath.row]];
    
    
    //    if([[tableView indexPathsForSelectedRows] containsObject:indexPath]) {
    //        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    //    } else {
    //        cell.accessoryType = UITableViewCellAccessoryNone;
    //    }
    
    return cell;
}

#pragma mark - UITableView Delegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self changeCell:tableView withIndexPath:indexPath andTag:((int)indexPath.row+100)];
    
    //cell.accessoryView.hidden = NO;
}
- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self changeCell:tableView withIndexPath:indexPath andTag:((int)indexPath.row+100)];
    
    //cell.accessoryView.hidden = YES;
}

-(void)addCell:(UITableView*)tableView withCell:(UITableViewCell*)cell withIndexPath:(NSIndexPath*)indexPath andTag:(int)tag{
    
    UIImageView *preView = [cell.contentView viewWithTag:tag];
    [preView removeFromSuperview];
    
    UILabel *preLabel = [cell.contentView viewWithTag:(tag+100)];
    [preLabel removeFromSuperview];
    
    NSInteger _h = cell.frame.size.height;
    NSInteger _x = 16;
    UIImageView* imv = [[UIImageView alloc]initWithFrame:CGRectMake(cell.frame.size.width - _x*4 ,_h/2-_x/2, _x, _x)];
    imv.tag = tag;
    UIImage *cellImage;
    
    if([[tableView indexPathsForSelectedRows] containsObject:indexPath]) {
        cellImage = [UIImage imageNamed:@"form_radio_chk"];
    }
    else {
        cellImage = [UIImage imageNamed:@"form_radio_normal"];
    }
    imv.image=cellImage;
    [cell.contentView addSubview:imv];
    /*
     UILabel *mylabel = [[UILabel alloc] initWithFrame:CGRectMake(20,5, 150, 40)];
     mylabel.tag = (tag+100);
     mylabel.text = [dataArray objectAtIndex:(tag-100)];
     [cell.contentView addSubview:mylabel];
     */
    //    UILabel *cellLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, _h/2-_x/2, 120, 35)];
    //    cellLabel.tag = (int)tag+100;
    //    [cellLabel setText:[dataArray objectAtIndex:(tag-100)]];
    //    //[cellLabel setFont:[UIFont boldSystemFontOfSize:12]];
    //    [cellLabel setBackgroundColor:[UIColor clearColor]];
    //
    //    [cell addSubview:cellLabel];
}

-(void)changeCell:(UITableView*)tableView withIndexPath:(NSIndexPath*)indexPath andTag:(int)tag{
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    UIImageView *preView = [cell.contentView viewWithTag:tag];
    [preView removeFromSuperview];
    
    UILabel *preLabel = [cell.contentView viewWithTag:(tag+100)];
    [preLabel removeFromSuperview];
    
    NSInteger _h = cell.frame.size.height;
    NSInteger _x = 16;
    UIImageView* imv = [[UIImageView alloc]initWithFrame:CGRectMake(cell.frame.size.width - _x*4 ,_h/2-_x/2, _x, _x)];
//    UIImageView* imv = [[UIImageView alloc]initWithFrame:CGRectMake(0,_h/2-_x/2, _x, _x)];
    imv.tag = tag;
    UIImage *cellImage;
    
    if([[tableView indexPathsForSelectedRows] containsObject:indexPath]) {
        cellImage = [UIImage imageNamed:@"form_radio_chk"];
    }
    else {
        cellImage = [UIImage imageNamed:@"form_radio_normal"];
    }
    imv.image=cellImage;
    [cell.contentView addSubview:imv];
    /*
     UILabel *mylabel = [[UILabel alloc] initWithFrame:CGRectMake(20,5, 150, 40)];
     mylabel.tag = (tag+100);
     mylabel.text = [dataArray objectAtIndex:(tag-100)];
     [cell.contentView addSubview:mylabel];
     */
    //    UILabel *cellLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, _h/2-_x/2, 120, 35)];
    //    cellLabel.tag = (int)tag+100;
    //    [cellLabel setText:[dataArray objectAtIndex:(tag-100)]];
    //    //[cellLabel setFont:[UIFont boldSystemFontOfSize:12]];
    //    [cellLabel setBackgroundColor:[UIColor clearColor]];
    //
    //    [cell addSubview:cellLabel];
}

#pragma mark -
#pragma mark Validator
#pragma mark -
-(void) resignKeyboard
{
    [self.view endEditing:YES];
}

- (IBAction)validateAction:(id)sender
{
}

#pragma ValidatorDelegate - Delegate methods

- (void) preValidation
{
}

- (void)onSuccess
{
}

- (void)onFailure:(Rule *)failedRule
{
    
//    NSMutableDictionary *options = [@{kCRToastNotificationTypeKey               : @(CRToastTypeNavigationBar),
//                                      kCRToastNotificationPresentationTypeKey   : @(CRToastPresentationTypeCover),
//                                      kCRToastUnderStatusBarKey                 : @(YES),
//                                      kCRToastTextKey                           : failedRule.failureMessage,
//                                      kCRToastFontKey                           : [UIFont systemFontOfSize:14],
//                                      kCRToastTextAlignmentKey                  : @(NSTextAlignmentCenter),
//                                      kCRToastTimeIntervalKey                   : @(2),
//                                      kCRToastAnimationInTypeKey                : @(CRToastAnimationTypeGravity),
//                                      kCRToastAnimationOutTypeKey               : @(CRToastAnimationTypeGravity),
//                                      kCRToastAnimationInDirectionKey           : @(CRToastAnimationDirectionTop),
//                                      kCRToastAnimationOutDirectionKey          : @(CRToastAnimationDirectionTop),
//                                      kCRToastNotificationPreferredPaddingKey   : @(15)} mutableCopy];
//    [CRToastManager showNotificationWithOptions:options
//                                completionBlock:^{
//                                    NSLog(@"Completed");
//                                }];
    
    [WToast showWithText:failedRule.failureMessage duration:kWTShort roundedCorners:NO gravity:kWTGravityTop];
}

@end
