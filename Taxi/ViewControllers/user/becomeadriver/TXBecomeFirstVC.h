//
//  TXNewsDetailVC.h
//  TXNewsDetailVC
//

//

#import <UIKit/UIKit.h>
#import "TXButton.h"


@interface TXBecomeFirstVC : UIViewController {
    
    IBOutlet UIView          *_introScrollView;
    
    IBOutlet TXButton           *btnstart;
    IBOutlet UIView             *btnView;
    
    IBOutlet UIButton           *btnTop;
    
    NSArray *myCovers;
    
    IBOutlet UILabel *text1;
    IBOutlet UILabel *text2;
    IBOutlet UILabel *text3;
}

@property (nonatomic, assign) BOOL isPreOrder;
@property (nonatomic, strong) NSDictionary *detailInfo;

@end

