//
//  TXNewsDetailVC.m
//  TXNewsDetailVC
//

//

#import "TXBecomeVC.h"
#import "TXBecomeFirstVC.h"
#import "TXAppDelegate.h"
#import "TXApp.h"
#import "utils.h"

@import IQKeyboardManager;

@implementation TXBecomeFirstVC

#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];

    [[IQKeyboardManager sharedManager] setEnable:NO];
    
    [self setNeedsStatusBarAppearanceUpdate];
    
//    [[UIApplication sharedApplication] setStatusBarHidden:YES
//                                            withAnimation:UIStatusBarAnimationFade];
    
    //[self.view setBackgroundColor:UIColorDefault];
    
    [self deregisterForNotifications];
    
//    [self onNextSkip:YES];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self deregisterForNotifications];
    
    // 사이드에서 보이는건지
    btnTop.hidden = YES;
//    [btnTop setImage:[UIImage imageNamed:@"btn_back"] forState:UIControlStateNormal];
//    [btnTop setImageEdgeInsets:UIEdgeInsetsMake(0,-6,0,0)];
    
    text1.textColor=[UIColor blackColor];
    text2.textColor=HEXCOLOR(0x999999FF);
    text3.textColor=HEXCOLOR(0x666666FF);

    text1.text=LocalizedStr(@"Menu.BecomeADriver.First.title");
    text1.text=@"";
    text2.text=@"";
    text3.text=LocalizedStr(@"Menu.BecomeADriver.First.text");
    
    [self resetLabel];
    
    [self.navigationController setToolbarHidden:YES animated:YES];
    
    // button
    [[btnstart titleLabel] setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:18]];
    
    NSString *str= LocalizedStr(@"Menu.BecomeADriver.First.BTN.text");
    [btnstart setTitle:str forState:UIControlStateNormal];
    [btnstart setTitleColor:UIColorButtonText forState:UIControlStateNormal];
    [btnstart setBackgroundColor:UIColorDefault];
    
}

//- (BOOL)prefersStatusBarHidden {
//    return YES;
//}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}



- (void)deregisterForNotifications {
}


- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
    
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)resetLabel {
    [text1 sizeToFit];
    //[text3 sizeToFit];
    
//    CGRect frame = text1.frame;
//    frame.size.width = self.view.frame.size.width - 20 * 2;
//    text1.frame = frame;
}

#pragma mark - Selectors
- (IBAction) onClose: (id) sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction) onNext: (id) sender {
    
    [self onNextSkip:NO];
}

- (void) onNextSkip:(BOOL)flag {
    
    NSString *step = [Utils getBCDSTEP];
    
    if (step != nil && [step intValue] == 1) {
        [self onClose:nil];
        return;
    }
    
    TXBecomeVC *mvc = [[TXBecomeVC alloc] initWithNibName:@"TXBecomeVC" bundle:nil];
    [self.navigationController pushViewController:mvc animated:YES];
}

@end
