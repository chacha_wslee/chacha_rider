//
//  TXAskCardNumberVC.m
//  Taxi

//

#import "TXSignatureVC.h"

#import "Validator.h"
#import "WToast.h"
//#import "CRToast.h"


@interface TXSignatureVC () <ValidatorDelegate> {
    NSArray *dataArray;
    NSArray *_msgList;
}

@end

@implementation TXSignatureVC {
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [super configureBottomLine];
    UIView *line = (UIView*)[[super navigationView] viewWithTag:1500];
    CGRect rect = line.frame;
    rect.size.height = 3;
    line.frame = rect;
//    UILabel *label = (UILabel*)[[super navigationView] viewWithTag:1111];
//    label.tag = 2001;
//    label.text = LocalizedStr(@"Menu.Qna.title");
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    UIButton *btn = [Utils addBottomButton:LocalizedStr(@"Button.OK") target:self selector:@selector(onNaviButtonClick:) color:YES];
    btn.tag = 2300;
    [self.view addSubview:btn];
    
    [_tfText becomeFirstResponder];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

-(void)dealloc {
    [self removeEventListeners];
}

-(void)configureStyles {
    [super configureStyles];
    
    [super navigationType01X:[UIImage imageNamed:@"btn_back_02"] centerText:LocalizedStr(@"Menu.BecomeADriver.List.text20")];
}

- (void)nextButtonOn:(BOOL)on
{
    UIButton *btn = [super.view viewWithTag:2300];
    btn.enabled = on;
    if (on)
        [btn setTitleColor:UIColorBasicText forState:UIControlStateNormal];
    else
        [btn setTitleColor:UIColorBasicTextOff forState:UIControlStateNormal];
}

-(void) configure {
    [super configure];
    
    UIView *n = [super navigationView];
    //NSInteger _y = n.frame.origin.y + n.frame.size.height;
    [n setBackgroundColor:HEXCOLOR(0x333333ff)];
    UILabel *lb = [n viewWithTag:1210];
    
    lb.textColor = [UIColor whiteColor];
    
    // 2. init event
    _msgList = @[
                 ];
    
//    [self registerEventListeners];
    
    dataArray = @[];
    
//    NSString *str = LocalizedStr(@"Map.Vechile.Request.CouponCode.text");
//    NSAttributedString *attr = [[NSAttributedString alloc] initWithString:str attributes:@{ NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue-Italic" size:36.0f], NSForegroundColorAttributeName : UIColorDefault }];
//    _tfText.attributedPlaceholder = attr;
    _tfText.textColor = UIColorDefault;
    
//    _lbCategory.text = LocalizedStr(@"Map.Vechile.Request.CouponCode.alert.title");
    
    //------------------------------------------------------------------------------------------------------------------------
    
    self.view.backgroundColor = HEXCOLOR(0x333333ff);
}

-(void) registerEventListeners {
    for (NSArray *msg in _msgList) {
        [self->model addEventListener:self forEvent:[msg objectAtIndex:0] eventParams:nil];
    }
}

-(void) removeEventListeners {
    for (NSArray *msg in _msgList) {
        [self->model removeEventListener:self forEvent:[msg objectAtIndex:0]];
    }
}

#pragma mark - Logic

#pragma mark - Action
-(IBAction)onNaviButtonClick:(id)sender {
    UIButton *btn = (UIButton*)sender;
    if (btn.tag == 1100 || btn.tag == 1101) {
        [self removeEventListeners];
        [self.navigationController popViewControllerAnimated:YES];
    }
    else if(btn.tag == 2200) {
        // 

    }
    else if(btn.tag == 2300) {
        [self next:nil];
    }
    
    
}

-(void)next:(id)sender {
    
    [self validateAction:sender];
    return;
}



#pragma mark - Event
-(void)onFail:(id)object error:(TXError *)error {
    
}

-(void)onEvent:(TXEvent *)event eventParams:(id)subscriptionParams {
    
}

#pragma mark - Keyboard Notification Method
- (void)keyboardWillShow:(NSNotification*)note{
    NSDictionary *userInfo = note.userInfo;
    CGRect finalKeyboardFrame = [userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    NSTimeInterval animationDuration = [userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    UIButton *btn = [self.view viewWithTag:2300];
    float inputViewFinalYPosition = self.view.bounds.size.height - finalKeyboardFrame.size.height - btn.frame.size.height;
    CGRect inputViewFrame = btn.bounds;
    inputViewFrame.origin.y = inputViewFinalYPosition;
    
    [UIView animateWithDuration:animationDuration animations:^{
        btn.frame = inputViewFrame;
    }];
    
}

- (void)keyboardWillHide:(NSNotification*)note{
    
    NSDictionary *userInfo = note.userInfo;
    NSTimeInterval animationDuration = [userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    UIButton *btn = [self.view viewWithTag:2300];
    CGRect inputViewFrame = btn.bounds;
    inputViewFrame.origin.y = self.view.bounds.size.height - btn.frame.size.height;
    
    [UIView animateWithDuration:animationDuration animations:^{
        btn.frame = inputViewFrame;
    }];
    
}

#pragma mark -
#pragma mark Validator
#pragma mark -
-(void) resignKeyboard
{
    [self.view endEditing:YES];
}

- (IBAction)validateAction:(id)sender
{
    [self resignKeyboard];
    
    Validator *validator = [[Validator alloc] init];
    validator.delegate   = self;
    //
    [validator putRule:[Rules minLength:2 withFailureString:LocalizedStr(@"Validator.Name.text") forTextField:_tfText]];
    
    NSString *trimmedString = [Utils trimStringOnly:_tfText.text];
    
    if (![validator validateForResult]) {
        return;
    }

    // signature
    NSDictionary *dic = @{@"signature":trimmedString};
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_DISSMISS_SIGN object:nil userInfo:dic];
    [self removeEventListeners];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma ValidatorDelegate - Delegate methods

- (void) preValidation
{
}

- (void)onSuccess
{
}

- (void)onFailure:(Rule *)failedRule
{
    [WToast showWithText:failedRule.failureMessage duration:kWTShort roundedCorners:NO gravity:kWTGravityTop];
}

@end
