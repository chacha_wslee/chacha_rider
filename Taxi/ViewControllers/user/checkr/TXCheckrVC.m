//
//  MenuViewController.m
//  SlideMenu
//

//

#import "TXSignatureVC.h"
#import "TXCheckrVC.h"
#import "utilities.h"
#import "RTLabel.h"
#import "TXBecomeVC.h"

#import "Validator.h"
#import "WToast.h"
//#import "CRToast.h"

//#define MaxProcessCnt 10
//#define NSProcessPct [NSString stringWithFormat:@"%d%@",(int)(processPct*100/MaxProcessCnt),LocalizedStr(@"Menu.BecomeADriver.Header.text2")]


@interface TXCheckrVC() <UIWebViewDelegate, RTLabelDelegate, ValidatorDelegate, UITableViewDelegate,UITableViewDataSource>{
    NSArray *items;
    NSArray *_msgList;
    UILabel *lbProcessInfo;
    int processPct;
    
    NSMutableArray *contentHeights;
    
    NSArray *dataArray;
    NSDictionary *result; // api result
}

@end

@implementation TXCheckrVC {
    NSMutableDictionary *propertyMap;
}


-(void)viewDidLoad {
    
    [super viewDidLoad];
    
    contentHeights = [[NSMutableArray alloc] init];
    
//    UIView *n = [super navigationView];
//    NSInteger _y = n.frame.origin.y + n.frame.size.height;
    
//    if (self.setupMode != 3) {
//        UIView *view = [Utils addRTLabelView:self.view underView:nil dim:kBasicMargin];
//        view.backgroundColor = UIColorLabelBG;
//        
//        RTLabel *label = [view viewWithTag:11];
//        label.textColor = UIColorLabelText;
//        if (self.setupMode == 1) {
//            label.text = LocalizedStr(@"Menu.BecomeADriver.Checkr1.tail.text");
//        }
//        else if (self.setupMode == 2) {
//            label.text = LocalizedStr(@"Menu.BecomeADriver.Checkr1.tail.text");
//        }
//        [label setFont:[UIFont systemFontOfSize:14.0f]];
//        label.tag = 3001;
//        [label sizeToFit];
//        CGSize optimumSize = [label optimumSize];
//        CGRect frame = view.frame;
//        frame.size.height = optimumSize.height + kBasicMargin*2;
//        view.frame = frame;
//        
//        [label setDelegate:self];
//    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dismissVcCancel:) name:NOTIFICATION_DISSMISS_SIGN object:nil];
    
    UIButton *btn = [Utils addBottomButton:LocalizedStr(@"Intro.Next.title") target:self selector:@selector(onNaviButtonClick:) color:YES];
    [self.view addSubview:btn];
    
//    if (self.setupMode != 1) {
//        [self onPP203];
//    }
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self checkProcess];

}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    //[self removeEventListeners];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
}

-(void)dealloc {
    [self removeEventListeners];
}

#pragma mark - Configure

-(void)configureStyles {
    [super configureStyles];
    
    NSString *title = nil;
    if (self.setupMode == 1) {
        title = LocalizedStr(@"Menu.BecomeADriver.Checkr1.title");
    }
    else if (self.setupMode == 2) {
        title = LocalizedStr(@"Menu.BecomeADriver.Checkr2.title");
    }
    else if (self.setupMode == 3) {
        title = LocalizedStr(@"Menu.BecomeADriver.Checkr3.title");
    }
    
    [super navigationType01X:[UIImage imageNamed:@"btn_back"] centerText:title];
    //[super navigationType11X:[UIImage imageNamed:@"btn_back"] centerText:title rightText:LocalizedStr(@"Intro.Next.title")];
    [self nextButtonOn:NO];
}

-(void)configure {
    [super configure];
    [super configureBottomLine];
    UIView *line = (UIView*)[[super navigationView] viewWithTag:1500];
    CGRect rect = line.frame;
    rect.size.height = 3;
    line.frame = rect;
    line.backgroundColor = HEXCOLOR(0x111111FF);
    
    UIView *n = [super navigationView];
    NSInteger _y = n.frame.origin.y + n.frame.size.height;
    
    // 2. init event
    if (self.setupMode == 1) {
        _msgList = @[
                     @[@"PP203",@""], // get
                     @[@"PP213",@""], // set
                     ];
    }
    else if (self.setupMode == 2) {
        _msgList = @[
                     @[@"PP204",@""], // get
                     @[@"PP214",@""], // set
                     ];
    }
    else if (self.setupMode == 3) {
        _msgList = @[
                     @[@"PP205",@""], // get
                     @[@"PP215",@""], // set
                     ];
    }
    
    [self registerEventListeners];
    
    //------------------------------------------------------------------------------------------------------------------------
    propertyMap = [[NSMutableDictionary alloc] init];
    //------------------------------------------------------------------------------------------------------------------------
    /*
    UIView *labelView = [Utils addDoubleLabelView:self.view underView:nil dim:0];
    CGRect rect = labelView.frame;
    rect.origin.y = kTopHeight + kNaviHeight;
    labelView.frame = rect;
    UILabel *label = [labelView viewWithTag:11];
    label.text = LocalizedStr(@"Menu.BecomeADriver.Header.text1");
    lbProcessInfo = [labelView viewWithTag:12];
    processPct = 0;
    lbProcessInfo.text = NSProcessPct;
    [self.view addSubview:labelView];
     */
    //------------------------------------------------------------------------------------------------------------------------
    //self.myScrollView.backgroundColor = UIColorLabelBG;
    
    //UIWebView *webViewHTML = [[UIWebView alloc]init];
    self.vbody.backgroundColor = [UIColor whiteColor];
    self.vbody.delegate = self;
    self.vbody.tag = 200;
    //self.vbody.backgroundColor = [UIColor clearColor];
    //self.vbody.opaque = NO;
    //        webViewHTML.userInteractionEnabled = NO;
    //self.vbody.scrollView.bounces = NO;
    //self.vbody.scrollView.showsHorizontalScrollIndicator = YES;
    
    self.vbody.frame = CGRectMake(0,
                                  _y + 10, //kNaviTopHeight + kBasicHeight,
                                  self.view.frame.size.width,
                                  self.view.frame.size.height - (_y + 10) - kBottomButtonHeight
                                  //self.view.frame.size.height - kNaviTopHeight - kBasicHeight
                                  );

    if (self.setupMode == 1) {
        [self onPP203];
    }
    else if (self.setupMode == 2) {
        [self onPP204];
    }
    else if (self.setupMode == 3) {
        [self onPP205];
    }
}

-(void) registerEventListeners {
    for (NSArray *msg in _msgList) {
        [self->model addEventListener:self forEvent:[msg objectAtIndex:0] eventParams:nil];
    }
}

-(void) removeEventListeners {
    for (NSArray *msg in _msgList) {
        [self->model removeEventListener:self forEvent:[msg objectAtIndex:0]];
    }
}

#pragma mark - Action
-(IBAction)onNaviButtonClick:(id)sender {
    UIButton *btn = (UIButton*)sender;
    NSLog(@"onNaviButtonClick");
    //[super onNaviButtonClick:sender];
    
    if (btn.tag == 1100 || btn.tag == 1101) {
        [self removeEventListeners];
        [self.navigationController popViewControllerAnimated:YES];
    }
    else if(btn.tag == 999) {
        // next
        if (self.setupMode == 1) {
            [self onPP213];
        }
        else if (self.setupMode == 2) {
            [self onPP214];
        }
        else if (self.setupMode == 3) {
            [self onPP215];
        }
    }
}

-(IBAction)addButtonPressed:(id)sender {
    UIButton *btn = (UIButton*)sender;
    
    if (btn.tag == 999) {
        [self removeEventListeners];
    }
}

- (void)dismissVcCancel:(NSNotification *)notification {
    
    NSDictionary* dict = notification.userInfo;
    
    NSString* signature = [dict valueForKey:@"signature"];
    
    // webview set
    [self.vbody stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"set_%@('%@');",@"signature",signature]];
    
    [self updateTrimString:signature key:@"signature"];
    
    [self checkProcess];
}

/*
- (CGFloat)height
{
    CGFloat webViewHeight = 0;
    // use Javascript to determine current document height
    if(!self.webView.loading && self.webView.frame.size.width)
        webViewHeight = [[self.webView stringByEvaluatingJavascriptFromString: @"document.documentElement.clientHeight"] integerValue];
    if(!webViewHeight)
        webViewHeight = [super height];
    if(webViewHeight > self.maximumHeight)
        webViewHeight = self.maximumHeight;
    
    return webViewHeight + 2*self.controlMargin;
}
*/

#pragma mark - Send Event
-(void)onPP203 {
    
    NSString* pseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.PSEQ];
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:pseq, @"pseq", nil];
    
    [self showBusyIndicator:@"Request info ... "];
    [self->model requsetAPI:API_FUNC(NSStringFromSelector(_cmd)) property:dic];
}

-(void)onPP204 {
    
    NSString* pseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.PSEQ];
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:pseq, @"pseq", nil];
    
    [self showBusyIndicator:@"Request info ... "];
    [self->model requsetAPI:API_FUNC(NSStringFromSelector(_cmd)) property:dic];
}

-(void)onPP205 {
    
    NSString* pseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.PSEQ];
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:pseq, @"pseq", nil];
    
    [self showBusyIndicator:@"Request info ... "];
    [self->model requsetAPI:API_FUNC(NSStringFromSelector(_cmd)) property:dic];
}

-(NSDictionary*)updateCheckrData {
    
    NSString* pseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.PSEQ];
    NSString *key = [NSString stringWithFormat:@"%@_%@",@"provider_check",pseq];
    
    TXSettings  *settings  = [[TXApp instance] getSettings];
    NSString *userUid   = [settings getUserId];
    NSString *userTel = [settings getUserTelno];
    
    NSDateFormatter __autoreleasing *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setTimeZone:[NSTimeZone systemTimeZone]];
    [dateFormat setDateFormat:@"MM'/'dd'/'yyyy"];
    NSDate *date = [dateFormat dateFromString:propertyMap[@"dob"]];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    NSString* dob = [dateFormat stringFromDate:date];
    
    NSString *middleName = propertyMap[@"middleName"];
    if([propertyMap[@"middleCheck"] isEqualToString:@"1"]) {
        middleName = @"";
    }
    if (middleName == nil) {
        middleName = @"";
    }
    NSDictionary *dic = @{
                          @"pseq" : pseq,
                          @"provider_check":@{
                                  @"type" : @"welcome",
                                  @"first_name" : propertyMap[@"firstName"],
                                  @"middle_name" : middleName,
                                  @"no_middle_name" : (([propertyMap[@"middleCheck"] isEqualToString:@"1"])?@YES:@NO),
                                  @"last_name" : propertyMap[@"lastName"],
                                  @"dob" : dob,
                                  @"uid" : userUid,
                                  @"utelno" : userTel,
                                  @"ssn" : propertyMap[@"ssn"],
                                  @"zipcode" : propertyMap[@"zipcode"],
                                  @"driver_license_number" : propertyMap[@"license"],
                                  @"driver_license_state" : propertyMap[@"licenseState"],
                                  @"check_welcome" : propertyMap[@"bottomCheck"]
                                  }
                          };
    [[[TXApp instance] getSettings] setFDKeychain:key value:[Utils dicToString:dic[@"provider_check"]]];

    return dic;
}

-(void)onPP213 {
    
    NSDictionary *dic = [self updateCheckrData];
    //NSLog(@"appDelegate.dicDriver:%@",appDelegate.dicDriver);
    [self showBusyIndicator:@"Update info ... "];
    [self->model requsetAPI:API_FUNC(NSStringFromSelector(_cmd)) property:dic];
}

-(void)onPP214 {
    
    NSString* pseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.PSEQ];
    
    NSDictionary *dic = @{
                          @"pseq" : pseq,
                          @"provider_check":@{
                                  @"type" : @"disclosure",
                                  @"check_disclosure" : propertyMap[@"bottomCheck"]
                                  }
                          };
    
    [self showBusyIndicator:@"Update info ... "];
    [self->model requsetAPI:API_FUNC(NSStringFromSelector(_cmd)) property:dic];
}

-(void)onPP215 {
    
    NSString* pseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.PSEQ];
    
    NSDictionary *dic = @{
                          @"pseq" : pseq,
                          @"provider_check":@{
                                  @"type" : @"authorization",
                                  
                                  @"check_ny" : propertyMap[@"newYork"],
                                  @"check_wa" : propertyMap[@"Washington"],
                                  @"check_mn" : propertyMap[@"Minnesota"],
                                  @"check_ca" : propertyMap[@"California"],
                                  @"esign" : propertyMap[@"signature"]
                                  }
                          };
    
    [self showBusyIndicator:@"Update info ... "];
    [self->model requsetAPI:API_FUNC(NSStringFromSelector(_cmd)) property:dic];
}

#pragma mark - onEvent
-(void)onEvent:(TXEvent *)event eventParams:(id)subscriptionParams {
    
    TXResponseDescriptor * descriptor = [event getEventProperty:TXEvents.Params.DESCRIPTOR];
    TXRequestObj *request = [event getEventProperty:TXEvents.Params.REQUEST];
    
    
    if([event.name isEqualToString:@"PP203"] || [event.name isEqualToString:@"PP204"] || [event.name isEqualToString:@"PP205"]) {
        
        if(descriptor.success == true) {
            NSString     *response = [[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding];
            result   = getJSONObj(response)[@"provider_check"];
            
            if (![Utils isDictionary:result]) {
                result = @{@"middleName":@"",@"middleCheck":@"1"};
            }
            NSString *fullURL;
            if (self.setupMode == 1) {
                fullURL=CHECKR_WELCOME_URL;
            }
            else if (self.setupMode == 2) {
                fullURL=CHECKR_DISCLOSURE_URL;
            }
            else if (self.setupMode == 3) {
                fullURL=CHECKR_AUTHORIZATION_URL;
            }
            
            if ([event.name isEqualToString:@"PP203"] && self.setupMode != 1) {
                [self updateCheckrData];
            }
            NSURL *url=[NSURL URLWithString:fullURL];
            NSURLRequest *requestObj=[NSURLRequest requestWithURL:url];
            [self.vbody loadRequest:requestObj];
            
        } else {
            //에러주석[self alertError:@"Error" message:descriptor.error];
        }
        [self hideBusyIndicator];
    }
    else if([event.name isEqualToString:@"PP213"] || [event.name isEqualToString:@"PP214"]) {
        [self hideBusyIndicator];
        if(descriptor.success == true) {
            TXCheckrVC *mvc = [[TXCheckrVC alloc] initWithNibName:@"TXCheckrVC" bundle:nil];
            mvc.setupMode = self.setupMode + 1;
            BOOL isAnimation = NO;
            NSString *step = [[[TXApp instance] getSettings] getFDKeychain:@"BCD_STEP"];
            if (mvc.setupMode != [step intValue]) {
                isAnimation = NO;
            }
            else {
                isAnimation = YES;
            }
            
            if([event.name isEqualToString:@"PP213"]) {
                [[[TXApp instance] getSettings] setFDKeychain:@"BCD_STEP" value:@"2"];
            }
            else if([event.name isEqualToString:@"PP214"]) {
                [[[TXApp instance] getSettings] setFDKeychain:@"BCD_STEP" value:@"3"];
            }
            
            [self.navigationController pushViewController:mvc animated:isAnimation];
//            [self pushViewController:mvc];
        } else {
            //에러주석[self alertError:@"Error" message:descriptor.error];
        }
    }
    else if([event.name isEqualToString:@"PP215"]) {
        [self hideBusyIndicator];
        if(descriptor.success == true) {
            
            if([event.name isEqualToString:@"PP215"]) {
                [[[TXApp instance] getSettings] setFDKeychain:@"BCD_STEP" value:@"4"];
            }
            
            TXBecomeVC *mvc = [[TXBecomeVC alloc] initWithNibName:@"TXBecomeVC" bundle:nil];
            [self pushViewController:mvc];
        } else {
            //에러주석[self alertError:@"Error" message:descriptor.error];
        }
    }
}

-(void)popupInput:(NSInteger)tag fname:(NSString*)fname text:(NSString*)text webView:(UIWebView *)webView
{
    NSString *__title = nil;
    NSString *__subtitle = nil;
    
    if (tag == 35) {
        TXSignatureVC *mvc = [[TXSignatureVC alloc] initWithNibName:@"TXSignatureVC" bundle:nil];
        [self pushViewController:mvc];
        return;
        
    }
    if (tag == 0) {
        //
        __title = LocalizedStr(@"Menu.BecomeADriver.List.text1");
        __subtitle = LocalizedStr(@"Menu.BecomeADriver.List.text1.input");
    }
    else if (tag == 1) {
        __title = LocalizedStr(@"Menu.BecomeADriver.List.text19");
        __subtitle = LocalizedStr(@"Menu.BecomeADriver.List.text19.input");
    }
    else if (tag == 2) {
        [self updateTrimString:text key:@"middleCheck"];
        if ([propertyMap[@"middleCheck"] isEqualToString:@"0"]) {
            NSString *middleName = [webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"get_middleName();"]];
            [self updateTrimString:middleName key:@"middleName"];
        }
        else {
            [propertyMap setValue:@"" forKey:@"middleName"];
        }
        
        [self checkProcess];
        return;
    }
    else if (tag == 3) {
        __title = LocalizedStr(@"Menu.BecomeADriver.List.text2");
        __subtitle = LocalizedStr(@"Menu.BecomeADriver.List.text2.input");
    }
    else if (tag == 4) {
        __title = LocalizedStr(@"Menu.BecomeADriver.List.text17");
    }
    else if (tag == 5) {
        __title = LocalizedStr(@"Menu.BecomeADriver.List.text5");
        __subtitle = LocalizedStr(@"Menu.BecomeADriver.List.text5.input");
    }
    else if (tag == 6) {
        __title = LocalizedStr(@"Menu.BecomeADriver.List.text13");
    }
    else if (tag == 7) {
        __title = LocalizedStr(@"Menu.BecomeADriver.List.text8");
    }
    else if (tag == 8) {
        __title = LocalizedStr(@"Menu.BecomeADriver.List.text12");
    }
    else if (tag == 9) {
        [self updateTrimString:text key:@"bottomCheck"];
        [self checkProcess];
        return;
    }
    else if (tag == 31) {
        [self updateTrimString:text key:@"newYork"];
        [self checkProcess];
        return;
    }
    else if (tag == 32) {
        [self updateTrimString:text key:@"Washington"];
        [self checkProcess];
        return;
    }
    else if (tag == 33) {
        [self updateTrimString:text key:@"Minnesota"];
        [self checkProcess];
        return;
    }
    else if (tag == 34) {
        [self updateTrimString:text key:@"California"];
        [self checkProcess];
        return;
    }
    else if (tag == 35) {
        __title = LocalizedStr(@"Menu.BecomeADriver.List.text20");
    }
    
    if (tag == 5) {
        
        REFormattedNumberField *txtNum = [[REFormattedNumberField alloc] initWithFrame:CGRectZero];
        txtNum.frame = CGRectMake(0.0f, 0.0f, 280.0f, 38.0f);
        txtNum.format = @"XXX-XX-XXXX";
        txtNum.placeholder = @"";
        txtNum.autocapitalizationType = UITextAutocapitalizationTypeNone;
        //txtPhoneNum.textColor = textField.textColor;
        txtNum.backgroundColor = [UIColor colorWithWhite:0.97 alpha:1.f];
        txtNum.textColor = [UIColor blackColor];
        txtNum.font = [UIFont systemFontOfSize:16.f];
        [txtNum setClearButtonMode:UITextFieldViewModeNever];
        txtNum.textAlignment = NSTextAlignmentCenter;
        
        LGAlertView *alertView = [[LGAlertView alloc] initWithViewAndTitle:__title
                                                                   message:nil
                                                                     style:LGAlertViewStyleAlert
                                                                      view:txtNum
                                                              buttonTitles:@[LocalizedStr(@"Button.OK")]
                                                         cancelButtonTitle:LocalizedStr(@"Button.CANCEL")
                                                    destructiveButtonTitle:nil
                                                             actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                                                                 NSLog(@"actionHandler, %@, %lu", title, (long unsigned)index);
                                                                 NSLog(@"text:%@",txtNum.text);
                                                                 
                                                                 [self resignKeyboard];
                                                                 Validator *validator = [[Validator alloc] init];
                                                                 validator.delegate   = self;
                                                                 //
                                                                 [validator putRule:[Rules checkRange:NSMakeRange(11, 11) withFailureString:LocalizedStr(@"Validator.SSN.text") forTextField:txtNum]];
                                                                 //[validator putRule:[Rules checkIfAlphaNumericWithFailureString:LocalizedStr(@"Validator.SSN.text") forTextField:txtNum]];
                                                                 
                                                                 if (![validator validateForResult]) {
                                                                     [alertView.firstButton setSelected:NO];
                                                                     return;
                                                                 }
                                                                 
                                                                 // webview set
                                                                 [webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"set_%@('%@');",fname,txtNum.text]];
                                                                 
                                                                 [self updateTrimString:txtNum.text key:@"ssn"];
                                                                 
                                                                 [self checkProcess];
                                                                 
                                                                 [alertView dismissAnimated:YES completionHandler:nil];
                                                             }
                                                             cancelHandler:^(LGAlertView *alertView) {
                                                                 [alertView dismissAnimated:YES completionHandler:nil];
                                                             }
                                                        destructiveHandler:^(LGAlertView *alertView) {
                                                            [alertView dismissAnimated:YES completionHandler:nil];
                                                        }];
        
        //alertView.heightMax = 256.f;
        //alertView.windowLevel = LGAlertViewWindowLevelAboveStatusBar;
        //alertView.separatorsColor = [UIColor redColor];
        [Utils initAlertButtonColor:alertView];
        [alertView showAnimated:YES completionHandler:^(void)
         {
             [txtNum becomeFirstResponder];
         }];
        [alertView setDismissOnAction:NO];
    }
    else if (tag == 4) {
        
        UIDatePicker *datePicker = [UIDatePicker new];
        datePicker.datePickerMode = UIDatePickerModeDate;
        datePicker.frame = CGRectMake(0.f, 0.f, datePicker.frame.size.width, 160.f);
        
        if ([propertyMap[@"dob"] length]>0) {
            NSString *calDate = propertyMap[@"dob"];
            NSDate *date = [[NSDate alloc] init];
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
            
            [dateFormatter setDateFormat:@"MM/dd/yyyy"];
            
            date = [dateFormatter dateFromString:calDate];
            
            [datePicker setDate:date];
        }
        
        LGAlertView *alertView = [[LGAlertView alloc] initWithViewAndTitle:LocalizedStr(@"Menu.BecomeADriver.List.text17")
                                                                   message:nil
                                                                     style:LGAlertViewStyleAlert
                                                                      view:datePicker
                                                              buttonTitles:@[@"OK"]
                                                         cancelButtonTitle:@"Cancel"
                                                    destructiveButtonTitle:nil
                                                             actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                                                                 NSLog(@"actionHandler, %@, %lu", title, (long unsigned)index);
                                                                 
                                                                 NSDateFormatter __autoreleasing *dateFormat = [[NSDateFormatter alloc] init];
                                                                 [dateFormat setTimeZone:[NSTimeZone systemTimeZone]];
                                                                 [dateFormat setDateFormat:@"MM/dd/yyyy"];
                                                                 NSString *dateString = [dateFormat stringFromDate:datePicker.date];
                                                                 NSLog(@"dateString:%@",dateString);
                                                                 
                                                                 // webview set
                                                                 [webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"set_%@('%@');",fname,dateString]];
                                                                 
//                                                                 [dateFormat setDateFormat:@"yyyy-MM-dd"];
//                                                                 dateString = [dateFormat stringFromDate:datePicker.date];
                                                                 
                                                                 [self updateTrimString:dateString key:@"dob"];
                                                                 
                                                                 [self checkProcess];
                                                                 
                                                                 [alertView dismissAnimated:YES completionHandler:nil];
                                                             }
                                                             cancelHandler:^(LGAlertView *alertView) {
                                                                 [alertView dismissAnimated:YES completionHandler:nil];
                                                             }
                                                        destructiveHandler:^(LGAlertView *alertView) {
                                                            [alertView dismissAnimated:YES completionHandler:nil];
                                                        }];
        
        //alertView.heightMax = 256.f;
        //alertView.windowLevel = LGAlertViewWindowLevelAboveStatusBar;
        //alertView.separatorsColor = [UIColor redColor];
        [Utils initAlertButtonColor:alertView];
        [alertView showAnimated:YES completionHandler:^(void)
         {
         }];
        [alertView setDismissOnAction:NO];
        
    }
    else if (tag == 8) {
        
        dataArray = @[@"Alabama",
                      @"Alaska",
                      @"Arizona",
                      @"Arkansas",
                      @"California",
                      @"Colorado",
                      @"Connecticut",
                      @"Delaware",
                      @"Florida",
                      @"Georgia",
                      @"Hawaii",
                      @"Idaho",
                      @"Illinois",
                      @"Indiana",
                      @"Iowa",
                      @"Kansas",
                      @"Kentucky",
                      @"Louisiana",
                      @"Maine",
                      @"Maryland",
                      @"Massachusetts",
                      @"Michigan",
                      @"Minnesota",
                      @"Mississippi",
                      @"Missouri",
                      @"Montana",
                      @"Nebraska",
                      @"Nevada",
                      @"New Hampshire",
                      @"New Jersey",
                      @"New Mexico",
                      @"New York",
                      @"North Carolina",
                      @"North Dakota",
                      @"Ohio",
                      @"Oklahoma",
                      @"Oregon",
                      @"Pennsylvania",
                      @"Rhode Island",
                      @"South Carolina",
                      @"South Dakota",
                      @"Tennessee",
                      @"Texas",
                      @"Utah",
                      @"Vermont",
                      @"Virginia",
                      @"Washington",
                      @"West Virginia",
                      @"Wisconsin",
                      @"Wyoming"
                      ];
        
        
        NSArray *dataArrayCode = @[@"AL",
                                   @"AK",
                                   @"AZ",
                                   @"AR",
                                   @"CA",
                                   @"CO",
                                   @"CT",
                                   @"DE",
                                   @"FL",
                                   @"GA",
                                   @"HI",
                                   @"ID",
                                   @"IL",
                                   @"IN",
                                   @"IA",
                                   @"KS",
                                   @"KY",
                                   @"LA",
                                   @"ME",
                                   @"MD",
                                   @"MA",
                                   @"MI",
                                   @"MN",
                                   @"MS",
                                   @"MO",
                                   @"MT",
                                   @"NE",
                                   @"NV",
                                   @"NH",
                                   @"NJ",
                                   @"NM",
                                   @"NY",
                                   @"NC",
                                   @"ND",
                                   @"OH",
                                   @"OK",
                                   @"OR",
                                   @"PA",
                                   @"RI",
                                   @"SC",
                                   @"SD",
                                   @"TN",
                                   @"TX",
                                   @"UT",
                                   @"VT",
                                   @"VA",
                                   @"WA",
                                   @"WV",
                                   @"WI",
                                   @"WY"
                                   ];
        
        UITableView* mTableView = [[UITableView alloc] init];
        mTableView.delegate=self;
        mTableView.dataSource=self;
        
        mTableView.separatorColor = [UIColor clearColor];
        mTableView.allowsMultipleSelection = NO;
        mTableView.frame = CGRectMake(0.f, 0.f, 320, 260.f);
        mTableView.contentMode = UIViewContentModeScaleAspectFit;
        
        NSIndexPath *indexPath = nil;
        if ([propertyMap[@"licenseState"] length]>0) {
            indexPath = [NSIndexPath indexPathForRow:[dataArrayCode indexOfObject:propertyMap[@"licenseState"]] inSection:0];
        }
        if (indexPath != nil) {
            [mTableView selectRowAtIndexPath:indexPath
                                    animated:YES
                              scrollPosition:UITableViewScrollPositionNone];
            //[mTableView tableView:mTableView didSelectRowAtIndexPath:indexPath];
            [mTableView.delegate tableView:mTableView didSelectRowAtIndexPath:indexPath];
        }
        
        LGAlertView *alertView = [[LGAlertView alloc] initWithViewAndTitle:__title
                                                                   message:__subtitle
                                                                     style:LGAlertViewStyleAlert
                                                                      view:mTableView
                                                              buttonTitles:@[LocalizedStr(@"Button.OK")]
                                                         cancelButtonTitle:LocalizedStr(@"Button.CANCEL")
                                                    destructiveButtonTitle:nil
                                                             actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                                                                 NSLog(@"actionHandler, %@, %lu", title, (long unsigned)index);
                                                                 
                                                                 for(NSIndexPath* indexPath in [mTableView indexPathsForSelectedRows]) {
                                                                     NSLog(@"cell.tag:%@",[dataArrayCode objectAtIndex:indexPath.row]);
                                                                     
                                                                     [propertyMap setValue:[dataArrayCode objectAtIndex:indexPath.row] forKey:@"licenseState"];
                                                                     
                                                                     [self checkProcess];
                                                                 
                                                                     // webview set
                                                                     [webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"set_%@('%@');",fname,[dataArrayCode objectAtIndex:indexPath.row]]];
                                                                     
                                                                     
                                                                     break;
                                                                 }
                                                                 
                                                                 //                                                                 [mTableView reloadData];
                                                                 //                                                                 [mTableView setContentOffset:CGPointZero animated:YES];
                                                                 //mTableView.indexPathsForSelectedRows = nil;
                                                                 
                                                             }
                                                             cancelHandler:^(LGAlertView *alertView) {
                                                                 NSLog(@"cancelHandler");
                                                             }
                                                        destructiveHandler:^(LGAlertView *alertView) {
                                                            NSLog(@"destructiveHandler");
                                                        }];
        //alertView.heightMax = 256.f;
        [Utils initAlertButtonColor:alertView];
        alertView.windowLevel = LGAlertViewWindowLevelAboveStatusBar;
        [alertView showAnimated:YES completionHandler:nil];
        
    }
    else {
        LGAlertView *alertView = [[LGAlertView alloc] initWithTextFieldsAndTitle:__title
                                                                         message:__subtitle
                                                              numberOfTextFields:1
                                                          textFieldsSetupHandler:^(UITextField *textField, NSUInteger index)
                                  {
                                      if (index == 0) {
                                          textField.placeholder = @"";
                                          textField.text = @"";
                                      }
                                      
                                      textField.tag = index;
                                      //textField.delegate = self;
                                      textField.enablesReturnKeyAutomatically = YES;
                                      textField.autocapitalizationType = NO;
                                      textField.autocorrectionType = NO;
                                      textField.clearButtonMode = UITextFieldViewModeNever;
                                      textField.textAlignment = NSTextAlignmentCenter;
                                      if (tag == 6) {
                                          [textField setKeyboardType:UIKeyboardTypeNumberPad];
                                      }
                                  }
                                                                    buttonTitles:@[LocalizedStr(@"Button.OK")]
                                                               cancelButtonTitle:LocalizedStr(@"Button.CANCEL")
                                                          destructiveButtonTitle:nil
                                                                   actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                                                                       UITextField *secondTextField = alertView.textFieldsArray[index];
                                                                       //promoCode = secondTextField.text;
                                                                       
                                                                       NSLog(@"actionHandler, %@, %lu, %@", title, (long unsigned)index, secondTextField.text);
                                                                       
                                                                       [self resignKeyboard];
                                                                       Validator *validator = [[Validator alloc] init];
                                                                       validator.delegate   = self;
                                                                       if (tag == 0 || tag == 1 || tag == 3 || tag == 35) {
                                                                           //
                                                                           [validator putRule:[Rules minLength:2 withFailureString:LocalizedStr(@"Validator.Name.text") forTextField:secondTextField]];
                                                                           //[validator putRule:[Rules checkIfAlphaNumericWithFailureString:LocalizedStr(@"Validator.Name.text") forTextField:secondTextField]];
                                                                       }
                                                                       else if (tag == 4) {
                                                                           // 생년월일
                                                                       }
                                                                       else if (tag == 5) {
                                                                           // SSN
                                                                       }
                                                                       else if (tag == 6) {
                                                                           // 우편번호
                                                                           [validator putRule:[Rules checkRange:NSMakeRange(5, 9) withFailureString:LocalizedStr(@"Validator.ZipCode.text") forTextField:secondTextField]];
                                                                           [validator putRule:[Rules checkIfNumericWithFailureString:LocalizedStr(@"Validator.ZipCode.text") forTextField:secondTextField]];
                                                                           
                                                                           if (![validator validateForResult]) {
                                                                               [alertView.firstButton setSelected:NO];
                                                                               return;
                                                                           }
                                                                           
                                                                           [self updateTrimString:secondTextField.text key:@"zipcode"];
                                                                       }
                                                                       else if (tag == 7) {
                                                                           // 운전면허번호
                                                                           [validator putRule:[Rules minLength:4 withFailureString:LocalizedStr(@"Validator.LicenseNo.text") forTextField:secondTextField]];
                                                                           //[validator putRule:[Rules checkIfAlphaNumericWithFailureString:LocalizedStr(@"Validator.LicenseNo.text") forTextField:secondTextField]];
                                                                           
                                                                           if (![validator validateForResult]) {
                                                                               [alertView.firstButton setSelected:NO];
                                                                               return;
                                                                           }
                                                                           
                                                                           [self updateTrimString:secondTextField.text key:@"license"];
                                                                       }
                                                                       else if (tag == 8) {
                                                                           // 주
                                                                       }
                                                                       
                                                                       if (![validator validateForResult]) {
                                                                           [alertView.firstButton setSelected:NO];
                                                                           return;
                                                                       }
                                                                       
                                                                       // webview set
                                                                       [webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"set_%@('%@');",fname,secondTextField.text]];
                                                                       
                                                                       
                                                                       if (tag == 0) {
                                                                           [self updateTrimString:secondTextField.text key:@"firstName"];
                                                                       }
                                                                       else if (tag == 1) {
                                                                           [self updateTrimString:secondTextField.text key:@"middleName"];
                                                                       }
                                                                       else if (tag == 3) {
                                                                           [self updateTrimString:secondTextField.text key:@"lastName"];
                                                                       }
                                                                       else if (tag == 35) {
                                                                           [self updateTrimString:secondTextField.text key:@"signature"];
                                                                       }
                                                                       
                                                                       [self checkProcess];
                                                                       
                                                                       [alertView dismissAnimated:YES completionHandler:nil];
                                                                   }
                                                                   cancelHandler:^(LGAlertView *alertView) {
                                                                       [alertView dismissAnimated:YES completionHandler:nil];
                                                                       
                                                                   }
                                                              destructiveHandler:^(LGAlertView *alertView) {
                                                                  [alertView dismissAnimated:YES completionHandler:nil];
                                                              }];
        //[alertView setButtonAtIndex:0 enabled:NO];
        [Utils initAlertButtonColor:alertView];
        [alertView showAnimated:YES completionHandler:nil];
        [alertView setDismissOnAction:NO];
    }
    
}

- (void)nextButtonOn:(BOOL)on
{
    UIButton *btn = [super.view viewWithTag:999];
    btn.enabled = on;
    if (on)
        [btn setTitleColor:UIColorButtonText forState:UIControlStateNormal];
    else
        [btn setTitleColor:UIColorBasicTextOff forState:UIControlStateNormal];
}


-(void) checkProcess {
    NSInteger MaxProcessCnt = 10;
    if (self.setupMode == 2) {
        MaxProcessCnt = 1;
    }
    else if (self.setupMode == 3) {
        MaxProcessCnt = 5;
    }
    processPct = (int)[propertyMap count];
    lbProcessInfo.text = [NSString stringWithFormat:@"%d%@",(int)(processPct*100/MaxProcessCnt),LocalizedStr(@"Menu.BecomeADriver.Header.text2")];
    
    //if (processPct == MaxProcessCnt && _btnAgree.tag == 1) {
    if (processPct == MaxProcessCnt) {
        if (self.setupMode == 3) {
            if ([propertyMap[@"newYork"] isEqualToString:@"1"] &&
                [propertyMap[@"Washington"] isEqualToString:@"1"] &&
                [propertyMap[@"Minnesota"] isEqualToString:@"1"] &&
                [propertyMap[@"California"] isEqualToString:@"1"] &&
                ![propertyMap[@"signature"] isEqualToString:@""]
                ) {
                [self nextButtonOn:YES];
            }
            else {
                [self nextButtonOn:NO];
            }
        }
        else if (self.setupMode == 1) {
            if (![propertyMap[@"firstName"] isEqualToString:@""] &&
                (
                (![propertyMap[@"middleName"] isEqualToString:@""] && [propertyMap[@"middleCheck"] isEqualToString:@"0"]) ||
                ([propertyMap[@"middleCheck"] isEqualToString:@"1"])
                ) &&
//                ![propertyMap[@"middleName"] isEqualToString:@""] &&
                ![propertyMap[@"lastName"] isEqualToString:@""] &&
                ![propertyMap[@"dob"] isEqualToString:@""] &&
                ![propertyMap[@"ssn"] isEqualToString:@""] &&
                ![propertyMap[@"zipcode"] isEqualToString:@""] &&
                ![propertyMap[@"license"] isEqualToString:@""] &&
                ![propertyMap[@"licenseState"] isEqualToString:@""] &&
                [propertyMap[@"bottomCheck"] isEqualToString:@"1"]
                ) {
                [self nextButtonOn:YES];
            }
            else {
                [self nextButtonOn:NO];
            }
        }
        else {
            if ([propertyMap[@"bottomCheck"] isEqualToString:@"1"]) {
                [self nextButtonOn:YES];
            }
            else {
                [self nextButtonOn:NO];
            }
        }
    }
    else {
        [self nextButtonOn:NO];
    }
    
}

-(void)updateTrimString:(NSString*)str key:(NSString*)key
{
    NSString *trimmedString = [Utils trimStringOnly:str];
    
    if ([trimmedString isEqualToString:@""]) {
        [propertyMap removeObjectForKey:key];
    }
    else {
        [propertyMap setValue:str forKey:key];
    }
}

#pragma mark -
#pragma mark Validator
#pragma mark -
-(void) resignKeyboard
{
    [self.view endEditing:YES];
}

- (IBAction)validateAction:(id)sender
{
}

#pragma ValidatorDelegate - Delegate methods

- (void) preValidation
{
}

- (void)onSuccess
{
}

- (void)onFailure:(Rule *)failedRule
{
    [WToast showWithText:failedRule.failureMessage duration:kWTShort roundedCorners:NO gravity:kWTGravityTop];
}

#pragma mark - UIWebViewDelegate

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSMutableURLRequest *)theRequest
 navigationType:(UIWebViewNavigationType)navigationType
{
    
    // jscall://loginview
    // Sent before a web view begins loading content, useful to trigger actions before the WebView.
    NSString *strUrl = [[theRequest URL] absoluteString];
    //    NSLog(@"webview:%@",strUrl);
    if ([strUrl hasPrefix:@"jscall://"]) {
        NSString *strRequest = [[strUrl componentsSeparatedByString:@"jscall://"] objectAtIndex:1];
        NSArray *arrRequest = [strRequest componentsSeparatedByString:@"?"];
        NSString *strCmd = [arrRequest objectAtIndex:0];
        NSString *noPercentString = @"";
        NSMutableDictionary *queryStringDictionary = [[NSMutableDictionary alloc] init];
        if ([arrRequest count]>1) { // parameter message
            noPercentString = [[[strUrl componentsSeparatedByString:[NSString stringWithFormat:@"jscall://%@?",strCmd]] objectAtIndex:1] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            
            NSArray *urlComponents = [[arrRequest objectAtIndex:1] componentsSeparatedByString:@"&"];
            for (NSString *keyValuePair in urlComponents)
            {
                NSArray *pairComponents = [keyValuePair componentsSeparatedByString:@"="];
                NSString *key = [[pairComponents firstObject] stringByRemovingPercentEncoding];
                NSString *value = [[pairComponents lastObject] stringByRemovingPercentEncoding];
                
                [queryStringDictionary setObject:value forKey:key];
            }
        }
        //        NSArray* array = [noPercentString componentsSeparatedByString: @"="];
        NSLog(@"strCmd:%@",strCmd);
        if ( [strCmd isEqualToString:@"checkr"] ){
            if (self.setupMode == 1) {
                if ( ![Utils isNillDictionary:queryStringDictionary[@"firstName"]] ){
                    //
                    [self popupInput:0 fname:@"firstName" text:queryStringDictionary[@"firstName"] webView:webView];
                }
                else if ( ![Utils isNillDictionary:queryStringDictionary[@"middleName"]] ){
                    //
                    [self popupInput:1 fname:@"middleName" text:queryStringDictionary[@"middleName"] webView:webView];
                }
                else if ( ![Utils isNillDictionary:queryStringDictionary[@"middleCheck"]] ){
                    //
                    [self popupInput:2 fname:@"middleCheck" text:queryStringDictionary[@"middleCheck"] webView:webView];
                }
                else if ( ![Utils isNillDictionary:queryStringDictionary[@"lastName"]] ){
                    //
                    [self popupInput:3 fname:@"lastName" text:queryStringDictionary[@"lastName"] webView:webView];
                }
                else if ( ![Utils isNillDictionary:queryStringDictionary[@"dob"]] ){
                    //
                    [self popupInput:4 fname:@"dob" text:queryStringDictionary[@"dob"] webView:webView];
                }
                else if ( ![Utils isNillDictionary:queryStringDictionary[@"ssn"]] ){
                    //
                    [self popupInput:5 fname:@"ssn" text:queryStringDictionary[@"ssn"] webView:webView];
                }
                else if ( ![Utils isNillDictionary:queryStringDictionary[@"zipcode"]] ){
                    //
                    [self popupInput:6 fname:@"zipcode" text:queryStringDictionary[@"zipcode"] webView:webView];
                }
                else if ( ![Utils isNillDictionary:queryStringDictionary[@"license"]] ){
                    //
                    [self popupInput:7 fname:@"license" text:queryStringDictionary[@"license"] webView:webView];
                }
                else if ( ![Utils isNillDictionary:queryStringDictionary[@"licenseState"]] ){
                    //
                    [self popupInput:8 fname:@"licenseState" text:queryStringDictionary[@"licenseState"] webView:webView];
                }
                else if ( ![Utils isNillDictionary:queryStringDictionary[@"bottomCheck"]] ){
                    //
                    [self popupInput:9 fname:@"bottomCheck" text:queryStringDictionary[@"bottomCheck"] webView:webView];
                }
            }
            else if (self.setupMode == 2) {
                if ( ![Utils isNillDictionary:queryStringDictionary[@"bottomCheck"]] ){
                    //
                    [self popupInput:9 fname:@"bottomCheck" text:queryStringDictionary[@"bottomCheck"] webView:webView];
                }
            }
            else if (self.setupMode == 3) {
                if ( ![Utils isNillDictionary:queryStringDictionary[@"newYork"]] ){
                    //
                    [self popupInput:31 fname:@"newYork" text:queryStringDictionary[@"newYork"] webView:webView];
                }
                else if ( ![Utils isNillDictionary:queryStringDictionary[@"Washington"]] ){
                    //
                    [self popupInput:32 fname:@"Washington" text:queryStringDictionary[@"Washington"] webView:webView];
                }
                else if ( ![Utils isNillDictionary:queryStringDictionary[@"Minnesota"]] ){
                    //
                    [self popupInput:33 fname:@"Minnesota" text:queryStringDictionary[@"Minnesota"] webView:webView];
                }
                else if ( ![Utils isNillDictionary:queryStringDictionary[@"California"]] ){
                    //
                    [self popupInput:34 fname:@"California" text:queryStringDictionary[@"California"] webView:webView];
                }
                else if ( ![Utils isNillDictionary:queryStringDictionary[@"signature"]] ){
                    //
                    [self popupInput:35 fname:@"signature" text:queryStringDictionary[@"signature"] webView:webView];
                }
            }
            
        }
        
        // toApp protocol do not StartLoad
        return NO;
    }
    
    return YES;
}

- (void)webViewDidStartLoad:(UIWebView *)webView {
}

- (void)webViewDidWillUpdate:(UIWebView *)webView key:(NSString*)key value:(NSString*)value
{
    NSString *val = [Utils nullToString:value];
    //[self updateTrimString:val key:key];
    [propertyMap setValue:val forKey:key];
    
    if (![val isEqualToString:@""]) {
        // webview set
        [webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"set_%@('%@');",key,val]];
    }
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    
    if (self.setupMode == 1) {
        //[propertyMap setValue:@"x" forKey:@"middleName"];
        
        NSString *check = [Utils nullToString:result[@"no_middle_name"]];
        if ([check isEqualToString:@"false"] || [check isEqualToString:@"0"]) {
            //[self webViewDidWillUpdate:webView key:@"middleName" value:@""];
            //[propertyMap setValue:@"" forKey:@"middleName"];
            [self webViewDidWillUpdate:webView key:@"middleName" value:result[@"middle_name"]];
            [self webViewDidWillUpdate:webView key:@"middleCheck" value:@"0"];
        }
        else {
            [self webViewDidWillUpdate:webView key:@"middleName" value:@""];
            //[self webViewDidWillUpdate:webView key:@"middleName" value:result[@"middle_name"]];
            [self webViewDidWillUpdate:webView key:@"middleCheck" value:@"1"];
        }
        check = [Utils nullToString:result[@"check_welcome"]];
        if (![check isEqualToString:@""]) {
            [self webViewDidWillUpdate:webView key:@"bottomCheck" value:check];
        }
        else {
            [self webViewDidWillUpdate:webView key:@"bottomCheck" value:@"0"];
        }
        
        
        [self webViewDidWillUpdate:webView key:@"firstName" value:result[@"first_name"]];
        //[self webViewDidWillUpdate:webView key:@"middleName" value:result[@"middle_name"]];
        
        [self webViewDidWillUpdate:webView key:@"lastName" value:result[@"last_name"]];
        
        if (![result[@"dob"] isEqualToString:@""]) {
            NSDateFormatter __autoreleasing *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setTimeZone:[NSTimeZone systemTimeZone]];
            [dateFormat setDateFormat:@"yyyy-MM-dd"];
            NSDate *date = [dateFormat dateFromString:result[@"dob"]];
            [dateFormat setDateFormat:@"MM'/'dd'/'yyyy"];
            NSString* dob = [dateFormat stringFromDate:date];
            [self webViewDidWillUpdate:webView key:@"dob" value:dob];
        }
        else {
            [self webViewDidWillUpdate:webView key:@"dob" value:result[@"dob"]];
        }
        [self webViewDidWillUpdate:webView key:@"ssn" value:result[@"ssn"]];
        [self webViewDidWillUpdate:webView key:@"zipcode" value:result[@"zipcode"]];
        [self webViewDidWillUpdate:webView key:@"license" value:result[@"driver_license_number"]];
        [self webViewDidWillUpdate:webView key:@"licenseState" value:result[@"driver_license_state"]];
    }
    else if (self.setupMode == 2) {
        
        NSString *check = [Utils nullToString:result[@"check_disclosure"]];
        if (![check isEqualToString:@""]) {
            [self updateTrimString:check key:@"bottomCheck"];
            // webview set
            [webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"set_bottomCheck('%@');",check]];
        }
        else {
            [self updateTrimString:@"0" key:@"bottomCheck"];
        }
    }
    else if (self.setupMode == 3) {
        NSString *check = [Utils nullToString:result[@"check_ny"]];
        if (![check isEqualToString:@""]) {
            [self updateTrimString:check key:@"newYork"];
            // webview set
            [webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"set_newYork('%@');",check]];
        }
        else {
            [self updateTrimString:@"0" key:@"newYork"];
        }
        check = [Utils nullToString:result[@"check_wa"]];
        if (![check isEqualToString:@""]) {
            [self updateTrimString:check key:@"Washington"];
            // webview set
            [webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"set_Washington('%@');",check]];
        }
        else {
            [self updateTrimString:@"0" key:@"Washington"];
        }
        check = [Utils nullToString:result[@"check_mn"]];
        if (![check isEqualToString:@""]) {
            [self updateTrimString:check key:@"Minnesota"];
            // webview set
            [webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"set_Minnesota('%@');",check]];
        }
        else {
            [self updateTrimString:@"0" key:@"Minnesota"];
        }
        check = [Utils nullToString:result[@"check_ca"]];
        if (![check isEqualToString:@""]) {
            [self updateTrimString:check key:@"California"];
            // webview set
            [webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"set_California('%@');",check]];
        }
        else {
            [self updateTrimString:@"0" key:@"California"];
        }
        
        // webview set
        [self webViewDidWillUpdate:webView key:@"signature" value:result[@"esign"]];
    }
    
    [self checkProcess];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    
    // To avoid getting an error alert when you click on a link
    // before a request has finished loading.
    if ([error code] == NSURLErrorCancelled) {
        return;
    }
}

#pragma mark - UITableView DataSource Methods
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [tableView setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [tableView setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
    
    UIView *seperatorView = [[UIView alloc] initWithFrame:CGRectMake(0, cell.frame.size.height - 1, cell.frame.size.width, 1)];
    seperatorView.backgroundColor = UIColorBasicBack;
    [cell addSubview:seperatorView];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    // This will create a "invisible" footer
    return 0.01f;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [UIView new];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [dataArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    cell.tag = indexPath.row;
    int tag = (int)indexPath.row+100;
    //[self changeCell:tableView withIndexPath:indexPath andTag:tag];
    [self addCell:tableView withCell:cell withIndexPath:indexPath andTag:tag];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    cell.textLabel.text = [NSString stringWithFormat:@"      %@",[dataArray objectAtIndex:indexPath.row]];
    
    
    //    if([[tableView indexPathsForSelectedRows] containsObject:indexPath]) {
    //        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    //    } else {
    //        cell.accessoryType = UITableViewCellAccessoryNone;
    //    }
    
    return cell;
}

#pragma mark - UITableView Delegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self changeCell:tableView withIndexPath:indexPath andTag:((int)indexPath.row+100)];
    
    //cell.accessoryView.hidden = NO;
}
- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self changeCell:tableView withIndexPath:indexPath andTag:((int)indexPath.row+100)];
    
    //cell.accessoryView.hidden = YES;
}

-(void)addCell:(UITableView*)tableView withCell:(UITableViewCell*)cell withIndexPath:(NSIndexPath*)indexPath andTag:(int)tag{
    
    UIImageView *preView = [cell.contentView viewWithTag:tag];
    [preView removeFromSuperview];
    
    UILabel *preLabel = [cell.contentView viewWithTag:(tag+100)];
    [preLabel removeFromSuperview];
    
    NSInteger _h = cell.frame.size.height;
    NSInteger _x = 16;
    UIImageView* imv = [[UIImageView alloc]initWithFrame:CGRectMake(cell.frame.size.width - _x*4 ,_h/2-_x/2, _x, _x)];
    imv.tag = tag;
    UIImage *cellImage;
    
    if([[tableView indexPathsForSelectedRows] containsObject:indexPath]) {
        cellImage = [UIImage imageNamed:@"form_radio_chk"];
    }
    else {
        cellImage = [UIImage imageNamed:@"form_radio_normal"];
    }
    imv.image=cellImage;
    [cell.contentView addSubview:imv];
}

-(void)changeCell:(UITableView*)tableView withIndexPath:(NSIndexPath*)indexPath andTag:(int)tag{
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    UIImageView *preView = [cell.contentView viewWithTag:tag];
    [preView removeFromSuperview];
    
    UILabel *preLabel = [cell.contentView viewWithTag:(tag+100)];
    [preLabel removeFromSuperview];
    
    NSInteger _h = cell.frame.size.height;
    NSInteger _x = 16;
    UIImageView* imv = [[UIImageView alloc]initWithFrame:CGRectMake(cell.frame.size.width - _x*4 ,_h/2-_x/2, _x, _x)];
    //    UIImageView* imv = [[UIImageView alloc]initWithFrame:CGRectMake(0,_h/2-_x/2, _x, _x)];
    imv.tag = tag;
    UIImage *cellImage;
    
    if([[tableView indexPathsForSelectedRows] containsObject:indexPath]) {
        cellImage = [UIImage imageNamed:@"form_radio_chk"];
    }
    else {
        cellImage = [UIImage imageNamed:@"form_radio_normal"];
    }
    imv.image=cellImage;
    [cell.contentView addSubview:imv];
}


#pragma mark - RTLabel Delegate
-(void)rtLabel:(id)rtLabel didSelectLinkWithURL:(NSURL*)url
{
    
    //RTLabel * label = (RTLabel*)rtLabel;
    
    //[[UIApplication sharedApplication] openURL: [NSURL URLWithString:[url absoluteString]]];
    //[self openBrowserViewForURL:[url absoluteString] pageTitle:label.text];
    
}

@end
