//
//  MenuViewController.h
//  SlideMenu
//

//

#import <UIKit/UIKit.h>
#import "TXBaseViewController.h"
#import "REFormattedNumberField.h"

@interface TXCheckrVC : TXBaseViewController {

}

@property (strong, nonatomic) IBOutlet UIScrollView *myScrollView;
@property (strong, nonatomic) IBOutlet UIWebView *vbody;

@property (nonatomic) NSInteger setupMode;
@property (assign) BOOL isAnimation;

@end
