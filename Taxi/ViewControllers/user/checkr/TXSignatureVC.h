//
//  TXAskCardNumberVC.h
//  Taxi
//
//

#import <UIKit/UIKit.h>
#import "TXBaseViewController.h"
#import "UITextView+Placeholder.h"

@interface TXSignatureVC : TXBaseViewController <UITextViewDelegate> {
    
}

@property (strong, nonatomic) IBOutlet TXButton    *btnNext;
@property (strong, nonatomic) IBOutlet UILabel    *lbCategory;
@property (nonatomic, strong) IBOutlet UITextField  *tfText;

@end
