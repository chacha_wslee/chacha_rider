//
//  TXProfileVC.m
//  Taxi
//

//

#import "TXInviteVC.h"

@interface TXInviteVC () {
    int userId;
    NSArray *_msgList;
    
    NSString *inviteString;
    
}

@property (strong, nonatomic) UIActivityViewController *activityViewController;
@end

@implementation TXInviteVC {
    
}

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    
    if(self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        
    }
    
    return self;
}

-(void)dealloc {
    [self removeEventListeners];
}

-(void)configureStyles {
    [super configureStyles];
    
    //[super navigationType01X:[UIImage imageNamed:@"btn_back"] centerText:LocalizedStr(@"Menu.Invite.title")];
    if (appDelegate.isDriverMode) {
        [super navigationType01X:[UIImage imageNamed:@"btn_back"] centerText:LocalizedStr(@"Menu.Invite.title")];
    }
    else {
        [super navigationType01X:[UIImage imageNamed:@"btn_back"] centerText:@""];
    }
}

-(void)configure {
    [super configure];
    [self configureConfig];
    
    _msgList = @[
                 @[@"UR200",@""], // rider profile
                 @[@"PP200",@""], // driver profile
                 ];
    
    [self registerEventListeners];
    
    NSString *code = @"";
    NSString *name = @"";
    if (appDelegate.isDriverMode) {
        NSDictionary *user = [appDelegate.dicDriver objectForKey:@"provider"];
        code = [user objectForKey:@"code"];
        NSDictionary *device = [appDelegate.dicDriver objectForKey:@"provider_device"];
        name = [Utils nameToString:[device objectForKey:@"name"]];
    }
    else {
        NSDictionary *user = [appDelegate.dicRider objectForKey:@"user"];
        code = [user objectForKey:@"code"];
        NSDictionary *device = [appDelegate.dicRider objectForKey:@"user_device"];
        name = [Utils nameToString:[device objectForKey:@"name"]];
    }
    
    if ([[Utils nullToString:code] isEqualToString:@""]) {
        if (appDelegate.isDriverMode) {
            [self->model PP200];
        }
        else {
            [self->model UR200];
        }
    }
    else {
#ifdef _DRIVER_MODE
        self->inviteString = [NSString stringWithFormat:LocalizedStr(@"Menu.Invite.invite.message.text"),name,code,_INVITE_DRIVER_URL];
#else
        self->inviteString = [NSString stringWithFormat:LocalizedStr(@"Menu.Invite.invite.message.text"),name,code,_INVITE_RIDER_URL];
#endif
    }
    
    //[Utils layerButton:_btnSNS];
#ifdef _WITZM
    [_btnSNS setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    _btnSNS.backgroundColor = UIColorDefault;
#elif defined _CHACHA
    [_btnSNS setTitleColor:UIColorButtonText forState:UIControlStateNormal];
    _btnSNS.backgroundColor = UIColorDefault;
#endif
    
    [_btnSNS setTitle:LocalizedStr(@"Menu.Invite.BTN.invite") forState:UIControlStateNormal];
    _btnSNS.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:18.0f];
    _btnSNS.tag = 202;
    _btnSNS.frame = CGRectMake(kLeftMargin, self.view.frame.size.height - kBottomButtonHeight - kBottomBottonSafeArea, self.view.frame.size.width - 20, kBottomButtonHeight);
    
    if (appDelegate.isDriverMode) {
#ifdef _WITZM
        _lbtext1.text = LocalizedStr(@"Menu.Invite.Driver.text1");
        _lbtext2.text = LocalizedStr(@"Menu.Invite.Driver.text2");
#elif defined _CHACHA
        _lbtext1.text = @"";
        _lbtext2.text = @"";
#endif
    }
    else {
        _lbtext0.text = LocalizedStr(@"Menu.Invite.text0");
        _lbtext1.text = LocalizedStr(@"Menu.Invite.text1");
#ifdef _WITZM
        _lbtext2.text = LocalizedStr(@"Menu.Invite.text2");
#elif defined _CHACHA
        _lbtext2.text = [NSString stringWithFormat:LocalizedStr(@"Menu.Invite.text2"),name,name];
#endif

    }
    
    _lbCodetext.text = LocalizedStr(@"Menu.Invite.code.text");
    _lbCode.text = [Utils nullToString:code];
    _lbCode.textColor = UIColorBasicText;
//    _lbCode.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
//    _lbCode.backgroundColor = [UIColor clearColor];
//    //label.textColor = [UIColor colorWithRed:62/255.0f green:68/255.0f blue:75/255.0f alpha:1.0f];
//    [_lbCode sizeToFit];
//    _lbCode.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
    
    if (appDelegate.isDriverMode) {
        _lbtext1.textColor = HEXCOLOR(0x000000ff);
        [_lbtext1 setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:18.0f]];
    }
    else {
        _lbtext1.textColor = HEXCOLOR(0x666666ff);
        [_lbtext1 setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:14.0f]];
    }
    //[_lbtext1 sizeToFit];
    
    _lbtext2.textColor = HEXCOLOR(0x666666ff);
    [_lbtext2 setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:14.0f]];
    [_lbtext2 sizeToFit];
    
    _lbCodetext.textColor = HEXCOLOR(0x666666ff);
    [_lbCodetext setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:14.0f]];
    //[_lbCodetext sizeToFit];
}

-(void) configureConfig {

    
}
-(void) registerEventListeners {
    for (NSArray *msg in _msgList) {
        [self->model addEventListener:self forEvent:[msg objectAtIndex:0] eventParams:nil];
    }
}

-(void) removeEventListeners {
    for (NSArray *msg in _msgList) {
        [self->model removeEventListener:self forEvent:[msg objectAtIndex:0]];
    }
}

#pragma mark - Action
-(IBAction)onNaviButtonClick:(id)sender {
    UIButton *btn = (UIButton*)sender;
    //[super onNaviButtonClick:sender];
    
    if (btn.tag == 1100) {
        [self removeEventListeners];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark - IBAction
-(IBAction)onClick:(id)sender {
    
    UIButton *btn = (UIButton*)sender;
    if (btn.tag == 202) {
        //
        if ([_lbCode.text isEqualToString:@""]) {
            return;
        }
        
        // Activity view
        NSMutableArray *activityItems = [NSMutableArray arrayWithObject:self->inviteString];
        
        self.activityViewController = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
        
        __typeof__(self) __weak selfBlock = self;
        
        {
            [self.activityViewController setCompletionWithItemsHandler:^(NSString *activityType, BOOL completed, NSArray *returnedItems, NSError *activityError) {
                selfBlock.activityViewController = nil;
            }];
        }
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            [self presentViewController:self.activityViewController animated:YES completion:nil];
        }
    }
}

#pragma mark - onEvent
-(void)onEvent:(TXEvent *)event eventParams:(id)subscriptionParams {
    
    TXResponseDescriptor * descriptor = [event getEventProperty:TXEvents.Params.DESCRIPTOR];
    TXRequestObj *request = [event getEventProperty:TXEvents.Params.REQUEST];
    
    [self hideBusyIndicator];
    if([event.name isEqualToString:@"UR200"]) {
        
        if(descriptor.success == true) {
            NSString     *response = [[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding];
            NSDictionary *result   = getJSONObj(response);
            
            NSDictionary *user = [result objectForKey:@"user"];
            if ([Utils isDictionary:user]) {
                _lbCode.text = user[@"code"];
#ifdef _DRIVER_MODE
                self->inviteString = [NSString stringWithFormat:LocalizedStr(@"Menu.Invite.invite.message.text"),[Utils nameToString:[user objectForKey:@"name"]],_lbCode.text,_INVITE_DRIVER_URL];
#else
                self->inviteString = [NSString stringWithFormat:LocalizedStr(@"Menu.Invite.invite.message.text"),[Utils nameToString:[user objectForKey:@"name"]],_lbCode.text,_INVITE_RIDER_URL];
#endif
                // 1. 정보 업데이트
                NSMutableDictionary *dic = [appDelegate.dicRider mutableCopy];
                [dic setObject:[result valueForKey:@"user"] forKey:@"user"];
                appDelegate.dicRider = dic;
            }
        } else {
            //에러주석[self alertError:@"Error" message:descriptor.error];
        }
    }
    else if([event.name isEqualToString:@"PP200"]) {
        
        if(descriptor.success == true) {
            NSString     *response = [[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding];
            NSDictionary *result   = getJSONObj(response);
            
            NSDictionary *user = [result objectForKey:@"provider"];
            if ([Utils isDictionary:user]) {
                _lbCode.text = user[@"code"];
#ifdef _DRIVER_MODE
                self->inviteString = [NSString stringWithFormat:LocalizedStr(@"Menu.Invite.invite.message.text"),[Utils nameToString:[user objectForKey:@"name"]],_lbCode.text,_INVITE_DRIVER_URL];
#else
                self->inviteString = [NSString stringWithFormat:LocalizedStr(@"Menu.Invite.invite.message.text"),[Utils nameToString:[user objectForKey:@"name"]],_lbCode.text,_INVITE_RIDER_URL];
#endif
                // 1. 정보 업데이트
                NSMutableDictionary *dic = [appDelegate.dicDriver mutableCopy];
                [dic setObject:[result valueForKey:@"provider"] forKey:@"provider"];
                appDelegate.dicDriver = dic;
            }
        } else {
            //에러주석[self alertError:@"Error" message:descriptor.error];
        }
    }
}

@end
