//
//  TXProfileVC.h
//  Taxi
//

//
#import <UIKit/UIKit.h>
#import "TXUserVC.h"

@interface TXInviteVC : TXUserVC

@property (nonatomic, strong) IBOutlet UILabel* lbtext0;
@property (nonatomic, strong) IBOutlet UILabel* lbtext1;
@property (nonatomic, strong) IBOutlet UILabel* lbCodetext;
@property (nonatomic, strong) IBOutlet UILabel* lbCode;
@property (nonatomic, strong) IBOutlet UILabel* lbtext2;

@property (nonatomic, strong) IBOutlet TXButton* btnSNS;

@end
