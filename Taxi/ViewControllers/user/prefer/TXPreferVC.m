//
//  MenuViewController.m
//  SlideMenu
//

//

#import "TXPreferVC.h"
#import "TXPreferTagVC.h"
#import "RTLabel.h"
#import "LGAlertView.h"
#import "TXCallModel.h"
#import "TXLauncherVC.h"
//#import "RedisSingleton.h"

@interface TXPreferVC() {
    NSMutableArray *items;
    NSArray *_msgList;
    
    NSArray *dataArray;
    UILabel *mapName;
}

@end

@implementation TXPreferVC {
}

-(void)viewDidLoad {
    
    [super viewDidLoad];
    
    [super configureBottomList];
    UIView *n = [super navigationView];
    NSInteger _y = n.frame.origin.y + n.frame.size.height;
/*
#ifdef _WITZM
    UIView *line = [n viewWithTag:1500];
    line.backgroundColor = [UIColor clearColor];
    
    UILabel *label = (UILabel*)[[super navigationView] viewWithTag:1111];
    label.tag = 2001;
    label.text = LocalizedStr(@"Menu.Prefer.sub.title");
    label.textColor = HEXCOLOR(0x666666ff);
    
    for(NSString *lang in DEFAULT_LANGUAGES) {
        if ([lang isEqualToString:@"korean"]) { // 10
            //
            [_btnLang1 setTitle:lang forState:UIControlStateNormal];
            [self initButton:_btnLang1];
        }
        else if ([lang isEqualToString:@"english"]) { // 11
            //
            [_btnLang2 setTitle:lang forState:UIControlStateNormal];
            [self initButton:_btnLang2];
        }
        else if ([lang isEqualToString:@"chinese"]) { // 12
            //
            [_btnLang3 setTitle:lang forState:UIControlStateNormal];
            [self initButton:_btnLang3];
        }
        else if ([lang isEqualToString:@"japanese"]) { // 13
            //
            [_btnLang4 setTitle:lang forState:UIControlStateNormal];
            [self initButton:_btnLang4];
        }
    }
#endif
*/
    _vLine.backgroundColor = UIColorBasicBack;
    _lbTag.text= LocalizedStr(@"Menu.Prefer.tag.title");
    _tvTag.text = @"";
    _tvTag.textColor = HEXCOLOR(0x666666ff);
    _lbtvTag.text = @"";
    _lbtvTag.textColor = HEXCOLOR(0x666666ff);
    self.vBody.frame = CGRectMake(0,
                                  _y,
                                  self.view.frame.size.width,
                                  self.view.frame.size.height - _y);
    
    [self.view addSubview:self.vBody];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self showBusyIndicator:@"Requesting ... "];
    if (appDelegate.isDriverMode) {
        [self->model requsetAPI:@"PP206" property:nil];
    }
    else {
        [self->model requsetAPI:@"UR206" property:nil];
    }
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

-(void)dealloc {
    [self removeEventListeners];
}

#pragma mark - Configure

-(void)configureStyles {
    [super configureStyles];
    
    
    [super navigationType01X:[UIImage imageNamed:@"btn_back"] centerText:LocalizedStr(@"Menu.Prefer.title")];

}

-(void)configure {
    [super configure];
    
    // 2. init event
    _msgList = @[
                 @[@"UR210",@""], // rider
                 @[@"UR216",@""], // rider
                 @[@"UR206",@""], // rider
                 
                 @[@"PP210",@""], // driver
                 @[@"PP216",@""], // driver
                 @[@"PP206",@""], // driver
                 ];
    
    [self registerEventListeners];
    
//    [self->model requsetAPI:@"UR206" property:nil];
}

-(void) registerEventListeners {
    for (NSArray *msg in _msgList) {
        [self->model addEventListener:self forEvent:[msg objectAtIndex:0] eventParams:nil];
    }
}

-(void) removeEventListeners {
    for (NSArray *msg in _msgList) {
        [self->model removeEventListener:self forEvent:[msg objectAtIndex:0]];
    }
}

#pragma mark - Action
-(IBAction)onNaviButtonClick:(id)sender {
    UIButton *btn = (UIButton*)sender;
    NSLog(@"onNaviButtonClick");
    //[super onNaviButtonClick:sender];
    
    if (btn.tag == 1100) {
        [self removeEventListeners];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

-(IBAction)langButtonPressed:(id)sender {
    UIButton *btn = (UIButton*)sender;
    
//    if (![btn isEqual:_btnLang1]) {
//        [self changeButton:_btnLang1 isOn:NO];
//    }
//    if (![btn isEqual:_btnLang2]) {
//        [self changeButton:_btnLang2 isOn:NO];
//    }
//    if (![btn isEqual:_btnLang3]) {
//        [self changeButton:_btnLang3 isOn:NO];
//    }
//    if (![btn isEqual:_btnLang4]) {
//        [self changeButton:_btnLang4 isOn:NO];
//    }
    
    if (btn.tag) {
        [self changeButton:btn isOn:NO api:YES];
    }
    else {
        [self changeButton:btn isOn:YES api:YES];
    }
}

-(IBAction)editButtonPressed:(id)sender {
    // edit
    TXPreferTagVC *vc = [[TXPreferTagVC alloc] initWithNibName:@"TXPreferTagVC" bundle:[NSBundle mainBundle]];
    vc.content = _tvTag.text;
    //vc.content = _lbtvTag.text;
    [super pushViewController:vc];
}

-(void)changeButton:(UIButton*)button isOn:(BOOL)flag api:(BOOL)isapi{
    
    // on
    if (flag) {
        [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        button.backgroundColor = UIColorDefault;
        button.tag = 1;
    }
    else {
        [button setTitleColor:HEXCOLOR(0x666666ff) forState:UIControlStateNormal];
        button.backgroundColor = [UIColor whiteColor];
        button.tag = 0;
    }
    
    NSString *language = @"";
    for(NSString *lang in DEFAULT_LANGUAGES) {
        if ([[lang lowercaseString] isEqualToString:@"korean"]) { // 10
            if (_btnLang1.tag) {
                language = [NSString stringWithFormat:@"%@ %@",language, lang];
            }
        }
        else if ([[lang lowercaseString] isEqualToString:@"english"]) { // 11
            if (_btnLang2.tag) {
                language = [NSString stringWithFormat:@"%@ %@",language, lang];
            }
        }
        else if ([[lang lowercaseString] isEqualToString:@"chinese"]) { // 12
            if (_btnLang3.tag) {
                language = [NSString stringWithFormat:@"%@ %@",language, lang];
            }
        }
        else if ([[lang lowercaseString] isEqualToString:@"japanese"]) { // 13
            if (_btnLang4.tag) {
                language = [NSString stringWithFormat:@"%@ %@",language, lang];
            }
        }
    }
    
    if (isapi) {
        NSString *replaced = [Utils replaceSpace:language];
        replaced = [replaced stringByReplacingOccurrencesOfString:@","
                                                       withString:@" "];
        NSArray *stringArray = [replaced componentsSeparatedByString:@" "];
        
        if (appDelegate.isDriverMode) {
            [self onPP210:@{@"languages":stringArray}];
        }
        else {
            [self onUR210:@{@"languages":stringArray}];
        }
    }
    
//    NSDictionary *propertyMap = @{
//                                  @"name" : @"language",
//                                  @"values" : stringArray
//                                  };
//    
//    [self showBusyIndicator:@""];
//    [self->model requsetAPI:@"UR216" property:propertyMap];
}

-(void)initButton:(UIButton*)button {
    
    [button setTitleColor:HEXCOLOR(0x666666ff) forState:UIControlStateNormal];
    button.backgroundColor = [UIColor whiteColor];
    button.tag = 0;
    
    button.layer.cornerRadius = 5;
    button.layer.borderWidth = 2;
    button.layer.borderColor = UIColorDefault.CGColor;
}

-(void)onUR210:(NSDictionary*)_propertyMap {
    
    NSString* useq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.USEQD];
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:useq, @"useq", nil];
    
    NSDictionary *user = [appDelegate.dicRider objectForKey:@"user"];
    dic[@"uid"] = user[@"uid"];
    //dic[@"utelno"] = user[@"utelno"];
    [dic addEntriesFromDictionary:_propertyMap];
    
    [self showBusyIndicator:@"Updating info ... "];
    [self->model requsetAPI:API_FUNC(NSStringFromSelector(_cmd)) property:dic];
}

-(void)onPP210:(NSDictionary*)_propertyMap {
    
    NSString* pseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.PSEQD];
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:pseq, @"pseq", nil];
    
    NSDictionary *user = [appDelegate.dicDriver objectForKey:@"provider"];
    dic[@"uid"] = user[@"uid"];
    //dic[@"utelno"] = user[@"utelno"];
    [dic addEntriesFromDictionary:_propertyMap];
    
    [self showBusyIndicator:@"Updating info ... "];
    [self->model requsetAPI:API_FUNC(NSStringFromSelector(_cmd)) property:dic];
}

#pragma mark - onEvent
-(void)onEvent:(TXEvent *)event eventParams:(id)subscriptionParams {
    
    TXResponseDescriptor * descriptor = [event getEventProperty:TXEvents.Params.DESCRIPTOR];
    TXRequestObj *request = [event getEventProperty:TXEvents.Params.REQUEST];
    
    if([event.name isEqualToString:@"UR206"] || [event.name isEqualToString:@"PP206"]) {
        
        if(descriptor.success == true) {
            NSString     *response = [[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding];
            NSDictionary *result   = getJSONObj(response);
            NSString *prefer = @"";
            NSArray *prefers;
            
            if ([event.name isEqualToString:@"PP206"]) {
                prefers = result[@"provider_matchs"];
            }
            else {
                prefers = result[@"user_matchs"];
            }
            for(NSDictionary *match in prefers) {
                /*
#ifdef _WITZM
                if ([match[@"name"] isEqualToString:@"language"]) {
                    for(NSString *lang in match[@"values"]) {
                        if ([lang isEqualToString:@"korean"]) { // 10
                            //
                            //[_btnLang1 setTitle:lang forState:UIControlStateNormal];
                            [self changeButton:_btnLang1 isOn:YES api:NO];
                        }
                        else if ([lang isEqualToString:@"english"]) { // 11
                            //
                            //[_btnLang2 setTitle:lang forState:UIControlStateNormal];
                            [self changeButton:_btnLang2 isOn:YES api:NO];
                        }
                        else if ([lang isEqualToString:@"chinese"]) { // 12
                            //
                            //[_btnLang3 setTitle:lang forState:UIControlStateNormal];
                            [self changeButton:_btnLang3 isOn:YES api:NO];
                        }
                        else if ([lang isEqualToString:@"japanese"]) { // 13
                            //
                            //[_btnLang4 setTitle:lang forState:UIControlStateNormal];
                            [self changeButton:_btnLang4 isOn:YES api:NO];
                        }
                    }
                }
                else
#endif
                 */
                if ([match[@"name"] isEqualToString:@"tag"]) {
                    for(NSString *lang in match[@"values"]) {
                        prefer = [NSString stringWithFormat:@"%@ %@",prefer, lang];
                    }
                    _tvTag.text = [Utils trimStringOnly:prefer];
                    //_lbtvTag.text = [Utils trimStringOnly:prefer];
                    //[_lbtvTag sizeToFit];
                }
            }
            
        } else {
            //에러주석[self alertError:@"Error" message:descriptor.error];
        }
    }
    [self hideBusyIndicator];
}

@end
