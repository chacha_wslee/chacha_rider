//
//  TXAskCardNumberVC.m
//  Taxi

//

#import "TXPreferTagVC.h"

#import "Validator.h"
#import "WToast.h"
//#import "CRToast.h"

@interface TXPreferTagVC () <ValidatorDelegate>{
    NSArray *dataArray;
    NSArray *_msgList;
}

@end

@implementation TXPreferTagVC {
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [super configureBottomLine];
    UIView *line = (UIView*)[[super navigationView] viewWithTag:1500];
    CGRect rect = line.frame;
    rect.size.height = 2;
    line.frame = rect;
//    UILabel *label = (UILabel*)[[super navigationView] viewWithTag:1111];
//    label.tag = 2001;
//    label.text = LocalizedStr(@"Menu.Qna.title");
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    _lbcontent.text = LocalizedStr(@"Menu.Prefer.tag.guide.text");
    [_lbcontent sizeToFit];
    
    UIButton *btn = [Utils addBottomButton:LocalizedStr(@"Menu.Prefer.BTN.text") target:self selector:@selector(onNaviButtonClick:) color:YES];
    btn.tag = 2300;
    [self.view addSubview:btn];
    
    UIView *n = [super navigationView];
    NSInteger _y = n.frame.origin.y + n.frame.size.height;
    CGRect frame = _tvText.frame;
    frame.origin.y = _y + 22 + _lbcontent.frame.size.height + 20;
    frame.size.height = self.view.frame.size.height - frame.origin.y - btn.frame.size.height - 10;
    _tvText.frame = frame;
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [_tvText becomeFirstResponder];
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

-(void)dealloc {
    [self removeEventListeners];
}

-(void)configureStyles {
    [super configureStyles];
    
    if (appDelegate.isDriverMode) {
        [super navigationType01X:[UIImage imageNamed:@"btn_back_02"] centerText:LocalizedStr(@"Menu.Prefer.tag.title")];
    }
    else {
        [super navigationType01X:[UIImage imageNamed:@"btn_back_02"] centerText:LocalizedStr(@"Menu.Prefer.tag.title")];
    }
}

- (void)nextButtonOn:(BOOL)on
{
    UIButton *btn = [super.view viewWithTag:2300];
    btn.enabled = on;
    if (on)
        [btn setTitleColor:UIColorBasicTextOn forState:UIControlStateNormal];
    else
        [btn setTitleColor:UIColorBasicTextOff forState:UIControlStateNormal];
}

-(void) configure {
    [super configure];
    
    UIView *n = [super navigationView];
    //NSInteger _y = n.frame.origin.y + n.frame.size.height;
    [n setBackgroundColor:HEXCOLOR(0x333333ff)];
    UILabel *lb = [n viewWithTag:1210];
    
    lb.textColor = [UIColor whiteColor];
    
    // 2. init event
    _msgList = @[
//                 @[@"UR501",@""], // rider
//                 @[@"PP501",@""], // driver
                 
                 @[@"UR216",@""], // rider post
                 @[@"PP216",@""], // driver post
                 ];
    
    [self registerEventListeners];
    
//    if (appDelegate.isDriverMode) {
//        [self configureDriver];
//    }
//    else {
//        [self configureRider];
//    }
    
//    UIButton *btn = [self.view viewWithTag:2300];
//    CGRect frame = _tvText.frame;
//    frame.origin.y = _y + 22;
//    frame.size.height = self.view.frame.size.height - frame.origin.y - btn.frame.size.height - 20;
//    _tvText.frame = frame;
    //------------------------------------------------------------------------------------------------------------------------
//    ((UILabel *)[self.view viewWithTag:111]).text = LocalizedStr(@"Menu.Payment.List1.text1");
//    ((UILabel *)[self.view viewWithTag:121]).text = LocalizedStr(@"Menu.Payment.List1.text1");
    _tvText.textColor = UIColorDefault;
    //------------------------------------------------------------------------------------------------------------------------
    
    [self initTextView];
    
    self.view.backgroundColor = HEXCOLOR(0x333333ff);
}

-(void) registerEventListeners {
    for (NSArray *msg in _msgList) {
        [self->model addEventListener:self forEvent:[msg objectAtIndex:0] eventParams:nil];
    }
}

-(void) removeEventListeners {
    for (NSArray *msg in _msgList) {
        [self->model removeEventListener:self forEvent:[msg objectAtIndex:0]];
    }
}

-(void)initTextView {
//    NSArray *stringArray = [_content componentsSeparatedByString:@","];
//    NSString *tags = @"";
    
//    for (NSString *tag in stringArray) {
//        //
//        tags = [NSString stringWithFormat:@"%@ %@",tags, tag];
//    }
//    _tvText.text = tags;
//    _tvText.text = [stringArray componentsJoinedByString:@" "];
    _tvText.text = _content;
}

-(NSArray*)textViewToArray {
    
    NSString *tags = _tvText.text;
    
    NSString *replaced = [Utils replaceSpace:tags];
    replaced = [replaced stringByReplacingOccurrencesOfString:@","
                                                   withString:@" "];
    NSArray *stringArray = [replaced componentsSeparatedByString:@" "];

//    replaced = [replaced stringByReplacingOccurrencesOfString:@" "
//                                                   withString:@","];
//    NSArray *stringArray = [replaced componentsSeparatedByString:@","];
    
    return stringArray;
}


/*
-(void) configureDriver {
    
    // 3. call api
    [self->model PP501:@""];
}

-(void) configureRider {
    
    // 3. call api
    [self->model UR501:@""];
}
*/
#pragma mark - Action
-(IBAction)onNaviButtonClick:(id)sender {
    UIButton *btn = (UIButton*)sender;
    if (btn.tag == 1100 || btn.tag == 1101) {
        [self removeEventListeners];
        [self.navigationController popViewControllerAnimated:YES];
    }
    else if(btn.tag == 2300) {
        [self next:nil];
    }
}

-(void)next:(id)sender {
    
    [self validateAction:sender];
    return;
}

#pragma mark - Event
-(void)onFail:(id)object error:(TXError *)error {
    
}

-(void)onEvent:(TXEvent *)event eventParams:(id)subscriptionParams {
    
    TXResponseDescriptor *descriptor = [event getEventProperty:TXEvents.Params.DESCRIPTOR];
    //TXRequestObj *request = [event getEventProperty:TXEvents.Params.REQUEST];
    
    if([event.name isEqualToString:@"UR216"] || [event.name isEqualToString:@"PP216"]) {
        
        if(descriptor.success == true) {
            
            [self removeEventListeners];
            [self.navigationController popViewControllerAnimated:YES];
            
        } else {
            //[self alertError:LocalizedStr(@"String.Alert") message:descriptor.error];
            //        NSString *message = [TXCode2MsgTranslator messageForCode:descriptor.rlt];
            //        [self alertError:@"Error" message:message];
        }
        [self hideBusyIndicator];
    }
}

#pragma mark - Keyboard Notification Method
- (void)keyboardWillShow:(NSNotification*)note{
    NSDictionary *userInfo = note.userInfo;
    CGRect finalKeyboardFrame = [userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    NSTimeInterval animationDuration = [userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    UIButton *btn = [self.view viewWithTag:2300];
    float inputViewFinalYPosition = self.view.bounds.size.height - finalKeyboardFrame.size.height - btn.frame.size.height;
    CGRect inputViewFrame = btn.bounds;
    inputViewFrame.origin.y = inputViewFinalYPosition;
    
    [UIView animateWithDuration:animationDuration animations:^{
        btn.frame = inputViewFrame;
    }];
    
    // textview
    UIView *n = [super navigationView];
    NSInteger _y = n.frame.origin.y + n.frame.size.height;
    CGRect frame = _tvText.frame;
    frame.origin.y = _y + 22 + _lbcontent.frame.size.height + 30;
    frame.size.height = self.view.frame.size.height - frame.origin.y - inputViewFrame.origin.y - 20;
    _tvText.frame = frame;
}

- (void)keyboardWillHide:(NSNotification*)note{
    
    NSDictionary *userInfo = note.userInfo;
    NSTimeInterval animationDuration = [userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    UIButton *btn = [self.view viewWithTag:2300];
    CGRect inputViewFrame = btn.bounds;
    inputViewFrame.origin.y = self.view.bounds.size.height - btn.frame.size.height;
    
    [UIView animateWithDuration:animationDuration animations:^{
        btn.frame = inputViewFrame;
    }];
    
    // textview
    UIView *n = [super navigationView];
    NSInteger _y = n.frame.origin.y + n.frame.size.height;
    CGRect frame = _tvText.frame;
    frame.origin.y = _y + 22 + _lbcontent.frame.size.height + 30;
    frame.size.height = self.view.frame.size.height - frame.origin.y - btn.frame.size.height - 20;
    _tvText.frame = frame;
}

#pragma mark -
#pragma mark Validator
#pragma mark -
-(void) resignKeyboard
{
    [self.view endEditing:YES];
}

- (IBAction)validateAction:(id)sender
{
    [self resignKeyboard];
    
//    Validator *validator = [[Validator alloc] init];
//    validator.delegate   = self;
//    [validator putRule:[Rules minLength:2 withFailureString:LocalizedStr(@"Validator.CardNo.text") forTextField:self.tvText]];
//    
//    [validator validate];
    
    if (self.tvText.text.length == 0) {
        [WToast showWithText:LocalizedStr(@"Validator.Prefer.text") duration:kWTShort roundedCorners:NO gravity:kWTGravityTop];
        return;
    }
    [self onSuccess];
}

#pragma ValidatorDelegate - Delegate methods

- (void) preValidation
{
}

- (void)onSuccess
{
    NSArray *values = [self textViewToArray];
    if (!values.count) {
        return;
    }
    NSDictionary *propertyMap = @{
                    @"name" : @"tag",
                    @"values" : values
                    };
    
    [self showBusyIndicator:@""];
    if (appDelegate.isDriverMode) {
        [self->model requsetAPI:@"PP216" property:propertyMap];
    }
    else {
        [self->model requsetAPI:@"UR216" property:propertyMap];
    }
    
    
    //NSLog(@"propertyMap:%@",propertyMap);
}

- (void)onFailure:(Rule *)failedRule
{
    [WToast showWithText:failedRule.failureMessage duration:kWTShort roundedCorners:NO gravity:kWTGravityTop];
}

@end
