//
//  MenuViewController.h
//  SlideMenu
//

//

#import <UIKit/UIKit.h>
#import "TXBaseViewController.h"

@interface TXPreferVC : TXBaseViewController {
    
}

@property (nonatomic, strong) IBOutlet UIView *vBody;

@property (nonatomic, strong) IBOutlet UIButton *btnLang1;
@property (nonatomic, strong) IBOutlet UIButton *btnLang2;
@property (nonatomic, strong) IBOutlet UIButton *btnLang3;
@property (nonatomic, strong) IBOutlet UIButton *btnLang4;

@property (nonatomic, strong) IBOutlet UIView *vLine;
@property (nonatomic, strong) IBOutlet UILabel *lbTag;

@property (nonatomic, strong) IBOutlet UITextView *tvTag;
@property (nonatomic, strong) IBOutlet UILabel *lbtvTag;
@end
