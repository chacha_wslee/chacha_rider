//
//  TXAskCardNumberVC.h
//  Taxi
//
//

#import <UIKit/UIKit.h>
#import "TXBaseViewController.h"

@interface TXPreferTagVC : TXBaseViewController <UITextViewDelegate> {
    
}

@property (strong, nonatomic) IBOutlet TXButton    *btnNext;
@property (strong, nonatomic) IBOutlet UIButton    *btnCategory;
@property (nonatomic, strong) IBOutlet UITextField  *tfText;
@property (nonatomic, strong) IBOutlet UITextView  *tvText;

@property (nonatomic, strong) IBOutlet UILabel  *lbcontent;

@property (nonatomic, strong) NSString *content;

@end
