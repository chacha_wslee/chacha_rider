//
//  MenuViewController.h
//  SlideMenu
//

//

#import <UIKit/UIKit.h>
#import "TXBaseViewController.h"

@interface TXWaybillVC : TXBaseViewController  {

}

@property (strong, nonatomic) IBOutlet UIScrollView *myScrollView;
@property (strong, nonatomic) IBOutlet UIView *vbody;

@property (strong, nonatomic) IBOutlet UILabel *lbBottom;
@property (strong, nonatomic) IBOutlet UIButton *btnBottom;

@end
