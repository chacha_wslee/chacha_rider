//
//  MenuViewController.m
//  SlideMenu
//

//

#import "TXWaybillVC.h"
#import "LGAlertView.h"
#import <GoogleMaps/GoogleMaps.h>

#define LINE_PRINT1(t,c) [NSString stringWithFormat:LocalizedStr(@"Menu.Driver.WayBill.content1"),t,c]
#define LINE_PRINT2(c) [NSString stringWithFormat:LocalizedStr(@"Menu.Driver.WayBill.content2"),c]

@interface TXWaybillVC() {
    NSDictionary *items;
    NSArray *_msgList;
}

@property (strong, nonatomic) IBOutlet RTLabel *lbalert;
@property (strong, nonatomic) IBOutlet UIView *alertView;
@property (strong, nonatomic) IBOutlet UIView *backView;

@end

@implementation TXWaybillVC {
    GMSGeocoder *_geocoder;
    NSDictionary *waybillData;
}


-(void)viewDidLoad {
    
    [super viewDidLoad];
    
    _geocoder = [[GMSGeocoder alloc] init];
    
    UIView *n = [super navigationView];
    [super configureBottomLine];
    UIView *line = (UIView*)[[super navigationView] viewWithTag:1500];
    CGRect rect = line.frame;
    rect.size.height = 3;
    line.frame = rect;
    line.backgroundColor = HEXCOLOR(0x111111FF);
    NSInteger _y = n.frame.origin.y + n.frame.size.height;
    
    CGFloat bottomHeight = self.view.frame.size.height - self.lbBottom.frame.origin.y;
    self.vbody.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - _y - kBasicHeight - bottomHeight);
    
    self.myScrollView.contentSize = CGSizeMake(self.vbody.frame.size.width, self.vbody.frame.size.height);
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0, 0.0, 0, 0.0);
    self.myScrollView.contentInset = contentInsets;
    self.myScrollView.scrollIndicatorInsets = contentInsets;
    
    [self.myScrollView addSubview:self.vbody];
    
    self.vbody.backgroundColor = [UIColor clearColor];
    self.myScrollView.backgroundColor = [UIColor clearColor];
    
    // text
    self.alertView = [Utils addRTLabelView:self.view underView:nil dim:0];
    self.alertView.backgroundColor = [UIColor whiteColor];
    self.lbalert = [self.alertView viewWithTag:11];
    [self.lbalert setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:14.0f]];
    self.lbalert.textColor = [UIColor blackColor];
    self.lbalert.backgroundColor = [UIColor clearColor];
    [self.vbody addSubview:self.alertView];
    
    //
//    self.backView = [Utils addButtonBasic:self.view underView:self.alertView dim:0];
//    rect = self.backView.frame;
//    rect.size.height = 0;
//    self.backView.frame = rect;
//    [self.vbody addSubview:self.backView];
    
    // bottom text
    [self.lbBottom setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:14.0f]];
    self.lbBottom.textColor = HEXCOLOR(0x666666FF);
    self.lbBottom.backgroundColor = [UIColor clearColor];
    
    // bottom button
    [self.btnBottom setTitle:LocalizedStr(@"Menu.Driver.WayBill.BTN.text") forState:UIControlStateNormal];
    [self.btnBottom addTarget:self action:@selector(insuranceButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    self.btnBottom.tag = 999;
    self.btnBottom.backgroundColor = UIColorDefault;
    [self.btnBottom setTitleColor:UIColorButtonText forState:UIControlStateNormal];
    self.btnBottom.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:18.0f];
    self.btnBottom.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self removeEventListeners];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    [self arrangeView];
}

- (void)arrangeView
{
    //_lbalert.frame = CGRectMake(6,3, _alertView.frame.size.width - 12, _alertView.frame.size.height - 6 );
    
    CGSize optimumSize = [self.lbalert optimumSize];
    NSLog(@"optimumSize:%@",NSStringFromCGSize(optimumSize));
    
    
    _lbalert.frame = CGRectMake(kBasicMargin, 0, _alertView.frame.size.width - kBasicMargin*2,optimumSize.height);
    NSLog(@"_lbalert:%@",NSStringFromCGRect(_lbalert.frame));
    
    CGRect rect = _alertView.frame;
    rect.origin.y = kBasicHeight;
    rect.size.height = _lbalert.frame.size.height + kBasicMargin;
    //    rect.origin.y = self.view.frame.size.height - rect.size.height - 6;
    _alertView.frame = rect;
    NSLog(@"_alertView:%@",NSStringFromCGRect(_alertView.frame));
    
    rect = self.vbody.frame;
    rect.size.height = _alertView.frame.origin.y + _alertView.frame.size.height - 10;
    self.vbody.frame = rect;
    NSLog(@"vbody:%@",NSStringFromCGRect(_vbody.frame));
    
    self.myScrollView.contentSize = CGSizeMake(self.vbody.frame.size.width, self.vbody.frame.size.height);
}

#pragma mark - Configure

-(void)configureStyles {
    [super configureStyles];
    
    [super navigationType01X:[UIImage imageNamed:@"btn_back"] centerText:LocalizedStr(@"Menu.Driver.WayBill.title")];
}

-(void)configure {
    [super configure];
    
    // 2. init event
    _msgList = @[
                 @[@"PD304",@""], // waybill
                 ];
    
    [self registerEventListeners];
    
    //propertyMap = [[NSMutableDictionary alloc] init];
    
    [self showBusyIndicator:@"Loading info ... "];
    // 3. call api
    [self->model PD304:@""];
    
}

-(void) registerEventListeners {
    for (NSArray *msg in _msgList) {
        [self->model addEventListener:self forEvent:[msg objectAtIndex:0] eventParams:nil];
    }
}

-(void) removeEventListeners {
    for (NSArray *msg in _msgList) {
        [self->model removeEventListener:self forEvent:[msg objectAtIndex:0]];
    }
}

#pragma mark - Action
-(IBAction)onNaviButtonClick:(id)sender {
    UIButton *btn = (UIButton*)sender;
    NSLog(@"onNaviButtonClick");
    //[super onNaviButtonClick:sender];
    
    if (btn.tag == 1100) {
        [self removeEventListeners];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

-(void)getAddress:(CLLocationCoordinate2D)coor target:(NSString*)target
{
    static NSString *from = @"";
    static NSString *to = @"";
    
    GMSReverseGeocodeCallback handler = ^(GMSReverseGeocodeResponse *response, NSError *error) {
        GMSAddress *address = response.firstResult;
        if (address) {
            NSLog(@"Geocoder result: %@", address);
            
            if ([target isEqualToString:@"from"]) {
                from = [Utils addressToString:address];
            }
            else if ([target isEqualToString:@"to"]) {
                to = [Utils addressToString:address];
            }
            if (![from isEqualToString:@""] && ![to isEqualToString:@""]) {
                [self printWaybill:from to:to];
            }
            
        } else {
        }
    };
    [_geocoder reverseGeocodeCoordinate:coor completionHandler:handler];
}


#pragma mark - IBAction
-(IBAction)insuranceButtonPressed:(id)sender {
    //UIButton *btn = (UIButton*)sender;
#ifndef _CHACHA
    NSCharacterSet *set = [NSCharacterSet URLQueryAllowedCharacterSet];
    NSString* content = [INSURANCE_URL stringByAddingPercentEncodingWithAllowedCharacters:set];
    [[UIApplication sharedApplication] openURL: [NSURL URLWithString:content]];
    //[[UIApplication sharedApplication] openURL: [NSURL URLWithString:INSURANCE_URL]];
#endif
}


#pragma mark - onEvent
-(void)printWaybill:(NSString*)from to:(NSString*)to {
    if ([Utils isDictionary:waybillData]) {
        if ([from isEqualToString:@""]) {
            from = waybillData[@"From"];
        }
        if ([to isEqualToString:@""]) {
            to = waybillData[@"To"];
        }
        /*
        self.lbalert.text = [NSString stringWithFormat:@"<font>%@<br>%@<br>%@<br>%@<br>%@<br>%@<br><br><br>%@<br>%@<br>%@<br>%@<br><br><br>%@<br>%@<br>%@<br>%@<br><br><br>%@<br>%@</font>",
                             LINE_PRINT1(LocalizedStr(@"Menu.Driver.WayBill.Line1.text"),[Utils nameOfuname:waybillData[@"Driver Name"]]),
                             LINE_PRINT1(LocalizedStr(@"Menu.Driver.WayBill.Line2.text"),[Utils nullToStringNoTrim:waybillData[@"Driver License"]]),
                             LINE_PRINT1(LocalizedStr(@"Menu.Driver.WayBill.Line3.text"),[Utils nullToStringNoTrim:waybillData[@"License Plate"]]),
                             LINE_PRINT1(LocalizedStr(@"Menu.Driver.WayBill.Line4.text"),[Utils nullToStringNoTrim:waybillData[@"TCP#"]]),
                             LINE_PRINT1(LocalizedStr(@"Menu.Driver.WayBill.Line5.text"),[Utils nullToStringNoTrim:waybillData[@"Time"]]),
                             LINE_PRINT1(LocalizedStr(@"Menu.Driver.WayBill.Line6.text"),[Utils nullToStringNoTrim:waybillData[@"Rate"]]),
                             
                             LINE_PRINT1(LocalizedStr(@"Menu.Driver.WayBill.Line7.text"),[Utils nullToStringNoTrim:waybillData[@"Passenger"]]),
                             LINE_PRINT1(LocalizedStr(@"Menu.Driver.WayBill.Line8.text"),[Utils nullToStringNoTrim:waybillData[@"Trip#"]]),
                             LINE_PRINT1(LocalizedStr(@"Menu.Driver.WayBill.Line9.text"),[Utils nullToStringNoTrim:waybillData[@"Via"]]),
                             LINE_PRINT1(LocalizedStr(@"Menu.Driver.WayBill.Line10.text"),[Utils nameOfuname:waybillData[@"Passenger Name"]]),
                             
                             LINE_PRINT1(LocalizedStr(@"Menu.Driver.WayBill.Line11.text"),[Utils nullToStringNoTrim:from]),
                             LINE_PRINT1(LocalizedStr(@"Menu.Driver.WayBill.Line12.text"),[Utils nullToStringNoTrim:to]),
                             LINE_PRINT1(LocalizedStr(@"Menu.Driver.WayBill.Line13.text"),[Utils nullToStringNoTrim:waybillData[@"Trip Start Time"]]),
                             LINE_PRINT1(LocalizedStr(@"Menu.Driver.WayBill.Line14.text"),[Utils nullToStringNoTrim:waybillData[@"Certificate of Insurance"]]),
                             
                             LINE_PRINT2([Utils nullToStringNoTrim:waybillData[@"Terms"]]),
                             LINE_PRINT2([Utils nullToStringNoTrim:waybillData[@"MadeBy"]])
                             ];
        */
        self.lbalert.text = [NSString stringWithFormat:@"<font>%@<br>%@<br>%@<br>%@<br>%@<br>%@<br><br><br>%@<br>%@<br>%@<br>%@<br><br><br>%@<br>%@<br>%@<br>%@<br></font>",
                             LINE_PRINT1(LocalizedStr(@"Menu.Driver.WayBill.Line1.text"),[Utils nameOfuname:waybillData[@"Driver Name"]]),
                             LINE_PRINT1(LocalizedStr(@"Menu.Driver.WayBill.Line2.text"),[Utils nullToStringNoTrim:waybillData[@"Driver License"]]),
                             LINE_PRINT1(LocalizedStr(@"Menu.Driver.WayBill.Line3.text"),[Utils nullToStringNoTrim:waybillData[@"License Plate"]]),
                             LINE_PRINT1(LocalizedStr(@"Menu.Driver.WayBill.Line4.text"),[Utils nullToStringNoTrim:waybillData[@"TCP#"]]),
                             LINE_PRINT1(LocalizedStr(@"Menu.Driver.WayBill.Line5.text"),[Utils nullToStringNoTrim:waybillData[@"Time"]]),
                             LINE_PRINT1(LocalizedStr(@"Menu.Driver.WayBill.Line6.text"),[Utils nullToStringNoTrim:waybillData[@"Rate"]]),
                             
                             LINE_PRINT1(LocalizedStr(@"Menu.Driver.WayBill.Line7.text"),[Utils nameOfuname:waybillData[@"Passenger"]]),
                             LINE_PRINT1(LocalizedStr(@"Menu.Driver.WayBill.Line8.text"),[Utils nullToStringNoTrim:waybillData[@"Trip#"]]),
                             LINE_PRINT1(LocalizedStr(@"Menu.Driver.WayBill.Line9.text"),[Utils nullToStringNoTrim:waybillData[@"Via"]]),
                             LINE_PRINT1(LocalizedStr(@"Menu.Driver.WayBill.Line10.text"),[Utils nameOfuname:waybillData[@"Passenger Name"]]),
                             
                             LINE_PRINT1(LocalizedStr(@"Menu.Driver.WayBill.Line11.text"),[Utils nullToStringNoTrim:from]),
                             LINE_PRINT1(LocalizedStr(@"Menu.Driver.WayBill.Line12.text"),[Utils nullToStringNoTrim:to]),
                             LINE_PRINT1(LocalizedStr(@"Menu.Driver.WayBill.Line13.text"),[Utils nullToStringNoTrim:waybillData[@"Trip Start Time"]]),
                             LINE_PRINT1(LocalizedStr(@"Menu.Driver.WayBill.Line14.text"),[Utils nullToStringNoTrim:waybillData[@"Certificate of Insurance"]])
                             ];
        self.lbalert.lineSpacing = -5;
        self.lbBottom.text = [NSString stringWithFormat:@"%@\n%@",
                              [Utils nullToStringNoTrim:waybillData[@"Terms"]],
                              [Utils nullToStringNoTrim:waybillData[@"MadeBy"]]];
        [self.lbBottom sizeToFit];
        [self arrangeView];
    }
}

-(void)onEvent:(TXEvent *)event eventParams:(id)subscriptionParams {
    
    TXResponseDescriptor * descriptor = [event getEventProperty:TXEvents.Params.DESCRIPTOR];
    TXRequestObj *request = [event getEventProperty:TXEvents.Params.REQUEST];
    
    [self hideBusyIndicator];
    if([event.name isEqualToString:@"PD304"]) {
        
        if(descriptor.success == true) {
            NSString     *response = [[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding];
            NSDictionary *result   = getJSONObj(response);
            
            waybillData              = [result valueForKey:@"waybill"];
            
            // 위치
            NSArray* floc = [[Utils nullTolocation:waybillData[@"From"]] componentsSeparatedByString:@","];
            NSArray* tloc = [[Utils nullTolocation:waybillData[@"To"]] componentsSeparatedByString:@","];
            
            if ([floc count] == 2 && [tloc count] == 2) {
                CLLocationCoordinate2D fposition;
                CLLocationCoordinate2D tposition;
                fposition.latitude  = [[NSDecimalNumber decimalNumberWithString:floc[0]] doubleValue];
                fposition.longitude = [[NSDecimalNumber decimalNumberWithString:floc[1]] doubleValue];
                tposition.latitude  = [[NSDecimalNumber decimalNumberWithString:tloc[0]] doubleValue];
                tposition.longitude = [[NSDecimalNumber decimalNumberWithString:tloc[1]] doubleValue];
                
                [self getAddress:fposition target:@"from"];
                [self getAddress:tposition target:@"to"];
            }
            
            [self printWaybill:waybillData[@"From"] to:waybillData[@"To"]];
            
        } else {
        //    [self alertError:@"Error" message:@"Failed to get profile image"];
        }
    }
}


@end
