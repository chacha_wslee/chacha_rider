//
//  TXLauncherVC.h

//
#import "TXUserModel.h"
#import "TXUserVC.h"
#import <GoogleMaps/GoogleMaps.h>


@interface TXAboutVC : TXUserVC<CLLocationManagerDelegate> {
    IBOutlet TXButton       *btnstart1;
    IBOutlet TXButton       *btnstart2;
}

@property (assign, nonatomic) NSInteger step;

@property (retain, nonatomic) IBOutlet UIView    *vBackground;
@property (retain, nonatomic) IBOutlet UILabel    *lbTitle;
@property (retain, nonatomic) IBOutlet UILabel    *lbBody;

@property (retain, nonatomic) IBOutlet UIButton    *btnSite;
@property (retain, nonatomic) IBOutlet UIButton    *btnNext;

//@property (retain, nonatomic) IBOutlet UIButton    *btnstart1;
//@property (retain, nonatomic) IBOutlet UIButton    *btnstart2;

@property (retain, nonatomic) IBOutlet UILabel    *lbCenter;
@property (retain, nonatomic) IBOutlet UILabel    *lbTelno;

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil step:(NSInteger)__step;
@end
