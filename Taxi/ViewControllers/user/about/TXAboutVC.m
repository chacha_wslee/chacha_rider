//
//  TXLauncherVC.m

//
#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import <CoreTelephony/CTCarrier.h>

#import "TXAboutVC.h"
#import "TXMainVC.h"
#import "TXUserModel.h"
#import "TXSharedObj.h"
#import "TXLauncherVC.h"
#import "TXAppDelegate.h"
#import "LGAlertView.h"
#import "UIImageView+AnimationCompletion.h"

#ifdef _CHACHA
#import "TXBecomeVC.h"
#import "TXBecomeVehicleVC.h"
#endif

@interface TXAboutVC () {
    LGAlertView* alertView;
    
}

@property (retain, nonatomic) IBOutlet UIImageView    *splash_background;

@property (retain, nonatomic) IBOutlet UIImageView    *symbolImage;

@end

@implementation TXAboutVC


-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil step:(NSInteger)__step{
    
    if(self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        self.step = __step;
    }
    
    return self;
}

#pragma mark - UIViewController

-(void)configureStyles {
    [super configureStyles];
    [self configureFieldStyles];
}

-(void) configure {

    [super configure];
    
    [self.btnNext setTitle:LocalizedStr(@"About.Next.BTN.text") forState:UIControlStateNormal];
    self.btnNext.backgroundColor = UIColorDefault;
    
    [[btnstart1 titleLabel] setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:17]];
    [[btnstart2 titleLabel] setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:17]];
    
    [btnstart1 setTitle:LocalizedStr(@"Intro.Login.title") forState:UIControlStateNormal];
    [btnstart2 setTitle:LocalizedStr(@"Intro.SignUp.title") forState:UIControlStateNormal];
    
    [btnstart1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnstart1 setBackgroundColor:UIColorDefault];
    [btnstart2 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnstart2 setBackgroundColor:UIColorDefault];

    if (_step == 12) {
        self.symbolImage.hidden = YES;
        
        self.vBackground.backgroundColor = HEXCOLOR(0x0301017f);
        self.vBackground.hidden = NO;
        self.lbTitle.text = LocalizedStr(@"About.Step2.Title.text");
        self.lbBody.text = LocalizedStr(@"About.Step2.Body.text");
        self.btnSite.hidden = YES;
        
        [self.lbBody sizeToFit];
    }
    else if (_step == 13) {
        self.symbolImage.hidden = YES;
        
        self.vBackground.backgroundColor = HEXCOLOR(0x0301017f);
        self.vBackground.hidden = NO;
        self.lbTitle.text = LocalizedStr(@"About.Step3.Title.text");
        self.lbBody.text = LocalizedStr(@"About.Step3.Body.text");
        [self.btnSite setTitle:LocalizedStr(@"About.Step3.Site.BTN.text") forState:UIControlStateNormal];
        self.btnSite.layer.cornerRadius = 3;
        
        CGRect frame = self.btnSite.frame;
        frame.origin.y = self.lbBody.frame.origin.y + self.lbBody.frame.size.height + 6;
        self.btnSite.frame = frame;
        
        [self.lbBody sizeToFit];
#ifndef _CHACHA
        //[[NSUserDefaults standardUserDefaults] setBool:YES forKey:FIRST_START];
#endif
    }
    else if (_step == 0) {
        [Utils setBCDSTEP:@"0"];

        self.symbolImage.hidden = YES;
        
        self.vBackground.backgroundColor = HEXCOLOR(0x0301017f);
        self.vBackground.hidden = NO;
        self.lbTitle.text = LocalizedStr(@"Menu.BecomeADriver.First.title");
        self.lbBody.text = LocalizedStr(@"Menu.BecomeADriver.First.text");
        [self.btnSite setTitle:LocalizedStr(@"Menu.BecomeADriver.First.Site.text") forState:UIControlStateNormal];
        self.btnSite.layer.cornerRadius = 3;
        [self.btnNext setTitle:LocalizedStr(@"Menu.BecomeADriver.First.BTN.text") forState:UIControlStateNormal];
        
        [self.btnSite addTarget:self action:@selector(onCall:) forControlEvents:UIControlEventTouchUpInside];
        
        [self.lbBody sizeToFit];
#ifndef _CHACHA
        //[[NSUserDefaults standardUserDefaults] setBool:YES forKey:FIRST_START];
#endif
    }
    else if (_step == 3) {
        [Utils setBCDSTEP:@"3"];
        
        btnstart1.hidden = YES;
        btnstart2.hidden = YES;
        
        self.symbolImage.hidden = YES;
        
        self.vBackground.backgroundColor = HEXCOLOR(0x0301017f);
        self.vBackground.hidden = NO;
        self.lbTitle.text = LocalizedStr(@"Menu.BecomeADriver.Wait.title");
        self.lbBody.text = LocalizedStr(@"Menu.BecomeADriver.Wait.text");
        self.lbCenter.text = LocalizedStr(@"Menu.BecomeADriver.Wait.CallCenter.text");
        
        self.btnSite.hidden = NO;
        [self.btnSite setTitle:LocalizedStr(@"Menu.BecomeADriver.Wait.CallCenter.tel") forState:UIControlStateNormal];
        [self.btnSite setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        self.btnSite.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        self.btnSite.backgroundColor = [UIColor clearColor];
        CGRect frame = self.btnSite.frame;
        frame.origin.y = self.lbCenter.frame.origin.y + self.lbCenter.frame.size.height + 6;
        self.btnSite.frame = frame;
        self.btnSite.titleLabel.font = [UIFont fontWithName:@"AppleSDGothicNeo-Bold" size:30];
        [self.btnSite setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        //[self.btnSite addTarget:self action:@selector(onCall:) forControlEvents:UIControlEventTouchUpInside];
        
        [self.btnNext setTitle:LocalizedStr(@"Menu.BecomeADriver.Wait.BTN.text") forState:UIControlStateNormal];
        [self.btnNext addTarget:self action:@selector(onBackClick) forControlEvents:UIControlEventTouchUpInside];
        
        [self.lbBody sizeToFit];
    }
    else {
        self.vBackground.hidden = YES;
        self.symbolImage.hidden = NO;
    }

#ifdef _DRIVER_MODE
    [self.splash_background setImage:[UIImage imageNamed:@"spalsh_background_driver"]];
#else
    [self.splash_background setImage:[UIImage imageNamed:@"spalsh_background"]];
#endif

}

- (void)viewDidLoad
{
    [super viewDidLoad];

}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self configureFieldStyles];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    //[super statusBarStyleLauncher];
    
    //[self removeFromParentViewController];
    if (_step > 10) {
        [super statusBarStyleLauncher];
        [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
    }
}

-(void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

-(BOOL)prefersStatusBarHidden{
    return YES;
}

-(void) configureFieldStyles {
    
    UIColor *bgColor = [UIColor blackColor];
    
    self.view.backgroundColor = bgColor;
}

#pragma mark - IBAction

-(NSString*)canTelephony {
    
    NSString* acall = LocalizedStr(@"Menu.BecomeADriver.Wait.CallCenter.tel");
    
    if ([[UIDevice currentDevice].model isEqualToString:@"iPad"]) {
        acall = @"";
    }
    
    if (![acall isEqualToString:@""] && acall != nil) {
        
        // Check if the device can place a phone call
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"tel://"]]) {
            // Device supports phone calls, lets confirm it can place one right now
            CTTelephonyNetworkInfo *netInfo = [[CTTelephonyNetworkInfo alloc] init];
            CTCarrier *carrier = [netInfo subscriberCellularProvider];
            NSString *mnc = [carrier mobileNetworkCode];
            NSLog(@"mnc:%@",mnc);
            if (([mnc length] == 0) || ([mnc isEqualToString:@"65535"]) || mnc == nil || [mnc isEqualToString:@""]) {
                // Device cannot place a call at this time.  SIM might be removed.
                //isSimCardAvailable = NO;
                return @"";
            } else {
                // Device can place a phone call
                //isSimCardAvailable = YES;
                return acall;
            }
        } else {
            // Device does not support phone calls
            //isSimCardAvailable =  NO;
            return @"";
        }
    }
    return acall;
}

-(IBAction)onCall:(id)sender {
    
    NSString* acall = [self canTelephony];
    
    if (![acall isEqualToString:@""] && acall != nil) {
        //        NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt://%@",acall]];
        //        if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        //            [[UIApplication sharedApplication] openURL:phoneUrl];
        //        }
        //
        NSURL *phoneUrl = [NSURL URLWithString:[@"telprompt://" stringByAppendingString:acall]];
        NSURL *phoneFallbackUrl = [NSURL URLWithString:[@"tel://" stringByAppendingString:acall]];
        
        if ([UIApplication.sharedApplication canOpenURL:phoneUrl]) {
            [UIApplication.sharedApplication openURL:phoneUrl];
        } else if ([UIApplication.sharedApplication canOpenURL:phoneFallbackUrl]) {
            [UIApplication.sharedApplication openURL:phoneFallbackUrl];
        } else
        {
            [self alertError:@"Error" message:@"Call facility is not available!!!"];
        }
    }
    else
        [self alertError:@"Error" message:@"Call facility is not available!!!"];
}

-(IBAction)onSite:(id)sender {
    if (_step == 3) {
        [self onCall:sender];
    }
    else {
        NSCharacterSet *set = [NSCharacterSet URLQueryAllowedCharacterSet];
        NSString* content = [RENTAL_INFO_URL stringByAddingPercentEncodingWithAllowedCharacters:set];
        [[UIApplication sharedApplication] openURL: [NSURL URLWithString:content]];
        //[[UIApplication sharedApplication] openURL: [NSURL URLWithString:RENTAL_INFO_URL]];
    }
}

- (IBAction) onClick: (id) sender {
    
    UIButton *btn = (UIButton*)sender;
    if (btn.tag == 10) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else if (btn.tag == 11) {
        // login
        // send notification
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else if (btn.tag == 12) {
        // signup
        // send notification
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_SIGNUP object:nil userInfo:nil];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

-(IBAction)onBackClick
{
    [self.navigationController popViewControllerAnimated:YES];
}

// about : 10 11 12 -> main(13)
// application : 0(운전자가 되려면) 1-TXBecomeVC(profile) 2-TXBecomeVehicleVC(vehicle) 3(신청이 완료되었습니다)
-(IBAction)onNext:(id)sender {
    
    // become a driver
    if (_step > 2 && _step < 10) {
        
//        if (_step == 0) {
//            [Utils setBCDSTEP:@"1"];
//        }
//        else if (_step == 1) {
//            [Utils setBCDSTEP:@"2"];
//        }
//        else
        if (_step == 2) {
            [Utils setBCDSTEP:@"3"];
        }
        
        //[self onBackClick];
        [self.navigationController popToRootViewControllerAnimated:YES];
        return;
    }
    else if (_step < 3) {
        
        if (_step == 0) {
            [Utils setBCDSTEP:@"1"];
            TXBecomeVC *mvc = [[TXBecomeVC alloc] initWithNibName:@"TXBecomeVC" bundle:nil];
            [self pushViewController:(TXBaseViewController *)mvc];
        }
        else if (_step == 1) {
            [Utils setBCDSTEP:@"2"];
            TXBecomeVehicleVC *mvc = [[TXBecomeVehicleVC alloc] initWithNibName:@"TXBecomeVehicleVC" bundle:[NSBundle mainBundle] isnew:NO];
            [self pushViewController:(TXBaseViewController *)mvc];
        }
    }
    // intro guide
    else if (_step < 13 && _step > 10) {
        TXAboutVC *aboutVC = [[TXAboutVC alloc] initWithNibName:@"TXAboutVC" bundle:nil step:(_step+1)];
        aboutVC.view.frame = [super view].frame;
        [self pushViewController:aboutVC];
    }
    else {
        
        
        NSString *userToken = [[[TXApp instance] getSettings] getUserToken];
        NSString *userUid   = [[[TXApp instance] getSettings] getUserId];
        
        TXLauncherVC *firstVC;
        
        if((![userToken isEqual:[NSNull null]] && [userToken length] > 0) &&
           (![userUid isEqual:[NSNull null]] && [userUid length] > 0) ) {
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
            //firstVC = [[TXLauncherVC alloc] initWithFlag:FALSE];
#if defined (_CHACHA) && defined (_DRIVER_MODE)
            //firstVC = [[TXLauncherVC alloc] initWithNibName:@"TXLauncherDriverVC" bundle:nil flag:NO];
            firstVC = [[TXLauncherVC alloc] initWithNibName:@"TXLauncherVC" bundle:nil flag:NO];
#else
            firstVC = [[TXLauncherVC alloc] initWithNibName:@"TXLauncherVC" bundle:nil flag:NO];
#endif

        } else {
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
            // 첫화면 로딩

#if defined (_CHACHA) && defined (_DRIVER_MODE)
            //firstVC = [[TXLauncherVC alloc] initWithNibName:@"TXLauncherDriverVC" bundle:nil];
            firstVC = [[TXLauncherVC alloc] initWithNibName:@"TXLauncherVC" bundle:nil];
#else
            firstVC = [[TXLauncherVC alloc] initWithNibName:@"TXLauncherVC" bundle:nil];
#endif

        }
        
        
//        aboutVC.view.frame = [super view].frame;
        [self pushViewController:firstVC];
    }
    
    double delayInSeconds = 0.5;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0),
                   ^(void){
                       [self removeFromParentViewController];
                   });
}

// event
-(void)onEvent:(TXEvent *)event eventParams:(id)subscriptionParams {
    
    
}

@end
