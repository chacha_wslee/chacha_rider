//
//  TXPaymentCell.m
//  TXPaymentCell
//
//  Created by __MyCompanyName__ on 14. 11. 18..
//  Copyright (c) 2014년 __MyCompanyName__. All rights reserved.
//

#import "TXAppDelegate.h"
#import "TXTripHistoryDetailCell.h"
#import "utils.h"
#import "utilities.h"

@implementation TXTripHistoryDetailCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
    }
    return self;
}
- (NSDictionary*)bindData:(NSDictionary *)dic type:(BOOL)isShare
{
    TXAppDelegate *appDelegate = (TXAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    BOOL isFinish = NO;
    NSDictionary* drive_charge  = nil;
    NSDictionary* drive_trip    = nil;
    NSDictionary* drive_tip     = nil;
    NSDictionary* drive_pickup  = nil;
    NSDictionary* pvehicle      = nil;
    NSDictionary* user          = nil;
    NSDictionary* drive_payout  = nil;
    NSDictionary *pMap          = nil;
    
    //    self.lb1.textColor = HEXCOLOR(0x999999FF);
    //    self.lb1.textColor = HEXCOLOR(0x999999FF);
    //    self.lb1.textColor = HEXCOLOR(0x999999FF);
    //    self.lb1.textColor = HEXCOLOR(0x999999FF);
    
    //self.lb6.lineBreakMode = kCTLineBreakByClipping;
    //self.lb6.numberOfLines = 1;
    //self.lb7.lineBreakMode = kCTLineBreakByClipping;
    //self.lb7.numberOfLines = 1;
    
    [self.btnHelp setTitle:LocalizedStr(@"Menu.TripHistory.Detail.BTN.help.text") forState:UIControlStateNormal];
    // driver
    if (appDelegate.isDriverMode) {
        drive_tip     = [dic valueForKey:@"drive_tip"]; //tip_payed
        drive_charge  = [dic valueForKey:@"drive_charge"]; //fare_rate, pay_fare, pay_tip, fare_extra
        drive_trip    = [dic valueForKey:@"drive_trip"]; //trip_duration/trip_distance, begin_location, end_location, update_date
        drive_pickup  = [dic valueForKey:@"drive_pickup"];
        //provider      = [dic valueForKey:@"provider"]; // name, picture, rate, trips
        pvehicle      = [dic valueForKey:@"drive_driver"]; // model, plateno
        user          = [dic valueForKey:@"drive_rider"]; // name, picture, rate, trips
        drive_payout  = [dic valueForKey:@"drive_payout"]; // total_payout
        
        pMap = @{
                 @"useq" : [user valueForKey:@"useq"],
                 @"picture" : [Utils nullToIntString:user[@"picture"]],
                 };
        // 취소요금0일때는 요금보기 버튼 감추기
        _btnCharge.hidden = YES;
    }
    else {
        drive_tip     = [dic valueForKey:@"drive_tip"]; //tip_payed
        drive_charge  = [dic valueForKey:@"drive_charge"]; //fare_rate, pay_fare, pay_tip, fare_extra
        drive_trip    = [dic valueForKey:@"drive_trip"]; //trip_duration/trip_distance, begin_location, end_location, update_date
        drive_pickup  = [dic valueForKey:@"drive_pickup"];
        user          = [dic valueForKey:@"drive_driver"]; // name, picture, rate, trips
        pvehicle      = [dic valueForKey:@"drive_driver"]; // model, plateno
        //user          = [dic valueForKey:@"user"]; // name, picture, rate, trips
        
        pMap = @{
                 @"pseq" : [user valueForKey:@"pseq"],
                 @"picture" : [Utils nullToIntString:user[@"picture"]],
                 };
        _btnCharge.hidden = NO;
    }
    
    NSString *lb1 = @"";
    NSString *lb2 = @"";
    NSString *lb4 = @"";
    //NSString *lb6 = @"";
    NSString *lb10 = @"";
    
    NSString *tip = @"";
    NSString *payed = @""; // 과금된 금액 운전자/승객이 보는 금액은 서로 다르다.
    BOOL isCancelPay = NO; // 취소수수료 발생여부
    
    // 과금여부
    if ([Utils isDictionary:drive_charge]) {
        //payed = (appDelegate.isDriverMode?NUMBER_FORMAT([drive_charge valueForKey:@"fare_trip"]):NUMBER_FORMAT([drive_charge valueForKey:@"fare_payed"]));
        if (appDelegate.isDriverMode) {
            if ([Utils isDictionary:drive_payout]) {
                payed = NUMBER_FORMAT([drive_payout valueForKey:@"total_payout"]);
            }
            else {
                payed = NUMBER_FORMAT(0);
            }
        }
        else {
            payed = NUMBER_FORMAT([drive_charge valueForKey:@"fare_payed"]);
        }
        
        if (![payed intValue]) {
            payed = @"";
        }
        // 추가요금(드라이버)
        if (appDelegate.isDriverMode) {
            tip = NUMBER_FORMAT([drive_charge valueForKey:@"fare_extra"]);
            if (![tip intValue]) {
                tip = @"";
            }
            else {
                tip = [NSString stringWithFormat:@"%d", [tip intValue]];
            }
        }
        //        tip = [drive_charge valueForKey:@"tip_payed"];
        //        if (![tip intValue]) {
        //            tip = @"";
        //        }
        //        else {
        //            tip = [NSString stringWithFormat:@"%d", [tip intValue]];
        //        }
        
        if ([[drive_charge valueForKey:@"fare_discount"] intValue]>0) {
            lb4 = [NSString stringWithFormat:@"%@",LocalizedStr(@"Menu.TripHistory.Trip.Discount")];
            //[self.lb4 setTextColor:HEXCOLOR(0xd0021bFF)]; // 붉
            [self.lb4 setTextColor:UIColorDefault]; // 기본
        }
    }
    payed = [Utils numberCheck:payed];
    // 팁(라이더)
    if ([Utils isDictionary:drive_tip] && !appDelegate.isDriverMode) {
        tip = NUMBER_FORMAT([drive_tip valueForKey:@"tip_payed"]);
        if (![tip intValue]) {
            tip = @"";
        }
        else {
            tip = [NSString stringWithFormat:@"%d", [tip intValue]];
        }
    }
    tip = [Utils numberCheck:tip];
    NSTimeInterval helpTime = 0;
    
    // pickup 취소
    if ([Utils isDictionary:drive_pickup]) {
        lb1 = [NSString stringWithFormat:@"%@",[drive_pickup valueForKey:@"reg_date"]];
        // 취소
        if ([drive_pickup[@"state"] intValue]<0) {
            isCancelPay = YES;
        }
        else {
            isCancelPay = NO;
        }
    }
    
    // trip 취소
    if ([Utils isDictionary:drive_trip]) {
        lb1 = [NSString stringWithFormat:@"%@",[drive_trip valueForKey:@"reg_date"]];
        // 취소
        if ([drive_trip[@"state"] intValue]<0) {
            isCancelPay = YES;
        }
        else {
            isCancelPay = NO;
        }
    }
    
    if (![lb1 isEqualToString:@""]) {
        helpTime = [[NSDate date] timeIntervalSinceDate:StringFormat2Date(lb1)];
        helpTime = helpTime/60/60/24;
    }
    
    // reg_date 보고 48시간 지나면 해당버튼 안보여주도록 보완하기
    if (helpTime>2) {
        self.btnHelp.hidden = YES;
    }
    
    if (![payed isEqualToString:@""] || [Utils isDictionary:drive_charge]){
        if (isShare) { // 공유는 금액이 나오지 않는다.
            lb2 = [NSString stringWithFormat:@"%d%@/%d%@",
                   [[drive_trip valueForKey:@"trip_duration"] intValue] + 1,
                   LocalizedStr(@"String.Minute"),
                   [[drive_trip valueForKey:@"trip_distance"] intValue],
                   LocalizedStr(@"String.Killo")
                   ];
        }
        else {
            lb2 = [NSString stringWithFormat:@"%d%@/%d%@",
                   [[drive_trip valueForKey:@"trip_duration"] intValue] + 1,
                   LocalizedStr(@"String.Minute"),
                   [[drive_trip valueForKey:@"trip_distance"] intValue],
                   LocalizedStr(@"String.Killo")
                   ];
            lb10 = CURRENCY_FORMAT(payed);
        }
        isFinish = YES;
        
    }
    else {
        if (isCancelPay) {
            if (isShare) { // 공유는 금액이 나오지 않는다.
                lb4 = [NSString stringWithFormat:@"%@",
                       LocalizedStr(@"Menu.ShareTripHistory.Trip.Cancel")];
                [self.lb4 setTextColor:HEXCOLOR(0xd0021bFF)]; // 붉
                //[self.lb4 setTextColor:HEXCOLOR(0xd0021bFF)]; // 기본
            }
            else {
                lb4 = [NSString stringWithFormat:@"%@",
                       LocalizedStr(@"Menu.TripHistory.Trip.Cancel")
                       ];
                lb10 = CURRENCY_FORMAT(@"0");
                [self.lb4 setTextColor:HEXCOLOR(0xd0021bFF)]; // 붉
                //[self.lb4 setTextColor:HEXCOLOR(0xd0021bFF)]; // 기본
                // 취소요금0일때는 요금보기 버튼 감추기
                _btnCharge.hidden = YES;
            }
            isFinish = YES;
            
        }
        else {
            if (isShare) { // 공유는 금액이 나오지 않는다.
                if ([Utils isDictionary:drive_trip]) {
                    if ([drive_trip[@"state"] isEqualToString:@"10"]) {
                        lb4 = [NSString stringWithFormat:@"%@",
                               LocalizedStr(@"Menu.TripHistory.Trip.On")];
                        //[self.lb4 setTextColor:HEXCOLOR(0xd0021bFF)]; // 붉
                        [self.lb4 setTextColor:UIColorDefault]; // 기본
                    }
                }
                else {
                    if ([drive_pickup[@"state"] isEqualToString:@"10"]) {
                        lb4 = [NSString stringWithFormat:@"%@",
                               LocalizedStr(@"Menu.TripHistory.Pickup.On")];
                        //[self.lb4 setTextColor:HEXCOLOR(0xd0021bFF)]; // 붉
                        [self.lb4 setTextColor:UIColorDefault]; // 기본
                    }
                    else {
                        lb4 = [NSString stringWithFormat:@"%@",
                               LocalizedStr(@"Menu.TripHistory.Arrived.On")];
                        //[self.lb4 setTextColor:HEXCOLOR(0xd0021bFF)]; // 붉
                        [self.lb4 setTextColor:UIColorDefault]; // 기본
                    }
                }
            }
            else {
                if ([Utils isDictionary:drive_trip]) {
                    if ([drive_trip[@"state"] isEqualToString:@"10"]) {
                        lb4 = [NSString stringWithFormat:@"%@",
                               LocalizedStr(@"Menu.TripHistory.Trip.On")
                               ];
                        lb10 = CURRENCY_FORMAT(@"0");
                        //[self.lb4 setTextColor:HEXCOLOR(0xd0021bFF)]; // 붉
                        [self.lb4 setTextColor:UIColorDefault]; // 기본
                        // 취소요금0일때는 요금보기 버튼 감추기
                        _btnCharge.hidden = YES;
                    }
                }
                else {
                    if ([drive_pickup[@"state"] isEqualToString:@"10"]) {
                        lb4 = [NSString stringWithFormat:@"%@",
                               LocalizedStr(@"Menu.TripHistory.Pickup.On")
                               ];
                        lb10 = CURRENCY_FORMAT(@"0");
                        //[self.lb4 setTextColor:HEXCOLOR(0xd0021bFF)]; // 붉
                        [self.lb4 setTextColor:UIColorDefault]; // 기본
                        // 취소요금0일때는 요금보기 버튼 감추기
                        _btnCharge.hidden = YES;
                    }
                    else {
                        lb4 = [NSString stringWithFormat:@"%@",
                               LocalizedStr(@"Menu.TripHistory.Arrived.On")
                               ];
                        lb10 = CURRENCY_FORMAT(@"0");
                        //[self.lb4 setTextColor:HEXCOLOR(0xd0021bFF)]; // 붉
                        [self.lb4 setTextColor:UIColorDefault]; // 기본
                        // 취소요금0일때는 요금보기 버튼 감추기
                        _btnCharge.hidden = YES;
                    }
                }
            }
            
        }
    }
    
    NSString* _name = [Utils nameToString:[user objectForKey:@"name"]];
    //    NSString *lb3 = [NSString stringWithFormat:@"%@ %@",
    //                     _name,
    //                     FONT_STR_COLOR([Utils labelStringUserInfo:[user valueForKey:@"trips"] rate:[user valueForKey:@"rate"]])
    //                     ];
    NSString *lb3 = [NSString stringWithFormat:@"%@ %@",
                     _name,
                     @""
                     ];
    
    
    if (![payed isEqualToString:@""] || [Utils isDictionary:drive_charge]){
        if (isCancelPay) {
            lb4 = [NSString stringWithFormat:@"%@",LocalizedStr(@"Menu.TripHistory.Trip.Cancel.Fee")];
            //[self.lb4 setTextColor:HEXCOLOR(0xd0021bFF)]; // 붉
            [self.lb4 setTextColor:UIColorDefault]; // 기본
        }
        else {
            if (![tip isEqualToString:@""]){
                /*
                 if (appDelegate.isDriverMode) {
                 lb4 = [NSString stringWithFormat:@"EXTRA %@",CURRENCY_FORMAT(tip)];
                 }
                 else {
                 lb4 = [NSString stringWithFormat:@"TIP %@",CURRENCY_FORMAT(tip)];
                 }
                 */
            }
        }
    }
    
    NSString *lb5 = [NSString stringWithFormat:@"%@ %@",
                     [pvehicle valueForKey:@"vehiclemodel"],
                     [pvehicle valueForKey:@"vehicleno"]];
    
    if (appDelegate.isDriverMode) {
        lb5  = @"";
    }
    
    if (isShare) {
        lb4 = @"";
    }
    
    
    self.lb1.text = [Utils nullToStringNoTrim:lb1];
    self.lb2.text = [Utils nullToStringNoTrim:lb2];
    self.lb4.text = [Utils nullToStringNoTrim:lb4];
    self.lb10.text = [Utils nullToStringNoTrim:lb10];
    
    self.lb3.text = [Utils nullToStringNoTrim:lb3];
    self.lb5.text = [Utils nullToStringNoTrim:lb5];
    
    [self.lb1 setTextColor:HEXCOLOR(0x4a4a4aFF)];
    [self.lb2 setTextColor:HEXCOLOR(0x4a4a4aFF)];
    [self.lb10 setTextColor:HEXCOLOR(0x111111FF)];
    
    [self.lb3 setTextColor:HEXCOLOR(0x333333FF)];
    [self.lb8 setTextColor:UIColorDefault];
    [self.lb5 setTextColor:HEXCOLOR(0x4a4a4aFF)];
    
    //[self.lb4 setTextColor:HEXCOLOR(0xd0021bFF)]; // 붉
    //[self.lb4 setTextColor:HEXCOLOR(0xd0021bFF)]; // 기본

    self.lb1.font = [UIFont fontWithName:@"AppleSDGothicNeo-Regular" size:12];
    self.lb2.font = [UIFont fontWithName:@"AppleSDGothicNeo-Medium" size:12];
    self.lb4.font = [UIFont fontWithName:@"AppleSDGothicNeo-Medium" size:12];
    self.lb10.font = [UIFont fontWithName:@"AppleSDGothicNeo-Medium" size:16];
    
    self.lb6.font = [UIFont fontWithName:@"AppleSDGothicNeo-Medium" size:12];
    self.lb7.font = [UIFont fontWithName:@"AppleSDGothicNeo-Medium" size:12];
    
    self.lb3.font = [UIFont fontWithName:@"AppleSDGothicNeo-Medium" size:16];
    self.lb8.font = [UIFont fontWithName:@"AppleSDGothicNeo-Regular" size:12];
    self.lb9.font = [UIFont fontWithName:@"AppleSDGothicNeo-Regular" size:12];
    self.lb5.font = [UIFont fontWithName:@"AppleSDGothicNeo-Regular" size:12];
    
    self.lb1.textAlignment = NSTextAlignmentRight;
    self.lb2.textAlignment = NSTextAlignmentRight;
    self.lb4.textAlignment = NSTextAlignmentRight;
    self.lb10.textAlignment = NSTextAlignmentRight;
    
    self.lb6.textAlignment = NSTextAlignmentLeft;
    self.lb7.textAlignment = NSTextAlignmentLeft;
    
    self.lb3.textAlignment = NSTextAlignmentLeft;
    self.lb8.textAlignment = NSTextAlignmentLeft;
    self.lb9.textAlignment = NSTextAlignmentLeft;
    self.lb5.textAlignment = NSTextAlignmentLeft;
    
    [self.lb6 setTextColor:HEXCOLOR(0x030303FF)];
    [self.lb7 setTextColor:UIColorDefault];
    
    self.lb1.backgroundColor = [UIColor clearColor];
    self.lb2.backgroundColor = [UIColor clearColor];
    self.lb4.backgroundColor = [UIColor clearColor];
    self.lb6.backgroundColor = [UIColor clearColor];
    self.lb7.backgroundColor = [UIColor clearColor];
    self.lb3.backgroundColor = [UIColor clearColor];
    self.lb8.backgroundColor = [UIColor clearColor];
    self.lb5.backgroundColor = [UIColor clearColor];
    self.lb8.text = @"";
    self.lb9.text = @"";
    self.lb9.tag = 0;
    self.lb10.backgroundColor = [UIColor clearColor];
    
    [self.lb9 setTextColor:self.lb8.textColor];
    self.lb9.font = self.lb8.font;
    self.lb9.textAlignment = self.lb8.textAlignment;
    self.lb9.backgroundColor = self.lb8.backgroundColor;
    self.lb8.hidden = YES;
    
    if (_btnCharge.hidden) {
        //
        CGRect rect = self.lb4.frame;
        rect.size.width = self.lb1.frame.size.width;
        self.lb4.frame = rect;
        
        rect = self.lb10.frame;
        rect.size.width = self.lb1.frame.size.width;
        self.lb10.frame = rect;
    }
    else {
        
    }
    
    if (!isShare) {
        NSString *text = [NSString stringWithFormat:@" %@", LocalizedStr(@"Button.CALL")];
        //[self.btnCall setTitleColor:UIColorButtonText forState:UIControlStateNormal];
        self.btnCall.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:18];
        [self.btnCall setTitle:text forState:UIControlStateNormal];
        
        text = [NSString stringWithFormat:@" %@", LocalizedStr(@"Button.CHAT")];
        //[self.btnChat setTitleColor:UIColorButtonText forState:UIControlStateNormal];
        self.btnChat.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:18];
        [self.btnChat setTitle:text forState:UIControlStateNormal];
        self.btnChat.tag = 20;
    }
    
    //self.lb3.lineSpacing = 20.0;
    return pMap;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

-(void)stateChanged
{
    
}

@end
