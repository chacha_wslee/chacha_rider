//
//  MenuViewController.h
//  SlideMenu
//

//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import "TXBaseViewController.h"

@interface TXTripDetailVC : TXBaseViewController <UITableViewDelegate, UITableViewDataSource> {
    NSDictionary *tripData;
    
}
@property (nonatomic, strong) NSDictionary *tripData;

@property (nonatomic, strong) NSString *cellIdentifier;
@property (nonatomic, strong) IBOutlet UITableView *tableView;

@end
