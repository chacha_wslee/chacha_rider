//
//  MenuViewController.m
//  SlideMenu
//

//
#import "UITableView+UITableViewAddition.h"
#import "TXTripHistoryVC.h"
#import "TXTripDetailVC.h"
#import "RTLabel.h"
#import "TXTripHistoryCell.h"
#import "LGAlertView.h"
#import "ChatView.h"
#import "UIButton+Badge.h"

#ifndef _DRIVER_MODE
#import "TXChargeViewVC.h"
#endif

@interface TXTripHistoryVC() {
    NSMutableArray *items;
    NSArray *_msgList;
    
    NSInteger selectIdx;
}

@end

@implementation TXTripHistoryVC {
    NSMutableDictionary *propertyMap;
}
@synthesize cellIdentifier;


-(void)viewDidLoad {
    
    [super viewDidLoad];
    
    selectIdx = -1;
    
    //NSString *_name = [Utils getUserName];
    
    [super configureBottomList];
    UILabel *label = (UILabel*)[[super navigationView] viewWithTag:1111];
    label.tag = 2001;
    label.text = [NSString stringWithFormat:@"0 %@",LocalizedStr(@"Menu.TripHistory.List1.title")];

    UIView *n = [super navigationView];
    NSInteger _y = n.frame.origin.y + n.frame.size.height;
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, _y, self.view.bounds.size.width, self.view.bounds.size.height-_y)
                                                  style:UITableViewStylePlain];
    self.tableView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:self.tableView];
    
    UINib *nib = [UINib nibWithNibName:@"TXTripHistoryCell" bundle:nil];
    self.cellIdentifier = @"TXTripHistoryCell";
    [self.tableView registerNib:nib forCellReuseIdentifier:self.cellIdentifier];
    
    self.tableView.separatorColor = [UIColor clearColor];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.opaque = NO;
    self.tableView.backgroundColor = [UIColor clearColor];
//    self.tableView.tableHeaderView = ({
//        
//        UIView *view = [Utils addRTLabelView:self.view underView:nil dim:0];
//        
//        RTLabel *label = [view viewWithTag:11];
//        NSString *htmlString = [NSString stringWithFormat:@"%@ %@",
//                                FONT_STR_COLOR(@"0"),
//                                FONT_STR_TEXT(LocalizedStr(@"Menu.TripHistory.List1.title"))];
//        
//        label.text = htmlString;
//        [label setFont:[UIFont systemFontOfSize:11.0f]];
//        //label.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
//        label.tag = 2001;
//        [Utils RTLabelReSize:view tag:label.tag];
//        
////        label.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
//        
//        view;
//    });
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    self.tableView.scrollsToTop = YES;
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if (selectIdx>-1) {
        //
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:selectIdx inSection:0];
        TXTripHistoryCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
        //[self updateRightBadge:cell.btnProfile];
        [Utils newBadge:cell.btnProfile show:NO];
        
        selectIdx = -1;
    }

}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self removeEventListeners];
}

#pragma mark - Configure

-(void)configureStyles {
    [super configureStyles];
    
    if (appDelegate.isDriverMode) {
        [super navigationType01X:[UIImage imageNamed:@"btn_back"] centerText:LocalizedStr(@"Menu.TripHistory.Driver.title")];
    }
    else {
        [super navigationType01X:[UIImage imageNamed:@"btn_back"] centerText:LocalizedStr(@"Menu.TripHistory.title")];
    }
}

-(void)configure {
    [super configure];
    
    // 2. init event
    _msgList = @[
                 @[@"UR301",@""], // rider
                 @[@"PP301",@""], // driver
                 ];
    
    [self registerEventListeners];
    
    propertyMap = [[NSMutableDictionary alloc] init];
    
    if (appDelegate.isDriverMode) {
        [self configureDriver];
    }
    else {
        [self configureRider];
    }
    
}

-(void) configureDriver {
    // 3. call api
    [self showBusyIndicator:@"Loading info ... "];
    [self->model PP301:@""];
}

-(void) configureRider {
    // 3. call api
    [self showBusyIndicator:@"Loading info ... "];
    [self->model UR301:@""];
}

-(void) registerEventListeners {
    for (NSArray *msg in _msgList) {
        [self->model addEventListener:self forEvent:[msg objectAtIndex:0] eventParams:nil];
    }
}

-(void) removeEventListeners {
    for (NSArray *msg in _msgList) {
        [self->model removeEventListener:self forEvent:[msg objectAtIndex:0]];
    }
}

#pragma mark - openChat
-(IBAction)openChatView:(id)sender
{
    UIButton *btn = (UIButton*)sender;
    selectIdx = btn.tag;
    NSDictionary *dict = self->items[btn.tag];
    NSDictionary *pdevice = nil;
    
    NSString *groupId = @"1";
    NSString *chatUserName = @"";
    NSString *srcName = @"";
    NSString *dstName = @"";
    NSString *srcId = @"";
    NSString *dstId = @"";
    
    NSString* useq = @"";
    NSString* pseq = @"";
    NSString* oseq = @"";
    NSString *pName = @"";
    NSString *uName = @"";
    NSString *pID = @"";
    NSString *uID = @"";
    
    pdevice = [dict valueForKey:@"drive_driver"];
    if ([Utils isDictionary:pdevice]) {
        pseq = [pdevice valueForKey:SettingsConst.CryptoKeys.PSEQ];
        pName = [Utils nameOfuname:[pdevice objectForKey:@"name"]];
        pID = [NSString stringWithFormat:@"pseq,%@",pdevice[@"pseq"]];
    }
    pdevice = [dict valueForKey:@"drive_rider"];
    if ([Utils isDictionary:pdevice]) {
        useq = [pdevice valueForKey:SettingsConst.CryptoKeys.USEQ];
        uName = [Utils nameOfuname:[pdevice objectForKey:@"name"]];
        uID = [NSString stringWithFormat:@"useq,%@",pdevice[@"useq"]];
    }
    
    pdevice = [dict valueForKey:@"drive_order"];
    if ([Utils isDictionary:pdevice]) {
        oseq = [pdevice valueForKey:SettingsConst.CryptoKeys.OSEQ];
    }
    pdevice = [dict valueForKey:@"drive_pickup"];
    if ([Utils isDictionary:pdevice]) {
        oseq = [pdevice valueForKey:SettingsConst.CryptoKeys.OSEQ];
    }
    pdevice = [dict valueForKey:@"drive_trip"];
    if ([Utils isDictionary:pdevice]) {
        oseq = [pdevice valueForKey:SettingsConst.CryptoKeys.OSEQ];
    }
    pdevice = [dict valueForKey:@"drive_charge"];
    if ([Utils isDictionary:pdevice]) {
        oseq = [pdevice valueForKey:SettingsConst.CryptoKeys.OSEQ];
    }
    
    if (appDelegate.isDriverMode) {
        srcName = pName;
        srcId = pID;
        
        dstName = uName;
        dstId = uID;
        
        chatUserName = [NSString stringWithFormat:LocalizedStr(@"Chat.Head.title"), dstName];
    }
    else {
        srcName = uName;
        srcId = uID;
        
        dstName = pName;
        dstId = pID;
        
        chatUserName = [NSString stringWithFormat:LocalizedStr(@"Chat.Head.title"), dstName];
    }
    
    chatUserName = [Utils trimStringOnly:chatUserName];
    UIImage *srcImage = nil;
    UIImage *dstImage = nil;
    if (![[dict objectForKey:@"picImage"] isEqual:[NSNull null]]) {
        dstImage = [dict objectForKey:@"picImage"];
    }
    else {
        dstImage = [UIImage imageNamed:@"ic_user_white"];//ic_user
        [self alertError:@"" message:LocalizedStr(@"Chat.Alert.image.text")];
        return;
    }
    
    if (appDelegate.isDriverMode) {
        srcImage = dstImage;
        dstImage = appDelegate.imgProfileDriver;
    }
    else {
        srcImage = appDelegate.imgProfileRider;
    }
    
    if (srcImage == nil) {
        srcImage = [UIImage imageNamed:@"ic_user_white"];
    }
    if (dstImage == nil) {
        dstImage = [UIImage imageNamed:@"ic_user_white"];
    }
    
    NSDictionary *dic = @{@"srcName":[Utils trimStringOnly:srcName], // 본인
                          @"dstName":[Utils trimStringOnly:dstName], // 상태방
                          @"srcId":srcId, // 본인
                          @"dstId":dstId, // 상태방
                          @"useq":useq, // 본인
                          @"pseq":pseq, // 상태방
                          @"oseq":oseq,
                          @"srcImage":srcImage,
                          @"dstImage":dstImage,
                          @"headerView":@"0"
                          };
    
    ChatView* chatView = [[ChatView alloc] initWith:groupId description:chatUserName recent:dic];
    [self.navigationController pushViewController:chatView animated:YES];
    
    [Utils updateAlrim:CODE_TYPE_CHAT code:CODE_ALRIM_CHAT(oseq) value:CODE_FLAG_OFF];
    //[Utils deleteAlrim:CODE_ALRIM_NEWCHAT];
}

-(void)updateRightBadge:(UIButton*)btnBadge
{
    UIButton *btn = (UIButton*)btnBadge;
    NSDictionary *dict = self->items[btn.tag];
    NSDictionary *pdevice = nil;
    NSString* oseq = @"";
    
    pdevice = [dict valueForKey:@"drive_order"];
    if ([Utils isDictionary:pdevice]) {
        oseq = [pdevice valueForKey:SettingsConst.CryptoKeys.OSEQ];
    }
    pdevice = [dict valueForKey:@"drive_pickup"];
    if ([Utils isDictionary:pdevice]) {
        oseq = [pdevice valueForKey:SettingsConst.CryptoKeys.OSEQ];
    }
    pdevice = [dict valueForKey:@"drive_trip"];
    if ([Utils isDictionary:pdevice]) {
        oseq = [pdevice valueForKey:SettingsConst.CryptoKeys.OSEQ];
    }
    pdevice = [dict valueForKey:@"drive_charge"];
    if ([Utils isDictionary:pdevice]) {
        oseq = [pdevice valueForKey:SettingsConst.CryptoKeys.OSEQ];
    }
    
    BOOL flag = [Utils showAlrim:CODE_TYPE_CHAT code:oseq];
    
    //[Utils newBadge:btnBadge show:flag];
}

#pragma mark - Action
-(IBAction)onNaviButtonClick:(id)sender {
    UIButton *btn = (UIButton*)sender;
    NSLog(@"onNaviButtonClick");
    //[super onNaviButtonClick:sender];
    
    if (btn.tag == 1100) {
        [self removeEventListeners];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#ifndef _DRIVER_MODE
-(IBAction)onClickCharge:(id)sender
{
    UIButton *btn = (UIButton*)sender;

    TXChargeViewVC *vc = [[TXChargeViewVC alloc] initWithNibName:@"TXChargeViewVC" bundle:[NSBundle mainBundle]];
    vc.chargeDic = self->items[btn.tag];;
    [self.navigationController pushViewController:vc animated:YES];
}
#endif

#pragma mark - UITableView Delegate & Datasrouce -
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [tableView setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [tableView setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
    UIView *seperatorView = [[UIView alloc] initWithFrame:CGRectMake(10, cell.frame.size.height - 1, cell.frame.size.width - 10*2, 1)];
    seperatorView.backgroundColor = UIColorTableSeperator;
    [cell addSubview:seperatorView];
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    //return 112;
    
    return 90;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return self->items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	TXTripHistoryCell *cell = [tableView dequeueReusableCellWithIdentifier:self.cellIdentifier];
	
    NSDictionary *dic           = self->items[indexPath.row];
    NSDictionary* drive_trip    = nil;
    NSDictionary* drive_pickup  = nil;
    NSDictionary *pMap          = nil;
    NSString *api               = @"";
    
    pMap = [cell bindData:dic type:NO];
    CGRect rect = cell.frame;
    rect.size.height = 61;
    cell.frame = rect;
    
    cell.btnProfile.tag = indexPath.row;
    cell.btnCharge.tag = indexPath.row;
    [self updateRightBadge:cell.btnProfile];
    
    if (appDelegate.isDriverMode) {
        api = @"PP601";
    }
    else {
        api = @"UR601";
    }
    
    drive_trip    = [dic valueForKey:@"drive_trip"]; //trip_duration/trip_distance, begin_location, end_location, update_date
    drive_pickup  = [dic valueForKey:@"drive_pickup"]; //trip_duration/trip_distance, begin_location, end_location, update_date
    
    cell.addrLine.hidden = YES;
    //cell.lb6.hidden = YES;
    //cell.lb7.hidden = YES;
    cell.lb1.tag = [pMap[@"finish"] integerValue];

    if (![[dic objectForKey:@"picImage"] isEqual:[NSNull null]]) {
        cell.profile.image = [dic objectForKey:@"picImage"];
        cell.profile.tag = 1;
    }
    else {
        cell.profile.image = [UIImage imageNamed:@"ic_user_white"];//ic_user
        // 3. call api
        //dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            //Background Thread
        
            [Utils downloadImageWithURL:api property:pMap completionBlock:^(BOOL succeeded, UIImage *image) {
                if (succeeded && image) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^(void){
                        //Run UI Updates
                        // change the image in the cell
                        cell.profile.image = image;
                        cell.profile.tag = 1;
                        [Utils setCircleImage:cell.profile];
                    });
                    
                    // cache the image for use later (when scrolling up)
                    NSMutableDictionary *newDict = [[NSMutableDictionary alloc] init];
                    [newDict addEntriesFromDictionary:dic];
                    [newDict setObject:image forKey:@"picImage"];
                    [self->items replaceObjectAtIndex:indexPath.row withObject:newDict];
                }
            }];
            
        //});
    }
    
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    TXTripHistoryCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    // 이미지가 존재하지 않으면
//    if (cell.profile.tag != 1) {
//        [self alertError:@"" message:LocalizedStr(@"String.Loading.Address")];
//        return;
//    }
    
    NSDictionary *dic = self->items[indexPath.row];
    NSString *src = cell.lb6.text;
    NSString *dst = cell.lb7.text;
    src = @"";
    dst = @"";
    NSDictionary *pos = @{
                          @"begin_address" : src,
                          @"end_address" : dst,
                          @"finish" : [NSString stringWithFormat:@"%ld",(long)cell.lb1.tag]
                          };
    
//    if ([src isEqualToString:@""]) {
//        LGAlertView *alertView = [[LGAlertView alloc] initWithTitle:nil
//                                    message:LocalizedStr(@"String.Loading.Address")
//                                      style:LGAlertViewStyleAlert
//                               buttonTitles:@[LocalizedStr(@"Button.OK")]
//                          cancelButtonTitle:nil
//                     destructiveButtonTitle:nil
//                              actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
//                                  NSLog(@"actionHandler, %@, %lu", title, (long unsigned)index);
//                                  
//                              }
//                              cancelHandler:^(LGAlertView *alertView) {
//                              }
//                         destructiveHandler:^(LGAlertView *alertView) {
//                         }];
//        [Utils initAlertButtonColor:alertView];
//        [alertView showAnimated:YES completionHandler:nil];
//        
//        return;
//    }
    NSMutableDictionary *trip = [NSMutableDictionary dictionaryWithDictionary:pos];
    [trip addEntriesFromDictionary:dic];
    //NSDictionary* drive_trip    = [dic valueForKey:@"drive_trip"]; //trip_duration/trip_distance, begin_location, end_location, update_date
    
    TXTripDetailVC *vc = [[TXTripDetailVC alloc] initWithNibName:@"TXTripDetailVC" bundle:[NSBundle mainBundle]];
    vc.tripData = (NSDictionary*)trip;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - onEvent
-(void)onEvent:(TXEvent *)event eventParams:(id)subscriptionParams {
    
    TXResponseDescriptor * descriptor = [event getEventProperty:TXEvents.Params.DESCRIPTOR];
    TXRequestObj *request = [event getEventProperty:TXEvents.Params.REQUEST];
    
    if([event.name isEqualToString:@"UR301"]) {
        
        if(descriptor.success == true) {
            NSString     *response = [[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding];
            NSDictionary *result   = getJSONObj(response);
            
            
            NSArray *tripData              = [result valueForKey:@"drive_historys"];
            
            self->items = [NSMutableArray arrayWithCapacity:[tripData count]];
            
            for (NSDictionary* dic in tripData) {
                if ([Utils isDictionary:[dic valueForKey:@"drive_driver"]]) {
                    NSMutableDictionary *newDict = [[NSMutableDictionary alloc] init];
                    [newDict addEntriesFromDictionary:dic];
                    [newDict setObject:[NSNull null] forKey:@"picImage"];
                    [newDict setObject:@"" forKey:@"src_addr"];
                    [newDict setObject:@"" forKey:@"dst_addr"];
                    [items addObject:newDict];
                }
            }
            
            NSString *count = [NSString stringWithFormat:@"%d",(int)[tripData count]];
            
            UILabel *label = (UILabel*)[[super navigationView] viewWithTag:2001];
            label.text = [NSString stringWithFormat:@"%@ %@",count,LocalizedStr(@"Menu.TripHistory.List1.title")];
            
//            NSString *htmlString = [NSString stringWithFormat:@"%@ %@",
//                                    FONT_STR_COLOR(count),
//                                    FONT_STR_TEXT(LocalizedStr(@"Menu.TripHistory.List1.title"))];
//            ((RTLabel *)[self.view viewWithTag:2001]).text = htmlString;
            
            //if ([self->items count])
            {
                self.tableView.separatorColor = [UIColor colorWithRed:150/255.0f green:161/255.0f blue:177/255.0f alpha:1.0f];
                //[self.tableView reloadData];
                [self.tableView reloadDataAndWait:^{
                    //call the required method here
                    [self hideBusyIndicator];
                }];
            }
            
        } else {
            //에러주석[self alertError:@"Error" message:descriptor.error];
            [self hideBusyIndicator];
        }
        //[self hideBusyIndicator];
    }
    else if([event.name isEqualToString:@"PP301"]) {
        
        if(descriptor.success == true) {
            NSString     *response = [[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding];
            NSDictionary *result   = getJSONObj(response);
            
            NSArray *tripData              = [result valueForKey:@"drive_historys"];
            
            self->items = [NSMutableArray arrayWithCapacity:[tripData count]];
            
            for (NSDictionary* dic in tripData) {
//                [dic setObject:@"" forKey:@"picImage"];
//                [items addObject:dic];
                if ([Utils isDictionary:[dic valueForKey:@"drive_rider"]]) {
                    NSMutableDictionary *newDict = [[NSMutableDictionary alloc] init];
                    [newDict addEntriesFromDictionary:dic];
                    [newDict setObject:[NSNull null] forKey:@"picImage"];
                    [newDict setObject:@"" forKey:@"src_addr"];
                    [newDict setObject:@"" forKey:@"dst_addr"];
                    [items addObject:newDict];
                }
            }
            
            NSString *count = [NSString stringWithFormat:@"%d",(int)[tripData count]];
            UILabel *label = (UILabel*)[[super navigationView] viewWithTag:2001];
            label.text = [NSString stringWithFormat:@"%@ %@",count,LocalizedStr(@"Menu.TripHistory.List1.title")];
            
//            NSString *htmlString = [NSString stringWithFormat:@"%@ %@",
//                                    FONT_STR_COLOR(count),
//                                    FONT_STR_TEXT(LocalizedStr(@"Menu.TripHistory.List1.title"))];
//            ((RTLabel *)[self.view viewWithTag:2001]).text = htmlString;
            
            //if ([self->items count])
            {
                self.tableView.separatorColor = [UIColor colorWithRed:150/255.0f green:161/255.0f blue:177/255.0f alpha:1.0f];
                //[self.tableView reloadData];
                [self.tableView reloadDataAndWait:^{
                    //call the required method here
                    [self hideBusyIndicator];
                }];
            }
            
        } else {
            //에러주석[self alertError:@"Error" message:descriptor.error];
            [self hideBusyIndicator];
        }
        //[self hideBusyIndicator];
    }
}

/*
//- (void)downloadImageWithURL:(NSString *)code property:(NSDictionary*)pMap completionBlock:(void (^)(BOOL succeeded, UIImage *image))completionBlock
{
    TXRequestObj *request     = [self->model createRequest:code];
    
    request.body = getJSONStr(pMap);
    NSMutableURLRequest *httpRequest = [request createHTTPRequest];
    DLogI(@"Sent Async Request to URL - %@\n\n", request.reqUrl);
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:httpRequest
                                            completionHandler:
                                  ^(NSData *data, NSURLResponse *response, NSError *error) {
                                      if ([response isKindOfClass:[NSHTTPURLResponse class]] &&
                                          ((NSHTTPURLResponse *)response).statusCode == 200 &&
                                          error == nil &&
                                          data != nil) {
                                          if ([data length]) {
                                              
                                              UIImage *image = [UIImage imageWithData:data];
                                              completionBlock(YES,image);
                                          }
                                          else {
                                              completionBlock(YES,[UIImage imageNamed:@"ic_user_white"]);
                                          }
                                      } else {
                                          completionBlock(NO,nil);
                                      }
                                  }];
    [task resume];
    //
    [NSURLConnection sendAsynchronousRequest:httpRequest
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               if ( !error )
                               {
                                   if ([data length]) {
                                       
                                       UIImage *image = [UIImage imageWithData:data];
                                       completionBlock(YES,image);
                                   }
                                   else {
                                       completionBlock(YES,[UIImage imageNamed:@"ic_user_white"]);
                                   }
                               } else{
                                   completionBlock(NO,nil);
                               }
                           }];
     //
}
*/
@end
