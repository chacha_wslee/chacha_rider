//
//  MenuViewController.m
//  SlideMenu
//

//

#import "TXTripDetailVC.h"
#import "TXTripHistoryDetailCell.h"
#import "TXHelpVC.h"
#import "ChatView.h"
#ifndef _DRIVER_MODE
#import "TXChargeViewVC.h"
#endif

#define RATE_STR(c) [NSString stringWithFormat:LocalizedStr(@"Menu.TripHistory.Trip.Rate.text"),c]

@interface TXTripDetailVC() <GMSMapViewDelegate> {
    NSMutableArray *items;
    NSArray *_msgList;
    
    UIButton *btnProfile;
    UIImageView *profile;
    UILabel *tridLabel;
    
    BOOL isDestination;
    BOOL isFirst;
}

@end

@implementation TXTripDetailVC {
    NSMutableDictionary *propertyMap;
    GMSMapView *_mapView;
    
    NSInteger height_;
}
@synthesize cellIdentifier;
@synthesize tripData;


-(void)viewDidLoad {
    
    [super viewDidLoad];
    
    isFirst = YES;
    height_ = 160;
    height_ = 224; // call, chat 포함
    
    //NSString *_name = [Utils getUserName];

    [super configureBottomLine];
    UIView  *view = (UIView*)[[super navigationView] viewWithTag:1500];
    view.hidden = YES;
    UIView *n = [super navigationView];
    NSInteger _y = n.frame.origin.y + n.frame.size.height;
    _y = 20;
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, _y, self.view.bounds.size.width, self.view.bounds.size.height-_y)
                                                  style:UITableViewStylePlain];
    self.tableView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:self.tableView];
    
    UINib *nib = [UINib nibWithNibName:@"TXTripHistoryDetailCell2" bundle:nil];
    if ([self.tripData[@"finish"] isEqualToString:@"1"]) {
        height_ = 160;
        nib = [UINib nibWithNibName:@"TXTripHistoryDetailCell" bundle:nil];
    }
    self.cellIdentifier = @"TXTripHistoryDetailCell";
    [self.tableView registerNib:nib forCellReuseIdentifier:self.cellIdentifier];
    
    self.tableView.separatorColor = [UIColor clearColor];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.opaque = NO;
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.tableHeaderView = ({
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, self.tableView.frame.size.height - height_)];
        view.tag = 2000;
        
        view;
    });
    self.tableView.scrollEnabled = NO;
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    [self initMapView];
    self->items = [NSMutableArray arrayWithCapacity:1];
    [items addObject:self.tripData];
    
    [self.view bringSubviewToFront:n];
    
    // 하단 사진이미지
    profile = [[UIImageView alloc] initWithFrame:CGRectMake(self.view.frame.size.width - self.view.frame.size.width/2 - 72/2,
                                                            self.view.frame.size.height - height_ - 72/2,
                                                            72, 72)];
    profile.tag = 2;
    profile.layer.cornerRadius = profile.frame.size.width / 2;
    profile.clipsToBounds = YES;
    profile.contentMode = UIViewContentModeScaleAspectFill;
    profile.layer.masksToBounds = YES;
    
    [self.view addSubview:profile];
    
    
    // TRID
    tridLabel = [[UILabel alloc] initWithFrame:CGRectMake(_mapView.frame.size.width-140-12, profile.frame.origin.y, 140, 18)];
    //tridLabel = [[UILabel alloc] initWithFrame:CGRectMake(_mapView.frame.size.width-140-12, kNaviTopHeight+kNaviBottomHeight, 140, 18)];
    //UILabel *tridLabel = [[UILabel alloc] initWithFrame:CGRectMake(_mapView.frame.size.width-140-12, self.view.frame.size.height-18-12 - height_, 140, 18)];
    //tridLabel.text = [NSString stringWithFormat:@"TRID %@%010d",service, [oseq intValue]];
    tridLabel.textColor = HEXCOLOR(0x000000FF);
    tridLabel.textAlignment = NSTextAlignmentCenter;
    tridLabel.backgroundColor = [UIColor whiteColor];
    tridLabel.font = [UIFont systemFontOfSize:14];
    [self.view addSubview:tridLabel];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self removeEventListeners];
}

-(void)dealloc {
}

-(void)initMapView {
    NSDictionary *drive_trip              = [self.tripData valueForKey:@"drive_trip"];
    NSArray *blocs = [[Utils nullTolocation:[drive_trip objectForKey:@"begin_location"]] componentsSeparatedByString:@","];
    if (![Utils isDictionary:drive_trip]) {
        drive_trip              = [self.tripData valueForKey:@"drive_pickup"];
        blocs = [[Utils nullTolocation:[drive_trip objectForKey:@"lsrc"]] componentsSeparatedByString:@","];
    }
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:[[blocs objectAtIndex:0] doubleValue]
                                                            longitude:[[blocs objectAtIndex:1] doubleValue]
                                                                 zoom:16];
    
    _mapView.hidden = YES;
    _mapView = [GMSMapView mapWithFrame:CGRectMake(0, 0, self.view.bounds.size.width,
                                                   [self.view viewWithTag:2000].frame.size.height) camera:camera];
    
    _mapView.delegate = self;
    
    _mapView.autoresizingMask = UIViewAutoresizingFlexibleWidth |
    UIViewAutoresizingFlexibleHeight |
    UIViewAutoresizingFlexibleBottomMargin |
    UIViewAutoresizingFlexibleTopMargin;
    
    UIView *n = [super navigationView];
    NSInteger _y = n.frame.origin.y + n.frame.size.height;
    //top, left, bottom, right
    _mapView.padding = UIEdgeInsetsMake(_y, 0, 0, 0);
    
    //_mapView.settings.compassButton = YES;
    // 모든 제스쳐를 막는다.
#ifdef _DEBUG
    _mapView.settings.compassButton = YES;
#else
    _mapView.settings.compassButton = NO;
    _mapView.settings.scrollGestures = NO;
    _mapView.settings.zoomGestures = NO;
    _mapView.settings.tiltGestures = NO;
    _mapView.settings.rotateGestures = NO;
#endif
    
//    [self.view viewWithTag:5];
    [[self.view viewWithTag:2000] addSubview:_mapView];
    
}

#pragma mark - Configure

-(void)configureStyles {
    [super configureStyles];
    
    if (appDelegate.isDriverMode) {
        [super navigationType01X:[UIImage imageNamed:@"btn_back"] centerText:LocalizedStr(@"Menu.TripHistory.Detail.title")];
    }
    else {
        [super navigationType01X:[UIImage imageNamed:@"btn_back"] centerText:LocalizedStr(@"Menu.TripHistory.Detail.title")];
    }
}

-(void)configure {
    [super configure];
    
    // 2. init event
    _msgList = @[
                 @[@"UR301",@""], // rider
                 @[@"PP301",@""], // driver
                 ];
    
    [self registerEventListeners];
    
    propertyMap = [[NSMutableDictionary alloc] init];
    
    if (appDelegate.isDriverMode) {
        [self configureDriver];
    }
    else {
        [self configureRider];
    }
    
    UIView *n = [super navigationView];
    [n setBackgroundColor:[UIColor clearColor]];
    
    CAGradientLayer *layer = [CAGradientLayer layer];
    layer.frame = n.bounds;
    UIColor *theColor = [UIColor whiteColor];
    layer.colors = [NSArray arrayWithObjects:
                    (id)[theColor CGColor],
                    (id)[[theColor colorWithAlphaComponent:0.9f] CGColor],
                    (id)[[theColor colorWithAlphaComponent:0.6f] CGColor],
                    (id)[[theColor colorWithAlphaComponent:0.5f] CGColor],
                    (id)[[theColor colorWithAlphaComponent:0.3f] CGColor],
                    (id)[[theColor colorWithAlphaComponent:0.1f] CGColor],
                    (id)[[theColor colorWithAlphaComponent:0.0f] CGColor],
                    (id)[[UIColor clearColor] CGColor],
                    (id)[[UIColor clearColor] CGColor],
                    nil];
    
    //layer.colors = [NSArray arrayWithObjects:(id)whiteColor.CGColor, (id)clearColor.CGColor, nil];
    [n.layer insertSublayer:layer atIndex:0];
}

-(void) configureDriver {

    //NSDictionary *user = [appDelegate.dicDriver objectForKey:@"provider"];
    
    NSDictionary* drive_trip    = [self.tripData valueForKey:@"drive_trip"];
    
    // 3. call api
    if ([Utils isDictionary:drive_trip]) {
        isDestination = YES;
        [self->model PP301:[drive_trip valueForKey:@"oseq"]];
    }
    else {
        isDestination = NO;
        isDestination = YES;
        NSDictionary* drive_pickup    = [self.tripData valueForKey:@"drive_pickup"];
        [self->model PP301:[drive_pickup valueForKey:@"oseq"]];
    }

    
}

-(void) configureRider {

    //NSDictionary *user = [appDelegate.dicRider objectForKey:@"user"];
    
    NSDictionary* drive_trip    = [self.tripData valueForKey:@"drive_trip"];
    
    // 3. call api
    if ([Utils isDictionary:drive_trip]) {
        isDestination = YES;
        [self->model UR301:[drive_trip valueForKey:@"oseq"]];
    }
    else {
        isDestination = NO;
        isDestination = YES;
        NSDictionary* drive_pickup    = [self.tripData valueForKey:@"drive_pickup"];
        [self->model UR301:[drive_pickup valueForKey:@"oseq"]];
    }
    
}

-(void) registerEventListeners {
    for (NSArray *msg in _msgList) {
        [self->model addEventListener:self forEvent:[msg objectAtIndex:0] eventParams:nil];
    }
}

-(void) removeEventListeners {
    for (NSArray *msg in _msgList) {
        [self->model removeEventListener:self forEvent:[msg objectAtIndex:0]];
    }
}

#pragma mark - Geo
-(CLLocationCoordinate2D) getLocation:(NSString*) location{
    NSArray* stringComponents = [location componentsSeparatedByString:@","];
    
    CLLocationCoordinate2D position;
    position.latitude  = [[NSDecimalNumber decimalNumberWithString:[stringComponents objectAtIndex:0]] doubleValue];
    position.longitude = [[NSDecimalNumber decimalNumberWithString:[stringComponents objectAtIndex:1]] doubleValue];
    
    return position;
}

-(void)getAddress:(NSString*)pos index:(NSIndexPath*)indexPath addr:(NSString*)addr
{
    CLLocationCoordinate2D coor = [self getLocation:pos];
    
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    [locMgr geocodeAPICall:coor
                completion:^(NSDictionary *user, NSString* params, NSString *str, int status) {
                    
                    if (status==1) {
                        //NSAssert(![NSThread isMainThread], @"This shouldn't be called from the main thread");
                        //NSLog(@"responseDict:%@",user);
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            // Update the UI
                            NSString *__address = user[@"fullAddress"];
                            TXTripHistoryDetailCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
                            if ([addr isEqualToString:@"src_addr"]) {
                                cell.lb6.text = __address;
                            }
                            else {
                                cell.lb7.text = __address;
                            }
                            
                            self->items[indexPath.item][addr] = __address;
                        });
                    }
                }];
}

#pragma mark - openChat
-(IBAction)openChatView:(id)sender
{
//    UIButton *btn = (UIButton*)sender;
    NSDictionary *dict = self->items[0];
    NSDictionary *pdevice = nil;
    
    NSString *groupId = @"1";
    NSString *chatUserName = @"";
    NSString *srcName = @"";
    NSString *dstName = @"";
    NSString *srcId = @"";
    NSString *dstId = @"";
    
    NSString* useq = @"";
    NSString* pseq = @"";
    NSString* oseq = @"";
    NSString *pName = @"";
    NSString *uName = @"";
    NSString *pID = @"";
    NSString *uID = @"";
    
    pdevice = [dict valueForKey:@"drive_driver"];
    if ([Utils isDictionary:pdevice]) {
        pseq = [pdevice valueForKey:SettingsConst.CryptoKeys.PSEQ];
        pName = [Utils nameOfuname:[pdevice objectForKey:@"name"]];
        pID = [NSString stringWithFormat:@"pseq,%@",pdevice[@"pseq"]];
    }
    pdevice = [dict valueForKey:@"drive_rider"];
    if ([Utils isDictionary:pdevice]) {
        useq = [pdevice valueForKey:SettingsConst.CryptoKeys.USEQ];
        uName = [Utils nameOfuname:[pdevice objectForKey:@"name"]];
        uID = [NSString stringWithFormat:@"useq,%@",pdevice[@"useq"]];
    }
    
    pdevice = [dict valueForKey:@"drive_order"];
    if ([Utils isDictionary:pdevice]) {
        oseq = [pdevice valueForKey:SettingsConst.CryptoKeys.OSEQ];
    }
    pdevice = [dict valueForKey:@"drive_pickup"];
    if ([Utils isDictionary:pdevice]) {
        oseq = [pdevice valueForKey:SettingsConst.CryptoKeys.OSEQ];
    }
    pdevice = [dict valueForKey:@"drive_trip"];
    if ([Utils isDictionary:pdevice]) {
        oseq = [pdevice valueForKey:SettingsConst.CryptoKeys.OSEQ];
    }
    pdevice = [dict valueForKey:@"drive_charge"];
    if ([Utils isDictionary:pdevice]) {
        oseq = [pdevice valueForKey:SettingsConst.CryptoKeys.OSEQ];
    }
    
    if (appDelegate.isDriverMode) {
        srcName = pName;
        srcId = pID;
        
        dstName = uName;
        dstId = uID;
        
        chatUserName = [NSString stringWithFormat:LocalizedStr(@"Chat.Head.title"), dstName];
    }
    else {
        srcName = uName;
        srcId = uID;
        
        dstName = pName;
        dstId = pID;
        
        chatUserName = [NSString stringWithFormat:LocalizedStr(@"Chat.Head.title"), dstName];
    }
    
    chatUserName = [Utils trimStringOnly:chatUserName];
    UIImage *srcImage = nil;
    UIImage *dstImage = nil;
    if (![[dict objectForKey:@"picImage"] isEqual:[NSNull null]]) {
        dstImage = [dict objectForKey:@"picImage"];
    }
    else {
        dstImage = [UIImage imageNamed:@"ic_user_white"];//ic_user
        [self alertError:@"" message:LocalizedStr(@"Chat.Alert.image.text")];
        return;
    }
    
    if (appDelegate.isDriverMode) {
        srcImage = dstImage;
        dstImage = appDelegate.imgProfileDriver;
    }
    else {
        srcImage = appDelegate.imgProfileRider;
    }
    
    if (srcImage == nil) {
        srcImage = [UIImage imageNamed:@"ic_user_white"];
    }
    if (dstImage == nil) {
        dstImage = [UIImage imageNamed:@"ic_user_white"];
    }
    
    NSDictionary *dic = @{@"srcName":[Utils trimStringOnly:srcName], // 본인
                          @"dstName":[Utils trimStringOnly:dstName], // 상태방
                          @"srcId":srcId, // 본인
                          @"dstId":dstId, // 상태방
                          @"useq":useq, // 본인
                          @"pseq":pseq, // 상태방
                          @"oseq":oseq,
                          @"srcImage":srcImage,
                          @"dstImage":dstImage,
                          @"headerView":@"0"
                          };
    
    ChatView* chatView = [[ChatView alloc] initWith:groupId description:chatUserName recent:dic];
    [self.navigationController pushViewController:chatView animated:YES];
    
    [Utils updateAlrim:CODE_TYPE_CHAT code:CODE_ALRIM_CHAT(oseq) value:CODE_FLAG_OFF];
    //[Utils deleteAlrim:CODE_ALRIM_NEWCHAT];
}

#pragma mark - IBAction

-(IBAction)openCall {
    
    NSString* acall = [Utils acall:items[0]];
    
    if ([acall isEqualToString:@"no"]) {
        if (appDelegate.isDriverMode)
            [Utils onErrorAlertViewTitle:@"Error" title:@"Failed Call to Rider"];
        else
            [Utils onErrorAlertViewTitle:@"Error" title:@"Failed Call to Driver"];
    }
    else if (![acall isEqualToString:@""] && acall != nil) {
        //        NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt://%@",acall]];
        //        if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        //            [[UIApplication sharedApplication] openURL:phoneUrl];
        //        }
        //
        NSURL *phoneUrl = [NSURL URLWithString:[@"telprompt://" stringByAppendingString:acall]];
        NSURL *phoneFallbackUrl = [NSURL URLWithString:[@"tel://" stringByAppendingString:acall]];
        
        if ([UIApplication.sharedApplication canOpenURL:phoneUrl]) {
            [UIApplication.sharedApplication openURL:phoneUrl];
        } else if ([UIApplication.sharedApplication canOpenURL:phoneFallbackUrl]) {
            [UIApplication.sharedApplication openURL:phoneFallbackUrl];
        } else
        {
            [Utils onErrorAlertViewTitle:@"Error" title:@"Call facility is not available!!!"];
        }
    }
    else
        [Utils onErrorAlertViewTitle:@"Error" title:@"Call facility is not available!!!"];
}

#pragma mark - Action
-(IBAction)onNaviButtonClick:(id)sender {
    UIButton *btn = (UIButton*)sender;
    NSLog(@"onNaviButtonClick");
    //[super onNaviButtonClick:sender];
    
    if (btn.tag == 1100) {
        [self removeEventListeners];
        [self.navigationController popViewControllerAnimated:YES];
    }
    else if (btn.tag == 1600) {
        TXHelpVC *vc = [[TXHelpVC alloc] initWithNibName:@"TXHelpVC" bundle:[NSBundle mainBundle]];
        [super pushViewController:vc];
    }
}

#ifndef _DRIVER_MODE
-(IBAction)onClickCharge:(id)sender
{
    UIButton *btn = (UIButton*)sender;
    
    TXChargeViewVC *vc = [[TXChargeViewVC alloc] initWithNibName:@"TXChargeViewVC" bundle:[NSBundle mainBundle]];
    vc.chargeDic = self->items[btn.tag];;
    [self.navigationController pushViewController:vc animated:YES];
}
#endif

#pragma mark - UITableView Delegate & Datasrouce -
//- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
//    
//    if ([tableView respondsToSelector:@selector(setSeparatorInset:)]) {
//        [tableView setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
//    }
//    
//    if ([tableView respondsToSelector:@selector(setLayoutMargins:)]) {
//        [tableView setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
//    }
//    
//    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
//        [cell setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
//    }
//    
//    
//    UIView *seperatorView = [[UIView alloc] initWithFrame:CGRectMake(0, cell.frame.size.height - 1, cell.frame.size.width, 1)];
//    seperatorView.backgroundColor = UIColorTableSeperator;
//    [cell addSubview:seperatorView];
//    
//}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return height_;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return self->items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TXTripHistoryDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:self.cellIdentifier];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    UIView *v = (UIView*)[cell viewWithTag:1000];
    v.backgroundColor = HEXCOLOR(0xf7f7f7FF);
    cell.backgroundColor = [UIColor clearColor];
    
    
#ifdef _DRIVER_MODE
    cell.btnCall.hidden = YES;
    CGRect frame = cell.btnChat.frame;
    frame.origin.x = 0;
    frame.size.width = cell.frame.size.width;
    cell.btnChat.frame = frame;
#endif
    cell.btnHelp.layer.cornerRadius = 4.0f;
    cell.btnHelp.layer.masksToBounds = YES;
    cell.btnHelp.layer.borderColor            = HEXCOLOR(0xccccccff).CGColor;
    cell.btnHelp.layer.borderWidth            = 1.0f;
    
    NSDictionary *dic           = self->items[indexPath.row];
    NSDictionary* drive_trip    = nil;
    NSDictionary* drive_pickup  = nil;
    NSDictionary* drive_rating  = nil;
    NSDictionary* drive_payout  = nil;
    NSString *service           = @"";
    NSString *oseq              = @"";
    
    drive_trip    = [dic valueForKey:@"drive_trip"]; //trip_duration/trip_distance, begin_location, end_location, update_date
    drive_pickup  = [dic valueForKey:@"drive_pickup"]; //trip_duration/trip_distance, begin_location, end_location, update_date
    drive_rating  = [dic valueForKey:@"drive_rating"]; //rate/note
    drive_payout  = [dic valueForKey:@"drive_payout"]; //driver total_payout
    [cell bindData:dic type:NO];
    
    if ([dic[@"src_addr"] isEqualToString:@""]) {
        NSString *lb6 = @"";
        NSString *lb61 = @"";
        if ([Utils isDictionary:drive_trip]) {
            lb6 = [Utils nullTolocation:[drive_trip valueForKey:@"begin_location"]];
            service = [drive_trip valueForKey:@"service"];
            oseq = [drive_trip valueForKey:@"oseq"];
        }
        else {
            lb6 = [Utils nullTolocation:[drive_pickup valueForKey:@"start_location"]];
            service = [drive_pickup valueForKey:@"service"];
            oseq = [drive_pickup valueForKey:@"oseq"];
        }
        
        if (![lb6 length] && ![lb6 isEqualToString:@"0,0"]) {
            cell.lb6.text = LocalizedStr(@"String.NotExist");
            self->items[indexPath.row][@"src_addr"] = LocalizedStr(@"String.NotExist");
        }
        else {
            if ([Utils isDictionary:drive_trip]) {
                lb61 = [Utils nullTolocation:[drive_trip valueForKey:@"begin_addr"]];
            }
            else {
                lb61 = [Utils nullTolocation:[drive_pickup valueForKey:@"start_addr"]];
            }
            
            //lb61 = @"";
            if (![lb61 isEqualToString:@""] && ![lb61 isEqualToString:@"0,0"]) {
                cell.lb6.text = lb61;
                self->items[indexPath.item][@"src_addr"] = lb61;
            }
            else {
                [self getAddress:lb6 index:indexPath addr:@"src_addr"];
            }
        }
    }
    else {
        cell.lb6.text = dic[@"src_addr"];
    }
    
    if ([dic[@"dst_addr"] isEqualToString:@""]) {
        NSString *lb7 = @"";
        NSString *lb71 = @"";
        if ([Utils isDictionary:drive_trip]) {
            if (![Utils nullToString:[drive_trip valueForKey:@"end_location"]]) {
                lb7 = [Utils nullTolocation:[drive_trip valueForKey:@"end_location"]];
            }
            else {
                lb7 = [Utils nullTolocation:[drive_trip valueForKey:@"ldst"]];
            }
        }
        else {
            lb7 = [Utils nullTolocation:[drive_pickup valueForKey:@"lsrc"]];
            //lb7 = [drive_pickup valueForKey:@"start_location"];
        }
        
        if (![lb7 length] && ![lb7 isEqualToString:@"0,0"]) {
            cell.lb7.text = LocalizedStr(@"String.NotExist");
            self->items[indexPath.row][@"dst_addr"] = LocalizedStr(@"String.NotExist");
        }
        else {
            
            if ([Utils isDictionary:drive_trip]) {
                lb71 = [Utils nullTolocation:[drive_trip valueForKey:@"end_addr"]];
            }
            else {
                //lb71 = [Utils nullTolocation:[drive_trip valueForKey:@"end_location"]];
            }
            
            //lb71 = @"";
            if (![lb71 isEqualToString:@""] && ![lb71 isEqualToString:@"0,0"]) {
                cell.lb7.text = lb71;
                self->items[indexPath.item][@"dst_addr"] = lb71;
            }
            else {
                [self getAddress:lb7 index:indexPath addr:@"dst_addr"];
            }
        }
    }
    else {
        cell.lb7.text = dic[@"dst_addr"];
    }
    
    tridLabel.text = [NSString stringWithFormat:@"TRID %@%010d",service, [oseq intValue]];
    
    //UIImageView *profile = ((UIImageView *)[cell.contentView viewWithTag:2]);
    //UIImageView *profile = ((UIImageView *)[self.view viewWithTag:2]);
    
    if (![[dic objectForKey:@"picImage"] isEqual:[NSNull null]]) {
        profile.image = [dic objectForKey:@"picImage"];
        [Utils setCircleImage:profile];
    }
    else {
        profile.image = [UIImage imageNamed:@"ic_user_white"];//ic_user
        [Utils setCircleImage:profile];
    }

    

	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)updateMapFitBoundsMarker:(NSArray*)_markers {
    GMSCoordinateBounds *bounds;
    for (GMSMarker *marker in _markers) {
        if (bounds == nil) {
            bounds = [[GMSCoordinateBounds alloc] initWithCoordinate:marker.position
                                                          coordinate:marker.position];
        }
        bounds = [bounds includingCoordinate:marker.position];
    }
    GMSCameraUpdate *update = [GMSCameraUpdate fitBounds:bounds
                                             withPadding:50.0f];
    [_mapView moveCamera:update];
    _mapView.hidden = NO;
    //[_mapView animateWithCameraUpdate:update];
}

- (void)updateMapFitBounds:(NSArray*)coord__ {
    GMSCoordinateBounds *bounds;
    for (NSValue *aValue in coord__) {
        CLLocationCoordinate2D coord = [aValue MKCoordinateValue];
        if (bounds == nil) {
            bounds = [[GMSCoordinateBounds alloc] initWithCoordinate:coord
                                                          coordinate:coord];
        }
        bounds = [bounds includingCoordinate:coord];
    }
    //CGFloat currentZoom = self.mapView_.camera.zoom;
    GMSCameraUpdate *update = [GMSCameraUpdate fitBounds:bounds withPadding:kBasicMapPadding];
    [_mapView moveCamera:update];
}

#pragma mark - onEvent
-(void)onEvent:(TXEvent *)event eventParams:(id)subscriptionParams {
    
    TXResponseDescriptor * descriptor = [event getEventProperty:TXEvents.Params.DESCRIPTOR];
    TXRequestObj *request = [event getEventProperty:TXEvents.Params.REQUEST];
    
    if([event.name isEqualToString:@"UR301"] || [event.name isEqualToString:@"PP301"]) {
        
        if(descriptor.success == true) {
            NSString     *response = [[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding];
            NSDictionary *result   = getJSONObj(response);
            
            [_mapView clear];
            
            NSArray *stepData              = [result valueForKey:@"drive_steps"];
            
            NSMutableArray *array = [[NSMutableArray alloc] init];
            NSMutableDictionary *dic = [items[0] mutableCopy];
            dic[@"drive_rating"] = [result valueForKey:@"drive_rating"];
            [array addObject:dic];
            items = array;
            
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
            TXTripHistoryDetailCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
            if (!cell.lb9.tag) {
                NSDictionary *attributes = @{ NSFontAttributeName:[UIFont fontWithName:@"AppleSDGothicNeo-Regular" size:12]};
                
                NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
                attachment.image = [UIImage imageNamed:@"star_mini_yellow"];
                NSAttributedString *attachmentString = [NSAttributedString attributedStringWithAttachment:attachment];
                NSMutableAttributedString *myString;
                
                if ([Utils isDictionary:[result valueForKey:@"drive_rating"]]) {
                    myString= [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ ",LocalizedStr(@"Menu.TripHistory.Trip.Rate.text")] attributes:attributes];
                    NSMutableAttributedString *myString2= [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %d",[result[@"drive_rating"][@"rate"] intValue]] attributes:attributes];
                    [myString appendAttributedString:attachmentString];
                    [myString appendAttributedString:myString2];
                }
                else {
                    myString= [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ ",LocalizedStr(@"Menu.TripHistory.Trip.Rate.text")] attributes:attributes];
                    NSMutableAttributedString *myString2= [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %d",0] attributes:attributes];
                    [myString appendAttributedString:attachmentString];
                    [myString appendAttributedString:myString2];
                }

                cell.lb9.attributedText = myString;
                cell.lb9.tag = 1;
            }
            
            
            //[self.tableView reloadData];
            NSDictionary *drive_trip              = [self.tripData valueForKey:@"drive_trip"];
            if (![Utils isDictionary:drive_trip]) {
                drive_trip              = [self.tripData valueForKey:@"drive_pickup"];
            }
            
            NSArray *blocs = nil;
            NSArray *elocs = nil;
            NSArray *tlocs = nil;
            
            if ([stepData count]>0) {
                blocs = [[[stepData objectAtIndex:0] valueForKey:@"location"] componentsSeparatedByString:@","];
                
                if ([stepData count]>0) {
                    elocs = [[[stepData objectAtIndex:[stepData count]-1] valueForKey:@"location"] componentsSeparatedByString:@","];
                }
                if ([stepData count]>1) {
                    tlocs = [[[stepData objectAtIndex:[stepData count]-2] valueForKey:@"location"] componentsSeparatedByString:@","]; // 전전 step
                }
            }
            else {
                blocs = [[drive_trip valueForKey:@"lsrc"] componentsSeparatedByString:@","];
            }
            
            NSArray *clocs = elocs;
            
            NSString *icon__ = @"";
            if ([Utils isDictionary:drive_trip]) {
                icon__ = drive_trip[@"service"];
                //                NSArray *blocs = [[drive_trip objectForKey:@"begin_location"] componentsSeparatedByString:@","];
                NSInteger type = [[self.tripData valueForKey:@"type"] integerValue];
                if (type == 230) {
                    // 목적지가 없을 수 도 있다.
                    if (![[Utils nullToString:[drive_trip objectForKey:@"lsrc"]] isEqualToString:@""]) {
                        elocs = [[drive_trip objectForKey:@"lsrc"] componentsSeparatedByString:@","];
                    }
                    else {
                        elocs = nil;
                    }
                }
                else {
                    // 목적지가 없을 수 도 있다.
                    if (![[Utils nullToString:[drive_trip objectForKey:@"end_location"]] isEqualToString:@""]) {
                        elocs = [[drive_trip objectForKey:@"end_location"] componentsSeparatedByString:@","];
                    }
                    else if (![[Utils nullToString:[drive_trip objectForKey:@"ldst"]] isEqualToString:@""]) {
                        elocs = [[drive_trip objectForKey:@"ldst"] componentsSeparatedByString:@","];
                    }
                    else {
                        elocs = nil;
                    }
                }
            }
            
            CLLocationCoordinate2D scoordinate = kCLLocationCoordinate2DInvalid;
            CLLocationCoordinate2D ecoordinate = kCLLocationCoordinate2DInvalid;
            CLLocationCoordinate2D ccoordinate = kCLLocationCoordinate2DInvalid;
            CLLocationCoordinate2D tcoordinate = kCLLocationCoordinate2DInvalid;
            
            scoordinate.latitude   = [[blocs objectAtIndex:0] doubleValue];
            scoordinate.longitude  = [[blocs objectAtIndex:1] doubleValue];
            if (elocs) {
                ecoordinate.latitude   = [[elocs objectAtIndex:0] doubleValue];
                ecoordinate.longitude  = [[elocs objectAtIndex:1] doubleValue];
            }
            if (clocs) {
                ccoordinate.latitude   = [[clocs objectAtIndex:0] doubleValue];
                ccoordinate.longitude  = [[clocs objectAtIndex:1] doubleValue];
            }
            if (tlocs) {
                tcoordinate.latitude   = [[tlocs objectAtIndex:0] doubleValue];
                tcoordinate.longitude  = [[tlocs objectAtIndex:1] doubleValue];
            }
            
            //NSArray *routes = [[directionResponse directionResponse] objectForKey:@"routes"];
            // NSLog(@"Route : %@", [[directionResponse directionResponse] objectForKey:@"routes"]);
            GMSMutablePath *path = [GMSMutablePath path];
            
            CLLocationCoordinate2D beforecoor = kCLLocationCoordinate2DInvalid;
            CLLocationCoordinate2D aftercoor  = kCLLocationCoordinate2DInvalid;
            
            NSMutableArray *__coords = [[NSMutableArray alloc] init];
            for (NSDictionary* dic in stepData) {
                NSString *loc = [dic valueForKey:@"location"];
                
                NSArray *locs = [loc componentsSeparatedByString:@","];
                //[path addCoordinate:CLLocationCoordinate2DMake(-33.85, 151.20)];
                [path addLatitude:[[locs objectAtIndex:0] doubleValue] longitude:[[locs objectAtIndex:1] doubleValue]];
                tcoordinate = [Utils getLocation:loc];
                if (![Utils isEqualLocation:aftercoor location:tcoordinate]) {
                    beforecoor = aftercoor;
                }
                aftercoor = tcoordinate;
                
                [__coords addObject:[NSValue valueWithMKCoordinate:tcoordinate]];
            }
            
            GMSPolyline *polyline = [GMSPolyline polylineWithPath:path];
            polyline.strokeColor = UIColorPolyLine;
            polyline.strokeWidth = PolyLineStrokeWidth;
            polyline.map = _mapView;
            
            GMSMarker *_markerStart = [GMSMarker new];
            _markerStart.title = LocalizedStr(@"Map.Drive.Marker.Pickup.text");
            //_markerStart.icon = [Utils image:[UIImage imageNamed:@"icon_pin_start"] scaledToSize:CGSizeMake(kMarkerSizeW, kMarkerSizeH)];
            //_markerStart.groundAnchor = CGPointMake(0.5f, 0.5f);
            _markerStart.flat = YES;
            _markerStart.tappable = NO;
            _markerStart.position = scoordinate;
            //_markerStart.map = _mapView;
            _markerStart.icon = [Utils imageFromInfoView:_markerStart.title image:[Utils image:[UIImage imageNamed:@"icon_pin_start"] scaledToSize:CGSizeMake(kMarkerSizeW, kMarkerSizeH)] color:UIColorDefault2];
            
            
            NSArray* _markers = nil;
            GMSMarker *_markerCar;
            if ([GoogleUtils isLocationValid:tcoordinate] && [GoogleUtils isLocationValid:ccoordinate]) {
                CLLocationDistance distance = GMSGeometryDistance(ccoordinate, ecoordinate);
                //CLLocationDirection heading = GMSGeometryHeading(tcoordinate, ccoordinate);
                CLLocationDirection heading = GMSGeometryHeading(beforecoor, aftercoor);
                
                if (distance>0) {
                    UIImage *img = [UIImage imageNamed:[NSString stringWithFormat:@"img_mapcar_%@",icon__]];
                    _markerCar = [GMSMarker new];
                    _markerCar.icon = [Utils image:img scaledToSize:CGSizeMake(kCarMarkerSizeW, kCarMarkerSizeH)];
                    _markerCar.rotation = heading;
                    //_markerFinish.groundAnchor = CGPointMake(0.5f, 0.5f);
                    _markerCar.flat = YES;
                    _markerCar.tappable = NO;
                    _markerCar.position = ccoordinate;
                    _markerCar.map = _mapView;
                }
            }
            if (isDestination) {
                if (elocs) {
                    CLLocationDistance distance = GMSGeometryDistance(ccoordinate, ecoordinate);
                    if (![GoogleUtils isLocationValid:ccoordinate]) {
                        distance = 0;
                    }
                    GMSMarker *_markerFinish = [GMSMarker new];
                    if (distance>0) {
                        NSString *dist = [NSString stringWithFormat:@"%.f",(distance*0.001)];
                        if ([dist intValue]<1) {
                            dist = [NSString stringWithFormat:@"%.02f",(distance*0.001)];
                        }
                        _markerFinish.title = [NSString stringWithFormat:LocalizedStr(@"Map.Drive.Marker.ETS.text"),[dist intValue]];
                        _markerFinish.icon = [Utils imageFromInfoView:_markerFinish.title image:[Utils image:[UIImage imageNamed:@"icon_pin_end"] scaledToSize:CGSizeMake(kMarkerSizeW, kMarkerSizeH)] color:UIColorDefault2];
                    }
                    else {
                        _markerFinish.icon = [Utils image:[UIImage imageNamed:@"icon_pin_end"] scaledToSize:CGSizeMake(kMarkerSizeW, kMarkerSizeH)];
                    }
                    //_markerFinish.groundAnchor = CGPointMake(0.5f, 0.5f);
                    _markerFinish.flat = YES;
                    _markerFinish.tappable = NO;
                    _markerFinish.position = ecoordinate;
                    _markerFinish.map = _mapView;
                    
                    //_markers = [[NSArray alloc] initWithObjects:_markerStart, _markerFinish, nil];
                    [__coords addObject:[NSValue valueWithMKCoordinate:_markerStart.position]];
                    [__coords addObject:[NSValue valueWithMKCoordinate:_markerFinish.position]];
                }
            }
            else {
                //_markers = [[NSArray alloc] initWithObjects:_markerStart, nil];
                [__coords addObject:[NSValue valueWithMKCoordinate:_markerCar.position]];
                [__coords addObject:[NSValue valueWithMKCoordinate:_markerStart.position]];
            }
            _markerStart.map = _mapView;
            if (isFirst) {
                if (__coords) {
                    [self updateMapFitBounds:__coords];
                }
                else {
                    [self updateMapFitBoundsMarker:_markers];
                }
                isFirst = NO;
            }
            
        } else {
            //에러주석[self alertError:@"Error" message:descriptor.error];
        }
    }
    [self hideBusyIndicator];
}

@end
