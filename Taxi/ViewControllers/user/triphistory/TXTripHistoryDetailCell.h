//
//  TXPaymentCell.h
//  TXPaymentCell
//
//  Created by __MyCompanyName__ on 14. 11. 18..
//  Copyright (c) 2014년 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RTLabel.h"
#import "RTUILabel.h"

@interface TXTripHistoryDetailCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UIImageView *profile;
@property (nonatomic, strong) IBOutlet RTUILabel *lb1;
@property (nonatomic, strong) IBOutlet RTUILabel *lb2;
@property (nonatomic, strong) IBOutlet RTUILabel *lb3;
@property (nonatomic, strong) IBOutlet RTUILabel *lb4;
@property (nonatomic, strong) IBOutlet RTUILabel *lb5;
@property (nonatomic, strong) IBOutlet RTUILabel *lb6;
@property (nonatomic, strong) IBOutlet RTUILabel *lb7;
@property (nonatomic, strong) IBOutlet RTUILabel *lb8;
@property (nonatomic, strong) IBOutlet UILabel *lb9;
@property (nonatomic, strong) IBOutlet UILabel *lb10;
@property (nonatomic, strong) IBOutlet UIImageView *addrLine;

@property (nonatomic, strong) IBOutlet UIButton *btnProfile;
@property (nonatomic, strong) IBOutlet UIButton *btnHelp;

@property (nonatomic, strong) IBOutlet UIButton *btnCall;
@property (nonatomic, strong) IBOutlet UIButton *btnChat;
@property (nonatomic, strong) IBOutlet UIButton *btnCharge;

- (NSDictionary*)bindData:(NSDictionary *)dic type:(BOOL)isShare;

@end
