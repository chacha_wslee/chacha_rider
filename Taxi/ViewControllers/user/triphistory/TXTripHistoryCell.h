//
//  TXPaymentCell.h
//  TXPaymentCell
//
//  Created by __MyCompanyName__ on 14. 11. 18..
//  Copyright (c) 2014년 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RTLabel.h"
#import "RTUILabel.h"

@interface TXTripHistoryCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UIImageView *profile;
@property (nonatomic, strong) IBOutlet UILabel *lb1;
@property (nonatomic, strong) IBOutlet UILabel *lb2;
@property (nonatomic, strong) IBOutlet UILabel *lb3;
@property (nonatomic, strong) IBOutlet UILabel *lb4;
@property (nonatomic, strong) IBOutlet UILabel *lb5;
@property (nonatomic, strong) IBOutlet UILabel *lb6;
@property (nonatomic, strong) IBOutlet UILabel *lb7;
@property (nonatomic, strong) IBOutlet UIImageView *addrLine;

@property (nonatomic, strong) IBOutlet UIButton *btnProfile;
@property (nonatomic, strong) IBOutlet UIButton *btnCharge;

- (NSDictionary*)bindData:(NSDictionary *)dic type:(BOOL)isShare;

@end
