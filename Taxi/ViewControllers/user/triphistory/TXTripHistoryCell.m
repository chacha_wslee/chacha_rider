//
//  TXPaymentCell.m
//  TXPaymentCell
//
//  Created by __MyCompanyName__ on 14. 11. 18..
//  Copyright (c) 2014년 __MyCompanyName__. All rights reserved.
//

#import "TXAppDelegate.h"
#import "TXTripHistoryCell.h"
#import "utils.h"

@implementation TXTripHistoryCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
    }
    return self;
}

- (NSDictionary*)bindData:(NSDictionary *)dic type:(BOOL)isShare
{
    TXAppDelegate *appDelegate = (TXAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    BOOL isFinish = NO;
    NSDictionary* drive_charge  = nil;
    NSDictionary* drive_trip    = nil;
    NSDictionary* drive_tip     = nil;
    NSDictionary* drive_pickup  = nil;
    NSDictionary* pvehicle      = nil;
    NSDictionary* user          = nil;
    NSDictionary* drive_payout  = nil;
    NSDictionary *pMap          = nil;
    
    //    self.lb1.textColor = HEXCOLOR(0x999999FF);
    //    self.lb1.textColor = HEXCOLOR(0x999999FF);
    //    self.lb1.textColor = HEXCOLOR(0x999999FF);
    //    self.lb1.textColor = HEXCOLOR(0x999999FF);
    
    //self.lb6.lineBreakMode = kCTLineBreakByClipping;
    //self.lb6.numberOfLines = 1;
    //self.lb7.lineBreakMode = kCTLineBreakByClipping;
    //self.lb7.numberOfLines = 1;
    
    // driver
    if (appDelegate.isDriverMode) {
        drive_tip     = [dic valueForKey:@"drive_tip"]; //tip_payed
        drive_charge  = [dic valueForKey:@"drive_charge"]; //fare_rate, pay_fare, pay_tip, fare_extra
        drive_trip    = [dic valueForKey:@"drive_trip"]; //trip_duration/trip_distance, begin_location, end_location, update_date
        drive_pickup  = [dic valueForKey:@"drive_pickup"];
        //provider      = [dic valueForKey:@"provider"]; // name, picture, rate, trips
        pvehicle      = [dic valueForKey:@"drive_driver"]; // model, plateno
        user          = [dic valueForKey:@"drive_rider"]; // name, picture, rate, trips
        drive_payout  = [dic valueForKey:@"drive_payout"]; // total_payout
        
        pMap = @{
                 @"useq" : [user valueForKey:@"useq"],
                 @"picture" : [Utils nullToIntString:user[@"picture"]],
                 };
        // 취소요금0일때는 요금보기 버튼 감추기
        _btnCharge.hidden = YES;
    }
    else {
        drive_tip     = [dic valueForKey:@"drive_tip"]; //tip_payed
        drive_charge  = [dic valueForKey:@"drive_charge"]; //fare_rate, pay_fare, pay_tip, fare_extra
        drive_trip    = [dic valueForKey:@"drive_trip"]; //trip_duration/trip_distance, begin_location, end_location, update_date
        drive_pickup  = [dic valueForKey:@"drive_pickup"];
        user          = [dic valueForKey:@"drive_driver"]; // name, picture, rate, trips
        pvehicle      = [dic valueForKey:@"drive_driver"]; // model, plateno
        //user          = [dic valueForKey:@"user"]; // name, picture, rate, trips
        
        pMap = @{
                 @"pseq" : [user valueForKey:@"pseq"],
                 @"picture" : [Utils nullToIntString:user[@"picture"]],
                 };
        _btnCharge.hidden = NO;
    }
    
    NSString *lb1 = @"";
    NSString *lb2 = @"";
    NSString *lb4 = @"";
    NSString *lb6 = @"";
    
    NSString *tip = @"";
    NSString *payed = @""; // 과금된 금액 운전자/승객이 보는 금액은 서로 다르다.
    BOOL isCancelPay = NO; // 취소수수료 발생여부
    
    // 과금여부
    if ([Utils isDictionary:drive_charge]) {
        //payed = (appDelegate.isDriverMode?NUMBER_FORMAT([drive_charge valueForKey:@"fare_trip"]):NUMBER_FORMAT([drive_charge valueForKey:@"fare_payed"]));
        if (appDelegate.isDriverMode) {
            if ([Utils isDictionary:drive_payout]) {
                payed = NUMBER_FORMAT([drive_payout valueForKey:@"total_payout"]);
            }
            else {
                payed = NUMBER_FORMAT(0);
            }
        }
        else {
            payed = NUMBER_FORMAT([drive_charge valueForKey:@"fare_payed"]);
        }
        
        if (![payed intValue]) {
            payed = @"";
        }
        // 추가요금(드라이버)
        if (appDelegate.isDriverMode) {
            tip = NUMBER_FORMAT([drive_charge valueForKey:@"fare_extra"]);
            if (![tip intValue]) {
                tip = @"";
            }
            else {
                tip = [NSString stringWithFormat:@"%d", [tip intValue]];
            }
        }
        //        tip = [drive_charge valueForKey:@"tip_payed"];
        //        if (![tip intValue]) {
        //            tip = @"";
        //        }
        //        else {
        //            tip = [NSString stringWithFormat:@"%d", [tip intValue]];
        //        }
        
        if ([[drive_charge valueForKey:@"fare_discount"] intValue]>0) {
            lb4 = [NSString stringWithFormat:@"%@",LocalizedStr(@"Menu.TripHistory.Trip.Discount")];
            //[self.lb4 setTextColor:HEXCOLOR(0xd0021bFF)]; // 붉
            [self.lb4 setTextColor:UIColorDefault]; // 기본
        }
        
    }
    payed = [Utils numberCheck:payed];
    // 팁(라이더)
    if ([Utils isDictionary:drive_tip] && !appDelegate.isDriverMode) {
        tip = NUMBER_FORMAT([drive_tip valueForKey:@"tip_payed"]);
        if (![tip intValue]) {
            tip = @"";
        }
        else {
            tip = [NSString stringWithFormat:@"%d", [tip intValue]];
        }
    }
    tip = [Utils numberCheck:tip];
    // pickup 취소
    if ([Utils isDictionary:drive_pickup]) {
        lb1 = [NSString stringWithFormat:@"%@",[drive_pickup valueForKey:@"reg_date"]];
        // 취소
        if ([drive_pickup[@"state"] intValue]<0) {
            isCancelPay = YES;
        }
        else {
            isCancelPay = NO;
        }
    }
    
    // trip 취소
    if ([Utils isDictionary:drive_trip]) {
        lb1 = [NSString stringWithFormat:@"%@",[drive_trip valueForKey:@"reg_date"]];
        // 취소
        if ([drive_trip[@"state"] intValue]<0) {
            isCancelPay = YES;
        }
        else {
            isCancelPay = NO;
        }
    }
    
    if (![payed isEqualToString:@""] || [Utils isDictionary:drive_charge]){
        if (isShare) { // 공유는 금액이 나오지 않는다.
            lb2 = [NSString stringWithFormat:@"%d%@/%d%@",
                   [[drive_trip valueForKey:@"trip_duration"] intValue] + 1,
                   LocalizedStr(@"String.Minute"),
                   [[drive_trip valueForKey:@"trip_distance"] intValue],
                   LocalizedStr(@"String.Killo")
                   ];
        }
        else {
            lb2 = [NSString stringWithFormat:@"%d%@/%d%@",
                   [[drive_trip valueForKey:@"trip_duration"] intValue] + 1,
                   LocalizedStr(@"String.Minute"),
                   [[drive_trip valueForKey:@"trip_distance"] intValue],
                   LocalizedStr(@"String.Killo")
                   ];
            lb6 = CURRENCY_FORMAT(payed);
        }
        isFinish = YES;
        
    }
    else {
        if (isCancelPay) {
            if (isShare) { // 공유는 금액이 나오지 않는다.
                lb4 = [NSString stringWithFormat:@"%@",
                       LocalizedStr(@"Menu.ShareTripHistory.Trip.Cancel")];
                [self.lb4 setTextColor:HEXCOLOR(0xd0021bFF)]; // 붉
                //[self.lb4 setTextColor:HEXCOLOR(0xd0021bFF)]; // 기본
            }
            else {
                lb4 = [NSString stringWithFormat:@"%@",
                       LocalizedStr(@"Menu.TripHistory.Trip.Cancel")
                       ];
                lb6 = CURRENCY_FORMAT(@"0");
                [self.lb4 setTextColor:HEXCOLOR(0xd0021bFF)]; // 붉
                //[self.lb4 setTextColor:HEXCOLOR(0xd0021bFF)]; // 기본
                // 취소요금0일때는 요금보기 버튼 감추기
                _btnCharge.hidden = YES;
            }
            isFinish = YES;
            
        }
        else {
            if (isShare) { // 공유는 금액이 나오지 않는다.
                if ([Utils isDictionary:drive_trip]) {
                    if ([drive_trip[@"state"] isEqualToString:@"10"]) {
                        lb4 = [NSString stringWithFormat:@"%@",
                               LocalizedStr(@"Menu.TripHistory.Trip.On")];
                        //[self.lb4 setTextColor:HEXCOLOR(0xd0021bFF)]; // 붉
                        [self.lb4 setTextColor:UIColorDefault]; // 기본
                    }
                }
                else {
                    if ([drive_pickup[@"state"] isEqualToString:@"10"]) {
                        lb4 = [NSString stringWithFormat:@"%@",
                               LocalizedStr(@"Menu.TripHistory.Pickup.On")];
                        //[self.lb4 setTextColor:HEXCOLOR(0xd0021bFF)]; // 붉
                        [self.lb4 setTextColor:UIColorDefault]; // 기본
                    }
                    else {
                        lb4 = [NSString stringWithFormat:@"%@",
                               LocalizedStr(@"Menu.TripHistory.Arrived.On")];
                        //[self.lb4 setTextColor:HEXCOLOR(0xd0021bFF)]; // 붉
                        [self.lb4 setTextColor:UIColorDefault]; // 기본
                    }
                }
            }
            else {
                if ([Utils isDictionary:drive_trip]) {
                    if ([drive_trip[@"state"] isEqualToString:@"10"]) {
                        lb4 = [NSString stringWithFormat:@"%@",
                               LocalizedStr(@"Menu.TripHistory.Trip.On")
                               ];
                        lb6 = CURRENCY_FORMAT(@"0");
                        //[self.lb4 setTextColor:HEXCOLOR(0xd0021bFF)]; // 붉
                        [self.lb4 setTextColor:UIColorDefault]; // 기본
                        // 취소요금0일때는 요금보기 버튼 감추기
                        _btnCharge.hidden = YES;
                    }
                }
                else {
                    if ([drive_pickup[@"state"] isEqualToString:@"10"]) {
                        lb4 = [NSString stringWithFormat:@"%@",
                               LocalizedStr(@"Menu.TripHistory.Pickup.On")
                               ];
                        lb6 = CURRENCY_FORMAT(@"0");
                        //[self.lb4 setTextColor:HEXCOLOR(0xd0021bFF)]; // 붉
                        [self.lb4 setTextColor:UIColorDefault]; // 기본
                        // 취소요금0일때는 요금보기 버튼 감추기
                        _btnCharge.hidden = YES;
                    }
                    else {
                        lb4 = [NSString stringWithFormat:@"%@",
                               LocalizedStr(@"Menu.TripHistory.Arrived.On")
                               ];
                        lb6 = CURRENCY_FORMAT(@"0");
                        //[self.lb4 setTextColor:HEXCOLOR(0xd0021bFF)]; // 붉
                        [self.lb4 setTextColor:UIColorDefault]; // 기본
                        // 취소요금0일때는 요금보기 버튼 감추기
                        _btnCharge.hidden = YES;
                    }
                }
            }
            
        }
    }
    
    NSString* _name = [Utils nameToString:[user objectForKey:@"name"]];
    //    NSString *lb3 = [NSString stringWithFormat:@"%@ %@",
    //                     _name,
    //                     FONT_STR_COLOR([Utils labelStringUserInfo:[user valueForKey:@"trips"] rate:[user valueForKey:@"rate"]])
    //                     ];
    NSString *lb3 = [NSString stringWithFormat:@"%@ %@",
                     _name,
                     @""
                     ];
    
    
    if (![payed isEqualToString:@""] || [Utils isDictionary:drive_charge]){
        if (isCancelPay) {
            lb4 = [NSString stringWithFormat:@"%@",LocalizedStr(@"Menu.TripHistory.Trip.Cancel.Fee")];
            //[self.lb4 setTextColor:HEXCOLOR(0xd0021bFF)]; // 붉
            [self.lb4 setTextColor:UIColorDefault]; // 기본
        }
        else {
            if (![tip isEqualToString:@""]){
                /*
                if (appDelegate.isDriverMode) {
                    lb4 = [NSString stringWithFormat:@"EXTRA %@",CURRENCY_FORMAT(tip)];
                }
                else {
                    lb4 = [NSString stringWithFormat:@"TIP %@",CURRENCY_FORMAT(tip)];
                }
                */
            }
        }
    }
    
    NSString *lb5 = [NSString stringWithFormat:@"%@ %@",
                     [pvehicle valueForKey:@"vehiclemodel"],
                     [pvehicle valueForKey:@"vehicleno"]];
    
    if (appDelegate.isDriverMode) {
        lb5  = @"";
    }
    
    if (isShare) {
        lb4 = @"";
    }
    
    self.lb3.text = [Utils nullToStringNoTrim:lb3];
    self.lb5.text = [Utils nullToStringNoTrim:lb5];
    self.lb1.text = [Utils nullToStringNoTrim:lb1];
    
    self.lb4.text = [Utils nullToStringNoTrim:lb4];
    self.lb2.text = [Utils nullToStringNoTrim:lb2];
    
    self.lb6.text = [Utils nullToStringNoTrim:lb6];
    
    [self.lb3 setTextColor:HEXCOLOR(0x333333FF)];
    [self.lb5 setTextColor:HEXCOLOR(0x4a4a4aFF)];
    [self.lb1 setTextColor:HEXCOLOR(0x4a4a4aFF)];
    
    //[self.lb4 setTextColor:HEXCOLOR(0xd0021bFF)]; // 붉
    //[self.lb4 setTextColor:HEXCOLOR(0xd0021bFF)]; // 기본
    
    [self.lb2 setTextColor:HEXCOLOR(0x111111FF)];
    [self.lb6 setTextColor:HEXCOLOR(0x111111FF)];
    
    if (appDelegate.isDriverMode) {
        pMap = @{
                 @"useq" : pMap[@"useq"],
                 @"picture" : pMap[@"picture"],
                 @"finish" : [NSString stringWithFormat:@"%d",isFinish]
                 };
    }
    else {
        pMap = @{
                 @"pseq" : pMap[@"pseq"],
                 @"picture" : pMap[@"picture"],
                 @"finish" : [NSString stringWithFormat:@"%d",isFinish]
                 };
    }
    
    return pMap;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

-(void)stateChanged
{
    
}

@end
