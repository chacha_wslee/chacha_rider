//
//  TXLauncherVC.m

//

#import "TXLauncherVC.h"
#import "TXMainVC.h"
#import "TXUserModel.h"
#import "TXSharedObj.h"
#import "utils.h"
//#import "TXSignUpVC.h"
#import "TXSignInVC.h"

#ifdef _WITZM
#import "TXSnsLoginVC.h"
#elif defined _CHACHA
#import "TXSnsLoginVC.h"
#endif
//#import "RedisSingleton.h"

#import "TXMapVC.h"
#ifdef _DRIVER_MODE
    #import "TXAboutVC.h"
#else
    //#import "TXGuideVC.h"
#endif
//#import "TXSubscriptionVC.h"
//#import "TXAskCardNumberVC.h"
#import "TXAppDelegate.h"
#import "LGAlertView.h"
#import "UIImageView+AnimationCompletion.h"

@interface TXLauncherVC () {
    BOOL isLoading;
    CLLocationManager *locationMgr;
    NSArray *_msgList;
    
    BOOL isFecthLocation;
    id observer1;
    
    LGAlertView* alertView;
    
    NSMutableArray *animationImages1;
    NSMutableArray *animationImages2;
    NSInteger playCount;
    
    TXSignInVC *signInVC;
    
    BOOL isGuideShow;
}

@property (retain, nonatomic) IBOutlet UIImageView    *splash_background;
@property (retain, nonatomic) IBOutlet UIImageView    *splash_text;

@property (retain, nonatomic) IBOutlet UIView    *symbolView;
@property (retain, nonatomic) IBOutlet UIImageView    *symbolImage;
@property (retain, nonatomic) IBOutlet UIImageView    *aniImage;

@property (retain, nonatomic) IBOutlet UILabel *lbDesc;

-(IBAction)signIn:(id)sender;
-(IBAction)signUp:(id)sender;

@end

@implementation TXLauncherVC

NSTimer *sliderTimer;

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    
    if(self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        self->isLoading = YES;
        self->isFecthLocation = NO;
        self->alertView = nil;
        self->isGuideShow = NO;
    }
    
    return self;
}

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil flag:(BOOL)flag {
    
    if(self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        self->isLoading = flag;
        self->isFecthLocation = NO;
        self->alertView = nil;
        self->isGuideShow = NO;
    }
    
    return self;
}

-(id)initWithFlag:(BOOL)flag {
    
    self = [self init];
    if (self) {
        self->isLoading = flag;
        self->isFecthLocation = NO;
        self->isGuideShow = NO;
    }
    return self;

}

#pragma mark - UIViewController

-(void)configureStyles {
    [super configureStyles];
    [self configureFieldStyles];
}

-(void) configure {

    [super configure];
    
    [appDelegate resetTripInfo];
    
    [appDelegate startLocationUpdateSubscription];
    
    if (![INTULocationManager locationServicesState]) {
        self->isFecthLocation = YES;
    }
    
    // 2. init event
    _msgList = @[
                 @[@"UR110",@""], //
                 @[@"UR114",@""], //
                 @[@"UR200",@""], //
//                 @[@"UR601",@""], //
                 
                 @[@"PP110",@""], //
                 @[@"PP200",@""], //
                 //@[@"PP602",@""], //
                 
                 @[@"PQ200",@""], //
                 @[@"PQ300",@""], //
                 ];
    
#ifdef _DRIVER_MODE
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:VIEWTUTORIAL];
#endif

    
#ifdef _CHACHA_0220
    //    self.lbDesc.textColor=UIColorLabelTextColor;
    //[self.lbDesc setText:LocalizedStr(@"Launch.logo.text")];
    [self.splash_background setBackgroundColor:UIColorLauncher];
    // Load launch image
    [self.splash_background setImage:[self launchImage]];

#else
    //    self.lbDesc.textColor=UIColorLabelTextColor;
    //[self.lbDesc setText:LocalizedStr(@"Launch.logo.text")];
    [self.splash_background setBackgroundColor:UIColorLauncher];
    // Load launch image
    [self.splash_background setImage:[self launchImage]];

#endif
    
}

- (UIImage*)launchImage
{
    UIImage* launchImage = nil;
    NSString *launchImageName;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        if ([UIScreen mainScreen].bounds.size.height == 480) launchImageName = @"LaunchImage-700@2x.png"; // iPhone 4/4s, 3.5 inch screen
        if ([UIScreen mainScreen].bounds.size.height == 568) launchImageName = @"LaunchImage-700-568h@2x.png"; // iPhone 5/5s, 4.0 inch screen
        if ([UIScreen mainScreen].bounds.size.height == 667) launchImageName = @"LaunchImage-800-667h@2x.png"; // iPhone 6, 4.7 inch screen
        if ([UIScreen mainScreen].bounds.size.height >= 736) launchImageName = @"LaunchImage-800-Portrait-736h@3x.png"; // iPhone 6+, 5.5 inch screen
    }
    else if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        if ([UIScreen mainScreen].scale == 1) launchImageName = @"LaunchImage.png"; // iPad 2
        if ([UIScreen mainScreen].scale == 2) launchImageName = @"LaunchImage@2x.png"; // Retina iPads
    }
    
    launchImage = [UIImage imageNamed:launchImageName];
    
    NSArray *allPngImageNames = [[NSBundle mainBundle] pathsForResourcesOfType:@"png"
                                                                   inDirectory:nil];
    
    for (NSString *imgName in allPngImageNames){
        // Find launch images
        if ([imgName containsString:@"LaunchImage"]){
            launchImage = [UIImage imageNamed:launchImageName];
            // Has image same scale and dimensions as our current device's screen?
            if (launchImage.scale == [UIScreen mainScreen].scale && CGSizeEqualToSize(launchImage.size, [UIScreen mainScreen].bounds.size)) {
                NSLog(@"Found launch image for current device %@", launchImage.description);
                break;
            }
        }
    }
    
    return launchImage;
}

-(void)nextStep
{
    if (signInVC != nil) {
        return;
    }
    
    //NSLog(@"debug");
    if (!appDelegate.isNetwork) {
        if (self->alertView) {
            return;
        }
        /*
        self->alertView = [[LGAlertView alloc] initWithTitle:@"Error"
                                                            message:LocalizedStr(@"String.Network.Errro.text")
                                                              style:LGAlertViewStyleAlert
                                                       buttonTitles:@[LocalizedStr(@"Button.OK")]
                                                  cancelButtonTitle:nil
                                             destructiveButtonTitle:nil
                                                      actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                                                          //exit(0);
                                                      }
                                                      cancelHandler:^(LGAlertView *alertView) {
                                                      }
                                                 destructiveHandler:^(LGAlertView *alertView) {
                                                 }];
        [Utils initAlertButtonColor:self->alertView];
        [self->alertView showAnimated:YES completionHandler:nil];
        
        [super backroundView].hidden = YES;
        self.view.backgroundColor = UIColorDarkButton;
        */
        return;
    }
    if (!self->isLoading) {
        [self registerEventListeners];
        //NSLog(@"debug");

        if (self->isFecthLocation) {
            if (!appDelegate.isDriverMode) {
                //NSLog(@"---------------------------------1");
                [self onSignInByRider:nil];
            }
            else {
                [self onSignInByDriver:nil];
            }
        }
        else {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
                
                //[self showBusyIndicator:@"Authenticating ... "];
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 3 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    [self hideBusyIndicator];
                    
                    if (!self->isFecthLocation) {
                        [appDelegate onCommonErrorAlert:LocalizedStr(@"Intro.GPS.Error.text")];
                    }
                    else {
                        if (!appDelegate.isDriverMode) {
                            //NSLog(@"---------------------------------2");
                            [self onSignInByRider:nil];
                        }
                        else {
                            [self onSignInByDriver:nil];
                        }
                    }
                });
                
            });
        }

    }
//    self->isLoading = YES;
    [super backroundView].hidden = YES;
#ifdef _DRIVER_MODE
    self.view.backgroundColor = UIColorDarkButton;
#else
    self.view.backgroundColor = HEXCOLOR(0xEAEAEAFF);
#endif
    
    self->alertView = nil;
}

-(void) registerEventListeners {
    for (NSArray *msg in _msgList) {
        [self->model addEventListener:self forEvent:[msg objectAtIndex:0] eventParams:nil];
    }
}

-(void) removeEventListeners {
    for (NSArray *msg in _msgList) {
        [self->model removeEventListener:self forEvent:[msg objectAtIndex:0]];
    }
    
    [[NSNotificationCenter defaultCenter] removeObserver:observer1];
    observer1 = nil;
}

-(void) initControls {
    
    if (signInVC != nil) {
        return;
    }
    
    if (![Utils getIsFirstLogin]) {
        [self performSelector:@selector(signIn:) withObject:nil afterDelay:0.1];
    }
    else {
        [self performSelector:@selector(signUp:) withObject:nil afterDelay:0.1];
    }
}

- (void)updateText:(NSInteger)idx {
#ifdef _DRIVER_MODE
    int size = 29;
    if ([UIScreen mainScreen].bounds.size.height == 480) size = 21;
    else if ([UIScreen mainScreen].bounds.size.height == 568) size = 23;
    else if ([UIScreen mainScreen].bounds.size.height == 667) size = 28;
    else if ([UIScreen mainScreen].bounds.size.height == 736) size = 29;

    text11.text = [NSString stringWithFormat:@"<center><font size=%d color='#774192'><b>%@</b></font></center><font size=8>\n\n</font><font size=%d color='#774192'>%@</font>",
                   size,
                   [[_scrollLabel objectAtIndex:idx] objectAtIndex:0],
                   size,
                   [[_scrollLabel2 objectAtIndex:idx] objectAtIndex:0]];
#else
    text1.text=[[_scrollLabel objectAtIndex:idx] objectAtIndex:0];
    text2.text=[[_scrollLabel2 objectAtIndex:idx] objectAtIndex:0];
#endif
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"load view");
    self.btnSignIn.alpha = 0.0;
    playCount = 0;
    signInVC = nil;
    appDelegate.signupViewStep = 9;
    
#ifdef _DRIVER_MODE
    text1.hidden = YES;
    text2.hidden = YES;
#else
    text11.hidden = YES;
#endif
    
#ifdef _DRIVER_MODE
    self->isGuideShow = YES;
#endif

#ifdef _CHACHA_0220
#ifdef _DRIVER_MODE
    if (!self->isLoading) {
        _symbolView.frame = self.view.frame;
        [self.view addSubview:_symbolView];
        
        _lbDesc.alpha = 0.0f;
        _symbolImage.alpha = 0.0f;
        // Load images
        @autoreleasepool {
            // symbol logo
            [UIView animateWithDuration:0.5 animations:^{
                //                _lbDesc.alpha = 1.0f;
                //                _symbolImage.alpha = 1.0f;
            }];
        }
    }
    else {
        [_symbolView removeFromSuperview];
    }
#else
    _symbolView.frame = self.view.frame;
    [self.view addSubview:_symbolView];
    
    _lbDesc.alpha = 0.0f;
    _symbolImage.alpha = 0.0f;
#endif
#else
    if (!self->isLoading) {
        _symbolView.frame = self.view.frame;
        [self.view addSubview:_symbolView];
        
        _lbDesc.alpha = 0.0f;
        _symbolImage.alpha = 0.0f;
        // Load images
        @autoreleasepool {
            // symbol logo
            [UIView animateWithDuration:0.5 animations:^{
                //                _lbDesc.alpha = 1.0f;
                //                _symbolImage.alpha = 1.0f;
            }];
        }
    }
    else {
        [_symbolView removeFromSuperview];
    }
#endif
    


    [self setNeedsStatusBarAppearanceUpdate];
    
    [self.view setBackgroundColor:UIColorDefaultBG];
    
    //---------------------------------------------------------------------------------------------------------------------------------------------
    observer1 = [[NSNotificationCenter defaultCenter] addObserverForName:@"NotificationNetworkReachableReceived"
                                                      object:nil
                                                       queue:[NSOperationQueue mainQueue]
                                                  usingBlock:^(NSNotification *notification) {
                                                      //NSLog(@"debug");
                                                      
                                                      if (self->isGuideShow) {
                                                          [self nextStep];
                                                      }
                                                      
                                                  }];
    // change login view
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(signInFromNoti:) name:NOTIFICATION_ENTER_LOGINVIEW object:nil];
    
////#if !TARGET_IPHONE_SIMULATOR
//    if ([CLLocationManager locationServicesEnabled]) {
//
//        [self startUpdatingLocation];
//    }
//    else {
//        [appDelegate onCommonErrorAlert:LocalizedStr(@"Intro.GPS.Error.text")];
//    }
////#endif
    
    [appDelegate startSingleLocationRequest];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self configureFieldStyles];
    
    appDelegate.signupViewStep = 9;
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [super statusBarStyleLauncher];
    
    [[UIApplication sharedApplication] setStatusBarHidden:YES
                                            withAnimation:UIStatusBarAnimationFade];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    // 로그인된 경우 바로 다음 step으로 넘어간다.
    if (!self->isLoading) {
        _lbDesc.alpha = 0.0f;
        _symbolImage.alpha = 0.0f;
        _symbolImage.hidden = NO;
        
        // symbol logo
        [UIView animateWithDuration:2.0 animations:^{
//            _lbDesc.alpha = 1.0f;
//            _symbolImage.alpha = 1.0f;
        }];
        
#ifdef _CHACHA_0220
#ifdef _DRIVER_MODE
        [self nextStep];
#else
        // Load images
        @autoreleasepool {
            animationImages1 = [[NSMutableArray alloc] init];
            for (int i = 1; i <= 96; i++) {
                NSString *imageNames = [NSString stringWithFormat:@"loading_%02d", i];
                [animationImages1 addObject:[UIImage imageNamed:imageNames]];
            }
            
            self.aniImage.hidden = NO;
            [self.splash_background setImage:[UIImage imageNamed:@"loading_96"]];
            self.aniImage.animationImages = animationImages1;
            self.aniImage.animationDuration = 3.0f;
            self.aniImage.animationRepeatCount = 1;
            [self.aniImage startAnimatingWithCompletionBlock:^(BOOL success) {
                self->isGuideShow = YES;
                
                [self nextStep];
            }];
        }
#endif
#else
        [self nextStep];
#endif
        
        return;
    }
    else {
#ifdef _CHACHA_0220
#ifdef _DRIVER_MODE
#else
        // Load images
        @autoreleasepool {
            animationImages1 = [[NSMutableArray alloc] init];
            for (int i = 1; i <= 96; i++) {
                NSString *imageNames = [NSString stringWithFormat:@"loading_%02d", i];
                [animationImages1 addObject:[UIImage imageNamed:imageNames]];
            }
            
            self.aniImage.hidden = NO;
            [self.splash_background setImage:[UIImage imageNamed:@"loading_96"]];
            self.aniImage.animationImages = animationImages1;
            self.aniImage.animationDuration = 3.0f;
            self.aniImage.animationRepeatCount = 1;
            [self.aniImage startAnimatingWithCompletionBlock:^(BOOL success) {
                self->isGuideShow = YES;
                
                [_symbolView removeFromSuperview];
            }];
        }
#endif
#endif
    }
    
#ifdef _DRIVER_MODE
    _scrollLabel= @[@[LocalizedStr(@"Intro.Driver.Picture.text1")],
                    @[LocalizedStr(@"Intro.Driver.Picture.text2")],
                    @[LocalizedStr(@"Intro.Driver.Picture.text3")],
                    @[LocalizedStr(@"Intro.Driver.Picture.text4")],
                    ];
    
    _scrollLabel2= @[@[LocalizedStr(@"Intro.Driver.Picture.text1.subtext")],
                     @[LocalizedStr(@"Intro.Driver.Picture.text2.subtext")],
                     @[LocalizedStr(@"Intro.Driver.Picture.text3.subtext")],
                     @[LocalizedStr(@"Intro.Driver.Picture.text4.subtext")],
                     ];
#else
    _scrollLabel= @[@[LocalizedStr(@"Intro.Rider.Picture.text1")],
                    @[LocalizedStr(@"Intro.Rider.Picture.text2")],
                    @[LocalizedStr(@"Intro.Rider.Picture.text3")],
                    ];
    
    _scrollLabel2= @[@[LocalizedStr(@"Intro.Rider.Picture.text1.subtext")],
                     @[LocalizedStr(@"Intro.Rider.Picture.text2.subtext")],
                     @[LocalizedStr(@"Intro.Rider.Picture.text3.subtext")],
                     ];
#endif
    

#ifdef _DRIVER_MODE
    text11.textColor=UIColorLabelTextColor;
    [text11 setFont:[UIFont systemFontOfSize:11]];
    CGRect rect = pageControl.frame;
    rect.origin.y = text11.frame.origin.y + text11.frame.size.height ;
    pageControl.frame = rect;
#else
    text1.textColor=UIColorLabelTextColor;
    text2.textColor=UIColorLabelTextColor;
#endif
    
    [self updateText:0];
    
    //init scollview
    scrollView.delegate = self;
    scrollView.pagingEnabled = YES;
    
    //[self.view layoutIfNeeded];
    
    NSMutableArray *viewArray = [[NSMutableArray alloc] init];
    int scrollCount = (int)[_scrollLabel count];
    for (int i = 0; i < scrollCount; i++) {
#ifdef _DRIVER_MODE
        UIImage *img = [UIImage imageNamed:[NSString stringWithFormat:LocalizedStr(@"Intro.Driver.Picture.image.name"),(i+1)]];
#else
        UIImage *img = [UIImage imageNamed:[NSString stringWithFormat:LocalizedStr(@"Intro.Rider.Picture.image.name"),(i+1)]];
#endif
        
        NSData *tempArchive = [NSKeyedArchiver archivedDataWithRootObject:_introScrollView];
        UIView *aView = [NSKeyedUnarchiver unarchiveObjectWithData:tempArchive];
        
        UIImageView *imgView = (UIImageView*)[aView viewWithTag:4];
        [imgView setImage:img];
        [viewArray addObject:aView];
    }
    [self setObjects:viewArray];
    
    //Adjust
    for (int i = 0; i < [myCovers count]; i++) {
        CGRect frame = scrollView.frame;
        frame.origin.x = scrollView.frame.size.width * i;
        frame.origin.y = 0;
        //frame.size = scrollView.frame.size;
        frame.size.height = scrollView.frame.size.height - kBottomBottonSafeArea - btnstart1.frame.size.height;
        
        //View 1
        UIView *subview1 = [myCovers objectAtIndex:i];
        subview1.contentMode  = UIViewContentModeScaleAspectFit;
        subview1.frame = frame;
        subview1.autoresizingMask = scrollView.autoresizingMask;
        //        NSLog(@"subview1: %@", NSStringFromCGRect(subview1.frame));
        //[subview1 addSubview:[myCovers objectAtIndex:i]];
        [scrollView addSubview:subview1];
        
        //NSLog(@"frame: %@", NSStringFromCGRect(frame));
    }
    scrollView.contentSize = CGSizeMake(scrollView.frame.size.width*([myCovers count]), scrollView.frame.size.height/2);
    pageControl.numberOfPages = [myCovers count];
    
    pageControl.currentPageIndicatorTintColor = UIColorDefault;
    pageControl.pageIndicatorTintColor = [UIColor blackColor];
    
    // 자동 넘기기
    [self initScrollingTimer];
    
    [self.navigationController setToolbarHidden:YES animated:YES];
    
    // button
    btnstart.hidden = YES;
    [[btnstart1 titleLabel] setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:17]];
    [[btnstart2 titleLabel] setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:17]];
    
    [btnstart1 setTitle:LocalizedStr(@"Intro.Login.title") forState:UIControlStateNormal];
    [btnstart2 setTitle:LocalizedStr(@"Intro.SignUp.title") forState:UIControlStateNormal];
    
    [btnstart1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnstart1 setBackgroundColor:UIColorDefault];
    [btnstart2 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnstart2 setBackgroundColor:UIColorDefault];
    
    CGRect frame = btnstart1.frame;
    frame.origin.y = scrollView.frame.size.height - kBottomBottonSafeArea - frame.size.height;
    btnstart1.frame = frame;
    
    frame = btnstart2.frame;
    frame.origin.y = scrollView.frame.size.height - kBottomBottonSafeArea - frame.size.height;
    btnstart2.frame = frame;
}

-(void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    appDelegate.signupViewStep = 9;
    _symbolImage.hidden = YES;;
    _symbolImage.alpha = 0.0f;
    _lbDesc.alpha = 0.0f;

    [_symbolView removeFromSuperview];
    
    [scrollView scrollRectToVisible:CGRectMake(0, 0, scrollView.frame.size.width, scrollView.frame.size.height) animated:YES];
    
    pageControl.currentPage = 0;
    self->isLoading = YES;
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self removeEventListeners];
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:VIEWTUTORIAL];
    
    if (sliderTimer) {
        [sliderTimer invalidate];
        sliderTimer = nil;
    }
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(BOOL)prefersStatusBarHidden{
    return YES;
}

-(void) configureFieldStyles {
    
    //UIColor *bgColor = [UIColor blackColor];
    
    //self.view.backgroundColor = bgColor;
}
#pragma mark - GuideVC
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)initScrollingTimer {
    
    return;
    /*
    // enable timer after each 2 seconds for scrolling.
    if (sliderTimer) {
        [sliderTimer invalidate];
        sliderTimer = nil;
    }
    NSLog(@"start timer");
    sliderTimer = [NSTimer scheduledTimerWithTimeInterval:4 target:self selector:@selector(scrollingTimer) userInfo:nil repeats:YES];
     */
}
- (void)scrollingTimer {
    // access the scroll view with the tag
    UIScrollView *scrMain = (UIScrollView*) [self.view viewWithTag:1];
    // same way, access pagecontroll access
    UIPageControl *pgCtr = (UIPageControl*) [self.view viewWithTag:12];
    // get the current offset ( which page is being displayed )
    CGFloat contentOffset = scrMain.contentOffset.x;
    // calculate next page to display
    int nextPage = (int)(contentOffset/scrMain.frame.size.width) + 1 ;
    // if page is not 4, display it
    if( nextPage!=[myCovers count] )  {
        [scrMain scrollRectToVisible:CGRectMake(nextPage*scrMain.frame.size.width, 0, scrMain.frame.size.width, scrMain.frame.size.height) animated:YES];
        pgCtr.currentPage=nextPage;
        // else start sliding form 1 :)
    } else {
        [scrMain scrollRectToVisible:CGRectMake(0, 0, scrMain.frame.size.width, scrMain.frame.size.height) animated:YES];
        pgCtr.currentPage=0;
    }
    
    [self updateText:pgCtr.currentPage];
}


#pragma mark - Selectors
- (IBAction) onClick: (id) sender {
    
    UIButton *btn = (UIButton*)sender;
    if (btn.tag == 10) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else if (btn.tag == 11) {
        // login
        [self signIn:nil];
    }
    else if (btn.tag == 12) {
        // signup
        [self onTerms:nil];
    }
}


#pragma mark -
#pragma mark Params setting

- (void) setObjects:(NSArray *)covers {
    myCovers = [[NSArray alloc] initWithArray:covers];
}


#pragma mark -
#pragma mark Scrollview delegate

- (void)scrollViewDidEndDecelerating:(UIScrollView *)sender {
    CGFloat pageWidth = scrollView.frame.size.width;
    NSInteger offsetLooping = 1;
    int page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + offsetLooping;
    NSLog(@"page:%d/%lu %f",page,(unsigned long)[myCovers count], pageWidth);
    pageControl.currentPage = page % [myCovers count];
    
    [self updateText:page];
    
    [self initScrollingTimer];
}

#pragma mark - IBAction
-(IBAction)signIn:(id)sender {
    
    TXSnsLoginVC *snsLoginVC = [[TXSnsLoginVC alloc] initWithNibName:@"TXSnsLoginVC" bundle:nil];
    snsLoginVC.setupMode = 88;
    snsLoginVC.view.frame = [super view].frame;
    [self pushViewController:snsLoginVC];
    
}

-(IBAction)signInFromNoti:(id)sender {
    
    signInVC = [[TXSignInVC alloc] initWithNibName:@"TXSignInVC" bundle:nil mode:1];
    signInVC.view.frame = [super view].frame;
    [self pushViewController:signInVC];
    
}

-(IBAction)signUp:(id)sender {
    
//    signInVC = [[TXSignInVC alloc] initWithNibName:@"TXSignInVC" bundle:nil mode:0];
//    signInVC.view.frame = [super view].frame;
//    [self pushViewController:signInVC];
    
    [self onTerms:sender];
}

-(IBAction)onTerms:(id)sender {
    // 약관동의
    signInVC = [[TXSignInVC alloc] initWithNibName:@"TXSignInVC" bundle:nil mode:5];
    signInVC.view.frame = [super view].frame;
    [self pushViewController:signInVC];
}


-(void)onSignInByRider:(id)sender {
    
    TXSettings  *settings  = [[TXApp instance] getSettings];
    NSString *userUid   = [settings getUserId];
    NSString *userPwd = [settings getPassword];
    NSString *userTel = [settings getUserTelno];
    
    if ([[Utils nullToString:userUid] isEqualToString:@""]) {
        if (![Utils getIsFirstLogin]) {
            [self signIn:nil];
        }
        else {
            [self signUp:nil];
        }
        return;
    }
    if ([[Utils nullToString:userPwd] isEqualToString:@""]) {
        if (![Utils getIsFirstLogin]) {
            [self signIn:nil];
        }
        else {
            [self signUp:nil];
        }
        return;
    }
    if ([[Utils nullToString:userTel] isEqualToString:@""]) {
        if (![Utils getIsFirstLogin]) {
            [self signIn:nil];
        }
        else {
            [self signUp:nil];
        }
        return;
    }
    if ([Utils checkStateUIInfo:UI_STATE_UR110 oldvalue:@"UR110"] == UI_STATE_STEP_RUN) {
        return;
    }
    [Utils setStateUIInfo:UI_STATE_UR110 value:UI_STATE_STEP_RUN oldvalue:@"UR110"];
    
    TXUser *user = [[TXUser alloc] init];
    user.uid = userUid;
    user.pwd = userPwd;
    user.utelno = userTel;
    
    [self showBusyIndicator:@"Authenticating ... "];
    [self->model UR110:user];
}

-(void)onSignInByDriver:(id)sender {
    
    TXSettings  *settings  = [[TXApp instance] getSettings];
    NSString *userUid    = [settings getUserId];
    NSString *userUtelno = [settings getUserTelno];
    
    if ([[Utils nullToString:userUtelno] isEqualToString:@""]) {
        return;
    }
    if ([Utils checkStateUIInfo:UI_STATE_PP110 oldvalue:@"PP110"] == UI_STATE_STEP_RUN) {
        return;
    }
    [Utils setStateUIInfo:UI_STATE_PP110 value:UI_STATE_STEP_RUN oldvalue:@"PP110"];
    
    TXUser *user = [[TXUser alloc] init];
    user.uid = userUid;
    user.utelno = userUtelno;
    
    [self showBusyIndicator:@"Authenticating driver ... "];
    [self->model PP110:user];
}

-(BOOL)catchNetworkError:(TXResponseDescriptor *)descriptor {
    if(!descriptor.success && descriptor.rlt == -999999 && ![descriptor.error isEqualToString:TXEvents.HTTPREQUESTCOMPLETED]) {
        return YES;
    }
    return NO;
}

-(void)onUR200 {
    
    //[self showBusyIndicator:@"Requesting LogOut ... "];
    [self->model UR200];
}

-(void)onUR601 {
    
    //[self showBusyIndicator:@"Requesting LogOut ... "];
    [self->model UR601];
}

-(void)onUR114 {
    
    TXSettings* settings  = [[TXApp instance] getSettings];
    NSString *userUid   = [settings getUserId];
    NSString *userTelno = [settings getUserTelno];
    
    TXUser *user = [[TXUser alloc] init];
    user.uid = userUid;
    user.utelno = userTelno;
    
    //[self showBusyIndicator:@"Requesting LogOut ... "];
    [self->model UR114:user];
}


-(void)onPP200 {
    
    //[self showBusyIndicator:@"Requesting LogOut ... "];
    [self->model PP200];
}

// auto login
-(void)onEvent:(TXEvent *)event eventParams:(id)subscriptionParams {
    
    [self hideBusyIndicator];
    TXResponseDescriptor *descriptor = [event getEventProperty:TXEvents.Params.DESCRIPTOR];
    TXRequestObj *request = [event getEventProperty:TXEvents.Params.REQUEST];
    
    // 네트웍 에러 부분 처리 필요 __DEBUG
    if ([self catchNetworkError:descriptor]) {
//        [self alertError:@"Error" message:@"Network Error"];
        //
//        return;
    }
    
    if([event.name isEqualToString:@"UR110"]) {
        [Utils setStateUIInfo:UI_STATE_UR110 value:UI_STATE_STEP_FIN oldvalue:nil];
        
        if(!descriptor.success) {
            //에러주석[self alertError:@"Error" message:descriptor.error];
            
            //[self onUR601]; // 정보 조회
            [self initControls];
            return;
        }
        [appDelegate onTokenToServer];
        int rlt = [self executeSingIn:event]; // 0:로그인 실패 1:정상로그인 11:subscription 12:card
        //NSLog(@"rlt:%d",rlt);
        if (rlt == 11) {
            //
//            TXSubscriptionVC *firstVC = [[TXSubscriptionVC alloc] initWithNibName:@"TXSubscriptionVC" bundle:nil];
//            firstVC.isNextCard = NO;
//            [self pushViewController:firstVC];
//            [self removeFromParentViewController];
            return;
        }
        else if (rlt == 12) {
            //
//            TXSubscriptionVC *firstVC = [[TXSubscriptionVC alloc] initWithNibName:@"TXSubscriptionVC" bundle:nil];
//            [self pushViewController:firstVC];
//            [self removeFromParentViewController];
            return;
        }
        else if (rlt == 21) {
            //
//            TXAskCardNumberVC *firstVC = [[TXAskCardNumberVC alloc] initWithNibName:@"TXAskCardNumberVC" bundle:nil];
//            [self pushViewController:firstVC];
//            [self removeFromParentViewController];
            return;
        }
        else if (rlt == 31) {
            // driver로그인
            //[self onSignInByDriver:nil];
            [self onUR114]; // rider logout -> driver login
            
            return;
        }
        
        if (appDelegate.isDriverMode) {
            // driver로그인
            //[self onSignInByDriver:nil];
            [self onUR114]; // rider logout -> driver login
            return;
        }
        
        
        @try {
            // redis 접속
            NSString     *response = [[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding];
            NSDictionary *responseObj   = getJSONObj(response);
            
            appDelegate.api_config = [responseObj objectForKey:@"user_config"];
            //[RedisSingleton redisLogin:appDelegate.api_config];
        }
        @catch (NSException *exception) {
            NSLog(@"Exception: %@, %@", exception.name, exception.reason);
        }
        
        if(descriptor.success) {
            [self removeEventListeners];

            dispatch_async(dispatch_get_main_queue(), ^{
#ifdef _DRIVER_MODE
                TXMapVC *mapVC = [[TXMapVC alloc] initWithNibName:@"TXMapDriverVC" bundle:nil];
#else
                TXMapVC *mapVC = [[TXMapVC alloc] initWithNibName:@"TXMapRiderVC" bundle:nil];
#endif
                //mapVC.isDriveMode = YES;
                [self pushViewController:mapVC];
                NSArray* tempVCA = [self.navigationController viewControllers];
                for(UIViewController *tempVC in tempVCA)
                {
                    if(![tempVC isKindOfClass:[TXMapVC class]]) {
                        [tempVC removeFromParentViewController];
                    }
                }
                [self removeFromParentViewController];
            });
            
        }
        else {
            //NSString *message = [TXCode2MsgTranslator messageForCode:descriptor.rlt];
            //[self alertError:@"Error" message:message];
            //에러주석[self alertError:@"Error" message:descriptor.error];
        }
    }
    else if([event.name isEqualToString:@"UR114"]) {
        
        if(descriptor.success) {
            [self onSignInByDriver:nil];
        }
    }
    else if([event.name isEqualToString:@"PP110"]) {
        [Utils setStateUIInfo:UI_STATE_PP110 value:UI_STATE_STEP_FIN oldvalue:nil];
        
        if(!descriptor.success) {
            //에러주석[self alertError:@"Error" message:descriptor.error];
            
            [self initControls];
            return;
        }
        
        [appDelegate onTokenToServer];
        @try {
            // redis 접속
            NSString     *response = [[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding];
            NSDictionary *responseObj   = getJSONObj(response);
            
            appDelegate.api_config = [responseObj objectForKey:@"user_config"];
            //[RedisSingleton redisLogin:appDelegate.api_config];
            
        }
        @catch (NSException *exception) {
            NSLog(@"Exception: %@, %@", exception.name, exception.reason);
        }
        
        if(descriptor.success) {
            
            // 앱 기동 시 최종 상태가 드라이브 모드면
            //
//            [self onUR200]; // 탑승자 정보 조회
//            [self onUR601]; // 탑승자 정보 조회
            //[self onPP602]; // 정보 조회
            NSString     *response = [[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding];
            NSDictionary *responseObj   = getJSONObj(response);
            
            NSDictionary *dic = [responseObj objectForKey:@"provider_device"];
            
//            NSLog(@"***********dic:%@",dic);
            if ([Utils isDictionary:dic]) {
                int state = [dic[@"state"] intValue];
//                NSLog(@"***********state:%d",state);
                if (state > 230) {
                    [self->model PQ300];
                    return;
                }
                else if (state >= 220) {
                    [self->model PQ200];
                    return;
                }
            }
            
            [self removeEventListeners];
            
            dispatch_async(dispatch_get_main_queue(), ^{
#ifdef _DRIVER_MODE
                TXMapVC *mapVC = [[TXMapVC alloc] initWithNibName:@"TXMapDriverVC" bundle:nil];
#else
                TXMapVC *mapVC = [[TXMapVC alloc] initWithNibName:@"TXMapRiderVC" bundle:nil];
#endif
                mapVC.isDriveMode = YES;
                [self pushViewController:mapVC];
                NSArray* tempVCA = [self.navigationController viewControllers];
                for(UIViewController *tempVC in tempVCA)
                {
                    if(![tempVC isKindOfClass:[TXMapVC class]]) {
                        [tempVC removeFromParentViewController];
                    }
                }
                [self removeFromParentViewController];
            });
        }
        else {
            //NSString *message = [TXCode2MsgTranslator messageForCode:descriptor.rlt];
            //[self alertError:@"Error" message:message];
            //에러주석[self alertError:@"Error" message:descriptor.error];
        }
    }
    else if([event.name isEqualToString:@"PQ200"] || [event.name isEqualToString:@"PQ300"]) {
        
        if(descriptor.success) {
            [self removeEventListeners];
            
            dispatch_async(dispatch_get_main_queue(), ^{
#ifdef _DRIVER_MODE
                TXMapVC *mapVC = [[TXMapVC alloc] initWithNibName:@"TXMapDriverVC" bundle:nil];
#else
                TXMapVC *mapVC = [[TXMapVC alloc] initWithNibName:@"TXMapRiderVC" bundle:nil];
#endif
                mapVC.isDriveMode = YES;
                [self pushViewController:mapVC];
                NSArray* tempVCA = [self.navigationController viewControllers];
                for(UIViewController *tempVC in tempVCA)
                {
                    if(![tempVC isKindOfClass:[TXMapVC class]]) {
                        [tempVC removeFromParentViewController];
                    }
                }
                [self removeFromParentViewController];
            });
            
        }
        else {
            //NSString *message = [TXCode2MsgTranslator messageForCode:descriptor.rlt];
            //[self alertError:@"Error" message:message];
            //에러주석[self alertError:@"Error" message:descriptor.error];
        }
    }
}

-(int) executeSingIn:(TXEvent *)event {
    
    //TXResponseDescriptor *descriptor = [event getEventProperty:TXEvents.Params.DESCRIPTOR];
    TXRequestObj *request = [event getEventProperty:TXEvents.Params.REQUEST];
    
    NSString     *response = [[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding];
    NSDictionary *result   = getJSONObj(response);
    
    if([event.name isEqualToString:@"UR110"]) {
        
        NSArray *user_payment           = [result valueForKey:@"user_payments"];
        
        // 성공
        
        // 사용가능 카드를 찾는다.
        //if (![[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.CSEQ]) {
            for (NSDictionary* key in user_payment) {
                NSString* cseq   = [key valueForKey:@"cseq"];
                NSString* mode   = [NSString stringWithFormat:@"%@", [key valueForKey:@"mode"]];
                NSString* state  = [key valueForKey:@"state"];
                
                if ([mode isEqualToString:@"1"] && [state isEqualToString:@"1"] ) {
                    [[[TXApp instance] getSettings] setFDKeychain:SettingsConst.CryptoKeys.CSEQ value:cseq];
                    break;
                }
            }
        //}
        
        
        NSDictionary *requestObj = getJSONObj(request.body);
        
        [[[TXApp instance] getSettings] setUserId:[requestObj valueForKey:API_JSON.UID]];

        // 등록된 카드정보가 없으면 카드등록화면으로 전환
        if (![user_payment count]) {
//            return 21;
        }
        
        if (appDelegate.pstate >= 220) {
//            appDelegate.isDriverMode = YES;
            return 31;
        }
    }
    else if([event.name isEqualToString:@"PP110"]) {
        NSLog(@"appDelegate.isBecomeMode:%d",appDelegate.isBecomeMode);
    }
    
    return 1;
}
/*
#pragma mark Current Location

-(void)startUpdatingLocation {
    if (self->locationMgr == nil) {
        self->locationMgr = [[CLLocationManager alloc] init];
        self->locationMgr.delegate = self;
//        self->locationMgr.allowsBackgroundLocationUpdates = YES;
        self->locationMgr.pausesLocationUpdatesAutomatically = NO;
        self->locationMgr.activityType = CLActivityTypeAutomotiveNavigation;
    
        if (appDelegate.isDriverMode) {
            // 항상 위치 정보 사용 요청
//            if ([self->locationMgr respondsToSelector:@selector(requestAlwaysAuthorization)]) {
//                [self->locationMgr requestAlwaysAuthorization];
//            }
            if ([self->locationMgr respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
                [self->locationMgr requestWhenInUseAuthorization];
            }
        }
        else {
            // 사용중일때만 요청
            if ([self->locationMgr respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
                [self->locationMgr requestWhenInUseAuthorization];
            }
        }
        
        self->locationMgr.distanceFilter = kCLDistanceFilterNone;
        self->locationMgr.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
    }
    [self->locationMgr startUpdatingLocation];
}

#pragma mark CLLocationManager delegate

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"didUpdateToLocation: %@", newLocation);
    CLLocation *currentLocation = newLocation;
    
    if (currentLocation != nil) {
        NSLog(@"%.8f", currentLocation.coordinate.longitude);
        NSLog(@"%.8f", currentLocation.coordinate.latitude);
    }
}


-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways) {
        [self->locationMgr startUpdatingLocation];
    }
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    
    CLLocation *location = [manager location];
    
    NSNumber *myDoubleNumberLat = [NSNumber numberWithDouble:location.coordinate.latitude];
    NSNumber *myDoubleNumberLng = [NSNumber numberWithDouble:location.coordinate.longitude];
    
    [Utils updateCurrentPosition:[myDoubleNumberLat stringValue] lng:[myDoubleNumberLng stringValue]];
    
    [self->locationMgr stopUpdatingLocation];
    NSLog(@"didUpdateLocations: %@", location);
    
    appDelegate.location = location;
    
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    locMgr.realTimeCLocation = location;
    [locMgr updateCurrentLocation:location];
    
    self->isFecthLocation = YES;
    [self hideBusyIndicator];
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
    NSLog(@"Error while getting core location : %@",[error localizedFailureReason]);
    if ([error code] == kCLErrorDenied) {
        //you had denied
    }
    [manager stopUpdatingLocation];
}
*/


@end
