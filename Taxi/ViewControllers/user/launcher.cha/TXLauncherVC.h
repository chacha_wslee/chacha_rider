//
//  TXLauncherVC.h

//
#import <UIKit/UIKit.h>
#import "TXButton.h"
#import "TXUserModel.h"
#import "TXUserVC.h"
#import <GoogleMaps/GoogleMaps.h>
#import "RTLabel.h"
#import "RTUILabel.h"

@interface TXLauncherVC : TXUserVC<CLLocationManagerDelegate,UIScrollViewDelegate> {
    
    IBOutlet UIScrollView    *scrollView;
    IBOutlet UIPageControl   *pageControl;
    
    IBOutlet TXButton       *btnstart;
    IBOutlet TXButton       *btnstart1;
    IBOutlet TXButton       *btnstart2;
    
    IBOutlet UIView          *_introScrollView;
    
    NSArray *myCovers;
    NSTimer *timer;
    
    IBOutlet UILabel *text1;
    IBOutlet UILabel *text2;
    
    IBOutlet RTUILabel *text11;
    
    NSArray *_scrollLabel;
    NSArray *_scrollLabel2;
}

@property (retain, nonatomic) IBOutlet UIButton    *btnSignIn;
@property (retain, nonatomic) IBOutlet UIButton    *btnSignUp;

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil flag:(BOOL)flag;
- (id)initWithFlag:(BOOL) flag;
- (IBAction) onClick: (id) sender;

@end
