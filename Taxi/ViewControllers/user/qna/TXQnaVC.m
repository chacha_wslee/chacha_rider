//
//  MenuViewController.m
//  SlideMenu
//

//

#import "TXQnaVC.h"
#import "TXQnaDetailVC.h"
#import "utilities.h"
#import "WZLBadgeImport.h"

@import UITextView_Placeholder;

@interface TXQnaVC() <UITextViewDelegate> {
    NSArray *items;
    NSArray *_msgList;
    
    NSString *currDate;
}

@end

@implementation TXQnaVC {
    NSMutableDictionary *propertyMap;
}
@synthesize cellIdentifier;


-(void)viewDidLoad {
    
    [super viewDidLoad];
    
    [[IQKeyboardManager sharedManager] setEnable:NO];
    
    [super configureBottomList];
    UIView *n = [super navigationView];
    NSInteger _y = n.frame.origin.y + n.frame.size.height;
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, _y, self.view.bounds.size.width, self.view.bounds.size.height-_y-kBottomButtonHeight)
                                                  style:UITableViewStylePlain];
    self.tableView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:self.tableView];
    
    UINib *nib = [UINib nibWithNibName:@"TXQnaCell" bundle:nil];
    self.cellIdentifier = @"TXQnaCell";
    [self.tableView registerNib:nib forCellReuseIdentifier:self.cellIdentifier];
    
    self.tableView.separatorColor = [UIColor clearColor];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.opaque = NO;
    self.tableView.backgroundColor = [UIColor clearColor];
//    self.tableView.tableHeaderView = ({
//        
//        UIView *view = [Utils addLabelView:self.view underView:nil dim:0];
//        
//        UILabel *label = [view viewWithTag:11];
//        //        label.text = [NSString stringWithFormat:@"CREDIT CARD LIST"];
//        label.tag = 2001;
//        //[label sizeToFit];
//        label.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
//        
//        view;
//    });
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    self.tableView.scrollsToTop = YES;
    
    UIButton *btn = [Utils addBottomButton:LocalizedStr(@"Menu.Qna.BTN.Add.text") target:self selector:@selector(addButtonPressed:) color:YES];
    [self.view addSubview:btn];
    self.tableView.scrollsToTop = YES;
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    //[self removeEventListeners];
}

#pragma mark - Configure

-(void)configureStyles {
    [super configureStyles];
    
    if (appDelegate.isDriverMode) {
        [super navigationType01X:[UIImage imageNamed:@"btn_back"] centerText:LocalizedStr(@"Menu.Qna.title")];
    }
    else {
        [super navigationType01X:[UIImage imageNamed:@"btn_back"] centerText:LocalizedStr(@"Menu.Qna.title")];
    }
}

-(void)configure {
    [super configure];
    
    NSDate *date = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:DATE_FORMAT_YYYYMMDD];
    currDate = [dateFormatter stringFromDate:date];
    //currDate = StringFormatDate2Date(currDate);
    
    // 2. init event
    _msgList = @[
                 @[@"UR502",@""], // rider
                 @[@"PP502",@""], // driver
                 
                 @[@"UR522",@""], // modify
                 @[@"PP522",@""], // modify
                 ];
    
    [self registerEventListeners];
    
    propertyMap = [[NSMutableDictionary alloc] init];
    
    if (appDelegate.isDriverMode) {
        [self configureDriver];
    }
    else {
        [self configureRider];
    }
    
}

-(void) configureDriver {
    
    // 3. call api
    [self showBusyIndicator:@"Requesting ... "];
    [self->model requsetAPI:@"PP502" property:nil];
}

-(void) configureRider {

    // 3. call api
    [self showBusyIndicator:@"Requesting ... "];
    [self->model requsetAPI:@"UR502" property:nil];
}

-(void) registerEventListeners {
    for (NSArray *msg in _msgList) {
        [self->model addEventListener:self forEvent:[msg objectAtIndex:0] eventParams:nil];
    }
}

-(void) removeEventListeners {
    for (NSArray *msg in _msgList) {
        [self->model removeEventListener:self forEvent:[msg objectAtIndex:0]];
    }
}

#pragma mark - Action
-(IBAction)onNaviButtonClick:(id)sender {
    UIButton *btn = (UIButton*)sender;
    NSLog(@"onNaviButtonClick");
    //[super onNaviButtonClick:sender];
    
    if (btn.tag == 1100) {
        [self removeEventListeners];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

-(IBAction)addButtonPressed:(id)sender {
    //UIButton *btn = (UIButton*)sender;
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(20.0f, 20.0f, 280.0f, 240.0f)];
    
    UIView *line1 = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0, 320.0f, 1)];
    line1.backgroundColor = [UIColor colorWithWhite:0.97 alpha:1.f];
    
    UITextField *tf = [[UITextField alloc] initWithFrame:CGRectMake(15.0f, 1.0f, 260.0f, 38)];
    tf.textColor = [UIColor blackColor];
    tf.font = [UIFont systemFontOfSize:14.f];
    tf.clearButtonMode = UITextFieldViewModeNever;
    tf.placeholder = LocalizedStr(@"Menu.Faq.Detail.Alert.text1.plcaholder");
    
    UIView *line2 = [[UIView alloc] initWithFrame:CGRectMake(10.0f, 39, 320.0f, 1)];
    line2.backgroundColor = [UIColor colorWithWhite:0.97 alpha:1.f];
    
    CGRect textViewFrame = CGRectMake(10.0f, 40.0f, 260.0f, 180.0f);
    UITextView *textView = [[UITextView alloc] initWithFrame:textViewFrame];
    //textView.returnKeyType = UIReturnKeyDone;
    //textView.backgroundColor = [UIColor clearColor];
    [textView setFont:[UIFont systemFontOfSize:14]];
    textView.userInteractionEnabled = YES;
    textView.placeholder = LocalizedStr(@"Menu.Faq.Detail.Alert.text2.plcaholder");
    textView.placeholderColor = [UIColor lightGrayColor]; // optional
    
    textView.delegate = self; //텍스트 뷰 델리게이트
    
    [view addSubview:line1];
    [view addSubview:tf];
    [view addSubview:line2];
    [view addSubview:textView];
    
    //CALayer 클래스 이용 텍스트 뷰 커스터마이징, 레이어 프로퍼티 사용을 위해 <QuartzCore/QuartzCore.h> 임포트 할 것.
    //[[textView layer] setBorderColor:[[UIColor grayColor] CGColor]]; //border color
    //[[textView layer] setBackgroundColor:[[UIColor whiteColor] CGColor]]; //background color
    //[[textView layer] setBorderWidth:0.5]; // border width
    //[[textView layer] setCornerRadius:5]; // radius of rounded corners
    //[textView setClipsToBounds: YES]; //clip text within the bounds
    
    LGAlertView *alertView = [[LGAlertView alloc] initWithViewAndTitle:LocalizedStr(@"Menu.Faq.Detail.Alert.title")
                                                               message:nil
                                                                 style:LGAlertViewStyleAlert
                                                                  view:view
                                                          buttonTitles:@[LocalizedStr(@"Button.OK")]
                                                     cancelButtonTitle:LocalizedStr(@"Button.CANCEL")
                                                destructiveButtonTitle:nil
                                                         actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                                                             NSLog(@"actionHandler, %@, %lu", title, (long unsigned)index);
                                                             NSLog(@"text:%@",textView.text);
                                                             //tf.text = textView.text;
                                                             
                                                             NSString *__title = [Utils trimString:tf.text];
                                                             __title = tf.text;
                                                             NSString *__content = [Utils trimString:textView.text];
                                                             __content = textView.text;
                                                             
                                                             if (![__title isEqualToString:@""] && ![__content isEqualToString:@""]) {
                                                                 if (appDelegate.isDriverMode) {
                                                                     [self onPP522:__title content:__content];
                                                                 }
                                                                 else {
                                                                     [self onUR522:__title content:__content];
                                                                 }
                                                             }
                                                             else {
                                                                 [WToast showWithText:LocalizedStr(@"Menu.Qna.reply.error.text") duration:kWTShort roundedCorners:NO gravity:kWTGravityTop];
                                                                 return;
                                                             }
                                                             
                                                             [alertView dismissAnimated:YES completionHandler:nil];
                                                             
                                                         }
                                                         cancelHandler:^(LGAlertView *alertView) {
                                                             [alertView dismissAnimated:YES completionHandler:nil];
                                                         }
                                                    destructiveHandler:^(LGAlertView *alertView) {
                                                        [alertView dismissAnimated:YES completionHandler:nil];
                                                    }];
    
    //alertView.heightMax = 256.f;
    [Utils initAlertButtonColor:alertView];
    alertView.windowLevel = LGAlertViewWindowLevelAboveStatusBar;
    //alertView.separatorsColor = [UIColor redColor];
    [alertView showAnimated:YES completionHandler:^(void)
     {
         //[textView becomeFirstResponder];
         [tf becomeFirstResponder];
     }];
    [alertView setDismissOnAction:NO];
}

#pragma mark - UITextView Delegate Methods

//캐릭터가 텍스트 뷰에 표시되기 직전 아래 메소드가 호출된다
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    
    
    //    //newLineCharacterSet이 있으면 done button이 호출됨. 따라서 키보드가 사라짐.
    //    NSCharacterSet *doneButtonCharacterSet = [NSCharacterSet newlineCharacterSet];
    //    NSRange replacementTextRange = [text rangeOfCharacterFromSet:doneButtonCharacterSet];
    //    NSUInteger location = replacementTextRange.location;
    //
    //    //텍스트가 190자가 넘지 않도록 제한
    //    if (textView.text.length + text.length > 190){
    //        if (location != NSNotFound){
    //            [textView resignFirstResponder];
    //        }
    //        return NO;
    //    }
    //
    //    else if (location != NSNotFound){
    //        if([text isEqualToString:@"\n"]) {
    //            return YES;
    //        }
    //        [textView resignFirstResponder];
    //        return NO;
    //    }
    
    return YES;
}

#pragma mark - UITableView Delegate & Datasrouce -
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [tableView setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [tableView setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
    UIView *seperatorView = [[UIView alloc] initWithFrame:CGRectMake(10, cell.frame.size.height - 1, cell.frame.size.width - 10*2, 1)];
    seperatorView.backgroundColor = UIColorTableSeperator;
    [cell addSubview:seperatorView];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}


- (CGFloat)calculateHeightForConfiguredSizingCell:(UITableViewCell *)sizingCell {
    [sizingCell layoutIfNeeded];
    
    CGSize size = [sizingCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    return size.height;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static UITableViewCell *cell = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        cell = [self.tableView dequeueReusableCellWithIdentifier:@"TXQnaCell"];
    });
    
    return [self calculateHeightForConfiguredSizingCell:cell];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return self->items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:self.cellIdentifier];
	
    NSDictionary *dic = self->items[indexPath.row];
    NSString *title = [dic valueForKey:@"title"];
    if ([[Utils nullToString:title] isEqualToString:@""]) {
        title = @"";
    }
    
    // driver
    if (appDelegate.isDriverMode) {
        
        ((UILabel *)[cell.contentView viewWithTag:10]).text = title;
        ((UILabel *)[cell.contentView viewWithTag:11]).text = [dic valueForKey:@"reg_date"];
        ((UILabel *)[cell.contentView viewWithTag:11]).textColor = HEXCOLOR(0x999999ff);
    }
    else {
        
        ((UILabel *)[cell.contentView viewWithTag:10]).text = title;
        ((UILabel *)[cell.contentView viewWithTag:11]).text = [dic valueForKey:@"reg_date"];
        ((UILabel *)[cell.contentView viewWithTag:11]).textColor = HEXCOLOR(0x999999ff);
    }
    UIImageView *imgFlag = (UIImageView *)[cell.contentView viewWithTag:15];
    //NSString *diff = StringFormatDate2Date([dic valueForKey:@"update_date"]);
    //if ([currDate isEqualToString:diff]) {
    if (![[Utils nullToString:dic[@"reply"]] isEqualToString:@""]) {
        imgFlag.hidden = NO;
        
//        UIImageView *drFlag = (UIImageView *)[cell.contentView viewWithTag:2];
//        
//        [drFlag showBadgeWithStyle:WBadgeStyleNew value:0 animationType:WBadgeAnimTypeNone];
//        CGRect frame = drFlag.badge.frame;
//        frame.origin.x -= 30;
//        frame.origin.y += 6;
//        drFlag.badge.frame = frame;
        
//        UILabel *lb = (UILabel *)[cell.contentView viewWithTag:10];
//        drFlag.badgeColor   = [UIColor redColor];
//        drFlag.badgeMinSize = 10;
//        drFlag.badgeOriginX = _btnRiderName.frame.size.width - _btnRiderName.badgeMinSize*2;
//        drFlag.badgeOriginY = _btnRider.frame.size.height/4 - 5;
    }
    else {
        imgFlag.hidden = YES;
    }
    
    // Make sure the constraints have been added to this cell, since it may have just been created from scratch
    //[cell setNeedsUpdateConstraints];
    //[cell updateConstraintsIfNeeded];
    
//    if (self->items.count == indexPath.row) {
//        ((UIView *)[cell.contentView viewWithTag:1]).backgroundColor = [UIColor clearColor];
//    }
//    else {
//        ((UIView *)[cell.contentView viewWithTag:1]).backgroundColor = UIColorBasicBack;
//    }

	
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSDictionary *dic = self->items[indexPath.row];
    NSDictionary *reply = self->items[indexPath.row];
    
    NSString *title = [dic valueForKey:@"title"];
    if ([[Utils nullToString:title] isEqualToString:@""]) {
        title = @"";
    }
    
    NSArray *list = @[dic];
    if (![[Utils nullToString:dic[@"reply"]] isEqualToString:@""]) {
        list = @[dic, reply];
    }
    TXQnaDetailVC *vc = [[TXQnaDetailVC alloc] initWithNibName:@"TXQnaDetailVC" bundle:[NSBundle mainBundle]];
    vc.tripData = list;
    vc.qseq = [dic valueForKey:@"qseq"];
    vc.subtitle = title;
    
    [super pushViewController:vc];
}

#pragma mark - onEvent
-(void)onUR522:(NSString*)title content:(NSString*)content {
    //useq,qtype,title,content
    
    NSString* useq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.USEQ];
    //NSString* qtype = self.tripData[0][@"qtype"];
    NSDictionary *_propertyMap = @{
                                   @"useq" : useq,
                                   @"ctype" : @"0",
                                   @"title" : title,
                                   @"content" : content
                                   };
    
    [self showBusyIndicator:@"Upload ... "];
    [self->model requsetAPI:API_FUNC(NSStringFromSelector(_cmd)) property:_propertyMap];
}

-(void)onPP522:(NSString*)title content:(NSString*)content {
    //useq,qtype,title,content
    
    NSString* pseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.PSEQ];
    //NSString* qtype = self.tripData[0][@"qtype"];
    NSDictionary *_propertyMap = @{
                                   @"pseq" : pseq,
                                   @"ctype" : @"0",
                                   @"title" : title,
                                   @"content" : content
                                   };
    
    [self showBusyIndicator:@"Upload ... "];
    [self->model requsetAPI:API_FUNC(NSStringFromSelector(_cmd)) property:_propertyMap];
}

-(void)onEvent:(TXEvent *)event eventParams:(id)subscriptionParams {
    
    TXResponseDescriptor * descriptor = [event getEventProperty:TXEvents.Params.DESCRIPTOR];
    TXRequestObj *request = [event getEventProperty:TXEvents.Params.REQUEST];
    
    if([event.name isEqualToString:@"UR502"]) {
        
        if(descriptor.success == true) {
            NSString     *response = [[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding];
            NSDictionary *result   = getJSONObj(response);
            
            self->items = [result valueForKey:@"user_qnas"];
            
            [self.tableView reloadData];
            
        } else {
            //에러주석[self alertError:@"Error" message:descriptor.error];
        }
    }
    else if([event.name isEqualToString:@"PP502"]) {
        
        if(descriptor.success == true) {
            NSString     *response = [[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding];
            NSDictionary *result   = getJSONObj(response);
            
            self->items = [result valueForKey:@"provider_qnas"];
            
            [self.tableView reloadData];
            
        } else {
            //에러주석[self alertError:@"Error" message:descriptor.error];
        }
    }
    else if([event.name isEqualToString:@"UR522"] || [event.name isEqualToString:@"PP522"]) {
        
        if(descriptor.success == true) {
            NSString     *response = [[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding];
            NSDictionary *result   = getJSONObj(response);
            
            // reload
            if (appDelegate.isDriverMode) {
                self->items = [result valueForKey:@"provider_qnas"];
            }
            else {
                self->items = [result valueForKey:@"user_qnas"];
            }
            [self.tableView reloadData];
            self.tableView.scrollsToTop = YES;
            
            if ([appDelegate.mapVCBeforeVC isEqualToString:@"TXQnaVC"]) {
                LGAlertView* alertView = [[LGAlertView alloc] initWithTitle:nil
                                                                    message:LocalizedStr(@"Menu.Faq.Detail.Alert.complete")
                                                                      style:LGAlertViewStyleAlert
                                                               buttonTitles:@[LocalizedStr(@"Button.OK")]
                                                          cancelButtonTitle:nil
                                                     destructiveButtonTitle:nil
                                                              actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                                                                  //NSLog(@"actionHandler, %@, %lu", title, (long unsigned)index);
                                                                  
                                                                  
                                                              }
                                                              cancelHandler:^(LGAlertView *alertView) {
                                                              }
                                                         destructiveHandler:^(LGAlertView *alertView) {
                                                         }];
                [Utils initAlertButtonColor:alertView];
                [alertView showAnimated:YES completionHandler:nil];
            }
        }
        else {
            [self hideBusyIndicator];
            //에러주석[self alertError:@"Error" message:descriptor.error];
        }
    }
    [self hideBusyIndicator];
}



@end
