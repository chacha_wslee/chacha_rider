//
//  TXAskCardNumberVC.m
//  Taxi

//

#import "TXHelpVC.h"

#import "Validator.h"
#import "WToast.h"
//#import "CRToast.h"
@import IQKeyboardManager;

@interface TXHelpVC () <UITableViewDelegate, UITableViewDataSource, ValidatorDelegate>{
    NSArray *dataArray;
    NSArray *_msgList;
}

@end

@implementation TXHelpVC {
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[IQKeyboardManager sharedManager] setEnable:NO];
    
    [super configureBottomLine];
    UIView *line = (UIView*)[[super navigationView] viewWithTag:1500];
#ifdef _WITZM
    CGRect rect = line.frame;
    rect.size.height = 3;
    line.frame = rect;
#elif defined _CHACHA
    CGRect rect = line.frame;
    rect.size.height = 1;
    line.frame = rect;
    line.backgroundColor = HEXCOLOR(0x666666FF);
#endif

//    UILabel *label = (UILabel*)[[super navigationView] viewWithTag:1111];
//    label.tag = 2001;
//    label.text = LocalizedStr(@"Menu.Qna.title");
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    //UIButton *btn = [Utils addBottomButton:LocalizedStr(@"Menu.Qna.BTN.text") target:self selector:@selector(onNaviButtonClick:) color:YES];
#ifdef _WITZM
    UIButton *btn = [Utils addBottomButton:LocalizedStr(@"Button.OK") target:self selector:@selector(onNaviButtonClick:) color:YES];
#elif defined _CHACHA
    UIButton *btn = [Utils addBottomButton:LocalizedStr(@"Button.Regist") target:self selector:@selector(onNaviButtonClick:) color:YES];
#endif

    btn.tag = 2300;
    [self.view addSubview:btn];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    //[_tfText becomeFirstResponder];
    [_tvText becomeFirstResponder];
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

-(void)dealloc {
    [self removeEventListeners];
}

-(void)configureStyles {
    [super configureStyles];
    
#ifdef _WITZM
    [super navigationType01X:[UIImage imageNamed:@"btn_back_02"] centerText:LocalizedStr(@"Menu.Qna.write.title")];
#elif defined _CHACHA
    [super navigationType01X:[UIImage imageNamed:@"btn_back"] centerText:LocalizedStr(@"Menu.Qna.write.title")];
#endif

}

- (void)nextButtonOn:(BOOL)on
{
    UIButton *btn = [super.view viewWithTag:2300];
    btn.enabled = on;
    if (on)
        [btn setTitleColor:UIColorBasicTextOn forState:UIControlStateNormal];
    else
        [btn setTitleColor:UIColorBasicTextOff forState:UIControlStateNormal];
}

-(void) configure {
    [super configure];
    
    UIView *n = [super navigationView];
    NSInteger _y = n.frame.origin.y + n.frame.size.height;
#ifdef _WITZM
    [n setBackgroundColor:HEXCOLOR(0x333333ff)];
    UILabel *lb = [n viewWithTag:1210];
    
    lb.textColor = [UIColor whiteColor];
#elif defined _CHACHA
    [n setBackgroundColor:[UIColor whiteColor]];
    UILabel *lb = [n viewWithTag:1210];
    
    lb.textColor = HEXCOLOR(0x111111FF);
#endif
    
    // 2. init event
    _msgList = @[
//                 @[@"UR501",@""], // rider
//                 @[@"PP501",@""], // driver
                 
                 @[@"UR522",@""], // rider post
                 @[@"PP522",@""], // driver post
                 ];
    
    [self registerEventListeners];
    
    if (appDelegate.isDriverMode) {
        [self configureDriver];
    }
    else {
        [self configureRider];
    }
    
    dataArray = @[];
    
    UIButton *btn = [self.view viewWithTag:2300];
    CGRect frame = _tvText.frame;
    frame.origin.y = _y + 22;
    frame.size.height = self.view.frame.size.height - frame.origin.y - btn.frame.size.height - 20;
    _tvText.frame = frame;
    //------------------------------------------------------------------------------------------------------------------------
//    ((UILabel *)[self.view viewWithTag:111]).text = LocalizedStr(@"Menu.Payment.List1.text1");
//    ((UILabel *)[self.view viewWithTag:121]).text = LocalizedStr(@"Menu.Payment.List1.text1");
    _tvText.textColor = UIColorDefault;
    //------------------------------------------------------------------------------------------------------------------------
#ifdef _WITZM
    self.view.backgroundColor = HEXCOLOR(0x333333ff);
#elif defined _CHACHA
    self.view.backgroundColor = [UIColor whiteColor];
#endif

}

-(void) registerEventListeners {
    for (NSArray *msg in _msgList) {
        [self->model addEventListener:self forEvent:[msg objectAtIndex:0] eventParams:nil];
    }
}

-(void) removeEventListeners {
    for (NSArray *msg in _msgList) {
        [self->model removeEventListener:self forEvent:[msg objectAtIndex:0]];
    }
}

-(void) configureDriver {
    
    // 3. call api
    [self->model PP501:@""];
}

-(void) configureRider {
    
    // 3. call api
    [self->model UR501:@""];
}

#pragma mark - Action
-(IBAction)onNaviButtonClick:(id)sender {
    UIButton *btn = (UIButton*)sender;
    if (btn.tag == 1100 || btn.tag == 1101) {
        [self removeEventListeners];
        [self.navigationController popViewControllerAnimated:YES];
    }
    else if(btn.tag == 2300) {
        [self next:nil];
    }
}

-(void)next:(id)sender {
    
    [self validateAction:sender];
    return;
}

-(IBAction)onClick:(id)sender {
    
    UIButton *btn = (UIButton*)sender;
    
    if (btn.tag == 313 || btn.tag == 323 || btn.tag == 333 || btn.tag == 343) {
        
        UITableView* mTableView = [[UITableView alloc] init];
        mTableView.delegate=self;
        mTableView.dataSource=self;
        
        mTableView.allowsMultipleSelection = NO;
        mTableView.frame = CGRectMake(0.f, 0.f, 320, 162.f);
        mTableView.contentMode = UIViewContentModeScaleAspectFit;
        mTableView.scrollEnabled = NO;
        
        NSIndexPath *indexPath = nil;
        indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
//        if ([tf.text isEqualToString:LocalizedStr(@"Menu.Account.List3.input.text1")]) {
//            indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
//        }
        if (indexPath != nil) {
            [mTableView selectRowAtIndexPath:indexPath
                                    animated:YES
                              scrollPosition:UITableViewScrollPositionNone];
            //[mTableView tableView:mTableView didSelectRowAtIndexPath:indexPath];
            [mTableView.delegate tableView:mTableView didSelectRowAtIndexPath:indexPath];
        }
        
        LGAlertView *alertView = [[LGAlertView alloc] initWithViewAndTitle:LocalizedStr(@"Menu.Account.List3.title")
                                                                   message:nil
                                                                     style:LGAlertViewStyleAlert
                                                                      view:mTableView
                                                              buttonTitles:@[LocalizedStr(@"Button.OK")]
                                                         cancelButtonTitle:LocalizedStr(@"Button.CANCEL")
                                                    destructiveButtonTitle:nil
                                                             actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                                                                 NSLog(@"actionHandler, %@, %lu", title, (long unsigned)index);
                                                                 
                                                                 for(NSIndexPath* indexPath in [mTableView indexPathsForSelectedRows]) {
                                                                     NSLog(@"cell.tag:%@",[dataArray objectAtIndex:indexPath.row]);
                                                                     
                                                                     self.tfText.text = [dataArray objectAtIndex:indexPath.row];
                                                                     self.tfText.tag = indexPath.row;
                                                                     
                                                                 }
                                                             }
                                                             cancelHandler:^(LGAlertView *alertView) {
                                                                 NSLog(@"cancelHandler");
                                                             }
                                                        destructiveHandler:^(LGAlertView *alertView) {
                                                            NSLog(@"destructiveHandler");
                                                        }];
        //alertView.heightMax = 256.f;
        //alertView.heightMax = 156.f;
        //alertView.cancelButtonBackgroundColor = [UIColor redColor];
        [Utils initAlertButtonColor:alertView];
        alertView.windowLevel = LGAlertViewWindowLevelAboveStatusBar;
        [alertView showAnimated:YES completionHandler:nil];
        
    }
}

#pragma mark - Event
-(void)onFail:(id)object error:(TXError *)error {
    
}

-(void)onEvent:(TXEvent *)event eventParams:(id)subscriptionParams {
    
    TXResponseDescriptor *descriptor = [event getEventProperty:TXEvents.Params.DESCRIPTOR];
    //TXRequestObj *request = [event getEventProperty:TXEvents.Params.REQUEST];
    
    if([event.name isEqualToString:@"UR522"] || [event.name isEqualToString:@"PP522"]) {
        
        if(descriptor.success == true) {
            
            LGAlertView* alertView = [[LGAlertView alloc] initWithTitle:nil
                                                                message:LocalizedStr(@"Menu.Faq.Detail.Alert.complete")
                                                                  style:LGAlertViewStyleAlert
                                                           buttonTitles:@[LocalizedStr(@"Button.OK")]
                                                      cancelButtonTitle:nil
                                                 destructiveButtonTitle:nil
                                                          actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                                                              //NSLog(@"actionHandler, %@, %lu", title, (long unsigned)index);
                                                              [self removeEventListeners];
                                                              [self.navigationController popViewControllerAnimated:YES];
                                                              
                                                          }
                                                          cancelHandler:^(LGAlertView *alertView) {
                                                          }
                                                     destructiveHandler:^(LGAlertView *alertView) {
                                                     }];
            [Utils initAlertButtonColor:alertView];
            [alertView showAnimated:YES completionHandler:nil];
            
        } else {
            //[self alertError:LocalizedStr(@"String.Alert") message:descriptor.error];
            //        NSString *message = [TXCode2MsgTranslator messageForCode:descriptor.rlt];
            //        [self alertError:@"Error" message:message];
        }
        [self hideBusyIndicator];
    }
}

#pragma mark - Keyboard Notification Method
- (void)keyboardWillShow:(NSNotification*)note{
    NSDictionary *userInfo = note.userInfo;
    CGRect finalKeyboardFrame = [userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    NSTimeInterval animationDuration = [userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    UIButton *btn = [self.view viewWithTag:2300];
    float inputViewFinalYPosition = self.view.bounds.size.height - finalKeyboardFrame.size.height - btn.frame.size.height;
    CGRect inputViewFrame = btn.bounds;
    inputViewFrame.origin.y = inputViewFinalYPosition;
    
    [UIView animateWithDuration:animationDuration animations:^{
        btn.frame = inputViewFrame;
    }];
    
    // textview
    UIView *n = [super navigationView];
    NSInteger _y = n.frame.origin.y + n.frame.size.height;
    CGRect frame = _tvText.frame;
    frame.origin.y = _y + 22;
    frame.size.height = self.view.frame.size.height - frame.origin.y - inputViewFrame.origin.y - 20;
    _tvText.frame = frame;
}

- (void)keyboardWillHide:(NSNotification*)note{
    
    NSDictionary *userInfo = note.userInfo;
    NSTimeInterval animationDuration = [userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    UIButton *btn = [self.view viewWithTag:2300];
    CGRect inputViewFrame = btn.bounds;
    inputViewFrame.origin.y = self.view.bounds.size.height - btn.frame.size.height;
    
    [UIView animateWithDuration:animationDuration animations:^{
        btn.frame = inputViewFrame;
    }];
    
    // textview
    UIView *n = [super navigationView];
    NSInteger _y = n.frame.origin.y + n.frame.size.height;
    CGRect frame = _tvText.frame;
    frame.origin.y = _y + 22;
    frame.size.height = self.view.frame.size.height - frame.origin.y - btn.frame.size.height - 20;
    _tvText.frame = frame;
}

#pragma mark - UITableView DataSource Methods
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [tableView setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [tableView setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
    
    UIView *seperatorView = [[UIView alloc] initWithFrame:CGRectMake(10, cell.frame.size.height - 1, cell.frame.size.width - 10*2, 1)];
    seperatorView.backgroundColor = UIColorTableSeperator;
    [cell addSubview:seperatorView];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    // This will create a "invisible" footer
    return 0.01f;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [UIView new];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [dataArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    cell.tag = indexPath.row;
    int tag = (int)indexPath.row+100;
    //[self changeCell:tableView withIndexPath:indexPath andTag:tag];
    [self addCell:tableView withCell:cell withIndexPath:indexPath andTag:tag];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    cell.textLabel.text = [NSString stringWithFormat:@"      %@",[dataArray objectAtIndex:indexPath.row]];
    
    return cell;
}

#pragma mark - UITableView Delegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self changeCell:tableView withIndexPath:indexPath andTag:((int)indexPath.row+100)];
    
    //cell.accessoryView.hidden = NO;
}
- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self changeCell:tableView withIndexPath:indexPath andTag:((int)indexPath.row+100)];
    
    //cell.accessoryView.hidden = YES;
}

-(void)addCell:(UITableView*)tableView withCell:(UITableViewCell*)cell withIndexPath:(NSIndexPath*)indexPath andTag:(int)tag{
    
    UIImageView *preView = [cell.contentView viewWithTag:tag];
    [preView removeFromSuperview];
    
    UILabel *preLabel = [cell.contentView viewWithTag:(tag+100)];
    [preLabel removeFromSuperview];
    
    NSInteger _h = cell.frame.size.height;
    NSInteger _x = 16;
    UIImageView* imv = [[UIImageView alloc]initWithFrame:CGRectMake(cell.frame.size.width - _x*4 ,_h/2-_x/2, _x, _x)];
    imv.tag = tag;
    UIImage *cellImage;
    
    if([[tableView indexPathsForSelectedRows] containsObject:indexPath]) {
        cellImage = [UIImage imageNamed:@"form_radio_chk"];
    }
    else {
        cellImage = [UIImage imageNamed:@"form_radio_normal"];
    }
    imv.image=cellImage;
    [cell.contentView addSubview:imv];
}

-(void)changeCell:(UITableView*)tableView withIndexPath:(NSIndexPath*)indexPath andTag:(int)tag{
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    UIImageView *preView = [cell.contentView viewWithTag:tag];
    [preView removeFromSuperview];
    
    UILabel *preLabel = [cell.contentView viewWithTag:(tag+100)];
    [preLabel removeFromSuperview];
    
    NSInteger _h = cell.frame.size.height;
    NSInteger _x = 16;
    UIImageView* imv = [[UIImageView alloc]initWithFrame:CGRectMake(cell.frame.size.width - _x*4 ,_h/2-_x/2, _x, _x)];
    //    UIImageView* imv = [[UIImageView alloc]initWithFrame:CGRectMake(0,_h/2-_x/2, _x, _x)];
    imv.tag = tag;
    UIImage *cellImage;
    
    if([[tableView indexPathsForSelectedRows] containsObject:indexPath]) {
        cellImage = [UIImage imageNamed:@"form_radio_chk"];
    }
    else {
        cellImage = [UIImage imageNamed:@"form_radio_normal"];
    }
    imv.image=cellImage;
    [cell.contentView addSubview:imv];
}

#pragma mark -
#pragma mark Validator
#pragma mark -
-(void) resignKeyboard
{
    [self.view endEditing:YES];
}

- (IBAction)validateAction:(id)sender
{
    [self resignKeyboard];
    
//    Validator *validator = [[Validator alloc] init];
//    validator.delegate   = self;
//    [validator putRule:[Rules minLength:2 withFailureString:LocalizedStr(@"Validator.CardNo.text") forTextField:self.tvText]];
//    
//    [validator validate];
    
    if (self.tvText.text.length == 0) {
        [WToast showWithText:LocalizedStr(@"Validator.Qna.text") duration:kWTShort roundedCorners:NO gravity:kWTGravityTop];
        return;
    }
    [self onSuccess];
}

#pragma ValidatorDelegate - Delegate methods

- (void) preValidation
{
}

- (void)onSuccess
{
    
    NSDictionary *propertyMap;
    
    [self showBusyIndicator:@""];
    if (appDelegate.isDriverMode) {
        propertyMap = @{
                        @"ctype" : @"70", // driver 70 고정
                        @"content" : _tvText.text
                        };
        [self->model requsetAPI:@"PP522" property:propertyMap];
    }
    else {
        propertyMap = @{
                        @"ctype" : @"40", // rider 40 고정
                        @"content" : _tvText.text
                        };
        [self->model requsetAPI:@"UR522" property:propertyMap];
    }
    //NSLog(@"propertyMap:%@",propertyMap);
}

- (void)onFailure:(Rule *)failedRule
{
    [WToast showWithText:failedRule.failureMessage duration:kWTShort roundedCorners:NO gravity:kWTGravityTop];
}

@end
