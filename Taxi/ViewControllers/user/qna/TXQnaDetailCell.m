//
//  RJCell.m
//  TableViewController
//
//  Created by Kevin Muldoon & Tyler Fox on 10/5/13.
//  Copyright (c) 2013 RobotJackalope. All rights reserved.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

#import "TXQnaDetailCell.h"

#define kLabelHorizontalInsets      15.0f
#define kLabelVerticalInsets        10.0f

@interface TXQnaDetailCell ()

@property (nonatomic, assign) BOOL didSetupConstraints;

@end

@implementation TXQnaDetailCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        //self.titleLabel = [UILabel newAutoLayoutView];
        self.titleLabel = [[UILabel alloc] init];
        self.titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
        [self.titleLabel setLineBreakMode:NSLineBreakByTruncatingTail];
        [self.titleLabel setNumberOfLines:1];
        [self.titleLabel setTextAlignment:NSTextAlignmentLeft];
        [self.titleLabel setTextColor:[UIColor blackColor]];
        //self.titleLabel.backgroundColor = [UIColor colorWithRed:0 green:0 blue:1 alpha:0.1]; // light blue
        
        //self.dateLabel = [UILabel newAutoLayoutView];
        self.dateLabel = [[UILabel alloc] init];
        self.dateLabel.translatesAutoresizingMaskIntoConstraints = NO;
        [self.dateLabel setLineBreakMode:NSLineBreakByTruncatingTail];
        [self.dateLabel setNumberOfLines:1];
        [self.dateLabel setTextAlignment:NSTextAlignmentLeft];
        [self.dateLabel setTextColor:UIColorLabelText];
        //self.dateLabel.backgroundColor = [UIColor colorWithRed:0 green:0 blue:1 alpha:0.1]; // light blue
        
        //self.bodyLabel = [UILabel newAutoLayoutView];
        self.bodyLabel = [[UILabel alloc] init];
        self.bodyLabel.translatesAutoresizingMaskIntoConstraints = NO;
        [self.bodyLabel setLineBreakMode:NSLineBreakByTruncatingTail];
        [self.bodyLabel setNumberOfLines:0];
        [self.bodyLabel setTextAlignment:NSTextAlignmentLeft];
        [self.bodyLabel setTextColor:[UIColor blackColor]];
        //self.bodyLabel.backgroundColor = [UIColor colorWithRed:1 green:0 blue:0 alpha:0.1]; // light red
        
        self.imgFlag = [[UIImageView alloc] init];
        self.imgFlag.frame = CGRectMake(10, 20, 23, 22);
        self.imgFlag.image = [UIImage imageNamed:@"icon_menu_qna"];
        
        self.contentView.backgroundColor = [UIColor whiteColor];
        
        [self.contentView addSubview:self.titleLabel];
        [self.contentView addSubview:self.dateLabel];
        [self.contentView addSubview:self.bodyLabel];
        [self.contentView addSubview:self.imgFlag];
        
        [self updateFonts];
    }
    
    return self;
}

- (void)updateConstraints
{
    if (!self.didSetupConstraints) {
        // Note: if the constraints you add below require a larger cell size than the current size (which is likely to be the default size {320, 44}), you'll get an exception.
        // As a fix, you can temporarily increase the size of the cell's contentView so that this does not occur using code similar to the line below.
        //      See here for further discussion: https://github.com/Alex311/TableCellWithAutoLayout/commit/bde387b27e33605eeac3465475d2f2ff9775f163#commitcomment-4633188
        // self.contentView.bounds = CGRectMake(0.0f, 0.0f, 99999.0f, 99999.0f);
        
        // Get the views dictionary
        NSDictionary *viewsDictionary;
        
        if (self.isReply) {
            viewsDictionary =
            @{
              @"imgFlag" : self.imgFlag,
              @"titleLabel" : self.titleLabel,
              @"dateLabel" : self.dateLabel,
              @"bodyLabel" : self.bodyLabel
              };
            
            NSString *format;
            NSArray *constraintsArray;
            
            //Create the constraints using the visual language format
            format = @"H:|-10-[imgFlag]-10-[titleLabel]|";
            constraintsArray = [NSLayoutConstraint constraintsWithVisualFormat:format options:0 metrics:nil views:viewsDictionary];
            [self.contentView addConstraints:constraintsArray];
            
            format = @"H:|-10-[bodyLabel]-10-|";
            constraintsArray = [NSLayoutConstraint constraintsWithVisualFormat:format options:0 metrics:nil views:viewsDictionary];
            [self.contentView addConstraints:constraintsArray];
            
            format = @"H:|-10-[dateLabel]|";
            constraintsArray = [NSLayoutConstraint constraintsWithVisualFormat:format options:0 metrics:nil views:viewsDictionary];
            [self.contentView addConstraints:constraintsArray];
            
            format = @"V:|-20-[imgFlag]-20-[bodyLabel]-20-[dateLabel]-20-|";
            constraintsArray = [NSLayoutConstraint constraintsWithVisualFormat:format options:0 metrics:nil views:viewsDictionary];
            [self.contentView addConstraints:constraintsArray];
            
            format = @"V:|-20-[titleLabel]-20-[bodyLabel]-20-[dateLabel]-20-|";
            constraintsArray = [NSLayoutConstraint constraintsWithVisualFormat:format options:0 metrics:nil views:viewsDictionary];
            [self.contentView addConstraints:constraintsArray];
        }
        else {
            self.imgFlag.hidden = YES;
            self.titleLabel.hidden = YES;
            
            viewsDictionary =
            @{
              @"dateLabel" : self.dateLabel,
              @"bodyLabel" : self.bodyLabel
              };
            
            NSString *format;
            NSArray *constraintsArray;
            
            //Create the constraints using the visual language format
            format = @"H:|-10-[bodyLabel]-10-|";
            constraintsArray = [NSLayoutConstraint constraintsWithVisualFormat:format options:0 metrics:nil views:viewsDictionary];
            [self.contentView addConstraints:constraintsArray];
            
            format = @"H:|-10-[dateLabel]|";
            constraintsArray = [NSLayoutConstraint constraintsWithVisualFormat:format options:0 metrics:nil views:viewsDictionary];
            [self.contentView addConstraints:constraintsArray];
            
            format = @"V:|-20-[bodyLabel]-20-[dateLabel]-20-|";
            constraintsArray = [NSLayoutConstraint constraintsWithVisualFormat:format options:0 metrics:nil views:viewsDictionary];
            [self.contentView addConstraints:constraintsArray];
        }
        
        
        self.didSetupConstraints = YES;
    }
    
    [super updateConstraints];
}

- (void)updateFonts
{
    self.contentView.backgroundColor = [UIColor clearColor];
    //self.backgroundColor = HEXCOLOR(0xEEEEEEFF);
    self.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:16];
    self.titleLabel.textColor = UIColorDefault;
    
    self.bodyLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:16];
    self.bodyLabel.textColor = HEXCOLOR(0x333333FF);
    
    self.dateLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:12];
    self.dateLabel.textColor = HEXCOLOR(0x999999FF);
}

@end
