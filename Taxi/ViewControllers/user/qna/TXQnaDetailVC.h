//
//  MenuViewController.h
//  SlideMenu
//
//  Created by Aryan Gh on 4/24/13.
//  Copyright (c) 2013 Aryan Ghassemi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TXBaseViewController.h"

@interface TXQnaDetailVC : TXBaseViewController <UITableViewDelegate, UITableViewDataSource> {
    NSArray *tripData;
}
@property (nonatomic, strong) NSArray *tripData;
@property (nonatomic, strong) NSString *subtitle;
@property (nonatomic, strong) NSString *qseq;
@property (nonatomic, strong) NSString *cellIdentifier;
@property (nonatomic, strong) IBOutlet UITableView *tableView;

@end
