//
//  MenuViewController.h
//  SlideMenu
//

//

#import <UIKit/UIKit.h>
#import "TXBaseViewController.h"

@interface TXNewsVC : TXBaseViewController <UITableViewDelegate, UITableViewDataSource> {

}

@property (nonatomic, strong) NSString *cellIdentifier;
@property (nonatomic, strong) IBOutlet UITableView *tableView;

@end
