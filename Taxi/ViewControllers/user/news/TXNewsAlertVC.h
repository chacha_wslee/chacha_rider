//
//  TXNewsDetailVC.h
//  TXNewsDetailVC
//

//

#import <UIKit/UIKit.h>
#import "TXButton.h"


@interface TXNewsAlertVC : UIViewController<UIScrollViewDelegate> {
    
    IBOutlet UIScrollView    *scrollView;
    
    IBOutlet UIView          *_introScrollView;
    
    IBOutlet UIPageControl      *pageControl;
    IBOutlet TXButton           *btnstart;
    IBOutlet UIView             *btnView;
    IBOutlet UIView             *bottomView;
    
    IBOutlet UIButton           *btnTop;
    
    NSArray *myCovers;
    
    IBOutlet UILabel *text1;
    IBOutlet UILabel *text2;
    NSMutableArray *_scrollLabel;
}

@property (nonatomic, assign) BOOL isPreOrder;
@property (nonatomic, strong) NSDictionary *detailInfo;

- (IBAction) onClick: (id) sender;

@end

