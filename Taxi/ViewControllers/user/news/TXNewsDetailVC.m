//
//  TXNewsDetailVC.m
//  TXNewsDetailVC
//

//

#import "TXNewsDetailVC.h"
#import "TXAppDelegate.h"
#import "TXApp.h"
#import "utils.h"
#import "utilities.h"

@implementation TXNewsDetailVC

#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];

    [self setNeedsStatusBarAppearanceUpdate];
    
//    [[UIApplication sharedApplication] setStatusBarHidden:YES
//                                            withAnimation:UIStatusBarAnimationFade];
    
    //[self.view setBackgroundColor:UIColorDefault];
    
    [self deregisterForNotifications];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self deregisterForNotifications];
    
    TXAppDelegate *appDelegate = (TXAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSArray *news = nil;
    
    if (_isPreOrder) {
        // 메인에서 보여지는건지
        [btnTop setImage:[UIImage imageNamed:@"btn_close_menu"] forState:UIControlStateNormal];
        CGRect frame = btnTop.frame;
        frame.origin.x = self.view.frame.size.width - btnTop.frame.size.width - kLeftMargin*2;
        btnTop.frame = frame;
        bottomView.hidden = NO;
        
        if (appDelegate.isDriverMode) {
            news = [appDelegate.dicDriver objectForKey:@"news_providers"];
        }
        else {
            news = [appDelegate.dicRider objectForKey:@"news_users"];
        }
    }
    else {
        // 사이드에서 보이는건지
        [btnTop setImage:[UIImage imageNamed:@"btn_back"] forState:UIControlStateNormal];
        CGRect frame = btnTop.frame;
        frame.origin.x = kLeftMargin;
        frame.origin.y = kTopHeight + 10;
        btnTop.frame = frame;
        bottomView.hidden = YES;
        
        [btnTop setImageEdgeInsets:UIEdgeInsetsMake(0,-6,0,0)];
        
        news = [NSArray arrayWithObject:_detailInfo];
        
        frame = scrollView.frame;
        frame.size.height = self.view.frame.size.height - frame.origin.y - kLeftMargin;
        scrollView.frame = frame;
    }

    //[[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    
    _scrollLabel = [[NSMutableArray alloc] init];
    for (NSDictionary *notice in news) {
        [_scrollLabel addObject:notice];
    }
    
    text1.textColor=[UIColor blackColor];
    text2.textColor=HEXCOLOR(0x999999FF);

    text1.text=[[_scrollLabel objectAtIndex:0] objectForKey:@"title"];
    text2.text=StringFormatDate2Date([[_scrollLabel objectAtIndex:0] objectForKey:@"start_date"]);
    [self resetLabel];
    
    // Set the size for the table view
    CGRect viewRect = CGRectMake(0,50,self.view.frame.size.width,self.view.frame.size.height);
    viewRect = CGRectMake(0,40,self.view.frame.size.width, scrollView.frame.size.height);

    //init scollview
    scrollView.delegate = self;
    scrollView.pagingEnabled = YES;
    
    NSMutableArray *viewArray = [[NSMutableArray alloc] init];
    int scrollCount = (int)[_scrollLabel count];
    for (int i = 0; i < scrollCount; i++) {
        
        NSData *tempArchive = [NSKeyedArchiver archivedDataWithRootObject:_introScrollView];
        UIView *aView = [NSKeyedUnarchiver unarchiveObjectWithData:tempArchive];
        __strong UIWebView *webview = (UIWebView*)[aView viewWithTag:4];
        webview.delegate = self;
        webview.tag = 200+i;
        webview.backgroundColor = [UIColor clearColor];
        
//        NSURL *url=[NSURL URLWithString:fullURL];
//        NSURLRequest *requestObj=[NSURLRequest requestWithURL:url];
//        [self.webview loadRequest:requestObj];
        [webview loadHTMLString:_scrollLabel[i][@"content"] baseURL:nil];
        [viewArray addObject:aView];
    }
    [self setObjects:viewArray];
    
    //Adjust
    for (int i = 0; i < [myCovers count]; i++) {
        CGRect frame = scrollView.frame;
        frame.origin.x = scrollView.frame.size.width * i;
        frame.origin.y = 0;
        frame.size = scrollView.frame.size;
        
        //View 1
        UIView *subview1 = [myCovers objectAtIndex:i];
        subview1.contentMode  = UIViewContentModeScaleAspectFit;
        subview1.frame = frame;
        subview1.autoresizingMask = scrollView.autoresizingMask;
        //        NSLog(@"subview1: %@", NSStringFromCGRect(subview1.frame));
        //[subview1 addSubview:[myCovers objectAtIndex:i]];
        [scrollView addSubview:subview1];
        
        //NSLog(@"frame: %@", NSStringFromCGRect(frame));
    }
    scrollView.contentSize = CGSizeMake(scrollView.frame.size.width*([myCovers count]), scrollView.frame.size.height/2);
    pageControl.numberOfPages = [myCovers count];
    pageControl.currentPageIndicatorTintColor = UIColorDefault;
    pageControl.pageIndicatorTintColor = UIColorDotGray;
    pageControl.transform = CGAffineTransformMakeScale(1.6, 1.6);
    
    btnView.backgroundColor = HEXCOLOR(0xf7f7f7FF);
    
    [self.navigationController setToolbarHidden:YES animated:YES];
    
    // button
    btnstart.titleLabel.minimumScaleFactor = 0.5f;
    btnstart.titleLabel.numberOfLines = 1;
    btnstart.titleLabel.adjustsFontSizeToFitWidth = YES;
    
    [[btnstart titleLabel] setFont:[UIFont fontWithName:@"AppleSDGothicNeo-Regular" size:14]];
    NSString *str= LocalizedStr(@"Button.NOMORE");
    [btnstart setTitle:str forState:UIControlStateNormal];
    //[btnstart setTitleColor:UIColorBasicText forState:UIControlStateNormal];
    //[btnstart setBackgroundColor:UIColorDarkButton];
    
    CGSize maximumSize = CGSizeMake(300, 9999);
    UIFont *myFont = [UIFont fontWithName:@"AppleSDGothicNeo-Regular" size:14.0f];
    CGRect textRect = [str boundingRectWithSize:maximumSize
                                        options:NSStringDrawingUsesLineFragmentOrigin| NSStringDrawingUsesFontLeading
                                     attributes:@{NSFontAttributeName:myFont}
                                        context:nil];
    
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(btnstart.frame.origin.x + btnstart.frame.size.width - textRect.size.width,
                                                              btnstart.frame.origin.y + btnstart.frame.size.height,
                                                              textRect.size.width,
                                                              1)];
    bgView.backgroundColor = [UIColor blackColor];
    [bottomView addSubview:bgView];
    
}

//- (BOOL)prefersStatusBarHidden {
//    return YES;
//}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}



- (void)deregisterForNotifications {
}


- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
    
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)resetLabel {
//    CGRect frame = text1.frame;
//    frame.size.width = self.view.frame.size.width - 20 * 2;
//    text1.frame = frame;
    //[text1 sizeToFit];
}

#pragma mark - Selectors
- (IBAction) onClick: (id) sender {
    
    NSDate *date = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:DATE_FORMAT_YYYYMMDD];
    NSString *dateString = [dateFormatter stringFromDate:date];
    [[[TXApp instance] getSettings] setFDKeychain:@"NEWS_NSEQ" value:dateString];
    
    [self onCloseClick:nil];
}

- (IBAction) onCloseClick: (id) sender {
    
    if (_isPreOrder) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark -
#pragma mark Params setting

- (void) setObjects:(NSArray *)covers {
    myCovers = [[NSArray alloc] initWithArray:covers];
}


#pragma mark -
#pragma mark Scrollview delegate

- (void)scrollViewDidEndDecelerating:(UIScrollView *)sender {
    CGFloat pageWidth = scrollView.frame.size.width;
    NSInteger offsetLooping = 1;
    int page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + offsetLooping;
    NSLog(@"page:%d/%lu %f",page,(unsigned long)[myCovers count], pageWidth);
    pageControl.currentPage = page % [myCovers count];
    
    text1.text=[[_scrollLabel objectAtIndex:page] objectForKey:@"title"];
    //text2.text=[[_scrollLabel objectAtIndex:page] objectForKey:@"start_date"];
    text2.text=StringFormatDate2Date([[_scrollLabel objectAtIndex:page] objectForKey:@"start_date"]);
    [self resetLabel];
}



@end
