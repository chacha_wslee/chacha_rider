//
//  MenuViewController.m
//  SlideMenu
//

//

#import "TXNewsCell.h"
#import "TXNewsVC.h"
#import "TXNewsDetailVC.h"
#import "utilities.h"

@interface TXNewsVC() {
    NSArray *items;
    NSArray *_msgList;
    
    NSString *currDate;
}

@end

@implementation TXNewsVC {
    NSMutableDictionary *propertyMap;
}
@synthesize cellIdentifier;


-(void)viewDidLoad {
    
    [super viewDidLoad];
    
    UIView *n = [super navigationView];
    NSInteger _y = n.frame.origin.y + n.frame.size.height;
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, _y, self.view.bounds.size.width, self.view.bounds.size.height-_y)
                                                  style:UITableViewStylePlain];
    self.tableView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:self.tableView];
    
    self.cellIdentifier = @"CellIdentifier";
    [self.tableView registerClass:[TXNewsCell class] forCellReuseIdentifier:self.cellIdentifier];
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    
    self.tableView.estimatedRowHeight = 44.0;
    
//    UINib *nib = [UINib nibWithNibName:@"TXNewsCell" bundle:nil];
//    self.cellIdentifier = @"TXNewsCell";
//    [self.tableView registerNib:nib forCellReuseIdentifier:self.cellIdentifier];
    
    
    //self.tableView.separatorColor = [UIColor clearColor];
    self.tableView.separatorColor = UIColorBasicBack;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.opaque = NO;
    self.tableView.backgroundColor = [UIColor clearColor];
//    self.tableView.tableHeaderView = ({
//        
//        UIView *view = [Utils addLabelView:self.view underView:nil dim:0];
//        
//        UILabel *label = [view viewWithTag:11];
////        label.text = [NSString stringWithFormat:@"CREDIT CARD LIST"];
//        label.tag = 2001;
//        //[label sizeToFit];
//        label.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
//        
//        view;
//    });
    
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    self.tableView.scrollsToTop = YES;
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self removeEventListeners];
}

#pragma mark - Configure

-(void)configureStyles {
    [super configureStyles];
    
    if (appDelegate.isDriverMode) {
        [super navigationType01X:[UIImage imageNamed:@"btn_back"] centerText:LocalizedStr(@"Menu.News.title")];
    }
    else {
        [super navigationType01X:[UIImage imageNamed:@"btn_back"] centerText:LocalizedStr(@"Menu.News.title")];
    }
}

-(void)configure {
    [super configure];
    [super configureBottomLine];
    
    NSDate *date = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:DATE_FORMAT_YYYYMMDD];
    currDate = [dateFormatter stringFromDate:date];
    //currDate = StringFormatDate2Date(currDate);
    //currDate = @"02/02/2017";
    
    // 2. init event
    _msgList = @[
                 @[@"UR503",@""], // rider
                 @[@"PP503",@""], // driver
                 ];
    
    [self registerEventListeners];
    
    propertyMap = [[NSMutableDictionary alloc] init];
    
    if (appDelegate.isDriverMode) {
        [self configureDriver];
    }
    else {
        [self configureRider];
    }
    
}

-(void) configureDriver {
    
    // 3. call api
    [self showBusyIndicator:@"Requesting ... "];
    [self->model PP503];
}

-(void) configureRider {

    // 3. call api
    [self showBusyIndicator:@"Requesting ... "];
    [self->model UR503];
}

-(void) registerEventListeners {
    for (NSArray *msg in _msgList) {
        [self->model addEventListener:self forEvent:[msg objectAtIndex:0] eventParams:nil];
    }
}

-(void) removeEventListeners {
    for (NSArray *msg in _msgList) {
        [self->model removeEventListener:self forEvent:[msg objectAtIndex:0]];
    }
}

#pragma mark - Action
-(IBAction)onNaviButtonClick:(id)sender {
    UIButton *btn = (UIButton*)sender;
    NSLog(@"onNaviButtonClick");
    //[super onNaviButtonClick:sender];
    
    if (btn.tag == 1100) {
        [self removeEventListeners];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark - UITableView Delegate & Datasrouce -
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [tableView setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [tableView setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
    
    UIView *seperatorView = [[UIView alloc] initWithFrame:CGRectMake(10, cell.frame.size.height - 1, cell.frame.size.width - 10*2, 1)];
    seperatorView.backgroundColor = UIColorTableSeperator;
    [cell addSubview:seperatorView];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return self->items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TXNewsCell *cell = [tableView dequeueReusableCellWithIdentifier:self.cellIdentifier];
    
    // Configure the cell for this indexPath
    NSDictionary *dic = self->items[indexPath.row];
    
    cell.titleLabel.text =  [dic valueForKey:@"title"];
    cell.dateLabel.text =   StringFormatDate2Date([dic valueForKey:@"start_date"]);
    
    NSString *diff = StringFormatDate2Date([dic valueForKey:@"start_date"]);
    if ([currDate isEqualToString:diff]) {
        cell.imgFlag.hidden = NO;
    }
    else {
        cell.imgFlag.hidden = YES;
    }
    
    // Make sure the constraints have been added to this cell, since it may have just been created from scratch
    [cell setNeedsUpdateConstraints];
    [cell updateConstraintsIfNeeded];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSDictionary *dic = self->items[indexPath.row];
    
    TXNewsDetailVC *vc = [[TXNewsDetailVC alloc] initWithNibName:@"TXNewsDetailVC" bundle:nil];
    vc.view.frame = self.view.frame;
    vc.isPreOrder = NO;
    vc.detailInfo = dic;
    [self pushViewController:(TXBaseViewController*)vc];
}

-(NSAttributedString*)convertHtmlText:(NSString*)HTMLString{
    
     NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[HTMLString dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    
    return attrStr;
}

#pragma mark - onEvent
-(void)onEvent:(TXEvent *)event eventParams:(id)subscriptionParams {
    
    TXResponseDescriptor * descriptor = [event getEventProperty:TXEvents.Params.DESCRIPTOR];
    TXRequestObj *request = [event getEventProperty:TXEvents.Params.REQUEST];
    
    
    if([event.name isEqualToString:@"UR503"]) {
        
        if(descriptor.success == true) {
            NSString     *response = [[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding];
            NSDictionary *result   = getJSONObj(response);

            self->items              = [result valueForKey:@"news_users"];
            
            [self.tableView reloadData];
            
        } else {
            //에러주석[self alertError:@"Error" message:descriptor.error];
        }
    }
    else if([event.name isEqualToString:@"PP503"]) {
        
        if(descriptor.success == true) {
            NSString     *response = [[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding];
            NSDictionary *result   = getJSONObj(response);
            
            self->items              = [result valueForKey:@"news_providers"];
            
            [self.tableView reloadData];
            
        } else {
            //에러주석[self alertError:@"Error" message:descriptor.error];
        }
    }
    [self hideBusyIndicator];
}


@end
