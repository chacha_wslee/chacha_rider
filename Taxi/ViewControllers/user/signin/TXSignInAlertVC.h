//
//  TXSignInVC.h

//
#import "TXUserModel.h"
#import "TXUserVC.h"

@interface TXSignInAlertVC : TXUserVC

@property (retain, nonatomic) IBOutlet UIImageView    *splash_background;
@property (retain, nonatomic) IBOutlet UILabel      *lbTitle;
@property (retain, nonatomic) IBOutlet UIButton     *btnClose;
@property (retain, nonatomic) IBOutlet UILabel      *lbBody;

@property (retain, nonatomic) IBOutlet UIView       *vBg;

@end
