//
//  TXSnsLoginVC.m

//

#import "TXSnsLoginVC.h"
#import "TXSignInVC.h"

#import "TXMainVC.h"
#import "TXUserModel.h"
#import "TXSharedObj.h"

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

#import <SafariServices/SafariServices.h>
#import <NaverThirdPartyLogin/NaverThirdPartyLogin.h>

#import <KakaoOpenSDK/KakaoOpenSDK.h>

@interface TXSnsLoginVC () <NaverThirdPartyLoginConnectionDelegate>{
    NaverThirdPartyLoginConnection *_thirdPartyLoginConn;
    
    NSDictionary *resultDic;
    
}
@property (retain, nonatomic) IBOutlet UIImageView    *splash_background;
@end

@implementation TXSnsLoginVC

#pragma mark - UIViewController

-(void)configureStyles {
    [super configureStyles];
    
    [super navigationType01X:[UIImage imageNamed:@"login_return"] centerText:@""];
}

-(void) configure {

    [super configure];
    
    [super statusBarStyle];
    
    [appDelegate startLocationUpdateSubscription];
    
    UIButton *btn = [self.view viewWithTag:13];
    if (self.setupMode == 88) {
        // login
        [btn setImage:[UIImage imageNamed:@"login_email_box"] forState:UIControlStateNormal];
    }
    else if (self.setupMode == 99) {
        // signup
        [btn setImage:[UIImage imageNamed:@"signup_email_box"] forState:UIControlStateNormal];
    }
}

#pragma mark ViewLoad

-(void)viewDidLoad {
    [super viewDidLoad];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    //[[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    //[[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
    
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}

-(BOOL)prefersStatusBarHidden{
    return NO;
}


-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

-(void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
//    NSArray* tempVCA = [self.navigationController viewControllers];
//    
//    for(UIViewController *tempVC in tempVCA)
//    {
//        if([tempVC isKindOfClass:[TXSnsLoginVC class]])
//        {
//            [tempVC removeFromParentViewController];
//        }
//    }
    
    //[self removeFromParentViewController];
//    if self.navigationController != nil{
//        for viewController in (self.navigationController?.viewControllers)!{
//            if viewController.isKindOfClass(MyVCKind.self){
//                self.navigationController?.viewControllers.removeAtIndex((self.navigationController?.viewControllers.indexOf(viewController))!)
//            }
//        }
//    }
}

- (void)dealloc
{
    _thirdPartyLoginConn.delegate = nil;
}


-(void) configureFieldStyles {
    
}

-(void)didBeginEditing:(id)sender {
    [self configureFieldStyles];
}


#pragma mark - Action
-(IBAction)onNaviButtonClick:(id)sender {
    UIButton *btn = (UIButton*)sender;
    
    if (btn.tag == 1100) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

-(IBAction)onLoginButtonClick:(id)sender {
    UIButton *btn = (UIButton*)sender;
    
    if (btn.tag == 10) {
        /*
        [[[LGAlertView alloc] initWithTitle:nil
                                    message:LocalizedStr(@"String.Service.Ready")
                                      style:LGAlertViewStyleAlert
                               buttonTitles:@[LocalizedStr(@"Button.OK")]
                          cancelButtonTitle:nil
                     destructiveButtonTitle:nil
                              actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                              }
                              cancelHandler:^(LGAlertView *alertView) {
                              }
                         destructiveHandler:^(LGAlertView *alertView) {
                         }] showAnimated:YES completionHandler:nil];
        */

        //
        // for naver
        _thirdPartyLoginConn = [NaverThirdPartyLoginConnection getSharedInstance];
        _thirdPartyLoginConn.delegate = self;
        
        [_thirdPartyLoginConn setIsNaverAppOauthEnable:YES];
        [_thirdPartyLoginConn setIsInAppOauthEnable:YES];
        [_thirdPartyLoginConn setServiceUrlScheme:kNaverServiceAppUrlScheme];
        [_thirdPartyLoginConn setConsumerKey:kNaverConsumerKey];
        [_thirdPartyLoginConn setConsumerSecret:kNaverConsumerSecret];
        [_thirdPartyLoginConn setAppName:kNaverServiceAppName];
        
        if ([_thirdPartyLoginConn isValidAccessTokenExpireTimeNow]) {
            // 이미 로그인이 되어있다.
            [self naverProfile];
        }
        else {
            [_thirdPartyLoginConn requestThirdPartyLogin];
        }
    }
    else if (btn.tag == 11) {
        
        if ([FBSDKAccessToken currentAccessToken]) {
            // User is logged in, do work such as go to next view controller.
            NSLog(@"facebook aleady login");
            
            [self facebookProfile];
        }
        else {
            //
            FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
            [login logInWithReadPermissions: @[@"public_profile", @"email"]
                         fromViewController:self
                                    handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
                                        if (error) {
                                            NSLog(@"Process error");
                                            [self nextStepError];
                                        } else if (result.isCancelled) {
                                            NSLog(@"Cancelled");
                                            [self nextStepError];
                                        } else {
                                            //거부된 권한
                                            if ([result.declinedPermissions containsObject:@"email"]) {
                                                // TODO: do not request permissions again immediately. Consider providing a NUX
                                                // describing  why the app want this permission.
                                                [self nextStepError];
                                            } else {
                                                // ...
                                                [self facebookProfile];
                                            }
                                            NSLog(@"Logged in");
                                        }
                                    }];
        }
        
    }
    else if (btn.tag == 12) {
        //
        [KOSessionTask accessTokenInfoTaskWithCompletionHandler:^(KOAccessTokenInfo *accessTokenInfo, NSError *error) {
            if (error) {
                switch (error.code) {
                    case KOErrorDeactivatedSession:
                        // 세션이 만료된(access_token, refresh_token이 모두 만료된 경우) 상태
                        
                        break;
                    default:
                        // 예기치 못한 에러. 서버 에러
                        
                        break;
                }
                // ensure old session was closed
                [[KOSession sharedSession] close];
                
                [[KOSession sharedSession] openWithCompletionHandler:^(NSError *error) {
                    if ([[KOSession sharedSession] isOpen]) {
                        // login success
                        NSLog(@"login succeeded.");
                        [self kakaoProfile];
                    } else {
                        // failed
                        NSLog(@"login failed.");
                        [self nextStepError];
                    }
                }];
            } else {
                // 성공 (토큰이 유효함)
                NSLog(@"남은 유효시간: %@ (단위: ms)", accessTokenInfo.expiresInMillis);
                [self kakaoProfile];
            }
        }];
    }
    else if (btn.tag == 13) {
        //  88(login), 99(signup)
        if (self.setupMode == 88) {
            [self onSignIn];
        }
        else if (self.setupMode == 99) {
            [self onSignUp];
        }
    }
}

-(void)onSignUp {
    // 가입하기
    TXSignInVC *signInVC = [[TXSignInVC alloc] initWithNibName:@"TXSignInVC" bundle:nil mode:0 profile:resultDic];
    signInVC.view.frame = [super view].frame;
    [self pushViewController:signInVC];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1.0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [self willMoveToParentViewController:nil];
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
        [self didMoveToParentViewController:nil];
    });
}

-(void)onSignIn {
    // 로그인하기
    TXSignInVC *signInVC = [[TXSignInVC alloc] initWithNibName:@"TXSignInVC" bundle:nil mode:1 profile:resultDic];
    signInVC.view.frame = [super view].frame;
    [self pushViewController:signInVC];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1.0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [self willMoveToParentViewController:nil];
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
        [self didMoveToParentViewController:nil];
    });
}

-(void)nextStep
{
    if (resultDic) {
        //  88(login), 99(signup)
        if (self.setupMode == 88) {
            [self onSignIn];
        }
        else if (self.setupMode == 99) {
            [self onSignUp];
        }
    }
}

-(void)nextStepError
{
    [appDelegate onCommonErrorAlert:LocalizedStr(@"Intro.sns.login.error")];
}

#pragma mark - OAuth20 deleagate

- (void)oauth20ConnectionDidOpenInAppBrowserForOAuth:(NSURLRequest *)request {
//    [self presentWebviewControllerWithRequest:request];
}

#pragma mark - Naver WebView
- (void)oauth20ConnectionDidFinishRequestACTokenWithAuthCode {
    NSString *errString = [NSString stringWithFormat:@"OAuth Success!\n\nAccess Token - %@\n\nAccess Token Expire Date- %@\n\nRefresh Token - %@", _thirdPartyLoginConn.accessToken, _thirdPartyLoginConn.accessTokenExpireDate, _thirdPartyLoginConn.refreshToken];
    
    NSLog(@"%s=[%@]", __FUNCTION__, errString);
    
    [self naverProfile];
}

- (void)oauth20ConnectionDidFinishRequestACTokenWithRefreshToken {
    NSString *errString = [NSString stringWithFormat:@"Refresh Success!\n\nAccess Token - %@\n\nAccess sToken ExpireDate- %@", _thirdPartyLoginConn.accessToken, _thirdPartyLoginConn.accessTokenExpireDate];
    
    NSLog(@"%s=[%@]", __FUNCTION__, errString);
    
}
- (void)oauth20ConnectionDidFinishDeleteToken {
    NSString *errString = [NSString stringWithFormat:@"인증해제 완료"];
    
    NSLog(@"%s=[%@]", __FUNCTION__, errString);
}


#pragma mark - NaverApp
- (void)oauth20Connection:(NaverThirdPartyLoginConnection *)oauthConnection didFinishAuthorizationWithResult:(THIRDPARTYLOGIN_RECEIVE_TYPE)recieveType
{
    NSLog(@"Getting auth code from NaverApp success!");
    
    [self naverProfile];
}

- (void)oauth20Connection:(NaverThirdPartyLoginConnection *)oauthConnection didFailAuthorizationWithRecieveType:(THIRDPARTYLOGIN_RECEIVE_TYPE)recieveType
{
    NSLog(@"NaverApp login fail handler");
    
    [self nextStepError];
}

- (void)oauth20Connection:(NaverThirdPartyLoginConnection *)oauthConnection didFailWithError:(NSError *)error {
    NSLog(@"%s=[%@]", __FUNCTION__, error);
    
    [self nextStepError];
}

#pragma mark - Profile
-(void)naverProfile
{
    //https://developers.naver.com/docs/login/devguide/
    //NSString *urlString = @"https://apis.naver.com/nidlogin/nid/getUserProfile.xml";  // 사용자 프로필 호출
    NSString *urlString = @"https://openapi.naver.com/v1/nid/me";
    
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    
    NSString *authValue = [NSString stringWithFormat:@"Bearer %@", _thirdPartyLoginConn.accessToken];
    
    [urlRequest setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    NSHTTPURLResponse *response = nil;
    NSError *error = nil;
    NSData *receivedData = [NSURLConnection sendSynchronousRequest:urlRequest returningResponse:&response error:&error];
    //NSString *decodingString = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
    
    if (error) {
        NSLog(@"Error happened - %@", [error description]);
    } else {
        NSDictionary *dic= getJSONObjFromData(receivedData);
        NSString *_id = @"";
        NSString *_name = @"";
        NSString *_email = @"";
        NSString *_nickname = @"";
        NSString *_gender = @"";
        NSString *_age = @"";
        NSString *_birthday = @"";
        NSString *_profile_image = @"";
        
        if ([dic[@"response"] valueForKey:@"id"]) {
            _id = [NSString stringWithFormat:@"%@",[dic[@"response"] valueForKey:@"id"]];
        }
        if ([dic[@"response"] valueForKey:@"name"]) {
            _name = [NSString stringWithFormat:@"%@",[dic[@"response"] valueForKey:@"name"]];
        }
        if ([dic[@"response"] valueForKey:@"email"]) {
            _email = [NSString stringWithFormat:@"%@",[dic[@"response"] valueForKey:@"email"]];
        }
        if ([dic[@"response"] valueForKey:@"nickname"]) {
            _nickname = [NSString stringWithFormat:@"%@",[dic[@"response"] valueForKey:@"nickname"]];
        }
        if ([dic[@"response"] valueForKey:@"gender"]) {
            _gender = [NSString stringWithFormat:@"%@",[dic[@"response"] valueForKey:@"gender"]];
        }
        if ([dic[@"response"] valueForKey:@"age"]) {
            _age = [NSString stringWithFormat:@"%@",[dic[@"response"] valueForKey:@"age"]];
        }
        if ([dic[@"response"] valueForKey:@"birthday"]) {
            _birthday = [NSString stringWithFormat:@"%@",[dic[@"response"] valueForKey:@"birthday"]];
        }
        if ([dic[@"response"] valueForKey:@"profile_image"]) {
            _profile_image = [NSString stringWithFormat:@"%@",[dic[@"response"] valueForKey:@"profile_image"]];
        }
        
        /*
         {"resultcode":"00","message":"success","response":{"nickname":"ds****","enc_id":"9e34572acef5decf71e10302b8f5ab8869ad190058d18438eef109fda6cb75f3","profile_image":"https:\/\/ssl.pstatic.net\/static\/pwe\/address\/img_profile.png","age":"40-49","gender":"M","id":"7918940","name":"\uc2e0\uc2b9\ubd09","email":"ds5zft@naver.com","birthday":"08-16"}}*/
        resultDic = @{@"socialsite":@"naver",
                      @"id":_id,
                      @"nickname":_nickname,
                      @"name":_name,
                      @"email":_email,
                      @"gender":_gender,
                      @"age":_age,
                      @"birthday":_birthday,
                      @"profile_image":_profile_image
                      };
        
        NSLog(@"recevied data - %@", dic);
//        NSLog(@"recevied data - %@", decodingString);
        NSLog(@"recevied data - %@", resultDic);
    }
    
    [self nextStep];
}

-(void)facebookProfile
{
    //https://developers.facebook.com/docs/graph-api/reference/user
    FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc]
                                  initWithGraphPath:@"/me"
                                  parameters:@{ @"fields": @"id,first_name,name,email,gender,age_range,locale,picture",}
                                  HTTPMethod:@"GET"];
    [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
        // Insert your code here
        NSDictionary *dic = (NSDictionary*)result;
        NSString *_id = @"";
        NSString *_first_name = @"";
        NSString *_name = @"";
        NSString *_email = @"";
        NSString *_gender = @"";
        NSString *_age_range = @"";
        NSString *_locale = @"";
        NSString *_picture = @"";
        
        if ([dic valueForKey:@"id"]) {
            _id = [NSString stringWithFormat:@"%@",[dic valueForKey:@"id"]];
        }
        if ([dic valueForKey:@"first_name"]) {
            _first_name = [NSString stringWithFormat:@"%@",[dic valueForKey:@"first_name"]];
        }
        if ([dic valueForKey:@"name"]) {
            _name = [NSString stringWithFormat:@"%@",[dic valueForKey:@"name"]];
        }
        if ([dic valueForKey:@"email"]) {
            _email = [NSString stringWithFormat:@"%@",[dic valueForKey:@"email"]];
        }
        if ([dic valueForKey:@"gender"]) {
            _gender = [NSString stringWithFormat:@"%@",[dic valueForKey:@"gender"]];
        }
        if ([dic valueForKey:@"age_range"]) {
            _age_range = [NSString stringWithFormat:@"%@",[dic valueForKey:@"age_range"][@"min"]];
        }
        if ([dic valueForKey:@"locale"]) {
            _locale = [NSString stringWithFormat:@"%@",[dic valueForKey:@"locale"]];
        }
        if ([dic valueForKey:@"picture"]) {
            _picture = [NSString stringWithFormat:@"%@",[dic valueForKey:@"picture"][@"data"][@"url"]];
        }
        
        /*
        {
            "age_range" =     {
                min = 21;
            };
            email = "sbshin@strastar.com";
            "first_name" = Seungbong;
            gender = male;
            id = 10215464164422001;
            locale = "ko_KR";
            name = "Seungbong Shin";
            picture =     {
                data =         {
                    height = 50;
                    "is_silhouette" = 1;
                    url = "https://scontent.xx.fbcdn.net/v/t1.0-1/c15.0.50.50/p50x50/10354686_10150004552801856_220367501106153455_n.jpg?oh=baf3745408876788393e9ca2b7e1dc94&oe=5AEBF02F";
                    width = 50;
                };
            };
        }
        */
        resultDic = @{@"socialsite":@"facebook",
                      @"id":_id,
                      @"first_name":_first_name,
                      @"name":_name,
                      @"email":_email,
                      @"gender":_gender,
                      @"age_range":_age_range,
                      @"locale":_locale,
                      @"picture":_picture
                      };
        
        NSLog(@"result:%@",result);
        NSLog(@"recevied data - %@", resultDic);
        
        [self nextStep];
    }];
}

-(void)kakaoProfile
{
    [KOSessionTask meTaskWithCompletionHandler:^(KOUser* result, NSError *error) {
        if (result) {
            // success
            NSString *_id = [NSString stringWithFormat:@"%@",result.ID];
            NSString *_nickname = @"";
            NSString *_email = @"";
            NSString *_emailverified = @"";
            NSString *_profile_image = @"";
            NSString *_thumbnail_image = @"";
            
            _nickname = [Utils nullToString:[result propertyForKey:KOUserNicknamePropertyKey]];
            _profile_image = [Utils nullToString:[result propertyForKey:KOUserProfileImagePropertyKey]];
            _thumbnail_image = [Utils nullToString:[result propertyForKey:KOUserThumbnailImagePropertyKey]];
            
            if (result.email) {
                _email = [NSString stringWithFormat:@"%@",result.email];
                _emailverified = [NSString stringWithFormat:@"%d",result.verifiedEmail];
            } else {
                // disagreed
            }
            
            resultDic = @{@"socialsite":@"kakao",
                          @"id":_id,
                          @"nickname":_nickname,
                          @"kaccount_email":_email,
                          @"kaccount_email_verified":_emailverified,
                          @"profile_image":_profile_image,
                          @"thumbnail_image":_thumbnail_image
                          };
            NSLog(@"recevied data - %@", resultDic);
        } else {
            // failed
        }
        
        [self nextStep];
    }];
}

@end
