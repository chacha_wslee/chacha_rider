//
//  TXSignInVC.m

//

#import "TXSignInVC.h"
#import "TXSignInAlertVC.h"
#import "TXSnsLoginVC.h"

#import "TXMainVC.h"
#import "TXUserModel.h"
#import "TXSharedObj.h"

//#import "RedisSingleton.h"

#import "TXMapVC.h"
#import "LGAlertView.h"
#import "TXSearchVC.h"

#import "Validator.h"
#import "WToast.h"
//#import "CRToast.h"
@import IQKeyboardManager;

@interface TXSignInVC () <UITextFieldDelegate,ValidatorDelegate,RTLabelDelegate>{
    CLLocationManager *locationMgr;
    
    NSString *lastFreeUsername;
    
    NSArray *_msgList;
    NSInteger validatorIndex;
    NSTimer *waitTimer;
    NSInteger waitTimerTime;
    
    NSString *datoken;
    
    BOOL isChanged;
    id observer;
    id observer2;
    
    BOOL isSignup;
}
@property (retain, nonatomic) IBOutlet UIImageView    *splash_background;
@end

@implementation TXSignInVC


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil mode:(NSInteger)mode profile:(NSDictionary*)profile
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.setupMode = mode;
        isChanged = NO;
        isSignup = NO;
        appDelegate.signupViewStep = mode;
        datoken = nil;
        _snsProfile = profile;
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil mode:(NSInteger)mode
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.setupMode = mode;
        isChanged = NO;
        isSignup = NO;
        appDelegate.signupViewStep = mode;
        datoken = nil;
    }
    return self;
}

#pragma mark - UIViewController

-(void)configureStyles {
    [super configureStyles];
    [self configureFieldStyles];
}

-(void) configure {

    [super configure];
    //[self initControls];
    
    validatorIndex = 0;
    
    [super statusBarStyle];
    
    [appDelegate startLocationUpdateSubscription];
    
    [self stopTimer];
    //[self refreshSignUpButton];
    
    if (self.setupMode != 3) {
        //[self->model onCleanSession];
    }

    // 2. init event
    _msgList = @[
                 @[@"UR111",@""], // 인증번호
                 @[@"UR110",@""], // 로그인
                 @[@"UR120",@""], // 생성
//                 @[@"UR311",@""], // pay_token
                 
                 @[@"PP111",@""], // 인증번호
                 @[@"PP110",@""], // 로그인
                 @[@"PP120",@""], // 생성
                 
                 @[@"UR113",@""], // 인증메일 요청
                 @[@"PP113",@""], // 인증메일 요청
                 
//                 @[@"UR212",@""], // 인증번호-로그인후
//                 @[@"PP212",@""], // 인증번호-로그인후
                 
                 @[@"UR210",@""], // 전화번호 변경
                 @[@"PP210",@""], // 전화번호 변경
                 
//                 @[@"UR211",@""], // 약관동의
//                 @[@"PP211",@""], // 약관동의
                 ];
    if (self.setupMode == 5) {
        _msgList = @[
                     @[@"UR211",@""], // 약관동의
                     @[@"PP211",@""], // 약관동의
                     ];
    }
    
    if (self.setupMode == 1) {
        // 회원로그인
        //[super navigationType11X:[UIImage imageNamed:@"btn_back"] centerText:LocalizedStr(@"Intro.Login.title") rightText:LocalizedStr(@"Intro.Next.title")];
        //[super navigationType11X:nil centerText:LocalizedStr(@"Intro.Login.title") rightText:LocalizedStr(@"Intro.Next.title")];
    }
    else if (self.setupMode == 0) {
        // 회원가입
        //[super navigationType11X:[UIImage imageNamed:@"btn_back"] centerText:LocalizedStr(@"Intro.SignUp.title") rightText:LocalizedStr(@"Intro.Next.title")];
    }
    else if (self.setupMode == 2) {
        // 비번찾기
        //[super navigationType11X:[UIImage imageNamed:@"btn_back"] centerText:LocalizedStr(@"Intro.FindPasswd.title") rightText:LocalizedStr(@"Intro.Next.title")];
    }
    else if (self.setupMode == 3) {
        // 전화번호변경
        //[super navigationType11X:[UIImage imageNamed:@"btn_back"] centerText:LocalizedStr(@"Intro.ChangeTelno.title") rightText:LocalizedStr(@"Intro.Save.title")];
    }
    else if (self.setupMode == 4) {
        // 비밀번호 초기화
        //[super navigationType11X:[UIImage imageNamed:@"btn_back"] centerText:LocalizedStr(@"Intro.ChangeTelno.title") rightText:LocalizedStr(@"Intro.Save.title")];
    }
    
#ifdef _WITZM
    
#ifdef _DRIVER_MODE
    [self.splash_background setImage:[UIImage imageNamed:@"spalsh_background_driver"]];
#else
    [self.splash_background setImage:[UIImage imageNamed:@"spalsh_background"]];
#endif
    
#elif defined _CHACHA
#endif
    
    [self registerEventListeners];

    [self nextButtonOn:NO];
}

- (void)nextButtonOn:(BOOL)on
{
    UIButton *btn = [super.view viewWithTag:1300];
    btn.enabled = on;
    if (on)
        [btn setTitleColor:UIColorBasicTextOn forState:UIControlStateNormal];
    else
        [btn setTitleColor:UIColorBasicTextOff forState:UIControlStateNormal];
}

-(void) initControls {
    //self.view.backgroundColor = colorFromRGB(102, 178, 255, 1);
    // 0: signup 1: signin 2:reset passwd 3:profile, 4:new pw, 5:term
    // 0: name, email, phone, passwwd, code
    // 1:       email, phone, passwwd, code
    // 2:       email, ncode
    // 3:       email, phone, code
    // 4:       passwwd, newpasswwd, ncode
    // 5:       term1, term2, term4, term3
    
    self.vUsername.hidden = YES;
    self.vUsernameL.hidden = YES;
    self.vEmail.hidden = YES;
    self.vPhone.hidden = YES;
    self.vPasswd.hidden = YES;
    self.vPasswdNew.hidden = YES;
    self.vPasswdButton.hidden = YES;
    self.vCode.hidden = YES;
    self.vNew.hidden = YES;

    self.vTerms1.hidden = YES;
    self.vTerms2.hidden = YES;
    self.vTerms3.hidden = YES;
    self.vTerms4.hidden = YES;
    self.vTerms0.hidden = YES;
    
    CGRect rect = self.vbody.frame;
    rect.size.width = self.view.frame.size.width;
    rect.size.height = self.view.frame.size.height-20;
    self.vbody.frame = rect;
    
    CGFloat vHeight = self.view.frame.size.height;
    
    if (self.setupMode == 0) {
        [self showTitle];
        
        [self showPhone:self.lbTitle.frame dim:26];
        [self showEmail:self.vPhone.frame dim:26];
//#ifdef _DRIVER_MODE
//        [self showUsername:self.vEmail.frame dim:26];
//        [self showUsernameL:self.vUsername.frame dim:26];
//        [self showPasswd:self.vUsernameL.frame dim:26];
//#else
//        [self showUsername:self.vEmail.frame dim:26];
//        [self showPasswd:self.vUsername.frame dim:26];
//#endif
        [self showUsername:self.vEmail.frame dim:26];
        if (self.snsProfile) {
            self.txtEmail.text = self.snsProfile[@"email"];
            self.txtUsername.text = self.snsProfile[@"name"];
            if (self.snsProfile[@"kaccount_email"]) {
                self.txtEmail.text = self.snsProfile[@"kaccount_email"];
                self.txtUsername.text = self.snsProfile[@"nickname"];
            }
            
#ifdef _DRIVER_MODE
            self.txtUsername.text = @"";
#endif
            // email이 있으면 수정못함
            if (![self.txtEmail.text isEqualToString:@""]) {
                self.txtEmail.enabled = NO;
            }
            
            [self showCode:self.vUsername.frame dim:30];
        }
        else {
            [self showPasswd:self.vUsername.frame dim:26];
            [self showCode:self.vPasswd.frame dim:30];
        }
        
        vHeight = self.vCode.frame.origin.y + self.vCode.frame.size.height;
    }
    else if (self.setupMode == 1) {
        [self showTitle];
        
        [self showPhone:self.lbTitle.frame dim:26];
        [self showEmail:self.vPhone.frame dim:26];
        if (self.snsProfile) {
            self.txtEmail.text = self.snsProfile[@"email"];
            if (self.snsProfile[@"kaccount_email"]) {
                self.txtEmail.text = self.snsProfile[@"kaccount_email"];
            }
            
            // email이 있으면 수정못함
            if (![self.txtEmail.text isEqualToString:@""]) {
                self.txtEmail.enabled = NO;
            }
            
            [self showCode:self.vEmail.frame dim:40];
        }
        else {
            [self showPasswd:self.vEmail.frame dim:26];
            [self showPasswdButton:self.vPasswd.frame dim:26];
            
            [self showCode:self.vPasswdButton.frame dim:40];
        }
        vHeight = self.vCode.frame.origin.y + self.vCode.frame.size.height;
    }
    else if (self.setupMode == 2) {
        [self showTitle];
        
        [self showEmail:self.lbTitle.frame dim:83];
        
        [self.vbody addSubview:self.vReset];
        CGRect frame = self.vReset.frame;
        frame.origin.x = self.lbTitle.frame.origin.x;
        frame.size.width = self.view.frame.size.width - self.lbTitle.frame.origin.x * 2;
        self.vReset.frame = frame;
        [self showReset:self.vEmail.frame dim:20];
        
        vHeight = self.vReset.frame.origin.y + self.vReset.frame.size.height;
    }
    else if (self.setupMode == 3) {
        [self showTitle];
        
        [self showEmail:self.lbTitle.frame dim:20];
        [self showPhone:self.vEmail.frame dim:20];
        
        [self showCode:self.vPhone.frame dim:20];
        
        vHeight = self.vCode.frame.origin.y + self.vCode.frame.size.height;
    }
    else if (self.setupMode == 4) {
        [self showTitle];
        
        [self showPasswd:self.lbTitle.frame dim:71];
        [self showPasswdNew:self.vPasswd.frame dim:20];
        
        [self.vbody addSubview:self.vNew];
        CGRect frame = self.vNew.frame;
        frame.origin.x = self.lbTitle.frame.origin.x;
        frame.size.width = self.view.frame.size.width - self.lbTitle.frame.origin.x * 2;
        self.vNew.frame = frame;
        [self showNew:self.vPasswdNew.frame dim:5];
        
        vHeight = self.vNew.frame.origin.y + self.vNew.frame.size.height;
    }
    else if (self.setupMode == 5) {
        [self showTitle];
        
        [self showTerms0:self.lbTitle.frame dim:34];
        [self showTerms1:self.vTerms0.frame dim:0];
        [self showTerms2:self.vTerms1.frame dim:0];
#ifdef _DRIVER_MODE
        [self showTerms4:self.vTerms2.frame dim:0];
#else
        [self showTerms3:self.vTerms2.frame dim:0];
        [self showTerms4:self.vTerms3.frame dim:0];
#endif
        vHeight = self.vTerms4.frame.origin.y + self.vTerms4.frame.size.height;
    }
    

    [self receiveSearchNotification:nil];
    //[self startTimer];
    
    rect = self.vbody.frame;
    rect.size.width = self.view.frame.size.width;
    rect.size.height = (self.view.frame.size.height>vHeight?self.view.frame.size.height:vHeight);
    self.vbody.frame = rect;
}

- (void)showTitle {
    
    [Utils labelAttrHeader:self.lbTitle];
    
    [Utils buttonAttrRight:self.btnSignUp];
    
    self.vSignUp.backgroundColor = UIColorDefault2;
    
    // top header
    if (self.setupMode == 0) {
        self.lbTitle.text = LocalizedStr(@"Intro.SignUp.title");
        [self.btnSignUp setTitle:LocalizedStr(@"Intro.Login.title") forState:UIControlStateNormal];
        self.btnSignUp.tag = 1100;
        
        [self.btnSignUp addTarget:self action:@selector(onNaviButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    else if (self.setupMode == 1) {
        self.lbTitle.text = LocalizedStr(@"Intro.Login.title");
        [self.btnSignUp setTitle:LocalizedStr(@"Intro.SignUp.title") forState:UIControlStateNormal];
        
        [self.btnSignUp addTarget:self action:@selector(onTerms:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    else if (self.setupMode == 2) {
        self.lbTitle.text = LocalizedStr(@"Intro.ResetPasswd.title");
        [self.btnSignUp setTitle:LocalizedStr(@"Button.CANCEL") forState:UIControlStateNormal];
        self.vSignUp.hidden = YES;
        
        self.btnSignUp.tag = 1100;
        //[self.btnSignUp setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        [self.btnSignUp addTarget:self action:@selector(onNaviButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    else if (self.setupMode == 3) {
        self.lbTitle.text = LocalizedStr(@"Intro.ChangeTelno.title");
        [self.btnSignUp setTitle:LocalizedStr(@"Button.CANCEL") forState:UIControlStateNormal];
        self.vSignUp.hidden = YES;
        
        self.btnSignUp.tag = 1100;
        //[self.btnSignUp setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        [self.btnSignUp addTarget:self action:@selector(onNaviButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    else if (self.setupMode == 4) {
        self.lbTitle.text = LocalizedStr(@"Intro.NewPasswd.title");
        [self.btnSignUp setTitle:LocalizedStr(@"Button.CANCEL") forState:UIControlStateNormal];
        self.vSignUp.hidden = YES;
        
        self.btnSignUp.tag = 1100;
        //[self.btnSignUp setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        [self.btnSignUp addTarget:self action:@selector(onNaviButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    else if (self.setupMode == 5) {
        self.lbTitle.text = LocalizedStr(@"Intro.Terms.title");
        [self.btnSignUp setTitle:LocalizedStr(@"Button.CANCEL") forState:UIControlStateNormal];
        self.vSignUp.hidden = YES;
        //self.btnSignUp.hidden = YES;
        
        self.btnSignUp.tag = 1100;
        //[self.btnSignUp setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        [self.btnSignUp addTarget:self action:@selector(onNaviButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    }
}

- (void)showPasswdButton:(CGRect)vrect dim:(NSInteger)dim {
    
    CGRect frame = self.vPasswdButton.frame;
    frame.origin.y = vrect.origin.y + vrect.size.height + dim;
    self.vPasswdButton.frame= frame;
    self.vPasswdButton.hidden = NO;
    
    //self.btnForgotPassword.titleLabel.text = LocalizedStr(@"Intro.FindPasswd.title");
    [self.btnForgotPassword setTitleColor:UIColorLabelTextColor forState:UIControlStateNormal];
    [self.btnForgotPassword setTitle:LocalizedStr(@"Intro.FindPasswd.title") forState:UIControlStateNormal];
    [self.btnForgotPassword addTarget:self action:@selector(fotgorPasswd:) forControlEvents:UIControlEventTouchUpInside];
    NSString *str= LocalizedStr(@"Intro.FindPasswd.title");
    CGSize maximumSize = CGSizeMake(300, 9999);
    UIFont *myFont = [UIFont fontWithName:@"ArialRoundedMTBold" size:16.0f];
    CGRect textRect = [str boundingRectWithSize:maximumSize
                                        options:NSStringDrawingUsesLineFragmentOrigin| NSStringDrawingUsesFontLeading
                                     attributes:@{NSFontAttributeName:myFont}
                                        context:nil];
    
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(self.vForgotPassword.frame.origin.x, self.vForgotPassword.frame.origin.y, textRect.size.width, self.vForgotPassword.frame.size.height)];
    bgView.backgroundColor = UIColorLabelTextColor;
    [self.vPasswdButton addSubview:bgView];
    self.vForgotPassword.hidden = YES;
}

- (void)showPasswd:(CGRect)vrect dim:(NSInteger)dim {
    
    CGRect frame = self.vPasswd.frame;
    frame.origin.y = vrect.origin.y + vrect.size.height + dim;
    self.vPasswd.frame= frame;
    self.vPasswd.hidden = NO;
    
    UILabel *lb = (UILabel*)[self.vPasswd viewWithTag:1];
    if (self.setupMode != 4) {
        lb.text = LocalizedStr(@"Intro.Login.Passwd.placeholder");
    }
    else {
        lb.text = LocalizedStr(@"Intro.Login.NewPasswd.placeholder");
    }

    self.txtPassword.textColor = UIColorLabelTextColor;
    self.txtPassword.secureTextEntry = true;
    [self.txtPassword setBorderStyle:UITextBorderStyleNone];
    self.txtPassword.delegate = self;
    
    [[self.vPasswd viewWithTag:10] setBackgroundColor:UIColorUnderLineColor];
}

- (void)showPasswdNew:(CGRect)vrect dim:(NSInteger)dim {
    
    CGRect frame = self.vPasswdNew.frame;
    frame.origin.y = vrect.origin.y + vrect.size.height + dim;
    self.vPasswdNew.frame= frame;
    self.vPasswdNew.hidden = NO;
    
    UILabel *lb = (UILabel*)[self.vPasswdNew viewWithTag:1];
    lb.text = LocalizedStr(@"Intro.Login.ConfirmPasswd.placeholder");
    
    self.txtPasswordNew.textColor = UIColorLabelTextColor;
    self.txtPasswordNew.secureTextEntry = true;
    [self.txtPasswordNew setBorderStyle:UITextBorderStyleNone];
    self.txtPasswordNew.delegate = self;
    
    [[self.vPasswdNew viewWithTag:10] setBackgroundColor:UIColorUnderLineColor];
}

-(void)showUsername:(CGRect)vrect dim:(NSInteger)dim {
    
    CGRect frame = self.vUsername.frame;
    frame.origin.y = vrect.origin.y + vrect.size.height + dim;
    self.vUsername.frame= frame;
    self.vUsername.hidden = NO;
    
    if (self.setupMode == 0 || self.setupMode == 2) {
        // name
        UILabel *lb = (UILabel*)[self.vUsername viewWithTag:1];
//#ifdef _DRIVER_MODE
//        lb.text = LocalizedStr(@"Intro.Login.FirstName.placeholder");
//#else
//        lb.text = LocalizedStr(@"Intro.Login.Name.placeholder");
//#endif
        
        lb.text = LocalizedStr(@"Intro.Login.Name.placeholder");

        self.txtUsername.autocapitalizationType = UITextAutocapitalizationTypeNone;
        self.txtUsername.textColor = UIColorLabelTextColor;
        [self.txtUsername setBorderStyle:UITextBorderStyleNone];
    }
    
    [[self.vUsername viewWithTag:10] setBackgroundColor:UIColorUnderLineColor];
}

-(void)showUsernameL:(CGRect)vrect dim:(NSInteger)dim {
    
    CGRect frame = self.vUsernameL.frame;
    frame.origin.y = vrect.origin.y + vrect.size.height + dim;
    self.vUsernameL.frame= frame;
    self.vUsernameL.hidden = NO;
    
    if (self.setupMode == 0 || self.setupMode == 2) {
        // last name
        UILabel *lb = (UILabel*)[self.vUsernameL viewWithTag:1];
        lb.text = LocalizedStr(@"Intro.Login.LastName.placeholder");
        self.txtUsernameL.autocapitalizationType = UITextAutocapitalizationTypeNone;
        self.txtUsernameL.textColor = UIColorLabelTextColor;
        [self.txtUsernameL setBorderStyle:UITextBorderStyleNone];
    }
    
    [[self.vUsernameL viewWithTag:10] setBackgroundColor:UIColorUnderLineColor];
}

-(void)showEmail:(CGRect)vrect dim:(NSInteger)dim {
    
    CGRect frame = self.vEmail.frame;
    frame.origin.y = vrect.origin.y + vrect.size.height + dim;
    self.vEmail.frame= frame;
    self.vEmail.hidden = NO;
    
    UILabel *lb = (UILabel*)[self.vEmail viewWithTag:1];
    lb.text = LocalizedStr(@"Intro.Login.Email.placeholder");
    self.txtEmail.autocapitalizationType = UITextAutocapitalizationTypeNone;
    self.txtEmail.keyboardType = UIKeyboardTypeEmailAddress;
    [self.txtEmail setBorderStyle:UITextBorderStyleNone];
    [self.txtEmail setClearButtonMode:UITextFieldViewModeNever];
    self.txtEmail.textColor = UIColorLabelTextColor;
    //self.txtEmail.delegate = self;
    
    if (self.setupMode == 3) {
        NSString *userUid   = [[[TXApp instance] getSettings] getUserId];
        self.txtEmail.text = userUid;
        self.txtEmail.enabled = NO;
    }
    
    [[self.vEmail viewWithTag:10] setBackgroundColor:UIColorUnderLineColor];
}

-(void)showPhone:(CGRect)vrect dim:(NSInteger)dim {
    
    CGRect frame = self.vPhone.frame;
    frame.origin.y = vrect.origin.y + vrect.size.height + dim;
    self.vPhone.frame= frame;
    self.vPhone.hidden = NO;
    
    // country code
    self.txtCountry.textColor = UIColorLabelTextColor;
    //self.txtCountry.enabled = NO;
    
    // country image
    self.imgCountry.image = nil;
    self.imgCountry.contentMode = UIViewContentModeScaleAspectFill;
    
    //[self.btnCountry addTarget:self action:@selector(onCountrySearch:) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel *lb = (UILabel*)[self.vPhone viewWithTag:1];
    lb.text = LocalizedStr(@"Intro.Login.Telno.placeholder");
    self.txtPhoneNum.textColor = UIColorLabelTextColor;
    self.txtPhoneNum.keyboardType = UIKeyboardTypeNumberPad;
    [self.txtPhoneNum setBorderStyle:UITextBorderStyleNone];
    
    //self.txtPhoneNum.delegate = self;
    
    [[self.vPhone viewWithTag:10] setBackgroundColor:UIColorUnderLineColor];
}

-(void)showTerms0:(CGRect)vrect dim:(NSInteger)dim {
    
    CGRect frame = self.vTerms0.frame;
    frame.origin.y = vrect.origin.y + vrect.size.height + dim;
    self.vTerms0.frame= frame;
    self.vTerms0.hidden = NO;
    
    // country code
    UIButton *txt = [self.vTerms0 viewWithTag:115];
    // button
    //[[txt titleLabel] setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:14.0f]];
    [txt setTitle:LocalizedStr(@"Intro.Terms.All.title") forState:UIControlStateNormal];
    [txt setTitleColor:UIColorLabelTextColor forState:UIControlStateNormal];
    [txt setBackgroundColor:[UIColor clearColor]];
    
    [[self.vTerms0 viewWithTag:10] setBackgroundColor:UIColorUnderLineColor];
}

-(void)showTerms1:(CGRect)vrect dim:(NSInteger)dim {
    
    CGRect frame = self.vTerms1.frame;
    frame.origin.y = vrect.origin.y + vrect.size.height + dim;
    self.vTerms1.frame= frame;
    self.vTerms1.hidden = NO;
    
    // country code
    UIButton *txt = [self.vTerms1 viewWithTag:111];
    // button
    //[[txt titleLabel] setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:14.0f]];
    [txt setTitle:LocalizedStr(@"Intro.Terms.text1") forState:UIControlStateNormal];
    [txt setTitleColor:UIColorLabelTextColor forState:UIControlStateNormal];
    [txt setBackgroundColor:[UIColor clearColor]];
    
    [[self.vTerms1 viewWithTag:10] setBackgroundColor:UIColorUnderLineColor];
}

-(void)showTerms2:(CGRect)vrect dim:(NSInteger)dim {
    
    CGRect frame = self.vTerms2.frame;
    frame.origin.y = vrect.origin.y + vrect.size.height + dim;
    self.vTerms2.frame= frame;
    self.vTerms2.hidden = NO;
    
    // country code
    UIButton *txt = [self.vTerms2 viewWithTag:112];
    // button
    //[[txt titleLabel] setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:14.0f]];
    [txt setTitle:LocalizedStr(@"Intro.Terms.text2") forState:UIControlStateNormal];
    [txt setTitleColor:UIColorLabelTextColor forState:UIControlStateNormal];
    [txt setBackgroundColor:[UIColor clearColor]];
    
    [[self.vTerms2 viewWithTag:10] setBackgroundColor:UIColorUnderLineColor];
}

-(void)showTerms3:(CGRect)vrect dim:(NSInteger)dim {
    
    CGRect frame = self.vTerms3.frame;
    frame.origin.y = vrect.origin.y + vrect.size.height + dim;
    self.vTerms3.frame= frame;
    self.vTerms3.hidden = NO;
    
    // country code
    UIButton *txt = [self.vTerms3 viewWithTag:113];
    // button
    //[[txt titleLabel] setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:14.0f]];
    [txt setTitle:LocalizedStr(@"Intro.Terms.text3") forState:UIControlStateNormal];
    [txt setTitleColor:UIColorLabelTextColor forState:UIControlStateNormal];
    [txt setBackgroundColor:[UIColor clearColor]];
    
    [[self.vTerms3 viewWithTag:10] setBackgroundColor:UIColorUnderLineColor];
}

-(void)showTerms4:(CGRect)vrect dim:(NSInteger)dim {
    
    CGRect frame = self.vTerms4.frame;
    frame.origin.y = vrect.origin.y + vrect.size.height + dim;
    self.vTerms4.frame= frame;
    self.vTerms4.hidden = NO;
    
    // country code
    UIButton *txt = [self.vTerms4 viewWithTag:114];
    // button
    //[[txt titleLabel] setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:14.0f]];
#ifdef _DRIVER_MODE
    [txt setTitle:LocalizedStr(@"Intro.Terms.Driver.text4") forState:UIControlStateNormal];
#else
    [txt setTitle:LocalizedStr(@"Intro.Terms.text4") forState:UIControlStateNormal];
#endif
    [txt setTitleColor:UIColorLabelTextColor forState:UIControlStateNormal];
    [txt setBackgroundColor:[UIColor clearColor]];
    
    UIButton *btn = [self.vTerms4 viewWithTag:140];
    btn.layer.cornerRadius           = 4.0f;
    
    [self resetTermsButton:YES];
    
    [[self.vTerms4 viewWithTag:10] setBackgroundColor:UIColorUnderLineColor];
}

-(void)showCode :(CGRect)vrect dim:(NSInteger)dim {
    
    CGRect frame = self.vCode.frame;
    frame.origin.y = vrect.origin.y + vrect.size.height + dim;
    self.vCode.frame= frame;
    self.vCode.hidden = NO;
    
    // button
    [[self.btnGetCode titleLabel] setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:14.0f]];
    self.btnGetCode.layer.cornerRadius           = 4.0f;
    
    [self resetCodeButton:NO];
    
    self.vVerifyCode.layer.borderColor            = UIColorLabelTextColor.CGColor;
    self.vVerifyCode.layer.borderWidth            = 2.0f;
    self.vVerifyCode.layer.cornerRadius           = 20.0f;
#ifdef _WITZM
#elif defined _CHACHA
    self.vVerifyCode.backgroundColor               = HEXCOLOR(0xf7f7f7FF);
    self.vVerifyCode.tintColor                     = HEXCOLOR(0xd42327FF);
#endif
    
    NSString *string = @"";
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:string];
    
    float spacing = 10.2f;
    [attributedString addAttribute:NSKernAttributeName
                             value:@(spacing)
                             range:NSMakeRange(0, [string length])];
    
    self.txtVerifyCode.attributedText = attributedString;
    self.txtVerifyCode.delegate = self;
    
    //self.txtVerifyCode.frame = [Utils frameUnder:self.imgUnderArrow dim:20];
//    self.txtVerifyCode.layer.borderColor            = [UIColor whiteColor].CGColor;
//    self.txtVerifyCode.layer.borderWidth            = 2.0f;
//    self.txtVerifyCode.layer.cornerRadius           = 15.0f;
    
#ifdef _WITZM
    self.txtVerifyCode.textColor                    = UIColorBasicText;
#elif defined _CHACHA
    self.txtVerifyCode.textColor                    = HEXCOLOR(0xd42327FF);
#endif

    [self.txtVerifyCode setClearButtonMode:UITextFieldViewModeNever];
    self.txtVerifyCode.textAlignment = NSTextAlignmentCenter;
    self.txtVerifyCode.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    
    //설정 줄 간격
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc]init];
    paragraphStyle.lineSpacing = 50;
    
    NSDictionary *attributes = @{ NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue-Bold" size:24.0f], NSParagraphStyleAttributeName:paragraphStyle};
    //NSDictionary *attributes = @{ NSParagraphStyleAttributeName:paragraphStyle};
    self.txtVerifyCode.attributedText = [[NSAttributedString alloc]initWithString:self.txtVerifyCode.text attributes:attributes];
    
    float __width = (self.view.frame.size.width/1.8);
    self.txtVerifyCode.frame = CGRectMake(self.vCode.frame.size.width/2 - __width/2,
                                          self.txtVerifyCode.frame.origin.y,
                                          __width,
                                          self.txtVerifyCode.frame.size.height);
    
    //UIView *view = [Utils addFooterLabelView:self.view underView:self.txtVerifyCode dim:0];
    
    string = LocalizedStr(@"Intro.Login.Footer.text");
    attributedString = [[NSMutableAttributedString alloc] initWithString:string];
    
    spacing = -0.1f;
    [attributedString addAttribute:NSKernAttributeName
                             value:@(spacing)
                             range:NSMakeRange(0, [string length])];
    
    self.lbFooter.attributedText = attributedString;
    
    // footer
    RTLabel *label = [[RTLabel alloc] initWithFrame:CGRectMake(0,
                                                               (self.vVerifyCode.frame.origin.y + self.vVerifyCode.frame.size.height) ,
                                                               self.lbFooter.frame.size.width,
                                                               self.lbFooter.frame.size.height)];
    [label setTextColor:UIColorLabelText];
    [label sizeToFit];
    label.backgroundColor = [UIColor clearColor];
    label.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    
    label.textColor = [UIColor blackColor];
    label.text = LocalizedStr(@"Intro.Login.Footer.text");
    label.alpha = 0.6f;
    [label setFont:[UIFont fontWithName:@"ArialRoundedMTBold" size:12.0f]];
    [label sizeToFit];
    CGSize optimumSize = [label optimumSize];
    CGRect rect = label.frame;
    rect.size.height = optimumSize.height;
    //frame.origin.y = self.lbFooter.frame.origin.y;
    label.frame = rect;
    self.lbFooter.hidden = YES;
    
    //[self.vCode addSubview:label];
}

-(void)showReset :(CGRect)vrect dim:(NSInteger)dim {
    
    CGRect frame = self.vReset.frame;
    frame.origin.y = vrect.origin.y + vrect.size.height + dim;
    self.vReset.frame= frame;
    self.vReset.hidden = NO;
    
    // button
    [[self.btnGetResetCode titleLabel] setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:14.0f]];
    self.btnGetResetCode.layer.cornerRadius           = 4.0f;
    
    [self.btnGetResetCode setTitle:LocalizedStr(@"Intro.ResetPW.BTN.text") forState:UIControlStateNormal];
    [self.btnGetResetCode setTitleColor:UIColorButtonText forState:UIControlStateNormal];
    [self.btnGetResetCode setBackgroundColor:UIColorDefault];
    
    [self.btnGetResetCode addTarget:self action:@selector(requestEmail:) forControlEvents:UIControlEventTouchUpInside];
    self.btnGetResetCode.tag = 1;
    
    NSString *string = @"";
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:string];
    
    float spacing = -0.1f;
    [attributedString addAttribute:NSKernAttributeName
                             value:@(spacing)
                             range:NSMakeRange(0, [string length])];
    
    self.lbResetFooter.attributedText = attributedString;
    
    // footer
    RTLabel *label = [[RTLabel alloc] initWithFrame:CGRectMake(0,
                                                               (self.vReset.frame.origin.y + self.vReset.frame.size.height) ,
                                                               self.lbResetFooter.frame.size.width,
                                                               self.lbResetFooter.frame.size.height)];

    [label sizeToFit];
    label.backgroundColor = [UIColor clearColor];
    label.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    label.tag = 100;
    label.textColor = UIColorLabelTextColor;
    label.text = string;
    label.alpha = 0.6f;
    label.textAlignment = RTTextAlignmentCenter;
    label.delegate = self;
    [label setFont:[UIFont fontWithName:@"ArialRoundedMTBold" size:12.0f]];
    [label sizeToFit];
//    CGSize optimumSize = [label optimumSize];
    CGRect rect = label.frame;
    //rect.size.height = optimumSize.height;
    //frame.origin.y = self.lbFooter.frame.origin.y;
    rect = self.lbResetFooter.frame;
    label.frame = rect;
    //self.lbResetFooter.hidden = YES;
    
    [self.vReset addSubview:label];
}

-(void)showNew :(CGRect)vrect dim:(NSInteger)dim {
    
    CGRect frame = self.vNew.frame;
    frame.origin.y = vrect.origin.y + vrect.size.height + dim;
    self.vNew.frame= frame;
    self.vNew.hidden = NO;
    
    // button
    [[self.btnGetNewCode titleLabel] setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:14.0f]];
    self.btnGetNewCode.layer.cornerRadius           = 4.0f;
    
    [self.btnGetNewCode setTitle:LocalizedStr(@"Intro.NewPW.BTN.text") forState:UIControlStateNormal];
    [self.btnGetNewCode setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.btnGetNewCode setBackgroundColor:UIColorDefault];
    
    [self.btnGetNewCode addTarget:self action:@selector(changePw:) forControlEvents:UIControlEventTouchUpInside];
    self.btnGetNewCode.tag = 1;
    
    //
    NSString *string = @"";
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:string];
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = 5.0f;
    [attributedString addAttribute:NSParagraphStyleAttributeName
                             value:paragraphStyle
                             range:NSMakeRange(0, [string length])];
    
    
    float spacing = -0.2f;
    [attributedString addAttribute:NSKernAttributeName
                             value:@(spacing)
                             range:NSMakeRange(0, [string length])];
    
    self.lbNewFooter1.attributedText = attributedString;
    self.lbNewFooter1.textColor = UIColorScarlet;
    //[self.lbNewFooter1 setFont:[UIFont fontWithName:@"ArialRoundedMTBold" size:16.0f]];

    //
    string = LocalizedStr(@"Intro.NewPW.Footer2.text");
    attributedString = [[NSMutableAttributedString alloc] initWithString:string];
    
    paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = 5.0f;
    [attributedString addAttribute:NSParagraphStyleAttributeName
                             value:paragraphStyle
                             range:NSMakeRange(0, [string length])];
    
    spacing = -0.3f;
    [attributedString addAttribute:NSKernAttributeName
                             value:@(spacing)
                             range:NSMakeRange(0, [string length])];
    
    self.lbNewFooter2.attributedText = attributedString;
    self.lbNewFooter2.textColor = UIColorLabelTextColor;
    //[self.lbNewFooter2 setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:16.0f]];
}

#pragma mark - waitTimer
// http://stockengineer.tistory.com/226
- (void)startTimer {
    // 타이머 셋팅 시작 - 개별 선택된 셀의 타이머가 따로 작동한다
    [self stopTimer];
    self.imgTimer.hidden = NO;
    self.lbTimer.hidden = NO;
    
    waitTimerTime = 0;
    [self printTimer];
    waitTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timerMethod:) userInfo:nil repeats:YES];
    
    // disable request button
    [self.btnGetCode setAlpha:0.3];
    [self.btnGetCode setEnabled:NO];
}

- (void)stopTimer {
    if (waitTimer != nil) {
        [waitTimer invalidate];
        waitTimer = nil;
    }
    [self printTimer];
    waitTimerTime = 0;
    
    self.imgTimer.hidden = YES;
    self.lbTimer.hidden = YES;
    self.txtVerifyCode.text = @"";
    [self resetCodeButton:NO];
}

-(void)timerMethod:(NSTimer *)timer
{
    waitTimerTime++;
    
    // 타이머 종료 시, 알람메시지를 띄운다
    if( waitTimerTime == kVerificationCodeWaitTime)
    {
        // 타이머를 종료한다
        [timer invalidate];
        waitTimer = nil;
        
        if (self.setupMode == 0) {
            [self resetCodeButton:NO];
        }
        else if (self.setupMode == 1) {
            [self resetCodeButton:NO];
        }
//        else if (self.setupMode == 2) {
//            [self resetCodeButton:NO];
//        }
        else if (self.setupMode == 3) {
            [self resetCodeButton:NO];
        }
//        else if (self.setupMode == 4) {
//            [self resetCodeButton:NO];
//        }
        
        [self stopTimer];
    }
    [self printTimer];
}

-(void)printTimer {
    // 화면 재로드
    NSInteger t = kVerificationCodeWaitTime - waitTimerTime;
    _lbTimer.text = [NSString stringWithFormat:@"%02d:%02d",(int)(t/60), (int)(t%60)];
}


#pragma mark - UITextFired Delegate
- (void)textDidChange:(UITextField *)textField
{
    //NSLog(@"textField.placeholder %@ textDidChange %@", textField.placeholder, textField.text);
}

-(void)textFieldValueChanged:(UITextField *)sender {
    //[self refreshSignUpButton];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    //Has Focus
    
    if ([textField isEqual:self.txtPassword]) {
        [self validatePasswd:NO];
    }
    if ([textField isEqual:self.txtPasswordNew]) {
        [self validatePasswd:NO];
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    //Has Focus out
    
    if ([textField isEqual:self.txtPassword]) {
        [self validatePasswd:NO];
    }
    if ([textField isEqual:self.txtPasswordNew]) {
        [self validatePasswd:NO];
    }
    
}

- (BOOL)textField:(UITextField *) textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if ([textField isEqual:self.txtVerifyCode]) {
        NSUInteger oldLength = [textField.text length];
        NSUInteger replacementLength = [string length];
        NSUInteger rangeLength = range.length;
        
        NSUInteger newLength = oldLength - rangeLength + replacementLength;
        
        BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
    
        if (newLength >= 6) {
            [self resetCodeButton:YES];
        }
        else {
            if (!isChanged) {
                NSString *txt = [NSString stringWithFormat:@"%@",string];
                NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:txt];
                
                float spacing = 10.2f;
                [attributedString addAttribute:NSKernAttributeName
                                         value:@(spacing)
                                         range:NSMakeRange(0, [txt length])];
                
                self.txtVerifyCode.attributedText = attributedString;
                self.txtVerifyCode.text = @"";
                [self resetCodeButton:NO];
                
                isChanged = YES;
            }
            
            //[self refreshSignUpButton];
            [self resetCodeButton:NO];
        }
        return newLength <= 6 || returnKey;
    }
    return YES;
}

#pragma mark ViewLoad

-(void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"load view");
    //NSLog(@"viewDidLoad:%d,%ld",(int)self.navigationController.viewControllers.count,self.setupMode);
    [[IQKeyboardManager sharedManager] setEnable:YES];
    
    [self resetCodeButton:NO];
    
    [self initControls];
    
    NSLog(@"self.view.frame:%@",NSStringFromCGRect(self.view.frame));
    NSLog(@"self.view.frame:%@",NSStringFromCGRect(self.vbody.frame));
    //if (self.setupMode == 0) {
    if (self.view.frame.size.height<self.vbody.frame.size.height) {
        //UIView *n = [super navigationView];
        NSInteger _y = [super navigationTopHeight];
        _y = 0;
        self.myScrollView.frame = CGRectMake(0,
                                             _y,
                                             self.view.frame.size.width,
                                             self.view.frame.size.height - _y);
        self.myScrollView.contentSize = CGSizeMake(self.vbody.frame.size.width, self.vbody.frame.size.height);
        
        UIEdgeInsets contentInsets = UIEdgeInsetsMake(0, 0.0, 0, 0.0);
        self.myScrollView.contentInset = contentInsets;
        self.myScrollView.scrollIndicatorInsets = contentInsets;
        self.myScrollView.scrollsToTop = YES;
        [self.myScrollView addSubview:self.vbody];
    }
    else {
        CGRect rect = self.vbody.frame;
        //rect.origin.y += 20;
        self.vbody.frame = rect;
        [self.view addSubview:self.vbody];
    }
    
    [self hideBusyIndicator];
    /*
    //if (self.setupMode == 0) {
    if (1) {
        UIView *n = [super navigationView];
        NSInteger _y = n.frame.origin.y + n.frame.size.height;
        _y = kTopHeight;
        self.myScrollView.frame = CGRectMake(0,
                                             _y,
                                             self.view.frame.size.width,
                                             self.view.frame.size.height - _y);
        self.myScrollView.contentSize = CGSizeMake(self.vbody.frame.size.width, self.vbody.frame.size.height);
        
        UIEdgeInsets contentInsets = UIEdgeInsetsMake(0, 0.0, 0, 0.0);
        self.myScrollView.contentInset = contentInsets;
        self.myScrollView.scrollIndicatorInsets = contentInsets;
        self.myScrollView.scrollsToTop = YES;
        [self.myScrollView addSubview:self.vbody];
    }
    else {
        CGRect rect = self.vbody.frame;
        rect.origin.y += 20;
        self.vbody.frame = rect;
        [self.view addSubview:self.vbody];
    }
    */
    
    [appDelegate startSingleLocationRequest];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self configureFieldStyles];
    
    [self addEvent];
    
    appDelegate.signupViewStep = self.setupMode;
    
    //NSLog(@"viewDidAppear self.setupMode:%ld",self.setupMode);
    [self registerEventListeners];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    //NSLog(@"viewWillAppear self.setupMode:%ld",self.setupMode);
    isSignup = NO;
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
    
    if ([self.txtVerifyCode.text length] == 6) {
        [self resetCodeButton:YES];
    }
    
    [[IQKeyboardManager sharedManager] setEnable:YES];
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    //NSLog(@"viewWillDisappear self.setupMode:%ld",self.setupMode);
    isSignup = YES;
    [self removeEventListeners];
    
    [self resignKeyboard];
}

-(void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    //NSLog(@"viewDidDisappear self.setupMode:%ld",self.setupMode);

    [self removeEventListeners];
    
    [self removeEvent];
}

-(void)addEvent {
    appDelegate.signupViewStep = self.setupMode;
    
    observer = [[NSNotificationCenter defaultCenter] addObserverForName:@"NotificationPushReceived"
                                                                 object:nil
                                                                  queue:[NSOperationQueue mainQueue]
                                                             usingBlock:^(NSNotification *notification) {
                                                                 
                                                                 
                                                                 NSDictionary* dict = notification.userInfo;
                                                                 NSLog(@"NotificationPushReceived:%@",dict);
                                                                 if ([Utils isDictionary:[dict valueForKey:@"notification"]]) {
                                                                     self.txtVerifyCode.text = [dict valueForKey:@"notification"][@"body"];
                                                                     [self resetCodeButton:YES];
                                                                 }
                                                                 else if ([Utils isDictionary:[dict valueForKey:@"aps.data"]]) {
                                                                     self.txtVerifyCode.text = [dict valueForKey:@"aps"][@"alert"][@"body"];
                                                                     [self resetCodeButton:YES];
                                                                 }
                                                                 else if ([Utils isDictionary:[dict valueForKey:@"aps"]]) {
                                                                     self.txtVerifyCode.text = [dict valueForKey:@"aps"][@"alert"][@"body"];
                                                                     [self resetCodeButton:YES];
                                                                 }
                                                                 [self resignFirstResponder];
                                                                 [self nextButtonOn:YES];
                                                             }];
    
    //if (self.setupMode == 1)
    {
        observer2 = [[NSNotificationCenter defaultCenter] addObserverForName:@"NotificationCustomURLReceived"
                                                                      object:nil
                                                                       queue:[NSOperationQueue mainQueue]
                                                                  usingBlock:^(NSNotification *notification) {
                                                                      
                                                                      if ([appDelegate isLoginState]) {
                                                                          return;
                                                                      }
                                                                      // change to new pw view
                                                                      NSDictionary* dict = notification.userInfo;
                                                                      NSLog(@"NSNotificationCenter NotificationPushReceived:%@\n%ld,%ld",dict,(long)appDelegate.signupViewStep,(long)self.setupMode);
                                                                      if ([dict valueForKey:@"datoken"]) {
                                                                          if (appDelegate.signupViewStep == 4) {
                                                                              datoken = [appDelegate.customShareDic objectForKey:@"datoken"];
                                                                              appDelegate.customShareDic = nil;
                                                                          }
                                                                          else if (self.setupMode != 4) {
                                                                              [self onNewPw:nil];
                                                                          }
                                                                          else {
                                                                              datoken = [appDelegate.customShareDic objectForKey:@"datoken"];
                                                                              appDelegate.customShareDic = nil;
                                                                          }
                                                                      }
                                                                  }];
    }
    
    //---------------------------------------------------------------------------------------------------------------------------------------------
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onTerms:) name:NOTIFICATION_SIGNUP object:nil];
    //---------------------------------------------------------------------------------------------------------------------------------------------
    //---------------------------------------------------------------------------------------------------------------------------------------------
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveSearchNotification:) name:NOTIFICATION_SEARCH_FINISH object:nil];
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveCustomUrlNotification:) name:NOTIFICATION_CUSTOM_URL object:nil];
    //---------------------------------------------------------------------------------------------------------------------------------------------
    
    NSLog(@"addEvent :%ld,%ld",(long)appDelegate.signupViewStep,(long)self.setupMode);
}

-(void)removeEvent {
    [[NSNotificationCenter defaultCenter] removeObserver:observer];
    observer = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:observer2];
    observer2 = nil;
    isSignup = YES;
    //    [[NSNotificationCenter defaultCenter] removeObserver:self
    //                                                    name:@"NotificationPushReceived"
    //                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:NOTIFICATION_SEARCH_FINISH
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:NOTIFICATION_SIGNUP
                                                  object:nil];
    
    NSLog(@"removeEvent :%ld,%ld",(long)appDelegate.signupViewStep,(long)self.setupMode);
}
-(void)dealloc {
    [self removeEventListeners];
    
    NSLog(@"load view dealloc");
    
    [self removeEvent];
}

-(BOOL)prefersStatusBarHidden{
    return NO;
}

-(void) configureFieldStyles {
    
    
    return;
    //UIColor *bgColor = [UIColor whiteColor];
    /*
    [super configureStyles];
    [self.txtUsername setTextAlignment:NSTextAlignmentLeft];
    [self.txtUsername setClearButtonMode:UITextFieldViewModeWhileEditing];
    self.txtUsername.layer.shadowOpacity = 0.0;
    [self.txtUsername.layer addSublayer:[TXUILayers layerWithRadiusTop:self.txtUsername.bounds color:[bgColor CGColor]]];
    [self.txtPassword setTextAlignment:NSTextAlignmentLeft];
    [self.txtPassword setClearButtonMode:UITextFieldViewModeWhileEditing];
    self.txtPassword.layer.shadowOpacity = 0.0;
    [self.txtPassword.layer addSublayer:[TXUILayers layerWithRadiusBottom:self.txtUsername.bounds color:[bgColor CGColor]]];
    */
}

-(void) registerEventListeners {
    for (NSArray *msg in _msgList) {
        [self->model addEventListener:self forEvent:[msg objectAtIndex:0] eventParams:nil];
    }
}

-(void) removeEventListeners {
    for (NSArray *msg in _msgList) {
        [self->model removeEventListener:self forEvent:[msg objectAtIndex:0]];
    }
}

-(void)didBeginEditing:(id)sender {
    [self configureFieldStyles];
}

#pragma mark Event
-(void)onEvent:(TXEvent *)event eventParams:(id)subscriptionParams {
    static BOOL isVC = NO;
    [self hideBusyIndicator];
    TXResponseDescriptor *descriptor = [event getEventProperty:TXEvents.Params.DESCRIPTOR];
    TXRequestObj *request = [event getEventProperty:TXEvents.Params.REQUEST];
    
    NSString     *response = [[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding];
    NSDictionary *result   = getJSONObj(response);
    
    //NSLog(@"%@",result);
    if([event.name isEqualToString:@"UR110"]) {
        
        if(descriptor.rlt != success) {
            return;
        }
        
        @try {
            // redis 접속
            NSString     *response = [[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding];
            NSDictionary *responseObj   = getJSONObj(response);
            
            appDelegate.api_config = [responseObj objectForKey:@"user_config"];
            //[RedisSingleton redisLogin:appDelegate.api_config];
            
        }
        @catch (NSException *exception) {
            NSLog(@"Exception: %@, %@", exception.name, exception.reason);
        }
        
//        NSDictionary *user_device       = [result valueForKey:@"user_device"];
        NSArray *user_payment           = [result valueForKey:@"user_payments"];
//        NSDictionary *provider_device   = [result valueForKey:@"provider_device"];
        
//        NSLog(@">>> %@",user);
//        NSLog(@">>> %@",[user valueForKey:SettingsConst.CryptoKeys.USEQ]);
        
        NSDictionary *requestObj = getJSONObj(request.body);
        
        [[[TXApp instance] getSettings] setUserId:[requestObj valueForKey:API_JSON.UID]];
        [[[TXApp instance] getSettings] setUserTelno:[requestObj valueForKey:API_JSON.UTELNO]];
        //[[[TXApp instance] getSettings] setPassword:[requestObj valueForKey:API_JSON.PASSWORD]];
        [[[TXApp instance] getSettings] setPassword:[Utils passcodeBysha256:self.txtPassword.text]];
        
        // 등록된 카드정보가 없으면 카드등록화면으로 전환
        if (![user_payment count]) {
            
        }
        // 사용가능 카드를 찾는다.
        for (NSDictionary* key in user_payment) {
            NSString* cseq   = [key valueForKey:@"cseq"];
            NSString* mode   = [NSString stringWithFormat:@"%@", [key valueForKey:@"mode"]];
            NSString* state  = [key valueForKey:@"state"];
            
            if ([mode isEqualToString:@"1"] && [state isEqualToString:@"1"] ) {
                [[[TXApp instance] getSettings] setFDKeychain:SettingsConst.CryptoKeys.CSEQ value:cseq];
                break;
            }
        }
        
//        int ustate = 0;
//        if ([user_device count]) {
//            ustate = (int)[user_device objectForKey:@"state"];
//        }
        
        [self performSelector:@selector(goMapView:) withObject:nil afterDelay:0.1];
        //[self goMapView:nil];
    }
    else if([event.name isEqualToString:@"UR111"] || [event.name isEqualToString:@"PP111"]) {
        if(descriptor.success == true) {
            // 인증코드 요청성공
            //[self resetCodeButton:YES];
            [self resetCodeButton:NO];
            
            TXSignInAlertVC *signInVC = [[TXSignInAlertVC alloc] initWithNibName:@"TXSignInAlertVC" bundle:nil];
            signInVC.view.frame = [super view].frame;
            [self presentViewController:signInVC animated:YES completion:nil];
            //[self pushViewController:signInVC];
            
        } else {
            // 인증코드 요청실패
            //에러주석id   error  = [result objectForKey:API_JSON.ResponseDescriptor.ERROR];
            /*
            if(descriptor.rlt == -51) {
                [self alertError:LocalizedStr(@"String.Alert") message:error];
            }
            else {
                [self alertError:LocalizedStr(@"String.Alert") message:error];
            }
             */
        }
    }
    else if([event.name isEqualToString:@"UR120"]) { // 사용자 생성
        if(descriptor.rlt == success) {
            // 성공
            NSDictionary *requestObj = getJSONObj(request.body);
            
            [[[TXApp instance] getSettings] setUserId:[requestObj valueForKey:API_JSON.UID]];
            [[[TXApp instance] getSettings] setUserTelno:[requestObj valueForKey:API_JSON.UTELNO]];
            [[[TXApp instance] getSettings] setPassword:[Utils passcodeBysha256:self.txtPassword.text]];
            
            NSString *seq = [Utils getBCDSTEP];
            if (seq != nil) {
                // keychain 초기화
                [[[TXApp instance] getSettings] removeFDKeychain:seq];
            }
            
            [self performSelector:@selector(goMapView:) withObject:nil afterDelay:0.1];
            //[self goMapView:nil];
        } else {
            //에러주석[self alertError:LocalizedStr(@"String.Alert") message:descriptor.error];
        }
    }
    else if([event.name isEqualToString:@"UR311"]) {
        
        if(descriptor.success == true) {
            [self goMapView:nil];
        } else {
            //에러주석[self alertError:LocalizedStr(@"String.Alert") message:descriptor.error];
        }
    }
    else if([event.name isEqualToString:@"PP110"]) {
        
        if(descriptor.rlt != success) {
            return;
        }
        
        @try {
            // redis 접속
            NSString     *response = [[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding];
            NSDictionary *responseObj   = getJSONObj(response);
            
            appDelegate.api_config = [responseObj objectForKey:@"provider_config"];
            //[RedisSingleton redisLogin:appDelegate.api_config];
        }
        @catch (NSException *exception) {
            NSLog(@"Exception: %@, %@", exception.name, exception.reason);
        }
        
        NSDictionary *requestObj = getJSONObj(request.body);
        
        [[[TXApp instance] getSettings] setUserId:[requestObj valueForKey:API_JSON.UID]];
        [[[TXApp instance] getSettings] setUserTelno:[requestObj valueForKey:API_JSON.UTELNO]];
        [[[TXApp instance] getSettings] setPassword:[Utils passcodeBysha256:self.txtPassword.text]];
        
        [self performSelector:@selector(goMapView:) withObject:nil afterDelay:0.1];
        //[self goMapView:nil];
    }
    else if([event.name isEqualToString:@"PP120"]) {
        
        if(descriptor.rlt != success) {
            return;
        }
        
        @try {
            // redis 접속
            NSString     *response = [[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding];
            NSDictionary *responseObj   = getJSONObj(response);
            
            appDelegate.api_config = [responseObj objectForKey:@"provider_config"];
            //[RedisSingleton redisLogin:appDelegate.api_config];
        }
        @catch (NSException *exception) {
            NSLog(@"Exception: %@, %@", exception.name, exception.reason);
        }
        
        NSDictionary *requestObj = getJSONObj(request.body);
        
        [[[TXApp instance] getSettings] setUserId:[requestObj valueForKey:API_JSON.UID]];
        [[[TXApp instance] getSettings] setUserTelno:[requestObj valueForKey:API_JSON.UTELNO]];
        [[[TXApp instance] getSettings] setPassword:[Utils passcodeBysha256:self.txtPassword.text]];
        
        NSString *seq = [Utils getBCDSTEP];
        if (seq != nil) {
            // keychain 초기화
            [[[TXApp instance] getSettings] removeFDKeychain:seq];
        }
        
        [self performSelector:@selector(goMapView:) withObject:nil afterDelay:0.1];
        //[self goMapView:nil];
    }
    else if([event.name isEqualToString:@"UR210"] || [event.name isEqualToString:@"PP210"]) {
        
        if(descriptor.success == true) {
        
            NSDictionary *requestObj = getJSONObj(request.body);
            
            if (self.setupMode == 3) {
                [[[TXApp instance] getSettings] setUserTelno:[requestObj valueForKey:API_JSON.UTELNO]];
            }
            
            if (self.setupMode == 0 || self.setupMode == 1 || self.setupMode == 4) {
                [[[TXApp instance] getSettings] setPassword:[Utils passcodeBysha256:self.txtPassword.text]];
            }
            
            //[self removeEventListeners];
            [self.navigationController popViewControllerAnimated:YES];
//            [self alertError:@"" message:LocalizedStr(@"Menu.Account.Utelno.result.text")];
            
        } else {
            //에러주석[self alertError:@"Error" message:descriptor.error];
        }
    }
    else if([event.name isEqualToString:@"UR113"] || [event.name isEqualToString:@"PP113"]) {
        
        if(descriptor.success == true) {
            // 메일을 보냈습니다.
            RTLabel *lb = (RTLabel*)[self.vReset viewWithTag:100];
            lb.text = LocalizedStr(@"Intro.ResetPW.Footer.text");
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1.0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [self.navigationController popViewControllerAnimated:YES];
            });
            //self.lbResetFooter.text = LocalizedStr(@"Intro.ResetPW.Footer.text");
        } else {
            RTLabel *lb = (RTLabel*)[self.vReset viewWithTag:100];
            lb.text = LocalizedStr(@"Intro.ResetPW.Footer.text2");
            //에러주석[self alertError:@"Error" message:descriptor.error];
        }
    }
    else if([event.name isEqualToString:@"UR211"] || [event.name isEqualToString:@"PP211"]) {
        //NSLog(@"self.setupMode:%ld",self.setupMode);
        if(descriptor.success == true) {

            isSignup = YES;
            if (self.setupMode == 5) {
                isSignup = NO;
            }

            if (!isSignup) {
                isSignup = YES;
                [self removeEventListeners];
                NSLog(@"onSignUp call");

                [self onSnsSignUp:nil];
                //[self onSignUp:nil];

            }
        } else {
            //에러주석[self alertError:LocalizedStr(@"String.Alert") message:descriptor.error];
        }
    }
}


#pragma mark Work
//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void) receiveSearchNotification:(NSNotification *) notification
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    if (notification == nil)
    {
        NSString *countryCode = @"KR";//[Utils getCountryCode];
        
        NSArray *countryList = [[NSArray alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"countries" ofType:@"plist"]];

        for (NSDictionary *dic in countryList) {
            if ([dic[@"code"] isEqualToString:countryCode]) {
                self.countryISOCode = countryCode;
                self.txtCountry.text  = [NSString stringWithFormat:@"(%@)",dic[@"dial_code"]];
                self.imgCountry.image = [Utils getCountryFlag:dic[@"code"]];
            }
        }
    }
    else {
        NSDictionary *userInfo = notification.userInfo;
        //
        NSLog(@"User selected %@", userInfo);
        self.countryISOCode = userInfo[@"code"];
        
        self.txtCountry.text  = [NSString stringWithFormat:@"(%@)",userInfo[@"dial_code"]];
        self.imgCountry.image = [Utils getCountryFlag:userInfo[@"code"]];
    }
}

-(void) refreshSignUpButton {
    if (self.setupMode == 0) {
        if(self.txtPhoneNum.text.length > 8 &&
           self.txtEmail.text.length > 10 &&
           self.txtUsername.text.length > 1 &&
           self.txtPassword.text.length > 5 ) {
            
            [self.btnGetCode setAlpha:1];
            [self.btnGetCode setEnabled:YES];
            
        } else {
            
            [self.btnGetCode setAlpha:0.3];
            [self.btnGetCode setEnabled:NO];
            
        }
    }
    else if (self.setupMode == 1) {
        if(self.txtPhoneNum.text.length > 8 &&
           self.txtEmail.text.length > 10 &&
           self.txtPassword.text.length > 5 ) {
            
            [self.btnGetCode setAlpha:1];
            [self.btnGetCode setEnabled:YES];
            
        } else {
            
            [self.btnGetCode setAlpha:0.3];
            [self.btnGetCode setEnabled:NO];
            
        }
    }
    else if (self.setupMode == 3) {
        if(self.txtPhoneNum.text.length > 8 &&
           self.txtEmail.text.length > 10) {
            
            [self.btnGetCode setAlpha:1];
            [self.btnGetCode setEnabled:YES];
            
        } else {
            
            [self.btnGetCode setAlpha:0.3];
            [self.btnGetCode setEnabled:NO];
            
        }
    }
}

- (void)resetCodeButton:(BOOL)flag {
    
    if (flag) {
        [self.btnGetCode setTitle:LocalizedStr(@"Intro.Next.title") forState:UIControlStateNormal];
        
#ifdef _WITZM
        [self.btnGetCode setTitleColor:UIColorButtonText forState:UIControlStateNormal];
        [self.btnGetCode setBackgroundColor:UIColorDefault];
#elif defined _CHACHA
        [self.btnGetCode setTitleColor:UIColorButtonText forState:UIControlStateNormal];
        [self.btnGetCode setBackgroundColor:UIColorDefault];
#endif

        /*
        [self.btnGetCode removeTarget:nil
                               action:NULL
                     forControlEvents:UIControlEventAllEvents];
        
        if (self.setupMode == 1) {
            [self.btnGetCode addTarget:self action:@selector(signIn:) forControlEvents:UIControlEventTouchUpInside];
        }
        else if (self.setupMode == 0) {
            [self.btnGetCode addTarget:self action:@selector(signUp:) forControlEvents:UIControlEventTouchUpInside];
        }
//        else if (self.setupMode == 2) {
//            [self.btnGetCode addTarget:self action:@selector(signUpByForgot:) forControlEvents:UIControlEventTouchUpInside];
//        }
        else if (self.setupMode == 3) {
            [self.btnGetCode addTarget:self action:@selector(changeTelno:) forControlEvents:UIControlEventTouchUpInside];
        }
         */
        self.btnGetCode.tag = 1;
    }
    else {
        [self.btnGetCode setTitle:LocalizedStr(@"Intro.Login.GetVerification.text") forState:UIControlStateNormal];
        
#ifdef _WITZM
        [self.btnGetCode setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [self.btnGetCode setBackgroundColor:[UIColor whiteColor]];
#elif defined _CHACHA
        [self.btnGetCode setTitleColor:UIColorButtonText forState:UIControlStateNormal];
        [self.btnGetCode setBackgroundColor:UIColorDefault];
#endif

        /*
        [self.btnGetCode removeTarget:nil
                               action:NULL
                     forControlEvents:UIControlEventAllEvents];
        [self.btnGetCode addTarget:self action:@selector(getVerificationCode:) forControlEvents:UIControlEventTouchUpInside];
        */
        self.btnGetCode.tag = 0;
    }
    
    NSString *txt = [NSString stringWithFormat:@"%@",self.txtVerifyCode.text];
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:txt];
    
    float spacing = 10.2f;
    [attributedString addAttribute:NSKernAttributeName
                             value:@(spacing)
                             range:NSMakeRange(0, [txt length])];
    
    self.txtVerifyCode.attributedText = attributedString;
    // disable request button
//    if (self.imgTimer.hidden) {
//        [self.btnGetCode setAlpha:0.3];
//        [self.btnGetCode setEnabled:NO];
//    }
//    else
    {
        [self.btnGetCode setAlpha:1];
        [self.btnGetCode setEnabled:YES];
    }
}

- (void)resetTermsButton:(BOOL)flag {
    
    UIButton *btn = [self.vTerms4 viewWithTag:140];
    
    if (flag) {
        [btn setTitle:LocalizedStr(@"Intro.Terms.BTN.text") forState:UIControlStateNormal];
        [btn setTitleColor:UIColorButtonText forState:UIControlStateNormal];
        [btn setBackgroundColor:UIColorDefault];
        [btn removeTarget:nil
                               action:NULL
                     forControlEvents:UIControlEventAllEvents];
        
        if (self.setupMode == 5) {
            [btn addTarget:self action:@selector(termsAllow:) forControlEvents:UIControlEventTouchUpInside];
        }
        [btn setEnabled:YES];
    }
    else {
        [btn setTitle:LocalizedStr(@"Intro.Terms.BTN.text") forState:UIControlStateNormal];
        [btn setTitleColor:UIColorButtonText forState:UIControlStateNormal];
        [btn setBackgroundColor:UIColorDefault];
        [btn removeTarget:nil
                        action:NULL
              forControlEvents:UIControlEventAllEvents];
        
        [btn setEnabled:NO];
    }
}

-(void)deallocView {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1.0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [self willMoveToParentViewController:nil];
        [self.view removeFromSuperview];
        [self removeFromParentViewController];
        [self didMoveToParentViewController:nil];
        
        [[NSNotificationCenter defaultCenter] removeObserver:observer];
        observer = nil;
        [[NSNotificationCenter defaultCenter] removeObserver:observer2];
        observer2 = nil;
        isSignup = YES;
        //    [[NSNotificationCenter defaultCenter] removeObserver:self
        //                                                    name:@"NotificationPushReceived"
        //                                                  object:nil];
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:NOTIFICATION_SEARCH_FINISH
                                                      object:nil];
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:NOTIFICATION_SIGNUP
                                                      object:nil];
        
        NSLog(@"load view dealloced ");
    });
}

#pragma mark - Action
-(IBAction)onNaviButtonClick:(id)sender {
    UIButton *btn = (UIButton*)sender;
    NSLog(@"onNaviButtonClick:%d,%ld,%ld",(int)self.navigationController.viewControllers.count,(long)btn.tag,self.setupMode);
    //[super onNaviButtonClick:sender];
    
    // 로그인 이력이 있다. -> 로그인페이지
    if (![Utils getIsFirstLogin]) {
        if (btn.tag == 1100 || btn.tag == 1101) {
            [self removeEventListeners];
            //[self.navigationController popViewControllerAnimated:YES];
            // 전화번호 변경, 비밀번호 변경
            if (self.setupMode == 3 || self.setupMode == 4) {
                [self.navigationController popViewControllerAnimated:YES];
            }
            else if (self.setupMode == 0) {

                [self onSnsLoginUp:nil];
                //[self onSignIn:nil];

            }
            else {
                [self.navigationController popToRootViewControllerAnimated:YES];
            }
        }
        else if(btn.tag == 1300) {
            if (self.setupMode == 1) {
                [self signIn:nil];
            }
            else if (self.setupMode == 0) {
                [self signUp:nil];
            }
            else if (self.setupMode == 2) {
                [self signUpByForgot:nil];
            }
            else if (self.setupMode == 3) {
                [self changeTelno:nil];
            }
        }
    }
    // 로그인 이력이 없다. -> 회원가입페이지
    else {
        if (btn.tag == 1100 || btn.tag == 1101) {
            [self removeEventListeners];
            //[self.navigationController popViewControllerAnimated:YES];
            // 전화번호 변경, 비밀번호 변경
            if (self.setupMode == 1 || self.setupMode == 2 || self.setupMode == 3 || self.setupMode == 4) {
                [self.navigationController popViewControllerAnimated:YES];
            }
            else if (self.setupMode == 0) {
                
                [self onSnsLoginUp:nil];
                //[self onSignIn:nil];

            }
            else {
                [self.navigationController popToRootViewControllerAnimated:YES];
            }
        }
        else if(btn.tag == 1300) {
            if (self.setupMode == 1) {
                [self signIn:nil];
            }
            else if (self.setupMode == 0) {
                [self signUp:nil];
            }
            else if (self.setupMode == 2) {
                [self signUpByForgot:nil];
            }
            else if (self.setupMode == 3) {
                [self changeTelno:nil];
            }
        }
    }
}

-(IBAction)onTermsButtonClick:(id)sender {
    UIButton *btn = (UIButton*)sender;
    
    if (btn.tag == 115) {
        UIButton *btn01 = [_vTerms0 viewWithTag:15];
        UIButton *btn02 = [_vTerms0 viewWithTag:25];
        if (btn01) {
            btn = btn01;
        }
        else {
            btn = btn02;
        }
    }
    
    // on
    if (btn.tag == 15) {
        UIButton *btn1 = [_vTerms1 viewWithTag:11];
        UIButton *btn2 = [_vTerms2 viewWithTag:12];
#ifndef _DRIVER_MODE
        UIButton *btn3 = [_vTerms3 viewWithTag:13];
#endif
        UIButton *btn4 = [_vTerms4 viewWithTag:14];
        
        btn1.tag = 21;
        [btn1 setImage:[UIImage imageNamed:@"form_chkbox_chk"] forState:UIControlStateNormal];
        btn2.tag = 22;
        [btn2 setImage:[UIImage imageNamed:@"form_chkbox_chk"] forState:UIControlStateNormal];
#ifndef _DRIVER_MODE
        btn3.tag = 23;
        [btn3 setImage:[UIImage imageNamed:@"form_chkbox_chk"] forState:UIControlStateNormal];
#endif
        btn4.tag = 24;
        [btn4 setImage:[UIImage imageNamed:@"form_chkbox_chk"] forState:UIControlStateNormal];
        
        btn.tag = 25;
        [btn setImage:[UIImage imageNamed:@"form_chkbox_chk"] forState:UIControlStateNormal];
    }
    // off
    else if (btn.tag == 25) {
        UIButton *btn1 = [_vTerms1 viewWithTag:21];
        UIButton *btn2 = [_vTerms2 viewWithTag:22];
#ifndef _DRIVER_MODE
        UIButton *btn3 = [_vTerms3 viewWithTag:23];
#endif
        UIButton *btn4 = [_vTerms4 viewWithTag:24];
        
        btn1.tag = 11;
        [btn1 setImage:[UIImage imageNamed:@"form_chkbox_normal"] forState:UIControlStateNormal];
        btn2.tag = 12;
        [btn2 setImage:[UIImage imageNamed:@"form_chkbox_normal"] forState:UIControlStateNormal];
#ifndef _DRIVER_MODE
        btn3.tag = 13;
        [btn3 setImage:[UIImage imageNamed:@"form_chkbox_normal"] forState:UIControlStateNormal];
#endif
        btn4.tag = 14;
        [btn4 setImage:[UIImage imageNamed:@"form_chkbox_normal"] forState:UIControlStateNormal];
        
        btn.tag = 15;
        [btn setImage:[UIImage imageNamed:@"form_chkbox_normal"] forState:UIControlStateNormal];
    }
    
    if (btn.tag == 11 || btn.tag == 21) {
        if (btn.tag == 11) {
            btn.tag = 21;
            [btn setImage:[UIImage imageNamed:@"form_chkbox_chk"] forState:UIControlStateNormal];
        }
        else {
            btn.tag = 11;
            [btn setImage:[UIImage imageNamed:@"form_chkbox_normal"] forState:UIControlStateNormal];
        }
    }
    else if(btn.tag == 12 || btn.tag == 22) {
        if (btn.tag == 12) {
            btn.tag = 22;
            [btn setImage:[UIImage imageNamed:@"form_chkbox_chk"] forState:UIControlStateNormal];
        }
        else {
            btn.tag = 12;
            [btn setImage:[UIImage imageNamed:@"form_chkbox_normal"] forState:UIControlStateNormal];
        }
    }
    else if(btn.tag == 13 || btn.tag == 23) {
        if (btn.tag == 13) {
            btn.tag = 23;
            [btn setImage:[UIImage imageNamed:@"form_chkbox_chk"] forState:UIControlStateNormal];
        }
        else {
            btn.tag = 13;
            [btn setImage:[UIImage imageNamed:@"form_chkbox_normal"] forState:UIControlStateNormal];
        }
    }
    else if(btn.tag == 14 || btn.tag == 24) {
        if (btn.tag == 14) {
            btn.tag = 24;
            [btn setImage:[UIImage imageNamed:@"form_chkbox_chk"] forState:UIControlStateNormal];
        }
        else {
            btn.tag = 14;
            [btn setImage:[UIImage imageNamed:@"form_chkbox_normal"] forState:UIControlStateNormal];
        }
    }
    // open
    else if (btn.tag == 111) {
        // 한글은 인코딩되어야 한다.
        NSCharacterSet *set = [NSCharacterSet URLQueryAllowedCharacterSet];
        NSString* content = [LOCATION_TERM_URL stringByAddingPercentEncodingWithAllowedCharacters:set];
        [[UIApplication sharedApplication] openURL: [NSURL URLWithString:content]];
        //[[UIApplication sharedApplication] openURL: [NSURL URLWithString:LOCATION_TERM_URL]];
    }
    else if(btn.tag == 112) {
        NSCharacterSet *set = [NSCharacterSet URLQueryAllowedCharacterSet];
        NSString* content = [PRIVACY_TERM_URL stringByAddingPercentEncodingWithAllowedCharacters:set];
        [[UIApplication sharedApplication] openURL: [NSURL URLWithString:content]];
        //[[UIApplication sharedApplication] openURL: [NSURL URLWithString:PRIVACY_TERM_URL]];
    }
    else if(btn.tag == 113) {
#ifndef _DRIVER_MODE
        NSCharacterSet *set = [NSCharacterSet URLQueryAllowedCharacterSet];
        NSString* content = [SERVICE_TERM_URL stringByAddingPercentEncodingWithAllowedCharacters:set];
        [[UIApplication sharedApplication] openURL: [NSURL URLWithString:content]];
#endif
        //[[UIApplication sharedApplication] openURL: [NSURL URLWithString:SERVICE_TERM_URL]];
    }
    else if(btn.tag == 114) {
        NSCharacterSet *set = [NSCharacterSet URLQueryAllowedCharacterSet];
#ifdef _DRIVER_MODE
        NSString* content = [DRIVER_TERM_URL stringByAddingPercentEncodingWithAllowedCharacters:set];
#else
        NSString* content = [SERVICE_RENT_URL stringByAddingPercentEncodingWithAllowedCharacters:set];
#endif
        [[UIApplication sharedApplication] openURL: [NSURL URLWithString:content]];
        //[[UIApplication sharedApplication] openURL: [NSURL URLWithString:SERVICE_RENT_URL]];
    }
    UIButton *btn1 = [_vTerms1 viewWithTag:21];
    UIButton *btn2 = [_vTerms2 viewWithTag:22];
#ifndef _DRIVER_MODE
    UIButton *btn3 = [_vTerms3 viewWithTag:23];
#endif
    UIButton *btn4 = [_vTerms4 viewWithTag:24];
    if (btn1 && btn2 &&
#ifndef _DRIVER_MODE
        btn3 &&
#endif
        btn4) {
        // ok next
        //[self resetTermsButton:YES];
    }
    else {
        //[self resetTermsButton:NO];
    }
}
/*
-(BOOL)checkEmail:(NSString*)email
{
    if (![Utils validateEmailWithString:email]) {
        [[[LGAlertView alloc] initWithTitle:nil
                                    message:LocalizedStr(@"Validation.Email.text")
                                      style:LGAlertViewStyleAlert
                               buttonTitles:@[@"OK"]
                          cancelButtonTitle:nil
                     destructiveButtonTitle:nil
                              actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                                  NSLog(@"actionHandler, %@, %lu", title, (long unsigned)index);
                                  
                              }
                              cancelHandler:^(LGAlertView *alertView) {
                              }
                         destructiveHandler:^(LGAlertView *alertView) {
                         }] showAnimated:YES completionHandler:nil];
        
        return NO;
    }
    return YES;
}
*/
-(IBAction)goMapView:(id)sender {
    [self removeEventListeners];
    
#ifdef _DRIVER_MODE
    TXMapVC *mapVC = [[TXMapVC alloc] initWithNibName:@"TXMapDriverVC" bundle:nil];
#else
    TXMapVC *mapVC = [[TXMapVC alloc] initWithNibName:@"TXMapRiderVC" bundle:nil];
#endif
    
    //[appDelegate removeAllVCFromNavigation];
    [self pushViewController:mapVC];
    NSArray* tempVCA = [self.navigationController viewControllers];
    for(UIViewController *tempVC in tempVCA)
    {
        if(![tempVC isKindOfClass:[TXMapVC class]]) {
            [tempVC removeFromParentViewController];
        }
    }
    
//    //Remove the child from the VC hierarchy
//    [self willMoveToParentViewController:nil];
//    [self.view removeFromSuperview];
//    [self removeFromParentViewController];
//    [self didMoveToParentViewController:nil];
    
    [self removeFromParentViewController];
    
    [self deallocView];
}

-(IBAction)onButtonClick:(id)sender
{
    if (self.btnGetCode.tag) {
        if (self.setupMode == 1) {
            [self signIn:sender];
        }
        else if (self.setupMode == 0) {
            [self signUp:sender];
        }
        //        else if (self.setupMode == 2) {
        //            [self.btnGetCode addTarget:self action:@selector(signUpByForgot:) forControlEvents:UIControlEventTouchUpInside];
        //        }
        else if (self.setupMode == 3) {
            [self changeTelno:sender];
        }
    }
    else {
        [self getVerificationCode:sender];
    }
}

-(IBAction)getVerificationCode:(id)sender {
    
    if (!self.imgTimer.hidden) {
        [self.btnGetCode setAlpha:0.3];
        [self.btnGetCode setEnabled:NO];
        return;
    }
    
    validatorIndex = 1;
    [self validateAction:sender];
    
    return;
}

-(IBAction)fotgorPasswd:(id)sender {
    [self resignKeyboard];
    
    TXSignInVC *signInVC = [[TXSignInVC alloc] initWithNibName:@"TXSignInVC" bundle:nil mode:2];
    signInVC.view.frame = [super view].frame;
    [self pushViewController:signInVC];
}

-(IBAction)onNewPw:(id)sender {
    [self resignKeyboard];
    
    TXSignInVC *signInVC = [[TXSignInVC alloc] initWithNibName:@"TXSignInVC" bundle:nil mode:4];
    signInVC.view.frame = [super view].frame;
    [self pushViewController:signInVC];
}

-(IBAction)onCountrySearch:(id)sender {
    [self resignKeyboard];
    
    TXSearchVC *vc = [[TXSearchVC alloc] initWithNibName:@"TXSearchVC" bundle:nil];
    vc.selectCountryCode = self.countryISOCode;
    vc.view.frame = [super view].frame;
    [self pushViewController:vc];
}

-(IBAction)onTerms:(id)sender {
    [self resignKeyboard];
    
    // 약관동의
    TXSignInVC *signInVC = [[TXSignInVC alloc] initWithNibName:@"TXSignInVC" bundle:nil mode:5];
    signInVC.view.frame = [super view].frame;
    [self pushViewController:signInVC];
    
    [self deallocView];
}

-(IBAction)onSnsSignUp:(id)sender {
    [self resignKeyboard];
    
    // 가입하기
    TXSnsLoginVC *signInVC = [[TXSnsLoginVC alloc] initWithNibName:@"TXSnsLoginVC" bundle:nil];
    signInVC.setupMode = 99;
    signInVC.view.frame = [super view].frame;
    [self pushViewController:signInVC];
    NSLog(@"onSignUp TXSignInVC");
    
    [self deallocView];
}

-(IBAction)onSnsLoginUp:(id)sender {
    [self resignKeyboard];
    
    // 가입하기
    TXSnsLoginVC *signInVC = [[TXSnsLoginVC alloc] initWithNibName:@"TXSnsLoginVC" bundle:nil];
    signInVC.setupMode = 88;
    signInVC.view.frame = [super view].frame;
    [self pushViewController:signInVC];
    NSLog(@"onSignUp TXSignInVC");
    
    [self deallocView];
}

-(IBAction)onSignUp:(id)sender {
    [self resignKeyboard];
    
    // 가입하기
    TXSignInVC *signInVC = [[TXSignInVC alloc] initWithNibName:@"TXSignInVC" bundle:nil mode:0];
    signInVC.view.frame = [super view].frame;
    [self pushViewController:signInVC];
    NSLog(@"onSignUp TXSignInVC");
    
    [self deallocView];
}

-(IBAction)onSignIn:(id)sender {
    [self resignKeyboard];
    
    // 로그인하기
    TXSignInVC *signInVC = [[TXSignInVC alloc] initWithNibName:@"TXSignInVC" bundle:nil mode:1];
    signInVC.view.frame = [super view].frame;
    [self pushViewController:signInVC];
    NSLog(@"onSignIn TXSignInVC");
    
    [self deallocView];
}


// 1
-(IBAction)signIn:(id)sender {
    if (self.btnGetCode.tag) {
        validatorIndex = 2;
        [self validateAction:sender];
    }
    else {
        [self getVerificationCode:nil];
    }
    return;
}

// 0
-(void)signUp:(id)sender {
    
    validatorIndex = 3;
    [self validateAction:sender];
    return;
}

// 1
-(void)signUpByForgot:(id)sender {
    
    validatorIndex = 4;
    [self validateAction:sender];
    return;
}

// 3
-(void)changeTelno:(id)sender {
    
    validatorIndex = 5;
    [self validateAction:sender];
    return;
}

// 2
-(void)requestEmail:(id)sender {
    
    validatorIndex = 6;
    [self validateAction:sender];
    return;
}

// 4
-(void)changePw:(id)sender {
    
    validatorIndex = 7;
    [self validateAction:sender];
    return;
}

// 5
-(void)termsAllow:(id)sender {
    
    validatorIndex = 8;
    UIButton *btn1 = [_vTerms1 viewWithTag:21];
    UIButton *btn2 = [_vTerms2 viewWithTag:22];
#ifndef _DRIVER_MODE
    UIButton *btn3 = [_vTerms3 viewWithTag:23];
#endif
    UIButton *btn4 = [_vTerms4 viewWithTag:24];
    if (btn1 && btn2 &&
#ifndef _DRIVER_MODE
        btn3 &&
#endif
        btn4) {
        // ok next
        // api
#ifdef _DRIVER_MODE
        NSDictionary *_propertyMap = @{
                                       @"provider_agree":@{
                                               @"private_location" : @"1",
                                               @"private_information" : @"1",
                                               @"driver_utility" : @"1",
                                               }
                                       };
        
        [self showBusyIndicator:@"Requesting ... "];
        [self->model requsetAPI:@"PP211" property:_propertyMap];
#else
        NSDictionary *_propertyMap = @{
                                       @"user_agree":@{
                                               @"private_location" : @"1",
                                               @"private_information" : @"1",
                                               @"service_utility" : @"1",
                                               @"service_rent" : @"1"
                                               }
                                       };
        
        [self showBusyIndicator:@"Requesting ... "];
        [self->model requsetAPI:@"UR211" property:_propertyMap];
#endif
    }
    else {
        [WToast showWithText:LocalizedStr(@"Intro.Terms.alert.text") duration:kWTShort roundedCorners:NO gravity:kWTGravityTop];
    }
    return;
}
#pragma mark -
#pragma mark Validator
#pragma mark -
-(void) resignKeyboard
{
    [self.view endEditing:YES];
    [self.vbody endEditing:YES];
}

- (BOOL)validatePasswd:(BOOL)flag {
    if (self.setupMode != 4) {
        return NO;
    }
    if (self.txtPassword.text.length == 0) {
        self.lbNewFooter1.text = @"";
        if (!flag) {
            UILabel *lb = (UILabel*)[self.vPasswd viewWithTag:1];
            lb.textColor = UIColorLabelTextColor;
            return NO;
        }
    }
    if (self.txtPassword.text.length < 6) {
        UILabel *lb = (UILabel*)[self.vPasswd viewWithTag:1];
        lb.textColor = [UIColor redColor];
        self.lbNewFooter1.text = LocalizedStr(@"Validator.Passwd.text");
        return NO;
    }
    else {
        UILabel *lb = (UILabel*)[self.vPasswd viewWithTag:1];
        lb.textColor = [UIColor blueColor];
        self.lbNewFooter1.text = @"";
    }
    
    if (self.txtPasswordNew.text.length == 0) {
        self.lbNewFooter1.text = @"";
        if (!flag) {
            UILabel *lb = (UILabel*)[self.vPasswdNew viewWithTag:1];
            lb.textColor = UIColorLabelTextColor;
            return NO;
        }
    }
    if (![self.txtPassword.text isEqualToString:self.txtPasswordNew.text]) {
        UILabel *lb = (UILabel*)[self.vPasswdNew viewWithTag:1];
        lb.textColor = [UIColor redColor];
        self.lbNewFooter1.text = LocalizedStr(@"Validator.Passwd.text2");
        return NO;
    }
    else {
        UILabel *lb = (UILabel*)[self.vPasswdNew viewWithTag:1];
        lb.textColor = [UIColor blueColor];
        self.lbNewFooter1.text = @"";
    }
    return YES;
}

- (IBAction)validateAction:(id)sender
{
    
    [self resignKeyboard];
    
    Validator *validator = [[Validator alloc] init];
    validator.delegate   = self;
    // 0: signup 1: signin 2:reset passwd 3:profile, 4:new pw
    // 0: name, email, phone, passwwd, code
    // 1:       email, phone, passwwd, code
    // 2:       email, ncode
    // 3:       email, phone, code
    // 4:       passwwd, newpasswwd, ncode
    
    if (self.setupMode == 0) {
        // 이름
        [validator putRule:[Rules minLength:2 withFailureString:LocalizedStr(@"Validator.Name.text") forTextField:self.txtUsername]];
        //[validator putRule:[Rules checkIfAlphaNumericWithFailureString:LocalizedStr(@"Validator.Name.text") forTextField:self.txtUsername]];
//#ifdef _DRIVER_MODE
//        // 이름
//        [validator putRule:[Rules minLength:2 withFailureString:LocalizedStr(@"Validator.Name.text") forTextField:self.txtUsernameL]];
//#endif
    }
    
    if (self.setupMode == 0 || self.setupMode == 1 || self.setupMode == 2 || self.setupMode == 3) {
        // 이메일
        [validator putRule:[Rules minLength:8 withFailureString:LocalizedStr(@"Validator.Email.text1") forTextField:self.txtEmail]];
        [validator putRule:[Rules checkIsValidEmailWithFailureString:LocalizedStr(@"Validator.Email.text2") forTextField:self.txtEmail]];
    }
    
    if (self.setupMode == 0 || self.setupMode == 1 || self.setupMode == 3) {
        // 전화번호
        [validator putRule:[Rules checkRange:NSMakeRange(8, 14) withFailureString:LocalizedStr(@"Validator.Telno.text") forTextField:self.txtPhoneNum]];
        //[validator putRule:[Rules checkValidCountryCodeUSWithFailureString:LocalizedStr(@"Validator.Telno.text") forTextField:self.txtPhoneNum]];
    }
    
    if (self.setupMode == 4) {
        // 비번
//        [validator putRule:[Rules minLength:6 withFailureString:LocalizedStr(@"Validator.Passwd.text") forTextField:self.txtPassword]];
//        [validator putRule:[Rules minLength:6 withFailureString:LocalizedStr(@"Validator.Passwd.text") forTextField:self.txtPasswordNew]];
        
        if (![self validatePasswd:YES]) {
            return;
        }
    }
    
    if (self.setupMode == 0 || self.setupMode == 1) {
        if (!self.snsProfile) {
            // 비번
            [validator putRule:[Rules minLength:6 withFailureString:LocalizedStr(@"Validator.Passwd.text") forTextField:self.txtPassword]];
        }
    }
    
//    if (self.setupMode == 0 || self.setupMode == 1 || self.setupMode == 3) {
//        // 인증코드
//        [validator putRule:[Rules minLength:6 withFailureString:LocalizedStr(@"Validator.Otp.text") forTextField:self.txtVerifyCode]];
//        [validator putRule:[Rules checkIfNumericWithFailureString:LocalizedStr(@"Validator.Otp.text") forTextField:self.txtVerifyCode]];
//    }
    
    [validator validate];
}

#pragma ValidatorDelegate - Delegate methods

- (void) preValidation
{
}

- (void)onSuccess
{
/*
//#if !TARGET_IPHONE_SIMULATOR
    if (![CLLocationManager locationServicesEnabled]) {
        
        [appDelegate onCommonErrorAlert:LocalizedStr(@"Intro.GPS.Error.text")];
        return;
    }
    else {
        if (self->locationMgr == nil) {
            self->locationMgr = [[CLLocationManager alloc] init];
            self->locationMgr.delegate = self;
//            self->locationMgr.allowsBackgroundLocationUpdates = YES;
//            self->locationMgr.pausesLocationUpdatesAutomatically = NO;
            self->locationMgr.activityType = CLActivityTypeAutomotiveNavigation;
        }
        
        if (appDelegate.isDriverMode) {
            if ([CLLocationManager authorizationStatus] != kCLAuthorizationStatusAuthorizedWhenInUse) {
                // 사용중일때만 요청
                if ([self->locationMgr respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
                    [self->locationMgr requestWhenInUseAuthorization];
                }
                self->locationMgr.distanceFilter = kCLDistanceFilterNone;
                self->locationMgr.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
                [self->locationMgr startUpdatingLocation];
                return;
            }
        }
        else {
            if ([CLLocationManager authorizationStatus] != kCLAuthorizationStatusAuthorizedWhenInUse) {
                // 사용중일때만 요청
                if ([self->locationMgr respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
                    [self->locationMgr requestWhenInUseAuthorization];
                }
                self->locationMgr.distanceFilter = kCLDistanceFilterNone;
                self->locationMgr.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
                [self->locationMgr startUpdatingLocation];
                return;
            }
        }
        [self->locationMgr startUpdatingLocation];
    }
//#endif
    */
    
    // 0: signup 1: signin 2:reset passwd 3:profile, 4:new pw
    // 0: name, email, phone, passwwd, code
    // 1:       email, phone, passwwd, code
    // 2:       email, ncode
    // 3:       email, phone, code
    // 4:       passwwd, newpasswwd, ncode
    if (validatorIndex == 1) {
        self.txtPhoneNum.text = [Utils getNumber:self.txtPhoneNum.text];
        
        TXUser *user = [[TXUser alloc] init];
        
        if (self.setupMode == 3) {
            user.uid   = [[[TXApp instance] getSettings] getUserId];
            user.pwd = [[[TXApp instance] getSettings] getPassword];
        }
        else {
            user.uid = self.txtEmail.text;
            user.pwd = [Utils passcodeBysha256:self.txtPassword.text];
        }
        NSString *prefix = [[self.txtCountry.text stringByReplacingOccurrencesOfString:@"("
                                                                            withString:@""] stringByReplacingOccurrencesOfString:@")"
                            withString:@""];
        
        user.utelno = [Utils NBPhoneNumber:[NSString stringWithFormat:@"%@%@", prefix, self.txtPhoneNum.text] locale:self.countryISOCode];
        
        [self showBusyIndicator:@"Requesting verification code ... "];

#ifdef _DRIVER_MODE
        if (self.setupMode == 3) {
            //[self->model PP212:user];
        }
        else {
            [self->model PP111:user];
        }
#else
        if (self.setupMode == 3) {
            //[self->model UR212:user];
        }
        else {
            [self->model UR111:user];
        }
#endif
        // 동작멈추기
        //[self startTimer];
        NSLog(@"getVerificationCode7:%ld,%ld", (long)validatorIndex,(long)self.setupMode);
        return;
    }
    NSLog(@"getVerificationCode8:%ld,%ld", (long)validatorIndex,(long)self.setupMode);
    if (self.setupMode == 1) {
        self.txtPhoneNum.text = [Utils getNumber:self.txtPhoneNum.text];
        
        NSString *prefix = [[self.txtCountry.text stringByReplacingOccurrencesOfString:@"("
                                                                            withString:@""] stringByReplacingOccurrencesOfString:@")"
                            withString:@""];
        
        TXUser *user = [[TXUser alloc] init];
        user.uid    = self.txtEmail.text;
        user.utelno = [Utils NBPhoneNumber:[NSString stringWithFormat:@"%@%@", prefix, self.txtPhoneNum.text] locale:self.countryISOCode];
        user.pwd    = [Utils passcodeBysha256:self.txtPassword.text];
        user.otp    = self.txtVerifyCode.text;
        if (self.snsProfile) {
            user.sns = self.snsProfile;
        }
        [self showBusyIndicator:@"Authenticating ... "];
#ifdef _DRIVER_MODE
            [self->model PP110:user];
#else
            [self->model UR110:user];
#endif
    }
    else if (self.setupMode == 0) {
        self.txtPhoneNum.text = [Utils getNumber:self.txtPhoneNum.text];
        NSString *prefix = [[self.txtCountry.text stringByReplacingOccurrencesOfString:@"("
                                                                            withString:@""] stringByReplacingOccurrencesOfString:@")"
                            withString:@""];
        
        TXUser *user = [[TXUser alloc] init];
        user.uid    = self.txtEmail.text;
        user.name   = [NSString stringWithFormat:@"%@",self.txtUsername.text];
        user.utelno = [Utils NBPhoneNumber:[NSString stringWithFormat:@"%@%@", prefix, self.txtPhoneNum.text] locale:self.countryISOCode];
        user.otp    = self.txtVerifyCode.text;
        user.pwd    = [Utils passcodeBysha256:self.txtPassword.text];
        if (self.snsProfile) {
            user.sns = self.snsProfile;
        }
        [[[TXApp instance] getSettings] setFDKeychain:@"SIGNUP_NAME" value:user.name];
        
        [self showBusyIndicator:@"Requesting signUp ... "];
        
#ifdef _DRIVER_MODE
            [self->model PP120:user];
#else
            [self->model UR120:user];
#endif
    }
    else if (self.setupMode == 2) { // reset email
        NSString* deviceID = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.DEVICEID];
        NSDictionary *param = @{@"uid":self.txtEmail.text, @"did":deviceID};
        
        RTLabel *lb = (RTLabel*)[self.vReset viewWithTag:100];
        lb.text = @"";
        
        [self showBusyIndicator:@"Requesting signIn ... "];
#ifdef _DRIVER_MODE
        [self->model requsetAPI:@"PP113" property:param];
#else
        [self->model requsetAPI:@"UR113" property:param];
#endif
    }
    else if (self.setupMode == 3) {
        self.txtPhoneNum.text = [Utils getNumber:self.txtPhoneNum.text];
        NSString *prefix = [[self.txtCountry.text stringByReplacingOccurrencesOfString:@"("
                                                                            withString:@""] stringByReplacingOccurrencesOfString:@")"
                            withString:@""];
        
        NSString* utelno = [Utils NBPhoneNumber:[NSString stringWithFormat:@"%@%@", prefix, self.txtPhoneNum.text] locale:self.countryISOCode];
        NSString* pseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.PSEQ];
        NSString* otp = self.txtVerifyCode.text;
        
        NSDictionary *param = @{@"utelno":utelno, @"otp":otp, @"pseq":pseq};
        
        [self showBusyIndicator:@"Requesting signIn ... "];
#ifdef _DRIVER_MODE
        [self->model requsetAPI:@"PP210" property:param];
#else
        [self->model requsetAPI:@"UR210" property:param];
#endif
    }
    else if (self.setupMode == 4) { // new pw
        NSString *pwd    = [Utils passcodeBysha256:self.txtPasswordNew.text];
        
        if (datoken == nil) {
            datoken = [appDelegate.customShareDic objectForKey:@"datoken"];
        }
        NSDictionary *param = @{@"pwd":pwd, @"datoken":datoken};
        
        [self showBusyIndicator:@"Requesting signIn ... "];
#ifdef _DRIVER_MODE
        [self->model requsetAPI:@"PP210" property:param];
#else
        [self->model requsetAPI:@"UR210" property:param];
#endif
    }
}

- (void)onFailure:(Rule *)failedRule
{
//    NSMutableDictionary *options = [@{kCRToastNotificationTypeKey               : @(CRToastTypeNavigationBar),
//                                      kCRToastNotificationPresentationTypeKey   : @(CRToastPresentationTypeCover),
//                                      kCRToastUnderStatusBarKey                 : @(YES),
//                                      kCRToastTextKey                           : failedRule.failureMessage,
//                                      kCRToastFontKey                           : [UIFont systemFontOfSize:14],
//                                      kCRToastTextAlignmentKey                  : @(NSTextAlignmentCenter),
//                                      kCRToastTimeIntervalKey                   : @(2),
//                                      kCRToastAnimationInTypeKey                : @(CRToastAnimationTypeGravity),
//                                      kCRToastAnimationOutTypeKey               : @(CRToastAnimationTypeGravity),
//                                      kCRToastAnimationInDirectionKey           : @(CRToastAnimationDirectionTop),
//                                      kCRToastAnimationOutDirectionKey          : @(CRToastAnimationDirectionTop),
//                                      kCRToastNotificationPreferredPaddingKey   : @(15)} mutableCopy];
//    
//    [CRToastManager showNotificationWithOptions:options
//                                completionBlock:^{
//                                    NSLog(@"Completed");
//                                }];
    
    [WToast showWithText:failedRule.failureMessage duration:kWTShort roundedCorners:NO gravity:kWTGravityTop];
}
/*
#pragma mark CLLocationManager delegate

-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways) {
        [self->locationMgr startUpdatingLocation];
    }
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    
    CLLocation *location = [manager location];
    
    NSNumber *myDoubleNumberLat = [NSNumber numberWithDouble:location.coordinate.latitude];
    NSNumber *myDoubleNumberLng = [NSNumber numberWithDouble:location.coordinate.longitude];
    
    [Utils updateCurrentPosition:[myDoubleNumberLat stringValue] lng:[myDoubleNumberLng stringValue]];
    
    appDelegate.location = location;
    
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    locMgr.realTimeCLocation = location;
    [locMgr updateCurrentLocation:location];
    
    [self->locationMgr stopUpdatingLocation];
    NSLog(@"didUpdateLocations: %@", location);
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
    NSLog(@"Error while getting core location : %@",[error localizedFailureReason]);
    if ([error code] == kCLErrorDenied) {
        //you had denied
        NSString *prodName = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleDisplayName"];
        NSString *message = [NSString stringWithFormat:LocalizedStr(@"Intro.LocationService.Error.text"),prodName];
        
        LGAlertView *alertView = [[LGAlertView alloc] initWithTitle:@"Location Service"
                                                            message:message
                                                              style:LGAlertViewStyleAlert
                                                       buttonTitles:@[LocalizedStr(@"Button.OK")]
                                                  cancelButtonTitle:nil
                                             destructiveButtonTitle:nil
                                                      actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {

                                                          //NSString *prodName = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleDisplayName"];
                                                          //NSString *path = [NSString stringWithFormat:@"prefs:root=LOCATION_SERVICES&path=%@",prodName];
                                                          //http://stackoverflow.com/questions/8246070/ios-launching-settings-restrictions-url-scheme/8246814#8246814
                                                          //NSString *path = [NSString stringWithFormat:@"prefs:root=LOCATION_SERVICES"];
                                                          //https://gist.github.com/phynet/471089a51b8f940f0fb4
                                                          //[[UIApplication sharedApplication] openURL:[NSURL URLWithString:path]];
                                                          [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
                                                      }
                                                      cancelHandler:^(LGAlertView *alertView) {
                                                      }
                                                 destructiveHandler:^(LGAlertView *alertView) {
                                                 }];
        [Utils initAlertButtonColor:alertView];
        [alertView showAnimated:YES completionHandler:nil];
    }
    [manager stopUpdatingLocation];
}
*/
#pragma mark - RTLabel Delegate
-(void)rtLabel:(id)rtLabel didSelectLinkWithURL:(NSURL*)url
{
    NSString *link = [url absoluteString];
    //RTLabel * label = (RTLabel*)rtLabel;
    NSLog(@"url:%@",link);
    if ([link isEqualToString:@"signup"]) {
        // goto signup
        [self onSignUp:nil];
        
        double delayInSeconds = 5;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0),
                       ^(void){
                           [self removeFromParentViewController];
                       });
    }
    //[[UIApplication sharedApplication] openURL: [NSURL URLWithString:[url absoluteString]]];
    //[self openBrowserViewForURL:[url absoluteString] pageTitle:label.text];
    
}

@end
