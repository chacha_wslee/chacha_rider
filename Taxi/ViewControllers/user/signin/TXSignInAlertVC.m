//
//  TXSignInVC.m

//

#import "TXSignInAlertVC.h"

#import "TXMainVC.h"
#import "TXUserModel.h"

@interface TXSignInAlertVC () {
}
@end

@implementation TXSignInAlertVC


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil mode:(NSInteger)mode
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

#pragma mark - UIViewController

-(void)configureStyles {
    [super configureStyles];
    
#ifdef _DRIVER_MODE
    [self.splash_background setImage:[UIImage imageNamed:@"spalsh_background_driver"]];
#else
    
#ifdef _WITZM
    [self.splash_background setImage:[UIImage imageNamed:@"spalsh_background"]];
#elif defined _CHACHA
    [self.view setBackgroundColor:UIColorDefault];
    [self.splash_background setBackgroundColor:HEXCOLOR(0xFFFFFF66)];
#endif
    
#endif

}

-(void) configure {

    [super configure];
    
    [super statusBarStyle];
    
    self.vBg.backgroundColor = HEXCOLOR(0x03010180);
    
    self.lbTitle.text = LocalizedStr(@"Intro.Login.GetVerification.POPUP.title.text");
    [self.lbTitle setTextColor:HEXCOLOR(0x333333ff)];
    
    self.lbBody.text = LocalizedStr(@"Intro.Login.GetVerification.POPUP.body.text");
    [self.lbBody setTextColor:HEXCOLOR(0x333333ff)];
    [self.lbBody sizeToFit];
    
    [self performSelector:@selector(onNaviButtonClick:) withObject:self.btnClose afterDelay:10];
}

#pragma mark ViewLoad

-(void)viewDidLoad {
    [super viewDidLoad];

}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

-(void)dealloc {
}

-(BOOL)prefersStatusBarHidden{
    return NO;
}


#pragma mark - Action
-(IBAction)onNaviButtonClick:(id)sender {
    UIButton *btn = (UIButton*)sender;
    
    if (btn.tag == 1100 || btn.tag == 1101) {
        [self dismissViewControllerAnimated:YES completion:nil];
        //[self.navigationController popViewControllerAnimated:YES];
    }
}

@end
