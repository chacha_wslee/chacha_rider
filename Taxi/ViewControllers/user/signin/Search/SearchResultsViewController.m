//
//  SearchResultsViewController.m
//  Table Search
//
//  Created by Jay Versluis on 03/11/2015.
//  Copyright © 2015 Pinkstone Pictures LLC. All rights reserved.
//

#import "SearchCell.h"
#import "SearchResultsViewController.h"

@interface SearchResultsViewController ()
@property (nonatomic, strong) NSArray *searchResults;
@end

@implementation SearchResultsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIView *n = [super navigationView];
    NSInteger _y = n.frame.origin.y + n.frame.size.height;
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, _y, self.view.bounds.size.width, self.view.bounds.size.height-_y)
                                                  style:UITableViewStylePlain];
    self.tableView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:self.tableView];
    
    UINib *nib = [UINib nibWithNibName:@"SearchCell" bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:@"SearchCell"];
    
    self.tableView.separatorColor = [UIColor clearColor];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.opaque = NO;
    self.tableView.backgroundColor = [UIColor clearColor];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
}

-(BOOL)prefersStatusBarHidden{
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//- (void) traitCollectionDidChange: (UITraitCollection *) previousTraitCollection {
//    
//    [super traitCollectionDidChange: previousTraitCollection];
//    
//    if(self.active && ![UIApplication sharedApplication].statusBarHidden && self.controller.searchBar.frame.origin.y == 0)
//    {
//        UIView *container = self.controller.searchBar.superview;
//        
//        container.frame = CGRectMake(container.frame.origin.x, container.frame.origin.y, container.frame.size.width, container.frame.size.height + [UIApplication sharedApplication].statusBarFrame.size.height);
//    }
//}


#pragma mark - Configure

-(void)configureStyles {
    [super configureStyles];
    
    [super navigationType010:[UIImage imageNamed:@"btn_back"] centerText:LocalizedStr(@"Intro.Country.Detail.title") rightImage:[UIImage imageNamed:@"btn_search"]];
}

-(void)configure {
    [super configure];
    
#ifdef _WITZM
    
#ifdef _DRIVER_MODE
    [self.splash_background setImage:[UIImage imageNamed:@"spalsh_background_driver"]];
#else
    [self.splash_background setImage:[UIImage imageNamed:@"spalsh_background"]];
#endif

#elif defined _CHACHA
#endif
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return self.searchResults.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SearchCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SearchCell" forIndexPath:indexPath];
    [cell bindData:self.searchResults[indexPath.item]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSDictionary *userInfo = self.searchResults[indexPath.item];
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_SEARCH_RESULT object:nil userInfo:userInfo];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context {
    
    // extract array from observer
    self.searchResults = [(NSArray *)object valueForKey:@"results"];
    [self.tableView reloadData];
}


@end
