

#import "utilities.h"
#import "utils.h"

//-------------------------------------------------------------------------------------------------------------------------------------------------
@interface SearchCell : UITableViewCell
//-------------------------------------------------------------------------------------------------------------------------------------------------

@property (strong, nonatomic) IBOutlet UILabel *name;
@property (strong, nonatomic) IBOutlet UIImageView *country;
@property (strong, nonatomic) IBOutlet UILabel *dial;


- (void)bindData:(NSDictionary *)talks_;

@end

