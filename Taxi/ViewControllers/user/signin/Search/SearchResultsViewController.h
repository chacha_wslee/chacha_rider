//
//  SearchResultsViewController.h
//  Table Search
//
//  Created by Jay Versluis on 03/11/2015.
//  Copyright © 2015 Pinkstone Pictures LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TXBaseViewController.h"

@interface SearchResultsViewController : TXBaseViewController <UITableViewDelegate, UITableViewDataSource> {
    
}

@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (retain, nonatomic) IBOutlet UIImageView    *splash_background;
@property (retain, nonatomic) IBOutlet UIView    *splash_view;
@end
