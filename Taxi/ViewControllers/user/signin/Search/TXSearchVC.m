
#import "TXSearchVC.h"
#import "SearchCell.h"
#import "LGAlertView.h"
#import "SearchResultsViewController.h"

@interface TXSearchVC () <UITableViewDelegate, UITableViewDataSource, UISearchResultsUpdating, UISearchControllerDelegate, UISearchBarDelegate> {
    
    BOOL beforeLogin;
}
@property (strong, nonatomic) NSMutableArray *data;
@property (strong, nonatomic) UISearchController *controller;
@property (strong, nonatomic) NSArray *results;

@property (strong, nonatomic) NSMutableArray *arrayCity;
@property (strong, nonatomic) NSMutableArray *sortedCountryArray;

@property (strong, nonatomic) SearchResultsViewController *searchResults;

@end

@implementation TXSearchVC

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveSearchNotification:) name:NOTIFICATION_SEARCH_RESULT object:nil];
    
    [super configureBottomLine];
    UIView *n = [super navigationView];
    NSInteger _y = n.frame.origin.y + n.frame.size.height;
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, _y, self.view.bounds.size.width, self.view.bounds.size.height-_y)
                                                  style:UITableViewStylePlain];
    self.tableView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:self.tableView];
    
    UINib *nib = [UINib nibWithNibName:@"SearchCell" bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:@"SearchCell"];
    
    self.tableView.separatorColor = [UIColor clearColor];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.opaque = NO;
    self.tableView.backgroundColor = [UIColor clearColor];
//    self.tableView.tableHeaderView = ({
//        
//        UIView *view = [Utils addLabelView:self.view underView:nil dim:0];
//        
//        UILabel *label = [view viewWithTag:11];
//        label.text = LocalizedStr(@"Intro.Country.Header.title");
//        label.tag = 2001;
//        //[label sizeToFit];
//        //view.autoresizingMask = UIViewAutoresizingFlexibleHeight;
//        
//        view;
//    });
    
    NSInteger index__ = [self insertItemData];
    
    //---------------------------------------------------------------------------------------------------------------------------------------------
    SearchResultsViewController *resultsController = [[SearchResultsViewController alloc] init];
    
    _controller = [[UISearchController alloc]initWithSearchResultsController:resultsController];
    _controller.searchResultsUpdater = self;
    //[_controller.searchBar sizeToFit];
    [self.controller.searchBar sizeToFit];
    
//    self.definesPresentationContext = YES;
//    self.extendedLayoutIncludesOpaqueBars = YES;
    
    self.controller.hidesNavigationBarDuringPresentation = NO;
    self.definesPresentationContext = NO;
    
    _controller.delegate = self;
    
    self.searchResults = (SearchResultsViewController *)self.controller.searchResultsController;
    [self addObserver:self.searchResults forKeyPath:@"results" options:NSKeyValueObservingOptionNew context:nil];
    //---------------------------------------------------------------------------------------------------------------------------------------------
    
    //----
    [self performSelector:@selector(updateCellRow:) withObject:[NSString stringWithFormat:@"%d",(int)index__] afterDelay:0.1];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}

#pragma mark - Configure

-(void)configureStyles {
    [super configureStyles];
    
    //[super navigationType01X:[UIImage imageNamed:@"btn_back"] centerText:LocalizedStr(@"Intro.Country.Detail.title")];
    //[super navigationType010:[UIImage imageNamed:@"btn_back"] centerText:LocalizedStr(@"Intro.Country.Detail.title") rightImage:[UIImage imageNamed:@"btn_search"]];
    [super navigationType01X:[UIImage imageNamed:@"btn_back_02"] centerText:LocalizedStr(@"Intro.Country.Detail.title")];
    [super navigationView].backgroundColor = [UIColor clearColor];
    UILabel *lb = [[super view] viewWithTag:1210];
    lb.textColor = [UIColor whiteColor];
//    [super statusView].backgroundColor = [UIColor clearColor];
//    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

-(void)configure {
    [super configure];

#ifdef _WITZM
    
#ifdef _DRIVER_MODE
    [self.splash_background setImage:[UIImage imageNamed:@"spalsh_background_driver"]];
#else
    [self.splash_background setImage:[UIImage imageNamed:@"spalsh_background"]];
#endif
    
#elif defined _CHACHA
#endif

    self.splash_view.backgroundColor = HEXCOLOR(0x030101FF);
    self.splash_view.alpha = 0.5f;
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)dealloc
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    [self removeObserver:self.searchResults forKeyPath:@"results"];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:NOTIFICATION_SEARCH_RESULT
                                                  object:nil];
}

//- (void)setNeedsLayout
//{
//    [super setNeedsStatusBarAppearanceUpdate];
//}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillTransitionToSize:(CGSize)size
       withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
    NSLog(@"%@", coordinator);
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
}


#pragma mark Work
//---------------------------------------------------------------------------------------------------------------------------------------------
- (void)onClickToolbarLeft:(id)sender
//---------------------------------------------------------------------------------------------------------------------------------------------
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)onNaviButtonClick:(id)sender {
    UIButton *btn = (UIButton*)sender;
    NSLog(@"onNaviButtonClick");
    //[super onNaviButtonClick:sender];
    
    if (btn.tag == 1100) {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else if (btn.tag == 1300) {
        // present the search controller
        [self presentViewController:self.controller animated:YES completion:nil];
    }
}

-(void)updateCellRow:(id)sender
{
    NSString *index__ = (NSString*)sender;
    NSIndexPath *newIndexPath = [NSIndexPath indexPathForRow:[index__ integerValue] inSection:0];
    [self.tableView scrollToRowAtIndexPath:newIndexPath
                          atScrollPosition:UITableViewScrollPositionTop
                                  animated:NO];
}

//---------------------------------------------------------------------------------------------------------------------------------------------
- (void) sendSelectionNotification:(NSDictionary*)userInfo
//---------------------------------------------------------------------------------------------------------------------------------------------
{
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_SEARCH_FINISH object:nil userInfo:userInfo];
    
    //[self performSelector:@selector(onClickToolbarLeft:) withObject:nil afterDelay:0.5];
    [self onClickToolbarLeft:nil];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void) receiveSearchNotification:(NSNotification *) notification
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    NSDictionary *userInfo = notification.userInfo;
    //
    //NSLog(@"User selected %@", userInfo);
    
    [self.controller dismissViewControllerAnimated:YES completion:nil];
    
    [self sendSelectionNotification:userInfo];
}

- (NSInteger)insertItemData
{
    _data = [[NSMutableArray alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"countries" ofType:@"plist"]];
    //[_data sortUsingSelector:@selector(localizedCompare:)];
    
    NSLocale *locale = [NSLocale currentLocale];
    //NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"ko"];
    
    //NSString *localeCountryCode = [locale objectForKey:NSLocaleCountryCode];
    
    // locale이용
    NSArray *cArray = [NSLocale ISOCountryCodes];
    NSMutableDictionary *sList = [[NSMutableDictionary alloc] init];
    
    for (NSString *countryCode in cArray) {
        NSString *countryName = [locale displayNameForKey:NSLocaleCountryCode value:countryCode];
        countryName = [countryName stringByReplacingOccurrencesOfString:@"," withString:@" "];
        [sList setObject:countryName forKey:countryCode];
    }

    NSInteger __index = 0;
    NSInteger __idx = 0;
    NSMutableArray *mdata = [[NSMutableArray alloc] init];
    for (NSMutableDictionary *dic in _data) {
        NSString *countryCode = dic[@"code"];
        if (![[Utils nullToString:sList[countryCode]] isEqualToString:@""]) {
            dic[@"name"] = sList[countryCode];
        }
        [mdata addObject:dic];
    }
    
    NSSortDescriptor * descriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
    NSArray *stories = [mdata sortedArrayUsingDescriptors:@[descriptor]];
    _data = [stories copy];
    
    for (NSMutableDictionary *dic in _data) {
        NSString *countryCode = dic[@"code"];
        if (![[Utils nullToString:sList[countryCode]] isEqualToString:@""]) {
            dic[@"name"] = sList[countryCode];
        }
        
        if ([countryCode isEqualToString:self.selectCountryCode]) {
            __index = __idx;
        }
        __idx++;
    }
    
    return __index;
}

#pragma mark - Table view data source
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [tableView setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [tableView setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
    
    UIView *seperatorView = [[UIView alloc] initWithFrame:CGRectMake(0, cell.frame.size.height - 1, cell.frame.size.width, 1)];
    seperatorView.backgroundColor = [UIColor clearColor];// UIColorBasicBack;
    [cell addSubview:seperatorView];
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.data.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SearchCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SearchCell" forIndexPath:indexPath];
    
    [cell bindData:self.data[indexPath.item]];
    cell.backgroundColor = [UIColor clearColor];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSDictionary *selectedItems = [self.data objectAtIndex:indexPath.row];
    
    //
    NSLog(@"User selected %@", selectedItems);
    
    [self sendSelectionNotification:selectedItems];
}

# pragma mark - Search Results Updater

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    
    // filter the search results
    //NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains [cd] %@", self.controller.searchBar.text];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name contains [cd] %@", self.controller.searchBar.text];
    self.results = [self.data filteredArrayUsingPredicate:predicate];
    
    //_controller.searchBar.frame = CGRectMake(0, 0, 320, 44);
    
    //NSLog(@"Search Results are: %@", [self.results description]);
}


- (IBAction)searchButtonPressed:(id)sender {
    
    // present the search controller
    [self presentViewController:self.controller animated:YES completion:nil];
    
}

# pragma mark - Search Controller Delegate (optional)

- (void)didDismissSearchController:(UISearchController *)searchController {
    
    // called when the search controller has been dismissed
}

- (void)didPresentSearchController:(UISearchController *)searchController {
    
    // called when the serach controller has been presented
}

- (void)presentSearchController:(UISearchController *)searchController {
    
    // if you want to implement your own presentation for how the search controller is shown,
    // you can do that here
}

- (void)willDismissSearchController:(UISearchController *)searchController {
    
    // called just before the search controller is dismissed
}

- (void)willPresentSearchController:(UISearchController *)searchController {
    
    // called just before the search controller is presented
}

@end
