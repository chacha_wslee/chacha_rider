//
//  TXSignInVC.h

//
#import "TXUserModel.h"
#import "TXUserVC.h"
#import "REFormattedNumberField.h"
#import "RTLabel.h"

@interface TXSignInVC : TXUserVC <CLLocationManagerDelegate>

@property (strong, nonatomic) IBOutlet UIScrollView *myScrollView;
@property (strong, nonatomic) IBOutlet UIView *vbody;

@property (retain, nonatomic) IBOutlet UILabel      *lbTitle;
@property (retain, nonatomic) IBOutlet UIButton     *btnSignUp;


@property (retain, nonatomic) IBOutlet UIView       *vSignUp;
@property (retain, nonatomic) IBOutlet UIView       *vUsername;
@property (retain, nonatomic) IBOutlet UIView       *vUsernameL;
@property (retain, nonatomic) IBOutlet UIView       *vPhone;
@property (retain, nonatomic) IBOutlet UIView       *vEmail;
@property (retain, nonatomic) IBOutlet UIView       *vPasswd;
@property (retain, nonatomic) IBOutlet UIView       *vPasswdNew;
@property (retain, nonatomic) IBOutlet UIView       *vPasswdButton;
@property (retain, nonatomic) IBOutlet UIView       *vCode;

@property (retain, nonatomic) IBOutlet UIView       *vReset;
@property (retain, nonatomic) IBOutlet UIView       *vNew;

@property (retain, nonatomic) IBOutlet UIView       *vTerms0;
@property (retain, nonatomic) IBOutlet UIView       *vTerms1;
@property (retain, nonatomic) IBOutlet UIView       *vTerms2;
@property (retain, nonatomic) IBOutlet UIView       *vTerms3;
@property (retain, nonatomic) IBOutlet UIView       *vTerms4;

@property (retain, nonatomic) IBOutlet UITextField *txtUsername;
@property (retain, nonatomic) IBOutlet UITextField *txtUsernameL;
@property (nonatomic, strong) IBOutlet UITextField *txtEmail;
@property (nonatomic, strong) IBOutlet UIImageView    *imgCountry;
@property (nonatomic, strong) IBOutlet UILabel *txtCountry;
@property (retain, nonatomic) IBOutlet UIButton    *btnCountry;

//@property (nonatomic, strong) IBOutlet REFormattedNumberField *txtPhoneNum;
@property (retain, nonatomic) IBOutlet UITextField *txtPhoneNum;
@property (retain, nonatomic) IBOutlet UITextField *txtPassword;
@property (retain, nonatomic) IBOutlet UITextField *txtPasswordNew;
@property (retain, nonatomic) IBOutlet UIButton    *btnForgotPassword;
@property (retain, nonatomic) IBOutlet UIView      *vForgotPassword;

@property (nonatomic, strong) IBOutlet UITextField *txtVerifyCode;

@property (nonatomic, strong) IBOutlet UIButton    *btnGetCode;
@property (retain, nonatomic) IBOutlet UIView      *vVerifyCode;
@property (retain, nonatomic) IBOutlet UILabel      *lbFooter;

@property (nonatomic, strong) IBOutlet UIImageView    *imgTimer;
@property (retain, nonatomic) IBOutlet UILabel      *lbTimer;

//
@property (nonatomic, strong) IBOutlet UIButton    *btnGetResetCode;
@property (retain, nonatomic) IBOutlet UILabel      *lbResetFooter;

//
@property (nonatomic, strong) IBOutlet UIButton    *btnGetNewCode;
@property (retain, nonatomic) IBOutlet UILabel      *lbNewFooter1;
@property (retain, nonatomic) IBOutlet UILabel      *lbNewFooter2;

@property (nonatomic, strong) NSString *countryISOCode;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil mode:(NSInteger)mode;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil mode:(NSInteger)mode profile:(NSDictionary*)profile;
@property (nonatomic) NSInteger setupMode;
@property (nonatomic, strong) NSDictionary *snsProfile;


@end
