//
// Copyright (c) 2015 Related Code - http://relatedcode.com
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#import <UIKit/UIKit.h>

//-------------------------------------------------------------------------------------------------------------------------------------------------
NSString*		Date2String				(NSDate *date);
NSDate*			String2Date				(NSString *dateStr);
NSDate*			StringFormat2Date		(NSString *dateStr);
NSDate*         StringFormat2DateKorea  (NSString *dateStr);
NSString*		StringFormatDate2Date	(NSString *dateStr);
NSString*		StringFormatDate2Date2	(NSString *dateStr);
NSString*       StringFormatDate2DateHHMM   (NSString *dateStr);
NSString*       StringFormatDate2DateYYYYMM (NSString *dateStr);

NSString*       StringFormatCouponEndDate(NSString *dateStr);
NSString*       StringFormattedDate(NSString *format);

NSDate*         StringFormat2DateLA();
NSString* StringFormatDate2DestinationTime(NSDate *date);
//-------------------------------------------------------------------------------------------------------------------------------------------------
NSString*		TimeElapsed				(NSTimeInterval seconds);
