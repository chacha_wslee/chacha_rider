//
// Copyright (c) 2015 Related Code - http://relatedcode.com
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#import "converter.h"

//-------------------------------------------------------------------------------------------------------------------------------------------------
NSString* Date2String(NSDate *date)
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateFormat:DATE_FORMAT_UTC];
	[formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
	return [formatter stringFromDate:date];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
NSDate* String2Date(NSString *dateStr)
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateFormat:DATE_FORMAT_UTC];
	[formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
	return [formatter dateFromString:dateStr];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
NSDate* StringFormat2Date(NSString *dateStr)
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:DATE_FORMAT_YYYYMMDDHH24MISS];
    //[formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    return [formatter dateFromString:dateStr];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
NSDate* StringFormat2DateKorea(NSString *dateStr)
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:DATE_FORMAT_YYYYMMDDHH24MISS];
    //[formatter setTimeZone:[NSTimeZone timeZoneWithName:@"Asia/Seoul"]];
    //[formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:-8]];
    //[formatter setLocale:[NSLocale currentLocale]];
    
    NSDate *newtime = [[formatter dateFromString:dateStr] dateByAddingTimeInterval:(26*60*60)];
    
    //2016-09-12 12:50:56
//    NSDate *newtime = [formatter dateFromString:dateStr];
//    formatter = [[NSDateFormatter alloc] init];
//    [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:-8]];
    //[formatter stringFromDate:newtime];
    NSLog(@"time:%@",[formatter stringFromDate:newtime]);
    return newtime;
    //return [formatter dateFromString:dateStr];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
NSDate* StringFormat2DateLA()
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    NSDate *newtime = [NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:DATE_FORMAT_YYYYMMDDHH24MISS];
    [formatter setLocale:[[NSLocale alloc]initWithLocaleIdentifier:@"ko_KR"]];
    //[formatter setTimeZone:[NSTimeZone timeZoneWithName:@"America/Los_Angeles"]];
    //[formatter setTimeZone:[NSTimeZone timeZoneWithName:@"Asia/Seoul"]];
    NSLog(@"time:%@",[formatter stringFromDate:newtime]);
    return newtime;
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
NSString* StringFormatDate2Date(NSString *dateStr)
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:DATE_FORMAT_YYYYMMDD];
    NSDate *date = StringFormat2Date(dateStr);
    return [formatter stringFromDate:date];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
NSString* StringFormatCouponEndDate(NSString *dateStr)
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:DATE_FORMAT_COUPON];
    NSDate *date = StringFormat2Date(dateStr);
    return [formatter stringFromDate:date];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
NSString* StringFormatDate2Date2(NSString *dateStr)
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    //[formatter setDateFormat:@"h':'mm' 'a' 'MM'/'dd'/'yyyy"];
    [formatter setDateFormat:@"yyyy'/'MM'/'dd h':'mm' 'a'"];
    NSDate *date = StringFormat2Date(dateStr);
    return [formatter stringFromDate:date];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
NSString* StringFormatDate2DateYYYYMM(NSString *dateStr)
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    //[formatter setDateFormat:@"h':'mm' 'a' 'MM'/'dd'/'yyyy"];
    [formatter setDateFormat:DATE_FORMAT_YYYYMMDDDOT];
    NSDate *date = StringFormat2Date(dateStr);
    return [formatter stringFromDate:date];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
NSString* StringFormatDate2DateHHMM(NSString *dateStr)
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    //[formatter setDateFormat:@"h':'mm' 'a' 'MM'/'dd'/'yyyy"];
    [formatter setDateFormat:@"h':'mm' 'a'"];
    NSDate *date = StringFormat2Date(dateStr);
    return [formatter stringFromDate:date];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
NSString* StringFormatDate2DestinationTime(NSDate *date)
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    //[formatter setDateFormat:@"a' 'h':'mm'"];
    [formatter setDateFormat:@"HH':'mm"];
    [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"Asia/Seoul"]];
    //[formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    return [formatter stringFromDate:date];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
NSString* TimeElapsed(NSTimeInterval seconds)
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	NSString *elapsed;
	if (seconds < 60)
	{
		elapsed = @"Just now";
	}
	else if (seconds < 60 * 60)
	{
		int minutes = (int) (seconds / 60);
		elapsed = [NSString stringWithFormat:@"%d %@", minutes, (minutes > 1) ? @"mins" : @"min"];
	}
	else if (seconds < 24 * 60 * 60)
	{
		int hours = (int) (seconds / (60 * 60));
		elapsed = [NSString stringWithFormat:@"%d %@", hours, (hours > 1) ? @"hours" : @"hour"];
	}
	else
	{
		int days = (int) (seconds / (24 * 60 * 60));
		elapsed = [NSString stringWithFormat:@"%d %@", days, (days > 1) ? @"days" : @"day"];
	}
	return elapsed;
}
