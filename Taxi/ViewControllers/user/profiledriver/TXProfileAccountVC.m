//
//  TXProfileVC.m
//  Taxi
//

//

#import "TXProfileAccountVC.h"
#import "LGAlertView.h"
//#import "PECropViewController.h" //PECropViewControllerDelegate
#import "TOCropViewController.h"
//#import "EXPhotoViewer.h"
#import "TXVehicleVC.h"
#import "TXSignInVC.h"
#import "utilities.h"

#import "Validator.h"
#import "WToast.h"
//#import "CRToast.h"

#define MaxProcessCnt 5
#define NSProcessPct [NSString stringWithFormat:@"%d%@",(int)(processPct*100/MaxProcessCnt),LocalizedStr(@"Menu.BecomeADriver.Header.text2")]

@interface TXProfileAccountVC () <UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIActionSheetDelegate, TOCropViewControllerDelegate, UIViewControllerTransitioningDelegate, ValidatorDelegate>{
    int userId;
    NSArray *_msgList;
    UILabel *lbProcessInfo;
    int processPct;
    
    NSString *fname;
    NSString *lname;
    BOOL isProfile;
    BOOL isDriver;
    
    NSArray *dataArray;
    NSString *newPwd;
    
    id observer1;
    id observer2;
    id observer3;
    
    NSString *bankroute;
    NSString *bankaccount;
    NSString *taxpayer;
    
    NSInteger loadImgCnt;
    NSInteger maxLoadImgCnt;
}
@property (nonatomic, assign) TOCropViewCroppingStyle croppingStyle; //The cropping style
@property (strong, nonatomic) LGAlertView *securityAlertView;
@end

@implementation TXProfileAccountVC {
    NSMutableDictionary *propertyMap;
    int _alertIdx;
}


-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    
    if(self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        self->_alertIdx = 1;
        bankroute = @"";
        bankaccount = @"";
        taxpayer = @"";
    }
    
    return self;
}

-(void)viewDidLoad {
    [super viewDidLoad];
    
    [[IQKeyboardManager sharedManager] setEnable:NO];
    
    observer1 = [[NSNotificationCenter defaultCenter] addObserverForName:@"NotificationPhotoAlbum"
                                                                  object:nil
                                                                   queue:[NSOperationQueue mainQueue]
                                                              usingBlock:^(NSNotification *notification) {
                                                                  
                                                                  [self openPhotoAlbum];
                                                                  
                                                              }];
    observer2 = [[NSNotificationCenter defaultCenter] addObserverForName:@"NotificationShowCamera"
                                                                  object:nil
                                                                   queue:[NSOperationQueue mainQueue]
                                                              usingBlock:^(NSNotification *notification) {
                                                                  
                                                                  [self showCamera];
                                                                  
                                                              }];
//    observer3 = [[NSNotificationCenter defaultCenter] addObserverForName:@"NotificationEditPhoto"
//                                                                  object:nil
//                                                                   queue:[NSOperationQueue mainQueue]
//                                                              usingBlock:^(NSNotification *notification) {
//                                                                  
//                                                                  NSLog(@"notification:%@",notification);
//                                                                  [self editPhoto];
//                                                                  
//                                                              }];
    
    loadImgCnt = 0;
    maxLoadImgCnt = 0;
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    //[self removeEventListeners];

}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

#pragma mark - Configure

-(void)configureStyles {
    [super configureStyles];
    
    [super navigationType01X:[UIImage imageNamed:@"btn_back"] centerText:LocalizedStr(@"Menu.Driver.Profile.title")];
}

- (void)nextButtonOn:(BOOL)on
{
//    UIButton *btn = [super.view viewWithTag:1300];
//    btn.enabled = on;
//    if (on)
//        [btn setTitleColor:UIColorBasicText forState:UIControlStateNormal];
//    else
//        [btn setTitleColor:UIColorBasicTextOff forState:UIControlStateNormal];
}

-(void)configure {
    [super configure];
    [super configureAccount];
    [self configureConfig];
    
    UIView *n = [super navigationView];
    NSInteger _y = n.frame.origin.y + n.frame.size.height;
    
    self->userId = [[[[self->model getApp] getSettings] getUserId] intValue];
    
    UIButton *button = [[super navigationView] viewWithTag:9999];
    _imgProfile = [[super navigationView] viewWithTag:9998];
    [button addTarget:self action:@selector(onClick:) forControlEvents:UIControlEventTouchUpInside];
    
    newPwd = @"";
    
    _imgProfile.layer.cornerRadius = _imgProfile.frame.size.width / 2;
    _imgProfile.clipsToBounds = YES;
    _imgProfile.contentMode = UIViewContentModeScaleAspectFill;
    _imgProfile.layer.masksToBounds = YES;
    //_imgProfile.alpha = 0.7;
    
    if (appDelegate.isDriverMode) {
        _imgProfile.image = [UIImage imageNamed:@"ic_user_white"];
//        _imgProfile.image = appDelegate.imgProfileDriver;
//        if (appDelegate.imgProfileDriver == nil) {
//            [self saveProfileImage:[UIImage imageNamed:@"ic_user_white"] withCode:@"PP605"];
//        }
    }
    
    
    //------------------------------------------------------------------------------------------------------------------------
    propertyMap = [[NSMutableDictionary alloc] init];
    //------------------------------------------------------------------------------------------------------------------------
//    UIView *labelView = [Utils addDoubleLabelView:self.view underView:nil dim:0];
//    CGRect rect = labelView.frame;
//    rect.origin.y = kTopHeight + kNaviHeight;
//    labelView.frame = rect;
//    UILabel *label = [labelView viewWithTag:11];
//    label.text = LocalizedStr(@"Menu.BecomeADriver.Header.text1");
//    lbProcessInfo = [labelView viewWithTag:12];
    lbProcessInfo = [[super navigationView] viewWithTag:1111];
    lbProcessInfo.text = LocalizedStr(@"Menu.Driver.Profile.Header.text2");
    //[self.view addSubview:labelView];
    //------------------------------------------------------------------------------------------------------------------------
    ((UILabel *)[self.vbody viewWithTag:11011]).text = LocalizedStr(@"Menu.BecomeADriver.List.text1");

    ((UILabel *)[self.vbody viewWithTag:11211]).text = LocalizedStr(@"Menu.BecomeADriver.List.text3");
    ((UILabel *)[self.vbody viewWithTag:11311]).text = LocalizedStr(@"Menu.BecomeADriver.List.text4");

    ((UILabel *)[self.vbody viewWithTag:11711]).text = LocalizedStr(@"Menu.BecomeADriver.List.text8");
    ((UILabel *)[self.vbody viewWithTag:12511]).text = LocalizedStr(@"Menu.BecomeADriver.List.text16");

    
    //((UILabel *)[self.vbody viewWithTag:13011]).text = LocalizedStr(@"Menu.BecomeADriver.footer.text");
    //((UILabel *)[self.vbody viewWithTag:13011]).textColor = UIColorDefault;
    
    //------------------------------------------------------------------------------------------------------------------------
    self.myScrollView.backgroundColor = [UIColor whiteColor];
    self.vbody.backgroundColor = UIColorLabelBG;
    self.vbody.frame = CGRectMake(self.vbody.frame.origin.x, self.vbody.frame.origin.y, self.view.frame.size.width, self.vbody.frame.size.height);
    
    self.myScrollView.frame = CGRectMake(0,
                                         _y,
                                         self.view.frame.size.width,
                                         self.view.frame.size.height - _y);
    
    self.myScrollView.contentSize = CGSizeMake(self.vbody.frame.size.width, self.vbody.frame.size.height);
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0, 0.0, 0, 0.0);
    self.myScrollView.contentInset = contentInsets;
    self.myScrollView.scrollIndicatorInsets = contentInsets;
    self.myScrollView.scrollsToTop = YES;
    [self.myScrollView addSubview:self.vbody];
    //------------------------------------------------------------------------------------------------------------------------
    // 2. init event
    _msgList = @[
                    @[@"PP310",@""], // modify
                    
//                    @[@"PP311",@""], // modify
//                    @[@"PP312",@""], // modify
//                    @[@"PP313",@""], // modify

                    @[@"PP602",@""], // image loading
                    @[@"PP605",@""], // image loading
//                    @[@"PP603",@""], // image loading
                    
                    @[@"PP628",@""], // image save
//                    @[@"PP629",@""], // image save
                    
                    @[@"PP210",@""], // loading
                    @[@"PP300",@""], // loading
                    
//                    @[@"PP301",@""], // loading
//                    @[@"PP302",@""], // loading
//                    @[@"PP303",@""], // loading
                    ];
    
    [self registerEventListeners];
    
    if (appDelegate.isDriverMode) {
        [self configureDriver];
    }
}

-(void) configureConfig {

    
}

-(void) configureDriver {
    // 1. init var
    
//    NSDictionary *user = [appDelegate.dicDriver objectForKey:@"provider"];
    NSString *pseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.PSEQ];
    
//    REFormattedNumberField *txtNum = [[REFormattedNumberField alloc] initWithFrame:CGRectZero];
//    txtNum.format = @"+X (XXX) XXX-XXXX";
//    txtNum.text = user[@"utelno"];
    
//    ((UILabel *)[self.vbody viewWithTag:11212]).text = user[@"uid"];
//    ((UILabel *)[self.vbody viewWithTag:11312]).text = user[@"utelno"];//txtNum.text;
    
    appDelegate.becomePseq = pseq;
    // 4. settup
    // 조회
    [self onPP300:pseq];
}

-(void) registerEventListeners {
    for (NSArray *msg in _msgList) {
        [self->model addEventListener:self forEvent:[msg objectAtIndex:0] eventParams:nil];
    }
}

-(void) removeEventListeners {
    for (NSArray *msg in _msgList) {
        [self->model removeEventListener:self forEvent:[msg objectAtIndex:0]];
    }
    
    [[NSNotificationCenter defaultCenter] removeObserver:observer1];
    observer1 = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:observer2];
    observer2 = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:observer3];
    observer3 = nil;
    
//    [[NSNotificationCenter defaultCenter] removeObserver:self
//                                                    name:@"NotificationPhotoAlbum"
//                                                  object:nil];
//    [[NSNotificationCenter defaultCenter] removeObserver:self
//                                                    name:@"NotificationShowCamera"
//                                                  object:nil];
//    [[NSNotificationCenter defaultCenter] removeObserver:self
//                                                    name:@"NotificationEditPhoto"
//                                                  object:nil];
}

-(void) checkProcess:(NSString*)state {
    processPct = (int)[propertyMap count];
    if((int)(processPct*100/MaxProcessCnt) != 100) {
        lbProcessInfo.text = NSProcessPct;
    }
    else {
        if ([state isEqualToString:@"1"]) {
            lbProcessInfo.text = NSProcessPct;
        }
        else if ([state isEqualToString:@""]) {
        }
        else {
            lbProcessInfo.text = LocalizedStr(@"Menu.Driver.Profile.Header.text2");
        }
    }
    
    //if (processPct == MaxProcessCnt && _btnAgree.tag == 1) {
    if (processPct == MaxProcessCnt) {
        [self nextButtonOn:YES];
    }
    else {
        [self nextButtonOn:NO];
    }
    
}

-(void)updateTrimString:(NSString*)str key:(NSString*)key
{
    NSString *trimmedString = [Utils trimStringOnly:str];
    
    if ([trimmedString isEqualToString:@""]) {
        [propertyMap removeObjectForKey:key];
    }
    else {
        [propertyMap setValue:str forKey:key];
    }
}

-(void)updateLabel:(NSInteger)tag flag:(BOOL)flag
{
    if (flag) {
        ((UILabel *)[self.vbody viewWithTag:tag]).textColor = [UIColor blackColor];
    }
    else {
        ((UILabel *)[self.vbody viewWithTag:tag]).textColor = UIColorRedColor;
        lbProcessInfo.textColor = UIColorRedColor;
    }
}

#pragma mark - Action
-(IBAction)onNaviButtonClick:(id)sender {
    UIButton *btn = (UIButton*)sender;
    NSLog(@"onNaviButtonClick");
    //[super onNaviButtonClick:sender];
    
    if (btn.tag == 1100 || btn.tag == 1101) {
        [self removeEventListeners];
        [self.navigationController popViewControllerAnimated:YES];
    }
}


#pragma mark - IBAction
-(IBAction)onPhoto:(id)sender {
    
    self->_alertIdx = (int)((UIButton*)sender).tag;
    NSLog(@"_alertIdx:%d",self->_alertIdx);
    NSArray *items = @[LocalizedStr(@"Menu.Driver.Profile.Image.album")];
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        items = @[LocalizedStr(@"Menu.Driver.Profile.Image.album"), LocalizedStr(@"Menu.Driver.Profile.Image.camera"), LocalizedStr(@"Menu.Driver.Profile.Image.edit")];
    }
    
    LGAlertView *alertView = [[LGAlertView alloc] initWithTitle:nil
                                message:LocalizedStr(@"Menu.Driver.Profile.Image.title2")
                                  style:LGAlertViewStyleActionSheet
                           buttonTitles:items
                      cancelButtonTitle:LocalizedStr(@"Button.CANCEL")
                 destructiveButtonTitle:nil
                          actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                              NSLog(@"actionHandler, %@, %lu", title, (long unsigned)index);
                              
                              if (index == 0) {
                                  [self openPhotoAlbum];
                              }
                              else if (index == 1) {
                                  [self showCamera];
                              }
                              else if (index == 2) {
                                  UIImageView *imageView;
                                  
                                  if (self->_alertIdx == 9999) {
                                      imageView = (UIImageView *)[[super navigationView] viewWithTag:9998];
                                  }
                                  
                                  if (imageView.image) {
                                      TOCropViewController *cropController = [[TOCropViewController alloc] initWithCroppingStyle:self.croppingStyle image:imageView.image];
                                      cropController.delegate = self;
                                      
                                      
                                      [self presentViewController:cropController animated:YES completion:nil];
                                  }
                              }
                          }
                          cancelHandler:^(LGAlertView *alertView) {
                              NSLog(@"cancelHandler");
                          }
                     destructiveHandler:^(LGAlertView *alertView) {
                         NSLog(@"destructiveHandler");
                     }];
    [Utils initAlertButtonColor:alertView];
    [alertView showAnimated:YES completionHandler:nil];
    
}

-(IBAction)onClickAgree:(id)sender {
    
    UIButton *btn = (UIButton*)sender;
    if (btn.tag == 1) {
        [btn setImage:[UIImage imageNamed:@"form_radio_normal"] forState:UIControlStateNormal];
        btn.tag = 0;
    }
    else {
        [btn setImage:[UIImage imageNamed:@"form_radio_chk"] forState:UIControlStateNormal];
        btn.tag = 1;
    }
}

-(IBAction)onClick:(id)sender {
    
    UIButton *btn = (UIButton*)sender;

    if (btn.tag == 12313 || btn.tag == 12413 || btn.tag == 12712 || btn.tag == 12713
        || btn.tag == 11013 || btn.tag == 11113 || btn.tag == 11213 || btn.tag == 11313 || btn.tag == 12513
        || btn.tag == 11713 || btn.tag == 11813
        ) {
        [self onClickNext:sender];
        return;
    }
    
    LGAlertView *alertView = [[LGAlertView alloc] initWithTitle:@""
                                                        message:LocalizedStr(@"Menu.Driver.Profile.Change.text")
                                                          style:LGAlertViewStyleAlert
                                                   buttonTitles:@[LocalizedStr(@"Button.MODIFY")]
                                              cancelButtonTitle:LocalizedStr(@"Button.CANCEL")
                                         destructiveButtonTitle:nil
                                                  actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                                                      [self onClickNext:sender];
                                                  }
                                                  cancelHandler:^(LGAlertView *alertView) {
                                                      NSLog(@"cancelHandler");
                                                  }
                                             destructiveHandler:^(LGAlertView *alertView) {
                                                 NSLog(@"destructiveHandler");
                                             }];
    [Utils initAlertButtonColor:alertView];
    [alertView showAnimated:YES completionHandler:^(void)
     {
     }];
    
}

-(IBAction)onClickNext:(id)sender {
    
    UIButton *btn = (UIButton*)sender;
    newPwd = @"";
    
    if (btn.tag == 9999) {
        self->_alertIdx = (int)btn.tag;
        if ([propertyMap valueForKey:@"picture"]) {
            [self onPhoto:sender];
        }
        else {
            [self onPhoto:btn];
        }
        return;
    }

    
    NSInteger tag = (btn.tag / 100) % 100 + 1 - 10;
    
    NSLog(@"tag:%ld",(long)tag);
    if (tag == 1 || tag == 2 || tag == 3 || tag == 4 || tag == 5 || tag == 6 || tag == 7 || tag == 8 || tag == 9) {
        TOAST_MSG(@"Menu.Account.List.none.edit");
    }
    else if (tag == 2) {}
    else if (tag == 3) {}
    else if (tag == 4) {
        TOAST_MSG(@"Menu.Account.List.none.edit");
    }
    //------------------------------------------------------------------------------------------------
    else if (tag == 5) {
    }
    else if (tag == 6) {  }
    else if (tag == 7) {  }
    else if (tag == 8) {  }
    else if (tag == 9) {  }
    else if (tag == 16) {
        //[[UIApplication sharedApplication] openURL: [NSURL URLWithString:TERMS_URL]];
        _securityAlertView = [[LGAlertView alloc] initWithTextFieldsAndTitle:LocalizedStr(@"Menu.Account.List2.text4")
                                                                     message:nil
                                                          numberOfTextFields:3
                                                      textFieldsSetupHandler:^(UITextField *textField, NSUInteger index)
                              {
                                  if (index == 0){
                                      textField.placeholder = LocalizedStr(@"Menu.Account.List2.text4.input.placeholder1");
                                      textField.secureTextEntry = YES;
                                  }
                                  else if (index == 1)
                                  {
                                      textField.placeholder = LocalizedStr(@"Menu.Account.List2.text4.input.placeholder2");
                                      textField.secureTextEntry = YES;
                                  }
                                  else if (index == 2)
                                  {
                                      textField.placeholder = LocalizedStr(@"Menu.Account.List2.text4.input.placeholder3");
                                      textField.secureTextEntry = YES;
                                  }
                                  
                                  textField.tag = index;
                                  textField.delegate = self;
                                  textField.enablesReturnKeyAutomatically = YES;
                                  textField.autocapitalizationType = NO;
                                  textField.autocorrectionType = NO;
                                  textField.clearButtonMode = UITextFieldViewModeNever;
                                  textField.textAlignment = NSTextAlignmentCenter;
                              }
                                                                buttonTitles:@[LocalizedStr(@"Button.OK")]
                                                           cancelButtonTitle:LocalizedStr(@"Button.CANCEL")
                                                      destructiveButtonTitle:nil
                                                               actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                                                                   //UITextField *secondTextField = _securityAlertView.textFieldsArray[index];
                                                                   //NSLog(@"actionHandler, %@, %lu", secondTextField.text, (long unsigned)index);
                                                                   //                                                                   secondTextField = _securityAlertView.textFieldsArray[0];
                                                                   //                                                                   NSLog(@"actionHandler, %@, %lu", secondTextField.text, (long unsigned)index);
                                                                   //                                                                   secondTextField = _securityAlertView.textFieldsArray[1];
                                                                   //                                                                   NSLog(@"actionHandler, %@, %lu", secondTextField.text, (long unsigned)index);
                                                                   //                                                                   secondTextField = _securityAlertView.textFieldsArray[2];
                                                                   //                                                                   NSLog(@"actionHandler, %@, %lu", secondTextField.text, (long unsigned)index);
                                                                   
                                                                   
                                                                   UITextField *textField1 = _securityAlertView.textFieldsArray[0];
                                                                   UITextField *textField2 = _securityAlertView.textFieldsArray[1];
                                                                   UITextField *textField3 = _securityAlertView.textFieldsArray[2];
                                                                   
                                                                   NSString *tf1 = textField1.text;
                                                                   NSString *tf2 = textField2.text;
                                                                   NSString *tf3 = textField3.text;
                                                                   
                                                                   [self resignKeyboard];
                                                                   
                                                                   Validator *validator = [[Validator alloc] init];
                                                                   validator.delegate   = self;
                                                                   
                                                                   // 비밀번호
                                                                   [validator putRule:[Rules minLength:6 withFailureString:LocalizedStr(@"Validator.Passwd.text") forTextField:textField1]];
                                                                   [validator putRule:[Rules minLength:6 withFailureString:LocalizedStr(@"Validator.Passwd.text") forTextField:textField2]];
                                                                   [validator putRule:[Rules minLength:6 withFailureString:LocalizedStr(@"Validator.Passwd.text") forTextField:textField3]];
                                                                   
                                                                   if (![validator validateForResult]) {
                                                                       [alertView.firstButton setSelected:NO];
                                                                       return;
                                                                   }
                                                                   
                                                                   propertyMap[@"user"][@"passwd"] = tf1;
                                                                   
                                                                   NSLog(@"actionHandler1, %@,", tf1);
                                                                   NSLog(@"actionHandler2, %@,", tf2);
                                                                   NSLog(@"actionHandler3, %@,", tf3);
                                                                   
                                                                   if (![tf2 isEqualToString:tf3]) {
                                                                       [Utils onErrorAlert:LocalizedStr(@"Menu.Account.List2.text4.input.result3")];
                                                                       [alertView.firstButton setSelected:NO];
                                                                       return;
                                                                       
                                                                   }
                                                                   else {
                                                                       NSString *pwd = [[[TXApp instance] getSettings] getPassword];
                                                                       NSString *oldpwd = [Utils passcodeBysha256:tf1];
                                                                       if ([oldpwd isEqualToString:pwd]){
                                                                           newPwd = [Utils passcodeBysha256:tf3];
                                                                           //[self onUR210:@{@"pwd":[Utils passcodeBysha256:textField1.text],@"newpwd":[Utils passcodeBysha256:textField3.text]}];
                                                                           [self onPP210:@{@"pwd":newPwd}];
                                                                       }
                                                                       else {
                                                                           [Utils onErrorAlert:LocalizedStr(@"Menu.Account.List2.text4.input.result1")];
                                                                           [alertView.firstButton setSelected:NO];
                                                                           return;
                                                                           
                                                                       }
                                                                   }
                                                                   //propertyMap[@"user"][@"newpasswd"] = textField3.text;
                                                                   
                                                                   [alertView dismissAnimated:YES completionHandler:nil];
                                                                   
                                                               }
                                                               cancelHandler:^(LGAlertView *alertView) {
                                                                   [alertView dismissAnimated:YES completionHandler:nil];
                                                               }
                                                          destructiveHandler:^(LGAlertView *alertView) {
                                                              [alertView dismissAnimated:YES completionHandler:nil];
                                                          }];
        [Utils initAlertButtonColor:_securityAlertView];
        [_securityAlertView setButtonAtIndex:0 enabled:NO];
        [_securityAlertView showAnimated:YES completionHandler:nil];
        [_securityAlertView setDismissOnAction:NO];
    }
    else {
        self->_alertIdx = 1;
        [self popupInput:(int)tag];
    }
}

-(void)popupInput:(int)tag
{
    NSString *__title = [NSString stringWithFormat:@"Menu.BecomeADriver.List.text%d",tag];
    __title = LocalizedStr(__title);
    
    NSString *__subtitle = nil;
    if (tag == 1 || tag == 2) {
        __subtitle = [NSString stringWithFormat:@"Menu.BecomeADriver.List.text%d.input",tag];
        __subtitle = LocalizedStr(__subtitle);
    }
    
    LGAlertView *alertView = [[LGAlertView alloc] initWithTextFieldsAndTitle:__title
                                                                     message:__subtitle
                                                          numberOfTextFields:1
                                                      textFieldsSetupHandler:^(UITextField *textField, NSUInteger index)
                              {
                                  if (index == 0) {
                                      textField.placeholder = @"";
                                      textField.text = @"";
                                  }
                                  
                                  textField.tag = index;
                                  //textField.delegate = self;
                                  textField.enablesReturnKeyAutomatically = YES;
                                  textField.autocapitalizationType = NO;
                                  textField.autocorrectionType = NO;
                                  textField.clearButtonMode = UITextFieldViewModeNever;
                                  textField.textAlignment = NSTextAlignmentCenter;
                                  
                                  if (tag == 5 || tag == 13 || tag == 14 || tag == 15 ) {
                                      [textField setKeyboardType:UIKeyboardTypeNumberPad];
                                  }
                              }
                                                                buttonTitles:@[LocalizedStr(@"Button.OK")]
                                                           cancelButtonTitle:LocalizedStr(@"Button.CANCEL")
                                                      destructiveButtonTitle:nil
                                                               actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                                                                   UITextField *secondTextField = alertView.textFieldsArray[index];
                                                                   //promoCode = secondTextField.text;
                                                                   
                                                                   NSLog(@"actionHandler, %@, %lu, %@", title, (long unsigned)index, secondTextField.text);
                                                                   
//                                                                   int __tag = tag * 100 + 1000 + 12 + 10000 - 100;
                                                                   
//                                                                   ((UILabel *)[self.vbody viewWithTag:__tag]).text = secondTextField.text;
                                                                   
                                                                   [self resignKeyboard];
                                                                   
                                                                   Validator *validator = [[Validator alloc] init];
                                                                   validator.delegate   = self;
                                                                   
                                                                   if (tag == 1) {
                                                                       //[self updateTrimString:secondTextField.text key:@"fname"];
                                                                   }
                                                                   else if (tag == 2) {
                                                                       //[self updateTrimString:secondTextField.text key:@"lname"];
                                                                   }
                                                                   else if (tag == 8) {
                                                                       
                                                                       //
                                                                       [validator putRule:[Rules minLength:4 withFailureString:LocalizedStr(@"Validator.LicenseNo.text") forTextField:secondTextField]];
                                                                       //[validator putRule:[Rules checkIfAlphaNumericWithFailureString:LocalizedStr(@"Validator.LicenseNo.text") forTextField:secondTextField]];
                                                                       
                                                                       if (![validator validateForResult]) {
                                                                           [alertView.firstButton setSelected:NO];
                                                                           return;
                                                                       }
                                                                       
                                                                       [self updateTrimString:secondTextField.text key:@"licenseno"];
                                                                       
                                                                       [self onPP310:@{@"licenseno":secondTextField.text}];
                                                                   }
                                                                   else if (tag == 14) {
                                                                       
                                                                       //
                                                                       [validator putRule:[Rules checkRange:NSMakeRange(9, 9) withFailureString:LocalizedStr(@"Validator.BankRoute.text") forTextField:secondTextField]];
                                                                       [validator putRule:[Rules checkIfNumericWithFailureString:LocalizedStr(@"Validator.BankRoute.text") forTextField:secondTextField]];
                                                                       
                                                                       if (![validator validateForResult]) {
                                                                           [alertView.firstButton setSelected:NO];
                                                                           return;
                                                                       }
                                                                       
                                                                       //[self updateTrimString:secondTextField.text key:@"bankroute"];
                                                                       bankroute = [Utils trimStringOnly:secondTextField.text];
                                                                       [self onPP310:@{@"bankroute":bankroute}];
                                                                   }
                                                                   else if (tag == 15) {
                                                                       
                                                                       //
                                                                       [validator putRule:[Rules checkRange:NSMakeRange(8, 14) withFailureString:LocalizedStr(@"Validator.BankAccount.text") forTextField:secondTextField]];
                                                                       [validator putRule:[Rules checkIfNumericWithFailureString:LocalizedStr(@"Validator.BankAccount.text") forTextField:secondTextField]];
                                                                       
                                                                       if (![validator validateForResult]) {
                                                                           [alertView.firstButton setSelected:NO];
                                                                           return;
                                                                       }
                                                                       
                                                                       //[self updateTrimString:secondTextField.text key:@"bankaccount"];
                                                                       bankaccount = [Utils trimStringOnly:secondTextField.text];
                                                                       [self onPP310:@{@"bankaccount":bankaccount}];
                                                                   }
                                                                   
                                                                   //((UILabel *)[self.vbody viewWithTag:__tag]).text = secondTextField.text;
                                                                   //[self checkProcess:];
                                                                   
                                                                   [alertView dismissAnimated:YES completionHandler:nil];
                                                               }
                                                               cancelHandler:^(LGAlertView *alertView) {
                                                                   [alertView dismissAnimated:YES completionHandler:nil];
                                                                   
                                                               }
                                                          destructiveHandler:^(LGAlertView *alertView) {
                                                              [alertView dismissAnimated:YES completionHandler:nil];
                                                          }];
    //[alertView setButtonAtIndex:0 enabled:NO];
    [Utils initAlertButtonColor:alertView];
    [alertView showAnimated:YES completionHandler:nil];
    [alertView setDismissOnAction:NO];
    
}

#pragma mark - Send Event
-(void)onPP210:(NSDictionary*)_propertyMap {
    
    NSString* useq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.USEQD];
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:useq, @"useq", nil];
    
    NSDictionary *user = [appDelegate.dicRider objectForKey:@"user"];
    dic[@"uid"] = user[@"uid"];
    //dic[@"utelno"] = user[@"utelno"];
    [dic addEntriesFromDictionary:_propertyMap];
    
    [self showBusyIndicator:@"Updating info ... "];
    [self->model requsetAPI:API_FUNC(NSStringFromSelector(_cmd)) property:dic];
}

-(void)onPP300:(NSString*)pseq {
    
    NSDictionary *_propertyMap = @{
                                   @"pseq" : pseq
                                   };
    
    [self showBusyIndicator:@"Loading info ... "];
    [self->model requsetAPI:API_FUNC(NSStringFromSelector(_cmd)) property:_propertyMap];
}
-(void)onPP602:(NSString*)pseq picture:(NSString*)picture {
    
    if ([picture intValue] > 0) {
        maxLoadImgCnt++;
        NSDictionary *_propertyMap = @{
                                       @"pseq" : pseq,
                                       @"name" : @"picture",
                                       @"picture" : picture
                                       };
        
        [self showBusyIndicator:@"Loading ... "];
        [self->model requsetAPI:@"PP605" property:_propertyMap];
    }
}
-(void)onPP302:(NSString*)pseq picture:(NSString*)picture {
    
    if ([picture intValue] > 0) {
        maxLoadImgCnt++;
        NSDictionary *_propertyMap = @{
                                       @"pseq" : pseq,
                                       @"name" : @"driver",
                                       @"driver" : picture
                                       };
        
        [self showBusyIndicator:@"Loading ... "];
        [self->model requsetAPI:@"PP605" property:_propertyMap];
    }
}
-(void)onPP303:(NSString*)pseq picture:(NSString*)picture {
    
    if ([picture intValue] > 0) {
        maxLoadImgCnt++;
        NSDictionary *_propertyMap = @{
                                       @"pseq" : pseq,
                                       @"name" : @"taxpayer",
                                       @"taxpayer" : picture
                                       };
        
        [self showBusyIndicator:@"Loading ... "];
        [self->model requsetAPI:@"PP605" property:_propertyMap];
    }
}


-(void)onPP310:(NSDictionary*)_propertyMap {
    
//    TXSettings  *settings  = [[TXApp instance] getSettings];
//    NSString *userPwd = [settings getPassword];
//    
//    NSDictionary *_propertyMap = @{
//                                   @"pseq":appDelegate.becomePseq,
//                                   @"pwd":userPwd,
//                                   @"ssn":propertyMap[@"ssn"],
//                                   @"address":propertyMap[@"address"],
//                                   @"city":propertyMap[@"city"],
//                                   @"states":propertyMap[@"states"],
//                                   @"zipcode":propertyMap[@"zipcode"],
//                                   @"licenseno":propertyMap[@"licenseno"],
//                                   @"licenseexpire":propertyMap[@"licenseexpire"],
//                                   @"bankroute":propertyMap[@"bankroute"],
//                                   @"bankaccount":propertyMap[@"bankaccount"]
//                                   };
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:appDelegate.becomePseq, @"pseq", nil];
    
    [dic addEntriesFromDictionary:_propertyMap];
    
    [self showBusyIndicator:@"Updating info ... "];
    [self->model requsetAPI:API_FUNC(NSStringFromSelector(_cmd)) property:dic];
}

-(void)onPP311:(UIImage*)image {
    
    NSString* pseq = appDelegate.becomePseq;
    
    NSString* picture = [Utils encodeToBase64String:image];
    
    NSDictionary *_propertyMap = @{
                                   @"pseq" : pseq,
                                   @"name" : @"picture",
                                   @"picture" : picture,
                                   };
    
    [self showBusyIndicator:@"Update Picture ... "];
    [self->model requsetAPI:@"PP628" property:_propertyMap];
}

-(void)onPP312:(UIImage*)image {
    
    NSString* pseq = appDelegate.becomePseq;
    
    NSString* picture = [Utils encodeToBase64String:image];
    
    NSDictionary *_propertyMap = @{
                                   @"pseq" : pseq,
                                   @"name" : @"driver",
                                   @"driver" : picture,
                                   };
    
    [self showBusyIndicator:@"Update Driver ... "];
    [self->model requsetAPI:@"PP628" property:_propertyMap];
}
-(void)onPP313:(UIImage*)image {
    
    NSString* pseq = appDelegate.becomePseq;
    
    NSString* picture = [Utils encodeToBase64String:image];
    
    NSDictionary *_propertyMap = @{
                                   @"pseq" : pseq,
                                   @"name" : @"taxpayer",
                                   @"taxpayer" : picture,
                                   };
    
    [self showBusyIndicator:@"Update Driver ... "];
    [self->model requsetAPI:@"PP628" property:_propertyMap];
}

-(void)shadowProfileImage:(UIImage*)image {
    _imgProfile.image = image;
    _imgProfile.alpha = 1.0;
}

-(void)saveProfileImage:(UIImage*)image withCode:(NSString*)code{
    
    [self shadowProfileImage:image];
}

#pragma mark - onEvent
-(void)onEvent:(TXEvent *)event eventParams:(id)subscriptionParams {
    
    TXResponseDescriptor * descriptor = [event getEventProperty:TXEvents.Params.DESCRIPTOR];
    TXRequestObj *request = [event getEventProperty:TXEvents.Params.REQUEST];
    
    if([event.name isEqualToString:@"PP300"]) {
        
        if(descriptor.success == true) {
            NSString     *response = [[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding];
            NSDictionary *result   = getJSONObj(response);
            
            
            NSDictionary *profile              = [result valueForKey:@"provider_profile"];
            
//            if (![Utils isDictionary:profile]) {
//                NSDictionary *provider              = [appDelegate.dicDriver valueForKey:@"provider"];
//                
//                profile = @{@"pseq":provider[@"pseq"],@"name":provider[@"name"],
//                            @"uid":provider[@"uid"],
//                            @"utelno":provider[@"utelno"],
//                            @"picture":provider[@"picture"],
//                            @"driver":@"0"};
//            }
            
            propertyMap[@"fname"]   = profile[@"name"];
            propertyMap[@"uid"]     = [Utils nullToString:profile[@"uid"]];
            propertyMap[@"utelno"]  = [Utils nullToString:profile[@"utelno"]];
            propertyMap[@"picture"] = [Utils nullToString:profile[@"picture"]];
            propertyMap[@"licenseno"] = [Utils nullToString:profile[@"licenseno"]];
            
            ((UILabel *)[self.vbody viewWithTag:11012]).text = propertyMap[@"fname"];
            ((UILabel *)[self.vbody viewWithTag:11212]).text = propertyMap[@"uid"];
            ((UILabel *)[self.vbody viewWithTag:11312]).text = propertyMap[@"utelno"];
            
            ((UILabel *)[self.vbody viewWithTag:12512]).text = @"";
            
            ((UILabel *)[self.vbody viewWithTag:11712]).text = propertyMap[@"licenseno"];
            
            [self onPP602:profile[@"pseq"] picture:profile[@"picture"]];
//            [self updateLabel:11511 flag:[profile[@"picture_flag"] boolValue]];

            for(NSString *key in [propertyMap allKeys]) {
                if ([propertyMap[key] isEqualToString:@""]) {
                    [propertyMap removeObjectForKey:key];
                }
            }
            [self checkProcess:profile[@"state"]];
        }
        else {
            [self alertError:@"Error" message:descriptor.error];
        }
    }
    else if([event.name isEqualToString:@"PP602"] || [event.name isEqualToString:@"PP605"]) {
        
        NSDictionary *param;
//        if (request.body && ([request.body isKindOfClass:[NSData class]] && ((NSData *)request.body).length > 0)) {
//            param = [Utils getDataOfQueryString:request.body];
//        }
        if (![Utils isDictionary:param]) {
            param = [Utils getDataOfQueryString:request.reqUrl];
        }
        
        if(descriptor.success == true) {
            NSString     *response = [[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding];
            NSDictionary *result   = getJSONObj(response);
            
            NSString *pictureData              = [result valueForKey:@"picture"];
            
            if ([pictureData length]) {
                
                UIImage *image = [Utils decodeBase64ToImage:pictureData];
                // Store image
                //[appDelegate.cache storeObject:event forKey:request.reqUrl];
                
                if ([param[@"name"] isEqualToString:@"picture"]) {
                    UIButton *btn = (UIButton *)[self.vbody viewWithTag:11512];
                    [btn setImage:nil forState:UIControlStateNormal];
                    UIImageView *imageView = (UIImageView *)[self.vbody viewWithTag:11515];
                    imageView.image = image;
                    [propertyMap setObject:image forKey:@"picture"];
                    
                    _imgProfile.alpha = 1.0;
                    [self saveProfileImage:image withCode:event.name];
                }
            }
            
            [self checkProcess:@""];
        }
        else {
            [self alertError:@"Error" message:descriptor.error];
        }
        loadImgCnt++;
        if (loadImgCnt >= maxLoadImgCnt) {
            [self hideBusyIndicator];
        }
    }
    else if([event.name isEqualToString:@"PP320"]) {
        
        if(descriptor.success == true) {
            NSString     *response = [[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding];
            NSDictionary *result   = getJSONObj(response);
            
            
            NSDictionary *profile              = [result valueForKey:@"provider_profile"];
            NSString *pseq = profile[@"pseq"];
            
            if ([pseq length]) {
                // UI refresh
                
            }
        }
        else {
            [self alertError:@"Error" message:descriptor.error];
        }
        [self hideBusyIndicator];
    }
    else if([event.name isEqualToString:@"PP628"]) {
        
        NSDictionary *param;
//        if (request.body && ([request.body isKindOfClass:[NSData class]] && ((NSData *)request.body).length > 0)) {
//            param = [Utils getDataOfQueryString:request.body];
//        }
        if (![Utils isDictionary:param]) {
            param = [Utils getDataOfQueryString:request.reqUrl];
        }
        
        if(descriptor.success == true) {
            NSString     *response = [[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding];
            NSDictionary *result   = getJSONObj(response);
            
            NSString *pictureData              = [result valueForKey:@"picture"];
            
            if ([pictureData length]) {
                
                UIImage *image = [Utils decodeBase64ToImage:pictureData];
                // Store image
                //[appDelegate.cache storeObject:event forKey:request.reqUrl];
                
                if ([param[@"name"] isEqualToString:@"picture"]) {
                    _imgProfile.image = image;
                    
//                    UIButton *btn = (UIButton *)[self.vbody viewWithTag:11512];
//                    [btn setImage:nil forState:UIControlStateNormal];
//                    UIImageView *imageView = (UIImageView *)[self.vbody viewWithTag:11515];
//                    imageView.image = image;
                    [propertyMap setObject:image forKey:@"picture"];
                }
            }
            
            // 죄회요청
            //            [self checkProcess:@""];
            // 조회
            //[self onPP300:appDelegate.becomePseq];
        }
        else {
            [self alertError:@"Error" message:descriptor.error];
        }
        [self hideBusyIndicator];
    }
    else if([event.name isEqualToString:@"PP310"]) {
        
        [self hideBusyIndicator];
        if(descriptor.success == true) {
            //NSString     *response = [[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding];
            //NSDictionary *result   = getJSONObj(request.body);
            
            // 죄회요청
            //[self checkProcess:@""];
            // 조회
            [self onPP300:appDelegate.becomePseq];
        }
        else {
            [self alertError:@"Error" message:descriptor.error];
        }
    }
    else if([event.name isEqualToString:@"PP210"]) {
//        NSString     *response = [[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding];
//        NSDictionary *result   = getJSONObj(response);
        
        if(descriptor.success == true) {
            
//            if (appDelegate.isDriverMode) {
//                [self configureDriver:[result valueForKey:@"user"]];
//                // 1. 정보 업데이트
//                NSMutableDictionary *dic = [appDelegate.dicDriver mutableCopy];
//                [dic setObject:[result valueForKey:@"provider"] forKey:@"provider"];
//                appDelegate.dicDriver = dic;
//            }
//            else {
//                [self configureRider:[result valueForKey:@"user"]];
//                // 1. 정보 업데이트
//                NSMutableDictionary *dic = [appDelegate.dicRider mutableCopy];
//                [dic setObject:[result valueForKey:@"user"] forKey:@"user"];
//                appDelegate.dicRider = dic;
//            }
            
            if (![newPwd isEqualToString:@""]) {
                [[[TXApp instance] getSettings] setPassword:newPwd];
                newPwd = @"";
                [self alertError:@"" message:LocalizedStr(@"Menu.Account.Passwd.result.text")];
            }
            
        } else {
            [self alertError:@"Error" message:descriptor.error];
        }
        [self hideBusyIndicator];
    }
    //[self hideBusyIndicator];
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker  dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Image Picker Delegate -
//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    TOCropViewController *cropController = [[TOCropViewController alloc] initWithCroppingStyle:self.croppingStyle image:image];
    cropController.delegate = self;
    
    //If profile picture, push onto the same navigation stack
    if (self.croppingStyle == TOCropViewCroppingStyleCircular) {
        [picker pushViewController:cropController animated:YES];
    }
    else { //otherwise dismiss, and then present from the main controller
        [picker dismissViewControllerAnimated:YES completion:^{
            [self presentViewController:cropController animated:YES completion:nil];
        }];
    }
}

#pragma mark - Cropper Delegate -
- (void)cropViewController:(TOCropViewController *)cropViewController didCropToImage:(UIImage *)image withRect:(CGRect)cropRect angle:(NSInteger)angle
{
    [self updateImageViewWithImage:image fromCropViewController:cropViewController];
}

- (void)cropViewController:(TOCropViewController *)cropViewController didCropToCircularImage:(UIImage *)image withRect:(CGRect)cropRect angle:(NSInteger)angle
{
    [self updateImageViewWithImage:image fromCropViewController:cropViewController];
}

- (void)updateImageViewWithImage:(UIImage *)croppedImage fromCropViewController:(TOCropViewController *)cropViewController
{
    if (self->_alertIdx == 9999) {
        UIImageView *imageView = (UIImageView *)[[super navigationView] viewWithTag:9998];
        if ([imageView.image isEqual:croppedImage]) {
            [cropViewController.presentingViewController dismissViewControllerAnimated:YES completion:nil];
            return;
        }
    }
    
    //---------------------------------------------------------------------------------------------------------------------------------------------
    //self.imageView.image = croppedImage;
    //[self shadowProfileImage:croppedImage];
    UIImage *image = [Utils resizeImage:croppedImage];

    //UIImage *image = [croppedImage resizedImageWithMaxEdge:2048];
    if (self->_alertIdx == 9999) {
        [self onPP311:image];
    }

    [cropViewController.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - Private methods

- (void)showCamera
{
    self.croppingStyle = TOCropViewCroppingStyleDefault;
    
    UIImagePickerController *standardPicker = [[UIImagePickerController alloc] init];
    standardPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    standardPicker.allowsEditing = NO;
    standardPicker.delegate = self;
    [self presentViewController:standardPicker animated:YES completion:nil];

}

- (void)openPhotoAlbum
{
    self.croppingStyle = TOCropViewCroppingStyleDefault;
    
    UIImagePickerController *standardPicker = [[UIImagePickerController alloc] init];
    standardPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    standardPicker.allowsEditing = NO;
    standardPicker.delegate = self;
    [self presentViewController:standardPicker animated:YES completion:nil];

}

- (void)editPhoto
{
    UIImageView *imageView;
    
    if (self->_alertIdx == 9999) {
        imageView = (UIImageView *)[[super navigationView] viewWithTag:9998];
    }
    
    if (imageView.image) {
        TOCropViewController *cropController = [[TOCropViewController alloc] initWithCroppingStyle:self.croppingStyle image:imageView.image];
        cropController.delegate = self;
        [self presentViewController:cropController animated:YES completion:nil];
    }
    
}

#pragma mark - UITextField Delegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (self->_alertIdx == 3) {
        UITextField *secondTextField = _securityAlertView.textFieldsArray[(textField.tag == 1 ? 2 : 1)];
        
        NSMutableString *currentString = textField.text.mutableCopy;
        
        [currentString replaceCharactersInRange:range withString:string];
        
        [_securityAlertView setButtonAtIndex:0 enabled:(currentString.length > 2 && secondTextField.text.length > 2)];
    }
    else {
        UITextField *secondTextField = _securityAlertView.textFieldsArray[(textField.tag == 0 ? 1 : 0)];
        
        NSMutableString *currentString = textField.text.mutableCopy;
        
        [currentString replaceCharactersInRange:range withString:string];
        
        [_securityAlertView setButtonAtIndex:0 enabled:(currentString.length > 2 && secondTextField.text.length > 2)];
    }
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField.tag < 1)
        [_securityAlertView.textFieldsArray[(textField.tag + 1)] becomeFirstResponder];
    else
    {
        if ([_securityAlertView isButtonEnabledAtIndex:0])
            [_securityAlertView dismissAnimated:YES completionHandler:nil];
        else
            [textField resignFirstResponder];
    }
    
    return YES;
}

#pragma mark - UIDatePicker Delegate

//listen to changes in the date picker and just log them
- (void) datePickerDateChanged:(UIDatePicker *)paramDatePicker{
    NSLog(@"Selected date = %@", paramDatePicker.date);
    NSDateFormatter __autoreleasing *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setTimeZone:[NSTimeZone systemTimeZone]];
    //[dateFormat setDateFormat:DATE_FORMAT_YYYYMMDDHH24MISS];
    //dateFormat.dateStyle = NSDateFormatterMediumStyle;
    //NSString *dateString =  [dateFormat stringFromDate:paramDatePicker.date];
    
    [dateFormat setDateFormat:DATE_FORMAT_YYYYMMDD];
    NSString *dateString = [dateFormat stringFromDate:paramDatePicker.date];
    NSLog(@"dateString:%@",dateString);
    
    UITextField *tf = (UITextField*)[self.view viewWithTag:103];
    tf.text = dateString;
    
}

#pragma mark - UITextView Delegate Methods

//캐릭터가 텍스트 뷰에 표시되기 직전 아래 메소드가 호출된다
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    
    
    //newLineCharacterSet이 있으면 done button이 호출됨. 따라서 키보드가 사라짐.
    NSCharacterSet *doneButtonCharacterSet = [NSCharacterSet newlineCharacterSet];
    NSRange replacementTextRange = [text rangeOfCharacterFromSet:doneButtonCharacterSet];
    NSUInteger location = replacementTextRange.location;
    
    //텍스트가 190자가 넘지 않도록 제한
    if (textView.text.length + text.length > 190){
        if (location != NSNotFound){
            [textView resignFirstResponder];
        }
        return NO;
    }
    
    else if (location != NSNotFound){
        if([text isEqualToString:@"\n"]) {
            return YES;
        }
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}

#pragma mark - UITableView DataSource Methods
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [tableView setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [tableView setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
    
    UIView *seperatorView = [[UIView alloc] initWithFrame:CGRectMake(0, cell.frame.size.height - 1, cell.frame.size.width, 1)];
    seperatorView.backgroundColor = UIColorTableSeperator;
    [cell addSubview:seperatorView];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    // This will create a "invisible" footer
    return 0.01f;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [UIView new];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [dataArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    cell.tag = indexPath.row;
    int tag = (int)indexPath.row+100;
    //[self changeCell:tableView withIndexPath:indexPath andTag:tag];
    [self addCell:tableView withCell:cell withIndexPath:indexPath andTag:tag];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    cell.textLabel.text = [NSString stringWithFormat:@"      %@",[dataArray objectAtIndex:indexPath.row]];
    
    
    //    if([[tableView indexPathsForSelectedRows] containsObject:indexPath]) {
    //        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    //    } else {
    //        cell.accessoryType = UITableViewCellAccessoryNone;
    //    }
    
    return cell;
}

#pragma mark - UITableView Delegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self changeCell:tableView withIndexPath:indexPath andTag:((int)indexPath.row+100)];
    
    //cell.accessoryView.hidden = NO;
}
- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self changeCell:tableView withIndexPath:indexPath andTag:((int)indexPath.row+100)];
    
    //cell.accessoryView.hidden = YES;
}

-(void)addCell:(UITableView*)tableView withCell:(UITableViewCell*)cell withIndexPath:(NSIndexPath*)indexPath andTag:(int)tag{
    
    UIImageView *preView = [cell.contentView viewWithTag:tag];
    [preView removeFromSuperview];
    
    UILabel *preLabel = [cell.contentView viewWithTag:(tag+100)];
    [preLabel removeFromSuperview];
    
    NSInteger _h = cell.frame.size.height;
    NSInteger _x = 16;
    UIImageView* imv = [[UIImageView alloc]initWithFrame:CGRectMake(cell.frame.size.width - _x*4 ,_h/2-_x/2, _x, _x)];
    imv.tag = tag;
    UIImage *cellImage;
    
    if([[tableView indexPathsForSelectedRows] containsObject:indexPath]) {
        cellImage = [UIImage imageNamed:@"form_radio_chk"];
    }
    else {
        cellImage = [UIImage imageNamed:@"form_radio_normal"];
    }
    imv.image=cellImage;
    [cell.contentView addSubview:imv];
    /*
     UILabel *mylabel = [[UILabel alloc] initWithFrame:CGRectMake(20,5, 150, 40)];
     mylabel.tag = (tag+100);
     mylabel.text = [dataArray objectAtIndex:(tag-100)];
     [cell.contentView addSubview:mylabel];
     */
    //    UILabel *cellLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, _h/2-_x/2, 120, 35)];
    //    cellLabel.tag = (int)tag+100;
    //    [cellLabel setText:[dataArray objectAtIndex:(tag-100)]];
    //    //[cellLabel setFont:[UIFont boldSystemFontOfSize:12]];
    //    [cellLabel setBackgroundColor:[UIColor clearColor]];
    //
    //    [cell addSubview:cellLabel];
}

-(void)changeCell:(UITableView*)tableView withIndexPath:(NSIndexPath*)indexPath andTag:(int)tag{
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    UIImageView *preView = [cell.contentView viewWithTag:tag];
    [preView removeFromSuperview];
    
    UILabel *preLabel = [cell.contentView viewWithTag:(tag+100)];
    [preLabel removeFromSuperview];
    
    NSInteger _h = cell.frame.size.height;
    NSInteger _x = 16;
    UIImageView* imv = [[UIImageView alloc]initWithFrame:CGRectMake(cell.frame.size.width - _x*4 ,_h/2-_x/2, _x, _x)];
//    UIImageView* imv = [[UIImageView alloc]initWithFrame:CGRectMake(0,_h/2-_x/2, _x, _x)];
    imv.tag = tag;
    UIImage *cellImage;
    
    if([[tableView indexPathsForSelectedRows] containsObject:indexPath]) {
        cellImage = [UIImage imageNamed:@"form_radio_chk"];
    }
    else {
        cellImage = [UIImage imageNamed:@"form_radio_normal"];
    }
    imv.image=cellImage;
    [cell.contentView addSubview:imv];
    /*
     UILabel *mylabel = [[UILabel alloc] initWithFrame:CGRectMake(20,5, 150, 40)];
     mylabel.tag = (tag+100);
     mylabel.text = [dataArray objectAtIndex:(tag-100)];
     [cell.contentView addSubview:mylabel];
     */
    //    UILabel *cellLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, _h/2-_x/2, 120, 35)];
    //    cellLabel.tag = (int)tag+100;
    //    [cellLabel setText:[dataArray objectAtIndex:(tag-100)]];
    //    //[cellLabel setFont:[UIFont boldSystemFontOfSize:12]];
    //    [cellLabel setBackgroundColor:[UIColor clearColor]];
    //
    //    [cell addSubview:cellLabel];
}

#pragma mark -
#pragma mark Validator
#pragma mark -
-(void) resignKeyboard
{
    [self.view endEditing:YES];
}

- (IBAction)validateAction:(id)sender
{
}

#pragma ValidatorDelegate - Delegate methods

- (void) preValidation
{
}

- (void)onSuccess
{
}

- (void)onFailure:(Rule *)failedRule
{
    
    [WToast showWithText:failedRule.failureMessage duration:kWTShort roundedCorners:NO gravity:kWTGravityTop];
}

@end
