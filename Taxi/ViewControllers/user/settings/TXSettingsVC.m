//
//  MenuViewController.m
//  SlideMenu
//

//

#import "TXSettingsVC.h"
#import "RTLabel.h"
#import "LGAlertView.h"
#import "TXCallModel.h"
#import "TXLauncherVC.h"
//#import "RedisSingleton.h"
#ifdef _DRIVER_MODE

#ifdef _WITZM
#import "TXAboutVC.h"
#elif defined _CHACHA
#import "TXAboutVC.h"
#endif

#import "CMMapLauncher.h"
#endif

@interface TXSettingsVC() <UITableViewDelegate,UITableViewDataSource>{
    NSMutableArray *items;
    NSArray *_msgList;
    
    NSArray *dataArray;
    UILabel *mapName;
    
    NSString *agree_email;
    NSString *agree_sms;
    NSString *agree_push;
    
    NSInteger swIndex;
}

@end

@implementation TXSettingsVC {
    //NSMutableDictionary *propertyMap;
    NSArray *_sectionTitles, *_cellTitles, *_cellMaps,*_cellThumbnails,*_cellIcons;
}

-(void)viewDidLoad {
    
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didEnterBackground) name:UIApplicationDidEnterBackgroundNotification object:nil];
    
    [super configureBottomList];//
    UIView *n = [super navigationView];
    NSInteger _y = n.frame.origin.y + n.frame.size.height;
    UIView *line = [n viewWithTag:1500];
    line.backgroundColor = [UIColor clearColor];
    
    //UILabel *label;
    
    agree_email = @"0";
    agree_sms = @"0";
    agree_push = @"0";
    swIndex = 0;

    // version
    NSString *htmlString = [NSString stringWithFormat:@"%@ %@ / %@ %@",LocalizedStr(@"Menu.Settings.Version.Current.title"),[Utils appVersion],LocalizedStr(@"Menu.Settings.Version.Last.title"),appDelegate.appversion];
    
    UILabel *label = (UILabel*)[[super navigationView] viewWithTag:1111];
    label.tag = 2001;
    label.text = htmlString;
    label.textColor = HEXCOLOR(0x666666ff);
    
    // update
    UIView *updateView = [Utils addButtonBasic:self.view underView:nil dim:0];
    UIButton *button = [updateView viewWithTag:11];
    button.tag = 999;
    [button setTitle:LocalizedStr(@"Menu.Settings.BTN.text") forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:14.0f];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(addButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    updateView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    CGRect rect = updateView.frame;
    rect.origin.y = _y;
    updateView.frame = rect;
    
    [self.view addSubview:updateView];

    UIView *baseView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 0)];
    baseView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    baseView.backgroundColor = [UIColor whiteColor];
    
    UIView *cell10View;
    
#ifdef _DRIVER_MODE
    // 지도
    cell10View = [Utils addLabelView:self.view underView:nil dim:0];
    label = [cell10View viewWithTag:11];
    label.text = LocalizedStr(@"Menu.Settings.Driver.title1");
    [baseView addSubview:cell10View];
    
    // 네비
    cell10View = [Utils addDoubleLabelView:self.view underView:cell10View dim:0];
    cell10View.backgroundColor = [UIColor whiteColor];
    label = [cell10View viewWithTag:11];
    label.text = LocalizedStr(@"Menu.Settings.Driver.title1.text1");
    mapName = [cell10View viewWithTag:12];
    mapName.textColor = HEXCOLOR(0x333333ff);
    [mapName setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:16.0f]];
    
#ifdef _WITZM
    mapName.tag = 2;
    mapName.text = [[[TXApp instance] getSettings] getFDKeychain:@"MAP"];
    [Utils addButtonBlank:cell10View target:self selector:@selector(onClickNavi)];
#elif defined _CHACHA
    [[[TXApp instance] getSettings] setFDKeychain:@"MAP" value:@""];
    if ([self isInstalledTMAP]) {
        mapName.text = [[[TXApp instance] getSettings] getFDKeychain:@"MAP"];
    }
    else {
        mapName.text = LocalizedStr(@"Menu.Settings.Driver.Map.Install.text");
        [Utils addButtonBlank:cell10View target:self selector:@selector(onClickNavi)];
    }
#endif
    
    [baseView addSubview:cell10View];
    
    // 회사
    cell10View = [Utils addLabelView:self.view underView:cell10View dim:0];
    label = [cell10View viewWithTag:11];
    label.text = LocalizedStr(@"Menu.Settings.Driver.title2");
    [baseView addSubview:cell10View];
    
    cell10View = [Utils addWhiteLabelView:self.view underView:cell10View dim:0];
    label = [cell10View viewWithTag:11];
    label.text = LocalizedStr(@"Menu.Settings.Driver.title2.text1");
    [Utils addButtonBlank:cell10View target:self selector:@selector(urlLinkAppStore)];
    [baseView addSubview:cell10View];
    
    cell10View = [Utils addWhiteLabelView:self.view underView:cell10View dim:0];
    label = [cell10View viewWithTag:11];
    label.text = LocalizedStr(@"Menu.Settings.Driver.title2.text2");
    [Utils addButtonBlank:cell10View target:self selector:@selector(urlLinkFacebook)];
    [baseView addSubview:cell10View];
    
    // 약관
    cell10View = [Utils addLabelView:self.view underView:cell10View dim:0];
    label = [cell10View viewWithTag:11];
    label.text = LocalizedStr(@"Menu.Settings.Driver.title5");
    [baseView addSubview:cell10View];
    
    cell10View = [Utils addWhiteLabelView:self.view underView:cell10View dim:0];
    label = [cell10View viewWithTag:11];
    label.text = LocalizedStr(@"Menu.Settings.Driver.title5.text1");
    line = [cell10View viewWithTag:222];
    line.hidden = YES;
    [Utils addButtonBlank:cell10View target:self selector:@selector(urlLink1)];
    [baseView addSubview:cell10View];
    
    cell10View = [Utils addWhiteLabelView:self.view underView:cell10View dim:0];
    label = [cell10View viewWithTag:11];
    label.text = LocalizedStr(@"Menu.Settings.Driver.title5.text2");
    line = [cell10View viewWithTag:222];
    line.hidden = YES;
    [Utils addButtonBlank:cell10View target:self selector:@selector(urlLink2)];
    [baseView addSubview:cell10View];

    cell10View = [Utils addWhiteLabelView:self.view underView:cell10View dim:0];
    label = [cell10View viewWithTag:11];
    label.text = LocalizedStr(@"Menu.Settings.Driver.title5.text3");
    [Utils addButtonBlank:cell10View target:self selector:@selector(urlLink3)];
    [baseView addSubview:cell10View];
    
    // 마케팅 수신동의
    cell10View = [Utils addLabelView:self.view underView:cell10View dim:0];
    label = [cell10View viewWithTag:11];
    label.text = LocalizedStr(@"Menu.Settings.Driver.title4");
    [baseView addSubview:cell10View];
    
    cell10View = [Utils addWhiteLabelViewSwitch:self.view underView:cell10View dim:0];
    label = [cell10View viewWithTag:11];
    label.text = LocalizedStr(@"Menu.Settings.Driver.title4.text1");
    line = [cell10View viewWithTag:222];
    line.hidden = YES;
    UISwitch *mySwitch = [cell10View viewWithTag:333];
    mySwitch.tag = 301;
    [mySwitch addTarget:self action:@selector(changeSwitch:) forControlEvents:UIControlEventValueChanged];
    [baseView addSubview:cell10View];
    
    cell10View = [Utils addWhiteLabelViewSwitch:self.view underView:cell10View dim:0];
    label = [cell10View viewWithTag:11];
    label.text = LocalizedStr(@"Menu.Settings.Driver.title4.text2");
    line = [cell10View viewWithTag:222];
    line.hidden = YES;
    mySwitch = [cell10View viewWithTag:333];
    mySwitch.tag = 302;
    [mySwitch addTarget:self action:@selector(changeSwitch:) forControlEvents:UIControlEventValueChanged];
    [baseView addSubview:cell10View];
    
    cell10View = [Utils addWhiteLabelViewSwitch:self.view underView:cell10View dim:0];
    label = [cell10View viewWithTag:11];
    label.text = LocalizedStr(@"Menu.Settings.Driver.title4.text3");
    mySwitch = [cell10View viewWithTag:333];
    mySwitch.tag = 303;
    [mySwitch addTarget:self action:@selector(changeSwitch:) forControlEvents:UIControlEventValueChanged];
    [baseView addSubview:cell10View];
    
    // 로그아웃
    cell10View = [Utils addLabelView:self.view underView:cell10View dim:0];
    label = [cell10View viewWithTag:11];
    //label.text = LocalizedStr(@"Menu.Settings.Driver.title3");
    [baseView addSubview:cell10View];
    
    //if (appDelegate.pstate < 220)
    {
        cell10View = [Utils addWhiteLabelView:self.view underView:cell10View dim:0];
        label = [cell10View viewWithTag:11];
        label.text = LocalizedStr(@"Menu.Settings.Driver.title3.text1");
        label.textColor = UIColorDefault;
        [Utils addButtonBlank:cell10View target:self selector:@selector(logout)];
        [baseView addSubview:cell10View];
    }
#else
    // 지도
    cell10View = [Utils addLabelView:self.view underView:cell10View dim:0];
    label = [cell10View viewWithTag:11];
    label.text = LocalizedStr(@"Menu.Settings.Driver.title2");
    [baseView addSubview:cell10View];
    
    cell10View = [Utils addWhiteLabelView:self.view underView:cell10View dim:0];
    label = [cell10View viewWithTag:11];
    label.text = LocalizedStr(@"Menu.Settings.Driver.title2.text1");
    [Utils addButtonBlank:cell10View target:self selector:@selector(urlLinkAppStore)];
    [baseView addSubview:cell10View];

    cell10View = [Utils addWhiteLabelView:self.view underView:cell10View dim:0];
    label = [cell10View viewWithTag:11];
    label.text = LocalizedStr(@"Menu.Settings.Driver.title2.text2");
    [Utils addButtonBlank:cell10View target:self selector:@selector(urlLinkFacebook)];
    [baseView addSubview:cell10View];
    
    // 약관
    cell10View = [Utils addLabelView:self.view underView:cell10View dim:0];
    label = [cell10View viewWithTag:11];
    label.text = LocalizedStr(@"Menu.Settings.Driver.title5");
    [baseView addSubview:cell10View];
    
    cell10View = [Utils addWhiteLabelView:self.view underView:cell10View dim:0];
    label = [cell10View viewWithTag:11];
    label.text = LocalizedStr(@"Menu.Settings.Rider.title5.text1");
    line = [cell10View viewWithTag:222];
    line.hidden = YES;
    [Utils addButtonBlank:cell10View target:self selector:@selector(urlLink1)];
    [baseView addSubview:cell10View];
    
    cell10View = [Utils addWhiteLabelView:self.view underView:cell10View dim:0];
    label = [cell10View viewWithTag:11];
    label.text = LocalizedStr(@"Menu.Settings.Rider.title5.text2");
    line = [cell10View viewWithTag:222];
    line.hidden = YES;
    [Utils addButtonBlank:cell10View target:self selector:@selector(urlLink2)];
    [baseView addSubview:cell10View];
    
    cell10View = [Utils addWhiteLabelView:self.view underView:cell10View dim:0];
    label = [cell10View viewWithTag:11];
    label.text = LocalizedStr(@"Menu.Settings.Rider.title5.text3");
    line = [cell10View viewWithTag:222];
    line.hidden = YES;
    [Utils addButtonBlank:cell10View target:self selector:@selector(urlLink3)];
    [baseView addSubview:cell10View];
    
    cell10View = [Utils addWhiteLabelView:self.view underView:cell10View dim:0];
    label = [cell10View viewWithTag:11];
    label.text = LocalizedStr(@"Menu.Settings.Rider.title5.text4");
    [Utils addButtonBlank:cell10View target:self selector:@selector(urlLink4)];
    [baseView addSubview:cell10View];

    // 마케팅 수신동의
    cell10View = [Utils addLabelView:self.view underView:cell10View dim:0];
    label = [cell10View viewWithTag:11];
    label.text = LocalizedStr(@"Menu.Settings.Driver.title4");
    [baseView addSubview:cell10View];
    
    cell10View = [Utils addWhiteLabelViewSwitch:self.view underView:cell10View dim:0];
    label = [cell10View viewWithTag:11];
    label.text = LocalizedStr(@"Menu.Settings.Driver.title4.text1");
    line = [cell10View viewWithTag:222];
    line.hidden = YES;
    UISwitch *mySwitch = [cell10View viewWithTag:333];
    mySwitch.tag = 301;
    [mySwitch addTarget:self action:@selector(changeSwitch:) forControlEvents:UIControlEventValueChanged];
    [baseView addSubview:cell10View];
    
    cell10View = [Utils addWhiteLabelViewSwitch:self.view underView:cell10View dim:0];
    label = [cell10View viewWithTag:11];
    label.text = LocalizedStr(@"Menu.Settings.Driver.title4.text2");
    line = [cell10View viewWithTag:222];
    line.hidden = YES;
    mySwitch = [cell10View viewWithTag:333];
    mySwitch.tag = 302;
    [mySwitch addTarget:self action:@selector(changeSwitch:) forControlEvents:UIControlEventValueChanged];
    [baseView addSubview:cell10View];
    
    cell10View = [Utils addWhiteLabelViewSwitch:self.view underView:cell10View dim:0];
    label = [cell10View viewWithTag:11];
    label.text = LocalizedStr(@"Menu.Settings.Driver.title4.text3");
    mySwitch = [cell10View viewWithTag:333];
    mySwitch.tag = 303;
    [mySwitch addTarget:self action:@selector(changeSwitch:) forControlEvents:UIControlEventValueChanged];
    [baseView addSubview:cell10View];
    
    // 로그아웃
    cell10View = [Utils addLabelView:self.view underView:cell10View dim:0];
    label = [cell10View viewWithTag:11];
    //label.text = LocalizedStr(@"Menu.Settings.Driver.title3");
    [baseView addSubview:cell10View];
    //UIView *line = [cell10View viewWithTag:222];
    //line.hidden = YES;
    
    //if (appDelegate.ustate < 120)
    {
        cell10View = [Utils addWhiteLabelView:self.view underView:cell10View dim:0];
        label = [cell10View viewWithTag:11];
        label.text =  LocalizedStr(@"Menu.Settings.Driver.title3.text1");
        label.textColor = UIColorDefault;
        [Utils addButtonBlank:cell10View target:self selector:@selector(logout)];
        [baseView addSubview:cell10View];
        //UIView *line = [cell10View viewWithTag:222];
        //line.hidden = YES;
    }
#endif
    
    cell10View = [Utils addLabelView:self.view underView:cell10View dim:0];
    label = [cell10View viewWithTag:11];
    //label.text = LocalizedStr(@"Menu.Settings.Driver.title3");
    [baseView addSubview:cell10View];
    line = [cell10View viewWithTag:222];
    line.hidden = YES;
    
/*
    // update
    UIView *updateView = [Utils addButtonBasic:self.view underView:cell10View dim:0];
    UIButton *button = [updateView viewWithTag:11];
    button.tag = 999;
    [button setTitle:LocalizedStr(@"Menu.Settings.BTN.text") forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:14.0f];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(addButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    updateView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    CGRect rect = updateView.frame;
    rect.origin.y = cell10View.frame.origin.y + cell10View.frame.size.height;//_y;
    updateView.frame = rect;
    
    // version
    NSString *htmlString = [NSString stringWithFormat:@"%@ %@ / %@ %@",LocalizedStr(@"Menu.Settings.Version.Current.title"),[Utils appVersion],LocalizedStr(@"Menu.Settings.Version.Last.title"),appDelegate.appversion];
    
    label = [[UILabel alloc] initWithFrame:CGRectMake(kLeftMargin,
                                                               updateView.frame.origin.y + updateView.frame.size.height + 10,
                                                               self.view.bounds.size.width-(
                                                                                            kLeftMargin * 2),
                                                               kNaviBottomTextHeight)];
    [label setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:12]];
    label.textAlignment = NSTextAlignmentCenter;
    label.tag = 2001;
    label.text = htmlString;
    label.textColor = HEXCOLOR(0x666666ff);
    
    [baseView addSubview:updateView];
    [baseView addSubview:label];
 
    baseView.frame = CGRectMake(0, 0, self.view.frame.size.width, label.frame.origin.y + label.frame.size.height + 20);
*/
    baseView.frame = CGRectMake(0, 0, self.view.frame.size.width, cell10View.frame.origin.y + cell10View.frame.size.height);
    
    
    //------------------------------------------------------------------------------------------------------------------------
    self.myScrollView.backgroundColor = [UIColor whiteColor];
    baseView.backgroundColor = [UIColor whiteColor];
    /*
    self.myScrollView.frame = CGRectMake(0,
                                         //_y + updateView.frame.size.height + 10,
                                         _y + 10,
                                         self.view.frame.size.width,
                                         self.view.frame.size.height - _y - 10
                                         //self.view.frame.size.height - _y - updateView.frame.size.height - 10
                                         );
    */
    self.myScrollView.frame = CGRectMake(0,
                                         _y + updateView.frame.size.height + 10,
                                         self.view.frame.size.width,
                                         self.view.frame.size.height - _y - updateView.frame.size.height - 10);
    
    self.myScrollView.contentSize = CGSizeMake(baseView.frame.size.width, baseView.frame.size.height);
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0, 0.0, 0, 0.0);
    self.myScrollView.contentInset = contentInsets;
    self.myScrollView.scrollIndicatorInsets = contentInsets;
    self.myScrollView.scrollsToTop = YES;
    [self.myScrollView addSubview:baseView];
    //------------------------------------------------------------------------------------------------------------------------
    //[self.view addSubview:baseView];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self registerEventListeners];
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self removeEventListeners];
}

#pragma mark - Configure

-(void)configureStyles {
    [super configureStyles];
    
    
    [super navigationType01X:[UIImage imageNamed:@"btn_back"] centerText:LocalizedStr(@"Menu.Settings.title")];

}

-(void)configure {
    [super configure];
    
    // 2. init event
    _msgList = @[
                 @[@"UR114",@""], // rider
                 @[@"PP114",@""], // driver
                 
                 @[@"UR200",@""], // rider
                 @[@"PP200",@""], // driver
                 
                 @[@"UR211",@""], // rider
                 @[@"PP211",@""], // driver
                 ];
    
    //[self registerEventListeners];
    dataArray = @[LocalizedStr(@"Menu.Settings.Driver.Map.List1.text"),
                  LocalizedStr(@"Menu.Settings.Driver.Map.List2.text"),
                  LocalizedStr(@"Menu.Settings.Driver.Map.List3.text")];
    
    if (appDelegate.isDriverMode) {
        [self->model requsetAPI:@"PP200" property:nil];
    }
    else {
        [self->model requsetAPI:@"UR200" property:nil];
    }
    
}

-(void) registerEventListeners {
    for (NSArray *msg in _msgList) {
        [self->model addEventListener:self forEvent:[msg objectAtIndex:0] eventParams:nil];
    }
}

-(void) removeEventListeners {
    for (NSArray *msg in _msgList) {
        [self->model removeEventListener:self forEvent:[msg objectAtIndex:0]];
    }
}

#pragma mark - Action
-(IBAction)onNaviButtonClick:(id)sender {
    UIButton *btn = (UIButton*)sender;
    NSLog(@"onNaviButtonClick");
    //[super onNaviButtonClick:sender];
    
    if (btn.tag == 1100) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark - IBAction
-(void)didEnterBackground{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)addButtonPressed:(id)sender {
    UIButton *btn = (UIButton*)sender;
    
    if (btn.tag == 999) {
        [Utils updateApp:YES];
    }
}
/*
-(IBAction)buttonPressed:(id)sender {
    UIButton *btn = (UIButton*)sender;
    
    //    _idx = btn.tag - 1000;
    for (NSDictionary* dic in self->items) {
        if ([[dic objectForKey:@"cseq"] isEqualToString:[NSString stringWithFormat:@"%d",(int)btn.tag]]) {
            LGAlertView *alertView = [[LGAlertView alloc] initWithTitle:@""
                                        message:@"Do you want to delete ?"
                                          style:LGAlertViewStyleAlert
                                   buttonTitles:@[LocalizedStr(@"Button.OK")]
                              cancelButtonTitle:LocalizedStr(@"Button.CANCEL")
                         destructiveButtonTitle:nil
                                  actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                                      NSLog(@"actionHandler, %@, %lu", title, (long unsigned)index);
                                      
                                      [self showBusyIndicator:@"deletting ... "];
                                      [self->model UR332:[dic objectForKey:@"cseq"]];
                                  }
                                  cancelHandler:^(LGAlertView *alertView) {
                                  }
                             destructiveHandler:^(LGAlertView *alertView) {
                             }];
            [Utils initAlertButtonColor:alertView];
            [alertView showAnimated:YES completionHandler:nil];
            
            break;
        }
    }
}
*/
- (void)onClickNavi
{
#ifdef _DRIVER_MODE
    
#ifdef _WITZM
    if (![self isInstalledTMAP]) {
        return;
    }
#elif defined _CHACHA
    if (![self isInstalledTMAP]) {
        return;
    }
#endif
    
#else
    
#ifdef _WITZM
#elif defined _CHACHA
#endif
    
#endif

    
    UITableView* mTableView = [[UITableView alloc] init];
    mTableView.delegate=self;
    mTableView.dataSource=self;
    
    mTableView.allowsMultipleSelection = NO;
    mTableView.frame = CGRectMake(0.f, 0.f, 320, 124.f);
    mTableView.contentMode = UIViewContentModeScaleAspectFit;
    mTableView.scrollEnabled = NO;
    
    NSIndexPath *indexPath = nil;
    int index = 0;
    for(NSString *mapSring in dataArray) {
        if ([mapName.text isEqualToString:mapSring]) {
            indexPath = [NSIndexPath indexPathForRow:index inSection:0];
            mapName.tag = index;
        }
        index++;
    }
    if (indexPath != nil) {
        [mTableView selectRowAtIndexPath:indexPath
                                animated:YES
                          scrollPosition:UITableViewScrollPositionNone];
        //[mTableView tableView:mTableView didSelectRowAtIndexPath:indexPath];
        [mTableView.delegate tableView:mTableView didSelectRowAtIndexPath:indexPath];
    }
    
    LGAlertView *alertView = [[LGAlertView alloc] initWithViewAndTitle:LocalizedStr(@"Menu.Settings.Driver.title1.text1")
                                                               message:nil
                                                                 style:LGAlertViewStyleAlert
                                                                  view:mTableView
                                                          buttonTitles:@[LocalizedStr(@"Button.OK")]
                                                     cancelButtonTitle:LocalizedStr(@"Button.CANCEL")
                                                destructiveButtonTitle:nil
                                                         actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                                                             NSLog(@"actionHandler, %@, %lu", title, (long unsigned)index);
                                                             
                                                             for(NSIndexPath* indexPath in [mTableView indexPathsForSelectedRows]) {
                                                                 NSLog(@"cell.tag:%@",[dataArray objectAtIndex:indexPath.row]);
                                                                 
                                                                 //[propertyMap setValue:dataArray[indexPath.row] forKey:@"gender"];
                                                                 //tf.text = dataArray[indexPath.row];
                                                                 
                                                                 mapName.text = [dataArray objectAtIndex:indexPath.row];
                                                                 mapName.tag = indexPath.row;
                                                                 
                                                                 [[[TXApp instance] getSettings] setFDKeychain:@"MAP" value:mapName.text];
                                                             }
                                                         }
                                                         cancelHandler:^(LGAlertView *alertView) {
                                                             NSLog(@"cancelHandler");
                                                         }
                                                    destructiveHandler:^(LGAlertView *alertView) {
                                                        NSLog(@"destructiveHandler");
                                                    }];
    //alertView.heightMax = 256.f;
    //alertView.heightMax = 156.f;
    //alertView.cancelButtonBackgroundColor = [UIColor redColor];
    [Utils initAlertButtonColor:alertView];
    alertView.windowLevel = LGAlertViewWindowLevelAboveStatusBar;
    [alertView showAnimated:YES completionHandler:nil];
    
}

-(void)urlLinkFacebook
{
    [[UIApplication sharedApplication] openURL: [NSURL URLWithString:FACEBOOK_URL]];
}

-(void)urlLinkLegal
{
    NSCharacterSet *set = [NSCharacterSet URLQueryAllowedCharacterSet];
    NSString* content = @"";
#ifdef _DRIVER_MODE
    // 한글은 인코딩되어야 한다.
#ifdef _WITZM
    content = [DRIVER_TERM_URL stringByAddingPercentEncodingWithAllowedCharacters:set];
#elif defined _CHACHA
    content = [DRIVER_TERM_URL stringByAddingPercentEncodingWithAllowedCharacters:set];
#endif
    
#else
    // 한글은 인코딩되어야 한다.
#ifdef _WITZM
    content = [SERVICE_TERM_URL stringByAddingPercentEncodingWithAllowedCharacters:set];
#elif defined _CHACHA
    content = [SERVICE_TERM_URL stringByAddingPercentEncodingWithAllowedCharacters:set];
#endif
    
#endif
    [[UIApplication sharedApplication] openURL: [NSURL URLWithString:content]];
}

// 이용약관
-(void)urlLink1 {
    // 한글은 인코딩되어야 한다.
    NSCharacterSet *set = [NSCharacterSet URLQueryAllowedCharacterSet];
#ifdef _DRIVER_MODE
    NSString* content = [DRIVER_TERM_URL stringByAddingPercentEncodingWithAllowedCharacters:set];
#else
    NSString* content = [SERVICE_TERM_URL stringByAddingPercentEncodingWithAllowedCharacters:set];
#endif
    [[UIApplication sharedApplication] openURL: [NSURL URLWithString:content]];
}
// 개인정보 수집 이용약관
-(void)urlLink2 {
    // 한글은 인코딩되어야 한다.
    NSCharacterSet *set = [NSCharacterSet URLQueryAllowedCharacterSet];
    NSString* content = [PRIVACY_TERM_URL stringByAddingPercentEncodingWithAllowedCharacters:set];
    [[UIApplication sharedApplication] openURL: [NSURL URLWithString:content]];
}
//위치정보 이용약관
-(void)urlLink3 {
    // 한글은 인코딩되어야 한다.
    NSCharacterSet *set = [NSCharacterSet URLQueryAllowedCharacterSet];
    NSString* content = [LOCATION_TERM_URL stringByAddingPercentEncodingWithAllowedCharacters:set];
    [[UIApplication sharedApplication] openURL: [NSURL URLWithString:content]];
}

#ifndef _DRIVER_MODE
//렌터카 대여약관
-(void)urlLink4 {
    // 한글은 인코딩되어야 한다.
    NSCharacterSet *set = [NSCharacterSet URLQueryAllowedCharacterSet];
    NSString* content = [SERVICE_RENT_URL stringByAddingPercentEncodingWithAllowedCharacters:set];
    [[UIApplication sharedApplication] openURL: [NSURL URLWithString:content]];
}
#endif

-(void)urlLinkAppStore
{
    [[UIApplication sharedApplication] openURL: [NSURL URLWithString:APPSTORE_URL]];
}

-(void)logout
{
    if (appDelegate.isDriverMode && appDelegate.pstate >= 220) {
        if ([Utils isNotAllowedMenu]) {
            return;
        }
    }
    
    if (!appDelegate.isDriverMode && appDelegate.ustate >= 120) {
        if ([Utils isNotAllowedMenu]) {
            return;
        }
    }
    
    // logout
    LGAlertView *alertView = [[LGAlertView alloc] initWithTitle:nil
                                message:LocalizedStr(@"Menu.Settings.logout.alert.text")
                                  style:LGAlertViewStyleAlert
                           buttonTitles:@[LocalizedStr(@"Button.OK")]
                      cancelButtonTitle:LocalizedStr(@"Button.CANCEL")
                 destructiveButtonTitle:nil
                          actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                              NSLog(@"actionHandler, %@, %lu", title, (long unsigned)index);
                              
                              dispatch_async(dispatch_get_main_queue(), ^{
                                  [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_LOGOUT object:nil userInfo:nil];
                              });
                              
                              TXUser *user;
                              // 메시지 body정보
                              TXSettings  *settings = [[TXApp instance] getSettings];
                              NSString *userUid   = [settings getUserId];
                              NSString *userTelno = [settings getUserTelno];
                              
                              user = [[TXUser alloc] init];
                              user.uid = userUid;
                              user.utelno = userTelno;
                              
                              [self showBusyIndicator:@"Requesting LogOut ... "];
                              
                              if (appDelegate.isDriverMode) {
                                  [self->model PP114];
                              }
                              else {
                                  [self->model UR114:user];
                              }
                              
                          }
                          cancelHandler:^(LGAlertView *alertView) {
//                              NSLog(@"cancelHandler");
//                              NSLog(@"Stay in system");
                          }
                     destructiveHandler:^(LGAlertView *alertView) {
//                         NSLog(@"destructiveHandler");
                     }];
    [Utils initAlertButtonColor:alertView];
    [alertView showAnimated:YES completionHandler:nil];
    
}

- (IBAction)changeSwitch:(id)sender{
    UISwitch *s = (UISwitch*)sender;
    
    [self showBusyIndicator:@"deletting ... "];
    swIndex = 0;
    
    if([s isOn]){
        NSLog(@"Switch(%ld) is ON",s.tag);
        if (s.tag == 301) {
            agree_email = @"1";
            swIndex = 11;
        }
        else if (s.tag == 302) {
            agree_sms = @"1";
            swIndex = 21;
        }
        else if (s.tag == 303) {
            agree_push = @"1";
            swIndex = 31;
        }
    } else{
        NSLog(@"Switch(%ld) is OFF",s.tag);
        if (s.tag == 301) {
            agree_email = @"0";
            swIndex = 10;
        }
        else if (s.tag == 302) {
            agree_sms = @"0";
            swIndex = 20;
        }
        else if (s.tag == 303) {
            agree_push = @"0";
            swIndex = 30;
        }
    }

    NSDictionary *dic = @{
                          @"sms":agree_sms,
                          @"email":agree_email,
                          @"push":agree_push
                          };
    if (appDelegate.isDriverMode) {
        [self->model requsetAPI:@"PP211" property:@{@"provider_setting":dic}];
    }
    else {
        [self->model requsetAPI:@"UR211" property:@{@"user_setting":dic}];
    }
    
}

#ifdef _DRIVER_MODE
-(BOOL)isInstalledTMAP {
    NSString *__mapName = LocalizedStr(@"Menu.Settings.Driver.Map.List1.text");
    CMMapApp mapApp = CMMapAppTmap;
    
    BOOL installed = [CMMapLauncher isMapAppInstalled:mapApp];
    if (installed) {
        [[[TXApp instance] getSettings] setFDKeychain:@"MAP" value:LocalizedStr(@"Menu.Settings.Driver.Map.List1.text")];
        return YES;
    }
    else {
        [[[TXApp instance] getSettings] setFDKeychain:@"MAP" value:@""];
        LGAlertView *alertView = [[LGAlertView alloc] initWithTitle:@""
                                                            message:[NSString stringWithFormat:@"%@ %@",__mapName, LocalizedStr(@"Menu.Settings.Driver.Map.NotExist.text")]
                                                              style:LGAlertViewStyleAlert
                                                       buttonTitles:@[LocalizedStr(@"Button.OK")]
                                                  cancelButtonTitle:LocalizedStr(@"Button.CANCEL")
                                             destructiveButtonTitle:nil
                                                      actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                                                          //TMAPSTORE_URL
                                                          [[UIApplication sharedApplication] openURL: [NSURL URLWithString:TMAPSTORE_URL]];
                                                      }
                                                      cancelHandler:^(LGAlertView *alertView) {
                                                      }
                                                 destructiveHandler:^(LGAlertView *alertView) {
                                                 }];
        [Utils initAlertButtonColor:alertView];
        [alertView showAnimated:YES completionHandler:nil];
    }
    
    return NO;
}
#endif

#pragma mark - onEvent
-(void)onEvent:(TXEvent *)event eventParams:(id)subscriptionParams {
    
    TXResponseDescriptor * descriptor = [event getEventProperty:TXEvents.Params.DESCRIPTOR];
    TXRequestObj *request = [event getEventProperty:TXEvents.Params.REQUEST];
    
    if([event.name isEqualToString:@"UR114"] || [event.name isEqualToString:@"PP114"]) {
        
        if(descriptor.success == true) {
            
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:VIEWTUTORIAL];
#ifdef _DRIVER_MODE
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:FIRST_START];
#endif
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_LOGOUT object:nil userInfo:nil];
            });
            
            [[TXCallModel instance] onCleanSession];
            [self removeEventListeners];
            
            //[RedisSingleton redisDisConnect];
            
            NSLog(@"Log out");
            //        [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:0] animated:YES];
            
            //TXLauncherVC *firstVC = [[TXLauncherVC alloc] initWithNibName:@"TXLauncherVC" bundle:nil];
            //TXLauncherVC *firstVC = [[TXLauncherVC alloc] initWithFlag:TRUE];
//#if defined (_CHACHA) && defined (_DRIVER_MODE)
//            //TXAboutVC *firstVC = [[TXAboutVC alloc] initWithNibName:@"TXAboutVC" bundle:nil step:11];
//            //TXLauncherVC *firstVC = [[TXLauncherVC alloc] initWithNibName:@"TXLauncherDriverVC" bundle:nil flag:YES];
//            TXLauncherVC *firstVC = [[TXLauncherVC alloc] initWithNibName:@"TXLauncherVC" bundle:nil flag:YES];
//#else
//            TXLauncherVC *firstVC = [[TXLauncherVC alloc] initWithNibName:@"TXLauncherVC" bundle:nil flag:YES];
//#endif
            
            TXLauncherVC *firstVC = [[TXLauncherVC alloc] initWithNibName:@"TXLauncherVC" bundle:nil flag:YES];
            
            //appDelegate.window.rootViewController = firstVC;
            
            [appDelegate resetRootVC:firstVC];
            //self.window.rootViewController = self.frostedViewController;
            //appDelegate.window.rootViewController = appDelegate.frostedViewController;
            
            //[super popToRootViewControllerAnimated:YES];
            
        } else {
            TXLauncherVC *firstVC = [[TXLauncherVC alloc] initWithFlag:TRUE];
            [appDelegate resetRootVC:firstVC];
//            [self alertError:@"Error" message:@"Failed to get profile image"];
        }
        [self hideBusyIndicator];
    }
    else if([event.name isEqualToString:@"UR200"] || [event.name isEqualToString:@"PP200"]) {
        
        if(descriptor.success == true) {
            NSString     *response = [[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding];
            NSDictionary *result   = getJSONObj(response);
            
            NSDictionary *agree = nil;
            if ([event.name isEqualToString:@"UR200"]) {
                agree = [result objectForKey:@"user_setting"];
            }
            else {
                agree = [result objectForKey:@"provider_setting"];
            }
            
            if ([Utils isDictionary:agree]) {
                //
                UISwitch *sw1 = [self.view viewWithTag:301];
                UISwitch *sw2 = [self.view viewWithTag:302];
                UISwitch *sw3 = [self.view viewWithTag:303];
                
                NSString *email_ = [NSString stringWithFormat:@"%@",agree[@"email"]];
                NSString *sms_ = [NSString stringWithFormat:@"%@",agree[@"sms"]];
                NSString *push_ = [NSString stringWithFormat:@"%@",agree[@"push"]];
                
                if ([email_ isEqualToString:@"1"]) {
                    agree_email = @"1";
                    [sw1 setOn:YES];
                }
                else {
                    agree_email = @"0";
                    [sw1 setOn:NO];
                }
                if ([sms_ isEqualToString:@"1"]) {
                    agree_sms = @"1";
                    [sw2 setOn:YES];
                }
                else {
                    agree_sms = @"0";
                    [sw2 setOn:NO];
                }
                if ([push_ isEqualToString:@"1"]) {
                    agree_push = @"1";
                    [sw3 setOn:YES];
                }
                else {
                    agree_push = @"0";
                    [sw3 setOn:NO];
                }
            }
            else {
                agree_sms = @"0";
                agree_email = @"0";
                agree_push = @"0";
            }
            
        } else {
        }
        [self hideBusyIndicator];
    }
    else if([event.name isEqualToString:@"UR211"] || [event.name isEqualToString:@"PP211"]) {
        
        if(descriptor.success == true) {
            //NSString     *response = [[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding];
            //NSDictionary *result   = getJSONObj(response);
            
            NSString *alert_msg;
            //NSInteger idx1 = swIndex/10;
            NSInteger idx2 = swIndex%10;
//            alert_msg = [NSString stringWithFormat:[NSString stringWithFormat:@"Menu.Settings.Driver.title4.Alert.agree%ld",idx2],
//                         [Utils StringFormattedDate:nil format:LocalizedStr(@"Menu.Settings.Driver.title4.Alert.date")],
//                         [NSString stringWithFormat:@"Menu.Settings.Driver.title4.Alert.agree%ld",idx1]
//                         ];
            NSString *lc = [NSString stringWithFormat:@"Menu.Settings.Driver.title4.Alert.agree%ld",(long)idx2];
            alert_msg = [NSString stringWithFormat:LocalizedStr(lc),
                         [Utils StringFormattedDate:nil format:LocalizedStr(@"Menu.Settings.Driver.title4.Alert.date")]
                         ];
            
            LGAlertView *alertView = [[LGAlertView alloc] initWithTitle:@""
                                                                message:alert_msg
                                                                  style:LGAlertViewStyleAlert
                                                           buttonTitles:@[LocalizedStr(@"Button.OK")]
                                                      cancelButtonTitle:nil
                                                 destructiveButtonTitle:nil
                                                          actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                                                          }
                                                          cancelHandler:^(LGAlertView *alertView) {
                                                          }
                                                     destructiveHandler:^(LGAlertView *alertView) {
                                                     }];
            [Utils initAlertButtonColor:alertView];
            [alertView showAnimated:YES completionHandler:nil];
        } else {
        }
        [self hideBusyIndicator];
    }
}

#pragma mark - UITableView DataSource Methods
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [tableView setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [tableView setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
    UIView *seperatorView = [[UIView alloc] initWithFrame:CGRectMake(10, cell.frame.size.height - 1, cell.frame.size.width - 10*2, 1)];
    seperatorView.backgroundColor = UIColorTableSeperator;
    [cell addSubview:seperatorView];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    // This will create a "invisible" footer
    return 0.01f;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [UIView new];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [dataArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    cell.tag = indexPath.row;
    int tag = (int)indexPath.row+100;
    //[self changeCell:tableView withIndexPath:indexPath andTag:tag];
    [self addCell:tableView withCell:cell withIndexPath:indexPath andTag:tag];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    cell.textLabel.text = [NSString stringWithFormat:@"      %@",[dataArray objectAtIndex:indexPath.row]];
    
    
    //    if([[tableView indexPathsForSelectedRows] containsObject:indexPath]) {
    //        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    //    } else {
    //        cell.accessoryType = UITableViewCellAccessoryNone;
    //    }
    
    return cell;
}

#pragma mark - UITableView Delegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self changeCell:tableView withIndexPath:indexPath andTag:((int)indexPath.row+100)];
    
    //cell.accessoryView.hidden = NO;
}
- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self changeCell:tableView withIndexPath:indexPath andTag:((int)indexPath.row+100)];
    
    //cell.accessoryView.hidden = YES;
}

-(void)addCell:(UITableView*)tableView withCell:(UITableViewCell*)cell withIndexPath:(NSIndexPath*)indexPath andTag:(int)tag{
    
    UIImageView *preView = [cell.contentView viewWithTag:tag];
    [preView removeFromSuperview];
    
    UILabel *preLabel = [cell.contentView viewWithTag:(tag+100)];
    [preLabel removeFromSuperview];
    
    NSInteger _h = cell.frame.size.height;
    NSInteger _x = 16;
    UIImageView* imv = [[UIImageView alloc]initWithFrame:CGRectMake(cell.frame.size.width - _x*4 ,_h/2-_x/2, _x, _x)];
    imv.tag = tag;
    UIImage *cellImage;
    
    if([[tableView indexPathsForSelectedRows] containsObject:indexPath]) {
        cellImage = [UIImage imageNamed:@"form_radio_chk"];
    }
    else {
        cellImage = [UIImage imageNamed:@"form_radio_normal"];
    }
    imv.image=cellImage;
    [cell.contentView addSubview:imv];
    /*
     UILabel *mylabel = [[UILabel alloc] initWithFrame:CGRectMake(20,5, 150, 40)];
     mylabel.tag = (tag+100);
     mylabel.text = [dataArray objectAtIndex:(tag-100)];
     [cell.contentView addSubview:mylabel];
     */
    //    UILabel *cellLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, _h/2-_x/2, 120, 35)];
    //    cellLabel.tag = (int)tag+100;
    //    [cellLabel setText:[dataArray objectAtIndex:(tag-100)]];
    //    //[cellLabel setFont:[UIFont boldSystemFontOfSize:12]];
    //    [cellLabel setBackgroundColor:[UIColor clearColor]];
    //
    //    [cell addSubview:cellLabel];
}

-(void)changeCell:(UITableView*)tableView withIndexPath:(NSIndexPath*)indexPath andTag:(int)tag{
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    UIImageView *preView = [cell.contentView viewWithTag:tag];
    [preView removeFromSuperview];
    
    UILabel *preLabel = [cell.contentView viewWithTag:(tag+100)];
    [preLabel removeFromSuperview];
    
    NSInteger _h = cell.frame.size.height;
    NSInteger _x = 16;
    UIImageView* imv = [[UIImageView alloc]initWithFrame:CGRectMake(cell.frame.size.width - _x*4 ,_h/2-_x/2, _x, _x)];
    //    UIImageView* imv = [[UIImageView alloc]initWithFrame:CGRectMake(0,_h/2-_x/2, _x, _x)];
    imv.tag = tag;
    UIImage *cellImage;
    
    if([[tableView indexPathsForSelectedRows] containsObject:indexPath]) {
        cellImage = [UIImage imageNamed:@"form_radio_chk"];
    }
    else {
        cellImage = [UIImage imageNamed:@"form_radio_normal"];
    }
    imv.image=cellImage;
    [cell.contentView addSubview:imv];
    /*
     UILabel *mylabel = [[UILabel alloc] initWithFrame:CGRectMake(20,5, 150, 40)];
     mylabel.tag = (tag+100);
     mylabel.text = [dataArray objectAtIndex:(tag-100)];
     [cell.contentView addSubview:mylabel];
     */
    //    UILabel *cellLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, _h/2-_x/2, 120, 35)];
    //    cellLabel.tag = (int)tag+100;
    //    [cellLabel setText:[dataArray objectAtIndex:(tag-100)]];
    //    //[cellLabel setFont:[UIFont boldSystemFontOfSize:12]];
    //    [cellLabel setBackgroundColor:[UIColor clearColor]];
    //
    //    [cell addSubview:cellLabel];
}

@end
