//
//  TXProfileVC.m
//  Taxi
//

//

#import "TXAccountVC.h"
#import "LGAlertView.h"
#import "TXSignInVC.h"
//#import "PECropViewController.h"
#import "TOCropViewController.h"

#import "Validator.h"
#import "WToast.h"
//#import "CRToast.h"

@import UITextView_Placeholder;

@interface TXAccountVC () <UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIActionSheetDelegate,
TOCropViewControllerDelegate, UITextViewDelegate,
#ifdef _WITZM
UITableViewDelegate, UITableViewDataSource,
#elif defined _CHACHA
#endif
ValidatorDelegate>{
    int userId;
    NSArray *_msgList;

#ifdef _WITZM
    NSArray *dataArray;
#elif defined _CHACHA
#endif

    NSString *newPwd;
    id observer1;
    id observer2;
    id observer3;
}

@property (nonatomic, assign) TOCropViewCroppingStyle croppingStyle; //The cropping style
@property (strong, nonatomic) LGAlertView *securityAlertView;
@end

@implementation TXAccountVC {
    NSMutableDictionary *propertyMap;
    int _alertIdx;
}


-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    
    if(self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        self->_alertIdx = 1;
    }
    
    return self;
}

-(void)viewDidLoad {
    [super viewDidLoad];
    
    observer1 = [[NSNotificationCenter defaultCenter] addObserverForName:@"NotificationPhotoAlbum"
                                                      object:nil
                                                       queue:[NSOperationQueue mainQueue]
                                                  usingBlock:^(NSNotification *notification) {
                                                      
                                                      [self openPhotoAlbum];
                                                      
                                                  }];
    observer2 = [[NSNotificationCenter defaultCenter] addObserverForName:@"NotificationShowCamera"
                                                      object:nil
                                                       queue:[NSOperationQueue mainQueue]
                                                  usingBlock:^(NSNotification *notification) {
                                                      
                                                      [self showCamera];
                                                      
                                                  }];
//    observer3 = [[NSNotificationCenter defaultCenter] addObserverForName:@"NotificationEditPhoto"
//                                                      object:nil
//                                                       queue:[NSOperationQueue mainQueue]
//                                                  usingBlock:^(NSNotification *notification) {
//                                                      
//                                                      NSLog(@"notification:%@",notification);
//                                                      [self editPhoto];
//                                                      
//                                                  }];

}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    NSString *userTel = [[[TXApp instance] getSettings] getUserTelno];
    ((UILabel *)[self.vbody viewWithTag:232]).text = userTel;
}


-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    //[self removeEventListeners];

}

#pragma mark - Configure

-(void)configureStyles {
    [super configureStyles];
    
    if (appDelegate.isDriverMode) {
        [super navigationType01X:[UIImage imageNamed:@"btn_back"] centerText:LocalizedStr(@"Menu.Account.title")];
    }
    else {
        [super navigationType01X:[UIImage imageNamed:@"btn_back"] centerText:LocalizedStr(@"Menu.Account.title")];
    }
    
//    [super navigationType01X:[UIImage imageNamed:@"btn_back"] centerText:@"Account"];
}

-(void)configure {
    [super configure];
    [super configureAccount];
#ifdef _WITZM
    [self configureConfig];
#elif defined _CHACHA
#endif
    
    UIView *n = [super navigationView];
    NSInteger _y = n.frame.origin.y + n.frame.size.height;
    
    self->userId = [[[[self->model getApp] getSettings] getUserId] intValue];
    
    UIButton *button = [[super navigationView] viewWithTag:9999];
    _imgProfile = [[super navigationView] viewWithTag:9998];
    [button addTarget:self action:@selector(onPhoto:) forControlEvents:UIControlEventTouchUpInside];
    
    newPwd = @"";
    //_imgGuide.backgroundColor = UIColorBasicBack;
    
    _imgProfile.layer.cornerRadius = _imgProfile.frame.size.width / 2;
    _imgProfile.clipsToBounds = YES;
    _imgProfile.contentMode = UIViewContentModeScaleAspectFill;
    _imgProfile.layer.masksToBounds = YES;
    //_imgProfile.alpha = 0.7;
    
    //----------------------------------------------------------------------------------------------------------------------------------------------------------------
    ((UILabel *)[self.vbody viewWithTag:101]).text = LocalizedStr(@"Menu.Account.List1.title");
    ((UILabel *)[self.vbody viewWithTag:201]).text = LocalizedStr(@"Menu.Account.List2.title");
    ((UILabel *)[self.vbody viewWithTag:211]).text = LocalizedStr(@"Menu.Account.List2.text1");
    ((UILabel *)[self.vbody viewWithTag:221]).text = LocalizedStr(@"Menu.Account.List2.text2");
    ((UILabel *)[self.vbody viewWithTag:231]).text = LocalizedStr(@"Menu.Account.List2.text3");
    ((UILabel *)[self.vbody viewWithTag:241]).text = LocalizedStr(@"Menu.Account.List2.text4");
    ((UILabel *)[self.vbody viewWithTag:251]).text = LocalizedStr(@"Menu.Account.List2.text5");
#ifdef _WITZM
    ((UILabel *)[self.vbody viewWithTag:261]).text = LocalizedStr(@"Menu.Account.List2.text6");
    
    ((UILabel *)[self.vbody viewWithTag:301]).text = LocalizedStr(@"Menu.Account.List3.title");
    
    ((UILabel *)[self.vbody viewWithTag:311]).text = LocalizedStr(@"Menu.Account.List3.text1");
    ((UILabel *)[self.vbody viewWithTag:321]).text = LocalizedStr(@"Menu.Account.List3.text2");
    ((UILabel *)[self.vbody viewWithTag:331]).text = LocalizedStr(@"Menu.Account.List3.text3");
    ((UILabel *)[self.vbody viewWithTag:341]).text = LocalizedStr(@"Menu.Account.List3.text4");
#endif
    
    //----------------------------------------------------------------------------------------------------------------------------------------------------------------
    self.vbody.backgroundColor = UIColorBasicBack;    
    self.vbody.frame = CGRectMake(self.vbody.frame.origin.x, self.vbody.frame.origin.y, self.view.frame.size.width, self.vbody.frame.size.height);
    self.myScrollView.frame = CGRectMake(0,
                                         _y,
                                         self.view.frame.size.width,
                                         self.view.frame.size.height - _y);
    
    self.myScrollView.contentSize = CGSizeMake(self.vbody.frame.size.width, self.vbody.frame.size.height);
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0, 0.0, 0, 0.0);
    self.myScrollView.contentInset = contentInsets;
    self.myScrollView.scrollIndicatorInsets = contentInsets;
    self.myScrollView.scrollsToTop = YES;
    [self.myScrollView addSubview:self.vbody];
    
    // 2. init event
    _msgList = @[
                 @[@"UR200",@""], // rider profile
                 @[@"PP200",@""], // driver profile
                 
                 @[@"UR601",@""], // rider profile
                 @[@"PP602",@""], // driver profile
                 
                 @[@"UR210",@""], // rider update
                 @[@"PP210",@""], // driver update
                 
                 @[@"UR621",@""], // rider profile update

                 ];
    
    [self registerEventListeners];
    
    propertyMap = [[NSMutableDictionary alloc] init];
    
    if (appDelegate.isDriverMode) {
        // 3. call api
        [self->model PP200];
        
        NSDictionary *user = [appDelegate.dicDriver objectForKey:@"provider_device"];
        if ([user[@"picture"] intValue]) {
            [self->model PP602];
        }
    }
    else {
        // 3. call api
        [self->model UR200];
        
        NSDictionary *user = [appDelegate.dicRider objectForKey:@"user_device"];
        if ([user[@"picture"] intValue]) {
            [self->model UR601];
        }
    }
}
#ifdef _WITZM
-(void) configureConfig {
    
    dataArray = @[LocalizedStr(@"Menu.Account.List3.input.text1"),
                  LocalizedStr(@"Menu.Account.List3.input.text2"),
                  LocalizedStr(@"Menu.Account.List3.input.text3"),
                  LocalizedStr(@"Menu.Account.List3.input.text4")];
}
#endif
-(void) configureDriver:(NSDictionary*)user {
    
    
    // 4. settup
    ((UILabel *)[self.vbody viewWithTag:212]).text = [Utils nameToString:[user objectForKey:@"name"]];
    ((UILabel *)[self.vbody viewWithTag:222]).text = user[@"uid"] ;
    ((UILabel *)[self.vbody viewWithTag:232]).text = user[@"utelno"];
    
    ((UILabel *)[self.vbody viewWithTag:252]).text = [Utils nullToString:[user objectForKey:@"profile"]];
    //((UITextField *)[self.vbody viewWithTag:252]).placeholder = LocalizedStr(@"Menu.Account.List2.text5.input.placeholder1");
    
#ifdef _WITZM
    int i = 0;
    int idx = 312;
    for(NSString *lang in user[@"languages"]) {
        ((UILabel *)[self.vbody viewWithTag:(idx+i*10)]).text = lang;
        i++;
        NSLog(@"idx:%d",idx);
    }
#endif
    if (appDelegate.imgProfileDriver == nil) {
        [self shadowProfileImage:[UIImage imageNamed:@"ic_user_white"]];
    }
    else {
        [self shadowProfileImage:appDelegate.imgProfileDriver];
    }
}

-(void) configureRider:(NSDictionary*)user {
    
    // 4. settup
    ((UILabel *)[self.vbody viewWithTag:212]).text = [Utils nameToString:[user objectForKey:@"name"]];
    ((UILabel *)[self.vbody viewWithTag:222]).text = user[@"uid"] ;
    ((UILabel *)[self.vbody viewWithTag:232]).text = user[@"utelno"];
    
    ((UILabel *)[self.vbody viewWithTag:252]).text = [Utils nullToString:[user objectForKey:@"profile"]];
    //((UITextField *)[self.vbody viewWithTag:252]).placeholder = LocalizedStr(@"Menu.Account.List2.text5.input.placeholder1");
    
    NSString *prefer = @"";
    for(NSDictionary *match in appDelegate.dicRider[@"user_matchs"]) {
        if ([match[@"name"] isEqualToString:@"tag"]) {
            for(NSString *lang in match[@"values"]) {
                prefer = [NSString stringWithFormat:@"%@  %@",prefer, lang];
            }
        }
    }
#ifdef _WITZM
    // vprefer
    ((UILabel *)[self.vbody viewWithTag:262]).text = [Utils trimStringOnly:prefer];
    
    int i = 0;
    int idx = 312;
    for(NSString *lang in user[@"languages"]) {
        ((UILabel *)[self.vbody viewWithTag:(idx+i*10)]).text = lang;
        i++;
    }
#endif
    if (appDelegate.imgProfileRider == nil) {
        [self shadowProfileImage:[UIImage imageNamed:@"ic_user_white"]];
    }
    else {
        [self shadowProfileImage:appDelegate.imgProfileRider];
    }
}

-(void) registerEventListeners {
    for (NSArray *msg in _msgList) {
        [self->model addEventListener:self forEvent:[msg objectAtIndex:0] eventParams:nil];
    }
}

-(void) removeEventListeners {
    for (NSArray *msg in _msgList) {
        [self->model removeEventListener:self forEvent:[msg objectAtIndex:0]];
    }
    [[NSNotificationCenter defaultCenter] removeObserver:observer1];
    observer1 = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:observer2];
    observer2 = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:observer3];
    observer3 = nil;
    
//    [[NSNotificationCenter defaultCenter] removeObserver:self
//                                                    name:@"NotificationPhotoAlbum"
//                                                  object:nil];
//    [[NSNotificationCenter defaultCenter] removeObserver:self
//                                                    name:@"NotificationShowCamera"
//                                                  object:nil];
//    [[NSNotificationCenter defaultCenter] removeObserver:self
//                                                    name:@"NotificationEditPhoto"
//                                                  object:nil];
}

#pragma mark - Action
-(IBAction)onNaviButtonClick:(id)sender {
    UIButton *btn = (UIButton*)sender;
    NSLog(@"onNaviButtonClick");
    //[super onNaviButtonClick:sender];
    
    if (btn.tag == 1100) {
        [self removeEventListeners];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark - IBAction
-(IBAction)onPhoto:(id)sender {
    
    NSArray *items = @[LocalizedStr(@"Menu.Driver.Profile.Image.album"), LocalizedStr(@"Menu.Driver.Profile.Image.edit")];
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        items = @[LocalizedStr(@"Menu.Driver.Profile.Image.album"), LocalizedStr(@"Menu.Driver.Profile.Image.camera"), LocalizedStr(@"Menu.Driver.Profile.Image.edit")];
    }

    LGAlertView *alertView = [[LGAlertView alloc] initWithTitle:nil
                                                        message:LocalizedStr(@"Menu.Driver.Profile.Image.title2")
                                                          style:LGAlertViewStyleActionSheet
                                                   buttonTitles:items
                                              cancelButtonTitle:LocalizedStr(@"Button.CANCEL")
                                         destructiveButtonTitle:nil
                                                  actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                                                      NSLog(@"actionHandler, %@, %lu", title, (long unsigned)index);
                                                      
                                                      if (index == 0) {
                                                          [self openPhotoAlbum];
                                                      }
                                                      else if (index == 1) {
                                                          [self showCamera];
                                                      }
                                                      else if (index == 2) {
                                                          if (self.imgProfile.image) {
                                                              TOCropViewController *cropController = [[TOCropViewController alloc] initWithCroppingStyle:self.croppingStyle image:self.imgProfile.image];
                                                              cropController.delegate = self;
                                                              [self presentViewController:cropController animated:YES completion:nil];
                                                          }
                                                      }
                                                  }
                                                  cancelHandler:^(LGAlertView *alertView) {
                                                      NSLog(@"cancelHandler");
                                                  }
                                             destructiveHandler:^(LGAlertView *alertView) {
                                                 NSLog(@"destructiveHandler");
                                             }];
    [Utils initAlertButtonColor:alertView];
    [alertView showAnimated:YES completionHandler:nil];

}

-(IBAction)onClick:(id)sender {
    
    UIButton *btn = (UIButton*)sender;
    
    NSInteger tag = btn.tag - 1;
    
    UILabel *tf = (UILabel*)[self.view viewWithTag:tag];
    
    newPwd = @"";
    if (btn.tag == 213) {
        self->_alertIdx = 1;
        LGAlertView *alertView = [[LGAlertView alloc] initWithTextFieldsAndTitle:LocalizedStr(@"Menu.Account.List2.text1")
                                                                         message:nil
                                                              numberOfTextFields:1
                                                          textFieldsSetupHandler:^(UITextField *textField, NSUInteger index)
                                  {
                                      if (index == 0) {
                                          textField.placeholder = @"";
                                          textField.text = @"";
//                                          if ([tf.text length]) {
//                                              //textField.text = tf.text;
//                                          }
                                      }
                                      
                                      textField.tag = index;
                                      //textField.delegate = self;
                                      textField.enablesReturnKeyAutomatically = YES;
                                      textField.autocapitalizationType = NO;
                                      textField.autocorrectionType = NO;
                                      textField.clearButtonMode = UITextFieldViewModeNever;
                                      textField.textAlignment = NSTextAlignmentCenter;
                                  }
                                                                    buttonTitles:@[LocalizedStr(@"Button.OK")]
                                                               cancelButtonTitle:LocalizedStr(@"Button.CANCEL")
                                                          destructiveButtonTitle:nil
                                                                   actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                                                                       UITextField *secondTextField = alertView.textFieldsArray[index];
                                                                       //promoCode = secondTextField.text;
                                                                       
                                                                       [self resignKeyboard];
                                                                       
                                                                       Validator *validator = [[Validator alloc] init];
                                                                       validator.delegate   = self;
                                                                       
                                                                       // 이름
                                                                       [validator putRule:[Rules minLength:2 withFailureString:LocalizedStr(@"Validator.Name.text") forTextField:secondTextField]];
                                                                       //[validator putRule:[Rules checkIfAlphaNumericWithFailureString:LocalizedStr(@"Validator.Name.text") forTextField:secondTextField]];
                                                                       
                                                                       if (![validator validateForResult]) {
                                                                           [alertView.firstButton setSelected:NO];
                                                                           return;
                                                                       }
                                                                       
                                                                       NSLog(@"actionHandler, %@, %lu, %@", title, (long unsigned)index, secondTextField.text);
                                                                       //propertyMap[@"user"][@"uid"] = secondTextField.text;
                                                                       [self onUR210:@{@"name":secondTextField.text}];
                                                                       
                                                                       [alertView dismissAnimated:YES completionHandler:nil];
                                                                       
                                                                   }
                                                                   cancelHandler:^(LGAlertView *alertView) {
                                                                       [alertView dismissAnimated:YES completionHandler:nil];
                                                                       
                                                                   }
                                                              destructiveHandler:^(LGAlertView *alertView) {
                                                                  [alertView dismissAnimated:YES completionHandler:nil];
                                                              }];
        //[alertView setButtonAtIndex:0 enabled:NO];
        [Utils initAlertButtonColor:alertView];
        [alertView showAnimated:YES completionHandler:nil];
        [alertView setDismissOnAction:NO];
        
    }
    else if (btn.tag == 223) {
        TOAST_MSG(@"Menu.Account.List.none.edit");
    }
    else if (btn.tag == 233) {
        TOAST_MSG(@"Menu.Account.List.none.edit");
//        TXSignInVC *signInVC = [[TXSignInVC alloc] initWithNibName:@"TXSignInVC" bundle:nil mode:3];
//        signInVC.view.frame = [super view].frame;
//        [self pushViewController:signInVC];
    }
    else if (btn.tag == 243) {
        NSString *datoken = [[[TXApp instance] getSettings] getUserToken];
        TXSignInVC *signInVC = [[TXSignInVC alloc] initWithNibName:@"TXSignInVC" bundle:nil mode:4];
        appDelegate.customShareDic = @{@"datoken":datoken};
        signInVC.view.frame = [super view].frame;
        [self pushViewController:signInVC];
    }
    else if (btn.tag == 24311111) {
        self->_alertIdx = 3;
        
        _securityAlertView = [[LGAlertView alloc] initWithTextFieldsAndTitle:LocalizedStr(@"Menu.Account.List2.text4")
                                                                     message:nil
                                                          numberOfTextFields:3
                                                      textFieldsSetupHandler:^(UITextField *textField, NSUInteger index)
                              {
                                  if (index == 0){
                                      textField.placeholder = LocalizedStr(@"Menu.Account.List2.text4.input.placeholder1");
                                      textField.secureTextEntry = YES;
                                  }
                                  else if (index == 1)
                                  {
                                      textField.placeholder = LocalizedStr(@"Menu.Account.List2.text4.input.placeholder2");
                                      textField.secureTextEntry = YES;
                                  }
                                  else if (index == 2)
                                  {
                                      textField.placeholder = LocalizedStr(@"Menu.Account.List2.text4.input.placeholder3");
                                      textField.secureTextEntry = YES;
                                  }
                                  
                                  textField.tag = index;
                                  textField.delegate = self;
                                  textField.enablesReturnKeyAutomatically = YES;
                                  textField.autocapitalizationType = NO;
                                  textField.autocorrectionType = NO;
                                  textField.clearButtonMode = UITextFieldViewModeNever;
                                  textField.textAlignment = NSTextAlignmentCenter;
                              }
                                                                buttonTitles:@[LocalizedStr(@"Button.OK")]
                                                           cancelButtonTitle:LocalizedStr(@"Button.CANCEL")
                                                      destructiveButtonTitle:nil
                                                               actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                                                                   //UITextField *secondTextField = _securityAlertView.textFieldsArray[index];
                                                                   //NSLog(@"actionHandler, %@, %lu", secondTextField.text, (long unsigned)index);
//                                                                   secondTextField = _securityAlertView.textFieldsArray[0];
//                                                                   NSLog(@"actionHandler, %@, %lu", secondTextField.text, (long unsigned)index);
//                                                                   secondTextField = _securityAlertView.textFieldsArray[1];
//                                                                   NSLog(@"actionHandler, %@, %lu", secondTextField.text, (long unsigned)index);
//                                                                   secondTextField = _securityAlertView.textFieldsArray[2];
//                                                                   NSLog(@"actionHandler, %@, %lu", secondTextField.text, (long unsigned)index);
                                                                   
                                                                   
                                                                   UITextField *textField1 = _securityAlertView.textFieldsArray[0];
                                                                   UITextField *textField2 = _securityAlertView.textFieldsArray[1];
                                                                   UITextField *textField3 = _securityAlertView.textFieldsArray[2];
                                                                   
                                                                   NSString *tf1 = textField1.text;
                                                                   NSString *tf2 = textField2.text;
                                                                   NSString *tf3 = textField3.text;
                                                                   
                                                                   [self resignKeyboard];
                                                                   
                                                                   Validator *validator = [[Validator alloc] init];
                                                                   validator.delegate   = self;
                                                                   
                                                                   // 비밀번호
                                                                   [validator putRule:[Rules minLength:6 withFailureString:LocalizedStr(@"Validator.Passwd.text") forTextField:textField1]];
                                                                   [validator putRule:[Rules minLength:6 withFailureString:LocalizedStr(@"Validator.Passwd.text") forTextField:textField2]];
                                                                   [validator putRule:[Rules minLength:6 withFailureString:LocalizedStr(@"Validator.Passwd.text") forTextField:textField3]];
                                                                   
                                                                   if (![validator validateForResult]) {
                                                                       [alertView.firstButton setSelected:NO];
                                                                       return;
                                                                   }
                                                                   
                                                                   propertyMap[@"user"][@"passwd"] = tf1;
                                                                   
                                                                   NSLog(@"actionHandler1, %@,", tf1);
                                                                   NSLog(@"actionHandler2, %@,", tf2);
                                                                   NSLog(@"actionHandler3, %@,", tf3);
                                                                   
                                                                   if (![tf2 isEqualToString:tf3]) {
                                                                       [Utils onErrorAlert:LocalizedStr(@"Menu.Account.List2.text4.input.result3")];
                                                                       [alertView.firstButton setSelected:NO];
                                                                       return;
                                                                       
//                                                                       LGAlertView *alertView2 = [[LGAlertView alloc] initWithTitle:nil
//                                                                                                   message:LocalizedStr(@"Menu.Account.List2.text4.input.result3")
//                                                                                                     style:LGAlertViewStyleAlert
//                                                                                              buttonTitles:@[LocalizedStr(@"Button.OK")]
//                                                                                         cancelButtonTitle:nil
//                                                                                    destructiveButtonTitle:nil
//                                                                                             actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
//                                                                                                 //NSLog(@"actionHandler, %@, %lu", title, (long unsigned)index);
//                                                                                                 
//                                                                                                 
//                                                                                             }
//                                                                                             cancelHandler:^(LGAlertView *alertView) {
//                                                                                             }
//                                                                                        destructiveHandler:^(LGAlertView *alertView) {
//                                                                                        }];
//                                                                       [Utils initAlertButtonColor:alertView2];
//                                                                       [alertView2 showAnimated:YES completionHandler:nil];
                                                                       
                                                                   }
                                                                   else {
                                                                       NSString *pwd = [[[TXApp instance] getSettings] getPassword];
                                                                       NSString *oldpwd = [Utils passcodeBysha256:tf1];
                                                                       if ([oldpwd isEqualToString:pwd]){
                                                                           newPwd = [Utils passcodeBysha256:tf3];
                                                                           //[self onUR210:@{@"pwd":[Utils passcodeBysha256:textField1.text],@"newpwd":[Utils passcodeBysha256:textField3.text]}];
                                                                           [self onUR210:@{@"pwd":newPwd}];
                                                                       }
                                                                       else {
                                                                           [Utils onErrorAlert:LocalizedStr(@"Menu.Account.List2.text4.input.result1")];
                                                                           [alertView.firstButton setSelected:NO];
                                                                           return;
//                                                                           
//                                                                           LGAlertView *alertView2 = [[LGAlertView alloc] initWithTitle:@""
//                                                                                                       message:LocalizedStr(@"Menu.Account.List2.text4.input.result1")
//                                                                                                         style:LGAlertViewStyleAlert
//                                                                                                  buttonTitles:@[LocalizedStr(@"Button.OK")]
//                                                                                             cancelButtonTitle:nil
//                                                                                        destructiveButtonTitle:nil
//                                                                                                 actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
//                                                                                                 }
//                                                                                                 cancelHandler:^(LGAlertView *alertView) {
//                                                                                                 }
//                                                                                            destructiveHandler:^(LGAlertView *alertView) {
//                                                                                            }];
//                                                                           [Utils initAlertButtonColor:alertView2];
//                                                                           [alertView2 showAnimated:YES completionHandler:nil];
                                                                           
                                                                       }
                                                                   }
                                                                   //propertyMap[@"user"][@"newpasswd"] = textField3.text;
                                                                   
                                                                   [alertView dismissAnimated:YES completionHandler:nil];
                                                                   
                                                               }
                                                               cancelHandler:^(LGAlertView *alertView) {
                                                                   [alertView dismissAnimated:YES completionHandler:nil];
                                                               }
                                                          destructiveHandler:^(LGAlertView *alertView) {
                                                              [alertView dismissAnimated:YES completionHandler:nil];
                                                          }];
        [Utils initAlertButtonColor:_securityAlertView];
        [_securityAlertView setButtonAtIndex:0 enabled:NO];
        [_securityAlertView showAnimated:YES completionHandler:nil];
        [_securityAlertView setDismissOnAction:NO];
        
    }
    else if (btn.tag == 253) {
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(20.0f, 20.0f, 280.0f, 190.0f)];
        
        UIView *line1 = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0, 320.0f, 1)];
        line1.backgroundColor = [UIColor colorWithWhite:0.97 alpha:1.f];
        
        CGRect textViewFrame = CGRectMake(10.0f, 1.0f, 260.0f, 180.0f);
        UITextView *textView = [[UITextView alloc] initWithFrame:textViewFrame];
        //textView.returnKeyType = UIReturnKeyDone;
        //textView.backgroundColor = [UIColor clearColor];
        [textView setFont:[UIFont systemFontOfSize:14]];
        textView.userInteractionEnabled = YES;
        textView.placeholder = LocalizedStr(@"Menu.Account.List2.text5.input.placeholder1");
        textView.placeholderColor = [UIColor lightGrayColor]; // optional
        
        textView.delegate = self; //텍스트 뷰 델리게이트
        
        if ([tf.text length]) {
            textView.text = tf.text;
        }
        [view addSubview:line1];
        [view addSubview:textView];
        
        LGAlertView *alertView = [[LGAlertView alloc] initWithViewAndTitle:LocalizedStr(@"Menu.Account.List2.text5")
                                                                   message:LocalizedStr(@"Menu.Account.List2.text5.input.title2")
                                                                     style:LGAlertViewStyleAlert
                                                                      view:view
                                                              buttonTitles:@[LocalizedStr(@"Button.OK")]
                                                         cancelButtonTitle:LocalizedStr(@"Button.CANCEL")
                                                    destructiveButtonTitle:nil
                                                             actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                                                                 //NSLog(@"actionHandler, %@, %lu", title, (long unsigned)index);
                                                                 //NSLog(@"text:%@",textView.text);
                                                                 //tf.text = textView.text;
                                                                 
                                                                 //[propertyMap setValue:textView.text forKey:@"profile"];
                                                                 [self onUR210:@{@"profile":textView.text}];
                                                             }
                                                             cancelHandler:^(LGAlertView *alertView) {
                                                             }
                                                        destructiveHandler:^(LGAlertView *alertView) {
                                                        }];
        
        //alertView.heightMax = 256.f;
        [Utils initAlertButtonColor:alertView];
        alertView.windowLevel = LGAlertViewWindowLevelAboveStatusBar;
        //alertView.separatorsColor = [UIColor redColor];
        [alertView showAnimated:YES completionHandler:^(void)
         {
             [textView becomeFirstResponder];
         }];
    }
    else if (btn.tag == 263) {
//        NSString *datoken = [[[TXApp instance] getSettings] getUserToken];
//        TXSignInVC *signInVC = [[TXSignInVC alloc] initWithNibName:@"TXSignInVC" bundle:nil mode:4];
//        appDelegate.customShareDic = @{@"datoken":datoken};
//        signInVC.view.frame = [super view].frame;
//        [self pushViewController:signInVC];
    }
#ifdef _WITZM
    else if (btn.tag == 313 || btn.tag == 323 || btn.tag == 333 || btn.tag == 343) {
        
        UITableView* mTableView = [[UITableView alloc] init];
        mTableView.delegate=self;
        mTableView.dataSource=self;
        
        mTableView.allowsMultipleSelection = NO;
        mTableView.frame = CGRectMake(0.f, 0.f, 320, 172.f);
        mTableView.contentMode = UIViewContentModeScaleAspectFit;
        mTableView.scrollEnabled = NO;
        
        NSIndexPath *indexPath = nil;
        if ([tf.text isEqualToString:LocalizedStr(@"Menu.Account.List3.input.text1")]) {
            indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        }
        else if ([tf.text isEqualToString:LocalizedStr(@"Menu.Account.List3.input.text2")]) {
            indexPath = [NSIndexPath indexPathForRow:1 inSection:0];
        }
        else if ([tf.text isEqualToString:LocalizedStr(@"Menu.Account.List3.input.text3")]) {
            indexPath = [NSIndexPath indexPathForRow:2 inSection:0];
        }
        else if ([tf.text isEqualToString:LocalizedStr(@"Menu.Account.List3.input.text4")]) {
            indexPath = [NSIndexPath indexPathForRow:3 inSection:0];
        }
        if (indexPath != nil) {
            [mTableView selectRowAtIndexPath:indexPath
                                    animated:YES
                              scrollPosition:UITableViewScrollPositionNone];
            //[mTableView tableView:mTableView didSelectRowAtIndexPath:indexPath];
            [mTableView.delegate tableView:mTableView didSelectRowAtIndexPath:indexPath];
        }
        
        LGAlertView *alertView = [[LGAlertView alloc] initWithViewAndTitle:LocalizedStr(@"Menu.Account.List3.title")
                                                                   message:nil
                                                                     style:LGAlertViewStyleAlert
                                                                      view:mTableView
                                                              buttonTitles:@[LocalizedStr(@"Button.OK")]
                                                         cancelButtonTitle:LocalizedStr(@"Button.CANCEL")
                                                    destructiveButtonTitle:nil
                                                             actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                                                                 NSLog(@"actionHandler, %@, %lu", title, (long unsigned)index);
                                                                 
                                                                 for(NSIndexPath* indexPath in [mTableView indexPathsForSelectedRows]) {
                                                                     NSLog(@"cell.tag:%@",[dataArray objectAtIndex:indexPath.row]);
                                                                     
                                                                     //[propertyMap setValue:dataArray[indexPath.row] forKey:@"gender"];
                                                                     //tf.text = dataArray[indexPath.row];
                                                                     
                                                                     UITextField *tf1 = (UITextField*)[self.view viewWithTag:312];
                                                                     UITextField *tf2 = (UITextField*)[self.view viewWithTag:322];
                                                                     UITextField *tf3 = (UITextField*)[self.view viewWithTag:332];
                                                                     UITextField *tf4 = (UITextField*)[self.view viewWithTag:342];
                                                                     
                                                                     if (btn.tag == 313)
                                                                     {
                                                                         [self onUR210:@{@"languages":@[[dataArray objectAtIndex:indexPath.row],tf2.text,tf3.text,tf4.text]}];
                                                                     }
                                                                     else if(btn.tag == 323)
                                                                     {
                                                                         [self onUR210:@{@"languages":@[tf1.text,[dataArray objectAtIndex:indexPath.row],tf3.text,tf4.text]}];
                                                                     }
                                                                     else if(btn.tag == 333)
                                                                     {
                                                                         [self onUR210:@{@"languages":@[tf1.text,tf2.text,[dataArray objectAtIndex:indexPath.row],tf4.text]}];
                                                                     }
                                                                     else if(btn.tag == 343)
                                                                     {
                                                                         [self onUR210:@{@"languages":@[tf1.text,tf2.text,tf3.text,[dataArray objectAtIndex:indexPath.row]]}];
                                                                     }
                                                                     
                                                                 }
                                                             }
                                                             cancelHandler:^(LGAlertView *alertView) {
                                                                 NSLog(@"cancelHandler");
                                                             }
                                                        destructiveHandler:^(LGAlertView *alertView) {
                                                            NSLog(@"destructiveHandler");
                                                        }];
        //alertView.heightMax = 256.f;
        //alertView.heightMax = 156.f;
        //alertView.cancelButtonBackgroundColor = [UIColor redColor];
        [Utils initAlertButtonColor:alertView];
        alertView.windowLevel = LGAlertViewWindowLevelAboveStatusBar;
        [alertView showAnimated:YES completionHandler:nil];
        
    }
#endif
}

#pragma mark - Send Event
-(void)onUR210:(NSDictionary*)_propertyMap {
    
    NSString* useq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.USEQD];
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:useq, @"useq", nil];
    
    NSDictionary *user = [appDelegate.dicRider objectForKey:@"user"];
    dic[@"uid"] = user[@"uid"];
    //dic[@"utelno"] = user[@"utelno"];
    [dic addEntriesFromDictionary:_propertyMap];
    
    [self showBusyIndicator:@"Updating info ... "];
    [self->model requsetAPI:API_FUNC(NSStringFromSelector(_cmd)) property:dic];
}

-(void)onUR211:(UIImage*)image {
    
    NSString* useq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.USEQD];
    
    NSString* picture = [Utils encodeToBase64String:image];
    
    NSDictionary *_propertyMap = @{
                                  @"useq" : useq,
                                  @"picture" : picture,
                                  };
    
    [self showBusyIndicator:@"Update Picture ... "];
    [self->model requsetAPI:@"UR621" property:_propertyMap];
}
/*
-(void)onPP211:(UIImage*)image {
    
    NSString* pseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.PSEQD];
    
    NSString* picture = [Utils encodeToBase64String:image];
    
    NSDictionary *_propertyMap = @{
                                  @"pseq" : pseq,
                                  @"picture" : picture,
                                  };
    
    [self showBusyIndicator:@"Update Picture ... "];
    [self->model requsetAPI:@"PP622" property:_propertyMap];
}
*/
#pragma mark - onEvent
-(void)onEvent:(TXEvent *)event eventParams:(id)subscriptionParams {
    
    TXResponseDescriptor * descriptor = [event getEventProperty:TXEvents.Params.DESCRIPTOR];
    TXRequestObj *request = [event getEventProperty:TXEvents.Params.REQUEST];
    
    if([event.name isEqualToString:@"UR200"]) {
        
        if(descriptor.success == true) {
            NSString     *response = [[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding];
            NSDictionary *result   = getJSONObj(response);
            
            NSDictionary *user = [result objectForKey:@"user"];
            
            if ([user[@"picture"] intValue]) {
                [self->model UR601];
            }
            [self configureRider:user];
            // 1. 정보 업데이트
            NSMutableDictionary *dic = [appDelegate.dicRider mutableCopy];
            [dic setObject:[result valueForKey:@"user"] forKey:@"user"];
            appDelegate.dicRider = dic;
        } else {
            //에러주석[self alertError:@"Error" message:descriptor.error];
        }
    }
    else if([event.name isEqualToString:@"PP200"]) {
        
        if(descriptor.success == true) {
            NSString     *response = [[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding];
            NSDictionary *result   = getJSONObj(response);
            
            NSDictionary *user = [result objectForKey:@"provider"];
            if ([user[@"picture"] intValue]) {
                [self->model PP602];
            }
            [self configureDriver:user];
            // 1. 정보 업데이트
            NSMutableDictionary *dic = [appDelegate.dicDriver mutableCopy];
            [dic setObject:[result valueForKey:@"provider"] forKey:@"provider"];
            appDelegate.dicRider = dic;
        } else {
            //에러주석[self alertError:@"Error" message:descriptor.error];
        }
    }
    else if([event.name isEqualToString:@"UR601"] || [event.name isEqualToString:@"PP602"]) {
        
        if(descriptor.success == true) {
            NSString     *response = [[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding];
            NSDictionary *result   = getJSONObj(response);
            
            
            NSString *pictureData              = [result valueForKey:@"picture"];
            
            if ([pictureData length]) {
                
//                NSData *data = [[NSData alloc] initWithBase64EncodedString:pictureData options:0];
//                UIImage *image = [[UIImage alloc] initWithData:data];
                UIImage *image = [Utils decodeBase64ToImage:pictureData];
                // Store image
                //[appDelegate.cache storeObject:event forKey:request.reqUrl];
                
                _imgProfile.alpha = 1.0;
                [self saveProfileImage:image withCode:event.name];
            }
            else {
                [self saveProfileImage:[UIImage imageNamed:@"ic_user_white"] withCode:event.name];
            }
            
        } else {
            [self saveProfileImage:[UIImage imageNamed:@"ic_user_white"] withCode:event.name];
        }
    }
    else if([event.name isEqualToString:@"UR210"] || [event.name isEqualToString:@"PP210"]) {
        NSString     *response = [[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding];
        NSDictionary *result   = getJSONObj(response);
        
        if(descriptor.success == true) {
            
            if (appDelegate.isDriverMode) {
                [self configureDriver:[result valueForKey:@"provider"]];
                // 1. 정보 업데이트
                NSMutableDictionary *dic = [appDelegate.dicDriver mutableCopy];
                [dic setObject:[result valueForKey:@"provider"] forKey:@"provider"];
                appDelegate.dicDriver = dic;
            }
            else {
                [self configureRider:[result valueForKey:@"user"]];
                // 1. 정보 업데이트
                NSMutableDictionary *dic = [appDelegate.dicRider mutableCopy];
                [dic setObject:[result valueForKey:@"user"] forKey:@"user"];
                appDelegate.dicRider = dic;
            }
            
            if (![newPwd isEqualToString:@""]) {
                [[[TXApp instance] getSettings] setPassword:newPwd];
                newPwd = @"";
                [self alertError:@"" message:LocalizedStr(@"Menu.Account.Passwd.result.text")];
            }
            
        } else {
            //에러주석[self alertError:@"Error" message:descriptor.error];
        }
    }
    else if([event.name isEqualToString:@"UR621"] || [event.name isEqualToString:@"PP622"]) {
        
        if(descriptor.success == true) {
            NSString     *response = [[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding];
            NSDictionary *result   = getJSONObj(response);
            
            
            NSString *pictureData              = [result valueForKey:@"picture"];
            
            if ([pictureData length]) {
                
                //                NSData *data = [[NSData alloc] initWithBase64EncodedString:pictureData options:0];
                //                UIImage *image = [[UIImage alloc] initWithData:data];
                UIImage *image = [Utils decodeBase64ToImage:pictureData];
                // Store image
                //[appDelegate.cache storeObject:event forKey:request.reqUrl];
                
                NSInteger imageSize = [Utils getImageSize:image];
                
                if (appDelegate.isDriverMode) {
                    appDelegate.pictureSizeDriver = [NSString stringWithFormat:@"%d",(int)imageSize];
                    if ([appDelegate.pictureSizeDriver isEqualToString:@""]) {
                        appDelegate.pictureSizeDriver = nil;
                    }
                    NSMutableDictionary *p = [appDelegate.dicDriver mutableCopy];
                    NSMutableDictionary *user = [[p objectForKey:@"provider_device"] mutableCopy];
                    [user setObject:[NSNumber numberWithInteger:imageSize] forKey:@"picture"];
                    [p setObject:user forKey:@"provider_device"];
                    [appDelegate setDicDriver:p];
                }
                else {
                    appDelegate.pictureSizeRider = [NSString stringWithFormat:@"%d",(int)imageSize];
                    if ([appDelegate.pictureSizeRider isEqualToString:@""]) {
                        appDelegate.pictureSizeRider = nil;
                    }
                    NSMutableDictionary *p = [appDelegate.dicRider mutableCopy];
                    NSMutableDictionary *user = [[p objectForKey:@"user_device"] mutableCopy];
                    [user setObject:[NSNumber numberWithInteger:imageSize] forKey:@"picture"];
                    [p setObject:user forKey:@"user_device"];
                    [appDelegate setDicRider:p];
                }
                _imgProfile.alpha = 1.0;
                [self saveProfileImage:image withCode:event.name];
            }
            else {
                [self saveProfileImage:[UIImage imageNamed:@"ic_user_white"] withCode:event.name];
            }
            
        } else {
            [self saveProfileImage:[UIImage imageNamed:@"ic_user_white"] withCode:event.name];
        }
    }
    [self hideBusyIndicator];
}


-(void)saveProfileImage:(UIImage*)image withCode:(NSString*)code{
    
    // 운전자 모드일때
    if (appDelegate.isDriverMode) {
        // rider 요청일때 1301, driver요청일때 1101
        if ([code isEqualToString:@"UR601"] || [code isEqualToString:@"UR621"]) {
            [appDelegate setImgProfileRider:image];
        }
        else {
            [appDelegate setImgProfileDriver:image];
        }
    }
    else {
        // driver 요청일때 1301, rider요청일때 1101
        if ([code isEqualToString:@"PP602"] || [code isEqualToString:@"PP622"]) {
            [appDelegate setImgProfileDriver:image];
        }
        else {
            [appDelegate setImgProfileRider:image];
        }
    }
    
    [self shadowProfileImage:image];
}

-(UIImage *)makeRoundedImage:(UIImage *) image
                      radius: (float) radius;
{
    CALayer *imageLayer = [CALayer layer];
    imageLayer.frame = CGRectMake(0, 0, image.size.width, image.size.height);
    imageLayer.contents = (id) image.CGImage;
    
    imageLayer.masksToBounds = YES;
    imageLayer.cornerRadius = radius;
    
    UIGraphicsBeginImageContext(image.size);
    [imageLayer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *roundedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return roundedImage;
}

-(void)shadowProfileImage:(UIImage*)image {
    _imgProfile.image = image;
    _imgProfile.alpha = 1.0;
}


#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker  dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Image Picker Delegate -
//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    TOCropViewController *cropController = [[TOCropViewController alloc] initWithCroppingStyle:self.croppingStyle image:image];
    cropController.delegate = self;
    
    //If profile picture, push onto the same navigation stack
    if (self.croppingStyle == TOCropViewCroppingStyleCircular) {
        [picker pushViewController:cropController animated:YES];
    }
    else { //otherwise dismiss, and then present from the main controller
        [picker dismissViewControllerAnimated:YES completion:^{
            [self presentViewController:cropController animated:YES completion:nil];
        }];
    }
}

#pragma mark - Cropper Delegate -
- (void)cropViewController:(TOCropViewController *)cropViewController didCropToImage:(UIImage *)image withRect:(CGRect)cropRect angle:(NSInteger)angle
{
    [self updateImageViewWithImage:image fromCropViewController:cropViewController];
}

- (void)cropViewController:(TOCropViewController *)cropViewController didCropToCircularImage:(UIImage *)image withRect:(CGRect)cropRect angle:(NSInteger)angle
{
    [self updateImageViewWithImage:image fromCropViewController:cropViewController];
}

- (void)updateImageViewWithImage:(UIImage *)croppedImage fromCropViewController:(TOCropViewController *)cropViewController
{
    if ([self.imgProfile.image isEqual:croppedImage]) {
        [cropViewController.presentingViewController dismissViewControllerAnimated:YES completion:nil];
        return;
    }

    
    //---------------------------------------------------------------------------------------------------------------------------------------------
    //self.imageView.image = croppedImage;
    //[self shadowProfileImage:croppedImage];
    UIImage *image = [Utils resizeImage:croppedImage];
    
    NSLog(@"%d,%d",(int)[Utils getImageSize:croppedImage],(int)[Utils getImageSize:image] );
    if (appDelegate.isDriverMode) {
        //[self onPP211:image];
    }
    else {
        [self onUR211:image];
    }
    
    [cropViewController.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - Private methods

- (void)showCamera
{
    self.croppingStyle = TOCropViewCroppingStyleDefault;
    
    UIImagePickerController *standardPicker = [[UIImagePickerController alloc] init];
    standardPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    standardPicker.allowsEditing = NO;
    standardPicker.delegate = self;
    [self presentViewController:standardPicker animated:YES completion:nil];

}

- (void)openPhotoAlbum
{
    self.croppingStyle = TOCropViewCroppingStyleDefault;
    
    UIImagePickerController *standardPicker = [[UIImagePickerController alloc] init];
    standardPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    standardPicker.allowsEditing = NO;
    standardPicker.delegate = self;
    [self presentViewController:standardPicker animated:YES completion:nil];

}

#pragma mark - UITextField Delegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (self->_alertIdx == 3) {
        UITextField *secondTextField = _securityAlertView.textFieldsArray[(textField.tag == 1 ? 2 : 1)];
        
        NSMutableString *currentString = textField.text.mutableCopy;
        
        [currentString replaceCharactersInRange:range withString:string];
        
        [_securityAlertView setButtonAtIndex:0 enabled:(currentString.length > 2 && secondTextField.text.length > 2)];
    }
    else {
        UITextField *secondTextField = _securityAlertView.textFieldsArray[(textField.tag == 0 ? 1 : 0)];
        
        NSMutableString *currentString = textField.text.mutableCopy;
        
        [currentString replaceCharactersInRange:range withString:string];
        
        [_securityAlertView setButtonAtIndex:0 enabled:(currentString.length > 2 && secondTextField.text.length > 2)];
    }
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField.tag < 1)
        [_securityAlertView.textFieldsArray[(textField.tag + 1)] becomeFirstResponder];
    else
    {
        if ([_securityAlertView isButtonEnabledAtIndex:0])
            [_securityAlertView dismissAnimated:YES completionHandler:nil];
        else
            [textField resignFirstResponder];
    }
    
    return YES;
}

#pragma mark - UIDatePicker Delegate

//listen to changes in the date picker and just log them
- (void) datePickerDateChanged:(UIDatePicker *)paramDatePicker{
    NSLog(@"Selected date = %@", paramDatePicker.date);
    NSDateFormatter __autoreleasing *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setTimeZone:[NSTimeZone systemTimeZone]];
    //[dateFormat setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    //dateFormat.dateStyle = NSDateFormatterMediumStyle;
    //NSString *dateString =  [dateFormat stringFromDate:paramDatePicker.date];
    
    [dateFormat setDateFormat:@"MM/dd/yyyy"];
    NSString *dateString = [dateFormat stringFromDate:paramDatePicker.date];
    NSLog(@"dateString:%@",dateString);
    
    UITextField *tf = (UITextField*)[self.view viewWithTag:103];
    tf.text = dateString;
    
}

#pragma mark - UITextView Delegate Methods

//캐릭터가 텍스트 뷰에 표시되기 직전 아래 메소드가 호출된다
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    
    
    //newLineCharacterSet이 있으면 done button이 호출됨. 따라서 키보드가 사라짐.
    NSCharacterSet *doneButtonCharacterSet = [NSCharacterSet newlineCharacterSet];
    NSRange replacementTextRange = [text rangeOfCharacterFromSet:doneButtonCharacterSet];
    NSUInteger location = replacementTextRange.location;
    
    //텍스트가 190자가 넘지 않도록 제한
    if (textView.text.length + text.length > 190){
        if (location != NSNotFound){
            [textView resignFirstResponder];
        }
        return NO;
    }
    
    else if (location != NSNotFound){
        if([text isEqualToString:@"\n"]) {
            return YES;
        }
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}

#ifdef _WITZM
#pragma mark - UITableView DataSource Methods
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [tableView setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [tableView setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
    
    UIView *seperatorView = [[UIView alloc] initWithFrame:CGRectMake(0, cell.frame.size.height - 1, cell.frame.size.width, 1)];
    seperatorView.backgroundColor = UIColorTableSeperator;
    [cell addSubview:seperatorView];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    // This will create a "invisible" footer
    return 0.01f;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [UIView new];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [dataArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    cell.tag = indexPath.row;
    int tag = (int)indexPath.row+100;
    //[self changeCell:tableView withIndexPath:indexPath andTag:tag];
    [self addCell:tableView withCell:cell withIndexPath:indexPath andTag:tag];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    cell.textLabel.text = [NSString stringWithFormat:@"      %@",[dataArray objectAtIndex:indexPath.row]];
    
    return cell;
}

#pragma mark - UITableView Delegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self changeCell:tableView withIndexPath:indexPath andTag:((int)indexPath.row+100)];
    
    //cell.accessoryView.hidden = NO;
}
- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self changeCell:tableView withIndexPath:indexPath andTag:((int)indexPath.row+100)];
    
    //cell.accessoryView.hidden = YES;
}

-(void)addCell:(UITableView*)tableView withCell:(UITableViewCell*)cell withIndexPath:(NSIndexPath*)indexPath andTag:(int)tag{
    
    UIImageView *preView = [cell.contentView viewWithTag:tag];
    [preView removeFromSuperview];
    
    UILabel *preLabel = [cell.contentView viewWithTag:(tag+100)];
    [preLabel removeFromSuperview];
    
    NSInteger _h = cell.frame.size.height;
    NSInteger _x = 16;
    UIImageView* imv = [[UIImageView alloc]initWithFrame:CGRectMake(cell.frame.size.width - _x*4 ,_h/2-_x/2, _x, _x)];
    imv.tag = tag;
    UIImage *cellImage;
    
    if([[tableView indexPathsForSelectedRows] containsObject:indexPath]) {
        cellImage = [UIImage imageNamed:@"form_radio_chk"];
    }
    else {
        cellImage = [UIImage imageNamed:@"form_radio_normal"];
    }
    imv.image=cellImage;
    [cell.contentView addSubview:imv];
}

-(void)changeCell:(UITableView*)tableView withIndexPath:(NSIndexPath*)indexPath andTag:(int)tag{
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    UIImageView *preView = [cell.contentView viewWithTag:tag];
    [preView removeFromSuperview];
    
    UILabel *preLabel = [cell.contentView viewWithTag:(tag+100)];
    [preLabel removeFromSuperview];
    
    NSInteger _h = cell.frame.size.height;
    NSInteger _x = 16;
    UIImageView* imv = [[UIImageView alloc]initWithFrame:CGRectMake(cell.frame.size.width - _x*4 ,_h/2-_x/2, _x, _x)];
    //    UIImageView* imv = [[UIImageView alloc]initWithFrame:CGRectMake(0,_h/2-_x/2, _x, _x)];
    imv.tag = tag;
    UIImage *cellImage;
    
    if([[tableView indexPathsForSelectedRows] containsObject:indexPath]) {
        cellImage = [UIImage imageNamed:@"form_radio_chk"];
    }
    else {
        cellImage = [UIImage imageNamed:@"form_radio_normal"];
    }
    imv.image=cellImage;
    [cell.contentView addSubview:imv];
}
#endif

#pragma mark -
#pragma mark Validator
#pragma mark -
-(void) resignKeyboard
{
    [self.view endEditing:YES];
}

- (IBAction)validateAction:(id)sender
{
    
    [self resignKeyboard];
    
    Validator *validator = [[Validator alloc] init];
    validator.delegate   = self;
    
//    if (self.setupMode != 1) {
//        // 이름
//        [validator putRule:[Rules minLength:2 withFailureString:LocalizedStr(@"Validator.Name.text") forTextField:self.txtUsername]];
//        [validator putRule:[Rules checkIfAlphaNumericWithFailureString:LocalizedStr(@"Validator.Name.text") forTextField:self.txtUsername]];
//    }
//    
//    // 이메일
//    [validator putRule:[Rules minLength:8 withFailureString:LocalizedStr(@"Validator.Email.text1") forTextField:self.txtEmail]];
//    [validator putRule:[Rules checkIsValidEmailWithFailureString:LocalizedStr(@"Validator.Email.text2") forTextField:self.txtEmail]];
//    
//    // 전화번호
//    [validator putRule:[Rules checkRange:NSMakeRange(17, 17) withFailureString:LocalizedStr(@"Validator.Telno.text") forTextField:self.txtPhoneNum]];
//    [validator putRule:[Rules checkValidCountryCodeUSWithFailureString:LocalizedStr(@"Validator.Telno.text") forTextField:self.txtPhoneNum]];
//    
//    // 비번
//    [validator putRule:[Rules minLength:6 withFailureString:LocalizedStr(@"Validator.Passwd.text") forTextField:self.txtPassword]];
//    
//    if (validatorIndex != 1) {
//        // 인증코드
//        [validator putRule:[Rules minLength:6 withFailureString:LocalizedStr(@"Validator.Otp.text") forTextField:self.txtVerifyCode]];
//        [validator putRule:[Rules checkIfNumericWithFailureString:LocalizedStr(@"Validator.Otp.text") forTextField:self.txtVerifyCode]];
//    }
    
    [validator validateForResult];
}

#pragma ValidatorDelegate - Delegate methods

- (void) preValidation
{
}

- (void)onSuccess
{
}

- (void)onFailure:(Rule *)failedRule
{
    
//    NSMutableDictionary *options = [@{kCRToastNotificationTypeKey               : @(CRToastTypeNavigationBar),
//                                      kCRToastNotificationPresentationTypeKey   : @(CRToastPresentationTypeCover),
//                                      kCRToastUnderStatusBarKey                 : @(YES),
//                                      kCRToastTextKey                           : failedRule.failureMessage,
//                                      kCRToastFontKey                           : [UIFont systemFontOfSize:14],
//                                      kCRToastTextAlignmentKey                  : @(NSTextAlignmentCenter),
//                                      kCRToastTimeIntervalKey                   : @(2),
//                                      kCRToastAnimationInTypeKey                : @(CRToastAnimationTypeGravity),
//                                      kCRToastAnimationOutTypeKey               : @(CRToastAnimationTypeGravity),
//                                      kCRToastAnimationInDirectionKey           : @(CRToastAnimationDirectionTop),
//                                      kCRToastAnimationOutDirectionKey          : @(CRToastAnimationDirectionTop),
//                                      kCRToastNotificationPreferredPaddingKey   : @(15)} mutableCopy];
//    [CRToastManager showNotificationWithOptions:options
//                                completionBlock:^{
//                                    NSLog(@"Completed");
//                                }];
    
    [WToast showWithText:failedRule.failureMessage duration:kWTShort roundedCorners:NO gravity:kWTGravityTop];
}


@end
