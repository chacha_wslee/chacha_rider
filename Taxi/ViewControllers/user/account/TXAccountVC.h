//
//  TXProfileVC.h
//  Taxi
//

//

#import "TXUserVC.h"

@interface TXAccountVC : TXUserVC {

}

@property (strong, nonatomic) IBOutlet UIScrollView *myScrollView;
@property (strong, nonatomic) IBOutlet UIView *vbody;

@property (nonatomic, strong) IBOutlet UIView* imgGuide;

@property (nonatomic, strong) IBOutlet UIImageView* imgProfile;

@property (nonatomic, strong) IBOutlet UILabel* tfName;
@property (nonatomic, strong) IBOutlet UILabel* tfEmail;
@property (nonatomic, strong) IBOutlet UILabel* tfTelno;
@property (nonatomic, strong) IBOutlet UILabel* tfPasswd;

@property (nonatomic, strong) IBOutlet UILabel* tfProfile;

@property (nonatomic, strong) IBOutlet TXButton* btnPhoto;

@end
