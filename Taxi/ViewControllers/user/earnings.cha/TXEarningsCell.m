//
//  TXPaymentCell.m
//  TXPaymentCell
//

//

#import "TXEarningsCell.h"
#import "utils.h"
#import "utilities.h"

@implementation TXEarningsCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
    }
    return self;
}

- (void)bindData:(NSDictionary*)dic
{
    // color
    //---------------------------------------------------------------------------------------------------------
    self.lb1.textColor = UIColorDefault;
    self.lb2.textColor = UIColorLabelText;
    self.lb3.textColor = UIColorLabelText;
    self.lb4.textColor = UIColorLabelText;
    
    self.lbv1.textColor = UIColorLabelText;
    
    // text
    //---------------------------------------------------------------------------------------------------------
    self.lb1.text = [self DateString2String:dic[@"start_date"] format:@"MM'월 'dd'일'"];
    self.lb2.text = LocalizedStr(@"Menu.Driver.Earning.Line2.text");
    self.lb3.text = LocalizedStr(@"Menu.Driver.Earning.Line3.text");
    self.lb4.text = LocalizedStr(@"Menu.Driver.Earning.Line4.text");
    self.lb5.text = LocalizedStr(@"Menu.Driver.Earning.Line5.text");
    
    // value
    //---------------------------------------------------------------------------------------------------------
    self.lbv1.text = @"";
    
    NSInteger payout   = [dic[@"total_payout"] intValue];
    NSInteger sum      = [dic[@"fare_sum"] intValue];
    NSInteger extra    = [dic[@"fare_extra"] intValue];
    NSInteger fee      = [dic[@"fare_fee"] intValue];
    
    NSInteger pay2 = sum+extra;
    NSInteger pay3 = sum+extra-payout-fee;
    NSInteger pay4 = fee;
    NSInteger pay5 = payout;
    
    
    NSString *p2 = [NSString stringWithFormat:@"%ld",(long)pay2];
    NSString *p3 = [NSString stringWithFormat:@"%ld",(long)pay3];
    NSString *p4 = [NSString stringWithFormat:@"%ld",(long)pay4];
    NSString *p5 = [NSString stringWithFormat:@"%ld",(long)pay5];
    
    self.lbv2.text = CURRENCY_FORMAT(p2);
    self.lbv3.text = CURRENCY_FORMAT(p3);
    self.lbv4.text = CURRENCY_FORMAT(p4);
    self.lbv5.text = CURRENCY_FORMAT(p5);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

-(void)stateChanged
{
    
}

-(NSString*)DateString2String:(NSString*)dateStr
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:DATE_FORMAT_YYYYMMDD];
    NSDate *date = [self StringFormat2Date:dateStr];
    return [formatter stringFromDate:date];
}

-(NSString*)DateString2String:(NSString*)dateStr format:(NSString*)format
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:format];
    NSDate *date = [self StringFormat2Date:dateStr];
    return [formatter stringFromDate:date];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
-(NSDate*) StringFormat2Date:(NSString *)dateStr
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:DATE_FORMAT_YYYYMMDDHH24MISS];
    //[formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    return [formatter dateFromString:dateStr];
}

@end
