//
//  TXPaymentCell.h
//  TXPaymentCell
//

//

#import <UIKit/UIKit.h>

@interface TXEarningsCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *lb1;
@property (nonatomic, weak) IBOutlet UILabel *lb2;
@property (nonatomic, weak) IBOutlet UILabel *lb3;
@property (nonatomic, weak) IBOutlet UILabel *lb4;
@property (nonatomic, weak) IBOutlet UILabel *lb5;

@property (nonatomic, weak) IBOutlet UILabel *lbv1;
@property (nonatomic, weak) IBOutlet UILabel *lbv2;
@property (nonatomic, weak) IBOutlet UILabel *lbv3;
@property (nonatomic, weak) IBOutlet UILabel *lbv4;
@property (nonatomic, weak) IBOutlet UILabel *lbv5;

- (void)bindData:(NSDictionary*)dic;
@end
