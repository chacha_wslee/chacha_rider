//
//  MenuViewController.m
//  SlideMenu
//

//

#import "TXOtherTripHistoryVC.h"
#import "TXOtherTripDetailVC.h"
#import "RTLabel.h"
#import "TXTripHistoryCell.h"
#import "LGAlertView.h"

@interface TXOtherTripHistoryVC() {
    NSMutableArray *items;
    NSArray *_msgList;
    
    NSInteger selectIdx;
}

@end

@implementation TXOtherTripHistoryVC {
    NSMutableDictionary *propertyMap;
}
@synthesize cellIdentifier;


-(void)viewDidLoad {
    
    [super viewDidLoad];
    
    selectIdx = -1;
    
    //NSString *_name = [Utils getUserName];

    [super configureBottomList];
    UILabel *label = (UILabel*)[[super navigationView] viewWithTag:1111];
    label.tag = 2001;
    label.text = [NSString stringWithFormat:@"0 %@",LocalizedStr(@"Menu.TripHistory.List1.title")];
    
    UIView *n = [super navigationView];
    NSInteger _y = n.frame.origin.y + n.frame.size.height;
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, _y, self.view.bounds.size.width, self.view.bounds.size.height-_y)
                                                  style:UITableViewStylePlain];
    self.tableView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:self.tableView];
    
    UINib *nib = [UINib nibWithNibName:@"TXTripHistoryCell" bundle:nil];
    self.cellIdentifier = @"TXTripHistoryCell";
    [self.tableView registerNib:nib forCellReuseIdentifier:self.cellIdentifier];
    
    self.tableView.separatorColor = [UIColor clearColor];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.opaque = NO;
    self.tableView.backgroundColor = [UIColor clearColor];
//    self.tableView.tableHeaderView = ({
//        
//        UIView *view = [Utils addRTLabelView:self.view underView:nil dim:0];
//        
//        RTLabel *label = [view viewWithTag:11];
//        NSString *htmlString = [NSString stringWithFormat:@"%@ %@",
//                                FONT_STR_COLOR(@"0"),
//                                FONT_STR_TEXT(LocalizedStr(@"Menu.TripHistory.List1.title"))];
//        
//        label.text = htmlString;
//        [label setFont:[UIFont systemFontOfSize:11.0f]];
//        //label.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
//        label.tag = 2001;
//        [Utils RTLabelReSize:view tag:label.tag];
//        
////        label.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
//        
//        view;
//    });
    
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    self.tableView.scrollsToTop = YES;
    
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    if (selectIdx>-1) {
        //
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:selectIdx inSection:0];
        TXTripHistoryCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
        [Utils newBadge:cell.btnProfile show:NO];
        
        selectIdx = -1;
    }
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self removeEventListeners];
}

#pragma mark - Configure

-(void)configureStyles {
    [super configureStyles];
    
    if (appDelegate.isDriverMode) {
        [super navigationType01X:[UIImage imageNamed:@"btn_back"] centerText:LocalizedStr(@"Menu.TripHistory.title")];
    }
    else {
        if (self.isShare) {
            [super navigationType01X:[UIImage imageNamed:@"btn_back"] centerText:LocalizedStr(@"Menu.ShareTripHistory.title")];
        }
        else {
            [super navigationType01X:[UIImage imageNamed:@"btn_back"] centerText:LocalizedStr(@"Menu.TripHistory.title")];
        }
    }
}

-(void)configure {
    [super configure];
    
    // 2. init event
    if (self.isShare) {
        // 친구 이력 정보
        _msgList = @[
                     @[@"UD302",@""], // rider
                     ];
    }
    else {
        _msgList = @[
                     @[@"UD301",@""], // rider
                     @[@"PD301",@""], // driver
                     ];
    }
    
    [self registerEventListeners];
    
    propertyMap = [[NSMutableDictionary alloc] init];
    
    if (appDelegate.isDriverMode) {
        [self configureDriver];
    }
    else {
        [self configureRider];
    }
    
}

-(void) configureDriver {
    // 3. call api
    [self showBusyIndicator:@"Loading info ... "];
    [self->model PD301:@""];
}

-(void) configureRider {
    // 3. call api
    [self showBusyIndicator:@"Loading info ... "];
    
    if (self.isShare) {
        [self->model UD302:@""];
    }
    else {
        [self->model UD301:@""];
    }
}

-(void) registerEventListeners {
    for (NSArray *msg in _msgList) {
        [self->model addEventListener:self forEvent:[msg objectAtIndex:0] eventParams:nil];
    }
}

-(void) removeEventListeners {
    for (NSArray *msg in _msgList) {
        [self->model removeEventListener:self forEvent:[msg objectAtIndex:0]];
    }
}

#pragma mark - Geo
-(CLLocationCoordinate2D) getLocation:(NSString*) location{
    NSArray* stringComponents = [location componentsSeparatedByString:@","];
    
    CLLocationCoordinate2D position;
    position.latitude  = [[NSDecimalNumber decimalNumberWithString:[stringComponents objectAtIndex:0]] doubleValue];
    position.longitude = [[NSDecimalNumber decimalNumberWithString:[stringComponents objectAtIndex:1]] doubleValue];
    
    return position;
}

-(void)getAddress:(NSString*)pos index:(NSIndexPath*)indexPath addr:(NSString*)addr
{
    CLLocationCoordinate2D coor = [self getLocation:pos];
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    [locMgr geocodeAPICall:coor
                completion:^(NSDictionary *user, NSString* params, NSString *str, int status) {
                    
                    if (status==1) {
                        //NSAssert(![NSThread isMainThread], @"This shouldn't be called from the main thread");
                        //NSLog(@"responseDict:%@",user);
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            // Update the UI
                            NSString *__address = user[@"fullAddress"];
                            TXTripHistoryCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
                            if ([addr isEqualToString:@"src_addr"]) {
                                cell.lb6.text = __address;
                            }
                            else {
                                cell.lb7.text = __address;
                            }
                            
                            self->items[indexPath.item][addr] = __address;
                        });
                    }
                }];
}

#pragma mark - Badge
-(void)updateRightBadge:(UIButton*)btnBadge
{
    UIButton *btn = (UIButton*)btnBadge;
    NSDictionary *dict = self->items[btn.tag];
    NSDictionary *pdevice = nil;
    NSString* oseq = @"";
    
    pdevice = [dict valueForKey:@"drive_order"];
    if ([Utils isDictionary:pdevice]) {
        oseq = [pdevice valueForKey:SettingsConst.CryptoKeys.OSEQ];
    }
    pdevice = [dict valueForKey:@"drive_pickup"];
    if ([Utils isDictionary:pdevice]) {
        oseq = [pdevice valueForKey:SettingsConst.CryptoKeys.OSEQ];
    }
    pdevice = [dict valueForKey:@"drive_trip"];
    if ([Utils isDictionary:pdevice]) {
        oseq = [pdevice valueForKey:SettingsConst.CryptoKeys.OSEQ];
    }
    pdevice = [dict valueForKey:@"drive_charge"];
    if ([Utils isDictionary:pdevice]) {
        oseq = [pdevice valueForKey:SettingsConst.CryptoKeys.OSEQ];
    }
    
    BOOL flag = [Utils showAlrim:CODE_TYPE_SHARE_TRIP code:oseq];
    
    [Utils newBadge:btnBadge show:flag];
}

#pragma mark - Action
-(IBAction)onNaviButtonClick:(id)sender {
    UIButton *btn = (UIButton*)sender;
    NSLog(@"onNaviButtonClick");
    //[super onNaviButtonClick:sender];
    
    if (btn.tag == 1100) {
        [self removeEventListeners];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark - UITableView Delegate & Datasrouce -
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [tableView setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [tableView setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
    
//    UIView *seperatorView = [[UIView alloc] initWithFrame:CGRectMake(0, cell.frame.size.height - 1, cell.frame.size.width, 1)];
    UIView *seperatorView = [[UIView alloc] initWithFrame:CGRectMake(10, cell.frame.size.height - 1, cell.frame.size.width - 10*2, 1)];
    seperatorView.backgroundColor = UIColorBasicBack;
    [cell addSubview:seperatorView];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    //return 112;
    
    return 96;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return self->items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	TXTripHistoryCell *cell = [tableView dequeueReusableCellWithIdentifier:self.cellIdentifier];
    //NSLog(@"indexPath.row:%ld",indexPath.row);
    NSDictionary *dic           = self->items[indexPath.row];
    NSDictionary* drive_trip    = nil;
    NSDictionary* drive_pickup  = nil;
    NSDictionary *pMap          = nil;
    NSString *api               = @"";
    
    pMap = [cell bindData:dic type:YES];
    CGRect rect = cell.frame;
    rect.size.height = 61;
    cell.frame = rect;
    
    cell.btnProfile.tag = indexPath.row;
    [self updateRightBadge:cell.btnProfile];
    if (appDelegate.isDriverMode) {
        api = @"PP601";
    }
    else {
        api = @"UR601";
    }
    
    drive_trip    = [dic valueForKey:@"drive_trip"]; //trip_duration/trip_distance, begin_location, end_location, update_date
    drive_pickup  = [dic valueForKey:@"drive_pickup"]; //trip_duration/trip_distance, begin_location, end_location, update_date
    
    cell.addrLine.hidden = YES;
    cell.lb6.hidden = YES;
    cell.lb7.hidden = YES;

    if (![[dic objectForKey:@"picImage"] isEqual:[NSNull null]]) {
        cell.profile.image = [dic objectForKey:@"picImage"];
        cell.profile.tag = 1;
    }
    else {
        cell.profile.image = [UIImage imageNamed:@"ic_user"];
        //dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            //Background Thread
            
            [Utils downloadImageWithURL:api property:pMap completionBlock:^(BOOL succeeded, UIImage *image) {
                if (succeeded) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^(void){
                        //Run UI Updates
                        // change the image in the cell
                        cell.profile.image = image;
                        cell.profile.tag = 1;
                        [Utils setCircleImage:cell.profile];
                    });
                    
                    // cache the image for use later (when scrolling up)
                    NSMutableDictionary *newDict = [[NSMutableDictionary alloc] init];
                    [newDict addEntriesFromDictionary:dic];
                    [newDict setObject:image forKey:@"picImage"];
                    [self->items replaceObjectAtIndex:indexPath.row withObject:newDict];
                }
            }];
            
        //});
    }
    
    
    if (![[Utils nullToString:appDelegate.shareOseq] isEqualToString:@""]) {
        NSString *oseq = @"";
        NSString *src = @"";
        NSString *dst = @"";
        
        if ([Utils isDictionary:drive_trip]) {
            oseq = drive_trip[@"oseq"];
            
            src = drive_trip[@"begin_location"];
            dst = drive_trip[@"end_location"];
        }
        else if ([Utils isDictionary:drive_pickup]) {
            oseq = drive_pickup[@"oseq"];
            
            src = drive_pickup[@"lsrc"];
            dst = drive_pickup[@"ldst"];
        }
        
        if (![oseq isEqualToString:@""] && [appDelegate.shareOseq isEqualToString:oseq]) {
//            NSString *src = cell.lb6.text;
//            NSString *dst = cell.lb7.text;
            NSDictionary *pos = @{
                                  @"begin_address" : src,
                                  @"end_address" : dst
                                  };
            
            NSDictionary *dic           = self->items[indexPath.row];
            NSMutableDictionary *trip = [NSMutableDictionary dictionaryWithDictionary:pos];
            [trip addEntriesFromDictionary:dic];
            
            TXOtherTripDetailVC *vc = [[TXOtherTripDetailVC alloc] initWithNibName:@"TXOtherTripDetailVC" bundle:[NSBundle mainBundle]];
            vc.tripData = (NSDictionary*)trip;
            if (self.isShare) {
                vc.oseq = oseq;
            }
            [self.navigationController pushViewController:vc animated:YES];
            appDelegate.shareOseq = @"";
        }
    }
    
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    TXTripHistoryCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    // 이미지가 존재하지 않으면
    if (cell.profile.tag != 1) {
        [self alertError:LocalizedStr(@"String.Alert") message:LocalizedStr(@"String.Loading.Address")];
        return;
    }
    
    NSDictionary *dic = self->items[indexPath.row];
    NSString *src = cell.lb6.text;
    NSString *dst = cell.lb7.text;
    src = @"";
    dst = @"";
    NSDictionary *pos = @{
                          @"begin_address" : src,
                          @"end_address" : dst
                          };
    
//    if ([src isEqualToString:@""]) {
//        LGAlertView *alertView = [[LGAlertView alloc] initWithTitle:@""
//                                    message:LocalizedStr(@"String.Loading.Address")
//                                      style:LGAlertViewStyleAlert
//                               buttonTitles:@[LocalizedStr(@"Button.OK")]
//                          cancelButtonTitle:nil
//                     destructiveButtonTitle:nil
//                              actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
//                                  NSLog(@"actionHandler, %@, %lu", title, (long unsigned)index);
//                                  
//                              }
//                              cancelHandler:^(LGAlertView *alertView) {
//                              }
//                         destructiveHandler:^(LGAlertView *alertView) {
//                         }];
//        [Utils initAlertButtonColor:alertView];
//        [alertView showAnimated:YES completionHandler:nil];
//        
//        return;
//    }
    NSMutableDictionary *trip = [NSMutableDictionary dictionaryWithDictionary:pos];
    [trip addEntriesFromDictionary:dic];
    //NSDictionary* drive_trip    = [dic valueForKey:@"drive_trip"]; //trip_duration/trip_distance, begin_location, end_location, update_date
    
    selectIdx = indexPath.row;
    
    TXOtherTripDetailVC *vc = [[TXOtherTripDetailVC alloc] initWithNibName:@"TXOtherTripDetailVC" bundle:[NSBundle mainBundle]];
    vc.tripData = (NSDictionary*)trip;
    if (self.isShare) {
        vc.oseq = dic[@"drive_trip"][@"oseq"];
    }
    [self.navigationController pushViewController:vc animated:YES];
}


#pragma mark - onEvent
-(void)onEvent:(TXEvent *)event eventParams:(id)subscriptionParams {
    
    TXResponseDescriptor * descriptor = [event getEventProperty:TXEvents.Params.DESCRIPTOR];
    TXRequestObj *request = [event getEventProperty:TXEvents.Params.REQUEST];
    
    if([event.name isEqualToString:@"UD301"] || [event.name isEqualToString:@"UD302"]) {
        
        if(descriptor.success == true) {
            NSString     *response = [[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding];
            NSDictionary *result   = getJSONObj(response);
            
            
            NSArray *tripData              = [result valueForKey:@"drive_historys"];
            
            self->items = [NSMutableArray arrayWithCapacity:[tripData count]];
            
            for (NSDictionary* dic in tripData) {
                if ([Utils isDictionary:[dic valueForKey:@"drive_driver"]]) {
                    NSMutableDictionary *newDict = [[NSMutableDictionary alloc] init];
                    [newDict addEntriesFromDictionary:dic];
                    [newDict setObject:[NSNull null] forKey:@"picImage"];
                    [newDict setObject:@"" forKey:@"src_addr"];
                    [newDict setObject:@"" forKey:@"dst_addr"];
                    [items addObject:newDict];
                }
            }
            
            NSString *count = [NSString stringWithFormat:@"%d",(int)[tripData count]];
            UILabel *label = (UILabel*)[[super navigationView] viewWithTag:2001];
//            NSString *htmlString = [NSString stringWithFormat:@"%@ %@",
//                                    FONT_STR_COLOR(count),
//                                    FONT_STR_TEXT(LocalizedStr(@"Menu.TripHistory.List1.title"))];
            if (self.isShare) {
                label.text = [NSString stringWithFormat:@"%@ %@",count,LocalizedStr(@"Menu.ShareTripHistory.List1.title")];
                
//                htmlString = [NSString stringWithFormat:@"%@ %@",
//                              FONT_STR_COLOR(count),
//                              FONT_STR_TEXT(LocalizedStr(@"Menu.ShareTripHistory.List1.title"))];
            }
            else {
                label.text = [NSString stringWithFormat:@"%@ %@",count,LocalizedStr(@"Menu.TripHistory.List1.title")];
            }
//            ((RTLabel *)[self.view viewWithTag:2001]).text = htmlString;
            
            if ([self->items count]) {
                self.tableView.separatorColor = [UIColor colorWithRed:150/255.0f green:161/255.0f blue:177/255.0f alpha:1.0f];
                [self.tableView reloadData];
            }
            
        } else {
            //에러주석[self alertError:@"Error" message:descriptor.error];
        }
    }
    else if([event.name isEqualToString:@"PD301"]) {
        
        if(descriptor.success == true) {
            NSString     *response = [[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding];
            NSDictionary *result   = getJSONObj(response);
            
            NSArray *tripData              = [result valueForKey:@"drive_historys"];
            
            self->items = [NSMutableArray arrayWithCapacity:[tripData count]];
            
            for (NSDictionary* dic in tripData) {
//                [dic setObject:@"" forKey:@"picImage"];
//                [items addObject:dic];
                if ([Utils isDictionary:[dic valueForKey:@"drive_rider"]]) {
                    NSMutableDictionary *newDict = [[NSMutableDictionary alloc] init];
                    [newDict addEntriesFromDictionary:dic];
                    [newDict setObject:[NSNull null] forKey:@"picImage"];
                    [newDict setObject:@"" forKey:@"src_addr"];
                    [newDict setObject:@"" forKey:@"dst_addr"];
                    [items addObject:newDict];
                }
            }
            
            NSString *count = [NSString stringWithFormat:@"%d",(int)[tripData count]];
            UILabel *label = (UILabel*)[[super navigationView] viewWithTag:2001];
            label.text = [NSString stringWithFormat:@"%@ %@",count,LocalizedStr(@"Menu.ShareTripHistory.List1.title")];
//            NSString *htmlString = [NSString stringWithFormat:@"%@ %@",
//                                    FONT_STR_COLOR(count),
//                                    FONT_STR_TEXT(LocalizedStr(@"Menu.TripHistory.List1.title"))];
//            ((RTLabel *)[self.view viewWithTag:2001]).text = htmlString;
            
            self.tableView.separatorColor = [UIColor colorWithRed:150/255.0f green:161/255.0f blue:177/255.0f alpha:1.0f];
            [self.tableView reloadData];
            
        } else {
            //에러주석[self alertError:@"Error" message:descriptor.error];
        }
    }
    [self hideBusyIndicator];
}

/*
//- (void)downloadImageWithURL:(NSString *)code property:(NSDictionary*)pMap completionBlock:(void (^)(BOOL succeeded, UIImage *image))completionBlock
{
    TXRequestObj *request     = [self->model createRequest:code];
    
    request.body = getJSONStr(pMap);
    NSMutableURLRequest *httpRequest = [request createHTTPRequest];
    DLogI(@"Sent Async Request to URL - %@\n\n", request.reqUrl);
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:httpRequest
                                            completionHandler:
                                  ^(NSData *data, NSURLResponse *response, NSError *error) {
                                      if ([response isKindOfClass:[NSHTTPURLResponse class]] &&
                                          ((NSHTTPURLResponse *)response).statusCode == 200 &&
                                          error == nil &&
                                          data != nil) {
                                          if ([data length]) {
                                              
                                              UIImage *image = [UIImage imageWithData:data];
                                              completionBlock(YES,image);
                                          }
                                          else {
                                              completionBlock(YES,[UIImage imageNamed:@"ic_user_white"]);
                                          }
                                      } else {
                                          completionBlock(NO,nil);
                                      }
                                  }];
    [task resume];
    //
    [NSURLConnection sendAsynchronousRequest:httpRequest
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               if ( !error )
                               {
                                   if ([data length]) {
                                       
                                       UIImage *image = [UIImage imageWithData:data];
                                       completionBlock(YES,image);
                                   }
                                   else {
                                       completionBlock(YES,[UIImage imageNamed:@"ic_user_white"]);
                                   }
                               } else{
                                   completionBlock(NO,nil);
                               }
                           }];
     //
}
*/
@end
