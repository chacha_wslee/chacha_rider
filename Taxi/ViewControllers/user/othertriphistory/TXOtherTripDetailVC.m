//
//  MenuViewController.m
//  SlideMenu
//

//

#import "TXOtherTripDetailVC.h"
#import "TXTripHistoryDetailCell.h"
#import "TXHelpVC.h"
#import "ChatView.h"
#import <QuartzCore/QuartzCore.h>

#define RATE_STR(c) [NSString stringWithFormat:LocalizedStr(@"Menu.TripHistory.Trip.Rate.text"),c]

@interface TXOtherTripDetailVC() <GMSMapViewDelegate> {
    NSMutableArray *items;
    NSArray *_msgList;
    
    UIButton *btnProfile;
    UIImageView *profile;
    UIButton *btnReload;
    UILabel *tridLabel;
    
    BOOL isDestination;
    NSTimer *timer;
    BOOL isFirst;
}


@end

@implementation TXOtherTripDetailVC {
    NSMutableDictionary *propertyMap;
    GMSMapView *_mapView;
    
    NSInteger height_;
}
@synthesize cellIdentifier;
@synthesize tripData;


-(void)viewDidLoad {
    
    [super viewDidLoad];
    
    isFirst = YES;
    timer = nil;
    height_ = 160;
    
    //NSString *_name = [Utils getUserName];
    
    [super configureBottomLine];
    UIView  *view = (UIView*)[[super navigationView] viewWithTag:1500];
    view.hidden = YES;
    UIView *n = [super navigationView];
    NSInteger _y = n.frame.origin.y + n.frame.size.height;
    _y = 20;
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, _y, self.view.bounds.size.width, self.view.bounds.size.height-_y)
                                                  style:UITableViewStylePlain];
    self.tableView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:self.tableView];
    
    UINib *nib = [UINib nibWithNibName:@"TXTripHistoryDetailCell" bundle:nil];
    self.cellIdentifier = @"TXTripHistoryDetailCell";
    [self.tableView registerNib:nib forCellReuseIdentifier:self.cellIdentifier];
    
    self.tableView.separatorColor = [UIColor clearColor];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.opaque = NO;
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.tableHeaderView = ({
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, self.tableView.frame.size.height - height_)];
        view.tag = 2000;
        
        view;
    });
    self.tableView.scrollEnabled = NO;
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    [self initMapView];
    self->items = [NSMutableArray arrayWithCapacity:1];
    [items addObject:self.tripData];
    
    [self.view bringSubviewToFront:n];
    
    // 하단 사진이미지
    profile = [[UIImageView alloc] initWithFrame:CGRectMake(self.view.frame.size.width - self.view.frame.size.width/2 - 72/2,
                                                            self.view.frame.size.height - height_ - 72/2,
                                                            72, 72)];
    profile.tag = 2;
    profile.layer.cornerRadius = profile.frame.size.width / 2;
    profile.clipsToBounds = YES;
    profile.contentMode = UIViewContentModeScaleAspectFill;
    profile.layer.masksToBounds = YES;
    
    [self.view addSubview:profile];
    
    [Utils updateAlrim:CODE_TYPE_SHARE_TRIP code:CODE_ALRIM_SHARE_TRIP(self.oseq) value:CODE_FLAG_OFF];
    
    // TRID
    tridLabel = [[UILabel alloc] initWithFrame:CGRectMake(_mapView.frame.size.width-140-12, kNaviTopHeight+kNaviBottomHeight, 140, 18)];
    //UILabel *tridLabel = [[UILabel alloc] initWithFrame:CGRectMake(_mapView.frame.size.width-140-12, self.view.frame.size.height-18-12 - height_, 140, 18)];
    //tridLabel.text = [NSString stringWithFormat:@"TRID %@%010d",service, [oseq intValue]];
    tridLabel.textColor = HEXCOLOR(0x000000FF);
    tridLabel.textAlignment = NSTextAlignmentCenter;
    tridLabel.backgroundColor = [UIColor whiteColor];
    tridLabel.font = [UIFont systemFontOfSize:14];
    [self.view addSubview:tridLabel];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didBecomeActive) name:UIApplicationDidBecomeActiveNotification object:nil];
    
    // 갱신버튼 누른다.
    [btnReload sendActionsForControlEvents: UIControlEventTouchUpInside];
    
    //NSDictionary* drive_charge  = [items[0] valueForKey:@"drive_charge"]; //trip_duration/trip_distance, begin_location, end_location, update_date
//    NSDictionary* drive_trip    = [self.tripData valueForKey:@"drive_trip"]; //trip_duration/trip_distance, begin_location, end_location, update_date
//    if ([Utils isDictionary:drive_trip]) {
//        if ([drive_trip[@"state"] isEqualToString:@"10"]) {
//            timer = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(timer) userInfo:nil repeats:YES];
//            [[NSRunLoop mainRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
//            btnReload.hidden = NO;
//        }
//    }
}

- (void)viewDidUnload {
    [super viewDidUnload];
    
    [_mapView removeFromSuperview];
    _mapView = nil;
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self removeEventListeners];
    [timer invalidate];
    timer = nil;
}

-(void)dealloc {
    [timer invalidate];
    timer = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)didBecomeActive{
    // 갱신버튼 누른다.
    [btnReload sendActionsForControlEvents: UIControlEventTouchUpInside];
}

-(void)initMapView {
    NSDictionary *drive_trip              = [self.tripData valueForKey:@"drive_trip"];
    NSArray *blocs = [[Utils nullTolocation:[drive_trip objectForKey:@"begin_location"]] componentsSeparatedByString:@","];
    if (![Utils isDictionary:drive_trip]) {
        drive_trip              = [self.tripData valueForKey:@"drive_pickup"];
        blocs = [[Utils nullTolocation:[drive_trip objectForKey:@"lsrc"]] componentsSeparatedByString:@","];
    }
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:[[blocs objectAtIndex:0] doubleValue]
                                                            longitude:[[blocs objectAtIndex:1] doubleValue]
                                                                 zoom:16];
    
    _mapView.hidden = YES;
    _mapView = [GMSMapView mapWithFrame:CGRectMake(0, 0, self.view.bounds.size.width,
                                                   [self.view viewWithTag:2000].frame.size.height) camera:camera];
    
    _mapView.delegate = self;
    
    //self.mapView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    _mapView.autoresizingMask = UIViewAutoresizingFlexibleWidth |
    UIViewAutoresizingFlexibleHeight |
    UIViewAutoresizingFlexibleBottomMargin |
    UIViewAutoresizingFlexibleTopMargin;
    
    UIView *n = [super navigationView];
    NSInteger _y = n.frame.origin.y + n.frame.size.height;
    //top, left, bottom, right
    _mapView.padding = UIEdgeInsetsMake(_y, 0, 0, 0);
    
    //_mapView.settings.compassButton = YES;
    // 모든 제스쳐를 막는다.
#ifdef _DEBUG
    _mapView.settings.compassButton = YES;
#else
    _mapView.settings.compassButton = NO;
    _mapView.settings.scrollGestures = NO;
    _mapView.settings.zoomGestures = NO;
    _mapView.settings.tiltGestures = NO;
    _mapView.settings.rotateGestures = NO;
#endif
    
//    [self.view viewWithTag:5];
    [[self.view viewWithTag:2000] addSubview:_mapView];
    
}

#pragma mark - Configure

-(void)configureStyles {
    [super configureStyles];
    
    if (appDelegate.isDriverMode) {
        [super navigationType01X:[UIImage imageNamed:@"btn_back"] centerText:LocalizedStr(@"Menu.TripHistory.Detail.title")];
    }
    else {
        if (![[Utils nullToString:self.oseq] isEqualToString:@""]) {
            //[super navigationType110:[UIImage imageNamed:@"btn_back"] centerText:LocalizedStr(@"Menu.ShareTripHistory.Detail.title") rightImage:[UIImage imageNamed:@"ic_reload"]];
            [super navigationType01X:[UIImage imageNamed:@"btn_back"] centerText:LocalizedStr(@"Menu.ShareTripHistory.Detail.title")];
        }
        else {
            [super navigationType01X:[UIImage imageNamed:@"btn_back"] centerText:LocalizedStr(@"Menu.TripHistory.Detail.title")];
        }
    }
}

-(void)configure {
    [super configure];
    
    btnReload = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width - 34 - 11, 51, 34, 34)];
    btnReload.tag = 1300;
    [btnReload addTarget:self action:@selector(onNaviButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    btnReload.backgroundColor = [UIColor clearColor];
    [btnReload setImage:[UIImage imageNamed:@"btn_reload"] forState:UIControlStateNormal];
    //[button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btnReload.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
    [[super navigationView] addSubview:btnReload];
    btnReload.hidden = YES;
    
    // 2. init event
    if (![[Utils nullToString:self.oseq] isEqualToString:@""]) {
        _msgList = @[
                     @[@"UD302",@""], // rider
                     ];
    }
    else {
        _msgList = @[
                     @[@"UD301",@""], // rider
                     @[@"PD301",@""], // driver
                     ];
    }
    
    [self registerEventListeners];
    
    propertyMap = [[NSMutableDictionary alloc] init];

    // 아래 갱신버튼으로 대신한다.
//    if (appDelegate.isDriverMode) {
//        [self configureDriver];
//    }
//    else {
//        [self configureRider];
//    }
    
    UIView *n = [super navigationView];
    [n setBackgroundColor:[UIColor clearColor]];
    
    CAGradientLayer *layer = [CAGradientLayer layer];
    layer.frame = n.bounds;
    UIColor *theColor = [UIColor whiteColor];
    layer.colors = [NSArray arrayWithObjects:
                    (id)[theColor CGColor],
                    (id)[[theColor colorWithAlphaComponent:0.9f] CGColor],
                    (id)[[theColor colorWithAlphaComponent:0.6f] CGColor],
                    (id)[[theColor colorWithAlphaComponent:0.5f] CGColor],
                    (id)[[theColor colorWithAlphaComponent:0.3f] CGColor],
                    (id)[[theColor colorWithAlphaComponent:0.1f] CGColor],
                    (id)[[theColor colorWithAlphaComponent:0.0f] CGColor],
                    (id)[[UIColor clearColor] CGColor],
                    (id)[[UIColor clearColor] CGColor],
                    nil];
    
    //layer.colors = [NSArray arrayWithObjects:(id)whiteColor.CGColor, (id)clearColor.CGColor, nil];
    [n.layer insertSublayer:layer atIndex:0];
}

-(void)timer {
    if([UIApplication sharedApplication].applicationState != UIApplicationStateActive) {
        [timer invalidate];
        timer = nil;
        return;
    }
    if (appDelegate.isNetwork) {
        dispatch_async(dispatch_get_main_queue(), ^{
            // 갱신버튼 누른다.
            [btnReload sendActionsForControlEvents: UIControlEventTouchUpInside];
        });
    }
}

- (void) runSpinAnimationOnView:(UIButton*)view duration:(CGFloat)duration rotations:(CGFloat)rotations repeat:(float)repeat
{
    CABasicAnimation* rotationAnimation;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    //rotationAnimation.toValue = [NSNumber numberWithFloat: M_PI * 2.0 /* full rotation*/ * rotations * duration ];
    //rotationAnimation.toValue = [NSNumber numberWithFloat: M_PI / 2.0];
    
    rotationAnimation.fromValue = [NSNumber numberWithFloat: 0.0];
    rotationAnimation.toValue   = [NSNumber numberWithFloat: M_PI * 2.0]; // 2바퀴
    
    rotationAnimation.duration = duration;
    rotationAnimation.cumulative = YES;
    rotationAnimation.repeatCount = repeat;
    
    [view.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
}

-(void) configureDriver {

    //NSDictionary *user = [appDelegate.dicDriver objectForKey:@"provider"];
    
    NSDictionary* drive_trip    = [self.tripData valueForKey:@"drive_trip"];
    
    [self showBusyIndicator:@"Loading info ... "];
    // 3. call api
    if ([Utils isDictionary:drive_trip]) {
        isDestination = YES;
        //[self showBusyIndicator:@"Loading info ... "];
        [self->model PD301:[drive_trip valueForKey:@"oseq"]];
    }
    else {
        isDestination = NO;
        isDestination = YES;
        NSDictionary* drive_pickup    = [self.tripData valueForKey:@"drive_pickup"];
        //[self showBusyIndicator:@"Loading info ... "];
        [self->model PD301:[drive_pickup valueForKey:@"oseq"]];
    }
}

-(void) configureRider {

    //NSDictionary *user = [appDelegate.dicRider objectForKey:@"user"];
    
    NSDictionary* drive_trip    = [self.tripData valueForKey:@"drive_trip"];
    
    // 3. call api
    if ([Utils isDictionary:drive_trip]) {
        isDestination = YES;
        if (![[Utils nullToString:self.oseq] isEqualToString:@""]) {
            //[self showBusyIndicator:@"Loading info ... "];
            [self->model UD302:self.oseq];
        }
        else {
            //[self showBusyIndicator:@"Loading info ... "];
            [self->model UD301:[drive_trip valueForKey:@"oseq"]];
        }
    }
    else {
        isDestination = NO;
        isDestination = YES;
        NSDictionary* drive_pickup    = [self.tripData valueForKey:@"drive_pickup"];
        if (![[Utils nullToString:self.oseq] isEqualToString:@""]) {
            //[self showBusyIndicator:@"Loading info ... "];
            [self->model UD302:self.oseq];
        }
        else {
            //[self showBusyIndicator:@"Loading info ... "];
            [self->model UD301:[drive_pickup valueForKey:@"oseq"]];
        }
    }
}

-(void) registerEventListeners {
    for (NSArray *msg in _msgList) {
        [self->model addEventListener:self forEvent:[msg objectAtIndex:0] eventParams:nil];
    }
}

-(void) removeEventListeners {
    for (NSArray *msg in _msgList) {
        [self->model removeEventListener:self forEvent:[msg objectAtIndex:0]];
    }
}
/*
#pragma mark - GMSMapViewDelegate
- (UIView *)mapView:(GMSMapView *)mapView markerInfoWindow:(GMSMarker *)marker {
    CLLocationCoordinate2D anchor = marker.position;
    
    CGPoint point = [mapView.projection pointForCoordinate:anchor];
    
    self.calloutView.title = marker.title;
    
    self.calloutView.calloutOffset = CGPointMake(0, -CalloutYOffset);
    
    self.calloutView.hidden = NO;
    
    CGRect calloutRect = CGRectZero;
    calloutRect.origin = point;
    calloutRect.size = CGSizeZero;
    
    [self.calloutView presentCalloutFromRect:calloutRect
                                      inView:mapView
                           constrainedToView:mapView
                                    animated:YES];
    
    return self.emptyCalloutView;
}

- (void)mapView:(GMSMapView *)pMapView didChangeCameraPosition:(GMSCameraPosition *)position {
    // move callout with map drag
    if (pMapView.selectedMarker != nil && !self.calloutView.hidden) {
        CLLocationCoordinate2D anchor = [pMapView.selectedMarker position];
        
        CGPoint arrowPt = self.calloutView.backgroundView.arrowPoint;
        
        CGPoint pt = [pMapView.projection pointForCoordinate:anchor];
        pt.x -= arrowPt.x;
        pt.y -= arrowPt.y + CalloutYOffset;
        
        self.calloutView.frame = (CGRect) {.origin = pt, .size = self.calloutView.frame.size };
    } else {
        self.calloutView.hidden = YES;
    }
}

- (void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate {
    self.calloutView.hidden = YES;
}

- (UIView *)mapView:(GMSMapView *)mapView markerInfoWindow3:(GMSMarker *)marker {
    CLLocationCoordinate2D anchor = marker.position;
    
    CGPoint point = [mapView.projection pointForCoordinate:anchor];
    
    SMCalloutView *calloutView = [[SMCalloutView alloc] init];
    calloutView.title = marker.title;
    
    calloutView.calloutOffset = CGPointMake(0, -CalloutYOffset);
    
    calloutView.hidden = NO;
    
    CGRect calloutRect = CGRectZero;
    calloutRect.origin = point;
    calloutRect.size = CGSizeZero;
    
//    [calloutView presentCalloutFromRect:calloutRect
//                                      inView:mapView
//                           constrainedToView:mapView
//                                    animated:YES];
    
    //calloutView.frame = CGRectMake(0, 0, calloutView.frame.size.width, calloutView.frame.size.height);
    
//    UIView *contentView = [[UIView alloc] initWithFrame:calloutView.frame];
//    [contentView setBackgroundColor:[UIColor clearColor]];
//    [calloutView addSubview:calloutView];
    
    return calloutView;
}
*/

-(UIView *)mapView:(GMSMapView *)mapView markerInfoWindow2:(GMSMarker *)marker{
    float infoWindowWidth = 150;
    float infoWindowHeight = 64;
    float anchorSize = 20;
    
    UIView *calloutView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, infoWindowWidth, infoWindowHeight)];
    
    float offset = anchorSize * M_SQRT2;
    CGAffineTransform rotateBy45Degrees = CGAffineTransformMakeRotation(M_PI_4);
    UIView *arrow = [[UIView alloc] initWithFrame:CGRectMake((infoWindowWidth - anchorSize)/2.0, infoWindowHeight - offset, anchorSize, anchorSize)];
    arrow.transform = rotateBy45Degrees;
    arrow.backgroundColor = [UIColor blackColor];
    [calloutView addSubview:arrow];
    
    UIView *contentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, infoWindowWidth, infoWindowHeight - offset/2)];
    [contentView setBackgroundColor:[UIColor blackColor]];
    
//    contentView.layer.cornerRadius = 5;
//    contentView.layer.masksToBounds = YES;
//    
//    contentView.layer.borderColor = [UIColor lightGrayColor].CGColor;
//    contentView.layer.borderWidth = 1.0f;
    
    UILabel *label = [UILabel new];
    label.text = @"1";
    //label.font = ...;
    [label sizeToFit];
    [contentView addSubview:label];
    
    [calloutView addSubview:contentView];
    
    //    CLLocationCoordinate2D anchor = [self.mapView.selectedMarker position];
    //    CGPoint point = [self.mapView.projection pointForCoordinate:anchor];
    //    point.y -= self.mapView.selectedMarker.icon.size.height + offset/2 + (infoWindowHeight - offset/2)/2;
    //    self.actionOverlayCalloutView.center = point;
    
    //    [self.mapView addSubview:self.actionOverlayCalloutView];
    return calloutView;
}

#pragma mark - Geo
-(CLLocationCoordinate2D) getLocation:(NSString*) location{
    NSArray* stringComponents = [location componentsSeparatedByString:@","];
    
    CLLocationCoordinate2D position;
    position.latitude  = [[NSDecimalNumber decimalNumberWithString:[stringComponents objectAtIndex:0]] doubleValue];
    position.longitude = [[NSDecimalNumber decimalNumberWithString:[stringComponents objectAtIndex:1]] doubleValue];
    
    return position;
}

-(void)getAddress:(NSString*)pos index:(NSIndexPath*)indexPath addr:(NSString*)addr
{
    CLLocationCoordinate2D coor = [self getLocation:pos];
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    [locMgr geocodeAPICall:coor
                completion:^(NSDictionary *user, NSString* params, NSString *str, int status) {
                    
                    if (status==1) {
                        //NSAssert(![NSThread isMainThread], @"This shouldn't be called from the main thread");
                        //NSLog(@"responseDict:%@",user);
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            // Update the UI
                            NSString *__address = user[@"fullAddress"];
                            TXTripHistoryDetailCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
                            if ([addr isEqualToString:@"src_addr"]) {
                                cell.lb6.text = __address;
                            }
                            else {
                                cell.lb7.text = __address;
                            }
                            
                            self->items[indexPath.item][addr] = __address;
                        });
                    }
                }];
}

#pragma mark - Action
-(IBAction)onNaviButtonClick:(id)sender {
    UIButton *btn = (UIButton*)sender;
    NSLog(@"onNaviButtonClick:%d",(int)btn.tag);
    //[super onNaviButtonClick:sender];
    
    if (btn.tag == 1100 || btn.tag == 1101) {
        [self removeEventListeners];
        [self.navigationController popViewControllerAnimated:YES];
    }
    else if (btn.tag == 1300) {
        
        if (timer == nil) {
            NSDictionary* drive_trip    = [self.tripData valueForKey:@"drive_trip"]; //trip_duration/trip_distance, begin_location, end_location, update_date
            if ([Utils isDictionary:drive_trip]) {
                if ([drive_trip[@"state"] isEqualToString:@"10"]) {
                    timer = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(timer) userInfo:nil repeats:YES];
                    [[NSRunLoop mainRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
                    btnReload.hidden = NO;
                }
            }
        }
        
        [self runSpinAnimationOnView:btn duration:0.8 rotations:1 repeat:1];
        if (appDelegate.isDriverMode) {
            [self configureDriver];
        }
        else {
            [self configureRider];
        }
    }
}

#pragma mark - UITableView Delegate & Datasrouce -
//- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
//    
//    if ([tableView respondsToSelector:@selector(setSeparatorInset:)]) {
//        [tableView setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
//    }
//    
//    if ([tableView respondsToSelector:@selector(setLayoutMargins:)]) {
//        [tableView setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
//    }
//    
//    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
//        [cell setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
//    }
//    
//    
//    UIView *seperatorView = [[UIView alloc] initWithFrame:CGRectMake(0, cell.frame.size.height - 1, cell.frame.size.width, 1)];
//    seperatorView.backgroundColor = UIColorBasicBack;
//    [cell addSubview:seperatorView];
//    
//}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return height_;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return self->items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TXTripHistoryDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:self.cellIdentifier];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    UIView *v = (UIView*)[cell viewWithTag:1000];
    v.backgroundColor = HEXCOLOR(0xf7f7f7FF);
    cell.backgroundColor = [UIColor clearColor];
    cell.btnHelp.hidden = YES;

    NSDictionary *dic           = self->items[indexPath.row];
    NSDictionary* drive_charge  = nil;
    NSDictionary* drive_trip    = nil;
    NSDictionary* drive_pickup  = nil;
    NSDictionary* drive_rating  = nil;
    NSDictionary *pMap          = nil;
    NSString *api               = @"";
    NSString *service           = @"";
    NSString *oseq              = @"";
    
    pMap = [cell bindData:dic type:YES];
    CGRect rect = cell.frame;
    rect.size.height = 61;
    cell.frame = rect;
    
    if (appDelegate.isDriverMode) {
        api = @"PP602";
    }
    else {
        api = @"UR602";
    }
    
    drive_charge  = [dic valueForKey:@"drive_charge"]; //trip_duration/trip_distance, begin_location, end_location, update_date
    drive_trip    = [dic valueForKey:@"drive_trip"]; //trip_duration/trip_distance, begin_location, end_location, update_date
    drive_pickup  = [dic valueForKey:@"drive_pickup"]; //trip_duration/trip_distance, begin_location, end_location, update_date
    drive_rating  = [dic valueForKey:@"drive_rating"]; //rate/note
    [cell bindData:dic type:YES];
    
    if ([Utils isDictionary:drive_charge]) {
        if ([drive_trip[@"state"] isEqualToString:@"20"]) {
            // freeze map
            _mapView.settings.compassButton = NO;
            _mapView.settings.scrollGestures = NO;
            _mapView.settings.zoomGestures = NO;
            _mapView.settings.tiltGestures = NO;
            _mapView.settings.rotateGestures = NO;
        }
    }
    
    if ([dic[@"src_addr"] isEqualToString:@""]) {
        NSString *lb6 = @"";
        NSString *lb61 = @"";
        if ([Utils isDictionary:drive_trip]) {
            lb6 = [Utils nullTolocation:[drive_trip valueForKey:@"begin_location"]];
            service = [drive_trip valueForKey:@"service"];
            oseq = [drive_trip valueForKey:@"oseq"];
        }
        else {
            lb6 = [Utils nullTolocation:[drive_pickup valueForKey:@"start_location"]];
            service = [drive_pickup valueForKey:@"service"];
            oseq = [drive_pickup valueForKey:@"oseq"];
        }
        
        if (![lb6 length] && ![lb6 isEqualToString:@"0,0"]) {
            cell.lb6.text = LocalizedStr(@"String.NotExist");
            self->items[indexPath.row][@"src_addr"] = LocalizedStr(@"String.NotExist");
        }
        else {
            if ([Utils isDictionary:drive_trip]) {
                lb61 = [Utils nullTolocation:[drive_trip valueForKey:@"begin_addr"]];
            }
            else {
                lb61 = [Utils nullTolocation:[drive_pickup valueForKey:@"start_addr"]];
            }
            
            //lb61 = @"";
            if (![lb61 isEqualToString:@""] && ![lb61 isEqualToString:@"0,0"]) {
                cell.lb6.text = lb61;
                self->items[indexPath.item][@"src_addr"] = lb61;
            }
            else {
                [self getAddress:lb6 index:indexPath addr:@"src_addr"];
            }
        }
    }
    else {
        cell.lb6.text = dic[@"src_addr"];
    }
    
    if ([dic[@"dst_addr"] isEqualToString:@""]) {
        NSString *lb7 = @"";
        NSString *lb71 = @"";
        if ([Utils isDictionary:drive_trip]) {
            if (![Utils nullToString:[drive_trip valueForKey:@"end_location"]]) {
                lb7 = [Utils nullTolocation:[drive_trip valueForKey:@"end_location"]];
            }
            else {
                lb7 = [Utils nullTolocation:[drive_trip valueForKey:@"ldst"]];
            }
        }
        else {
            lb7 = [Utils nullTolocation:[drive_pickup valueForKey:@"lsrc"]];
            //lb7 = [drive_pickup valueForKey:@"start_location"];
        }
        
        if (![lb7 length] && ![lb7 isEqualToString:@"0,0"]) {
            cell.lb7.text = LocalizedStr(@"String.NotExist");
            self->items[indexPath.row][@"dst_addr"] = LocalizedStr(@"String.NotExist");
        }
        else {
            
            if ([Utils isDictionary:drive_trip]) {
                lb71 = [Utils nullTolocation:[drive_trip valueForKey:@"end_addr"]];
            }
            else {
                //lb71 = [Utils nullTolocation:[drive_trip valueForKey:@"end_location"]];
            }
            
            //lb71 = @"";
            if (![lb71 isEqualToString:@""] && ![lb71 isEqualToString:@"0,0"]) {
                cell.lb7.text = lb71;
                self->items[indexPath.item][@"dst_addr"] = lb71;
            }
            else {
                [self getAddress:lb7 index:indexPath addr:@"dst_addr"];
            }
        }
    }
    else {
        cell.lb7.text = dic[@"dst_addr"];
    }
    
    //UIImageView *profile = ((UIImageView *)[cell.contentView viewWithTag:2]);
    
    if (![[dic objectForKey:@"picImage"] isEqual:[NSNull null]]) {
        profile.image = [dic objectForKey:@"picImage"];
        [Utils setCircleImage:profile];
    }
    else {
        cell.profile.image = [UIImage imageNamed:@"ic_user"];
        // 3. call api
        [Utils downloadImageWithURL:api property:pMap completionBlock:^(BOOL succeeded, UIImage *image) {
            if (succeeded) {
                
                // change the image in the cell
                profile.image = image;
                [Utils setCircleImage:profile];
            }
        }];
    }
    
    // TRID
    tridLabel.text = [NSString stringWithFormat:@"TRID %@%010d",service, [oseq intValue]];
    
    cell.lb1.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:12];
    cell.lb2.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:12];
    cell.lb4.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:12];
    
    cell.lb6.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:12];
    cell.lb7.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:12];
    
    cell.lb3.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:12];
    cell.lb8.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:12];
    cell.lb5.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:12];
    
    cell.lb1.textAlignment = NSTextAlignmentRight;
    cell.lb2.textAlignment = NSTextAlignmentRight;
    cell.lb4.textAlignment = NSTextAlignmentRight;
    
    cell.lb6.textAlignment = NSTextAlignmentLeft;
    cell.lb7.textAlignment = NSTextAlignmentLeft;
    
    cell.lb3.textAlignment = NSTextAlignmentLeft;
    cell.lb8.textAlignment = NSTextAlignmentLeft;
    cell.lb5.textAlignment = NSTextAlignmentLeft;
    
    [cell.lb1 setTextColor:HEXCOLOR(0x999999FF)];
    [cell.lb2 setTextColor:HEXCOLOR(0x999999FF)];
    //[cell.lb4 setTextColor:HEXCOLOR(0x999999FF)];
    
    [cell.lb6 setTextColor:HEXCOLOR(0x030303FF)];
    [cell.lb7 setTextColor:UIColorDefault];
    
    [cell.lb3 setTextColor:HEXCOLOR(0x333333FF)];
    [cell.lb8 setTextColor:UIColorDefault];
    [cell.lb5 setTextColor:HEXCOLOR(0x000000FF)];
    
    cell.lb1.backgroundColor = [UIColor clearColor];
    cell.lb2.backgroundColor = [UIColor clearColor];
    cell.lb4.backgroundColor = [UIColor clearColor];
    cell.lb6.backgroundColor = [UIColor clearColor];
    cell.lb7.backgroundColor = [UIColor clearColor];
    cell.lb3.backgroundColor = [UIColor clearColor];
    cell.lb8.backgroundColor = [UIColor clearColor];
    cell.lb5.backgroundColor = [UIColor clearColor];
    cell.lb8.text = @"";
    cell.lb9.text = @"";
    cell.lb9.tag = 0;
    
    [cell.lb9 setTextColor:cell.lb8.textColor];
    cell.lb9.font = cell.lb8.font;
    cell.lb9.textAlignment = cell.lb8.textAlignment;
    cell.lb9.backgroundColor = cell.lb8.backgroundColor;
    cell.lb8.hidden = YES;
	
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)updateMapFitBoundsMarker:(NSArray*)_markers {
    GMSCoordinateBounds *bounds;
    for (GMSMarker *marker in _markers) {
        if (bounds == nil) {
            bounds = [[GMSCoordinateBounds alloc] initWithCoordinate:marker.position
                                                          coordinate:marker.position];
        }
        bounds = [bounds includingCoordinate:marker.position];
    }
    GMSCameraUpdate *update = [GMSCameraUpdate fitBounds:bounds
                                             withPadding:50.0f];
    [_mapView moveCamera:update];
    _mapView.hidden = NO;
    //[_mapView animateWithCameraUpdate:update];
}

- (void)updateMapFitBounds:(NSArray*)coord__ {
    GMSCoordinateBounds *bounds;
    for (NSValue *aValue in coord__) {
        CLLocationCoordinate2D coord = [aValue MKCoordinateValue];
        if (bounds == nil) {
            bounds = [[GMSCoordinateBounds alloc] initWithCoordinate:coord
                                                          coordinate:coord];
        }
        bounds = [bounds includingCoordinate:coord];
    }
    //CGFloat currentZoom = self.mapView_.camera.zoom;
    GMSCameraUpdate *update = [GMSCameraUpdate fitBounds:bounds withPadding:kBasicMapPadding];
    [_mapView moveCamera:update];
}

#pragma mark - onEvent
-(void)onEvent:(TXEvent *)event eventParams:(id)subscriptionParams {
    
    TXResponseDescriptor * descriptor = [event getEventProperty:TXEvents.Params.DESCRIPTOR];
    TXRequestObj *request = [event getEventProperty:TXEvents.Params.REQUEST];
    
    if([event.name isEqualToString:@"UD301"] || [event.name isEqualToString:@"PD301"] || [event.name isEqualToString:@"UD302"]) {
        
        if(descriptor.success == true) {
            NSString     *response = [[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding];
            NSDictionary *result   = getJSONObj(response);
            
            [_mapView clear];
            
            NSArray *stepData              = [result valueForKey:@"drive_steps"];
            
            NSMutableArray *array = [[NSMutableArray alloc] init];
            NSMutableDictionary *dic = [items[0] mutableCopy];
            dic[@"drive_rating"] = [result valueForKey:@"drive_rating"];
            [array addObject:dic];
            items = array;
            
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
            TXTripHistoryDetailCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
            if (!cell.lb9.tag) {
                NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
                attachment.image = [UIImage imageNamed:@"star_mini_yellow"];
                NSAttributedString *attachmentString = [NSAttributedString attributedStringWithAttachment:attachment];
                NSMutableAttributedString *myString;
                if ([Utils isDictionary:[result valueForKey:@"drive_rating"]]) {
                    myString= [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@""]];
                    NSMutableAttributedString *myString2= [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %d",[result[@"drive_rating"][@"rate"] intValue]]];
                    [myString appendAttributedString:attachmentString];
                    [myString appendAttributedString:myString2];
                }
                else {
                    myString= [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@""]];
                    NSMutableAttributedString *myString2= [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %d",0]];
                    [myString appendAttributedString:attachmentString];
                    [myString appendAttributedString:myString2];
                }
                cell.lb9.attributedText = myString;
                cell.lb9.tag = 1;
            }
            
            
            //[self.tableView reloadData];
            
            NSArray *blocs = [[[stepData objectAtIndex:0] valueForKey:@"location"] componentsSeparatedByString:@","];
            NSArray *elocs = nil;
            NSArray *tlocs = nil;
            if ([stepData count]>0) {
                elocs = [[[stepData objectAtIndex:[stepData count]-1] valueForKey:@"location"] componentsSeparatedByString:@","];
            }
            if ([stepData count]>1) {
                tlocs = [[[stepData objectAtIndex:[stepData count]-2] valueForKey:@"location"] componentsSeparatedByString:@","]; // 전전 step
            }
            NSArray *clocs = elocs;
            NSDictionary *drive_trip              = [self.tripData valueForKey:@"drive_trip"];
            if (![Utils isDictionary:drive_trip]) {
                drive_trip              = [self.tripData valueForKey:@"drive_pickup"];
            }
            NSString *icon__ = @"";
            if ([Utils isDictionary:drive_trip]) {
                icon__ = drive_trip[@"service"];
//                NSArray *blocs = [[drive_trip objectForKey:@"begin_location"] componentsSeparatedByString:@","];
                
                NSInteger type = [[self.tripData valueForKey:@"type"] integerValue];
                if (type == 230) {
                    // 목적지가 없을 수 도 있다.
                    if (![[Utils nullToString:[drive_trip objectForKey:@"lsrc"]] isEqualToString:@""]) {
                        elocs = [[drive_trip objectForKey:@"lsrc"] componentsSeparatedByString:@","];
                    }
                    else {
                        elocs = nil;
                    }
                }
                else {
                    // 목적지가 없을 수 도 있다.
                    if (![[Utils nullToString:[drive_trip objectForKey:@"end_location"]] isEqualToString:@""]) {
                        elocs = [[drive_trip objectForKey:@"end_location"] componentsSeparatedByString:@","];
                    }
                    else if (![[Utils nullToString:[drive_trip objectForKey:@"ldst"]] isEqualToString:@""]) {
                        elocs = [[drive_trip objectForKey:@"ldst"] componentsSeparatedByString:@","];
                    }
                    else {
                        elocs = nil;
                    }
                }
            }
            
            CLLocationCoordinate2D scoordinate;
            CLLocationCoordinate2D ecoordinate;
            CLLocationCoordinate2D ccoordinate;
            CLLocationCoordinate2D tcoordinate;
            
            scoordinate.latitude   = [[blocs objectAtIndex:0] doubleValue];
            scoordinate.longitude  = [[blocs objectAtIndex:1] doubleValue];
            if (elocs) {
                ecoordinate.latitude   = [[elocs objectAtIndex:0] doubleValue];
                ecoordinate.longitude  = [[elocs objectAtIndex:1] doubleValue];
            }
            if (clocs) {
                ccoordinate.latitude   = [[clocs objectAtIndex:0] doubleValue];
                ccoordinate.longitude  = [[clocs objectAtIndex:1] doubleValue];
            }
            if (tlocs) {
                tcoordinate.latitude   = [[tlocs objectAtIndex:0] doubleValue];
                tcoordinate.longitude  = [[tlocs objectAtIndex:1] doubleValue];
            }
            
            //NSArray *routes = [[directionResponse directionResponse] objectForKey:@"routes"];
            // NSLog(@"Route : %@", [[directionResponse directionResponse] objectForKey:@"routes"]);
            GMSMutablePath *path = [GMSMutablePath path];
            
            CLLocationCoordinate2D beforecoor = kCLLocationCoordinate2DInvalid;
            CLLocationCoordinate2D aftercoor  = kCLLocationCoordinate2DInvalid;
            
            NSMutableArray *__coords = [[NSMutableArray alloc] init];
            for (NSDictionary* dic in stepData) {
                NSString *loc = [dic valueForKey:@"location"];
                
                NSArray *locs = [loc componentsSeparatedByString:@","];
                //[path addCoordinate:CLLocationCoordinate2DMake(-33.85, 151.20)];
                [path addLatitude:[[locs objectAtIndex:0] doubleValue] longitude:[[locs objectAtIndex:1] doubleValue]];
                tcoordinate = [Utils getLocation:loc];
                if (![Utils isEqualLocation:aftercoor location:tcoordinate]) {
                    beforecoor = aftercoor;
                }
                aftercoor = tcoordinate;
                
                [__coords addObject:[NSValue valueWithMKCoordinate:tcoordinate]];
            }
            
            GMSPolyline *polyline = [GMSPolyline polylineWithPath:path];
            polyline.strokeColor = UIColorPolyLine;
            polyline.strokeWidth = PolyLineStrokeWidth;
            polyline.map = _mapView;
            
            GMSMarker *_markerStart = [GMSMarker new];
            _markerStart.title = LocalizedStr(@"Map.Drive.Marker.Pickup.text");
            //_markerStart.icon = [Utils image:[UIImage imageNamed:@"icon_pin_start"] scaledToSize:CGSizeMake(kMarkerSizeW, kMarkerSizeH)];
            //_markerStart.groundAnchor = CGPointMake(0.5f, 0.5f);
            _markerStart.flat = YES;
            _markerStart.tappable = NO;
            _markerStart.position = scoordinate;
            _markerStart.map = _mapView;
            _markerStart.icon = [Utils imageFromInfoView:_markerStart.title image:[Utils image:[UIImage imageNamed:@"icon_pin_start"] scaledToSize:CGSizeMake(kMarkerSizeW, kMarkerSizeH)] color:UIColorDefault2];
            
            
            NSArray* _markers = nil;
            GMSMarker *_markerCar;
            if (CLLocationCoordinate2DIsValid(tcoordinate) && CLLocationCoordinate2DIsValid(ccoordinate)) {
                CLLocationDistance distance = GMSGeometryDistance(ccoordinate, ecoordinate);
                //CLLocationDirection heading = GMSGeometryHeading(tcoordinate, ccoordinate);
                CLLocationDirection heading = GMSGeometryHeading(beforecoor, aftercoor);
                
                if (distance>0) {
                    UIImage *img = [UIImage imageNamed:[NSString stringWithFormat:@"img_mapcar_%@",icon__]];
                    _markerCar = [GMSMarker new];
                    _markerCar.icon = [Utils image:img scaledToSize:CGSizeMake(kCarMarkerSizeW, kCarMarkerSizeH)];
                    _markerCar.rotation = heading;
                    //_markerFinish.groundAnchor = CGPointMake(0.5f, 0.5f);
                    _markerCar.flat = YES;
                    _markerCar.tappable = NO;
                    _markerCar.position = ccoordinate;
                    _markerCar.map = _mapView;
                }
            }
            if (isDestination) {
                if (elocs) {
                    CLLocationDistance distance = GMSGeometryDistance(ccoordinate, ecoordinate);
                    
                    GMSMarker *_markerFinish = [GMSMarker new];
                    if (distance>0) {
                        NSString *dist = [NSString stringWithFormat:@"%.f",(distance*0.001)];
                        if ([dist intValue]<1) {
                            dist = [NSString stringWithFormat:@"%.02f",(distance*0.001)];
                        }
                        _markerFinish.title = [NSString stringWithFormat:LocalizedStr(@"Map.Drive.Marker.ETS.text"),[dist intValue]];
                        //_markerFinish.title = [NSString stringWithFormat:LocalizedStr(@"Map.Drive.Marker.ET.text"),(int)distance];
                        _markerFinish.icon = [Utils imageFromInfoView:_markerFinish.title image:[Utils image:[UIImage imageNamed:@"icon_pin_end"] scaledToSize:CGSizeMake(kMarkerSizeW, kMarkerSizeH)] color:UIColorDefault2];
                    }
                    else {
                        _markerFinish.icon = [Utils image:[UIImage imageNamed:@"icon_pin_end"] scaledToSize:CGSizeMake(kMarkerSizeW, kMarkerSizeH)];
                    }
                    //_markerFinish.groundAnchor = CGPointMake(0.5f, 0.5f);
                    _markerFinish.flat = YES;
                    _markerFinish.tappable = NO;
                    _markerFinish.position = ecoordinate;
                    _markerFinish.map = _mapView;
                    
                    //_markers = [[NSArray alloc] initWithObjects:_markerStart, _markerFinish, nil];
                    [__coords addObject:[NSValue valueWithMKCoordinate:_markerStart.position]];
                    [__coords addObject:[NSValue valueWithMKCoordinate:_markerFinish.position]];
                }
            }
            else {
                //_markers = [[NSArray alloc] initWithObjects:_markerStart, nil];
                [__coords addObject:[NSValue valueWithMKCoordinate:_markerCar.position]];
                [__coords addObject:[NSValue valueWithMKCoordinate:_markerStart.position]];
            }

            if ([event.name isEqualToString:@"UD302"] && isFirst) {
                if (__coords) {
                    [self updateMapFitBounds:__coords];
                }
                else {
                    [self updateMapFitBoundsMarker:_markers];
                }
                isFirst = NO;
            }

//            if ([event.name isEqualToString:@"UD302"] && isFirst) {
//                if (isFirst) {
//                    if (__coords) {
//                        [self updateMapFitBounds:__coords];
//                    }
//                    else {
//                        [self updateMapFitBoundsMarker:_markers];
//                    }
//                    isFirst = NO;
//                }
//            }
//            else {
//                if (__coords) {
//                    [self updateMapFitBounds:__coords];
//                }
//                else {
//                    [self updateMapFitBoundsMarker:_markers];
//                }
//            }
        } else {
            //에러주석[self alertError:@"Error" message:descriptor.error];
        }
    }
    [self hideBusyIndicator];
}
/*
//- (void)downloadImageWithURL:(NSString *)code property:(NSDictionary*)pMap completionBlock:(void (^)(BOOL succeeded, UIImage *image))completionBlock
{
    TXRequestObj *request     = [self->model createRequest:code];
    
    request.body = getJSONStr(pMap);
    NSMutableURLRequest *httpRequest = [request createHTTPRequest];
    DLogI(@"Sent Async Request to URL - %@\n\n", request.reqUrl);
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:httpRequest
                                            completionHandler:
                                  ^(NSData *data, NSURLResponse *response, NSError *error) {
                                      if ([response isKindOfClass:[NSHTTPURLResponse class]] &&
                                          ((NSHTTPURLResponse *)response).statusCode == 200 &&
                                          error == nil &&
                                          data != nil) {
                                          if ([data length]) {
                                              
                                              UIImage *image = [UIImage imageWithData:data];
                                              completionBlock(YES,image);
                                          }
                                          else {
                                              completionBlock(YES,[UIImage imageNamed:@"ic_user_white"]);
                                          }
                                      } else {
                                          completionBlock(NO,nil);
                                      }
                                  }];
    [task resume];
    //
    [NSURLConnection sendAsynchronousRequest:httpRequest
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               if ( !error )
                               {
                                   if ([data length]) {
                                       
                                       UIImage *image = [UIImage imageWithData:data];
                                       completionBlock(YES,image);
                                   }
                                   else {
                                       completionBlock(YES,[UIImage imageNamed:@"ic_user_white"]);
                                   }
                               } else{
                                   completionBlock(NO,nil);
                               }
                           }];
     //
}
*/
@end
