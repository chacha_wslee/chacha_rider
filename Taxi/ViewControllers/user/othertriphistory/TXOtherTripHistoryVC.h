//
//  MenuViewController.h
//  SlideMenu
//

//

#import <UIKit/UIKit.h>
#import "TXBaseViewController.h"

@interface TXOtherTripHistoryVC : TXBaseViewController <UITableViewDelegate, UITableViewDataSource> {

}

@property (nonatomic, strong) NSString *cellIdentifier;
@property (nonatomic, strong) IBOutlet UITableView *tableView;

@property (nonatomic, assign) BOOL isShare;

@end
