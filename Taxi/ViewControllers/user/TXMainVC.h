//
//  TXMainVC.h

//

#import "TXBaseViewController.h"

@interface TXMainVC : TXBaseViewController

- (id)initWithToken:(NSString *) token;

@end
