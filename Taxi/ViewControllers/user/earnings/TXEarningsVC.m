//
//  MenuViewController.m
//  SlideMenu
//

//

#import "TXEarningsVC.h"
#import "TXEarningsCell.h"
#import "RTLabel.h"
#import "LGAlertView.h"

@interface TXEarningsVC() <RTLabelDelegate>{
    NSMutableArray *items;
    NSArray *_msgList;
    
    double totalEarning;
}

@end

@implementation TXEarningsVC {
    //NSMutableDictionary *propertyMap;
}
@synthesize cellIdentifier;


-(void)viewDidLoad {
    
    [super viewDidLoad];
    
    UIView *n = [super navigationView];
    NSInteger _y = n.frame.origin.y + n.frame.size.height;
    _y = _imgPattern.frame.origin.y + _imgPattern.frame.size.height + 5;
    [n setBackgroundColor:UIColorDefault];
#ifdef _CHACHA
    UILabel *lb = [n viewWithTag:1210];
    lb.textColor = [UIColor whiteColor];
    
    
    _vBg.backgroundColor = UIColorDefault;
#endif
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, _y, self.view.bounds.size.width, self.view.bounds.size.height-_y)
                                                  style:UITableViewStylePlain];
    self.tableView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:self.tableView];
    
    UINib *nib = [UINib nibWithNibName:@"TXEarningsCell" bundle:nil];
    self.cellIdentifier = @"TXEarningsCell";
    [self.tableView registerNib:nib forCellReuseIdentifier:self.cellIdentifier];
    
    //self.tableView.separatorColor = [UIColor colorWithRed:150/255.0f green:161/255.0f blue:177/255.0f alpha:1.0f];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.opaque = NO;
    self.tableView.backgroundColor = [UIColor clearColor];
//    self.tableView.tableHeaderView = ({
//        
//        UIView *baseview = [Utils addBackView:self.view targetView:nil];
//        baseview.backgroundColor = [UIColor clearColor];
//        CGRect rect = baseview.frame;
//        rect.size.height = kBasicHeight+kBasicHeight;
//        baseview.frame = rect;
//        
//        // total
//        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(kBasicMargin,
//                                                                 kBasicHeight/4,
//                                                                 self.view.frame.size.width - kBasicMargin*2,
//                                                                 kBasicHeight/2)];
//        [label setTextColor:UIColorDefault];
//        [label setFont:[UIFont systemFontOfSize:12.0f]];
//        [label setNumberOfLines:1];
//        label.textAlignment = NSTextAlignmentCenter;
//        label.text = LocalizedStr(@"Menu.Driver.Earning.Header.text");
//        [baseview addSubview:label];
//        
//        // 금액
//        label = [[UILabel alloc] initWithFrame:CGRectMake(kBasicMargin,
//                                                          kBasicHeight/1.2,
//                                                          self.view.frame.size.width - kBasicMargin*2,
//                                                          kBasicHeight)];
//        [label setTextColor:[UIColor blackColor]];
//        [label setFont:[UIFont boldSystemFontOfSize:20.0f]];
//        [label setNumberOfLines:1];
//        label.textAlignment = NSTextAlignmentCenter;
//        label.tag = 2002;
//        [baseview addSubview:label];
//        
//        baseview;
//    });
    
    _imgPattern.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"img_fare_line"]];
    
    UIView *view = [Utils addRTLabelView:self.view underView:nil dim:kBasicMargin];
    view.backgroundColor = UIColorLabelBG;
    
    RTLabel *label = [view viewWithTag:11];
    label.textColor = UIColorLabelText;
    label.text = [NSString stringWithFormat:LocalizedStr(@"Menu.Driver.Earning.Footer.text"),INSURANCE_FEE_URL];
    [label setFont:[UIFont systemFontOfSize:10.0f]];
    label.tag = 3001;
    [label sizeToFit];
    CGSize optimumSize = [label optimumSize];
    CGRect frame = view.frame;
    frame.size.height = optimumSize.height + kBasicMargin*2;
    view.frame = frame;
    
//    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, view.frame.size.height)];
//    self.tableView.tableFooterView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
//    [self.tableView.tableFooterView addSubview:({
//                view;
//    })];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    
    [label setDelegate:self];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    totalEarning = 0;
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self removeEventListeners];
}

#pragma mark - Configure

-(void)configureStyles {
    [super configureStyles];
    
    
#ifdef _CHACHA
    [super navigationType01X:[UIImage imageNamed:@"btn_back_02"] centerText:LocalizedStr(@"Menu.Driver.Earning.title")];
#else
    [super navigationType01X:[UIImage imageNamed:@"btn_back"] centerText:LocalizedStr(@"Menu.Driver.Earning.title")];
#endif
}

-(void)configure {
    [super configure];
    
    // 2. init event
    _msgList = @[
                 @[@"PD402",@""], // earnings
                 ];
    
    [self registerEventListeners];
    
    //propertyMap = [[NSMutableDictionary alloc] init];
    
    _lbDesc.text = LocalizedStr(@"Menu.Driver.Earning.Header.text");
    
    [self showBusyIndicator:@"Loading info ... "];
    // 3. call api
    [self->model PD402];
    
}

-(void) registerEventListeners {
    for (NSArray *msg in _msgList) {
        [self->model addEventListener:self forEvent:[msg objectAtIndex:0] eventParams:nil];
    }
}

-(void) removeEventListeners {
    for (NSArray *msg in _msgList) {
        [self->model removeEventListener:self forEvent:[msg objectAtIndex:0]];
    }
}

#pragma mark - Action
-(IBAction)onNaviButtonClick:(id)sender {
    UIButton *btn = (UIButton*)sender;
    NSLog(@"onNaviButtonClick");
    //[super onNaviButtonClick:sender];
    
    if (btn.tag == 1100) {
        [self removeEventListeners];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark - UITableView Delegate & Datasrouce -
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [tableView setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [tableView setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
    
    UIView *seperatorView = [[UIView alloc] initWithFrame:CGRectMake(10, cell.frame.size.height - 3, cell.frame.size.width - 10*2, 3)];
    seperatorView.backgroundColor = HEXCOLOR(0x111111FF);
    [cell addSubview:seperatorView];
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
#ifdef _CHACHA
    return 81;
#else
    return 208;
#endif
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return self->items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	TXEarningsCell *cell = [tableView dequeueReusableCellWithIdentifier:self.cellIdentifier];
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    NSDictionary *dic = self->items[indexPath.row];
    [cell bindData:dic];
    
	return cell;
}

#pragma mark - onEvent
-(void)onEvent:(TXEvent *)event eventParams:(id)subscriptionParams {
    
    TXResponseDescriptor * descriptor = [event getEventProperty:TXEvents.Params.DESCRIPTOR];
    TXRequestObj *request = [event getEventProperty:TXEvents.Params.REQUEST];
    
    [self hideBusyIndicator];
    if([event.name isEqualToString:@"PD402"]) {
        
        if(descriptor.success == true) {
            NSString     *response = [[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding];
            NSDictionary *result   = getJSONObj(response);
            
            NSArray *payoutData              = [result valueForKey:@"provider_payouts"];
            
            self->items = [NSMutableArray arrayWithCapacity:[payoutData count]];

            totalEarning = 0.0f;
            for (NSDictionary* p in payoutData) {
                [items addObject:p];
                
                totalEarning += [p[@"total_payout"] doubleValue];
            }
            
#ifdef _CHACHA
            NSString *total = [NSString stringWithFormat:@"%.f",totalEarning];
#else
            NSString *total = [NSString stringWithFormat:@"%.2f",totalEarning];
#endif
            //((UILabel*)[self.view viewWithTag:2002]).text = CURRENCY_FORMAT(total);
            _lbTotal.text = CURRENCY_FORMAT(total);
            [self.tableView reloadData];
            
        } else {
            //[self alertError:@"Error" message:descriptor.error];
        }
    }
}

#pragma mark - RTLabel Delegate
-(void)rtLabel:(id)rtLabel didSelectLinkWithURL:(NSURL*)url
{
    
    //RTLabel * label = (RTLabel*)rtLabel;
    
    [[UIApplication sharedApplication] openURL: [NSURL URLWithString:[url absoluteString]]];
    //[self openBrowserViewForURL:[url absoluteString] pageTitle:label.text];
    
}

@end
