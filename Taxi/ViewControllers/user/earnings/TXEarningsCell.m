//
//  TXPaymentCell.m
//  TXPaymentCell
//

//

#import "TXEarningsCell.h"
#import "utils.h"
#import "utilities.h"

@implementation TXEarningsCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
    }
    return self;
}

- (void)bindData:(NSDictionary*)dic
{
    // color
    //---------------------------------------------------------------------------------------------------------
    self.lb1.textColor = UIColorDefault;
    
    self.lbv1.textColor = UIColorLabelText;
    
    // text
    //---------------------------------------------------------------------------------------------------------
    self.lb1.text = LocalizedStr(@"Menu.Driver.Earning.Line1.text");
    self.lb5.text = LocalizedStr(@"Menu.Driver.Earning.Line5.text");
    
    // value
    //---------------------------------------------------------------------------------------------------------
    self.lbv1.text = [NSString stringWithFormat:@"%@ ~ %@",
                      [self DateString2String:dic[@"start_date"]],
                      [self DateString2String:dic[@"end_date"]]];
    
    self.lbv5.text = CURRENCY_FORMAT(dic[@"total_payout"]);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

-(void)stateChanged
{
    
}

-(NSString*)DateString2String:(NSString*)dateStr
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MM'/'dd'/'yyyy"];
    NSDate *date = [self StringFormat2Date:dateStr];
    return [formatter stringFromDate:date];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
-(NSDate*) StringFormat2Date:(NSString *)dateStr
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy'-'MM'-'dd' 'HH':'mm':'ss"];
    //[formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    return [formatter dateFromString:dateStr];
}

@end
