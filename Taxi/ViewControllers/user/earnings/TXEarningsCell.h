//
//  TXPaymentCell.h
//  TXPaymentCell
//

//

#import <UIKit/UIKit.h>

@interface TXEarningsCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *lb1;
@property (nonatomic, weak) IBOutlet UILabel *lb5;

@property (nonatomic, weak) IBOutlet UILabel *lbv1;
@property (nonatomic, weak) IBOutlet UILabel *lbv5;

- (void)bindData:(NSDictionary*)dic;
@end
