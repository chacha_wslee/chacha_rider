//
//  MenuViewController.h
//  SlideMenu
//

//

#import <UIKit/UIKit.h>
#import "TXBaseViewController.h"

@interface TXEarningsVC : TXBaseViewController <UITableViewDelegate, UITableViewDataSource> {

}

@property (nonatomic, strong) NSString *cellIdentifier;
@property (nonatomic, strong) IBOutlet UITableView *tableView;


@property (nonatomic, strong) IBOutlet UILabel *lbTotal;
@property (nonatomic, strong) IBOutlet UILabel *lbDesc;

@property (nonatomic, strong) IBOutlet UIView* vBg;
@property (nonatomic, strong) IBOutlet UIImageView* imgPattern;

@end
