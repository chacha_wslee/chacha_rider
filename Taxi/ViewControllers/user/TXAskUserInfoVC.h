//
//  TXAskUserInfoVC.h
//  Taxi

//

#import "TXUserVC.h"

@interface TXAskUserInfoVC : TXUserVC

@property (nonatomic, strong) IBOutlet UITextField *txtName;
@property (nonatomic, strong) IBOutlet UITextField *txtSurname;
@property (nonatomic, strong) IBOutlet UITextField *txtEmail;

@end
