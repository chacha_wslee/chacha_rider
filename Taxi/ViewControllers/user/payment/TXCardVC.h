//
//  MenuViewController.h
//  SlideMenu
//

//

#import <UIKit/UIKit.h>
#import "TXBaseViewController.h"
#import "REFormattedNumberField.h"

@interface TXCardVC : TXBaseViewController {

}

@property (strong, nonatomic) IBOutlet UIWebView *vbody;

@property (nonatomic) NSInteger setupMode;
@property (assign) BOOL isAnimation;
@property (assign, nonatomic) BOOL isInAPP;

@end

@interface UIWebView (Javascript)
- (BOOL)webView:(UIWebView *)sender runJavaScriptConfirmPanelWithMessage:(NSString *)message initiatedByFrame:(id)frame;
- (void)webView:(UIWebView *)sender runJavaScriptAlertPanelWithMessage:(NSString *)message initiatedByFrame:(id)frame;
@end
