//
//  TXAskCardNumberVC.m
//  Taxi

//

#import "TXAskCardNumberVC.h"
//#import "TXAskUserInfoVC.h"
//#import "TXSubscriptionVC.h"
#import "TXUserModel.h"
#import "TXMapVC.h"
#import "LGAlertView.h"
#import "MenuNavigationController.h"
#import "CardIO.h"
#import "TXPaymentFinishVC.h"

@import IQKeyboardManager;

#import "Validator.h"
#import "WToast.h"
//#import "CRToast.h"

@implementation CountryCodeItem
@synthesize name, code;
@end

@interface TXAskCardNumberVC () <UIPickerViewDelegate,
CardIOPaymentViewControllerDelegate,
ValidatorDelegate>{
    NSMutableArray *items;
    
    NSNumber *userId;
}

@property (strong, readwrite, nonatomic) RECreditCardItem *creditCardItem;
@property (strong, readwrite, nonatomic) RECreditCardItem *creditCardItemCVV;
@property (nonatomic, strong) UITableView *tableView;

-(IBAction)next:(id)sender;

@end

@implementation TXAskCardNumberVC {
    NSDateComponents *currentDateComponents;
    NSMutableArray *monthsArray;
    NSMutableArray *yearsArray;
    
    long year;
    long month;
}
@synthesize isInAPP;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [super configureBottomLine];
    NSInteger _y = [super navigationTopHeight];
    UILabel *label = (UILabel*)[[super navigationView] viewWithTag:1111];
    label.tag = 2001;
    label.text = LocalizedStr(@"Menu.Payment.List1.title");
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];

    [[IQKeyboardManager sharedManager] setEnable:YES];
    
    //[super configureBottomLine];
    UIView *line = (UIView*)[[super navigationView] viewWithTag:1500];
    CGRect rect = line.frame;
    rect.size.height = 3;
    line.frame = rect;
    
    
/*
    self.tableView = [[UITableView alloc] initWithFrame:self.view.frame
                                                  style:UITableViewStylePlain];
    self.tableView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:self.tableView];
    
    
    //self.view = self.tableView;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 58)];
    [self.tableView.tableFooterView addSubview:({
        UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        button.frame = CGRectMake(40, 7, 240, 44);
        button.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
        [button setTitle:@"Load more" forState:UIControlStateNormal];
        [button addTarget:self action:@selector(next:) forControlEvents:UIControlEventTouchUpInside];
        button;
    })];
    
    [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    */
    
    UIButton *btn = [Utils addBottomButton:LocalizedStr(@"Menu.Payment.Next.title") target:self selector:@selector(onNaviButtonClick:) color:YES];
    btn.tag = 2300;
    [self.view addSubview:btn];
    
    NSInteger _height = self.view.frame.size.height - _y - btn.frame.size.height - kBottomBottonSafeArea;
    if (_height<self.vbody.frame.size.height) {
        self.vbody.frame = CGRectMake(0,
                                      _y,
                                      self.view.frame.size.width,
                                      self.view.frame.size.height - _y - btn.frame.size.height);
    }
    else {
        self.vbody.frame = CGRectMake(0,
                                      _y,
                                      self.view.frame.size.width,
                                      self.vbody.frame.size.height);
    }
    
    
    [self.view addSubview:self.vbody];
}

-(void)viewDidAppear:(BOOL)animated {
    UIPickerView *datePicker = [UIPickerView new];
    //datePicker.frame = CGRectMake(0.f, 0.f, datePicker.frame.size.width, 160.f);
    datePicker.frame = CGRectMake(0.f, 0.f, 240, 160.f);
    
    datePicker.showsSelectionIndicator = YES;
    [datePicker selectRow:[currentDateComponents month]-1 inComponent:0 animated:YES];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    [[IQKeyboardManager sharedManager] setEnable:YES];
    
    [CardIOUtilities preload];

    [self checkProcess];
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    //[self removeEventListeners];
}

-(void)dealloc {
    [self removeEventListeners];
}

-(void)configureStyles {
    [super configureStyles];
    
    [super navigationType01X:[UIImage imageNamed:@"btn_back"] centerText:LocalizedStr(@"Menu.Payment.Add.title")];
    
    [self nextButtonOn:NO];
}

- (void)nextButtonOn:(BOOL)on
{
    UIButton *btn = [super.view viewWithTag:2300];
    btn.enabled = on;
    if (on)
        [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    else
        [btn setTitleColor:UIColorBasicTextOff forState:UIControlStateNormal];
}

-(void) configure {
    [super configure];
    [self registerEventListeners];
    [self configureFormatting];
    
    [[IQKeyboardManager sharedManager] setEnable:YES];
    
    //[button addTarget:self action:@selector(onPhoto:) forControlEvents:UIControlEventTouchUpInside];
    /*
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width - 35 - 19, 57, 35, 24)];
    button.tag = 999;
    [button addTarget:self action:@selector(onClickCard:) forControlEvents:UIControlEventTouchUpInside];
    button.backgroundColor = [UIColor clearColor];
    [button setImage:[UIImage imageNamed:@"btn_camera"] forState:UIControlStateNormal];
    //[button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    button.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
    [[super navigationView] addSubview:button];
    */
//    UIView *n = [super navigationView];
//    NSInteger _y = n.frame.origin.y + n.frame.size.height;
    
    //------------------------------------------------------------------------------------------------------------------------
    //((UILabel *)[self.view viewWithTag:101]).text = LocalizedStr(@"Menu.Payment.List1.title");
    _lbText1.text = LocalizedStr(@"Menu.Payment.Header.text1");
    _lbText2.text = LocalizedStr(@"Menu.Payment.Header.text2");
    
    ((UILabel *)[self.view viewWithTag:111]).text = LocalizedStr(@"Menu.Payment.List1.text1");
    ((UILabel *)[self.view viewWithTag:121]).text = LocalizedStr(@"Menu.Payment.List1.text2");
    ((UILabel *)[self.view viewWithTag:131]).text = LocalizedStr(@"Menu.Payment.List1.text3");
    ((UILabel *)[self.view viewWithTag:141]).text = LocalizedStr(@"Menu.Payment.List1.text4");
    ((UILabel *)[self.view viewWithTag:151]).text = LocalizedStr(@"Menu.Payment.List1.text5");
    //------------------------------------------------------------------------------------------------------------------------
    //self.view.backgroundColor = UIColorLabelBG;
    
    _lbDesc.text = LocalizedStr(@"Menu.Payment.Bottom.text1");;
}

-(void) configureFormatting {
    
    UIView *v1 = [self.vbody viewWithTag:910];
    v1.hidden = YES;
    
    self.txtCardNumber.tag = 10;
    self.txtCardNumber.format = @"XXXX XXXX XXXX XXXX XXXX";
    self.txtCardNumber.placeholder = @"1234 5678 9102 3456";
    //self.txtCardNumber.font = [UIFont systemFontOfSize:15];
    [self.txtCardNumber addTarget:self action:@selector(textDidChange:) forControlEvents:UIControlEventEditingChanged];
    [self.btnCardNum setTitle:@"1234" forState:UIControlStateNormal];
    [self.btnCardNum setTitleColor:HEXCOLOR(0xd8d8d8ff) forState:UIControlStateNormal];
    
    self.txtMMYY.tag = 11;
    self.txtMMYY.format = @"XX/XX";
    self.txtMMYY.placeholder = @"MM/YY";
    //self.txtMMYY.textColor = UIColorCellBlue;
    //self.txtMMYY.font = [UIFont systemFontOfSize:15];
    [self.txtMMYY addTarget:self action:@selector(textDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    self.txtPASS.tag = 12;
    self.txtPASS.format = @"XX";
    self.txtPASS.placeholder = @"XX";
    //self.txtPASS.textColor = UIColorCellBlue;
    //self.txtPASS.font = [UIFont systemFontOfSize:15];
    [self.txtPASS addTarget:self action:@selector(textDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    self.txtDOB.tag = 13;
    self.txtDOB.format = @"XXXXXX";
    self.txtDOB.placeholder = @"YYMMDD";
    //self.txtDOB.textColor = UIColorCellBlue;
    //self.txtDOB.font = [UIFont systemFontOfSize:15];
    [self.txtDOB addTarget:self action:@selector(textDidChange:) forControlEvents:UIControlEventEditingChanged];

    self.txtDOB2.tag = 14;
    self.txtDOB2.format = @"XXX-XX-XXXXX";
    self.txtDOB2.placeholder = @"000-00-00000";
    //self.txtDOB2.textColor = UIColorCellBlue;
    //self.txtDOB.font = [UIFont systemFontOfSize:15];
    [self.txtDOB2 addTarget:self action:@selector(textDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    currentDateComponents = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy"];
    NSString *yearString = [formatter stringFromDate:[NSDate date]];
    
    //Array for picker view
    monthsArray=[[NSMutableArray alloc]initWithObjects:@"Jan",@"Feb",@"Mar",@"Apr",@"May",@"Jun",@"Jul",@"Aug",@"Sep",@"Oct",@"Nov",@"Dec",nil];
    yearsArray=[[NSMutableArray alloc]init];
    
    
    for (int i=0; i<13; i++)
    {
        [yearsArray addObject:[NSString stringWithFormat:@"%d",[yearString intValue]+i]];
    }
}

-(void) registerEventListeners {
    [self->model addEventListener:self forEvent:@"UR322" eventParams:nil];
    [self->model addEventListener:self forEvent:@"UR311" eventParams:nil];
}

-(void) removeEventListeners {
    [self->model removeEventListener:self forEvent:@"UR322"];
    [self->model removeEventListener:self forEvent:@"UR311"];
}

#pragma mark - Action
-(IBAction)onNaviButtonClick:(id)sender {
    UIButton *btn = (UIButton*)sender;
    if (btn.tag == 1100 || btn.tag == 1101) {
        [self removeEventListeners];
        [self.navigationController popViewControllerAnimated:YES];
    }
    else if(btn.tag == 2300) {
        [self next:nil];
    }
    else if(btn.tag == 4100) {
//        UIButton *btn1 = [self.view viewWithTag:4000];
//        UIButton *btn2 = [self.view viewWithTag:4001];
//        [btn1 setImage:[UIImage imageNamed:@"form_chkbox_chk"] forState:UIControlStateNormal];
//        [btn2 setImage:[UIImage imageNamed:@"form_chkbox_normal"] forState:UIControlStateNormal];

        UIImageView *img1 = [self.view viewWithTag:4000];
        UIImageView *img2 = [self.view viewWithTag:4001];
        [img1 setImage:[UIImage imageNamed:@"form_chkbox_chk"]];
        [img2 setImage:[UIImage imageNamed:@"form_chkbox_normal"]];
        
        ((UILabel *)[self.view viewWithTag:140]).hidden = NO;
        ((UILabel *)[self.view viewWithTag:150]).hidden = YES;
        
        [self checkProcess];
    }
    else if(btn.tag == 4101) {
//        UIButton *btn1 = [self.view viewWithTag:4000];
//        UIButton *btn2 = [self.view viewWithTag:4001];
//        [btn1 setImage:[UIImage imageNamed:@"form_chkbox_normal"] forState:UIControlStateNormal];
//        [btn2 setImage:[UIImage imageNamed:@"form_chkbox_chk"] forState:UIControlStateNormal];

        UIImageView *img1 = [self.view viewWithTag:4000];
        UIImageView *img2 = [self.view viewWithTag:4001];
        [img1 setImage:[UIImage imageNamed:@"form_chkbox_normal"]];
        [img2 setImage:[UIImage imageNamed:@"form_chkbox_chk"]];
        
        ((UILabel *)[self.view viewWithTag:140]).hidden = YES;
        ((UILabel *)[self.view viewWithTag:150]).hidden = NO;
        
        [self checkProcess];
    }
    else if(btn.tag == 5000) {
        //
        [btn setImage:[UIImage imageNamed:@"form_chkbox_chk"] forState:UIControlStateNormal];
        btn.tag = 5001;
    }
    else if(btn.tag == 5001) {
        //
        [btn setImage:[UIImage imageNamed:@"form_chkbox_normal"] forState:UIControlStateNormal];
        btn.tag = 5000;
    }
}

-(void) checkProcess {
    //UIButton *btn = [self.view viewWithTag:4000];
    BOOL is_private = YES;
    if (![self.view viewWithTag:150].hidden) {
        is_private = NO;
    }
    if (is_private) {
        // private card
        if([self.txtCardNumber.text length] >= 14 &&
           [self.txtPASS.text length] == 2 &&
           [self.txtDOB.text length] == 6 &&
           [self.txtMMYY.text length] == 5 ) {
            [self nextButtonOn:YES];
        }
        else {
            [self nextButtonOn:NO];
        }
    }
    else {
        if([self.txtCardNumber.text length] >= 14 &&
           [self.txtPASS.text length] == 2 &&
           [self.txtDOB2.text length] == 12 &&
           [self.txtMMYY.text length] == 5 ) {
            [self nextButtonOn:YES];
        }
        else {
            [self nextButtonOn:NO];
        }
    }
}

-(void)next:(id)sender {
    [self.view endEditing:YES];
    [self validateAction:sender];
    return;
}

-(IBAction)onClickCardInput:(id)sender {
    
    [self textFieldBackground:sender];
    
}

-(IBAction)onClickCard:(id)sender {
    [self.view resignFirstResponder];
    [self.view endEditing:YES];

    CardIOPaymentViewController *scanViewController = [[CardIOPaymentViewController alloc] initWithPaymentDelegate:self];
    [self presentViewController:scanViewController animated:YES completion:nil];

}

-(void)textFieldFocusLost:(UITextField *)sender {
    [self checkProcess];
}

-(void)textFieldFocusGained:(UITextField *)sender {
    NSLog(@"textFieldFocusGained %ld textDidChange %@", sender.tag, sender.text);
    
    [self textFieldBackground:sender];
    
    if (sender.tag == 110) {
        
    }
    else if (sender.tag == 120) {
        
    }
    else if (sender.tag == 130) {
        
    }
    else if (sender.tag == 140) {
        
    }
    if([sender isEqual:self.txtMMYY]) {
        //[self onClick:nil];
    }
    [self checkProcess];
}

-(void)textFieldValueChanged:(UITextField *)sender {

}

-(void)onFail:(id)object error:(TXError *)error {
    NSLog(@"event,name:");
}

-(void)onEvent:(TXEvent *)event eventParams:(id)subscriptionParams {
    
    //[self showBusyIndicator:@""];
    //[self hideBusyIndicator];
    TXResponseDescriptor *descriptor = [event getEventProperty:TXEvents.Params.DESCRIPTOR];
    NSLog(@"event,name:%@",event.name);

    if([event.name isEqualToString:@"UR322"]) {
        
        if(descriptor.success == true) {
            
//            TXRequestObj *request = [event getEventProperty:TXEvents.Params.REQUEST];
//            NSDictionary *responseObj = getJSONObj([[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding]);
//            NSDictionary *dic = responseObj[@"user_payments"];
//            NSDictionary *propertyMap = @{
//                                          @"useq" : dic[@"useq"],
//                                          @"cseq" : dic[@"cseq"],
//                                          //                                                        @"pay_balance" : @"0.0",
//                                          @"pay_nonce" : dic[@"no"],
//                                          };
//
//            [self->model UR311:propertyMap];
            
            TXRequestObj *request = [event getEventProperty:TXEvents.Params.REQUEST];
            NSDictionary *responseObj = getJSONObj([[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding]);
            //NSDictionary *dic = responseObj[@"user_payment"];
            NSArray *lists = responseObj[@"user_payments"];
            
            NSString *CCNumber = [self.txtCardNumber.text stringByReplacingOccurrencesOfString:@" " withString:@""];
            CCNumber = [CCNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
            
            NSInteger lastIdx = [CCNumber length] - 4;
            CCNumber = [CCNumber substringFromIndex : lastIdx];
            
            for (NSDictionary *dic in lists) {
                NSString *no = dic[@"no"];
                
                if ([no isEqualToString:CCNumber]) {
                    // 처리가 정상이면 새로운 카드정보를 저장한다.
                    if ([dic[@"mode"] intValue] == 1 && [dic[@"state"] intValue] == 1) {
                        [[[TXApp instance] getSettings] setFDKeychain:SettingsConst.CryptoKeys.CSEQ value:[dic valueForKey:@"cseq"]];
                        
                        // 1. 정보 업데이트
                        NSMutableDictionary *riderDic = [[NSMutableDictionary alloc] init];
                        riderDic = [appDelegate.dicRider mutableCopy];
                        
                        NSMutableArray *upay = [[NSMutableArray alloc] init];
                        if ([riderDic valueForKey:@"user_payments"]) {
                            upay = [riderDic[@"user_payments"] mutableCopy];
                        }
                        [upay addObject:dic];
                        [riderDic setObject:upay forKey:@"user_payments"];
                        appDelegate.dicRider = riderDic;
                    }
                    
                    TXPaymentFinishVC *firstVC = [[TXPaymentFinishVC alloc] initWithNibName:@"TXPaymentFinishVC" bundle:nil];
                    firstVC.isInAPP = isInAPP;
                    [self pushViewController:firstVC];
                    
                    /*
                    if (isInAPP) {
                        [self removeEventListeners];
                        [self.navigationController popViewControllerAnimated:YES];
                        //[self.navigationController popToRootViewControllerAnimated:YES];
                    }
                    else {
                        [self removeEventListeners];
                        
#ifdef _DRIVER_MODE
                        TXMapVC *mapVC = [[TXMapVC alloc] initWithNibName:@"TXMapDriverVC" bundle:nil];
#else
                        TXMapVC *mapVC = [[TXMapVC alloc] initWithNibName:@"TXMapRiderVC" bundle:nil];
#endif
                        
                        [self pushViewController:mapVC];
                        NSArray* tempVCA = [self.navigationController viewControllers];
                        for(UIViewController *tempVC in tempVCA)
                        {
                            if(![tempVC isKindOfClass:[TXMapVC class]]) {
                                [tempVC removeFromParentViewController];
                            }
                        }
                        [self removeFromParentViewController];
                    }
                     */
                    break;
                }
            }
            
        } else {
            [self hideBusyIndicator];
            //[self alertError:LocalizedStr(@"String.Alert") message:descriptor.error];
            //        NSString *message = [TXCode2MsgTranslator messageForCode:descriptor.rlt];
            //        [self alertError:@"Error" message:message];
        }
        [self hideBusyIndicator];
    }
    else if([event.name isEqualToString:@"UR311"]) {
        
        if(descriptor.success == true) {
            
            //        NSDictionary*source = (NSDictionary*)descriptor.atoken;
            //        [[[self->model getApp] getSettings] setUserToken:[source objectForKey:SettingsConst.CryptoKeys.USERTOKEN]];
            
            TXRequestObj *request = [event getEventProperty:TXEvents.Params.REQUEST];
            NSDictionary *responseObj = getJSONObj([[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding]);
            NSDictionary *dic = responseObj[@"user_payment"];
            
            // 처리가 정상이면 새로운 카드정보를 저장한다.
            if ([dic[@"mode"] intValue] == 1 && [dic[@"state"] intValue] == 1) {
                [[[TXApp instance] getSettings] setFDKeychain:SettingsConst.CryptoKeys.CSEQ value:[dic valueForKey:@"cseq"]];
                
                // 1. 정보 업데이트
                NSMutableDictionary *riderDic = [[NSMutableDictionary alloc] init];
                riderDic = [appDelegate.dicRider mutableCopy];
                
                NSMutableArray *upay = [[NSMutableArray alloc] init];
                if ([riderDic valueForKey:@"user_payments"]) {
                    upay = [riderDic[@"user_payments"] mutableCopy];
                }
                [upay addObject:dic];
                [riderDic setObject:upay forKey:@"user_payments"];
                appDelegate.dicRider = riderDic;
            }
            
            if (isInAPP) {
                [self removeEventListeners];
                [self.navigationController popViewControllerAnimated:YES];
                //[self.navigationController popToRootViewControllerAnimated:YES];
            }
            else {
                [self removeEventListeners];
                
#ifdef _DRIVER_MODE
                TXMapVC *mapVC = [[TXMapVC alloc] initWithNibName:@"TXMapDriverVC" bundle:nil];
#else
                TXMapVC *mapVC = [[TXMapVC alloc] initWithNibName:@"TXMapRiderVC" bundle:nil];
#endif
                
                [self pushViewController:mapVC];
                NSArray* tempVCA = [self.navigationController viewControllers];
                for(UIViewController *tempVC in tempVCA)
                {
                    if(![tempVC isKindOfClass:[TXMapVC class]]) {
                        [tempVC removeFromParentViewController];
                    }
                }
                [self removeFromParentViewController];
            }
            
        } else {
            //[self alertError:LocalizedStr(@"String.Alert") message:descriptor.error];
            //        NSString *message = [TXCode2MsgTranslator messageForCode:descriptor.rlt];
            //        [self alertError:@"Error" message:message];
        }
        [self hideBusyIndicator];
    }
}

#pragma mark -
#pragma mark Funcs
-(void)textFieldResize:(BOOL)flag {
    UIView *v1 = [self.vbody viewWithTag:910];
    UIView *v2 = [self.vbody viewWithTag:120];
    
    if (flag) {
        CGRect rect = v1.frame;
        if (rect.size.width>self.view.frame.size.width/2) {
            return;
        }
        
        v1.alpha = 1;
        v1.hidden = NO;
        self.btnCardNum.hidden = YES;
        
        [UIView animateWithDuration:0.2
                         animations:^{
                             // wide
                             CGRect rect = v1.frame;
                             rect.size.width += v2.frame.size.width;
                             v1.frame = rect;
                         }];
    }
    else {
        CGRect rect = v1.frame;
        if (rect.size.width<self.view.frame.size.width/2) {
            return;
        }
        
        if ([self.txtCardNumber.text length] > 4) {
            NSInteger lastIdx = [self.txtCardNumber.text length] - 4;
            [self.btnCardNum setTitle:[self.txtCardNumber.text substringFromIndex : lastIdx] forState:UIControlStateNormal];
        }
        else if ([self.txtCardNumber.text length] == 0) {
            [self.btnCardNum setTitle:@"1234" forState:UIControlStateNormal];
            [self.btnCardNum setTitleColor:HEXCOLOR(0xd8d8d8ff) forState:UIControlStateNormal];
        }
        else {
            [self.btnCardNum setTitle:self.txtCardNumber.text forState:UIControlStateNormal];
            [self.btnCardNum setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        }
        
        [UIView animateWithDuration:0.2f delay:0.f options:UIViewAnimationOptionCurveEaseIn animations:^{
            // small
            CGRect rect = v1.frame;
            rect.size.width -= v2.frame.size.width;
            v1.frame = rect;
            v1.alpha = 0;
        } completion:^(BOOL finished) {
            
            v1.hidden = YES;
            self.btnCardNum.hidden = NO;
        }];
        
    }
    
}
- (void)textFieldBackground:(UITextField *)textField
{
    UIView *v1 = [self.view viewWithTag:110];
    UIView *v9 = [self.view viewWithTag:910];
    UIView *v2 = [self.view viewWithTag:120];
    UIView *v3 = [self.view viewWithTag:130];
    UIView *v4 = [self.view viewWithTag:140];
    UIView *v5 = [self.view viewWithTag:150];
    
    v1.backgroundColor = [UIColor clearColor];
    v9.backgroundColor = [UIColor clearColor];
    v2.backgroundColor = [UIColor clearColor];
    v3.backgroundColor = [UIColor clearColor];
    v4.backgroundColor = [UIColor clearColor];
    v5.backgroundColor = [UIColor clearColor];
    
    if (textField == nil) {
        //
        [self textFieldResize:NO];
        
        [self.view resignFirstResponder];
        [self.view endEditing:YES];
        
        return;
    }
    
    UIView *v = nil;
    if (textField.tag == 10 ) {
        v = [self.view viewWithTag:110];
        
        [self textFieldResize:YES];
    }
    else if (textField.tag == 11 ) {
        v = [self.view viewWithTag:120];
        
        [self textFieldResize:NO];
    }
    else if (textField.tag == 12 ) {
        v = [self.view viewWithTag:130];
        
        [self textFieldResize:NO];
    }
    else if (textField.tag == 13 ) {
        v = [self.view viewWithTag:140];
        
        [self textFieldResize:NO];
    }
    else if (textField.tag == 14 ) {
        v = [self.view viewWithTag:150];
        
        [self textFieldResize:NO];
    }
    
    if (v != nil) {
        [textField becomeFirstResponder];
        
        v.backgroundColor = HEXCOLOR(0xeeeeeeff);
        if (textField.tag == 10 ) {
            [self.txtCardNumber becomeFirstResponder];
            v = [self.view viewWithTag:910];
            v.backgroundColor = HEXCOLOR(0xeeeeeeff);
        }
    }
    
    [self checkProcess];
}

#pragma mark -
#pragma mark Handle events

- (void)textDidChange:(UITextField *)textField
{
    NSLog(@"textField.placeholder %ld textDidChange %@", textField.tag, textField.text);
    
    if (textField.tag == 10 ) {
        if ([textField.text length] >= 19) {
            [self textFieldBackground:_txtMMYY];
        }
    }
    else if (textField.tag == 11 ) {
        if ([textField.text length] >= 5) {
            [self textFieldBackground:_txtPASS];
        }
    }
    else if (textField.tag == 12 ) {
        if ([textField.text length] >= 2) {
            
            if (![self.view viewWithTag:150].hidden) {
                [self textFieldBackground:_txtDOB2];
            }
            else {
                [self textFieldBackground:_txtDOB];
            }
        }
    }
    else if (textField.tag == 13 || textField.tag == 14 ) {
        
        if (![self.view viewWithTag:150].hidden) {
            if ([textField.text length] >= 12) {
                [self.view resignFirstResponder];
                [self.view endEditing:YES];
                [self checkProcess];
            }
        }
        else {
            if ([textField.text length] >= 6) {
                [self.view resignFirstResponder];
                [self.view endEditing:YES];
                [self checkProcess];
            }
        }
    }
//    else if (textField.tag == 10 && [textField.text length] >= 16) {
//        [self checkProcess];
//    }
//    else if (textField.tag == 11 && [textField.text length] >= 3) {
//        [self checkProcess];
//    }
}

#pragma mark - Keyboard Notification Method
- (void)keyboardWillShow:(NSNotification*)note{
    NSDictionary *userInfo = note.userInfo;
    CGRect finalKeyboardFrame = [userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    NSTimeInterval animationDuration = [userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    UIButton *btn = [self.view viewWithTag:2300];
    float inputViewFinalYPosition = self.view.bounds.size.height - finalKeyboardFrame.size.height - btn.frame.size.height;
    CGRect inputViewFrame = btn.frame;
    inputViewFrame.origin.y = inputViewFinalYPosition;
//    inputViewFrame.origin.x = kNaviBottomNullHeight;
    [self.view bringSubviewToFront:btn];
    [UIView animateWithDuration:animationDuration animations:^{
        btn.frame = inputViewFrame;
    }];
    
}

- (void)keyboardWillHide:(NSNotification*)note{
    
    [self textFieldBackground:nil];
    
    NSDictionary *userInfo = note.userInfo;
    NSTimeInterval animationDuration = [userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    UIButton *btn = [self.view viewWithTag:2300];
    CGRect inputViewFrame = btn.frame;
    inputViewFrame.origin.y = self.view.bounds.size.height - btn.frame.size.height - kBottomBottonSafeArea;
    //inputViewFrame.origin.x = kNaviBottomNullHeight;
    
    [UIView animateWithDuration:animationDuration animations:^{
        btn.frame = inputViewFrame;
    }];
    
}

#pragma mark - CardIO Delegate Method
- (void)userDidCancelPaymentViewController:(CardIOPaymentViewController *)scanViewController {
    NSLog(@"User canceled payment info");
    // Handle user cancellation here...
    [scanViewController dismissViewControllerAnimated:YES completion:nil];
}

- (void)userDidProvideCreditCardInfo:(CardIOCreditCardInfo *)info inPaymentViewController:(CardIOPaymentViewController *)scanViewController {
    // The full card number is available as info.cardNumber, but don't log that!
    NSLog(@"Received card info. Number: %@, expiry: %02lu/%lu, cvv: %@.", info.cardNumber, (unsigned long)info.expiryMonth, (unsigned long)info.expiryYear, info.cvv);
    
    self.txtCardNumber.text = info.cardNumber;
    //self.txtCVV.text = info.cvv;
    self.txtMMYY.text = [NSString stringWithFormat:@"%02d/%02d",(int)info.expiryMonth,(int)info.expiryYear%100];
    
    if ([self.txtCardNumber.text length] > 4) {
        NSInteger lastIdx = [self.txtCardNumber.text length] - 4;
        [self.btnCardNum setTitle:[self.txtCardNumber.text substringFromIndex : lastIdx] forState:UIControlStateNormal];
    }
    else if ([self.txtCardNumber.text length] == 0) {
        [self.btnCardNum setTitle:@"1234" forState:UIControlStateNormal];
        [self.btnCardNum setTitleColor:HEXCOLOR(0xd8d8d8ff) forState:UIControlStateNormal];
    }
    else {
        [self.btnCardNum setTitle:self.txtCardNumber.text forState:UIControlStateNormal];
        [self.btnCardNum setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    }
    
    [self checkProcess];
    // Use the card info...
    [scanViewController dismissViewControllerAnimated:YES completion:nil];
    
    [self textDidChange:self.txtMMYY];
}


#pragma mark - UIPickerView Delegate Method

// tell the picker how many components it will have
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 2;
}

// tell the picker how many rows are available for a given component
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    NSInteger rowsInComponent;
    if (component==0)
    {
        rowsInComponent=[monthsArray count];
    }
    else
    {
        rowsInComponent=[yearsArray count];
    }
    return rowsInComponent;
}



- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
//    NSLog(@"[currentDateComponents month]-->%ld<--",(long)[currentDateComponents month]);
//    NSLog(@"-->%ld<--",(long)row);
//    NSLog(@"row->%@<--",[yearsArray objectAtIndex:row]);
//    NSLog(@"-->%@<--",[yearsArray objectAtIndex:[pickerView selectedRowInComponent:1]]);
    
    year = [pickerView selectedRowInComponent:1];
    month = row;
    
    if ([pickerView selectedRowInComponent:0]+1<[currentDateComponents month] && [[yearsArray objectAtIndex:[pickerView selectedRowInComponent:1]] intValue]==[currentDateComponents year])
    {
        [pickerView selectRow:[currentDateComponents month]-1 inComponent:0 animated:YES];
        
        NSLog(@"Need to shift");
    }
    
    if (component==1)
    {
        [pickerView reloadComponent:0];
    }
    
}


- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 60, 44)];
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor blackColor];
    //label.font = [UIFont fontWithName:@"Arial-BoldMT" size:17];
    label.font = [UIFont systemFontOfSize:22];
    label.text = component==0?[monthsArray objectAtIndex:row]:[yearsArray objectAtIndex:row];
    
    if (component==0)
    {
        if (row+1<[currentDateComponents month] && [[yearsArray objectAtIndex:[pickerView selectedRowInComponent:1]] intValue]==[currentDateComponents year])
        {
            label.textColor = [UIColor grayColor];
        }
    }
    return label;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    CGFloat componentHeight ;
    
    if (component==0)
    {
        componentHeight = 45;
    }
    else  {
        componentHeight = 45;
    }
    
    return componentHeight;
}
// tell the picker the width of each row for a given component
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    CGFloat componentWidth ;
    
    if (component==0)
    {
        componentWidth = 100;
    }
    else  {
        componentWidth = 100;
    }
    
    return componentWidth;
}

#pragma mark -
#pragma mark Validator
#pragma mark -
-(void) resignKeyboard
{
    [self.view endEditing:YES];
}

- (IBAction)validateAction:(id)sender
{
    /*
    UIButton *btn = [self.view viewWithTag:5001];
    if (!btn) {
        __block LGAlertView *alertView = [[LGAlertView alloc] initWithTitle:nil
                                                                    message:LocalizedStr(@"Menu.Payment.Bottom.alert.text")
                                                                      style:LGAlertViewStyleAlert
                                                               buttonTitles:@[LocalizedStr(@"Button.OK")]
                                                          cancelButtonTitle:nil
                                                     destructiveButtonTitle:nil
                                                              actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                                                              }
                                                              cancelHandler:^(LGAlertView *alertView) {
                                                              }
                                                         destructiveHandler:^(LGAlertView *alertView) {
                                                         }];
        [Utils initAlertButtonColor:alertView];
        [alertView showAnimated:YES completionHandler:^(void)
         {
         }];
        return;
    }
     */
    [self resignKeyboard];
    
    Validator *validator = [[Validator alloc] init];
    validator.delegate   = self;
    [validator putRule:[Rules minLength:16 withFailureString:LocalizedStr(@"Validator.CardNo.text") forTextField:self.txtCardNumber]];
//    [validator putRule:[Rules checkRange:NSMakeRange(3, 5) withFailureString:LocalizedStr(@"Validator.CVV.text") forTextField:self.txtCVV]];
    
    
    //    if (self.setupMode != 1) {
    //        // 이름
    //        [validator putRule:[Rules minLength:2 withFailureString:LocalizedStr(@"Validator.Name.text") forTextField:self.txtUsername]];
    //        [validator putRule:[Rules checkIfAlphaNumericWithFailureString:LocalizedStr(@"Validator.Name.text") forTextField:self.txtUsername]];
    //    }
    //
    //    // 이메일
    //    [validator putRule:[Rules minLength:8 withFailureString:LocalizedStr(@"Validator.Email.text1") forTextField:self.txtEmail]];
    //    [validator putRule:[Rules checkIsValidEmailWithFailureString:LocalizedStr(@"Validator.Email.text2") forTextField:self.txtEmail]];
    //
    //    // 전화번호
    //    [validator putRule:[Rules checkRange:NSMakeRange(17, 17) withFailureString:LocalizedStr(@"Validator.Telno.text") forTextField:self.txtPhoneNum]];
    //    [validator putRule:[Rules checkValidCountryCodeUSWithFailureString:LocalizedStr(@"Validator.Telno.text") forTextField:self.txtPhoneNum]];
    //
    //    // 비번
    //    [validator putRule:[Rules minLength:6 withFailureString:LocalizedStr(@"Validator.Passwd.text") forTextField:self.txtPassword]];
    //
    //    if (validatorIndex != 1) {
    //        // 인증코드
    //        [validator putRule:[Rules minLength:6 withFailureString:LocalizedStr(@"Validator.Otp.text") forTextField:self.txtVerifyCode]];
    //        [validator putRule:[Rules checkIfNumericWithFailureString:LocalizedStr(@"Validator.Otp.text") forTextField:self.txtVerifyCode]];
    //    }
    
    [validator validate];
}

#pragma ValidatorDelegate - Delegate methods

- (void) preValidation
{
}

- (void)onSuccess
{
    
    NSString *CCNumber = [self.txtCardNumber.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    CCNumber = [CCNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    
    //NSString *ExpDate = [self.txtMMYY.text stringByReplacingOccurrencesOfString:@"/" withString:@""];
    NSString *ExpDate = self.txtMMYY.text;
//    NSArray *ExpDates = [self.txtMMYY.text componentsSeparatedByString:@"/"];
//    if ([ExpDates count] == 2) {
//        ExpDate = [NSString stringWithFormat:@"%@/%@",ExpDates[1],ExpDates[0]];
//    }
    NSString *business = @"0";
    NSString *DOB = self.txtDOB.text;
    if (![self.view viewWithTag:150].hidden) {
        DOB = self.txtDOB2.text;
        business = @"1";
    }
    DOB = [DOB stringByReplacingOccurrencesOfString:@" " withString:@""];
    DOB = [DOB stringByReplacingOccurrencesOfString:@"-" withString:@""];
    
    NSDictionary *propertyMap = @{
                                  //@"type" : business,
                                  @"no" : CCNumber,
                                  @"expire" : ExpDate,
                                  @"business" : business,
                                  @"birth" : DOB,
                                  @"pwd" : self.txtPASS.text,
                                  };
    NSLog(@"propertyMap:%@",propertyMap);
    [self showBusyIndicator:@"Requesting regist information for credit card ... "];
    [self->model requsetAPI:@"UR322" property:propertyMap];
}

- (void)onFailure:(Rule *)failedRule
{
    
//    NSMutableDictionary *options = [@{kCRToastNotificationTypeKey               : @(CRToastTypeNavigationBar),
//                                      kCRToastNotificationPresentationTypeKey   : @(CRToastPresentationTypeCover),
//                                      kCRToastUnderStatusBarKey                 : @(YES),
//                                      kCRToastTextKey                           : failedRule.failureMessage,
//                                      kCRToastFontKey                           : [UIFont systemFontOfSize:14],
//                                      kCRToastTextAlignmentKey                  : @(NSTextAlignmentCenter),
//                                      kCRToastTimeIntervalKey                   : @(2),
//                                      kCRToastAnimationInTypeKey                : @(CRToastAnimationTypeGravity),
//                                      kCRToastAnimationOutTypeKey               : @(CRToastAnimationTypeGravity),
//                                      kCRToastAnimationInDirectionKey           : @(CRToastAnimationDirectionTop),
//                                      kCRToastAnimationOutDirectionKey          : @(CRToastAnimationDirectionTop),
//                                      kCRToastNotificationPreferredPaddingKey   : @(15)} mutableCopy];
//    [CRToastManager showNotificationWithOptions:options
//                                completionBlock:^{
//                                    NSLog(@"Completed");
//                                }];
    
    [WToast showWithText:failedRule.failureMessage duration:kWTShort roundedCorners:NO gravity:kWTGravityTop];
}

@end
