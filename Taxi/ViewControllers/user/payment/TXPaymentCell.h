//
//  TXPaymentCell.h
//  TXPaymentCell
//

//

#import <UIKit/UIKit.h>

@interface TXPaymentCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIButton *button;

@end
