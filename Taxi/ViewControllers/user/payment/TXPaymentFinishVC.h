//
//  TXProfileVC.h
//  Taxi
//

//
#import <UIKit/UIKit.h>
#import "TXUserVC.h"

@interface TXPaymentFinishVC : TXUserVC {
    BOOL isInAPP; // NO:goto map, or back
}

@property (nonatomic, strong) IBOutlet UILabel* lbCodetext;
@property (nonatomic, strong) IBOutlet UILabel* lbtext0;
@property (nonatomic, strong) IBOutlet UILabel* lbtext1;
@property (nonatomic, strong) IBOutlet UILabel* lbtext2;

@property (nonatomic, strong) IBOutlet TXButton* btnSNS;

@property (assign, nonatomic) BOOL isInAPP;

@end
