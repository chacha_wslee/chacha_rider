//
//  MenuViewController.h
//  SlideMenu
//

//

#import <UIKit/UIKit.h>
#import "TXBaseViewController.h"

@interface TXPaymentVC : TXBaseViewController <UITableViewDelegate, UITableViewDataSource> {

}

@property (nonatomic, strong) NSString *cellIdentifier;
@property (nonatomic, strong) IBOutlet UITableView *tableView;

@end
