//
//  TXProfileVC.m
//  Taxi
//

//

#import "TXPaymentFinishVC.h"

@interface TXPaymentFinishVC () {
    int userId;
    NSArray *_msgList;
}

@end

@implementation TXPaymentFinishVC {
    
}
@synthesize isInAPP;

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    
    if(self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        
    }
    
    return self;
}

-(void)dealloc {
    [self removeEventListeners];
}

-(void)configureStyles {
    [super configureStyles];
    
}

-(void)configure {
    [super configure];
    //[self configureConfig];
    [super statusBarStyleDefault];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
    
    _msgList = @[
                 ];
    
    [self registerEventListeners];
    

    [_btnSNS setTitleColor:UIColorButtonText forState:UIControlStateNormal];
    _btnSNS.backgroundColor = UIColorDefault;
    
    [_btnSNS setTitle:LocalizedStr(@"Button.OK") forState:UIControlStateNormal];
    _btnSNS.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:18.0f];
    _btnSNS.tag = 202;
    
    _lbtext0.text = LocalizedStr(@"Menu.Payment.Finish.text1");
    _lbtext1.text = LocalizedStr(@"Menu.Payment.Finish.text2");
    _lbtext2.text = LocalizedStr(@"Menu.Payment.Finish.text3");
    _lbCodetext.text = LocalizedStr(@"Menu.Payment.Finish.title");
    
    _lbtext0.textColor = HEXCOLOR(0x666666ff);
    _lbtext1.textColor = HEXCOLOR(0x666666ff);
    _lbtext2.textColor = HEXCOLOR(0x666666ff);
    
    UIView *v1 = [self.view viewWithTag:31];
    UIView *v2 = [self.view viewWithTag:32];
    UIView *v3 = [self.view viewWithTag:33];
    
    v1.backgroundColor = HEXCOLOR(0x666666ff);
    v2.backgroundColor = HEXCOLOR(0x666666ff);
    v3.backgroundColor = HEXCOLOR(0x666666ff);
    
    [Utils setCircleImage:v1];
    [Utils setCircleImage:v2];
    [Utils setCircleImage:v3];
    
    //_lbCodetext.textColor = HEXCOLOR(0x666666ff);
}

-(void) configureConfig {

    
}
-(void) registerEventListeners {
    for (NSArray *msg in _msgList) {
        [self->model addEventListener:self forEvent:[msg objectAtIndex:0] eventParams:nil];
    }
}

-(void) removeEventListeners {
    for (NSArray *msg in _msgList) {
        [self->model removeEventListener:self forEvent:[msg objectAtIndex:0]];
    }
}

#pragma mark - Action
-(IBAction)onNaviButtonClick:(id)sender {
    UIButton *btn = (UIButton*)sender;
    //[super onNaviButtonClick:sender];
    
    if (btn.tag == 1100) {
        [self removeEventListeners];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark - IBAction
-(IBAction)onClick:(id)sender {
    
    UIButton *btn = (UIButton*)sender;
    if (btn.tag == 202) {
        [self removeEventListeners];
        
        // NO:goto map, or back
        if (isInAPP) {
            [self removeEventListeners];
            
            //Your navigation controller
            UINavigationController *nav = self.navigationController;
            //NSLog(@"nav.viewControllers.count:%ld",nav.viewControllers.count);
            //Get the view controller that is 2 step behind
            UIViewController *controller = [nav.viewControllers objectAtIndex:nav.viewControllers.count - (2+1)];
            
            //Go to that controller
            [nav popToViewController:controller animated:YES];
        }
        else {
            // from map
            [self removeEventListeners];
            //[self.navigationController popViewControllerAnimated:YES];
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
    }
}

#pragma mark - onEvent
-(void)onEvent:(TXEvent *)event eventParams:(id)subscriptionParams {
    
//    TXResponseDescriptor * descriptor = [event getEventProperty:TXEvents.Params.DESCRIPTOR];
//    TXRequestObj *request = [event getEventProperty:TXEvents.Params.REQUEST];
    
    [self hideBusyIndicator];
}

@end
