//
//  MenuViewController.m
//  SlideMenu
//

//
#import <SDWebImage/UIImageView+WebCache.h>
#import "TXPaymentVC.h"
#import "TXPaymentCell.h"
#import "RTLabel.h"
#import "LGAlertView.h"
#ifndef _DRIVER_MODE

#ifdef _WITZM
//#import "TXCardVC.h"
//#import "TXAskCardNumberVC.h"
#elif defined _CHACHA
//#import "TXCardVC.h"
#import "TXAskCardNumberVC.h"
#endif

#endif

@interface TXPaymentVC() {
    NSMutableArray *items;
    NSArray *_msgList;
}

@end

@implementation TXPaymentVC {
    //NSMutableDictionary *propertyMap;
}
@synthesize cellIdentifier;



-(void)viewDidLoad {
    
    [super viewDidLoad];
    
    //NSString *_name = [Utils getUserName];
    [super configureBottomList];
    UIView *n = [super navigationView];
    NSInteger _y = n.frame.origin.y + n.frame.size.height;
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, _y, self.view.bounds.size.width, self.view.bounds.size.height-_y-kBottomButtonHeight)
                                                  style:UITableViewStylePlain];
    self.tableView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:self.tableView];
    
    UINib *nib = [UINib nibWithNibName:@"TXPaymentCell" bundle:nil];
    self.cellIdentifier = @"TXPaymentCell";
    [self.tableView registerNib:nib forCellReuseIdentifier:self.cellIdentifier];
    
    self.tableView.separatorColor = [UIColor clearColor];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.opaque = NO;
    self.tableView.backgroundColor = [UIColor clearColor];
    
    UILabel *label = (UILabel*)[[super navigationView] viewWithTag:1111];
    label.tag = 2001;
    label.text = LocalizedStr(@"Menu.Payment.Header.title");
    
    self.tableView.tableHeaderView = ({
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 130)];
        
        UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(self.tableView.frame.size.width/2 - 133/2, 23, 133, 91)];
        button.tag = 999;
        [button setImage:[UIImage imageNamed:@"addCard"] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(addButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        
        button.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
        [view addSubview:button];
        view;
    });
    
    //self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, kBasicHeight + kBasicMargin*2 - kBottomButtonHeight)];
    self.tableView.tableFooterView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
//    [self.tableView.tableFooterView addSubview:({
//        UIView *view = [Utils addButtonBasic:self.view underView:nil dim:kBasicMargin];
//        
//        view.backgroundColor = UIColorLabelBG;
//        UIButton *button = [view viewWithTag:11];
//        button.tag = 999;
//        [button setTitle:LocalizedStr(@"Menu.Payment.BTN.add") forState:UIControlStateNormal];
//        [button addTarget:self action:@selector(addButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
//        //view.autoresizingMask = UIViewAutoresizingFlexibleHeight;
//        
//        view;
//    })];
    
    //[self.tableView setSeparatorInset:UIEdgeInsetsZero];
    
//    UIButton *btn = [Utils addBottomButton:LocalizedStr(@"Menu.Payment.BTN.add") target:self selector:@selector(addButtonPressed:) color:YES];
//    [self.view addSubview:btn];
    self.tableView.scrollsToTop = YES;
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    [self registerEventListeners];
    
    //propertyMap = [[NSMutableDictionary alloc] init];
    
    // 3. call api
    [self showBusyIndicator:@"Requesting ... "];
    [self->model UR300];
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self removeEventListeners];
}

#pragma mark - Configure

-(void)configureStyles {
    [super configureStyles];
    
    if (appDelegate.isDriverMode) {
        [super navigationType01X:[UIImage imageNamed:@"btn_back"] centerText:LocalizedStr(@"Menu.Payment.Detail.title")];
    }
    else {
        [super navigationType01X:[UIImage imageNamed:@"btn_back"] centerText:LocalizedStr(@"Menu.Payment.Detail.title")];
    }
}

-(void)configure {
    [super configure];
    
    // 2. init event
    _msgList = @[
                 @[@"UR300",@""], // rider
                 @[@"UR310",@""], // rider
                 @[@"UR332",@""], // card
                 ];
    
    
}

-(void) registerEventListeners {
    for (NSArray *msg in _msgList) {
        [self->model addEventListener:self forEvent:[msg objectAtIndex:0] eventParams:nil];
    }
}

-(void) removeEventListeners {
    for (NSArray *msg in _msgList) {
        [self->model removeEventListener:self forEvent:[msg objectAtIndex:0]];
    }
}

#pragma mark - Action
-(IBAction)onNaviButtonClick:(id)sender {
    UIButton *btn = (UIButton*)sender;
    NSLog(@"onNaviButtonClick");
    //[super onNaviButtonClick:sender];
    
    if (btn.tag == 1100) {
        [self removeEventListeners];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark - IBAction
-(IBAction)addButtonPressed:(id)sender {
#ifndef _DRIVER_MODE
    UIButton *btn = (UIButton*)sender;
    
    if (btn.tag == 999) {
        [self removeEventListeners];
#ifdef _WITZM
        //TXCardVC *firstVC = [[TXCardVC alloc] initWithNibName:@"TXCardVC" bundle:nil];
        TXAskCardNumberVC *firstVC = [[TXAskCardNumberVC alloc] initWithNibName:@"TXAskCardNumberVC" bundle:nil];
#elif defined _CHACHA
        //TXCardVC *firstVC = [[TXCardVC alloc] initWithNibName:@"TXCardVC" bundle:nil];
        TXAskCardNumberVC *firstVC = [[TXAskCardNumberVC alloc] initWithNibName:@"TXAskCardNumberVC" bundle:nil];
#endif

        firstVC.isInAPP = YES;
        [self pushViewController:firstVC];
    }
#endif
}

-(IBAction)buttonPressed:(id)sender {
    UIButton *btn = (UIButton*)sender;
    
    if ([Utils isNotAllowedMenu]) {return;}
    
//    _idx = btn.tag - 1000;
    for (NSDictionary* dic in self->items) {
        if ([[dic objectForKey:@"cseq"] isEqualToString:[NSString stringWithFormat:@"%d",(int)btn.tag]]) {
            LGAlertView *alertView = [[LGAlertView alloc] initWithTitle:@""
                                        message:LocalizedStr(@"Menu.Payment.BTN.delete.alert.text")
                                          style:LGAlertViewStyleAlert
                                   buttonTitles:@[LocalizedStr(@"Button.OK")]
                              cancelButtonTitle:LocalizedStr(@"Button.CANCEL")
                         destructiveButtonTitle:nil
                                  actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                                      NSLog(@"actionHandler, %@, %lu", title, (long unsigned)index);
                                      
                                      [self showBusyIndicator:@"deletting ... "];
                                      [self->model UR332:[dic objectForKey:@"cseq"]];
                                  }
                                  cancelHandler:^(LGAlertView *alertView) {
                                  }
                             destructiveHandler:^(LGAlertView *alertView) {
                             }];
            [Utils initAlertButtonColor:alertView];
            [alertView showAnimated:YES completionHandler:nil];
            
            break;
        }
    }
}

#pragma mark - UITableView Delegate & Datasrouce -
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    UIView *v = (UIView *)[cell.contentView viewWithTag:800];
    v = [self roundCornersOnView:v onTopLeft:YES topRight:YES bottomLeft:NO bottomRight:NO radius:3.0];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 103;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 90;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return self->items.count;
}

- (UIView *)roundCornersOnView:(UIView *)view onTopLeft:(BOOL)tl topRight:(BOOL)tr bottomLeft:(BOOL)bl bottomRight:(BOOL)br radius:(float)radius
{
    if (tl || tr || bl || br) {
        UIRectCorner corner = 0;
        if (tl) corner = corner | UIRectCornerTopLeft;
        if (tr) corner = corner | UIRectCornerTopRight;
        if (bl) corner = corner | UIRectCornerBottomLeft;
        if (br) corner = corner | UIRectCornerBottomRight;
        
        UIView *roundedView = view;
        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:roundedView.bounds byRoundingCorners:corner cornerRadii:CGSizeMake(radius, radius)];
        CAShapeLayer *maskLayer = [CAShapeLayer layer];
        maskLayer.frame = roundedView.bounds;
        maskLayer.path = maskPath.CGPath;
        roundedView.layer.mask = maskLayer;
        return roundedView;
    }
    return view;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	TXPaymentCell *cell = [tableView dequeueReusableCellWithIdentifier:self.cellIdentifier];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    NSDictionary *dic = self->items[indexPath.row];
    
    NSString *cardNo = [Utils trimStringOnly:[dic valueForKey:@"no"]];
    if ([cardNo length] > 4) {
        NSInteger lastIdx = [cardNo length] - 4;
        cardNo = [cardNo substringFromIndex : lastIdx];
    }
    
    NSString *lb1 = [NSString stringWithFormat:@"XXXX-%@",
                     cardNo
                     ];
    
    NSInteger lastIdx = [[dic valueForKey:@"expire"] length] - 3;
    NSString *lb2 = [NSString stringWithFormat:@"Exp. %@/%@",
                     [[[dic valueForKey:@"expire"] substringFromIndex : 0] substringToIndex : 2],
                     [[[dic valueForKey:@"expire"] substringFromIndex : 3] substringToIndex : lastIdx]
                     ];
    
    UIView *v = (UIView *)[cell.contentView viewWithTag:800];
    v.backgroundColor = HEXCOLOR(0xf7f7f7FF);
    //v = [self roundCornersOnView:v onTopLeft:YES topRight:YES bottomLeft:NO bottomRight:NO radius:3.0];
    ((RTLabel *)[cell.contentView viewWithTag:902]).text = lb1;
    ((RTLabel *)[cell.contentView viewWithTag:902]).textColor = HEXCOLOR(0x333333FF);
    ((RTLabel *)[cell.contentView viewWithTag:902]).font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:18];
    
    ((RTLabel *)[cell.contentView viewWithTag:903]).text = lb2;
    ((RTLabel *)[cell.contentView viewWithTag:903]).textColor = HEXCOLOR(0x999999FF);
    ((RTLabel *)[cell.contentView viewWithTag:903]).font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:12];
    
    cell.button.tag = [[dic valueForKey:@"cseq"] integerValue];
    //[cell.button setTitle:LocalizedStr(@"Menu.Payment.BTN.delete") forState:UIControlStateNormal];
    //[cell.button setTitleColor:UIColorBasicText forState:UIControlStateNormal];
    
    //cell.button.tag = indexPath.row + 1000;
    //NSLog(@"dic:%@",dic);
    if ([dic[@"mode"] intValue] == 1 && [dic[@"state"] intValue] == 1) {
        ((UIImageView *)[cell.contentView viewWithTag:900]).image = [UIImage imageNamed:@"form_radio_chk"];
        [[[TXApp instance] getSettings] setFDKeychain:SettingsConst.CryptoKeys.CSEQ value:[dic valueForKey:@"cseq"]];
    }
    else {
        ((UIImageView *)[cell.contentView viewWithTag:900]).image = [UIImage imageNamed:@"form_radio_normal"];
    }
    
//    if (!appDelegate.isDriverMode && appDelegate.ustate > 110) {
//        cell.button.enabled = NO;
//    }
//    else {
//        cell.button.enabled = YES;
//    }
    
    // 카드 이미지 불러온다.
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    NSString *cadimage = [dic valueForKey:@"pay_imageurl"];
    if (![cadimage isKindOfClass:[NSNull class]] && [cadimage length]>0) {
        [manager downloadImageWithURL:[NSURL URLWithString:cadimage]
                              options:0
                             progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                 // progression tracking code
                             }
                            completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                if (image) {
                                    // do something with image
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        ((UIImageView *)[cell.contentView viewWithTag:901]).image = image;
                                    });
                                }
                            }];
    }
    
    /*
    SDWebImageDownloader *downloader = [SDWebImageDownloader sharedDownloader];
    NSString *cadimage = [dic valueForKey:@"pay_imageurl"];
    if (![cadimage isKindOfClass:[NSNull class]] && [cadimage length]>0) {
        
        [downloader downloadImageWithURL:[NSURL URLWithString:cadimage]
                                 options:0
                                progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                    // progression tracking code
                                }
                               completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
                                   if (image && finished) {
                                       // do something with image
                                       dispatch_async(dispatch_get_main_queue(), ^{
                                           ((UIImageView *)[cell.contentView viewWithTag:901]).image = image;
                                       });
                                   }
                               }];
    }
    */
    
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSDictionary *dic = self->items[indexPath.row];
    if ([dic[@"mode"] intValue] == 1 && [dic[@"state"] intValue] == 1) {
        return;
    }
    
    LGAlertView *alertView = [[LGAlertView alloc] initWithTitle:@""
                                message:LocalizedStr(@"Menu.Payment.Select.title")
                                  style:LGAlertViewStyleAlert
                           buttonTitles:@[LocalizedStr(@"Button.OK")]
                      cancelButtonTitle:LocalizedStr(@"Button.CANCEL")
                 destructiveButtonTitle:nil
                          actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                              NSLog(@"actionHandler, %@, %lu", title, (long unsigned)index);
                              
                              [self showBusyIndicator:@"registering ... "];
                              [self->model UR310:[dic objectForKey:@"cseq"]];
                          }
                          cancelHandler:^(LGAlertView *alertView) {
                          }
                     destructiveHandler:^(LGAlertView *alertView) {
                     }];
    [Utils initAlertButtonColor:alertView];
    [alertView showAnimated:YES completionHandler:nil];
    
    
    //NSDictionary* drive_trip    = [dic valueForKey:@"drive_trip"]; //trip_duration/trip_distance, begin_location, end_location, update_date
/*
    TXTripDetailVC *vc = [[TXTripDetailVC alloc] initWithNibName:@"TXTripDetailVC" bundle:[NSBundle mainBundle]];
    vc.tripData = dic;
    [self.navigationController pushViewController:vc animated:YES];
*/
}

#pragma mark - onEvent
-(void)onEvent:(TXEvent *)event eventParams:(id)subscriptionParams {
    
    TXResponseDescriptor * descriptor = [event getEventProperty:TXEvents.Params.DESCRIPTOR];
    TXRequestObj *request = [event getEventProperty:TXEvents.Params.REQUEST];
    
    //[self hideBusyIndicator];
    if([event.name isEqualToString:@"UR300"]) {
        
        if(descriptor.success == true) {
            NSString     *response = [[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding];
            NSDictionary *result   = getJSONObj(response);
            
            NSArray *tripData              = [result valueForKey:@"user_payments"];
            
            self->items = [NSMutableArray arrayWithCapacity:[tripData count]];

            for (NSDictionary* p in tripData) {
                [items addObject:p];
            }

            if ([self->items count]) {
                // 1. 정보 업데이트
                NSMutableDictionary *dic = [appDelegate.dicRider mutableCopy];
                [dic setObject:[result valueForKey:@"user_payments"] forKey:@"user_payments"];
                appDelegate.dicRider = dic;
                
                //self.tableView.separatorColor = [UIColor colorWithRed:150/255.0f green:161/255.0f blue:177/255.0f alpha:1.0f];
                [self.tableView reloadData];
            }
            else {
                NSMutableDictionary *dic = [appDelegate.dicRider mutableCopy];
                [dic removeObjectForKey:@"user_payments"];
                appDelegate.dicRider = dic;
            }
            
        } else {
        //    [self alertError:@"Error" message:@"Failed to get profile image"];
        }
        [self hideBusyIndicator];
    }
    else if([event.name isEqualToString:@"UR310"]) {
        
        if(descriptor.success == true) {
            NSString     *response = [[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding];
            NSDictionary *result   = getJSONObj(response);
            
            NSMutableDictionary *tripData              = [result valueForKey:@"user_payment"];
            
            //self->items = [NSMutableArray arrayWithCapacity:[tripData count]];
            
            NSInteger idx = 0;
            for (;idx<[self->items count];idx++) {
                NSDictionary* p = self->items[idx];
                if (![[p valueForKey:@"cseq"] isEqualToString:[tripData valueForKey:@"cseq"]]) {
                    
                    NSMutableDictionary *mutableDict = [p mutableCopy];
                    [mutableDict setObject:@"0" forKey:@"mode"];
                    p = [mutableDict mutableCopy];
                    
                    [self->items replaceObjectAtIndex:idx withObject:p];
                }
                else {
                    [self->items replaceObjectAtIndex:idx withObject:tripData];
                }
            }
            
            //NSString *htmlString = [NSString stringWithFormat:@"<font face='HelveticaNeue-CondensedBold' size=20 color='#CCFF00'>%d</font> TRIP HISTORIES",(int)[self->items count]];
            //((RTLabel *)[self.view viewWithTag:2001]).text = htmlString;
            
            
            [self.tableView reloadData];
            
        } else {
            //    [self alertError:@"Error" message:@"Failed to get profile image"];
        }
        [self hideBusyIndicator];
    }
    else if([event.name isEqualToString:@"UR332"]) {
        
        if(descriptor.success == true) {
            NSString     *response = [[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding];
            NSDictionary *result   = getJSONObj(response);
            
            NSDictionary *param = [Utils getDataOfQueryString:request.reqUrl];
            NSInteger idx = [[param objectForKey:@"cseq"] integerValue];
            
            int _idx = 0;
            for (NSDictionary* dic in self->items) {
                if ([[dic objectForKey:@"cseq"] isEqualToString:[NSString stringWithFormat:@"%d",(int)idx]]) {
                    [self->items removeObjectAtIndex:_idx];
                    NSArray *deleteIndexPaths = [[NSArray alloc] initWithObjects:
                                                 [NSIndexPath indexPathForRow:_idx inSection:0],
                                                 nil];
                    [self.tableView beginUpdates];
                    //[self.tableView insertRowsAtIndexPaths:insertIndexPaths withRowAnimation:UITableViewRowAnimationFade];
                    [self.tableView deleteRowsAtIndexPaths:deleteIndexPaths withRowAnimation:UITableViewRowAnimationRight];
                    [self.tableView endUpdates];
                    
                    break;
                }
                _idx++;
            }
            
            
            NSArray *tripData              = [result valueForKey:@"user_payments"];
            
            self->items = [NSMutableArray arrayWithCapacity:[tripData count]];
            
            for (NSDictionary* p in tripData) {
                [items addObject:p];
            }
            
            //NSString *htmlString = [NSString stringWithFormat:@"<font face='HelveticaNeue-CondensedBold' size=20 color='#CCFF00'>%d</font> TRIP HISTORIES",(int)[tripData count]];
            //((RTLabel *)[self.view viewWithTag:2001]).text = htmlString;
            
            [self.tableView reloadData];
            
            // 카드가 모두 삭제되었다면
            if (!self->items) {
                [[[TXApp instance] getSettings] setFDKeychain:SettingsConst.CryptoKeys.CSEQ value:@""];
            }
            
        } else {
        //    [self alertError:@"Error" message:@"Failed to get profile image"];
        }
        [self hideBusyIndicator];
    }
}


@end
