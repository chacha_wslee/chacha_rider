//
//  TXAskCardNumberVC.h
//  Taxi
//
//

#import "TXUserVC.h"
#import "RETableViewManager.h"
#import "RTLabel.h"
#import "RTUILabel.h"

@interface CountryCodeItem : NSObject
@property (nonatomic, strong) NSString* name;
@property (nonatomic, strong) NSString* code;
@end

@interface TXAskCardNumberVC : TXUserVC <UITextFieldDelegate, RETableViewManagerDelegate> {
    BOOL isInAPP; // NO:goto map, or back
}
@property (assign, nonatomic) BOOL isInAPP;

@property (strong, nonatomic) IBOutlet UIView    *vbody;

@property (strong, nonatomic) IBOutlet TXButton    *btnNext;
@property (nonatomic, strong) IBOutlet REFormattedNumberField *txtCardNumber;
@property (nonatomic, strong) IBOutlet REFormattedNumberField *txtMMYY;
@property (nonatomic, strong) IBOutlet REFormattedNumberField *txtPASS;
@property (nonatomic, strong) IBOutlet REFormattedNumberField *txtDOB;
@property (nonatomic, strong) IBOutlet REFormattedNumberField *txtDOB2;

@property (strong, nonatomic) IBOutlet UIButton    *btnCardNum;

@property (strong, nonatomic) IBOutlet UILabel    *lbText1;
@property (strong, nonatomic) IBOutlet UILabel    *lbText2;

@property (strong, nonatomic) IBOutlet RTUILabel    *lbDesc;

-(IBAction)next:(id)sender;
-(IBAction)textFieldFocusLost:(UITextField *)sender;
-(IBAction)textFieldFocusGained:(UITextField *)sender;
-(IBAction)textFieldValueChanged:(UITextField *)sender;

@end
