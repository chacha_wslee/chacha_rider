//
//  MenuViewController.m
//  SlideMenu
//

//
#import "TXCardVC.h"
#import "utilities.h"

#import "WToast.h"
//#import "CRToast.h"
#import "UIWebView+JavaScript.h"
#import <JavaScriptCore/JavaScriptCore.h>


@interface TXCardVC() <UIWebViewDelegate>{
    NSMutableArray *contentHeights;
    
    NSArray *_msgList;
}

@end

@implementation TXCardVC {
    NSMutableDictionary *propertyMap;
}
@synthesize isInAPP;

-(void)viewDidLoad {
    
    [super viewDidLoad];
    
    contentHeights = [[NSMutableArray alloc] init];
    
    // cookie허용이 변경되었다
    [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookieAcceptPolicy:NSHTTPCookieAcceptPolicyAlways];
    
//    NSString* useragent = @"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.90 Safari/537.36";
//    NSDictionary *dictionnary = [[NSDictionary alloc] initWithObjectsAndKeys:
//                                 useragent, @"UserAgent",
//                                 @"1", @"Upgrade-Insecure-Requests",
//                                 @"max-age=0", @"Cache-Control",
//                                 @"keep-alive", @"Connection",
//                                 @"ko-KR,ko;q=0.8,en-US;q=0.6,en;q=0.4", @"Accept-Language",
//                                 @"gzip, deflate, br", @"Accept-Encoding",
//                                 nil];
//    [[NSUserDefaults standardUserDefaults] registerDefaults:dictionnary];
    
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dismissVcCancel:) name:NOTIFICATION_DISSMISS_SIGN object:nil];
    
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    //[self removeEventListeners];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
}

-(void)dealloc {
    [self removeEventListeners];
}

#pragma mark - Configure

-(void)configureStyles {
    [super configureStyles];

    [super navigationType01X:[UIImage imageNamed:@"btn_back"] centerText:LocalizedStr(@"Menu.Payment.List1.title")];
}

-(void)configure {
    [super configure];
    [super configureBottomLine];
    UIView *line = (UIView*)[[super navigationView] viewWithTag:1500];
    CGRect rect = line.frame;
    rect.size.height = 3;
    line.frame = rect;
    line.backgroundColor = HEXCOLOR(0x111111FF);
    
    UIView *n = [super navigationView];
    NSInteger _y = n.frame.origin.y + n.frame.size.height;
    
    // 2. init event
    _msgList = @[
                 //@[@"UR302",@""], // get
                 @[@"UR300",@""], // get
                 ];
    
    [self registerEventListeners];
    
    //UIWebView *webViewHTML = [[UIWebView alloc]init];
    self.vbody.backgroundColor = [UIColor whiteColor];
    self.vbody.delegate = self;
    self.vbody.tag = 200;
    //self.vbody.backgroundColor = [UIColor clearColor];
    //self.vbody.opaque = NO;
    //        webViewHTML.userInteractionEnabled = NO;
    //self.vbody.scrollView.bounces = NO;
    //self.vbody.scrollView.showsHorizontalScrollIndicator = YES;
    
    self.vbody.frame = CGRectMake(0,
                                  _y + 10, //kNaviTopHeight + kBasicHeight,
                                  self.view.frame.size.width,
                                  self.view.frame.size.height - (_y + 10)
                                  //self.view.frame.size.height - kNaviTopHeight - kBasicHeight
                                  );
    
    [self onUR302];
}

-(void) registerEventListeners {
    for (NSArray *msg in _msgList) {
        [self->model addEventListener:self forEvent:[msg objectAtIndex:0] eventParams:nil];
    }
}

-(void) removeEventListeners {
    for (NSArray *msg in _msgList) {
        [self->model removeEventListener:self forEvent:[msg objectAtIndex:0]];
    }
}

#pragma mark - Action
-(IBAction)onNaviButtonClick:(id)sender {
    UIButton *btn = (UIButton*)sender;
    NSLog(@"onNaviButtonClick");
    //[super onNaviButtonClick:sender];
    
    if (btn.tag == 1100 || btn.tag == 1101) {
        [self removeEventListeners];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

/*
- (CGFloat)height
{
    CGFloat webViewHeight = 0;
    // use Javascript to determine current document height
    if(!self.webView.loading && self.webView.frame.size.width)
        webViewHeight = [[self.webView stringByEvaluatingJavascriptFromString: @"document.documentElement.clientHeight"] integerValue];
    if(!webViewHeight)
        webViewHeight = [super height];
    if(webViewHeight > self.maximumHeight)
        webViewHeight = self.maximumHeight;
    
    return webViewHeight + 2*self.controlMargin;
}
*/

#pragma mark - Send Event
-(void)onUR302 {
    
    [self showBusyIndicator:@"Update info ... "];
    TXRequestObj *request     = [self->model createRequest:@"UR302"];
    NSMutableURLRequest *httpRequest = [request createHTTPRequest];
    NSLog(@"reqUrl:%@",httpRequest.URL.absoluteString);
    
    NSURL *url=httpRequest.URL;
    NSMutableURLRequest *requestObj=[NSMutableURLRequest requestWithURL:url];
    [self.vbody loadRequest:requestObj];
    
    //[self->model requsetAPI:API_FUNC(NSStringFromSelector(_cmd)) property:nil];
}

#pragma mark - onEvent
-(void)onEvent:(TXEvent *)event eventParams:(id)subscriptionParams {
    TXResponseDescriptor * descriptor = [event getEventProperty:TXEvents.Params.DESCRIPTOR];
    TXRequestObj *request = [event getEventProperty:TXEvents.Params.REQUEST];
    
    //[self hideBusyIndicator];
    if([event.name isEqualToString:@"UR300"]) {
        
        if(descriptor.success == true) {
            NSString     *response = [[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding];
            NSDictionary *result   = getJSONObj(response);
            
            NSArray *tripData              = [result valueForKey:@"user_payments"];
            
            if ([tripData count]) {
                // 1. 정보 업데이트
                NSMutableDictionary *dic = [appDelegate.dicRider mutableCopy];
                [dic setObject:[result valueForKey:@"user_payments"] forKey:@"user_payments"];
                appDelegate.dicRider = dic;
            }
            else {
                NSMutableDictionary *dic = [appDelegate.dicRider mutableCopy];
                [dic removeObjectForKey:@"user_payments"];
                appDelegate.dicRider = dic;
            }
            
        } else {
            //    [self alertError:@"Error" message:@"Failed to get profile image"];
        }
        [self hideBusyIndicator];
        [self removeEventListeners];
        [self.navigationController popViewControllerAnimated:YES];
    }

}


#pragma mark - UIWebViewDelegate

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSMutableURLRequest *)theRequest
 navigationType:(UIWebViewNavigationType)navigationType
{
//    NSMutableURLRequest *request = (NSMutableURLRequest *)theRequest;
//    NSLog(@"request:%@",request.URL.absoluteString);
//    NSLog(@"shouldStartLoadWithRequest->%@",[request allHTTPHeaderFields]);
    
    // jscall://loginview
    // Sent before a web view begins loading content, useful to trigger actions before the WebView.
    NSString *strUrl = [[theRequest URL] absoluteString];
    //    NSLog(@"webview:%@",strUrl);
    if ([strUrl hasPrefix:_CARD_APP_URL]) {
        NSString *strRequest = [[strUrl componentsSeparatedByString:_CARD_APP_URL] objectAtIndex:1];
        NSArray *arrRequest = [strRequest componentsSeparatedByString:@"?"];
        NSString *strCmd = [arrRequest objectAtIndex:0];
        NSString *noPercentString = @"";
        NSMutableDictionary *queryStringDictionary = [[NSMutableDictionary alloc] init];
        if ([arrRequest count]>1) { // parameter message
            noPercentString = [[[strUrl componentsSeparatedByString:[NSString stringWithFormat:@"%@%@?",_CARD_APP_URL,strCmd]] objectAtIndex:1] stringByRemovingPercentEncoding];
            
            NSArray *urlComponents = [[arrRequest objectAtIndex:1] componentsSeparatedByString:@"&"];
            for (NSString *keyValuePair in urlComponents)
            {
                NSArray *pairComponents = [keyValuePair componentsSeparatedByString:@"="];
                NSString *key = [[pairComponents firstObject] stringByRemovingPercentEncoding];
                NSString *value = [[pairComponents lastObject] stringByRemovingPercentEncoding];
                
                [queryStringDictionary setObject:value forKey:key];
            }
        }
        //        NSArray* array = [noPercentString componentsSeparatedByString: @"="];
        NSLog(@"strCmd:%@",strCmd);
        if ( [strCmd isEqualToString:@"UR322"] ){
            // 처리 완료 후 카드정보를 다시 확인한다. session에 넣어야 하므로
            [self showBusyIndicator:@"Requesting ... "];
            [self->model UR300];
            
//            [self removeEventListeners];
//            [self.navigationController popViewControllerAnimated:YES];
        }
        
        // toApp protocol do not StartLoad
        return NO;
    }
    
    return YES;
}

- (void)webViewDidStartLoad:(UIWebView *)webView {
}

- (void)webViewDidWillUpdate:(UIWebView *)webView key:(NSString*)key value:(NSString*)value
{
    NSString *val = [Utils nullToString:value];
    //[self updateTrimString:val key:key];
    [propertyMap setValue:val forKey:key];
    
    if (![val isEqualToString:@""]) {
        // webview set
        [webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"set_%@('%@');",key,val]];
    }
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [self hideBusyIndicator];
    
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    
    // To avoid getting an error alert when you click on a link
    // before a request has finished loading.
    if ([error code] == NSURLErrorCancelled) {
        return;
    }
}

@end
