
#import "TXAppDelegate.h"

#import "Incoming.h"
#import "Outgoing.h"

#import "ChatView.h"
#import "UIImage+ResizeMagick.h"

#import "NavigationController.h"

#import "CustomMessagesCollectionViewCellOutgoing.h"
#import "CustomMessagesCollectionViewCellIncoming.h"

#import "TXCode2MsgTranslator.h"
#import "TXModelBase.h"
#import "TXUserModel.h"
#import "TXBaseObj.h"
#import "TXModelBase.h"
#import "TXEventTarget.h"
#import "SVProgressHUD.h"

#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import <CoreTelephony/CTCarrier.h>
#import <MessageUI/MessageUI.h>


//-------------------------------------------------------------------------------------------------------------------------------------------------
@interface ChatView() <TXEventListener,UITableViewDelegate,UITableViewDataSource>
{
    //-------------------------------------------------------------------------------------------------------------------------------------------------
    UIView *statusView;
    
    UIView *navigationView;
    UIView *profileView;
    
    CGRect mainframe;
    NSArray *_msgList;
    id observer1;
    //-------------------------------------------------------------------------------------------------------------------------------------------------
    
	NSString *groupId;
    NSString *description;
    NSMutableDictionary *recent;

	BOOL initialized;

	NSInteger loaded; // item중에 화면에 load된 개수
	NSMutableArray *loads;
	NSMutableArray *items;
	NSMutableArray *messages;

	NSMutableDictionary *started;
	NSMutableDictionary *avatars;

	JSQMessagesBubbleImage *bubbleImageOutgoing;
	JSQMessagesBubbleImage *bubbleImageIncoming;
	JSQMessagesAvatarImage *avatarImageBlank;
    
    TXAppDelegate *appDelegate;
    
    NSArray *dataArray;
    IBOutlet UIButton *btnCancel;
}
@end

//-------------------------------------------------------------------------------------------------------------------------------------------------
//static CGFloat kAniTime = 1.4f;

@implementation ChatView

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (id)initWith:(NSString *)groupId_
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	self = [super init];
	groupId = groupId_;
    description = @"";
    recent = [[NSMutableDictionary alloc] init];
	return self;
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (id)initWith:(NSString *)groupId_ description:(NSString*)desc_
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    self = [super init];
    groupId = groupId_;
    description = desc_;
    recent = [[NSMutableDictionary alloc] init];
    return self;
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (id)initWith:(NSString *)groupId_ description:(NSString*)desc_ recent:(NSDictionary *)recent_
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    self = [super init];
    groupId = groupId_;
    description = desc_;
    recent = [[NSMutableDictionary alloc] init];
    recent = [recent_ mutableCopy];
    return self;
}

-(void)onUD305:(NSString*)mseq {
    
//    NSString* useq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.USEQ];
//    NSString* oseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.OSEQ];

    NSString *useq = recent[@"useq"];
    NSString *oseq = recent[@"oseq"];
    
    NSDictionary *_propertyMap = @{
                                   @"useq" : useq,
                                   @"oseq" : oseq,
                                   };
    if (![[Utils nullToString:mseq] isEqualToString:@""]) {
        _propertyMap = @{
                         @"useq" : useq,
                         @"oseq" : oseq,
                         @"mseq" : mseq,
                         };
    }
    [self showBusyIndicator:@"Update Driver ... "];
    [[TXUserModel instance] requsetAPI:API_FUNC(NSStringFromSelector(_cmd)) property:_propertyMap];
}

-(void)onUD325:(NSString*)msg {
    
    if ([[Utils nullToString:msg] isEqualToString:@""]) {
        return;
    }
    
    NSString *useq = recent[@"useq"];
    NSString *oseq = recent[@"oseq"];
    
//    NSString* useq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.USEQ];
//    NSString* oseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.OSEQ];
    
    NSDictionary *_propertyMap = @{
                                   @"useq" : useq,
                                   @"oseq" : oseq,
                                   @"msg" : msg,
                                   };
    [self showBusyIndicator:@"Update Driver ... "];
    [[TXUserModel instance] requsetAPI:API_FUNC(NSStringFromSelector(_cmd)) property:_propertyMap];
}

-(void)onPD305:(NSString*)mseq {
    
    NSString *pseq = recent[@"pseq"];
    NSString *oseq = recent[@"oseq"];
    
//    NSString* useq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.USEQ];
//    NSString* oseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.OSEQ];
    
    NSDictionary *_propertyMap = @{
                                   @"pseq" : pseq,
                                   @"oseq" : oseq,
                                   };
    if (![[Utils nullToString:mseq] isEqualToString:@""]) {
        _propertyMap = @{
                         @"pseq" : pseq,
                         @"oseq" : oseq,
                         @"mid" : mseq,
                         };
    }
    [self showBusyIndicator:@"Update Driver ... "];
    [[TXUserModel instance] requsetAPI:API_FUNC(NSStringFromSelector(_cmd)) property:_propertyMap];
}

-(void)onPD325:(NSString*)msg {
    
    if ([[Utils nullToString:msg] isEqualToString:@""]) {
        return;
    }
    
    NSString *pseq = recent[@"pseq"];
    NSString *oseq = recent[@"oseq"];
    
//    NSString* useq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.USEQ];
//    NSString* oseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.OSEQ];
    
    NSDictionary *_propertyMap = @{
                                   @"pseq" : pseq,
                                   @"oseq" : oseq,
                                   @"msg" : msg,
                                   };
    [self showBusyIndicator:@"Update Driver ... "];
    [[TXUserModel instance] requsetAPI:API_FUNC(NSStringFromSelector(_cmd)) property:_propertyMap];
}

-(void)onPD355:(NSDictionary*)item
{
    BOOL incoming = [self addMessage:item];
    if (incoming) [JSQSystemSoundPlayer jsq_playMessageReceivedSound];
    [self finishReceivingMessage];
}

-(void)onUD355:(NSDictionary*)item
{
    BOOL incoming = [self addMessage:item];
    if (incoming) [JSQSystemSoundPlayer jsq_playMessageReceivedSound];
    [self finishReceivingMessage];
}

-(void)onPD330:(NSString*)rsncode {
    
    [self showBusyIndicator:@"Canceling Trip ... "];
    [[TXUserModel instance] PD330:rsncode];
    
}

-(void)onPD232:(NSString*)rsncode {
    
    [self showBusyIndicator:@"Canceling Pickup ... "];
    [[TXUserModel instance] PD232:rsncode];
}

-(void)onPD130:(NSString*)rsncode {
    
    [self showBusyIndicator:@"Canceling Order ... "];
    [[TXUserModel instance] PD130:rsncode];
}

-(void)onUD130 {
    
    [self showBusyIndicator:@"Canceling Cue ... "];
    [[TXUserModel instance] UD130];
}

-(void)onUD230 {
    
    [self showBusyIndicator:@"Canceling Cue ... "];
    [[TXUserModel instance] UD230];
}

-(void) alertError : (NSString *) title message : (NSString *) message {
    
    [Utils onErrorAlertViewTitle:message title:title];
}


#pragma mark - IBAction
/*
//#ifndef _CHACHA
-(IBAction)onCall {
    
    NSString* acall = @"";
    if (appDelegate.isDriverMode) {
        acall = [Utils acall:appDelegate.dicDriver];
    }
    else {
        acall = [Utils acall:appDelegate.dicRider];
    }
    
    if ([acall isEqualToString:@"no"]) {
        if (appDelegate.isDriverMode)
            [self alertError:@"Error" message:@"Failed Call to Rider"];
        else
            [self alertError:@"Error" message:@"Failed Call to Driver"];
    }
    else if (![acall isEqualToString:@""] && acall != nil) {
        //        NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt://%@",acall]];
        //        if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        //            [[UIApplication sharedApplication] openURL:phoneUrl];
        //        }
        //
        NSURL *phoneUrl = [NSURL URLWithString:[@"telprompt://" stringByAppendingString:acall]];
        NSURL *phoneFallbackUrl = [NSURL URLWithString:[@"tel://" stringByAppendingString:acall]];
        
        if ([UIApplication.sharedApplication canOpenURL:phoneUrl]) {
            [UIApplication.sharedApplication openURL:phoneUrl];
        } else if ([UIApplication.sharedApplication canOpenURL:phoneFallbackUrl]) {
            [UIApplication.sharedApplication openURL:phoneFallbackUrl];
        } else
        {
            [self alertError:@"Error" message:@"Call facility is not available!!!"];
        }
    }
    else
        [self alertError:@"Error" message:@"Call facility is not available!!!"];
}



-(IBAction)onSMS {
    
    NSString* acall = @"";
    if (appDelegate.isDriverMode) {
        acall = [Utils acall:appDelegate.dicDriver];
    }
    else {
        acall = [Utils acall:appDelegate.dicRider];
    }
    
    if ([acall isEqualToString:@"no"]) {
        if (appDelegate.isDriverMode)
            [self alertError:@"Error" message:@"Failed Call to Rider"];
        else
            [self alertError:@"Error" message:@"Failed Call to Driver"];
    }
    else if (![acall isEqualToString:@""] && acall != nil) {
        //        NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"sms://%@",acall]];
        //        if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        //            [[UIApplication sharedApplication] openURL:phoneUrl];
        //        }
        
        NSURL *phoneUrl = [NSURL URLWithString:[@"sms://" stringByAppendingString:acall]];
        NSURL *phoneFallbackUrl = [NSURL URLWithString:[@"sms://" stringByAppendingString:acall]];
        
        if ([UIApplication.sharedApplication canOpenURL:phoneUrl]) {
            [UIApplication.sharedApplication openURL:phoneUrl];
        } else if ([UIApplication.sharedApplication canOpenURL:phoneFallbackUrl]) {
            [UIApplication.sharedApplication openURL:phoneFallbackUrl];
        } else
        {
            [self alertError:@"Error" message:@"Sms facility is not available!!!"];
        }
    }
    else
        [self alertError:@"Error" message:@"Call facility is not available!!!"];
}

-(IBAction)onCancel {
    
    NSString *alertmsg = @" ";
    if (appDelegate.isDriverMode) {
        if (appDelegate.pstate == 220) {
            alertmsg = LocalizedStr(@"Map.Driver.Request.Driver.Cancel.text");
        }
        if (appDelegate.pstate == 230 && appDelegate.pstate2 == 10) {
            alertmsg = LocalizedStr(@"Map.Driver.Pickup.Cancel.title");
        }
        else if (appDelegate.pstate == 230 && appDelegate.pstate2 == 20) {
            alertmsg = @"";
        }
        else if (appDelegate.pstate == 240) {
            alertmsg = LocalizedStr(@"Map.Driver.onTrip.Cancel.text");
        }
    }
    else {
        if (appDelegate.ustate == 120) {
            alertmsg = LocalizedStr(@"Map.Rider.Request.Cancel.text");
        }
        else if (appDelegate.ustate == 130) {
            alertmsg = LocalizedStr(@"Map.Rider.Pickup.Cancel.text");
        }
        else if (appDelegate.ustate == 140) {
            return;
        }
    }
    
    if (![alertmsg isEqualToString:@""]) {
        
        if ([alertmsg isEqualToString:@" "]) {
            return;
        }
        
        LGAlertView *alertView = [[LGAlertView alloc] initWithTitle:nil
                                                            message:alertmsg
                                                              style:LGAlertViewStyleAlert
                                                       buttonTitles:@[LocalizedStr(@"Button.OK")]
                                                  cancelButtonTitle:LocalizedStr(@"Button.CANCEL")
                                             destructiveButtonTitle:nil
                                                      actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                                                          NSLog(@"actionHandler, %@, %lu", title, (long unsigned)index);
                                                          
                                                          appDelegate.isMyCancel = YES;
                                                          if (appDelegate.isDriverMode) {
                                                              if (appDelegate.pstate == 220) {
                                                                  [self onPD130:@""];
                                                              }
                                                              else if (appDelegate.pstate == 230 && appDelegate.pstate2 == 10) {
                                                                  [self onPD232:@""];
                                                              }
                                                              else if (appDelegate.pstate == 240) {
                                                                  [self onPD330:@""];
                                                              }
                                                          }
                                                          else {
                                                              if (appDelegate.ustate == 120) {
                                                                  [self onUD130];
                                                              }
                                                              else if (appDelegate.ustate == 130) {
                                                                  [self onUD230];
                                                              }
                                                          }
                                                      }
                                                      cancelHandler:^(LGAlertView *alertView) {
                                                      }
                                                 destructiveHandler:^(LGAlertView *alertView) {
                                                 }];
        [Utils initAlertButtonColor:alertView];
        [alertView showAnimated:YES completionHandler:nil];
        
        return;
    }
    
    // 취소사유 appDelegate.pstate : 230.20
    dataArray = (NSMutableArray*)@[LocalizedStr(@"Map.Drive.Rider.Cancel.text1"),
                                   LocalizedStr(@"Map.Drive.Rider.Cancel.text2"),
                                   LocalizedStr(@"Map.Drive.Rider.Cancel.text3"),
                                   LocalizedStr(@"Map.Drive.Rider.Cancel.text4"),
                                   LocalizedStr(@"Map.Drive.Rider.Cancel.text5"),
                                   LocalizedStr(@"Map.Drive.Rider.Cancel.text6"),
                                   LocalizedStr(@"Map.Drive.Rider.Cancel.text7")];
    
    // 취소사유 선택화면 show
    UITableView* mTableView = [[UITableView alloc] init];
    mTableView.delegate=self;
    mTableView.dataSource=self;
    
    mTableView.allowsMultipleSelection = NO;
    mTableView.frame = CGRectMake(0.f, 0.f, 320, 260.f);
    mTableView.contentMode = UIViewContentModeScaleAspectFit;
    
    //            NSIndexPath *indexPath = nil;
    
    LGAlertView *alertView = [[LGAlertView alloc] initWithViewAndTitle:LocalizedStr(@"Map.Drive.Rider.Cancel.title")
                                                               message:@""
                                                                 style:LGAlertViewStyleAlert
                                                                  view:mTableView
                                                          buttonTitles:@[LocalizedStr(@"Button.OK")]
                                                     cancelButtonTitle:LocalizedStr(@"Button.CANCEL")
                                                destructiveButtonTitle:nil
                                                         actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                                                             NSLog(@"actionHandler, %@, %lu", title, (long unsigned)index);
                                                             
                                                             BOOL flag = NO;
                                                             NSString *rsncode = @"";
                                                             for(NSIndexPath* indexPath in [mTableView indexPathsForSelectedRows]) {
                                                                 NSLog(@"cell.tag:%@",[dataArray objectAtIndex:indexPath.row]);
                                                                 //rsncode = [dataArray objectAtIndex:indexPath.row];
                                                                 rsncode = [NSString stringWithFormat:@"%d",(int)(indexPath.row+1)];
                                                                 flag = YES;
                                                                 break;
                                                             }
                                                             
                                                             if (!flag) {
                                                                 return;
                                                             }
                                                             appDelegate.isMyCancel = YES;
                                                             if (appDelegate.isDriverMode) {
                                                                 if (appDelegate.pstate == 220 && appDelegate.pstate2 == 20) {
                                                                     [self onPD130:rsncode];
                                                                 }
                                                                 if (appDelegate.pstate == 230) {
                                                                     [self onPD232:rsncode];
                                                                 }
                                                                 else if (appDelegate.pstate == 240 && appDelegate.pstate2 == 10) {
                                                                     [self onPD330:rsncode];
                                                                 }
                                                             }
                                                             else {
                                                                 if (appDelegate.ustate == 120 && appDelegate.ustate2 == 20) {
                                                                     [self onUD130];
                                                                 }
                                                                 if (appDelegate.ustate == 130) {
                                                                     [self onUD230];
                                                                 }
                                                             }
                                                             
                                                             
                                                             [mTableView reloadData];
                                                             [mTableView setContentOffset:CGPointZero animated:YES];
                                                             //mTableView.indexPathsForSelectedRows = nil;
                                                             
                                                         }
                                                         cancelHandler:^(LGAlertView *alertView) {
                                                             NSLog(@"cancelHandler");
                                                         }
                                                    destructiveHandler:^(LGAlertView *alertView) {
                                                        NSLog(@"destructiveHandler");
                                                    }];
    //alertView.heightMax = 256.f;
    [Utils initAlertButtonColor:alertView];
    alertView.windowLevel = LGAlertViewWindowLevelAboveStatusBar;
    [alertView showAnimated:YES completionHandler:nil];
}
#endif
*/

#pragma mark - onEvent
-(void)onEvent:(TXEvent *)event eventParams:(id)subscriptionParams {
    
    TXResponseDescriptor * descriptor = [event getEventProperty:TXEvents.Params.DESCRIPTOR];
    TXRequestObj *request = [event getEventProperty:TXEvents.Params.REQUEST];
    
    [self hideBusyIndicator];
    if([event.name isEqualToString:@"UD305"] || [event.name isEqualToString:@"PD305"]) {
        
        if(descriptor.success == true) {
            NSString     *response = [[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding];
            NSDictionary *result   = getJSONObj(response);
            
            NSArray *mtext              = [result valueForKey:@"drive_messages"];
            //"send_by" = 1; rider
            //"send_by" = 2; driver
            if ([Utils isArray:mtext]) {
                loads = (NSMutableArray*)mtext;
                [self insertMessages];
                [self delayedReload];
                
                [Utils updateChatInfo:mtext];
            }
        }
        else {
//            [self alertError:@"Error" message:descriptor.error];
        }
        
        [self scrollToBottomAnimated:NO];
        initialized	= YES; // 수신완료
    }
    else if([event.name isEqualToString:@"UD325"] || [event.name isEqualToString:@"PD325"]) {
        
        if(descriptor.success == true) {
            NSString     *response = [[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding];
            NSDictionary *result   = getJSONObj(response);
            
            NSDictionary *mtext              = [result valueForKey:@"drive_message"];
            
            if ([Utils isDictionary:mtext]) {
                [self addMessage:mtext];
                [self delayedReload];
                [JSQSystemSoundPlayer jsq_playMessageSentSound];
                [self finishSendingMessage];
            }
        }
        else {
//            [self alertError:@"Error" message:descriptor.error];
        }
    }
    else if([event.name isEqualToString:@"PD130"] ||
            [event.name isEqualToString:@"PD232"]) { // 요청취소
        
        if(descriptor.success == true) {
            [self removeEventListeners];
            
            // 상태 초기화
            [appDelegate resetTripInfo];
            
            [self.navigationController popViewControllerAnimated:YES];
        } else {
            if (descriptor.rlt != -1000 && descriptor.rlt != -999999) {
                //에러주석[self alertError:@"Error" message:descriptor.error];
            }
        }
    }
    else if([event.name isEqualToString:@"PD330"]) { // trip del
        
        if(descriptor.success == true) {
            // 대기상태이 전이
            [self removeEventListeners];
            
            // 상태 초기화
            [appDelegate resetTripInfo];
            
            [self.navigationController popViewControllerAnimated:YES];
        } else {
            if (descriptor.rlt != -1000 && descriptor.rlt != -999999) {
                //에러주석[self alertError:@"Error" message:descriptor.error];
            }
        }
    }
    else if([event.name isEqualToString:@"UD130"] || [event.name isEqualToString:@"UD230"]) { // del pickup
        
        if(descriptor.success == true) {
            // updateActivityState에서 110의 상태로 돌아간다.
            [self removeEventListeners];
            
            // 상태 초기화
            [appDelegate resetTripInfo];
            
            [self.navigationController popViewControllerAnimated:YES];
        } else {
            if (descriptor.rlt != -1000 && descriptor.rlt != -999999) {
                //에러주석[self alertError:@"Error" message:descriptor.error];
            }
        }
    }
}

#pragma mark - Action
-(IBAction)onNaviButtonClick:(id)sender {
    UIButton *btn = (UIButton*)sender;
    NSLog(@"onNaviButtonClick");
    //[super onNaviButtonClick:sender];
    
    if (btn.tag == 1100 || btn.tag == 1101) {
        [self removeEventListeners];
        [self.navigationController popViewControllerAnimated:YES];
    }
}


#pragma mark -

-(void) showBusyIndicator:(NSString *)title {
    if ([SVProgressHUD isVisible])
        return;
    if (!appDelegate.isNetwork) {
        return;
    }
    //[SVProgressHUD showWithStatus:title];
    [SVProgressHUD showWithStatus:nil];
    //[SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeNone];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleCustom];
    [SVProgressHUD setBackgroundColor:[UIColor clearColor]];
    [SVProgressHUD setForegroundColor:UIColorDefault];
    [SVProgressHUD setRingThickness:5];
    
}

-(void)hideBusyIndicator {
    if ([SVProgressHUD isVisible])
        [SVProgressHUD dismiss];
}

-(void)navigationType01X:(UIImage*)leftImage centerText:(NSString*)centerText{
    
    UIButton *btn;
    UILabel *lb;
    
    btn = [self.view viewWithTag:1100];
    if (leftImage != nil) {
        //[btn setBackgroundImage:leftImage forState:UIControlStateNormal];
    }
    btn.hidden = NO;
    
    lb = [self.view viewWithTag:1210];
    [lb setText:centerText];
    lb.hidden = NO;
    
    [self.view viewWithTag:1101].hidden = YES;
    [self.view viewWithTag:1109].hidden = YES;
    
    [self.view viewWithTag:1211].hidden = YES;
    [self.view viewWithTag:1212].hidden = YES;
    [self.view viewWithTag:1200].hidden = YES;
    
    [self.view viewWithTag:1300].hidden = YES;
    [self.view viewWithTag:1301].hidden = YES;
    
    self->navigationView.hidden = NO;

}

//-------------------------------------------------------------------------------------------------------------------------------------------------
-(void) configureNaviBar
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    [self.navigationController.navigationBar setHidden:YES];
    self->statusView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.navigationController.view.frame.size.width, kTopHeight)];
    
#ifdef _DRIVER_MODE
//    if ([recent[@"headerView"] isEqualToString:@"1"]) {
//        self->navigationView = [[UIView alloc] initWithFrame:CGRectMake(0, kTopHeight, self.navigationController.view.frame.size.width, kMainNaviTopHeight+kMainNaviHeight)];
//    }
//    else {
//        self->navigationView = [[UIView alloc] initWithFrame:CGRectMake(0, kTopHeight, self.navigationController.view.frame.size.width, kMainNaviHeight)];
//    }
    
    self->navigationView = [[UIView alloc] initWithFrame:CGRectMake(0, kTopHeight, self.navigationController.view.frame.size.width, kMainNaviHeight)];
#else
    self->navigationView = [[UIView alloc] initWithFrame:CGRectMake(0, kTopHeight, self.navigationController.view.frame.size.width, kMainNaviHeight)];
#endif
    self->navigationView.backgroundColor = [UIColor clearColor];
    //self->navigationView.alpha = 0.5;
    self->navigationView.backgroundColor = [UIColor whiteColor];
    self->navigationView.opaque = NO;
    self->navigationView.hidden = YES;
    /* Create custom view to display section header... */
    
    [self navigationViewLeftImageButton];
    
    [self navigationViewCenterTextButton];
    
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, kTopHeight+kTopHeight-1, self.navigationController.view.frame.size.width, 1)];
    lineView.backgroundColor = HEXCOLOR(0xD2D2D2FF);
    [self->navigationView addSubview:lineView];
    
    [[self view] addSubview:self->navigationView];
    [[self view] addSubview:self->statusView];
    
    [self->statusView setBackgroundColor:[UIColor whiteColor]];
    
    [self navigationType01X:[UIImage imageNamed:@"btn_back"] centerText:description];
    
#ifdef _DRIVER_MODE
//    if ([recent[@"headerView"] isEqualToString:@"1"]) {
//        // profile
//        self->profileView = [[UIView alloc] initWithFrame:CGRectMake(0,
//                                                                     self->navigationView.frame.size.height + self->navigationView.frame.origin.y - kBasicHeight,
//                                                                     self.navigationController.view.frame.size.width,
//                                                                     kBasicHeight)];
//        self->profileView.backgroundColor = [UIColor blackColor];
//        self->profileView.opaque = NO;
//        [self profileButton:0 selector:NSSelectorFromString(@"onCall")];
//        [self profileButton:1 selector:NSSelectorFromString(@"onSMS")];
//        [self profileButton:2 selector:NSSelectorFromString(@"onCancel")];
//        
//        // add button 3
//        [[self view] addSubview:self->profileView];
//    }
#endif
}

-(void)navigationViewLeftImageButton {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    //[button setTitle:@"X" forState:UIControlStateNormal];
    [button sizeToFit];
    button.tag = 1100;
    button.hidden = YES;
    //button.frame = CGRectMake(kLeftMargin, kRadiusImageTopMargin, kRadiusImageSize, kRadiusImageSize);
    button.frame = CGRectMake(kLeftMargin, kLeftMargin, kRadiusImageSize, kRadiusImageSize);
    [button setImageEdgeInsets:UIEdgeInsetsMake(0,-6,0,0)];
    [button setImage:[UIImage imageNamed:@"btn_back"] forState:UIControlStateNormal];
    
    // Add an action in current code file (i.e. target)
    [button addTarget:self action:@selector(onNaviButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    
    [self->navigationView addSubview:button];
}

-(void)navigationViewCenterTextButton {
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(kLeftMargin * 2 + kRadiusImageSize,
                                                               kRadiusImageTopMargin,
                                                               self.navigationController.view.frame.size.width-(
                                                                                            kLeftMargin * 2 + kRadiusImageSize)*2,
                                                               kRadiusImageSize)];
    label.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:14];
    label.textColor = HEXCOLOR(0x333333ff);
    label.textAlignment = NSTextAlignmentCenter;
    label.tag = 1210;
    label.hidden = YES;
    [label setText:@"test..."];
    
    [self->navigationView addSubview:label];
}


-(void)profileButton:(NSInteger)idx selector:(SEL)select {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    
    float __y = 0;
    if (idx == 0) {
        [button setImage:[UIImage imageNamed:@"ic_call"] forState:UIControlStateNormal];
        __y = 0;
    }
    else if (idx == 1) {
        [button setImage:[UIImage imageNamed:@"ic_sms"] forState:UIControlStateNormal];
        __y = self.navigationController.view.frame.size.width/3;
    }
    else if (idx == 2) {
        [button setImage:[UIImage imageNamed:@"ic_cancel_request"] forState:UIControlStateNormal];
        __y = (self.navigationController.view.frame.size.width/3)*2;
    }
    
//    [button sizeToFit];
    button.tag = 1100;
    button.frame = CGRectMake(__y, 0, self.navigationController.view.frame.size.width/3, kBasicHeight);
    
    // Add an action in current code file (i.e. target)
    [button addTarget:self action:select forControlEvents:UIControlEventTouchUpInside];
    
    [self->profileView addSubview:button];
}

-(void) registerEventListeners {
    for (NSArray *msg in _msgList) {
        [[TXUserModel instance] addEventListener:self forEvent:[msg objectAtIndex:0] eventParams:nil];
    }
}

-(void) removeEventListeners {
    for (NSArray *msg in _msgList) {
        [[TXUserModel instance] removeEventListener:self forEvent:[msg objectAtIndex:0]];
    }
    [[NSNotificationCenter defaultCenter] removeObserver:observer1];
    observer1 = nil;
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)viewDidLoad
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	[super viewDidLoad];
	self.title = description;

#ifdef _DRIVER_MODE
//    if ([recent[@"headerView"] isEqualToString:@"1"]) {
//        self.topContentAdditionalInset = kMainNaviTopHeight + kMainNaviHeight;
//    }
//    else {
//        self.topContentAdditionalInset = kMainNaviTopHeight;
//    }
    
    self.topContentAdditionalInset = kMainNaviTopHeight;
#else
    self.topContentAdditionalInset = kMainNaviTopHeight;
#endif
    
    observer1 = [[NSNotificationCenter defaultCenter] addObserverForName:@"NotificationPushChat"
                                                                  object:nil
                                                                   queue:[NSOperationQueue mainQueue]
                                                              usingBlock:^(NSNotification *notification) {
                                                                  
                                                                  NSDictionary* dict = notification.userInfo;
                                                                  NSString *chat_oseq = dict[@"drive_message"][@"oseq"];
                                                                  
                                                                  if ([chat_oseq isEqualToString:[self getOseq]]) {
                                                                      NSString* _msg = [dict valueForKey:@"mid"];
                                                                      // 채팅메시지
                                                                      if ([_msg isEqualToString:@"UD355"] || [_msg isEqualToString:@"PD355"]) {
                                                                          NSString *chat_name = @"";
#ifdef _DEBUG
                                                                          NSString *chat_body = [NSString stringWithFormat:@"%@",LocalizedStr(@"Push.Alert.chat.text")];
#else
                                                                          NSString *chat_body = LocalizedStr(@"Push.Alert.chat.text");
#endif
                                                                          if (appDelegate.isDriverMode) {
                                                                              chat_name = [NSString stringWithFormat:@"%@",dict[@"drive_message"][@"uname"]];
                                                                          }
                                                                          else {
                                                                              chat_name = [NSString stringWithFormat:@"%@",dict[@"drive_message"][@"pname"]];
                                                                          }
                                                                          
                                                                          // noti
                                                                          if (appDelegate.isDriverMode) {
                                                                              [self onPD355:dict[@"drive_message"]];
                                                                          }
                                                                          else {
                                                                              [self onUD355:dict[@"drive_message"]];
                                                                          }
                                                                          chat_name = @"";
                                                                          // alert
                                                                          if([UIApplication sharedApplication].applicationState != UIApplicationStateActive) {
                                                                              [Utils sendNotofication:chat_body title:chat_name];
                                                                          }
                                                                          
                                                                          [Utils updateAlrim:CODE_TYPE_CHAT code:CODE_ALRIM_CHAT(chat_oseq) value:CODE_FLAG_OFF];
                                                                      }
                                                                  }
                                                              }];
    
    //------------------------------------------------------------------------------------------------------------------------
    // 2. init event
    _msgList = @[
                 @[@"UD305",@""], // rider
                 @[@"UD325",@""], // rider
                 
                 @[@"PD305",@""], // driver
                 @[@"PD325",@""], // driver
                 
//                 @[@"UD130",@""], // rider cancel
//                 @[@"UD230",@""], // rider cancel pickup
//                 @[@"PD130",@""], // driver cancel order
//                 @[@"PD232",@""], // driver cancel pickup
//                 @[@"PD330",@""], // driver cancel trip
                 
                 ];
    
    [self registerEventListeners];
    
    [self configureNaviBar];
    
    appDelegate = (TXAppDelegate *)[[UIApplication sharedApplication] delegate];
    //---------------------------------------------------------------------------------------------------------------------------------------------
    
    self.inputToolbar.contentView.leftBarButtonItem = nil;
	//---------------------------------------------------------------------------------------------------------------------------------------------
	loads = [[NSMutableArray alloc] init]; // 최초 메시지 로딩시 임시 저장소
	items = [[NSMutableArray alloc] init]; // 채팅 item 저장소
	messages = [[NSMutableArray alloc] init]; // 채팅 cell 저장소
	started = [[NSMutableDictionary alloc] init]; // 채팅 아바타 download 진행중 여부 저장
	avatars = [[NSMutableDictionary alloc] init]; // 채팅 아바타 정보
	//---------------------------------------------------------------------------------------------------------------------------------------------
    self.senderId = recent[@"srcId"];
    if (appDelegate.isDriverMode) {
        self.senderId = @"2";
    }
    else {
        self.senderId = @"1";
    }
    self.senderDisplayName = recent[@"srcName"];
    //---------------------------------------------------------------------------------------------------------------------------------------------

    self.incomingCellIdentifier = @"CustomMessagesCollectionViewCellIncoming";
    [self.collectionView registerNib:[CustomMessagesCollectionViewCellIncoming nib] forCellWithReuseIdentifier:self.incomingCellIdentifier];
    self.outgoingCellIdentifier = @"CustomMessagesCollectionViewCellOutgoing";
    [self.collectionView registerNib:[CustomMessagesCollectionViewCellOutgoing nib] forCellWithReuseIdentifier:self.outgoingCellIdentifier];

	//---------------------------------------------------------------------------------------------------------------------------------------------
//	JSQMessagesBubbleImageFactory *bubbleFactory = [[JSQMessagesBubbleImageFactory alloc] init];
//    bubbleImageOutgoing = [bubbleFactory outgoingMessagesBubbleImageWithColor:COLOR_OUTGOING];
//    bubbleImageIncoming = [bubbleFactory incomingMessagesBubbleImageWithColor:COLOR_INCOMING];
    self.collectionView.collectionViewLayout.outgoingAvatarViewSize = CGSizeZero;
    
//    JSQMessagesBubbleImageFactory *bubbleFactoryOutgoing = [[JSQMessagesBubbleImageFactory alloc] initWithBubbleImage:[UIImage jsq_bubbleCompactTaillessImage] capInsets:UIEdgeInsetsZero];
//    JSQMessagesBubbleImageFactory *bubbleFactoryIncoming = [[JSQMessagesBubbleImageFactory alloc] initWithBubbleImage:[UIImage jsq_bubbleCompactTaillessImage] capInsets:UIEdgeInsetsZero];
    
    JSQMessagesBubbleImageFactory *bubbleFactory = [[JSQMessagesBubbleImageFactory alloc] init];
    //set your bubble color
    bubbleImageOutgoing = [bubbleFactory outgoingMessagesBubbleImageWithColor:COLOR_OUTGOING];
    bubbleImageIncoming = [bubbleFactory incomingMessagesBubbleImageWithColor:COLOR_INCOMING];
    
	//---------------------------------------------------------------------------------------------------------------------------------------------
	avatarImageBlank = [JSQMessagesAvatarImageFactory avatarImageWithImage:[UIImage imageNamed:@"chat_blank"] diameter:30.0];
	//---------------------------------------------------------------------------------------------------------------------------------------------
	[JSQMessagesCollectionViewCell registerMenuAction:@selector(actionCopy:)];
	[JSQMessagesCollectionViewCell registerMenuAction:@selector(actionDelete:)];
	[JSQMessagesCollectionViewCell registerMenuAction:@selector(actionSave:)];
	//---------------------------------------------------------------------------------------------------------------------------------------------
	UIMenuItem *menuItemCopy = [[UIMenuItem alloc] initWithTitle:@"Copy" action:@selector(actionCopy:)];
	UIMenuItem *menuItemDelete = [[UIMenuItem alloc] initWithTitle:@"Delete" action:@selector(actionDelete:)];
	UIMenuItem *menuItemSave = [[UIMenuItem alloc] initWithTitle:@"Save" action:@selector(actionSave:)];
	[UIMenuController sharedMenuController].menuItems = @[menuItemCopy, menuItemDelete, menuItemSave];
	//---------------------------------------------------------------------------------------------------------------------------------------------
	//---------------------------------------------------------------------------------------------------------------------------------------------
    
	//[self loadMessages];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)viewWillAppear:(BOOL)animated
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	[super viewWillAppear:animated];
    
    [self loadMessages];
//    appDelegate.currentGroupID = groupId;
    
    appDelegate.mapVCBeforeVC = @"ChatView";
    appDelegate.chatOseq = recent[@"oseq"];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)viewDidAppear:(BOOL)animated
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    [super viewDidAppear:animated];
    self.collectionView.collectionViewLayout.springinessEnabled = NO;
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)viewWillDisappear:(BOOL)animated
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	[super viewWillDisappear:animated];
	if (self.isMovingFromParentViewController)
	{
		//ClearRecentCounter(groupId);
	}
    
    [self removeEventListeners];
    
    appDelegate.mapVCBeforeVC = @"";
    appDelegate.chatOseq = @"";
    
//    appDelegate.currentGroupID = @"";
}

//-(void)viewDidLayoutSubviews
//{
//    CGRect frame = self.collectionView.frame;
//    frame.origin.y = kNaviTopHeight;
//    frame.size.height -= kNaviTopHeight;
//    self.collectionView.frame = frame;
//}

- (void)dealloc {
    
    [self removeEventListeners];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}

#pragma mark - Backend methods

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)loadMessages
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	initialized = NO;
	self.automaticallyScrollsToMostRecentMessage = NO;
    
    if (appDelegate.isDriverMode) {
        [self onPD305:@""];
    }
    else {
        [self onUD305:@""];
    }
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)insertMessages
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    // 화면에 출력할 개수(INSERT_MESSAGES)만큼만 메시지를 입력한다.
	NSInteger max = [loads count];
	NSInteger min = 0;
	//---------------------------------------------------------------------------------------------------------------------------------------------
	for (NSInteger i=max-1; i>=min; i--)
	{
		NSDictionary *item = loads[i];
		[self insertMessage:item];
		loaded++;
	}
	//---------------------------------------------------------------------------------------------------------------------------------------------
	self.automaticallyScrollsToMostRecentMessage = NO;
	[self finishReceivingMessage];
	self.automaticallyScrollsToMostRecentMessage = YES;
	//---------------------------------------------------------------------------------------------------------------------------------------------
	self.showLoadEarlierMessagesHeader = (loaded != [loads count]);
    
    NSLog(@"[load cnt:%lu, max:%ld, min:%ld loaded:%ld, %ld,%ld",(unsigned long)[loads count], (long)max, (long)min, (long)loaded, (unsigned long)[items count], (unsigned long)[messages count]);
    //self.showLoadEarlierMessagesHeader = NO;
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (BOOL)insertMessage:(NSDictionary *)item__
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    NSString *userId = [NSString stringWithFormat:@"%@", item__[@"send_by"]];
    NSDictionary * item = [[NSDictionary alloc] initWithObjectsAndKeys:
                           recent[@"dstName"], @"name",
                           userId, @"userId",
                           item__[@"reg_date"], @"date",
                           item__[@"msg"], @"text",
                           userId, @"send_by",
                           @"1", @"status",
                           nil];

    Incoming *incoming = [[Incoming alloc] initWith:groupId CollectionView:self.collectionView];
	JSQMessage *message = [incoming create:item];
	//---------------------------------------------------------------------------------------------------------------------------------------------
	[items insertObject:item atIndex:0];
	[messages insertObject:message atIndex:0];
	//---------------------------------------------------------------------------------------------------------------------------------------------
	return [self incoming:item];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (BOOL)addMessage:(NSDictionary *)item__
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    NSString *userId = [NSString stringWithFormat:@"%@", item__[@"send_by"]];
    NSDictionary * item = [[NSDictionary alloc] initWithObjectsAndKeys:
                           recent[@"dstName"], @"name",
                           userId, @"userId",
                           item__[@"reg_date"], @"date",
                           item__[@"msg"], @"text",
                           userId, @"send_by",
                           @"1", @"status",
                           nil];
    
    NSArray *mtext = [[NSArray alloc] initWithObjects:item__, nil];
    [Utils updateChatInfo:mtext];
    
	Incoming *incoming = [[Incoming alloc] initWith:groupId CollectionView:self.collectionView];
	JSQMessage *message = [incoming create:item];
	//---------------------------------------------------------------------------------------------------------------------------------------------
	[items addObject:item];
	[messages addObject:message];
	//---------------------------------------------------------------------------------------------------------------------------------------------
	return [self incoming:item];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)delayedReload
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    [self.collectionView reloadData];
}

#pragma mark - Picture methods

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)loadAvatar:(NSString *)senderId
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	if (started[senderId] == nil) started[senderId] = @YES; else return;
    NSLog(@"loadAvatar");
	//---------------------------------------------------------------------------------------------------------------------------------------------
    // 본인이면 바로 다운로드
    // __DEBUG
	//---------------------------------------------------------------------------------------------------------------------------------------------
    // 타인이면 사용자 정보 찾아서 다운로드
    UIImage *image1 = [recent[@"srcImage"] resizedImageByMagick: @"320x320#"];
    UIImage *image2 = [recent[@"dstImage"] resizedImageByMagick: @"320x320#"];
        
    //avatars[user.objectId] = [JSQMessagesAvatarImageFactory avatarImageWithImage:image diameter:30.0];
    avatars[@"1"] = [JSQMessagesAvatarImageFactory avatarImageWithImage:image1 diameter:30.0];
    avatars[@"2"] = [JSQMessagesAvatarImageFactory avatarImageWithImage:image2 diameter:30.0];
    
    [self performSelector:@selector(delayedReload) withObject:nil afterDelay:0.1];
}

#pragma mark - Message sendig methods

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)messageSend:(NSString *)text
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    if (appDelegate.isDriverMode) {
        [self onPD325:text];
    }
    else {
        [self onUD325:text];
    }
}

#pragma mark - UITextViewDelegate

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	return YES;
}

#pragma mark - JSQMessagesViewController method overrides

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)didPressSendButton:(UIButton *)button withMessageText:(NSString *)text senderId:(NSString *)senderId senderDisplayName:(NSString *)name date:(NSDate *)date
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	[self messageSend:text];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)didPressAccessoryButton:(UIButton *)sender
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	
}

#pragma mark - JSQMessages CollectionView DataSource

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (id<JSQMessageData>)collectionView:(JSQMessagesCollectionView *)collectionView messageDataForItemAtIndexPath:(NSIndexPath *)indexPath
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	return messages[indexPath.item];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (id<JSQMessageBubbleImageDataSource>)collectionView:(JSQMessagesCollectionView *)collectionView
			 messageBubbleImageDataForItemAtIndexPath:(NSIndexPath *)indexPath
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	if ([self outgoing:items[indexPath.item]])
	{
		return bubbleImageOutgoing;
	}
	else return bubbleImageIncoming;
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (id<JSQMessageAvatarImageDataSource>)collectionView:(JSQMessagesCollectionView *)collectionView
					avatarImageDataForItemAtIndexPath:(NSIndexPath *)indexPath
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	JSQMessage *message = messages[indexPath.item];
	if (avatars[message.senderId] == nil)
	{
		[self loadAvatar:message.senderId];
		return avatarImageBlank;
	}
	else return avatars[message.senderId];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForCellTopLabelAtIndexPath:(NSIndexPath *)indexPath
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    JSQMessage *message = messages[indexPath.item];
    
    if (indexPath.item == 0) {
        return [[JSQMessagesTimestampFormatter sharedFormatter] attributedTimestampForDate:message.date];
    }
    
    if (indexPath.item - 1 > -1) {
        JSQMessage *previousMessage = [messages objectAtIndex:indexPath.item - 1];
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyyMMdd"];
        NSString *dateStr1 = [formatter stringFromDate:message.date];
        NSString *dateStr2 = [formatter stringFromDate:previousMessage.date];
        
        if (![dateStr1 isEqualToString:dateStr2]) {
            return [[JSQMessagesTimestampFormatter sharedFormatter] attributedTimestampForDate:message.date];
        }
    }
    
//    if (indexPath.item - 1 > 0) {
//        JSQMessage *previousMessage = [messages objectAtIndex:indexPath.item - 1];
//        
//        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//        [formatter setDateFormat:@"yyyyMMdd"];
//        NSString *dateStr1 = [formatter stringFromDate:message.date];
//        NSString *dateStr2 = [formatter stringFromDate:previousMessage.date];
//
//        if (![dateStr1 isEqualToString:dateStr2]) {
//            return [[JSQMessagesTimestampFormatter sharedFormatter] attributedTimestampForDate:message.date];
//        }

//        if ([message.date timeIntervalSinceDate:previousMessage.date] / 60*60*24 > 1) {
//            return [[JSQMessagesTimestampFormatter sharedFormatter] attributedTimestampForDate:message.date];
//        }
//    }
    
    return nil;
    
//	if (indexPath.item % 3 == 0)
//	{
//		JSQMessage *message = messages[indexPath.item];
//		return [[JSQMessagesTimestampFormatter sharedFormatter] attributedTimestampForDate:message.date];
//	}
//	else return nil;
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForMessageBubbleTopLabelAtIndexPath:(NSIndexPath *)indexPath
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	if ([self incoming:items[indexPath.item]])
	{
		JSQMessage *message = messages[indexPath.item];
		if (indexPath.item > 0)
		{
			JSQMessage *previous = messages[indexPath.item-1];
			if ([previous.senderId isEqualToString:message.senderId])
			{
				return nil;
			}
		}
		return [[NSAttributedString alloc] initWithString:message.senderDisplayName];
	}
	else return nil;
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForCellBottomLabelAtIndexPath:(NSIndexPath *)indexPath
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
//	NSDictionary *item = items[indexPath.item];
//	if ([self outgoing:item])
//	{
//		return [[NSAttributedString alloc] initWithString:item[@"status"]];
//	}
//	else return nil;
    return nil;
}

#pragma mark - UICollectionView DataSource

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	return [messages count];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (UICollectionViewCell *)collectionView:(JSQMessagesCollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	UIColor *color = [self outgoing:items[indexPath.item]] ? [UIColor whiteColor] : [UIColor blackColor];

    NSDictionary *item = items[indexPath.item];
    
    id<JSQMessageData> messageItem = [collectionView.dataSource collectionView:collectionView messageDataForItemAtIndexPath:indexPath];
    
    BOOL isOutgoingMessage = [self isOutgoingMessage:messageItem];
    
    NSDate *date = StringFormat2Date(item[@"date"]);
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HH:mm"];
    [formatter setLocale:[NSLocale currentLocale]];
    NSString *dateStr = [formatter stringFromDate:date];
    
    //NSLog(@"isMediaMessage:%@/%@",item[@"type"],item[@"picture"]);
    if (isOutgoingMessage) {
        CustomMessagesCollectionViewCellOutgoing *cell = (CustomMessagesCollectionViewCellOutgoing *)[super collectionView:collectionView cellForItemAtIndexPath:indexPath];
        cell.timeLabel.text = dateStr;
//        cell.timeLabel.textColor = [UIColor blackColor];
        cell.textView.textColor = color;
        cell.textView.linkTextAttributes = @{NSForegroundColorAttributeName:color};

        return cell;
    }
    else {
        CustomMessagesCollectionViewCellIncoming *cell = (CustomMessagesCollectionViewCellIncoming *)[super collectionView:collectionView cellForItemAtIndexPath:indexPath];
        cell.timeLabel.text = dateStr;
//        cell.timeLabel.textColor = [UIColor blackColor];
        cell.textView.textColor = color;
        cell.textView.linkTextAttributes = @{NSForegroundColorAttributeName:color};

        return cell;
    }
}

#pragma mark - UICollectionView Delegate

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath
			withSender:(id)sender
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
//	NSDictionary *item = items[indexPath.item];
//	//---------------------------------------------------------------------------------------------------------------------------------------------
//	if (action == @selector(actionCopy:))
//	{
//		if ([item[@"type"] isEqualToString:@"text"]) return YES;
//	}
//	if (action == @selector(actionDelete:))
//	{
//		if ([self outgoing:item]) return YES;
//	}
//	if (action == @selector(actionSave:))
//	{
//		if ([item[@"type"] isEqualToString:@"picture"]) return YES;
//		if ([item[@"type"] isEqualToString:@"audio"]) return YES;
//		if ([item[@"type"] isEqualToString:@"video"]) return YES;
//	}
	return NO;
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath
			withSender:(id)sender
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	if (action == @selector(actionCopy:))		[self actionCopy:indexPath];
	if (action == @selector(actionDelete:))		[self actionDelete:indexPath];
	if (action == @selector(actionSave:))		[self actionSave:indexPath];
}

#pragma mark - JSQMessages collection view flow layout delegate

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
				   layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForCellTopLabelAtIndexPath:(NSIndexPath *)indexPath
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    if (indexPath.item == 0) {
        return kJSQMessagesCollectionViewCellLabelHeightDefault;
    }
    
    if (indexPath.item - 1 > 0) {
        JSQMessage *previousMessage = [messages objectAtIndex:indexPath.item - 1];
        JSQMessage *message = [messages objectAtIndex:indexPath.item];
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyyMMdd"];
        NSString *dateStr1 = [formatter stringFromDate:message.date];
        NSString *dateStr2 = [formatter stringFromDate:previousMessage.date];
        
        if (![dateStr1 isEqualToString:dateStr2]) {
            return kJSQMessagesCollectionViewCellLabelHeightDefault;
        }
        
//        if ([message.date timeIntervalSinceDate:previousMessage.date] / 60*60*24 > 1) {
//            return kJSQMessagesCollectionViewCellLabelHeightDefault;
//        }
    }
    
    return 0.0f;
//	if (indexPath.item % 3 == 0)
//	{
//		return kJSQMessagesCollectionViewCellLabelHeightDefault;
//	}
//	else return 0;
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
				   layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForMessageBubbleTopLabelAtIndexPath:(NSIndexPath *)indexPath
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
//	if ([self incoming:items[indexPath.item]])
//	{
//		if (indexPath.item > 0)
//		{
//			JSQMessage *message = messages[indexPath.item];
//			JSQMessage *previous = messages[indexPath.item-1];
//			if ([previous.senderId isEqualToString:message.senderId])
//			{
//				return 0;
//			}
//		}
//		return kJSQMessagesCollectionViewCellLabelHeightDefault;
//	}
//	else return 0;
    
    return 0;
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
				   layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForCellBottomLabelAtIndexPath:(NSIndexPath *)indexPath
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
//	if ([self outgoing:items[indexPath.item]])
//	{
//		return kJSQMessagesCollectionViewCellLabelHeightDefault;
//	}
//	else return 0;
    return kJSQMessagesCollectionViewCellLabelHeightDefault;
}

#pragma mark - Responding to collection view tap events

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)collectionView:(JSQMessagesCollectionView *)collectionView didTapAvatarImageView:(UIImageView *)avatarImageView
		   atIndexPath:(NSIndexPath *)indexPath
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	NSDictionary *item = items[indexPath.item];
	if ([self incoming:item])
	{
//		ProfileView *profileView = [[ProfileView alloc] initWith:item[@"userId"] User:nil];
//		[self.navigationController pushViewController:profileView animated:YES];
	}
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)collectionView:(JSQMessagesCollectionView *)collectionView didTapMessageBubbleAtIndexPath:(NSIndexPath *)indexPath
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
//	NSDictionary *item = items[indexPath.item];
//	JSQMessage *message = messages[indexPath.item];
	//---------------------------------------------------------------------------------------------------------------------------------------------
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)collectionView:(JSQMessagesCollectionView *)collectionView didTapCellAtIndexPath:(NSIndexPath *)indexPath touchLocation:(CGPoint)touchLocation
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    [self.view endEditing:YES];
}

#pragma mark - User actions


//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)actionDelete:(NSIndexPath *)indexPath
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)actionCopy:(NSIndexPath *)indexPath
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
//    NSDictionary *item = items[indexPath.item];
//    [[UIPasteboard generalPasteboard] setString:item[@"text"]];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)actionSave:(NSIndexPath *)indexPath
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
	
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)video:(NSString *)videoPath didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
}

//---------------------------------------------------------------------------------------------------------------------------------------------
- (void)onClickToolbarLeft:(id)sender
//---------------------------------------------------------------------------------------------------------------------------------------------
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(NSString*)getOseq
{
    NSString *oseq = recent[@"oseq"];
    return oseq;
}

#pragma mark - Helper methods

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (BOOL)incoming:(NSDictionary *)item
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    return ([self.senderId isEqualToString:item[@"send_by"]] == NO);
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (BOOL)outgoing:(NSDictionary *)item
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    return ([self.senderId isEqualToString:item[@"send_by"]] == YES);
}

#pragma mark - UITableView DataSource Methods
#pragma mark - UITableView DataSource Methods
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [tableView setSeparatorInset:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [tableView setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsMake(0, 0, 0, 0)];
    }
    
    
    UIView *seperatorView = [[UIView alloc] initWithFrame:CGRectMake(10, cell.frame.size.height - 1, cell.frame.size.width - 10*2, 1)];
    seperatorView.backgroundColor = UIColorTableSeperator;
    [cell addSubview:seperatorView];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    // This will create a "invisible" footer
    return 0.01f;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [UIView new];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [dataArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    cell.tag = indexPath.row;
    int tag = (int)indexPath.row+100;
    //[self changeCell:tableView withIndexPath:indexPath andTag:tag];
    [self addCell:tableView withCell:cell withIndexPath:indexPath andTag:tag];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    cell.textLabel.text = [NSString stringWithFormat:@"      %@",[dataArray objectAtIndex:indexPath.row]];
    
    return cell;
}

#pragma mark - UITableView Delegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self changeCell:tableView withIndexPath:indexPath andTag:((int)indexPath.row+100)];
    
    //cell.accessoryView.hidden = NO;
}
- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self changeCell:tableView withIndexPath:indexPath andTag:((int)indexPath.row+100)];
    
    //cell.accessoryView.hidden = YES;
}

-(void)addCell:(UITableView*)tableView withCell:(UITableViewCell*)cell withIndexPath:(NSIndexPath*)indexPath andTag:(int)tag{
    
    UIImageView *preView = [cell.contentView viewWithTag:tag];
    [preView removeFromSuperview];
    
    UILabel *preLabel = [cell.contentView viewWithTag:(tag+100)];
    [preLabel removeFromSuperview];
    
    NSInteger _h = cell.frame.size.height;
    NSInteger _x = 16;
    UIImageView* imv = [[UIImageView alloc]initWithFrame:CGRectMake(cell.frame.size.width - _x*4 ,_h/2-_x/2, _x, _x)];
    imv.tag = tag;
    UIImage *cellImage;
    
    if([[tableView indexPathsForSelectedRows] containsObject:indexPath]) {
        cellImage = [UIImage imageNamed:@"form_radio_chk"];
    }
    else {
        cellImage = [UIImage imageNamed:@"form_radio_normal"];
    }
    imv.image=cellImage;
    [cell.contentView addSubview:imv];
}

-(void)changeCell:(UITableView*)tableView withIndexPath:(NSIndexPath*)indexPath andTag:(int)tag{
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    UIImageView *preView = [cell.contentView viewWithTag:tag];
    [preView removeFromSuperview];
    
    UILabel *preLabel = [cell.contentView viewWithTag:(tag+100)];
    [preLabel removeFromSuperview];
    
    NSInteger _h = cell.frame.size.height;
    NSInteger _x = 16;
    UIImageView* imv = [[UIImageView alloc]initWithFrame:CGRectMake(cell.frame.size.width - _x*4 ,_h/2-_x/2, _x, _x)];
    //    UIImageView* imv = [[UIImageView alloc]initWithFrame:CGRectMake(0,_h/2-_x/2, _x, _x)];
    imv.tag = tag;
    UIImage *cellImage;
    
    if([[tableView indexPathsForSelectedRows] containsObject:indexPath]) {
        cellImage = [UIImage imageNamed:@"form_radio_chk"];
    }
    else {
        cellImage = [UIImage imageNamed:@"form_radio_normal"];
    }
    imv.image=cellImage;
    [cell.contentView addSubview:imv];
}

@end

