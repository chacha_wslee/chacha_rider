

#import "utilities.h"
#import "JSQMessages.h"

//-------------------------------------------------------------------------------------------------------------------------------------------------
@interface ChatView : JSQMessagesViewController
//-------------------------------------------------------------------------------------------------------------------------------------------------

- (id)initWith:(NSString *)groupId_;
- (id)initWith:(NSString *)groupId_ description:(NSString*)desc_;
- (id)initWith:(NSString *)groupId_ description:(NSString*)desc_ recent:(NSDictionary *)recent_;

-(void)onPD355:(NSDictionary*)item;
-(void)onUD355:(NSDictionary*)item;

-(NSString*)getOseq;

@end

