//
//  SimpleTableCell.h
//  SimpleTable
//
//  Created by Simon Ng on 28/4/12.
//  Copyright (c) 2012 Appcoda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "utilities.h"
#import "JSQMessages.h"

@interface CustomMessagesCollectionViewCellOutgoing : JSQMessagesCollectionViewCellOutgoing

@property (nonatomic, strong) IBOutlet UILabel *timeLabel;
//@property (weak, nonatomic) JSQMessagesLabel *timeLabel;

@end
