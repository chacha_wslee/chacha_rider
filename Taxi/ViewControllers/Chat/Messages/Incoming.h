

#import "utilities.h"
#import "JSQMessages.h"

//-------------------------------------------------------------------------------------------------------------------------------------------------
@interface Incoming : NSObject
//-------------------------------------------------------------------------------------------------------------------------------------------------

- (id)initWith:(NSString *)groupId_ CollectionView:(JSQMessagesCollectionView *)collectionView_;

- (JSQMessage *)create:(NSDictionary *)item;

@end

