

#import "utilities.h"

//-------------------------------------------------------------------------------------------------------------------------------------------------
@interface Outgoing : NSObject
//-------------------------------------------------------------------------------------------------------------------------------------------------

- (id)initWith:(NSString *)groupId_ View:(UIView *)view_;
- (id)initWith:(NSString *)groupId_ View:(UIView *)view_ recent:(NSDictionary*)recent_;

@end

