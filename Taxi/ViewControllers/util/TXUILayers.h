//
//  TXUILayers.h
//  Taxi
//

//

#import <Foundation/Foundation.h>

@interface TXUILayers : NSObject

+(CAShapeLayer *) layerWithRadiusTop:(CGRect) bounds color:(CGColorRef) color;
+(CAShapeLayer *) layerWithRadiusBottom:(CGRect) bounds color:(CGColorRef) color;
+(CAShapeLayer *) layerWithRadiusNone:(CGRect) bounds color:(CGColorRef) color;

@end
