//
//  TXRootVC.h
//  Taxi
//

//

#import <UIKit/UIKit.h>
#import "TXCode2MsgTranslator.h"
#import "TXModelBase.h"
#import "TXUILayers.h"
#import "TXTextField.h"
#import "TXButton.h"
#import "UIUtil.h"
#import "TXUserModel.h"
#import "TXAppDelegate.h"

#import "Validator.h"
#import "WToast.h"

#import "INTULocationManager.h"
#import "GoogleUtils.h"
#import "MSWeakTimer.h"

@interface TXBaseViewController : UIViewController<TXEventListener> {
    NSDictionary *parameters;
    float x;
    float y;
    float width;
    float height;
    
    TXUserModel *model;
    TXAppDelegate *appDelegate;
}

@property (nonatomic, strong) IBOutlet UIActivityIndicatorView *activityIndicator;

-(void) pushViewController : (TXBaseViewController *) viewController;
-(void) presentViewController : (TXBaseViewController *) viewController;
-(void) alertError : (NSString *) title message : (NSString *) message;
-(TXBaseViewController *) vcFromName: (NSString *) name;
-(void) refreshInterfaceBasedOnSignIn;
-(void) setParameters:(NSDictionary *)props;
-(void) showBusyIndicator;
-(void) showBusyIndicator:(NSString *)title;
-(void) hideBusyIndicator;
-(void) configureMain;
-(void) configure;
-(void) configureStyles;
-(void) statusBarStyle;
-(void)statusBarStyleLightContent;
-(void)statusBarStyleDefault;
-(void)statusBarStyleLauncher;
-(void)statusBarStyleWhite;
-(void)statusBarStyleTestMode;
-(void)bringTOTopstatusBar:(UIView*)view;
-(NSInteger)navigationTopHeight;
-(void)navigationClear;
-(void) configureNaviBar;
-(void) configureBottomList;
-(void) configureBottomLine;
-(void) configureAccount;
-(UIView*)backroundView;
-(UIView*)navigationView;
-(UIView*)statusView;
-(void)refreshView;

-(void)navigationBottomText:(NSString*)centerText;
-(void)navigationType111:(UIImage*)leftImage centerText:(NSString*)centerText rightImage:(UIImage*)rightImage;
-(void)navigationType110:(UIImage*)leftImage centerText:(NSString*)centerText rightImage:(UIImage*)rightImage;
-(void)navigationType010:(UIImage*)leftImage centerText:(NSString*)centerText rightImage:(UIImage*)rightImage;
-(void)navigationType10X:(UIImage*)leftImage centerImage:(UIImage*)centerImage;
-(void)navigationType101:(UIImage*)leftImage centerImage:(UIImage*)centerImage rightImage:(UIImage*)rightImage;
-(void)navigationType01X:(UIImage*)leftImage centerText:(NSString*)centerText;
-(void)navigationType00X:(UIImage*)leftImage centerImage:(UIImage*)centerImage;
-(void)navigationType11X:(UIImage*)leftImage centerText:(NSString*)centerText rightText:(NSString*)rightText;


@end
