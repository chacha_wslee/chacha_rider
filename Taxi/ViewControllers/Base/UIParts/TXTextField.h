//
//  TXTextField.h
//  Taxi
//

//

#import "UIUtil.h"

@interface TXTextField : UITextField

@property (nonatomic, assign) float height;
@property (nonatomic, assign) float width;

- (id)initUnder:(UIView *) view dim:(float) pix;
- (void)frameUnder:(UIView *) view dim:(float) pix;

@end
