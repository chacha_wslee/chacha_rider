//
//  TXButton.m
//  Taxi

//

#import "TXButton.h"

@implementation TXButton

-(id)initWithCoder:(NSCoder *)aDecoder {
    
    if(self = [super initWithCoder:aDecoder]) {
        
    }
    
    return self;
}

-(id)initWithFrame:(CGRect)frame {
    
    if(self = [super initWithFrame:frame]) {
        self.layer.masksToBounds = true;
        self.layer.cornerRadius = 3.0;
        self.alpha = 0.8f;
        self.backgroundColor = [UIColor clearColor];
        self.titleLabel.font = [UIFont boldSystemFontOfSize:15];
        [self setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    }
    
    return self;
}


-(id) initUnderOrOnTop:(UIView *) view dim:(float) pix under:(BOOL)under {
    
    self = [self initWithFrame:view.frame];
    
    if(self) {
        
        CGRect viewFrame = view.frame;
        float y = under ? ((viewFrame.origin.y + viewFrame.size.height + pix)) : (viewFrame.origin.y - viewFrame.size.height - pix);
        self.frame = CGRectMake(viewFrame.origin.x,
                                y,
                                viewFrame.size.width,
                                viewFrame.size.height);
        
    }
    
    return self;
}

- (id)initUnder:(UIView *) view dim:(float) pix;
{
    return [self initUnderOrOnTop:view dim:pix under:true];
}

- (id)initOnTopOf:(UIView *) view dim:(float) pix;
{
    return [self initUnderOrOnTop:view dim:pix under:false];
}

- (void) setBackgroundColor:(UIColor *) _backgroundColor forState:(UIControlState) _state {
    if (backgroundStates == nil)
        backgroundStates = [[NSMutableDictionary alloc] init];
    
    [backgroundStates setObject:_backgroundColor forKey:[NSNumber numberWithInt:_state]];
    
    if (self.backgroundColor == nil)
        [self setBackgroundColor:_backgroundColor];
}

- (UIColor*) backgroundColorForState:(UIControlState) _state {
    return [backgroundStates objectForKey:[NSNumber numberWithInt:_state]];
}

- (void) setBasicColor:(UIColor *)_textColor background:(UIColor *)_backgroundColor highlighted:(UIColor *)_highlightColor
{
    [self setTitleColor:_textColor forState:UIControlStateNormal];
    [self setBackgroundColor:_backgroundColor];
    [self setBackgroundColor:_backgroundColor forState:UIControlStateNormal];
    [self setBackgroundColor:_highlightColor forState:UIControlStateHighlighted];
}

- (void) setBasicButton:(NSString*)_text
{
    [[self titleLabel] setFont:[UIFont fontWithName:@"HelveticaNeue" size:15.0f]];
    [self setTitle:_text forState:UIControlStateNormal];
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [self setBackgroundColor:UIColorBasicText];
//    [self setBackgroundColor:HEXCOLOR(0xFFC700FF) forState:UIControlStateNormal];
//    [self setBackgroundColor:HEXCOLOR(0xFFC700FF) forState:UIControlStateHighlighted];
}

- (void) setBasicText:(NSString*)_text
{
    [self setBasicText:_text fontsize:15.0f];
}

- (void) setBasicText:(NSString*)_text fontsize:(float)_size
{
    [[self titleLabel] setFont:[UIFont fontWithName:@"HelveticaNeue" size:_size]];
    [self setTitle:_text forState:UIControlStateNormal];
    [self setTitleColor:UIColorBasicText forState:UIControlStateNormal];
    
    [self setBackgroundColor:[UIColor clearColor]];
//    [self setBackgroundColor:[UIColor clearColor] forState:UIControlStateNormal];
//    [self setBackgroundColor:[UIColor clearColor] forState:UIControlStateHighlighted];
}


#pragma mark -
#pragma mark Touches

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesBegan:touches withEvent:event];
    
    UIColor *selectedColor = [backgroundStates objectForKey:[NSNumber numberWithInt:UIControlStateHighlighted]];
    if (selectedColor) {
        CATransition *animation = [CATransition animation];
        [animation setType:kCATransitionFade];
        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
        [self.layer addAnimation:animation forKey:@"EaseOut"];
        self.backgroundColor = selectedColor;
    }
    else {
        CATransition *animation = [CATransition animation];
        [animation setType:kCATransitionFade];
        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
        [self.layer addAnimation:animation forKey:@"EaseOut"];
        self.alpha = 0.7;
    }
}

- (void) touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesCancelled:touches withEvent:event];
    
    UIColor *normalColor = [backgroundStates objectForKey:[NSNumber numberWithInt:UIControlStateNormal]];
    if (normalColor) {
        CATransition *animation = [CATransition animation];
        [animation setType:kCATransitionFade];
        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
        [self.layer addAnimation:animation forKey:@"EaseOut"];
        self.backgroundColor = normalColor;
    }
    else {
        CATransition *animation = [CATransition animation];
        [animation setType:kCATransitionFade];
        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
        [self.layer addAnimation:animation forKey:@"EaseOut"];
        self.alpha = 1.0;
    }
}

- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesEnded:touches withEvent:event];
    
    UIColor *normalColor = [backgroundStates objectForKey:[NSNumber numberWithInt:UIControlStateNormal]];
    if (normalColor) {
        CATransition *animation = [CATransition animation];
        [animation setType:kCATransitionFade];
        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
        [self.layer addAnimation:animation forKey:@"EaseOut"];
        self.backgroundColor = normalColor;
    }
    else {
        CATransition *animation = [CATransition animation];
        [animation setType:kCATransitionFade];
        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
        [self.layer addAnimation:animation forKey:@"EaseOut"];
        self.alpha = 1.0;
    }
}

- (void) dealloc {
    //    [backgroundStates release];
    //    [super dealloc];
}

@end
