//
//  TXButton.h
//  Taxi

//
#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>
#import "UIUtil.h"

@interface TXButton : UIButton
{
@private
    NSMutableDictionary *backgroundStates;
@public
    
}

- (id)initUnder:(UIView *) view dim:(float) pix;
- (id)initOnTopOf:(UIView *) view dim:(float) pix;

- (void) setBackgroundColor:(UIColor *) _backgroundColor forState:(UIControlState) _state;
- (UIColor*) backgroundColorForState:(UIControlState) _state;

- (void) setBasicButton:(NSString*)_text;
- (void) setBasicText:(NSString*)_text;
- (void) setBasicText:(NSString*)_text fontsize:(float)_size;
- (void) setBasicColor:(UIColor *)_textColor background:(UIColor *)_backgroundColor highlighted:(UIColor *)_highlightColor;
//- (void) setBasic:(NSString*)_text textColor:(UIColor *)_textColor background:(UIColor *)_backgroundColor highlighted:(UIColor *)_highlightColor;

@end
