//
//  UIConsts.h
//  Taxi
//

//

#import <UIKit/UIKit.h>

typedef enum {
    kSettingTitletHeight = 42,
    kSettingDefaultHeight = 38,
    kTextFieldDefaultHeight = 53,
    kButtonDefaultHeight = 50
} Heights;

typedef enum {
    kTitlePaddingFromLeft = 25,
    kTitlePaddingFromTop = 30,
    kLabelPaddingFromLeft = 30,
    kDefaultPaddingFromLeft = 16,
	kDefaultPaddingFromRight = 8,
    kDefaultPaddingFromTop = 2,
    kDefaultPaddingFromBottom = 2,
    kDefaultPaddingFromTopScreen = ((int)99.0f + kTextFieldDefaultHeight),
    kDefaultPaddingFromBottomScreen = 20,
} Paddings;
