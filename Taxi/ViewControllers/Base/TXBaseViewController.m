//
//  TXRootVC.m
//  Taxi
//

//

#import "TXBaseViewController.h"
#import "TXSharedObj.h"
#import "SVProgressHUD.h"

#import "UIImage+Extra.h"

@import IQKeyboardManager;

@interface TXBaseViewController () <UITextFieldDelegate>{
    UIView *statusView;
    
    UIView *navigationView;
    UIView *backgroundView;
    
    CGRect mainframe;
}

@end

@implementation TXBaseViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self->model = [TXUserModel instance];
        self->appDelegate = (TXAppDelegate *)[[UIApplication sharedApplication] delegate];
        self->navigationView = nil;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	self.view.userInteractionEnabled = TRUE;
    
    self.view.autoresizingMask =  UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    self.view.contentMode = UIViewContentModeScaleAspectFill;
    
    [[IQKeyboardManager sharedManager] setEnable:NO];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(statusBarFrameChanged:) name:UIApplicationDidChangeStatusBarFrameNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(statusBarFrameWillChange:) name:UIApplicationWillChangeStatusBarFrameNotification object:nil];
    
    self.view.frame = self->appDelegate.frostedViewController.view.frame;
    [self configureOnce];
    
    [self configure];
}

-(void)viewWillAppear:(BOOL)animated {
    static BOOL isLoad;
    [super viewWillAppear:animated];
    [self configureStyles];
    
//    if (isLoad) return;
    //[self configure];
    isLoad = YES;
    
    UIViewController *currentVC = self.navigationController.visibleViewController;
    NSString *className = NSStringFromClass([currentVC class]);
    //NSLog(@"The Current Class : %@", className);
    if ([className isEqualToString:@"TXMapVC"]) {
        appDelegate.isMapView = YES;
    }
    else {
        appDelegate.mapVCBeforeVC = className;
        appDelegate.isMapView = NO;
    }
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    //[self configureStyles];
}

-(void)viewWillLayoutSubviews{
    [super viewWillLayoutSubviews];
    
    [self refreshView];
    //self.view.frame = self->appDelegate.frostedViewController.view.frame;
    
    //mainframe = self.view.frame;
}

- (void)statusBarFrameWillChange:(NSNotification*)notification {
    NSValue* rectValue = [[notification userInfo] valueForKey:UIApplicationStatusBarFrameUserInfoKey];
    CGRect newFrame;
    [rectValue getValue:&newFrame];
    //NSLog(@"statusBarFrameWillChange: newSize %f, %f", newFrame.size.width, newFrame.size.height);
}

- (void)statusBarFrameChanged:(NSNotification*)notification {
    NSValue* rectValue = [[notification userInfo] valueForKey:UIApplicationStatusBarFrameUserInfoKey];
    CGRect oldFrame;
    [rectValue getValue:&oldFrame];
    //NSLog(@"statusBarFrameChanged: oldSize %f, %f", oldFrame.size.width, oldFrame.size.height);
}

#pragma mark -
-(void) configureOnce {
    self->x = self.view.frame.origin.x;
    self->y = self.view.frame.origin.y;
    self->width = self.view.frame.size.width;
    self->height = self.view.frame.size.height;
    
    self->backgroundView = [[UIImageView alloc] init];
    //[self->backgroundView setBackgroundColor:UIColorBasicBack];
    [self->backgroundView setBackgroundColor:[UIColor clearColor]];
    self->backgroundView.frame = self.view.bounds;
    [[self view] addSubview:self->backgroundView];
    [[self view] sendSubviewToBack:self->backgroundView];
}

-(void) configure {
    self->x = self.view.frame.origin.x;
    self->y = self.view.frame.origin.y;
    self->width = self.view.frame.size.width;
    self->height = self.view.frame.size.height;
    
    if(self->navigationView == nil) {
        self->backgroundView.frame = self.view.bounds;
        [self->backgroundView setBackgroundColor:[UIColor clearColor]];
        [[self view] sendSubviewToBack:self->backgroundView];
        
        [self configureNaviBar];
    }
    
    [self statusBarStyleDefault];
}

-(void) configureMain {
    self->x = self.view.frame.origin.x;
    self->y = self.view.frame.origin.y;
    self->width = self.view.frame.size.width;
    self->height = self.view.frame.size.height;
    
    if(self->navigationView == nil) {
        self->backgroundView.frame = self.view.bounds;
        [self->backgroundView setBackgroundColor:[UIColor clearColor]];
        [[self view] sendSubviewToBack:self->backgroundView];
        
        [self configureMainNaviBar];
    }
    
    [self statusBarStyleDefault];
}

-(UIView *)backroundView {
    return self->backgroundView;
}

-(void) configureStyles {
    
}

- (void)statusBarStyle
{
    [self->statusView setBackgroundColor:[UIColor whiteColor]];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

- (void)statusBarStyleDefault
{
    [self->statusView setBackgroundColor:[UIColor whiteColor]];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
}

- (void)statusBarStyleLightContent
{
    [self->statusView setBackgroundColor:[UIColor whiteColor]];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

- (void)statusBarStyleLauncher
{
    [self->statusView setBackgroundColor:[UIColor blackColor]];
    self->statusView.hidden = YES;
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
}

- (void)statusBarStyleWhite
{
    [self->statusView setBackgroundColor:[UIColor whiteColor]];
    [self->navigationView setBackgroundColor:[UIColor whiteColor]];
}

- (void)statusBarStyleTestMode
{
    [self->statusView setBackgroundColor:UIColorDefault];
    [self->navigationView setBackgroundColor:UIColorDefault];
}


- (void)bringTOTopstatusBar:(UIView*)view
{
    [view bringSubviewToFront:self->statusView];
}

-(NSInteger)navigationTopHeight {
    return self->navigationView.frame.size.height;
}

-(void)navigationClear {
    self->navigationView.backgroundColor = [UIColor clearColor];
}

-(void) configureNaviBar {
    [self.navigationController.navigationBar setHidden:YES];
    CGRect rect = self.view.bounds;
    rect.origin.y = kNaviTopHeight;
    rect.size.height -= kNaviTopHeight;
    
    self->statusView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, kTopHeight)];
    self->navigationView = [[UIView alloc] initWithFrame:CGRectMake(0, kTopHeight, self.view.bounds.size.width, kNaviHeight)];
    self->navigationView.backgroundColor = [UIColor clearColor];
    self->navigationView.backgroundColor = [UIColor whiteColor];
    self->navigationView.hidden = YES;
    
    self->statusView.tag = 50;
    self->navigationView.tag = 60;
    
    /* Create custom view to display section header... */
    
    [self navigationViewLeftImageButton];
    [self navigationViewLeftRadiusImageButton];
    
    [self navigationViewCenterImageButton];
    [self navigationViewCenterTextButton];
    [self navigationViewCenterDoubleTextButton];
    
    [self navigationViewRightRadiusImageButton];
    [self navigationViewRightImageButton];
    
    [[self view] addSubview:self->navigationView];
    [[self view] addSubview:self->statusView];
}

-(void) configureMainNaviBar {
    [self.navigationController.navigationBar setHidden:YES];
    CGRect rect = self.view.bounds;
    rect.origin.y = kMainNaviTopHeight;
    rect.size.height -= kMainNaviTopHeight;
    
    self->statusView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, kTopHeight)];
    self->navigationView = [[UIView alloc] initWithFrame:CGRectMake(0, kTopHeight, self.view.bounds.size.width, kMainNaviHeight)];
    self->navigationView.backgroundColor = [UIColor clearColor];
    self->navigationView.backgroundColor = [UIColor whiteColor];
    self->navigationView.hidden = YES;
    
    self->statusView.tag = 50;
    self->navigationView.tag = 60;
    /* Create custom view to display section header... */
    
    [self navigationViewLeftImageButton];
    [self navigationViewLeftImageButton2];
    [self navigationViewLeftImageButton3];
    [self navigationViewLeftRadiusImageButton];
    
    [self navigationViewCenterImageButton];
    [self navigationViewCenterTextButton];
    [self navigationViewCenterDoubleTextButton];
    
    [self navigationViewRightRadiusImageButton];
    [self navigationViewRightImageButton];
    
    [[self view] addSubview:self->navigationView];
    [[self view] addSubview:self->statusView];
}

-(void) configureBottomList {
    [self navigationViewBottomList];
}

-(void) configureBottomLine {
    [self navigationViewBottomLine];
}

-(void) configureAccount {
    [self navigationViewAccount];
}



-(void)navigationViewBottomLine {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(kLeftMargin,
                                                            kNaviTopHeight + kNaviBottomNullHeight - 1,
                                                            self.view.bounds.size.width-(
                                                                                         kLeftMargin * 2),
                                                            1)];
    view.backgroundColor = UIColorBasicBack;
    view.tag = 1500;
    [self->navigationView addSubview:view];
    
    CGRect frame = self->navigationView.frame;
    frame.size.height = kNaviTopHeight + kNaviBottomNullHeight;
    self->navigationView.frame = frame;
}

-(void)navigationViewBottomList {
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(kLeftMargin,
                                                               kNaviTopHeight,
                                                               self.view.bounds.size.width-(
                                                                                            kLeftMargin * 2),
                                                               kNaviBottomTextHeight)];
    [label setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:12]];
    label.textAlignment = NSTextAlignmentLeft;
    label.tag = 1111;
    [label setText:@""];
    
    [self->navigationView addSubview:label];
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(kLeftMargin,
                                                            kNaviTopHeight + kNaviBottomHeight - 1,
                                                            self.view.bounds.size.width-(
                                                                                         kLeftMargin * 2),
                                                            1)];
    view.backgroundColor = UIColorBasicBack;
    view.tag = 1500;
    [self->navigationView addSubview:view];
    
    CGRect frame = self->navigationView.frame;
    frame.size.height = kNaviTopHeight + kNaviBottomHeight;
    self->navigationView.frame = frame;
}

-(void)navigationViewAccount {
    UIImageView *image = [[UIImageView alloc] init];
    
    image.tag = 9998;
//    image.hidden = YES;
    image.frame = CGRectMake(self.view.bounds.size.width - kLeftMargin - 88, 15, 88, 88);
    //[image setImage:[UIImage imageNamed:@"btn_camera"]];
    image.layer.cornerRadius = image.frame.size.width / 2;
    //image.layer.borderWidth = 3.0f;
    //button.layer.borderColor = [UIColor whiteColor].CGColor;
    image.clipsToBounds = YES;
    image.contentMode = UIViewContentModeScaleAspectFill;
    
    [self->navigationView addSubview:image];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = image.frame;
    button.tag = 9999;
    button.backgroundColor = [UIColor clearColor];
//    [button setImage:[UIImage imageNamed:@"btn_camera"] forState:UIControlStateNormal];
//    button.layer.cornerRadius = button.frame.size.width / 2;
//    button.layer.borderWidth = 3.0f;
//    //button.layer.borderColor = [UIColor whiteColor].CGColor;
//    button.clipsToBounds = YES;
    
    [self->navigationView addSubview:button];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(kLeftMargin,
                                                               113,
                                                               self.view.bounds.size.width-(
                                                                                            kLeftMargin * 2),
                                                               kNaviBottomTextHeight)];
    [label setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:12]];
    label.textAlignment = NSTextAlignmentLeft;
    label.tag = 1111;
    [label setText:@""];
    label.textColor = HEXCOLOR(0x666666FF);
    
    [self->navigationView addSubview:label];
    
    NSInteger h_ = button.frame.origin.y + button.frame.size.height + 35;
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(kLeftMargin,
                                                            h_ - 1,
                                                            self.view.bounds.size.width-(
                                                                                         kLeftMargin * 2),
                                                            1)];
    view.backgroundColor = UIColorBasicBack;
    [self->navigationView addSubview:view];
    
    CGRect frame = self->navigationView.frame;
    frame.size.height = h_;
    self->navigationView.frame = frame;
}

-(void)navigationViewLeftRadiusImageButton {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    //[button setTitle:@"<" forState:UIControlStateNormal];
    [button sizeToFit];
    button.tag = 1101;
    button.hidden = YES;
    //button.frame = CGRectMake(kLeftMargin, kRadiusImageTopMargin, kRadiusImageSize, kRadiusImageSize);
    button.frame = CGRectMake(0, 0, kRadiusImageSize + kRadiusImageTopMargin*2, kRadiusImageSize + kRadiusImageTopMargin*2);
    
//    button.layer.cornerRadius = button.frame.size.width / 2;
//    button.layer.borderWidth = 3.0f;
//    button.layer.borderColor = [UIColor whiteColor].CGColor;
//    button.clipsToBounds = YES;
    
    // Add an action in current code file (i.e. target)
    [button addTarget:self action:@selector(onNaviButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    
    [self->navigationView addSubview:button];
}

-(void)navigationViewLeftImageButton {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    //[button setTitle:@"X" forState:UIControlStateNormal];
    [button sizeToFit];
    button.tag = 1100;
    button.hidden = YES;
    //button.frame = CGRectMake(kLeftMargin, kRadiusImageTopMargin, kRadiusImageSize, kRadiusImageSize);
    button.frame = CGRectMake(kLeftMargin, kLeftMargin, kRadiusImageSize, kRadiusImageSize);
    [button setImageEdgeInsets:UIEdgeInsetsMake(0,-6,0,0)];
    
    // Add an action in current code file (i.e. target)
    [button addTarget:self action:@selector(onNaviButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    
    [self->navigationView addSubview:button];
}

-(void)navigationViewLeftImageButton2 {
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(kLeftMargin+kRadiusImageSize+kLeftMargin, kLeftMargin, kRadiusImageSize, kRadiusImageSize)];
    //[button setTitle:@"X" forState:UIControlStateNormal];
    [button sizeToFit];
    button.tag = 1102;
    button.hidden = YES;
    //button.frame = CGRectMake(kLeftMargin, kRadiusImageTopMargin, kRadiusImageSize, kRadiusImageSize);
    //button.frame = CGRectMake(kLeftMargin+kRadiusImageSize+kLeftMargin, kLeftMargin, kRadiusImageSize, kRadiusImageSize);
    //[button setImageEdgeInsets:UIEdgeInsetsMake(0,0,0,0)];
    
    // Add an action in current code file (i.e. target)
    [button addTarget:self action:@selector(onNaviButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    
    [self->navigationView addSubview:button];
}

-(void)navigationViewLeftImageButton3 {
    //UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(kLeftMargin+kRadiusImageSize+kLeftMargin+kRadiusImageSize+kLeftMargin, kLeftMargin, kRadiusImageSize, kRadiusImageSize)];
    //[button setTitle:@"X" forState:UIControlStateNormal];
    [button sizeToFit];
    button.tag = 1103;
    button.hidden = YES;
    //button.frame = CGRectMake(kLeftMargin, kRadiusImageTopMargin, kRadiusImageSize, kRadiusImageSize);
    //button.frame = CGRectMake(kLeftMargin+kRadiusImageSize+kLeftMargin+kRadiusImageSize+kLeftMargin, kLeftMargin, kRadiusImageSize, kRadiusImageSize);
    //[button setImageEdgeInsets:UIEdgeInsetsMake(0,-6,0,0)];
    
    // Add an action in current code file (i.e. target)
    [button addTarget:self action:@selector(onNaviButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    
    [self->navigationView addSubview:button];
}

-(void)navigationViewCenterImageButton {
    UIImageView *button = [[UIImageView alloc] init];
    
    button.tag = 1200;
    button.hidden = YES;
    button.frame = CGRectMake(self.view.bounds.size.width/2 - kRadiusCenterImageSize/2,
                              kRadiusImageTopMargin,
                              kRadiusCenterImageSize,
                              kRadiusImageSize);
    
    [self->navigationView addSubview:button];
}

-(void)navigationViewCenterTextButton {
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(kLeftMargin,
                                                               kNaviTextMargin,
                                                               self.view.bounds.size.width-(
                                                               kLeftMargin * 2),
                                                               kNaviTextHeight)];
    [label setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:28]];
    label.textAlignment = NSTextAlignmentLeft;
    label.tag = 1210;
    label.adjustsFontSizeToFitWidth = YES;
    
    label.hidden = YES;
    [label setText:@"test..."];
    [label setBackgroundColor:[UIColor clearColor]];
    
    [self->navigationView addSubview:label];
    
    UIView *v = [[UIView alloc] initWithFrame:CGRectMake(30, 46, self.view.frame.size.width - 30*2, 34)];
    v.tag = 1220;
    v.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    v.hidden = YES;
    
    UILabel *lb = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 60, 33)];
    [lb setFont:[UIFont fontWithName:@"ArialRoundedMTBold" size:13]];
    lb.textAlignment = NSTextAlignmentLeft;
    [lb setBackgroundColor:[UIColor clearColor]];
#ifdef _WITZM
    [lb setText:@"Name"];
    //[lb setText:LocalizedStr(@"PRE_ORDER_Place_Confirm.input.placeholder.text")];
    lb.textColor = [UIColor whiteColor];
#elif defined _CHACHA
    [lb setText:LocalizedStr(@"PRE_ORDER_Place_Confirm.input.placeholder.text")];
    //[lb setText:@"장소이름"];
    lb.textColor = UIColorGray;
#endif

    UITextField *textField = [[UITextField alloc] initWithFrame:CGRectMake(60, 0, v.frame.size.width - 60, 33)];
    textField.tag = 10;
    textField.delegate = self;
    textField.enablesReturnKeyAutomatically = YES;
    textField.autocapitalizationType = NO;
    textField.autocorrectionType = NO;
    textField.clearButtonMode = UITextFieldViewModeNever;
    textField.keyboardType = UIKeyboardTypeDefault;
    textField.returnKeyType = UIReturnKeyDone;
    textField.textAlignment = NSTextAlignmentRight;
    [textField setBackgroundColor:[UIColor clearColor]];
    [textField setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:28]];
    
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, v.frame.size.height-1, v.frame.size.width, 1)];
#ifdef _WITZM
    line.backgroundColor = HEXCOLOR(0xffffff7f);
#elif defined _CHACHA
    line.backgroundColor = HEXCOLOR(0xcccccc7f);
#endif
    [v addSubview:lb];
    [v addSubview:textField];
    [v addSubview:line];
    [self->navigationView addSubview:v];
}

-(void)navigationViewCenterDoubleTextButton {
    // first label
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(kLeftMargin * 2 + kRadiusImageSize,
                                                               kRadiusImageTopMargin,
                                                               self.view.bounds.size.width-(
                                                                                            kLeftMargin * 2 + kRadiusImageSize)*2,
                                                               kRadiusImageSize)];
    [label setFont:[UIFont boldSystemFontOfSize:21]];
    label.textAlignment = NSTextAlignmentCenter;
    label.tag = 1211;
    label.hidden = YES;
    [label setText:@"test..."];
    
    [self->navigationView addSubview:label];
    
    // second label
    label = [[UILabel alloc] initWithFrame:CGRectMake(kLeftMargin * 2 + kRadiusImageSize,
                                                      label.frame.origin.y + label.frame.size.height + 2,
                                                      self.view.bounds.size.width-(
                                                                                   kLeftMargin * 2 + kRadiusImageSize)*2,
                                                      label.frame.origin.y + label.frame.size.height + 2 - kNaviHeight - 2)];
    [label setFont:[UIFont boldSystemFontOfSize:21]];
    label.textAlignment = NSTextAlignmentCenter;
    label.tag = 1212;
    label.hidden = YES;
    [label setText:@"test..."];
    
    [self->navigationView addSubview:label];
}

-(void)navigationViewRightRadiusImageButton {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeSystem];
    //[button setTitle:@"<" forState:UIControlStateNormal];
    [button sizeToFit];
    button.tag = 1301;
    button.hidden = YES;
    [button setTitleColor:UIColorBasicText forState:UIControlStateNormal];
    button.frame = CGRectMake(self.view.bounds.size.width - kLeftMargin - kRadiusImageSize,
                              kRadiusImageTopMargin,
                              kRadiusImageSize,
                              kRadiusImageSize);
    //button.frame = CGRectMake(self.view.bounds.size.width - kBasicMargin - kRadiusImageSize, 0, kRadiusImageSize + kRadiusImageTopMargin*2, kRadiusImageSize + kRadiusImageTopMargin*2);
    button.layer.cornerRadius = button.frame.size.width / 2;
//    button.layer.borderWidth = 3.0f;
//    button.layer.borderColor = [UIColor whiteColor].CGColor;
    button.clipsToBounds = YES;
    [Utils setCircleImage:button];
    
    // Add an action in current code file (i.e. target)
    [button addTarget:self action:@selector(onNaviButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    
    [self->navigationView addSubview:button];
}

-(void)navigationViewRightImageButton {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeSystem];
    //[button setTitle:@"X" forState:UIControlStateNormal];
    //[button sizeToFit];
    button.tag = 1300;
    button.titleLabel.adjustsFontSizeToFitWidth = YES;
    button.hidden = YES;
    [button setTitleColor:UIColorBasicText forState:UIControlStateNormal];
    button.frame = CGRectMake(self.view.bounds.size.width - kLeftMargin - kRadiusImageSize,
                              kRadiusImageTopMargin,
                              kRadiusImageSize,
                              kRadiusImageSize);
    button.frame = CGRectMake(self.view.bounds.size.width - kBasicMargin - kRadiusImageSize, 0, kRadiusImageSize + kRadiusImageTopMargin*2, kRadiusImageSize + kRadiusImageTopMargin*2);
    //button.imageView.contentMode = UIViewContentModeScaleAspectFill;
    //button.titleLabel.font = [UIFont systemFontOfSize:14];
    button.titleLabel.numberOfLines = 1;
    button.titleLabel.adjustsFontSizeToFitWidth = YES;
    button.titleLabel.lineBreakMode = NSLineBreakByClipping;
    
    // Add an action in current code file (i.e. target)
    [button addTarget:self action:@selector(onNaviButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    
    [self->navigationView addSubview:button];
}

-(void)navigationBottomText:(NSString*)centerText{
    
    UILabel *lb;
    
    lb = [self.view viewWithTag:1111];
    [lb setText:centerText];
    lb.hidden = NO;
}

// < Image
-(void)navigationType00X:(UIImage*)leftImage centerImage:(UIImage*)centerImage{
    
    UIButton *btn;
    
    btn = [self.view viewWithTag:1100];
    if (leftImage != nil) {
        //[btn setBackgroundImage:leftImage forState:UIControlStateNormal];
        [btn setImage:leftImage forState:UIControlStateNormal];
    }
    btn.hidden = NO;
    
    UIImageView *imgV = [self.view viewWithTag:1200];
    if (centerImage != nil) {
        imgV.contentMode = UIViewContentModeCenter;
        imgV.image = centerImage;
    }
    imgV.hidden = NO;
    
    [self.view viewWithTag:1101].hidden = YES;
    [self.view viewWithTag:1109].hidden = YES;
    
    [self.view viewWithTag:1210].hidden = YES;
    [self.view viewWithTag:1211].hidden = YES;
    [self.view viewWithTag:1212].hidden = YES;
    
    
    [self.view viewWithTag:1300].hidden = YES;
    [self.view viewWithTag:1301].hidden = YES;
    
    self->navigationView.hidden = NO;
}

// < Text
-(void)navigationType01X:(UIImage*)leftImage centerText:(NSString*)centerText{
    
    UIButton *btn;
    UILabel *lb;
    
    btn = [self.view viewWithTag:1100];
    if (leftImage != nil) {
        //[btn setBackgroundImage:leftImage forState:UIControlStateNormal];
        [btn setImage:leftImage forState:UIControlStateNormal];
    }
    btn.hidden = NO;
    
    lb = [self.view viewWithTag:1210];
    [lb setText:centerText];
    lb.hidden = NO;
    
    [self.view viewWithTag:1101].hidden = YES;
    [self.view viewWithTag:1109].hidden = YES;
    
    [self.view viewWithTag:1211].hidden = YES;
    [self.view viewWithTag:1212].hidden = YES;
    [self.view viewWithTag:1200].hidden = YES;
    
    [self.view viewWithTag:1300].hidden = YES;
    [self.view viewWithTag:1301].hidden = YES;
    
    self->navigationView.hidden = NO;
    
}

// < Image
-(void)navigationType10X:(UIImage*)leftImage centerImage:(UIImage*)centerImage{
    
    UIButton *btn;
    btn = [self.view viewWithTag:1101];
    if (leftImage != nil) {
        [btn setBackgroundImage:leftImage forState:UIControlStateNormal];
    }
    btn.hidden = NO;
    
    UIImageView *imgV = [self.view viewWithTag:1200];
    if (centerImage != nil) {
        imgV.contentMode = UIViewContentModeCenter;
        imgV.image = centerImage;
    }
    imgV.hidden = NO;
    
//        btn = [self.view viewWithTag:1200];
//        if (centerImage != nil) {
//            [btn setBackgroundImage:centerImage forState:UIControlStateNormal];
//        }
//        btn.hidden = NO;
    
    [self.view viewWithTag:1100].hidden = YES;
    
    [self.view viewWithTag:1210].hidden = YES;
    [self.view viewWithTag:1211].hidden = YES;
    [self.view viewWithTag:1212].hidden = YES;
    
    
    [self.view viewWithTag:1300].hidden = YES;
    [self.view viewWithTag:1301].hidden = YES;
    
    self->navigationView.hidden = NO;
}

// < Image Image
-(void)navigationType101:(UIImage*)leftImage centerImage:(UIImage*)centerImage rightImage:(UIImage*)rightImage{
    
    UIButton *btn;
    btn = [self.view viewWithTag:1101];
    if (leftImage != nil) {
        [btn setBackgroundImage:leftImage forState:UIControlStateNormal];
    }
    btn.hidden = NO;
    
    UIImageView *imgV = [self.view viewWithTag:1200];
    if (centerImage != nil) {
        imgV.contentMode = UIViewContentModeCenter;
        imgV.image = centerImage;
    }
    imgV.hidden = NO;
    
//        btn = [self.view viewWithTag:1200];
//        if (centerImage != nil) {
//            [btn setBackgroundImage:centerImage forState:UIControlStateNormal];
//        }
//        btn.hidden = NO;
    
    btn = [self.view viewWithTag:1301];
    if (rightImage != nil) {
        [btn setBackgroundImage:rightImage forState:UIControlStateNormal];
    }
    btn.hidden = NO;
    
    [self.view viewWithTag:1100].hidden = YES;
    
    [self.view viewWithTag:1210].hidden = YES;
    [self.view viewWithTag:1211].hidden = YES;
    [self.view viewWithTag:1212].hidden = YES;
    
    
    [self.view viewWithTag:1300].hidden = YES;
    //[self.view viewWithTag:1301].hidden = YES;
    
    self->navigationView.hidden = NO;
}

// < Text X
-(void)navigationType010:(UIImage*)leftImage centerText:(NSString*)centerText rightImage:(UIImage*)rightImage{
    
    UIButton *btn;
    UILabel *lb;
    
    btn = [self.view viewWithTag:1100];
    if (leftImage != nil) {
        //[btn setBackgroundImage:leftImage forState:UIControlStateNormal];
        [btn setImage:leftImage forState:UIControlStateNormal];
    }
    btn.hidden = NO;
    
    lb = [self.view viewWithTag:1210];
    [lb setText:centerText];
    lb.hidden = NO;
    
    btn = [self.view viewWithTag:1300];
    if (rightImage != nil) {
        [btn setBackgroundImage:rightImage forState:UIControlStateNormal];
    }
    btn.hidden = NO;
    
    [self.view viewWithTag:1101].hidden = YES;
    [self.view viewWithTag:1109].hidden = YES;
    
    [self.view viewWithTag:1211].hidden = YES;
    [self.view viewWithTag:1212].hidden = YES;
    [self.view viewWithTag:1200].hidden = YES;
    
    [self.view viewWithTag:1301].hidden = YES;
    
    self->navigationView.hidden = NO;

}

// < Text X
-(void)navigationType110:(UIImage*)leftImage centerText:(NSString*)centerText rightImage:(UIImage*)rightImage{
    
    UIButton *btn;
    UILabel *lb;
    
    btn = [self.view viewWithTag:1101];
    if (leftImage != nil) {
        [btn setBackgroundImage:leftImage forState:UIControlStateNormal];
    }
    btn.hidden = NO;
    
    lb = [self.view viewWithTag:1210];
    [lb setText:centerText];
    lb.hidden = NO;
    
    btn = [self.view viewWithTag:1300];
    if (rightImage != nil) {
        [btn setBackgroundImage:rightImage forState:UIControlStateNormal];
    }
    btn.hidden = NO;
    
    [self.view viewWithTag:1100].hidden = YES;
    
    [self.view viewWithTag:1211].hidden = YES;
    [self.view viewWithTag:1212].hidden = YES;
    [self.view viewWithTag:1200].hidden = YES;
    
    [self.view viewWithTag:1301].hidden = YES;
    
    self->navigationView.hidden = NO;

}

// < Text X
-(void)navigationType111:(UIImage*)leftImage centerText:(NSString*)centerText rightImage:(UIImage*)rightImage{
    
    UIButton *btn;
    UILabel *lb;
    
    btn = [self.view viewWithTag:1101];
    if (leftImage != nil) {
        [btn setBackgroundImage:leftImage forState:UIControlStateNormal];
    }
    btn.hidden = NO;
    
    lb = [self.view viewWithTag:1210];
    [lb setText:centerText];
    lb.hidden = NO;
    
    btn = [self.view viewWithTag:1301];
    if (rightImage != nil) {
        [btn setBackgroundImage:rightImage forState:UIControlStateNormal];
    }
    btn.hidden = NO;
    
    [self.view viewWithTag:1100].hidden = YES;
    
    [self.view viewWithTag:1211].hidden = YES;
    [self.view viewWithTag:1212].hidden = YES;
    [self.view viewWithTag:1200].hidden = YES;
    
    [self.view viewWithTag:1300].hidden = YES;
    
    self->navigationView.hidden = NO;

}

// < Text X
-(void)navigationType11X:(UIImage*)leftImage centerText:(NSString*)centerText rightText:(NSString*)rightText{
    
    UIButton *btn;
    UILabel *lb;
    
    btn = [self.view viewWithTag:1101];
    if (leftImage != nil) {
        [btn setBackgroundImage:leftImage forState:UIControlStateNormal];
        btn.hidden = NO;
    }
    else {
        btn.hidden = YES;
    }
    
    lb = [self.view viewWithTag:1210];
    [lb setText:centerText];
    lb.hidden = NO;
    
    btn = [self.view viewWithTag:1300];
    if (![rightText isEqualToString:@""]) {
        [btn setTitle:rightText forState:UIControlStateNormal];
    }
    btn.hidden = NO;
    
    [self.view viewWithTag:1100].hidden = YES;
    
    [self.view viewWithTag:1211].hidden = YES;
    [self.view viewWithTag:1212].hidden = YES;
    [self.view viewWithTag:1200].hidden = YES;
    
    [self.view viewWithTag:1301].hidden = YES;
    
    self->navigationView.hidden = NO;

}


-(UIView *)navigationView {
    return self->navigationView;
}

-(UIView *)statusView {
    return self->statusView;
}

-(void)refreshView {
    CGRect rect;
    rect = [[UIApplication sharedApplication] statusBarFrame];
    //NSLog(@"Statusbar frame: %1.0f, %1.0f, %1.0f, %1.0f", rect.origin.x, rect.origin.y, rect.size.width, rect.size.height);
    if (rect.size.height == 20) {
        self.view.frame = self->appDelegate.frostedViewController.view.frame;
    }
}
/*
 * By default all the view controllers disable auto rotation,
 * if any UI view wants to display UI controls in landscape orientation, view controller must override this function
 */
-(BOOL)shouldAutorotate {
    return NO;
}

/*
 * By default all the view controllers handle touchesBegan to hide keyboard when it is not necessary,
 * If any UI view wants to force keyboard to be displayed permanently, it's controller should override this function
 */
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [[self view] endEditing:YES];
    //[super touchesBegan: touches withEvent: event];
}

- (IBAction)backgroundTouched:(id)sender {
    [self.view endEditing:YES];
}

-(void) pushViewController : (TXBaseViewController *) viewController {
//    CATransition* transition = [CATransition animation];
//    transition.duration = 0.5;
//    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
//    transition.type = kCATransitionFade; //kCATransitionMoveIn; //, kCATransitionPush, kCATransitionReveal, kCATransitionFade
//    //transition.subtype = kCATransitionFromTop; //kCATransitionFromLeft, kCATransitionFromRight, kCATransitionFromTop, kCATransitionFromBottom
//    
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFromRight;
//    [self.navigationController.view.layer addAnimation:transition forKey:nil];
//    //[[self navigationController] popViewControllerAnimated:NO];
//    
    //[[self navigationController] pushViewController:viewController animated:YES];
    [self.navigationController pushViewController:viewController animated:YES];
    
    //[self presentViewController:viewController animated:YES completion:nil];
    
}

-(void) presentViewController : (TXBaseViewController *) viewController {
    [self presentViewController:viewController animated:YES completion:nil];
}

-(void) pushViewController_old : (TXBaseViewController *) viewController {
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.25;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    
    CGRect rect = viewController.view.bounds;
    //rect.origin.y = 64;
    //rect.size.height -= 64;
    viewController.view.bounds = rect;
    viewController.view.frame = rect;
    
//    [self.view.layer addAnimation:transition forKey:kCATransition];
    [self presentViewController:viewController animated:YES completion:nil];

}

-(TXBaseViewController *) vcFromName: (NSString *) name {
    return [[TXBaseViewController alloc] initWithNibName:name bundle:nil];
}

-(void) alertError : (NSString *) title message : (NSString *) message {
    
    [Utils onErrorAlertViewTitle:message title:title];
//    [[[UIAlertView alloc] initWithTitle:title
//                                message:message
//                               delegate:nil
//                      cancelButtonTitle:@"OK"
//                      otherButtonTitles:nil] show];
    
}

-(void) setParameters:(NSDictionary *)params {
    self->parameters = params;
}

-(void) showBusyIndicator {
    [self showBusyIndicator:nil];
}

-(void) showBusyIndicator:(NSString *)title {
    if (!appDelegate.isNetwork) {
        [self hideBusyIndicator];
        return;
    }
    
    if ([SVProgressHUD isVisible])
        return;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        //[SVProgressHUD showWithStatus:title];
        [SVProgressHUD showWithStatus:nil];
        //[SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeNone];
        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
        [SVProgressHUD setDefaultStyle:SVProgressHUDStyleCustom];
        [SVProgressHUD setBackgroundColor:[UIColor clearColor]];
        [SVProgressHUD setForegroundColor:UIColorDefault];
        [SVProgressHUD setRingThickness:5];
    });
}

-(void)hideBusyIndicator {
    if (!appDelegate.isNetwork) {
        return;
    }
    if ([SVProgressHUD isVisible]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
        });
    }
}

/*
 * Subclasses should override this function
 */
-(void)onEvent:(TXEvent *)event eventParams:(id)subscriptionParams{
    NSLog(@"Function onEvent not implemented in TXRootVC !");
}


-(void)refreshInterfaceBasedOnSignIn {
    NSLog(@"Function refreshInterfaceBasedOnSignIn not implemented in TXRootVC !");
}

- (void)onNaviButtonClick:(UIButton *)button {
    //NSLog(@"Button Pressed");
    NSLog(@"onNaviButtonClick");
}

- (UIImage *) getScaledImage:(UIImage *)img insideButton:(UIButton *)btn {
    
    // Check which dimension (width or height) to pay respect to and
    // calculate the scale factor
    CGFloat imgRatio = img.size.width / img.size.height;
    CGFloat btnRatio = btn.frame.size.width / btn.frame.size.height;
    CGFloat scaleFactor = (imgRatio > btnRatio)
    ? img.size.width / btn.frame.size.width
    : img.size.height / btn.frame.size.height;
    
    // Create image using scale factor
    UIImage *scaledImg = [UIImage imageWithCGImage:[img CGImage]
                                             scale:scaleFactor
                                       orientation:UIImageOrientationUp];
    return scaledImg;
}

- (UIImage *) getScaledImageView:(UIImage *)img insideButton:(UIImageView *)btn {
    
    // Check which dimension (width or height) to pay respect to and
    // calculate the scale factor
    CGFloat imgRatio = img.size.width / img.size.height;
    CGFloat btnRatio = btn.frame.size.width / btn.frame.size.height;
    CGFloat scaleFactor = (imgRatio > btnRatio)
    ? img.size.width / btn.frame.size.width
    : img.size.height / btn.frame.size.height;
    
    NSLog(@"scaleFactor:%f",scaleFactor);
    if (scaleFactor > 0) return img;
    // Create image using scale factor
    UIImage *scaledImg = [UIImage imageWithCGImage:[img CGImage]
                                             scale:scaleFactor
                                       orientation:UIImageOrientationUp];
    return scaledImg;
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}


@end
