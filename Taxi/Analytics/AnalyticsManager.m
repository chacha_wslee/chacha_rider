//
//  AnalyticsManager.m
//  Taxi
//
//  Created by wslee-chacha on 2018. 7. 11..
//  Copyright © 2018년 chachacreation.co.kr. All rights reserved.
//

#import "AnalyticsManager.h"

@implementation AnalyticsManager

#pragma mark - AdBrix Events

+ (void)agreeTermsWithAd
{
    [AdBrix retention:kAgreeTermsWithFR];
}

+ (void)joinWithAd:(NSString *)funnels funnelDetail:(NSString * _Nullable)funnelDetail
{
    if (funnelDetail && funnelDetail.length > 0)
    {
        funnels = [NSString stringWithFormat:@"%@-%@", funnels, funnelDetail];
    }
    
    [AdBrix retention:kJoinWithFR param:funnels];
}

+ (void)loginWithAd:(NSString *)type
{
    [AdBrix retention:kLoginWithFR param:type];
}

+ (void)changePasswordWithAd
{
    [AdBrix retention:kChangePasswordWithFR];
}

+ (void)requestPasswordWithAd
{
    [AdBrix retention:kRequestPasswordWithFR];
}

+ (void)resetPasswordWithAd
{
    [AdBrix retention:kResetPasswordWithFR];
}

+ (void)closeNoticeWithAd:(NSInteger)pageNum
{
    [AdBrix retention:kCloseNoticeWithFR param:[NSString stringWithFormat:@"%ld", pageNum]];
}

+ (void)closeAllDayWithAd:(NSInteger)pageNum
{
    [AdBrix retention:kCloseAllDayWithFR param:[NSString stringWithFormat:@"%ld", pageNum]];
}

+ (void)deleteHistoryWithAd
{
    [AdBrix retention:kDeleteHistoryWithFR];
}

+ (void)deleteAllHistoryWithAd
{
    [AdBrix retention:kDeleteAllHistoryWithFR];
}

+ (void)callWithAd:(BOOL)useCoupon
{
    [AdBrix retention:kCallWithFR param:[NSString stringWithFormat:@"%d", useCoupon]];
}

+ (void)callSuccessWithAd:(NSString *)carType
{
    [AdBrix retention:kCallSuccessWithFR param:carType];
}

+ (void)callCancelWithAd:(NSString *)carType
{
    [AdBrix retention:kCallCancelWithFR param:carType];
}

+ (void)callCancelByDriverWithAd:(NSString *)carType
{
    [AdBrix retention:kCallCancelByDriverWithFR param:carType];
}

+ (void)callNotFoundWithAd
{
    [AdBrix retention:kCallNotFoundWithFR];
}

+ (void)changeCardWithAd
{
    [AdBrix retention:kChangeCardWithFR];
}

+ (void)selectCouponWithAd
{
    [AdBrix retention:kSelectCouponWithFR];
}

+ (void)callToDriverWithAd
{
    [AdBrix retention:kCallToDriverWithFR];
}

+ (void)messageToDriverWithAd
{
    [AdBrix retention:kMessageToDriverWithFR];
}

+ (void)chargeWithAd:(NSString *)transactionId currency:(NSString *)currency value:(double)value couponName:(NSString *)coupon carType:(NSString *)type
{
    AdBrixCommerceProductAttrModel *attrModel = [AdBrixCommerceProductAttrModel create:@{kPurchaseCouponWithFR : coupon}];
    AdBrixCommerceProductModel *productModel = [AdBrix createCommerceProductModel:@"chacha"
                                                                      productName:type
                                                                            price:value
                                                                         discount:0
                                                                         quantity:1
                                                                   currencyString:kPurchaseCurrencyWithFR
                                                                         category:[AdBrixCommerceProductCategoryModel create:@""]
                                                                    extraAttrsMap:attrModel];
    
    [AdBrix purchase:transactionId product:productModel paymentMethod:[AdBrix paymentMethod:AdBrixPaymentMobilePayment]];
}

+ (void)chargeFailWithAd:(NSString *)type
{
    [AdBrix retention:kChargeFailWithFR param:type];
}

+ (void)evaluationWithAd:(NSInteger)rate
{
    [AdBrix retention:kEvaluationWithFR param:[NSString stringWithFormat:@"%ld", rate]];
}

+ (void)changePhotoWithAd
{
    [AdBrix retention:kChangePhotoWithFR];
}

+ (void)deleteCardWithAd
{
    [AdBrix retention:kDeleteCardWithFR];
}

+ (void)addCardWithAd
{
    [AdBrix retention:kAddCardWithFR];
}

+ (void)addFavoriteWithAd
{
    [AdBrix retention:kAddFavoriteWithFR];
}

+ (void)openNoticeWithAd
{
    [AdBrix retention:kOpenNoticeWithFR];
}

+ (void)addCouponWithAd:(NSString *)type
{
    [AdBrix retention:kAddCouponWithFR param:type];
}

+ (void)inviteFriendWithAd
{
    [AdBrix retention:kInviteFriendWithFR];
}

+ (void)rateAppWithAd
{
    [AdBrix retention:kRateAppWithFR];
}

+ (void)likeFacebookWithAd
{
    [AdBrix retention:kLikeFacebookWithFR];
}

+ (void)logoutWithAd
{
    [AdBrix retention:kLogOutWithFR];
}

+ (void)firstRideWithAd
{
    [AdBrix retention:kFirstRideWithFR];
}

+ (void)firstAddCardWithAd
{
    [AdBrix retention:kFirstAddCardWithFR];
}

#pragma mark - AdBrix Property

/**
 AdBrix Property : 가입단계

 @param value 가입 단계
 */
+ (void)setJoinStepWithAd:(NSString *)value
{
    if (value != nil &&
        ([value isEqualToString:kValueJoinStepAgreeTermsWithFR] || [value isEqualToString:kValueJoinStepAddCardWithFR] || [value isEqualToString:kValueJoinStepAuthWithFR] || [value isEqualToString:kValueJoinStepRodeWithFR]))
    {
        [AdBrix firstTimeExperience:value];
    }
    else
    {
        NSLog(@"[%s] invalid value : [%@]", __FUNCTION__, value);
    }
}

/**
 AdBrix Property : 유입 경로

 @param value 유입 경로
 */
+ (void)setJoinFunnelWithAd:(NSString *)value
{
    if (value != nil &&
        ([value isEqualToString:kValueJoinFunnelFriendWithFR] || [value isEqualToString:kValueJoinFunnelGoogleADWithFR] || [value isEqualToString:kValueJoinFunnelFacebookADWithFR]))
    {
        [AdBrix setCustomCohort:AdBrixCustomCohort_1 filterName:@"friend"];
    }
    else
    {
        NSLog(@"[%s] invalid value : [%@]", __FUNCTION__, value);
    }
}

/**
 AdBrix Property : 나이

 @param value 나이
 */
+ (void)setAgeWithAd:(int)value
{
    if (value > 0)
    {
        [IgaworksCore setAge:value];
    }
    else
    {
        NSLog(@"[%s] invalid value : [%d]", __FUNCTION__, value);
    }
}

/**
 AdBrix Property : 성별

 @param value male / female
 */
+ (void)setSexWithAd:(NSString *)value
{
    if (value != nil &&
        ([value isEqualToString:kValueSexMaleWithFR] || [value isEqualToString:kValueSexFemaleWithFR]))
    {
        [IgaworksCore setGender:[value isEqualToString:kValueSexMaleWithFR] ? IgaworksCoreGenderMale : IgaworksCoreGenderFemale];
    }
    else
    {
        NSLog(@"[%s] invalid value : [%@]", __FUNCTION__, value);
    }
}

#pragma mark - Firebase Events

/**
 Firebase Events : 약관동의
 */
+ (void)agreeTermsWithFR
{
    [FIRAnalytics logEventWithName:kAgreeTermsWithFR parameters:nil];
}

/**
 Firebase Events : 가입
 다이나믹링크 '/download/{유입경로이름}/{세부이름}' 로 넘어올때 '{유입경로이름}-{세부이름}' 으로 전달 한다.
 @param type naver, facebook, kakao, email
 @param funnels 유입경로이름
 @param funnelDetail 세부이름
 */
+ (void)joinWithFR:(NSString *)type funnels:(NSString *)funnels funnelDetail:(NSString * _Nullable)funnelDetail
{
    if (funnelDetail && funnelDetail.length > 0)
    {
        funnels = [NSString stringWithFormat:@"%@-%@", funnels, funnelDetail];
    }
    
    [FIRAnalytics logEventWithName:kJoinWithFR parameters:@{kJoinTypeWithFR : type, kJoinFunnelWithFR : funnels}];
}

/**
 Firebase Events : 로그인

 @param type naver, facebook, kakao, email
 */
+ (void)loginWithFR:(NSString *)type
{
    [FIRAnalytics logEventWithName:kLoginWithFR parameters:@{kLoginTypeWithFR : type}];
}

/**
 Firebase Events : 비밀번호 변경
 */
+ (void)changePasswordWithFR
{
    [FIRAnalytics logEventWithName:kChangePasswordWithFR parameters:nil];
}

/**
 Firebase Events : 비밀번호 재설정
 */
+ (void)requestPasswordWithFR
{
    [FIRAnalytics logEventWithName:kRequestPasswordWithFR parameters:nil];
}

/**
 Firebase Events : 신규 비밀번호
 */
+ (void)resetPasswordWithFR
{
    [FIRAnalytics logEventWithName:kResetPasswordWithFR parameters:nil];
}

/**
 Firebase Events : 공지사항 닫기

 @param pageNum 닫은 공지사항 페이지 (1~3)
 */
+ (void)closeNoticeWithFR:(NSInteger)pageNum
{
    [FIRAnalytics logEventWithName:kCloseNoticeWithFR parameters:@{kPageNumWithFR : [NSNumber numberWithInteger:pageNum + 1]}];
}

/**
 Firebase Events : 오늘 하루 열지 않기

 @param pageNum 닫은 공지사항 페이지 (1~3)
 */
+ (void)closeAllDayWithFR:(NSInteger)pageNum
{
    [FIRAnalytics logEventWithName:kCloseAllDayWithFR parameters:@{kPageNumWithFR : [NSNumber numberWithInteger:pageNum + 1]}];
}

/**
 Firebase Events : 여정 삭제
 */
+ (void)deleteHistoryWithFR
{
    [FIRAnalytics logEventWithName:kDeleteHistoryWithFR parameters:nil];
}

/**
 Firebase Events : 전체 여정 삭제
 */
+ (void)deleteAllHistoryWithFR
{
    [FIRAnalytics logEventWithName:kDeleteAllHistoryWithFR parameters:nil];
}

/**
 Firebase Events : 호출하기

 @param useCoupon 쿠폰 사용 유무
 @param useFavorite 즐겨찾기 사용 진입 여부
 */
+ (void)callWithFR:(BOOL)useCoupon useFavorite:(BOOL)useFavorite
{
    [FIRAnalytics logEventWithName:kCallWithFR parameters:@{kUseCouponWithFR:[NSNumber numberWithBool:useCoupon], kUseFavoriteWithFR: [NSNumber numberWithBool:useFavorite]}];
}

/**
 Firebase Events : 호출 성공

 @param waitTime 기다린 시간
 @param type 차량 종류 (E | X | XL)
 */
+ (void)callSuccessWithFR:(long)waitTime carType:(NSString *)type
{
    [FIRAnalytics logEventWithName:kCallSuccessWithFR parameters:@{kCallWaitTimeWithFR : [NSNumber numberWithLong:waitTime],
                                                                   kCarType : type
                                                                   }];
}

/**
 Firebase Events : 호출 취소

 @param waitTime 대기한 시간
 @param remainTime 남은 시간
 @param type 차량 종류 (E | X | XL)
 */
+ (void)callCancelWithFR:(long)waitTime remainingTime:(long)remainTime carType:(NSString *)type
{
    [FIRAnalytics logEventWithName:kCallCancelWithFR parameters:@{kCallWaitTimeWithFR : [NSNumber numberWithLong:waitTime],
                                                                  kCallRemainingTimeWithFR : [NSNumber numberWithLong:remainTime]
                                                                  }];
}

/**
 Firebase Events : 드라이버의 호출 취소

 @param waitTime 대기한 시간
 @param remainTime 남은 시간
 @param type 차량 종류 (E | X | XL)
 */
+ (void)callCancelByDriverWithFR:(long)waitTime remainingTime:(long)remainTime carType:(NSString *)type
{
    [FIRAnalytics logEventWithName:kCallCancelByDriverWithFR parameters:@{kCallWaitTimeWithFR : [NSNumber numberWithLong:waitTime],
                                                                          kCallRemainingTimeWithFR : [NSNumber numberWithLong:remainTime]
                                                                          }];
}

/**
 Firebase Events : 차량을 찾지 못함
 */
+ (void)callNotFoundWithFR
{
    [FIRAnalytics logEventWithName:kCallNotFoundWithFR parameters:nil];
}

/**
 Firebase Events : 결제 카드 변경
 */
+ (void)changeCardWithFR
{
    [FIRAnalytics logEventWithName:kChangeCardWithFR parameters:nil];
}

/**
 Firebase Events : 쿠폰 선택
 */
+ (void)selectCouponWithFR
{
    [FIRAnalytics logEventWithName:kSelectCouponWithFR parameters:nil];
}

/**
 Firebase Events : 드라이버에게 전화
 */
+ (void)callToDriverWithFR
{
    [FIRAnalytics logEventWithName:kCallToDriverWithFR parameters:nil];
}

/**
 Firebase Events : 드라이버에게 메세지
 */
+ (void)messageToDriverWithFR
{
    [FIRAnalytics logEventWithName:kMessageToDriverWithFR parameters:nil];
}

/**
 Firebase Events : 운행종료(결제)

 @param transactionId 주문번호
 @param currency KRW
 @param value 요금 전체
 @param coupon 쿠폰 이름
 @param type 차량 종류 (E | X | XL)
 */
+ (void)chargeWithFR:(NSString *)transactionId currency:(NSString *)currency value:(double)value couponName:(NSString *)coupon carType:(NSString *)type
{
    [FIRAnalytics logEventWithName:kFIREventEcommercePurchase parameters:@{kTransactionIdWithFR : transactionId,
                                                                           kPurchaseCurrencyWithFR : currency,
                                                                           kPurchaseValueWithFR : [NSNumber numberWithDouble:value],
                                                                           kPurchaseCouponWithFR : coupon,
                                                                           kCarType : type
                                                                           }];
}

/**
 Firebase Events : 결제 실패 / 운행 건당 최대 1회

 @param type #wslee#
 */
+ (void)chargeFailWithFR:(NSString *)type
{
    [FIRAnalytics logEventWithName:kChargeFailWithFR parameters:@{kChargeFailTypeWithFR : type}];
}

/**
 Firebase Events : 서비스 만족도

 @param rate 별점
 */
+ (void)evaluationWithFR:(NSInteger)rate
{
    [FIRAnalytics logEventWithName:kEvaluationWithFR parameters:@{kEvaluationRateWithFR : [NSNumber numberWithInteger:rate]}];
}

/**
 Firebase Events : 사진 변경
 */
+ (void)changePhotoWithFR
{
    [FIRAnalytics logEventWithName:kChangePhotoWithFR parameters:nil];
}

/**
 Firebase Events : 카드 삭제
 */
+ (void)deleteCardWithFR
{
    [FIRAnalytics logEventWithName:kDeleteCardWithFR parameters:nil];
}

/**
 Firebase Events : 카드 등록
 */
+ (void)addCardWithFR
{
    [FIRAnalytics logEventWithName:kAddCardWithFR parameters:nil];
}

/**
 Firebase Events : 즐겨찾기 추가
 */
+ (void)addFavoriteWithFR
{
    [FIRAnalytics logEventWithName:kAddFavoriteWithFR parameters:nil];
}

/**
 Firebase Events : 공지사항 보기
 */
+ (void)openNoticeWithFR
{
    [FIRAnalytics logEventWithName:kOpenNoticeWithFR parameters:nil];
}

/**
 Firebase Events : 쿠폰 추가

 @param type 쿠폰코드 값이 u로 시작하면 친구추천 / 나머지는 쿠폰 코드의 첫 두글자
 */
+ (void)addCouponWithFR:(NSString *)type
{
    [FIRAnalytics logEventWithName:kAddCouponWithFR parameters:@{kAddCouponTypeWithFR : type}];
}

/**
 Firebase Events : 친구 초대하기
 */
+ (void)inviteFriendWithFR
{
    [FIRAnalytics logEventWithName:kInviteFriendWithFR parameters:nil];
}

/**
 Firebase Events : 앱스토어 평가
 */
+ (void)rateAppWithFR
{
    [FIRAnalytics logEventWithName:kRateAppWithFR parameters:nil];
}

/**
 Firebase Events : 페이스북에 좋아요
 */
+ (void)likeFacebookWithFR
{
    [FIRAnalytics logEventWithName:kLikeFacebookWithFR parameters:nil];
}

/**
 Firebase Events : 로그아웃
 */
+ (void)logoutWithFR
{
    [FIRAnalytics logEventWithName:kLogOutWithFR parameters:nil];
}

/**
 #wslee#
 */
+ (void)firstRideWithFR
{
    [FIRAnalytics logEventWithName:kFirstRideWithFR parameters:nil];
}

/**
 #wslee#
 */
+ (void)firstAddCardWithFR
{
    [FIRAnalytics logEventWithName:kFirstAddCardWithFR parameters:nil];
}

#pragma mark - Firebase UserProperty

/**
 Firebase Property : 가입단계

 @param value 가입 단계
 */
+ (void)setJoinStepWithFR:(NSString *)value
{
    if (value != nil &&
        ([value isEqualToString:kValueJoinStepAgreeTermsWithFR] || [value isEqualToString:kValueJoinStepAddCardWithFR] ||
        [value isEqualToString:kValueJoinStepAuthWithFR] || [value isEqualToString:kValueJoinStepRodeWithFR]))
    {
        [FIRAnalytics setUserPropertyString:value forName:kPropertyJoinStepWithFR];
    }
    else
    {
        NSLog(@"[%s] invalid value : [%@]", __FUNCTION__, value);
    }
}

/**
 Firebase Property : 유입경로

 @param value 유입 경로 종류
 */
+ (void)setJoinFunnelWithFR:(NSString *)value
{
    if (value != nil &&
        ([value isEqualToString:kValueJoinFunnelFriendWithFR] || [value isEqualToString:kValueJoinFunnelGoogleADWithFR] ||
         [value isEqualToString:kValueJoinFunnelFacebookADWithFR]))
    {
        [FIRAnalytics setUserPropertyString:value forName:kPropertyJoinFunnelWithFR];
    }
    else
    {
        NSLog(@"[%s] invalid value : [%@]", __FUNCTION__, value);
    }
}

/**
 Firebase Property : 나이

 @param value 90s_late, 90s_early... 2002년생은 102s_early
 */
+ (void)setAgeWithFR:(NSString *)value
{
    //#wslee# 출생연도를 Integer 형태로 받아서 처리하는 것으로 고려하자
    if (value != nil &&
        ([value isEqualToString:kValueJoinFunnelFriendWithFR] || [value isEqualToString:kValueJoinFunnelGoogleADWithFR] ||
         [value isEqualToString:kValueJoinFunnelFacebookADWithFR]))
    {
        [FIRAnalytics setUserPropertyString:value forName:kPropertyAgeWithFR];
    }
    else
    {
        NSLog(@"[%s] invalid value : [%@]", __FUNCTION__, value);
    }
}

/**
 Firebase Property : 성별

 @param value male / female
 */
+ (void)setSexWithFR:(NSString *)value
{
    if (value != nil &&
        ([value isEqualToString:kValueSexMaleWithFR] || [value isEqualToString:kValueSexFemaleWithFR]))
    {
        [FIRAnalytics setUserPropertyString:value forName:kPropertySexWithFR];
    }
    else
    {
        NSLog(@"[%s] invalid value : [%@]", __FUNCTION__, value);
    }
}

@end
