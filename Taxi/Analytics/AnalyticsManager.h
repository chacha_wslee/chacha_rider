//
//  AnalyticsManager.h
//  Taxi
//
//  Created by wslee-chacha on 2018. 7. 11..
//  Copyright © 2018년 chachacreation.co.kr. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <FirebaseAnalytics/FIRAnalytics.h>
#import <IgaworksCore/IgaworksCore.h>
#import <AdBrix/AdBrix.h>

//이벤트 key
#define kAgreeTermsWithFR       @"agree_terms"
#define kJoinWithFR             @"join"
#define kLoginWithFR            @"login"
#define kChangePasswordWithFR   @"change_password"
#define kRequestPasswordWithFR  @"request_password"
#define kResetPasswordWithFR    @"reset_password"
#define kCloseNoticeWithFR      @"close_notice"
#define kCloseAllDayWithFR      @"close_oneday_notice"
#define kDeleteHistoryWithFR    @"delete_history"
#define kDeleteAllHistoryWithFR @"delete_all_history"
#define kCallWithFR             @"call"
#define kCallSuccessWithFR      @"call_success"
#define kCallCancelWithFR       @"call_cancel"
#define kCallCancelByDriverWithFR    @"call_canceled"
#define kCallNotFoundWithFR     @"call_not_found"
#define kChangeCardWithFR       @"change_card"
#define kSelectCouponWithFR     @"select_coupon"
#define kCallToDriverWithFR     @"phone"
#define kMessageToDriverWithFR  @"message"
#define kChargeFailWithFR       @"ecommerce_purchase_error"
#define kEvaluationWithFR       @"evaluation"
#define kChangePhotoWithFR      @"change_photo"
#define kDeleteCardWithFR       @"delete_card"
#define kAddCardWithFR          @"add_card"
#define kAddFavoriteWithFR      @"add_favorite"
#define kOpenNoticeWithFR       @"select_notice"
#define kAddCouponWithFR        @"add_coupon"
#define kInviteFriendWithFR     @"invite_friend"
#define kRateAppWithFR          @"rate_app"
#define kLikeFacebookWithFR     @"like_facebook"
#define kLogOutWithFR           @"logout"
#define kFirstRideWithFR        @"first_ride"
#define kFirstAddCardWithFR     @"first_add_card"


//이벤트 parameter key

#define kJoinTypeWithFR         @"type"
#define kJoinFunnelWithFR       @"funnels"
#define kLoginTypeWithFR        @"type"
#define kPageNumWithFR          @"page_num"
#define kUseCouponWithFR        @"use_coupon"
#define kUseFavoriteWithFR      @"use_favorite"
#define kCarType                @"car_type"
#define kCallWaitTimeWithFR   @"waiting_time"
#define kCallRemainingTimeWithFR  @"remaining_time"
#define kTransactionIdWithFR    @"transaction_id"
#define kPurchaseCurrencyWithFR @"currency"
#define kPurchaseValueWithFR    @"value"
#define kPurchaseCouponWithFR   @"coupon"
#define kChargeFailTypeWithFR   @"type"
#define kEvaluationRateWithFR   @"rate"
#define kNoticeIdWithFR         @"noticeId"
#define kAddCouponTypeWithFR    @"type"

//속성 key
#define kPropertyJoinStepWithFR         @"join_step"
#define kPropertyJoinFunnelWithFR       @"join_funnels"
#define kPropertyAgeWithFR              @"age"
#define kPropertySexWithFR              @"sex"

//가입단계 value
#define kValueJoinStepAgreeTermsWithFR    @"agreed_terms"
#define kValueJoinStepAddCardWithFR    @"added_card"
#define kValueJoinStepAuthWithFR    @"authorizationed"
#define kValueJoinStepRodeWithFR    @"rode"

//유입경로 value
#define kValueJoinFunnelFriendWithFR  @"friend"
#define kValueJoinFunnelGoogleADWithFR  @"google_ad"
#define kValueJoinFunnelFacebookADWithFR  @"facebook_ad"

//#wslee#
//나이 value

//성별 value
#define kValueSexMaleWithFR     @"male"
#define kValueSexFemaleWithFR   @"female"


@interface AnalyticsManager : NSObject

#pragma mark - AdBrix Events
+ (void)agreeTermsWithAd;
+ (void)joinWithAd:(NSString *)funnels funnelDetail:(NSString * _Nullable)funnelDetail;
+ (void)loginWithAd:(NSString *)type;
+ (void)changePasswordWithAd;
+ (void)requestPasswordWithAd;
+ (void)resetPasswordWithAd;
+ (void)closeNoticeWithAd:(NSInteger)pageNum;
+ (void)closeAllDayWithAd:(NSInteger)pageNum;
+ (void)deleteHistoryWithAd;
+ (void)deleteAllHistoryWithAd;
+ (void)callWithAd:(BOOL)useCoupon;
+ (void)callSuccessWithAd:(NSString *)carType;
+ (void)callCancelWithAd:(NSString *)carType;
+ (void)callCancelByDriverWithAd:(NSString *)carType;
+ (void)callNotFoundWithAd;
+ (void)changeCardWithAd;
+ (void)selectCouponWithAd;
+ (void)callToDriverWithAd;
+ (void)messageToDriverWithAd;
+ (void)chargeWithAd:(NSString *)transactionId currency:(NSString *)currency value:(double)value couponName:(NSString *)coupon carType:(NSString *)type;
+ (void)chargeFailWithAd:(NSString *)type;
+ (void)evaluationWithAd:(NSInteger)rate;
+ (void)changePhotoWithAd;
+ (void)deleteCardWithAd;
+ (void)addCardWithAd;
+ (void)addFavoriteWithAd;
+ (void)openNoticeWithAd;
+ (void)addCouponWithAd:(NSString *)type;
+ (void)inviteFriendWithAd;
+ (void)rateAppWithAd;
+ (void)likeFacebookWithAd;
+ (void)logoutWithAd;
+ (void)firstRideWithAd;
+ (void)firstAddCardWithAd;

#pragma mark - AdBrix Property
+ (void)setJoinStepWithAd:(NSString *)value;
+ (void)setJoinFunnelWithAd:(NSString *)value;
+ (void)setAgeWithAd:(int)value;
+ (void)setSexWithAd:(NSString *)value;

#pragma mark - Firebase Events
+ (void)agreeTermsWithFR;
+ (void)joinWithFR:(NSString *)type funnels:(NSString *)funnels funnelDetail:(NSString * _Nullable)funnelDetail;
+ (void)loginWithFR:(NSString *)type;
+ (void)changePasswordWithFR;
+ (void)requestPasswordWithFR;
+ (void)resetPasswordWithFR;
+ (void)closeNoticeWithFR:(NSInteger)pageNum;
+ (void)closeAllDayWithFR:(NSInteger)pageNum;
+ (void)deleteHistoryWithFR;
+ (void)deleteAllHistoryWithFR;
+ (void)callWithFR:(BOOL)useCoupon useFavorite:(BOOL)useFavorite;
+ (void)callSuccessWithFR:(long)waitTime carType:(NSString *)type;
+ (void)callCancelWithFR:(long)waitTime remainingTime:(long)remainTime carType:(NSString *)type;
+ (void)callCancelByDriverWithFR:(long)waitTime remainingTime:(long)remainTime carType:(NSString *)type;
+ (void)callNotFoundWithFR;
+ (void)changeCardWithFR;
+ (void)selectCouponWithFR;
+ (void)callToDriverWithFR;
+ (void)messageToDriverWithFR;
+ (void)chargeWithFR:(NSString *)transactionId currency:(NSString *)currency value:(double)value couponName:(NSString *)coupon carType:(NSString *)type;
+ (void)chargeFailWithFR:(NSString *)type;
+ (void)evaluationWithFR:(NSInteger)rate;
+ (void)changePhotoWithFR;
+ (void)deleteCardWithFR;
+ (void)addCardWithFR;
+ (void)addFavoriteWithFR;
+ (void)openNoticeWithFR;
+ (void)addCouponWithFR:(NSString *)type;
+ (void)inviteFriendWithFR;
+ (void)rateAppWithFR;
+ (void)likeFacebookWithFR;
+ (void)logoutWithFR;
+ (void)firstRideWithFR;
+ (void)firstAddCardWithFR;

#pragma mark - Firebase UserProperty
+ (void)setJoinStepWithFR:(NSString *)value;
+ (void)setJoinFunnelWithFR:(NSString *)value;
+ (void)setAgeWithFR:(NSString *)value;
+ (void)setSexWithFR:(NSString *)value;

@end
