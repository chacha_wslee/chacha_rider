//
//  TXLauncherVC.m

//

#import "TXLauncherVC.h"
#import "TXMainVC.h"
#import "TXUserModel.h"
#import "TXSharedObj.h"

//#import "TXSignUpVC.h"
#import "TXSignInVC.h"

//#import "RedisSingleton.h"

#import "TXMapVC.h"
#ifndef _DRIVER_MODE
    #import "TXGuideVC.h"
#endif
//#import "TXSubscriptionVC.h"
//#import "TXAskCardNumberVC.h"
#import "TXAppDelegate.h"
#import "LGAlertView.h"
#import "UIImageView+AnimationCompletion.h"

@interface TXLauncherVC () {
    BOOL isLoading;
    CLLocationManager *locationMgr;
    NSArray *_msgList;
    
    BOOL isFecthLocation;
    id observer1;
    
    LGAlertView* alertView;
    
    NSMutableArray *animationImages1;
    NSMutableArray *animationImages2;
    NSInteger playCount;
    BOOL isPlay;
    BOOL isLoadingFlag;
    BOOL isGuideShow;
}

@property (retain, nonatomic) IBOutlet UIImageView    *splash_background;
@property (retain, nonatomic) IBOutlet UIImageView    *splash_text;

@property (retain, nonatomic) IBOutlet UIImageView    *symbolImage;

-(IBAction)signIn:(id)sender;
-(IBAction)signUp:(id)sender;

@end

@implementation TXLauncherVC


-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    
    if(self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        self->isLoading = TRUE;
        self->isFecthLocation = NO;
        self->alertView = nil;
        self->isGuideShow = NO;
    }
    
    return self;
}

-(id)initWithFlag:(BOOL)flag {
    
    self = [self init];
    if (self) {
        self->isLoading = flag;
        self->isFecthLocation = NO;
        self->isGuideShow = NO;
    }
    return self;

}

#pragma mark - UIViewController

-(void)configureStyles {
    [super configureStyles];
    [self configureFieldStyles];
}

-(void) configure {

    [super configure];
    
    isLoadingFlag = NO;
    
    [appDelegate resetTripInfo];
    
    //self.symbolImage.alpha = 0.0f;
    self.btnSignIn.hidden = YES;
    self.btnSignUp.hidden = YES;
    [self.btnSignIn setTitle:LocalizedStr(@"Intro.SignIn.BTN.text") forState:UIControlStateNormal];
    [self.btnSignUp setTitle:LocalizedStr(@"Intro.SignUp.BTN.text") forState:UIControlStateNormal];
    
    // 2. init event
    _msgList = @[
                 @[@"UR110",@""], //
                 @[@"UR114",@""], //
                 @[@"UR200",@""], //
//                 @[@"IC101",@""], //
                 
                 @[@"PP110",@""], //
                 @[@"PP200",@""], //
                 @[@"IC102",@""], //
                 
                 @[@"PD200",@""], //
                 @[@"PD300",@""], //
                 ];
    
#ifdef _DRIVER_MODE
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:VIEWTUTORIAL];
#else
    // guide view
    if (![[NSUserDefaults standardUserDefaults] boolForKey:VIEWTUTORIAL]) {
        TXGuideVC *vc = [[TXGuideVC alloc] initWithNibName:@"TXGuideVC" bundle:nil];
        vc.view.frame = self.view.frame;
        [self presentViewController:vc animated:NO completion:nil];
        self->isGuideShow = YES;
    }
#endif

#ifdef _DRIVER_MODE
    [self.splash_background setImage:[UIImage imageNamed:@"spalsh_background_driver"]];
    self.symbolImage.image = [UIImage imageNamed:@"loading_96"];
#else
    [self.splash_background setImage:[UIImage imageNamed:@"spalsh_background"]];
    self.symbolImage.image = [UIImage imageNamed:@"loading_96"];
#endif

    if (!self->isGuideShow) {
        [self nextStep];
    }
    
//    [super backroundView].hidden = YES;
//    self.view.backgroundColor = UIColorDarkButton;
    //[super navigationType01X:[UIImage imageNamed:@"btn_back"] centerText:@"Launcher"];
}

-(void)nextStep
{
    NSLog(@"debug");
    if (!appDelegate.isNetwork) {
        if (self->alertView) {
            return;
        }
        self->alertView = [[LGAlertView alloc] initWithTitle:@"Error"
                                                            message:LocalizedStr(@"Intro.Network.Errro.text")
                                                              style:LGAlertViewStyleAlert
                                                       buttonTitles:@[LocalizedStr(@"Button.OK")]
                                                  cancelButtonTitle:nil
                                             destructiveButtonTitle:nil
                                                      actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                                                          //exit(0);
                                                      }
                                                      cancelHandler:^(LGAlertView *alertView) {
                                                      }
                                                 destructiveHandler:^(LGAlertView *alertView) {
                                                 }];
        [Utils initAlertButtonColor:self->alertView];
        [self->alertView showAnimated:YES completionHandler:nil];
        
        [super backroundView].hidden = YES;
        self.view.backgroundColor = UIColorDarkButton;
        
        return;
    }
    if (self->isLoading) {
        NSLog(@"debug");
        if (isLoadingFlag) {
            return;
        }
        isLoadingFlag = YES;
        NSLog(@"debug");
        if (![[NSUserDefaults standardUserDefaults] boolForKey:VIEWTUTORIAL]) {
            //[self performSelector:@selector(initControls) withObject:nil afterDelay:0.5];
            [self performSelector:@selector(playAnimation) withObject:nil afterDelay:0.2];
//            [self performSelector:@selector(logoFadeOut) withObject:nil afterDelay:1.0];
//            [self performSelector:@selector(initControls) withObject:nil afterDelay:1.0];
            NSLog(@"debug");
        }
        else {
        //    [self performSelector:@selector(playAnimation) withObject:nil afterDelay:0.2];
            //[self logoFadeOut];
            //[self initControls];
            NSLog(@"debug");
            [self performSelector:@selector(playAnimation) withObject:nil afterDelay:0.2];
        }
    }
    else {
        [self registerEventListeners];
        NSLog(@"debug");
        [self performSelector:@selector(playAnimationLogin) withObject:nil afterDelay:0.2];
/*
        if (self->isFecthLocation) {
            if (!appDelegate.isDriverMode) {
                [self onSignInByRider:nil];
            }
            else {
                [self onSignInByDriver:nil];
            }
        }
        else {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
                
                //[self showBusyIndicator:@"Authenticating ... "];
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 3 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    [self hideBusyIndicator];
                    
                    if (!self->isFecthLocation) {
                        [appDelegate onCommonErrorAlert:LocalizedStr(@"Intro.GPS.Fetch.Error.text")];
                    }
                    else {
                        if (!appDelegate.isDriverMode) {
                            [self onSignInByRider:nil];
                        }
                        else {
                            [self onSignInByDriver:nil];
                        }
                    }
                });
                
            });
        }
*/
    }
//    self->isLoading = YES;
    [super backroundView].hidden = YES;
    self.view.backgroundColor = UIColorDarkButton;
    self->alertView = nil;
}

-(void) registerEventListeners {
    for (NSArray *msg in _msgList) {
        [self->model addEventListener:self forEvent:[msg objectAtIndex:0] eventParams:nil];
    }
}

-(void) removeEventListeners {
    for (NSArray *msg in _msgList) {
        [self->model removeEventListener:self forEvent:[msg objectAtIndex:0]];
    }
    
    [[NSNotificationCenter defaultCenter] removeObserver:observer1];
    observer1 = nil;
}

-(void) logoFadeOut {
    
    if (self.splash_text.alpha) {
        self.splash_text.alpha = 1.0;
    }
    
    
    [UIView animateWithDuration:0.5 animations:^{
#ifdef _DRIVER_MODE
        self.symbolImage.image = [UIImage imageNamed:@"loading_96"];
#else
        self.symbolImage.image = [UIImage imageNamed:@"loading_96"];
#endif
        
        if (self.splash_text.alpha) {
            self.splash_text.alpha = 0.0;
        }
    } completion:^(BOOL finished){
        //self.splash_text.hidden = YES;
    }];
}

-(void) initControls {
    
    if (self.btnSignIn.alpha) {
        return;
    }
    
    [self.btnSignIn setTitleColor:UIColorDefault forState:UIControlStateNormal];
    self.btnSignIn.backgroundColor = [UIColor clearColor];
    self.btnSignIn.layer.cornerRadius = 3;
    self.btnSignIn.layer.borderWidth = 2;
    self.btnSignIn.layer.borderColor = UIColorDefault.CGColor;
    
    [self.btnSignUp setTitleColor:UIColorDarkButton forState:UIControlStateNormal];
    self.btnSignUp.backgroundColor = UIColorDefault;
    self.btnSignUp.layer.cornerRadius = 3;
    
    self.btnSignIn.alpha = 0.0;
    self.btnSignUp.alpha = 0.0;
    self.btnSignIn.hidden = NO;
    self.btnSignUp.hidden = NO;
    
    [UIView animateWithDuration:0.5 animations:^{
        //self.splash_text.alpha = 0.0;
//        self.btnSignIn.alpha = 1.0;
//        self.btnSignUp.alpha = 1.0;
    } completion:^(BOOL finished){
//        self.btnSignIn.hidden = NO;
//        self.btnSignUp.hidden = NO;
        [self performSelector:@selector(signIn:) withObject:nil afterDelay:0.1];
    }];
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.btnSignIn.alpha = 0.0;
    playCount = 0;
    isPlay = NO;
    isLoadingFlag = NO;
    
    _symbolImage.alpha = 0.0f;
    _splash_text.alpha = 0.0f;
    // Load images
    @autoreleasepool {
        // Do any additional setup after loading the view from its nib.
        animationImages1 = [[NSMutableArray alloc] init];
        animationImages2 = [[NSMutableArray alloc] init];
        //NSString *imageNames = [NSString stringWithFormat:@"loading_96"];
        //[animationImages addObject:[UIImage imageNamed:imageNames]];
        for (int i = 1; i <= 48; i++) {
            NSString *imageNames = [NSString stringWithFormat:@"loading_%02d", i];
            [animationImages1 addObject:[UIImage imageNamed:imageNames]];
        }
        for (int i = 49; i <= 96; i++) {
            NSString *imageNames = [NSString stringWithFormat:@"loading_%02d", i];
            [animationImages2 addObject:[UIImage imageNamed:imageNames]];
        }

#ifdef _DRIVER_MODE
        self.symbolImage.animationImages = animationImages1;
        self.symbolImage.animationDuration = 1.0f;
        self.symbolImage.animationRepeatCount = 1;
#else
        if ([[NSUserDefaults standardUserDefaults] boolForKey:VIEWTUTORIAL]) {
            self.symbolImage.animationImages = animationImages1;
            self.symbolImage.animationDuration = 1.0f;
            self.symbolImage.animationRepeatCount = 1;
        }
#endif
        
        // symbol logo
        [UIView animateWithDuration:0.5 animations:^{
            _symbolImage.alpha = 1.0f;
            _splash_text.alpha = 1.0f;
        }];
    }
    
    observer1 = [[NSNotificationCenter defaultCenter] addObserverForName:@"NotificationNetworkReachableReceived"
                                                      object:nil
                                                       queue:[NSOperationQueue mainQueue]
                                                  usingBlock:^(NSNotification *notification) {
                                                      NSLog(@"debug");
                                                      
                                                      if (!self->isGuideShow) {
                                                          [self nextStep];
                                                      }
                                                      //[self nextStep];
                                                      //[self configure];
//                                                      if (appDelegate.isNetwork) {
//                                                          //
//                                                          [self configure];
//                                                      }
//                                                      else {
//                                                      }
                                                      
                                                  }];
    
//#if !TARGET_IPHONE_SIMULATOR
    if ([CLLocationManager locationServicesEnabled]) {
        
        [self startUpdatingLocation];
    }
    else {
        [appDelegate onCommonErrorAlert:LocalizedStr(@"Intro.GPS.Error.text")];
        //        if ([CLLocationManager authorizationStatus] != kCLAuthorizationStatusAuthorizedAlways) {
        //            if ([self->locationMgr respondsToSelector:@selector(requestAlwaysAuthorization)]) {
        //                [self->locationMgr requestAlwaysAuthorization];
        //            }
        //        }
    }
//#endif
    
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
//        
//        [self showBusyIndicator:@"Authenticating ... "];
//        
//        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
//            [self hideBusyIndicator];
//            
//            if (!self->isFecthLocation) {
//                [appDelegate onCommonErrorAlert:LocalizedStr(@"Intro.GPS.Fetch.Error.text")];
//            }
//        });
//        
//    });
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self configureFieldStyles];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [super statusBarStyleLauncher];
    NSLog(@"playCount:%d",(int)playCount);
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
    
    TXSettings  *settings  = [[TXApp instance] getSettings];
    NSString *userUid   = [settings getUserId];
    NSString *userTel = [settings getUserTelno];
    
    if (playCount == 2 && userUid == nil && userTel == nil) {
        NSLog(@"playCount:%d",(int)playCount);
        playCount = 0;
        _symbolImage.alpha = 1.0f;
//        _splash_text.alpha = 1.0;
#ifdef _DRIVER_MODE
        self.symbolImage.image = [UIImage imageNamed:@"loading_96"];
#else
        self.symbolImage.image = [UIImage imageNamed:@"loading_96"];
#endif

        [self performSelector:@selector(logoFadeOut) withObject:nil afterDelay:0.1];
        [self performSelector:@selector(initControls) withObject:nil afterDelay:0.1];
        return;
    }
    NSLog(@"playCount:%d",(int)playCount);
    if (self->isGuideShow) {
        playCount = 2;
    }
    
    if (playCount) {
        _symbolImage.alpha = 0.0f;
        _symbolImage.hidden = NO;
    
        // symbol logo
        [UIView animateWithDuration:2.0 animations:^{
            _symbolImage.alpha = 1.0f;
            
            if (self->isGuideShow) {
                self->isGuideShow = NO;
                playCount = 0;
                [self performSelector:@selector(playAnimation) withObject:nil afterDelay:0.2];
            }
        }];
    }
    else {
        //[self playAnimation];
    }
    //splash_text.hidden = NO;
    //[self playAnimation];
    
    //[self removeFromParentViewController];
//    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
}

-(void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    //_symbolImage.hidden = YES;
    _symbolImage.alpha = 0.0f;
    
    //self.splash_text.hidden = YES;
    //[self removeFromParentViewController];
    
    isPlay = NO;
    isLoadingFlag = NO;
    playCount = 2;
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self removeEventListeners];
}

-(BOOL)prefersStatusBarHidden{
    return YES;
}

-(void) configureFieldStyles {
    
    UIColor *bgColor = [UIColor blackColor];
    
    self.view.backgroundColor = bgColor;
}

#pragma mark - Animation
- (void) setBackgroundPlayAnimation:(NSArray*)animationImages
{
    self.symbolImage.image = [animationImages objectAtIndex:[animationImages count]-1];
}

- (void) playAnimation {
    
    if (isPlay) {
        return;
    }
    if (playCount==2) {
        //
        return;
    }
    NSLog(@"playCount:%d",(int)playCount);
    isPlay = YES;
    
    if (playCount==0) {
        self.symbolImage.animationImages = animationImages1;
        [self performSelector:@selector(setBackgroundPlayAnimation:) withObject:animationImages1 afterDelay:0.5];
    }
    else if (playCount==1) {
        self.symbolImage.animationImages = animationImages2;
        [self performSelector:@selector(setBackgroundPlayAnimation:) withObject:animationImages2 afterDelay:0.3];
    }
    else {
        return;
    }
    
    [self.symbolImage startAnimatingWithCompletionBlock:^(BOOL success){
        //NSLog(@"Completed %@",[NSNumber numberWithBool:success]);
        if (playCount==0) {
            self.symbolImage.animationImages = animationImages2;
            [self performSelector:@selector(playAnimation) withObject:nil afterDelay:1.0];
            isPlay = NO;
            playCount++;
            NSLog(@"playCount:%d",(int)playCount);
        }
        else if (playCount==1) {
            //[self performSelector:@selector(logoFadeOut) withObject:nil afterDelay:0.1];
            [self performSelector:@selector(initControls) withObject:nil afterDelay:0.1];
            isPlay = NO;
            playCount = 2;
            NSLog(@"playCount:%d",(int)playCount);
            return;
        }
        else {
            isPlay = NO;
            return;
        }
    }];
}

- (void) playAnimationLogin {
    
    if (isPlay) {
        return;
    }
    if (playCount==2) {
        return;
    }
    //NSLog(@"playCount:%d",(int)playCount);
    isPlay = YES;
    
    if (playCount==0) {
        [self performSelector:@selector(setBackgroundPlayAnimation:) withObject:animationImages1 afterDelay:0.5];
    }
    else if (playCount==1) {
        [self performSelector:@selector(setBackgroundPlayAnimation:) withObject:animationImages2 afterDelay:0.5];
    }
    else {
        return;
    }
    
    
    [self.symbolImage startAnimatingWithCompletionBlock:^(BOOL success){
        //NSLog(@"Completed %@",[NSNumber numberWithBool:success]);
        if (playCount==0) {
            self.symbolImage.animationImages = animationImages2;
            [self performSelector:@selector(playAnimationLogin) withObject:nil afterDelay:1.0];
            isPlay = NO;
            playCount++;
        }
        else if (playCount==1) {
            
            if (self->isFecthLocation) {
                if (!appDelegate.isDriverMode) {
                    [self onSignInByRider:nil];
                }
                else {
                    [self onSignInByDriver:nil];
                }
            }
            else {
                [self performSelector:@selector(logoFadeOut) withObject:nil afterDelay:0.1];
            }
//            [self performSelector:@selector(initControls) withObject:nil afterDelay:0.1];
            isPlay = NO;
            playCount = 2;
            return;
        }
        else {
            isPlay = NO;
            return;
        }
    }];
}

#pragma mark - IBAction
-(IBAction)signIn:(id)sender {
    TXSignInVC *signInVC = [[TXSignInVC alloc] initWithNibName:@"TXSignInVC" bundle:nil mode:1];
    signInVC.view.frame = [super view].frame;
    [self pushViewController:signInVC];
    
    double delayInSeconds = 3;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0),
                   ^(void){
                       [self removeFromParentViewController];
                   });
}

-(IBAction)signUp:(id)sender {
    TXSignInVC *signInVC = [[TXSignInVC alloc] initWithNibName:@"TXSignInVC" bundle:nil mode:0];
    signInVC.view.frame = [super view].frame;
    [self pushViewController:signInVC];
}

-(void)onSignInByRider:(id)sender {
    
    TXSettings  *settings  = [[TXApp instance] getSettings];
    NSString *userUid   = [settings getUserId];
    NSString *userPwd = [settings getPassword];
    NSString *userTel = [settings getUserTelno];
    
    if ([[Utils nullToString:userUid] isEqualToString:@""]) {
        [self signIn:nil];
        return;
    }
    if ([[Utils nullToString:userPwd] isEqualToString:@""]) {
        [self signIn:nil];
        return;
    }
    if ([[Utils nullToString:userTel] isEqualToString:@""]) {
        [self signIn:nil];
        return;
    }
    if ([Utils checkStateUIInfo:UI_STATE_UR110 oldvalue:@"UR110"] == UI_STATE_STEP_RUN) {
        return;
    }
    [Utils setStateUIInfo:UI_STATE_UR110 value:UI_STATE_STEP_RUN oldvalue:@"UR110"];
    
    TXUser *user = [[TXUser alloc] init];
    user.uid = userUid;
    user.pwd = userPwd;
    user.utelno = userTel;
    
    [self showBusyIndicator:@"Authenticating ... "];
    [self->model UR110:user];
}

-(void)onSignInByDriver:(id)sender {
    
    TXSettings  *settings  = [[TXApp instance] getSettings];
    NSString *userUid    = [settings getUserId];
    NSString *userUtelno = [settings getUserTelno];
    
    if ([[Utils nullToString:userUtelno] isEqualToString:@""]) {
        return;
    }
    if ([Utils checkStateUIInfo:UI_STATE_PP110 oldvalue:@"PP110"] == UI_STATE_STEP_RUN) {
        return;
    }
    [Utils setStateUIInfo:UI_STATE_PP110 value:UI_STATE_STEP_RUN oldvalue:@"PP110"];
    
    TXUser *user = [[TXUser alloc] init];
    user.uid = userUid;
    user.utelno = userUtelno;
    
    //[self showBusyIndicator:@"Authenticating driver ... "];
    [self->model PP110:user];
}

-(BOOL)catchNetworkError:(TXResponseDescriptor *)descriptor {
    if(!descriptor.success && descriptor.rlt == -999999 && ![descriptor.error isEqualToString:TXEvents.HTTPREQUESTCOMPLETED]) {
        return YES;
    }
    return NO;
}

-(void)onUR200 {
    
    //[self showBusyIndicator:@"Requesting LogOut ... "];
    [self->model UR200];
}

-(void)onIC101 {
    
    //[self showBusyIndicator:@"Requesting LogOut ... "];
    [self->model IC101];
}

-(void)onUR114 {
    
    TXSettings* settings  = [[TXApp instance] getSettings];
    NSString *userUid   = [settings getUserId];
    NSString *userTelno = [settings getUserTelno];
    
    TXUser *user = [[TXUser alloc] init];
    user.uid = userUid;
    user.utelno = userTelno;
    
    //[self showBusyIndicator:@"Requesting LogOut ... "];
    [self->model UR114:user];
}


-(void)onPP200 {
    
    //[self showBusyIndicator:@"Requesting LogOut ... "];
    [self->model PP200];
}

// auto login
-(void)onEvent:(TXEvent *)event eventParams:(id)subscriptionParams {
    
    [self hideBusyIndicator];
    TXResponseDescriptor *descriptor = [event getEventProperty:TXEvents.Params.DESCRIPTOR];
    TXRequestObj *request = [event getEventProperty:TXEvents.Params.REQUEST];
    
    // 네트웍 에러 부분 처리 필요 __DEBUG
    if ([self catchNetworkError:descriptor]) {
//        [self alertError:@"Error" message:@"Network Error"];
        //
//        return;
    }
    
    if([event.name isEqualToString:@"UR110"]) {
        [Utils setStateUIInfo:UI_STATE_UR110 value:UI_STATE_STEP_FIN oldvalue:nil];
        
        if(!descriptor.success) {
            //에러주석[self alertError:@"Error" message:descriptor.error];
            
            //[self onIC101]; // 정보 조회
            [self initControls];
            return;
        }
        
        int rlt = [self executeSingIn:event]; // 0:로그인 실패 1:정상로그인 11:subscription 12:card
        NSLog(@"rlt:%d",rlt);
        if (rlt == 11) {
            //
//            TXSubscriptionVC *firstVC = [[TXSubscriptionVC alloc] initWithNibName:@"TXSubscriptionVC" bundle:nil];
//            firstVC.isNextCard = NO;
//            [self pushViewController:firstVC];
//            [self removeFromParentViewController];
            return;
        }
        else if (rlt == 12) {
            //
//            TXSubscriptionVC *firstVC = [[TXSubscriptionVC alloc] initWithNibName:@"TXSubscriptionVC" bundle:nil];
//            [self pushViewController:firstVC];
//            [self removeFromParentViewController];
            return;
        }
        else if (rlt == 21) {
            //
//            TXAskCardNumberVC *firstVC = [[TXAskCardNumberVC alloc] initWithNibName:@"TXAskCardNumberVC" bundle:nil];
//            [self pushViewController:firstVC];
//            [self removeFromParentViewController];
            return;
        }
        else if (rlt == 31) {
            // driver로그인
            //[self onSignInByDriver:nil];
            [self onUR114]; // rider logout -> driver login
            
            return;
        }
        
        if (appDelegate.isDriverMode) {
            // driver로그인
            //[self onSignInByDriver:nil];
            [self onUR114]; // rider logout -> driver login
            return;
        }
        
        
        @try {
            // redis 접속
            NSString     *response = [[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding];
            NSDictionary *responseObj   = getJSONObj(response);
            
            appDelegate.api_config = [responseObj objectForKey:@"user_config"];
            //[RedisSingleton redisLogin:appDelegate.api_config];
            
        }
        @catch (NSException *exception) {
            NSLog(@"Exception: %@, %@", exception.name, exception.reason);
        }
        
        if(descriptor.success) {
            [self removeEventListeners];

            dispatch_async(dispatch_get_main_queue(), ^{
#ifdef _DRIVER_MODE
                TXMapVC *mapVC = [[TXMapVC alloc] initWithNibName:@"TXMapDriverVC" bundle:nil];
#else
                TXMapVC *mapVC = [[TXMapVC alloc] initWithNibName:@"TXMapRiderVC" bundle:nil];
#endif
                //mapVC.isDriveMode = YES;
                [self pushViewController:mapVC];
                NSArray* tempVCA = [self.navigationController viewControllers];
                for(UIViewController *tempVC in tempVCA)
                {
                    if(![tempVC isKindOfClass:[TXMapVC class]]) {
                        [tempVC removeFromParentViewController];
                    }
                }
                [self removeFromParentViewController];
            });
            
        }
        else {
            //NSString *message = [TXCode2MsgTranslator messageForCode:descriptor.rlt];
            //[self alertError:@"Error" message:message];
            //에러주석[self alertError:@"Error" message:descriptor.error];
        }
    }
    else if([event.name isEqualToString:@"UR114"]) {
        
        if(descriptor.success) {
            [self onSignInByDriver:nil];
        }
    }
    else if([event.name isEqualToString:@"PP110"]) {
        [Utils setStateUIInfo:UI_STATE_PP110 value:UI_STATE_STEP_FIN oldvalue:nil];
        
        if(!descriptor.success) {
            //에러주석[self alertError:@"Error" message:descriptor.error];
            
            [self initControls];
            return;
        }
        
        @try {
            // redis 접속
            NSString     *response = [[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding];
            NSDictionary *responseObj   = getJSONObj(response);
            
            appDelegate.api_config = [responseObj objectForKey:@"user_config"];
            //[RedisSingleton redisLogin:appDelegate.api_config];
            
        }
        @catch (NSException *exception) {
            NSLog(@"Exception: %@, %@", exception.name, exception.reason);
        }
        
        if(descriptor.success) {
            
            // 앱 기동 시 최종 상태가 드라이브 모드면
            //
//            [self onUR200]; // 탑승자 정보 조회
//            [self onIC101]; // 탑승자 정보 조회
            //[self onIC102]; // 정보 조회
            NSString     *response = [[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding];
            NSDictionary *responseObj   = getJSONObj(response);
            
            NSDictionary *dic = [responseObj objectForKey:@"provider_device"];
            
//            NSLog(@"***********dic:%@",dic);
            if ([Utils isDictionary:dic]) {
                int state = [dic[@"state"] intValue];
//                NSLog(@"***********state:%d",state);
                if (state > 230) {
                    [self->model PD300];
                    return;
                }
                else if (state >= 220) {
                    [self->model PD200];
                    return;
                }
            }
            
            [self removeEventListeners];
            
            dispatch_async(dispatch_get_main_queue(), ^{
#ifdef _DRIVER_MODE
                TXMapVC *mapVC = [[TXMapVC alloc] initWithNibName:@"TXMapDriverVC" bundle:nil];
#else
                TXMapVC *mapVC = [[TXMapVC alloc] initWithNibName:@"TXMapRiderVC" bundle:nil];
#endif
                mapVC.isDriveMode = YES;
                [self pushViewController:mapVC];
                NSArray* tempVCA = [self.navigationController viewControllers];
                for(UIViewController *tempVC in tempVCA)
                {
                    if(![tempVC isKindOfClass:[TXMapVC class]]) {
                        [tempVC removeFromParentViewController];
                    }
                }
                [self removeFromParentViewController];
            });
        }
        else {
            //NSString *message = [TXCode2MsgTranslator messageForCode:descriptor.rlt];
            //[self alertError:@"Error" message:message];
            //에러주석[self alertError:@"Error" message:descriptor.error];
        }
    }
    else if([event.name isEqualToString:@"PD200"] || [event.name isEqualToString:@"PD300"]) {
        
        if(descriptor.success) {
            [self removeEventListeners];
            
            dispatch_async(dispatch_get_main_queue(), ^{
#ifdef _DRIVER_MODE
                TXMapVC *mapVC = [[TXMapVC alloc] initWithNibName:@"TXMapDriverVC" bundle:nil];
#else
                TXMapVC *mapVC = [[TXMapVC alloc] initWithNibName:@"TXMapRiderVC" bundle:nil];
#endif
                mapVC.isDriveMode = YES;
                [self pushViewController:mapVC];
                NSArray* tempVCA = [self.navigationController viewControllers];
                for(UIViewController *tempVC in tempVCA)
                {
                    if(![tempVC isKindOfClass:[TXMapVC class]]) {
                        [tempVC removeFromParentViewController];
                    }
                }
                [self removeFromParentViewController];
            });
            
        }
        else {
            //NSString *message = [TXCode2MsgTranslator messageForCode:descriptor.rlt];
            //[self alertError:@"Error" message:message];
            //에러주석[self alertError:@"Error" message:descriptor.error];
        }
    }
}

-(int) executeSingIn:(TXEvent *)event {
    
    //TXResponseDescriptor *descriptor = [event getEventProperty:TXEvents.Params.DESCRIPTOR];
    TXRequestObj *request = [event getEventProperty:TXEvents.Params.REQUEST];
    
    NSString     *response = [[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding];
    NSDictionary *result   = getJSONObj(response);
    
    if([event.name isEqualToString:@"UR110"]) {
        
        NSArray *user_payment           = [result valueForKey:@"user_payments"];
        
        // 성공
        
        // 사용가능 카드를 찾는다.
        //if (![[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.CSEQ]) {
            for (NSDictionary* key in user_payment) {
                NSString* cseq   = [key valueForKey:@"cseq"];
                NSString* mode   = [NSString stringWithFormat:@"%@", [key valueForKey:@"mode"]];
                NSString* state  = [key valueForKey:@"state"];
                
                if ([mode isEqualToString:@"1"] && [state isEqualToString:@"1"] ) {
                    [[[TXApp instance] getSettings] setFDKeychain:SettingsConst.CryptoKeys.CSEQ value:cseq];
                    break;
                }
            }
        //}
        
        
        NSDictionary *requestObj = getJSONObj(request.body);
        
        [[[TXApp instance] getSettings] setUserId:[requestObj valueForKey:API_JSON.UID]];

        // 등록된 카드정보가 없으면 카드등록화면으로 전환
        if (![user_payment count]) {
//            return 21;
        }
        
        if (appDelegate.pstate >= 220) {
//            appDelegate.isDriverMode = YES;
            return 31;
        }
    }
    else if([event.name isEqualToString:@"PP110"]) {
        
//        NSDictionary *user              = [result valueForKey:@"user"];
////        NSDictionary *user_device       = [result valueForKey:@"user_device"];
////        NSDictionary *user_payment      = [result valueForKey:@"user_payment"];
//        NSDictionary *provider          = [result valueForKey:@"provider"];
//        NSDictionary *provider_device   = [result valueForKey:@"provider_device"];
//
//        // 성공
//        // 로그인 정보 저장 : ATOKEN, USEQ, UID가 존재하면 자동로그인
//        if (user) {
//            [[[TXApp instance] getSettings] setFDKeychain:SettingsConst.CryptoKeys.USEQ value:[user valueForKey:SettingsConst.CryptoKeys.USEQ]];
//        }
//        if (provider) {
//            [[[TXApp instance] getSettings] setFDKeychain:SettingsConst.CryptoKeys.PSEQ value:[provider valueForKey:SettingsConst.CryptoKeys.PSEQ]];
//        }
//        
////        NSDictionary *requestObj = getJSONObj(request.body);
//        
//        if (appDelegate.pstateold != appDelegate.pstate) {
//            appDelegate.pstateold = appDelegate.pstate;
//        }
//        appDelegate.pstate = [[provider_device objectForKey:@"state"] intValue];
//        appDelegate.isDriverMode = YES;
//        appDelegate.isCanDriverMode = YES;
//        appDelegate.isBecomeMode = NO;
        
        NSLog(@"appDelegate.isBecomeMode:%d",appDelegate.isBecomeMode);
    }
    
    return 1;
}

#pragma mark Current Location

-(void)startUpdatingLocation {
    if (self->locationMgr == nil) {
        self->locationMgr = [[CLLocationManager alloc] init];
        self->locationMgr.delegate = self;
//        self->locationMgr.allowsBackgroundLocationUpdates = YES;
        self->locationMgr.pausesLocationUpdatesAutomatically = NO;
        self->locationMgr.activityType = CLActivityTypeAutomotiveNavigation;
    
        if (appDelegate.isDriverMode) {
            // 항상 위치 정보 사용 요청
//            if ([self->locationMgr respondsToSelector:@selector(requestAlwaysAuthorization)]) {
//                [self->locationMgr requestAlwaysAuthorization];
//            }
            if ([self->locationMgr respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
                [self->locationMgr requestWhenInUseAuthorization];
            }
        }
        else {
            // 사용중일때만 요청
            if ([self->locationMgr respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
                [self->locationMgr requestWhenInUseAuthorization];
            }
        }
        
        self->locationMgr.distanceFilter = kCLDistanceFilterNone;
        self->locationMgr.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
    }
    [self->locationMgr startUpdatingLocation];
}

#pragma mark CLLocationManager delegate

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"didUpdateToLocation: %@", newLocation);
    CLLocation *currentLocation = newLocation;
    
    if (currentLocation != nil) {
        NSLog(@"%.8f", currentLocation.coordinate.longitude);
        NSLog(@"%.8f", currentLocation.coordinate.latitude);
    }
}


-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways) {
        [self->locationMgr startUpdatingLocation];
    }
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    
    CLLocation *location = [manager location];
    
    NSNumber *myDoubleNumberLat = [NSNumber numberWithDouble:location.coordinate.latitude];
    NSNumber *myDoubleNumberLng = [NSNumber numberWithDouble:location.coordinate.longitude];
    
    [Utils updateCurrentPosition:[myDoubleNumberLat stringValue] lng:[myDoubleNumberLng stringValue]];
    
    [self->locationMgr stopUpdatingLocation];
    NSLog(@"didUpdateLocations: %@", location);
    
    appDelegate.location = location;
    
    self->isFecthLocation = YES;
    [self hideBusyIndicator];
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
    NSLog(@"Error while getting core location : %@",[error localizedFailureReason]);
    if ([error code] == kCLErrorDenied) {
        //you had denied
    }
    [manager stopUpdatingLocation];
}



@end
