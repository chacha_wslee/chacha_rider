
#import "GoogleUtils.h"
#import "INTULocationManager.h"

@implementation GoogleUtils

#pragma mark - LocationUtils

+(double)findTotalDistanceOfPath:(GMSMutablePath *)path{
    NSLog(@"%lu",(unsigned long)path.count);
    
    double numberOfCoords = path.count;
    double totalDistance = 0.0;
    if(numberOfCoords > 1){
        for (int i = 0; i < numberOfCoords-1; i++) {
            
            CLLocationCoordinate2D currentCoord = [path coordinateAtIndex:i];
            CLLocationCoordinate2D nextCoord = [path coordinateAtIndex: i+1];
            
            CLLocationDistance newDistance =  GMSGeometryDistance(currentCoord,nextCoord);
            totalDistance = totalDistance + newDistance;
        }
    }
    return totalDistance;
}

+(CLLocationCoordinate2D)findMiddlePointInPath:(GMSMutablePath *)path distance :(double)totalDistance threshold:(int)threshold{
    threshold = threshold;
    double numberOfCoords = path.count;
    double halfDistance = totalDistance/2;
    double midDistance = 0.0;
    if(numberOfCoords > 1){
        for (int i = 0; i < numberOfCoords-1; i++) {
            
            CLLocationCoordinate2D currentCoord = [path coordinateAtIndex:i];
            CLLocationCoordinate2D nextCoord = [path coordinateAtIndex: i+1];
            
            CLLocationDistance newDistance =  GMSGeometryDistance(currentCoord,nextCoord);
            midDistance = midDistance + newDistance;
            
            if (fabs(midDistance - halfDistance) < threshold){ //Found the middle point in route
                return nextCoord;
            }
        }
    }
    return [self findMiddlePointInPath:path distance:totalDistance threshold:2*threshold];
}

+(BOOL)pointCompare:(double)param1 comp:(double)param2 {
    
    NSString *p1 = [NSString stringWithFormat:@"%.6f",param1];
    NSString *p2 = [NSString stringWithFormat:@"%.6f",param2];
    
    //    NSString *p1 = [GoogleUtils formatedLocationOne:param1];
    //    NSString *p2 = [GoogleUtils formatedLocationOne:param2];
    
    //    p1 = [Utils replacingWithPattern2:p1 pattern:@"0*$" withTemplate:@"" error:nil];
    //    p2 = [Utils replacingWithPattern2:p2 pattern:@"0*$" withTemplate:@"" error:nil];
    
    if ([p1 isEqualToString:p2]) {
        return YES;
    }
    return NO;
}

+(BOOL) isEqualLocation:(CLLocationCoordinate2D) coord1 location:(CLLocationCoordinate2D) coord2{
    if ([GoogleUtils pointCompare:coord1.latitude comp:coord2.latitude] && [GoogleUtils pointCompare:coord1.longitude comp:coord2.longitude]) {
        return YES;
    }
    return NO;
}

+(CLLocationCoordinate2D)formatedLocation:(NSString *)coord1 lng:(NSString*)coord2 {
    CLLocation *pointLocation = [[CLLocation alloc] initWithLatitude:[coord1 doubleValue]
                                                           longitude:[coord2 doubleValue]];
    return pointLocation.coordinate;
}

+(NSString *)formatedLocationOne:(NSString *)coord1 {
    CLLocation *pointLocation = [[CLLocation alloc] initWithLatitude:[coord1 doubleValue]
                                                           longitude:[coord1 doubleValue]];
    
    NSString *usercood = [NSString stringWithFormat:@"%f",pointLocation.coordinate.latitude];
    return usercood;
}

/**
 * Gives N/S/E/W or combination, based on degrees provided
 */
+ (NSString *)directionNameFromCompassValue:(CGFloat)compassValue {
    NSString *directionName = [NSString string];
    
    if (compassValue >= 0 && compassValue < 22.5) {
        directionName = @"N";
    } else if (compassValue >= 22.5 && compassValue < 67.5) {
        directionName = @"NE";
    } else if (compassValue >= 67.5 && compassValue < 112.5) {
        directionName = @"E";
    } else if (compassValue >= 112.5 && compassValue < 157.5) {
        directionName = @"SE";
    } else if (compassValue >= 157.5 && compassValue < 202.5) {
        directionName = @"S";
    } else if (compassValue >= 202.5 && compassValue < 247.5) {
        directionName = @"SW";
    } else if (compassValue >= 247.5 && compassValue < 292.5) {
        directionName = @"W";
    } else if (compassValue >= 292.5 && compassValue < 337.5) {
        directionName = @"NW";
    } else if (compassValue > 337.5) {
        directionName = @"N";
    } else if (compassValue < 0) {
        directionName = @"?";
    }
    
    return directionName;
}

+(BOOL)isLocationValid:(CLLocationCoordinate2D)coor
{
    if ((coor.latitude == 0.0f && coor.longitude == 0.0f) ||
        (coor.latitude == 180.0f || coor.longitude == 180.0f) ||
        (coor.latitude == -180.0f || coor.longitude == -180.0f)
        ) {
        return NO;
    }
    return YES;
}

#pragma mark - UTILS
+ (NSString *)replacingWithPattern2:(NSString *)str pattern:(NSString *)pattern withTemplate:(NSString *)withTemplate error:(NSError **)error {
    
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pattern
                                                                           options:NSRegularExpressionCaseInsensitive
                                                                             error:error];
    return [regex stringByReplacingMatchesInString:str
                                           options:0
                                             range:NSMakeRange(0, str.length)
                                      withTemplate:withTemplate];
}

+(NSString*)replaceStr:(NSString*)mstr search:(NSString*)search replace:(NSString*)replace
{
    if ([mstr containsString:@","]) {
        NSArray *strParse = [search componentsSeparatedByString:@","];
        mstr = [strParse componentsJoinedByString:@", "];
    }
    
    mstr=[mstr stringByReplacingOccurrencesOfString:search withString:replace];
    
    mstr = [GoogleUtils trimStringOnly:mstr];
    
    return mstr;
}


+(NSString*)appendUrl:(NSString*)url param:(NSString*)param {
    
    if ([url rangeOfString:@"?"].location == NSNotFound) {
        url = [NSString stringWithFormat:@"%@?%@",url,param];
    }
    else {
        url = [NSString stringWithFormat:@"%@&%@",url,param];
    }
    
    return url;
}

+ (NSString *)serializeParams:(NSDictionary *)params {
    NSMutableArray *pairs = NSMutableArray.array;
    for (NSString *key in params.keyEnumerator) {
        id value = params[key];
        if ([value isKindOfClass:[NSDictionary class]])
            for (NSString *subKey in value)
                [pairs addObject:[NSString stringWithFormat:@"%@[%@]=%@", key, subKey, [self escapeValueForURLParameter:[value objectForKey:subKey]]]];
        
        else if ([value isKindOfClass:[NSArray class]])
            for (NSString *subValue in value)
                [pairs addObject:[NSString stringWithFormat:@"%@[]=%@", key, [self escapeValueForURLParameter:subValue]]];
        
        else
            [pairs addObject:[NSString stringWithFormat:@"%@=%@", key, [self escapeValueForURLParameter:value]]];
        
    }
    return [pairs componentsJoinedByString:@"&"];
}

+ (NSString *)escapeValueForURLParameter:(NSString *)valueToEscape {
    NSCharacterSet *set = [NSCharacterSet URLQueryAllowedCharacterSet];
    return [valueToEscape stringByAddingPercentEncodingWithAllowedCharacters:set];
}

+ (NSDictionary*)getDataOfQueryString:(NSString*)url {
    
    NSString* urlString = @"";
    
    NSArray *strURLParse = [url componentsSeparatedByString:@"?"];
    if ([strURLParse count] == 2) {
        urlString = strURLParse[1];
    }
    else {
        urlString = strURLParse[0];
    }
    
    NSMutableDictionary *queryStringDictionary = [[NSMutableDictionary alloc] init];
    NSArray *urlComponents = [urlString componentsSeparatedByString:@"&"];
    
    for (NSString *keyValuePair in urlComponents)
    {
        NSArray *pairComponents = [keyValuePair componentsSeparatedByString:@"="];
        NSString *key = [[pairComponents firstObject] stringByRemovingPercentEncoding];
        NSString *value = [[pairComponents lastObject] stringByRemovingPercentEncoding];
        
        [queryStringDictionary setObject:value forKey:key];
    }
    
    return queryStringDictionary;
}

+ (NSString*)trimStringOnly:(NSString*)str
{
    NSString *trimmedString = [str stringByTrimmingCharactersInSet:
                               [NSCharacterSet whitespaceCharacterSet]];
    return trimmedString;
}


+ (NSString*)nullToString:(id)string
{
    if ([string isKindOfClass:[NSNumber class]]) {
        string = [NSString stringWithFormat:@"%@",string];
    }
    if (string && string != [NSNull null]) {
        return [GoogleUtils trimStringOnly:string];
    }
    return @"";
}

+ (BOOL)isDictionary:(NSDictionary*)dic
{
    if (!dic || dic == nil || dic == (id)[NSNull null]) return NO;
    if (dic && ([dic isKindOfClass:[NSDictionary class]] || [dic isKindOfClass:[NSMutableDictionary class]]) && [dic count]) return YES;
    return NO;
}


+ (BOOL)isArray:(NSArray*)dic
{
    if (!dic || dic == nil || dic == (id)[NSNull null]) return NO;
    if (dic && ([dic isKindOfClass:[NSArray class]] || [dic isKindOfClass:[NSMutableArray class]]) && [dic count]) return YES;
    return NO;
}

+ (NSString*)nowTimestamp {
    return [NSString stringWithFormat:@"%d",(int)[[NSDate date] timeIntervalSince1970]];
}

+ (BOOL)nullToImage:(UIImage*)image
{
    CGImageRef cgref = [image CGImage];
    CIImage *cim = [image CIImage];
    
    if (cim == nil && cgref == NULL)
    {
        return YES;
    }
    return NO;
}

@end
