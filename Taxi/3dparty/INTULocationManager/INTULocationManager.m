//
//  INTULocationManager.m
//
//  Copyright (c) 2014-2017 Intuit Inc.
//
//  Permission is hereby granted, free of charge, to any person obtaining
//  a copy of this software and associated documentation files (the
//  "Software"), to deal in the Software without restriction, including
//  without limitation the rights to use, copy, modify, merge, publish,
//  distribute, sublicense, and/or sell copies of the Software, and to
//  permit persons to whom the Software is furnished to do so, subject to
//  the following conditions:
//
//  The above copyright notice and this permission notice shall be
//  included in all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
//  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
//  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
//  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
//  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
//  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

#import "INTULocationManager.h"
#import "INTULocationManager+Internal.h"
#import "INTULocationRequest.h"
#import "INTUHeadingRequest.h"

// added
//******************************************************************************************
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "GoogleUtils.h"
#import "MSWeakTimer.h"
#import "TXAppDelegate.h"

#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import <CoreTelephony/CTCarrier.h>
#import <MapKit/MapKit.h>
//#import <GooglePlaces/GooglePlaces.h>

// 정확도
#define DESIRED_HORIZONTAL_ACCURACY 50.0 // 50보다 작은 정확도(정확도는 숫자가 적은게 좋은거)
#define DESIRED_LOCATION_AGE 15.0 // 15초 이내 업데이트 된 정보
#define DESIRED_SPEED 1.0 // 속도가 1m/s 이상이어야 한다

static NSString* const HDR_ACCEPT           = @"Accept";
static NSString* const HDR_CONTENTTYPE      = @"Content-Type";
static NSString* const HDR_CONTENTLEN       = @"Content-Length";

static float const kMapPaddingDefault       = 20.0f;

// end of added
//******************************************************************************************

#ifndef INTU_ENABLE_LOGGING
#   ifdef DEBUG
#       define INTU_ENABLE_LOGGING 1
#   else
#       define INTU_ENABLE_LOGGING 0
#   endif /* DEBUG */
#endif /* INTU_ENABLE_LOGGING */

#if INTU_ENABLE_LOGGING
#   define INTULMLog(...)          NSLog(@"INTULocationManager: %@", [NSString stringWithFormat:__VA_ARGS__]);
#else
#   define INTULMLog(...)
#endif /* INTU_ENABLE_LOGGING */


@interface INTULocationManager () <CLLocationManagerDelegate, INTULocationRequestDelegate>//, TMapTapiDelegate>

/** The instance of CLLocationManager encapsulated by this class. */
@property (nonatomic, strong) CLLocationManager *locationManager;
/** The most recent current location, or nil if the current location is unknown, invalid, or stale. */
@property (nonatomic, strong) CLLocation *currentLocation;
/** The most recent current heading, or nil if the current heading is unknown, invalid, or stale. */
@property (nonatomic, strong) CLHeading *currentHeading;
/** Whether or not the CLLocationManager is currently monitoring significant location changes. */
@property (nonatomic, assign) BOOL isMonitoringSignificantLocationChanges;
/** Whether or not the CLLocationManager is currently sending location updates. */
@property (nonatomic, assign) BOOL isUpdatingLocation;
/** Whether or not the CLLocationManager is currently sending heading updates. */
@property (nonatomic, assign) BOOL isUpdatingHeading;
/** Whether an error occurred during the last location update. */
@property (nonatomic, assign) BOOL updateFailed;

// An array of active location requests in the form:
// @[ INTULocationRequest *locationRequest1, INTULocationRequest *locationRequest2, ... ]
@property (nonatomic, strong) __INTU_GENERICS(NSArray, INTULocationRequest *) *locationRequests;

// An array of active heading requests in the form:
// @[ INTUHeadingRequest *headingRequest1, INTUHeadingRequest *headingRequest2, ... ]
@property (nonatomic, strong) __INTU_GENERICS(NSArray, INTUHeadingRequest *) *headingRequests;

// added
//******************************************************************************************
// timer
@property (strong, nonatomic) MSWeakTimer *timer;
@property (strong, nonatomic) TXAppDelegate *appDelegate;

// end of added
//******************************************************************************************

@end


@implementation INTULocationManager

static id _sharedInstance;

/**
 Returns the current state of location services for this app, based on the system settings and user authorization status.
 */
+ (INTULocationServicesState)locationServicesState
{
    if ([CLLocationManager locationServicesEnabled] == NO) {
        return INTULocationServicesStateDisabled;
    }
    else if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined) {
        return INTULocationServicesStateNotDetermined;
    }
    else if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied) {
        return INTULocationServicesStateDenied;
    }
    else if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusRestricted) {
        return INTULocationServicesStateRestricted;
    }
    
    return INTULocationServicesStateAvailable;
}

/**
 Returns the current state of heading services for this device.
 */
+ (INTUHeadingServicesState)headingServicesState
{
    if ([CLLocationManager headingAvailable]) {
        return INTUHeadingServicesStateAvailable;
    } else {
        return INTUHeadingServicesStateUnavailable;
    }
}

/**
 Returns the singleton instance of this class.
 */
+ (instancetype)sharedInstance
{
    static dispatch_once_t _onceToken;
    dispatch_once(&_onceToken, ^{
        _sharedInstance = [[self alloc] init];
    });
    return _sharedInstance;
}

- (instancetype)init
{
    NSAssert(_sharedInstance == nil, @"Only one instance of INTULocationManager should be created. Use +[INTULocationManager sharedInstance] instead.");
    self = [super init];
    if (self) {
        _locationManager = [[CLLocationManager alloc] init];
        _locationManager.delegate = self;
        
#ifdef __IPHONE_8_4
#if __IPHONE_OS_VERSION_MAX_ALLOWED > __IPHONE_8_4
        /* iOS 9 requires setting allowsBackgroundLocationUpdates to YES in order to receive background location updates.
         We only set it to YES if the location background mode is enabled for this app, as the documentation suggests it is a
         fatal programmer error otherwise. */
        NSArray *backgroundModes = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"UIBackgroundModes"];
        if ([backgroundModes containsObject:@"location"]) {
            if (@available(iOS 9, *)) {
                [_locationManager setAllowsBackgroundLocationUpdates:YES];
            }
        }
        // 배터리 소모와 관련
        _locationManager.pausesLocationUpdatesAutomatically = NO;
        //_locationManager.activityType = CLActivityTypeAutomotiveNavigation;
#endif /* __IPHONE_OS_VERSION_MAX_ALLOWED > __IPHONE_8_4 */
#endif /* __IPHONE_8_4 */
        
        _locationRequests = @[];
        
        // added
        //******************************************************************************************
        
        _appDelegate = (TXAppDelegate *)[[UIApplication sharedApplication] delegate];
        [self resetMgr];
        
        // end of added
        //******************************************************************************************
    }
    return self;
}

#pragma mark - Public location methods

/**
 Asynchronously requests the current location of the device using location services.
 
 @param desiredAccuracy The accuracy level desired (refers to the accuracy and recency of the location).
 @param timeout         The maximum amount of time (in seconds) to wait for a location with the desired accuracy before completing.
 If this value is 0.0, no timeout will be set (will wait indefinitely for success, unless request is force completed or canceled).
 @param block           The block to be executed when the request succeeds, fails, or times out. Three parameters are passed into the block:
 - The current location (the most recent one acquired, regardless of accuracy level), or nil if no valid location was acquired
 - The achieved accuracy for the current location (may be less than the desired accuracy if the request failed)
 - The request status (if it succeeded, or if not, why it failed)
 
 @return The location request ID, which can be used to force early completion or cancel the request while it is in progress.
 */
- (INTULocationRequestID)requestLocationWithDesiredAccuracy:(INTULocationAccuracy)desiredAccuracy
                                                    timeout:(NSTimeInterval)timeout
                                                      block:(INTULocationRequestBlock)block
{
    return [self requestLocationWithDesiredAccuracy:desiredAccuracy
                                            timeout:timeout
                               delayUntilAuthorized:NO
                                              block:block];
}

/**
 Asynchronously requests the current location of the device using location services, optionally waiting until the user grants the app permission
 to access location services before starting the timeout countdown.
 
 @param desiredAccuracy      The accuracy level desired (refers to the accuracy and recency of the location).
 @param timeout              The maximum amount of time (in seconds) to wait for a location with the desired accuracy before completing. If
 this value is 0.0, no timeout will be set (will wait indefinitely for success, unless request is force completed or canceled).
 @param delayUntilAuthorized A flag specifying whether the timeout should only take effect after the user responds to the system prompt requesting
 permission for this app to access location services. If YES, the timeout countdown will not begin until after the
 app receives location services permissions. If NO, the timeout countdown begins immediately when calling this method.
 @param block                The block to be executed when the request succeeds, fails, or times out. Three parameters are passed into the block:
 - The current location (the most recent one acquired, regardless of accuracy level), or nil if no valid location was acquired
 - The achieved accuracy for the current location (may be less than the desired accuracy if the request failed)
 - The request status (if it succeeded, or if not, why it failed)
 
 @return The location request ID, which can be used to force early completion or cancel the request while it is in progress.
 */
- (INTULocationRequestID)requestLocationWithDesiredAccuracy:(INTULocationAccuracy)desiredAccuracy
                                                    timeout:(NSTimeInterval)timeout
                                       delayUntilAuthorized:(BOOL)delayUntilAuthorized
                                                      block:(INTULocationRequestBlock)block
{
    NSAssert([NSThread isMainThread], @"INTULocationManager should only be called from the main thread.");
    
    if (desiredAccuracy == INTULocationAccuracyNone) {
        NSAssert(desiredAccuracy != INTULocationAccuracyNone, @"INTULocationAccuracyNone is not a valid desired accuracy.");
        desiredAccuracy = INTULocationAccuracyCity; // default to the lowest valid desired accuracy
    }
    
    INTULocationRequest *locationRequest = [[INTULocationRequest alloc] initWithType:INTULocationRequestTypeSingle];
    locationRequest.delegate = self;
    locationRequest.desiredAccuracy = desiredAccuracy;
    locationRequest.timeout = timeout;
    locationRequest.block = block;
    locationRequest.distanceFilter = kCLDistanceFilterNone;
    
    BOOL deferTimeout = delayUntilAuthorized && ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined);
    if (!deferTimeout) {
        [locationRequest startTimeoutTimerIfNeeded];
    }
    
    [self addLocationRequest:locationRequest];
    
    return locationRequest.requestID;
}

/**
 Creates a subscription for location updates that will execute the block once per update indefinitely (until canceled), regardless of the accuracy of each location.
 This method instructs location services to use the highest accuracy available (which also requires the most power).
 If an error occurs, the block will execute with a status other than INTULocationStatusSuccess, and the subscription will be canceled automatically.
 
 @param block The block to execute every time an updated location is available.
 The status will be INTULocationStatusSuccess unless an error occurred; it will never be INTULocationStatusTimedOut.
 
 @return The location request ID, which can be used to cancel the subscription of location updates to this block.
 */
- (INTULocationRequestID)subscribeToLocationUpdatesWithBlock:(INTULocationRequestBlock)block
{
    return [self subscribeToLocationUpdatesWithDesiredAccuracy:INTULocationAccuracyRoom
                                                         block:block];
}

/**
 Creates a subscription for location updates that will execute the block once per update indefinitely (until canceled), regardless of the accuracy of each location.
 The specified desired accuracy is passed along to location services, and controls how much power is used, with higher accuracies using more power.
 If an error occurs, the block will execute with a status other than INTULocationStatusSuccess, and the subscription will be canceled automatically.
 
 @param desiredAccuracy The accuracy level desired, which controls how much power is used by the device's location services.
 @param block           The block to execute every time an updated location is available. Note that this block runs for every update, regardless of
 whether the achievedAccuracy is at least the desiredAccuracy.
 The status will be INTULocationStatusSuccess unless an error occurred; it will never be INTULocationStatusTimedOut.
 
 @return The location request ID, which can be used to cancel the subscription of location updates to this block.
 */
// added modified
//******************************************************************************************
- (INTULocationRequestID)subscribeToLocationUpdatesWithDesiredAccuracy:(INTULocationAccuracy)desiredAccuracy
                                                                 block:(INTULocationRequestBlock)block
{
    return [self subscribeToLocationUpdatesWithDesiredAccuracy:INTULocationAccuracyRoom
                                                distanceFilter:kCLDistanceFilterNone
                                                         block:block];
}

- (INTULocationRequestID)subscribeToLocationUpdatesWithDesiredAccuracy:(INTULocationAccuracy)desiredAccuracy
                                                        distanceFilter:(int)distanceFilter
                                                                 block:(INTULocationRequestBlock)block
{
    NSAssert([NSThread isMainThread], @"INTULocationManager should only be called from the main thread.");
    
    INTULocationRequest *locationRequest = [[INTULocationRequest alloc] initWithType:INTULocationRequestTypeSubscription];
    locationRequest.delegate = self;
    locationRequest.desiredAccuracy = desiredAccuracy;
    locationRequest.block = block;
    locationRequest.distanceFilter = distanceFilter;
    
//    // added
//    // subscriber에는 timer를 하나씩 동작시킨다.
//    [locationRequest startTimeoutTimerIfNeededForContinues];
    
    [self addLocationRequest:locationRequest];
    
    return locationRequest.requestID;
}
// end of added by modified
//******************************************************************************************
/**
 Creates a subscription for significant location changes that will execute the block once per change indefinitely (until canceled).
 If an error occurs, the block will execute with a status other than INTULocationStatusSuccess, and the subscription will be canceled automatically.
 
 @param block The block to execute every time an updated location is available.
 The status will be INTULocationStatusSuccess unless an error occurred; it will never be INTULocationStatusTimedOut.
 
 @return The location request ID, which can be used to cancel the subscription of significant location changes to this block.
 */
- (INTULocationRequestID)subscribeToSignificantLocationChangesWithBlock:(INTULocationRequestBlock)block
{
    NSAssert([NSThread isMainThread], @"INTULocationManager should only be called from the main thread.");
    
    INTULocationRequest *locationRequest = [[INTULocationRequest alloc] initWithType:INTULocationRequestTypeSignificantChanges];
    locationRequest.block = block;
    
    [self addLocationRequest:locationRequest];
    
    return locationRequest.requestID;
}

/**
 Immediately forces completion of the location request with the given requestID (if it exists), and executes the original request block with the results.
 This is effectively a manual timeout, and will result in the request completing with status INTULocationStatusTimedOut.
 */
- (void)forceCompleteLocationRequest:(INTULocationRequestID)requestID
{
    NSAssert([NSThread isMainThread], @"INTULocationManager should only be called from the main thread.");
    
    for (INTULocationRequest *locationRequest in self.locationRequests) {
        if (locationRequest.requestID == requestID) {
            if (locationRequest.isRecurring) {
                // Recurring requests can only be canceled
                [self cancelLocationRequest:requestID];
            } else {
                [locationRequest forceTimeout];
                [self completeLocationRequest:locationRequest];
            }
            break;
        }
    }
}

/**
 Immediately cancels the location request with the given requestID (if it exists), without executing the original request block.
 */
- (void)cancelLocationRequest:(INTULocationRequestID)requestID
{
    NSAssert([NSThread isMainThread], @"INTULocationManager should only be called from the main thread.");
    
    for (INTULocationRequest *locationRequest in self.locationRequests) {
        if (locationRequest.requestID == requestID) {
            [locationRequest cancel];
            INTULMLog(@"Location Request canceled with ID: %ld", (long)locationRequest.requestID);
            [self removeLocationRequest:locationRequest];
            break;
        }
    }
}

#pragma mark - Public heading methods

/**
 Asynchronously requests the current heading of the device using location services.
 
 @param block The block to be executed when the request succeeds. One parameter is passed into the block:
 - The current heading (the most recent one acquired, regardless of accuracy level), or nil if no valid heading was acquired
 
 @return The heading request ID, which can be used remove the request from being called in the future.
 */
- (INTUHeadingRequestID)subscribeToHeadingUpdatesWithBlock:(INTUHeadingRequestBlock)block
{
    INTUHeadingRequest *headingRequest = [[INTUHeadingRequest alloc] init];
    headingRequest.block = block;
    
    [self addHeadingRequest:headingRequest];
    
    return headingRequest.requestID;
}

/**
 Immediately cancels the heading request with the given requestID (if it exists), without executing the original request block.
 */
- (void)cancelHeadingRequest:(INTUHeadingRequestID)requestID
{
    for (INTUHeadingRequest *headingRequest in self.headingRequests) {
        if (headingRequest.requestID == requestID) {
            [self removeHeadingRequest:headingRequest];
            INTULMLog(@"Heading Request canceled with ID: %ld", (long)headingRequest.requestID);
            break;
        }
    }
}

#pragma mark - Internal location methods

/**
 Adds the given location request to the array of requests, updates the maximum desired accuracy, and starts location updates if needed.
 */
- (void)addLocationRequest:(INTULocationRequest *)locationRequest
{
    INTULocationServicesState locationServicesState = [INTULocationManager locationServicesState];
    if (locationServicesState == INTULocationServicesStateDisabled ||
        locationServicesState == INTULocationServicesStateDenied ||
        locationServicesState == INTULocationServicesStateRestricted) {
        // No need to add this location request, because location services are turned off device-wide, or the user has denied this app permissions to use them
        [self completeLocationRequest:locationRequest];
        return;
    }
    
    switch (locationRequest.type) {
        case INTULocationRequestTypeSingle:
        case INTULocationRequestTypeSubscription:
        {
            INTULocationAccuracy maximumDesiredAccuracy = INTULocationAccuracyNone;
            // Determine the maximum desired accuracy for all existing location requests (does not include the new request we're currently adding)
            for (INTULocationRequest *locationRequest in [self activeLocationRequestsExcludingType:INTULocationRequestTypeSignificantChanges]) {
                if (locationRequest.desiredAccuracy > maximumDesiredAccuracy) {
                    maximumDesiredAccuracy = locationRequest.desiredAccuracy;
                }
            }
            // Take the max of the maximum desired accuracy for all existing location requests and the desired accuracy of the new request we're currently adding
            maximumDesiredAccuracy = MAX(locationRequest.desiredAccuracy, maximumDesiredAccuracy);
            [self updateWithMaximumDesiredAccuracy:maximumDesiredAccuracy];
            
            // added
            //******************************************************************************************
            //self.locationManager.distanceFilter = locationRequest.distanceFilter;//the minimum update distance in meters.
            // end of added
            //******************************************************************************************
            
            [self startUpdatingLocationIfNeeded];
        }
            break;
        case INTULocationRequestTypeSignificantChanges:
            [self startMonitoringSignificantLocationChangesIfNeeded];
            break;
    }
    __INTU_GENERICS(NSMutableArray, INTULocationRequest *) *newLocationRequests = [NSMutableArray arrayWithArray:self.locationRequests];
    [newLocationRequests addObject:locationRequest];
    self.locationRequests = newLocationRequests;
    INTULMLog(@"Location Request added with ID: %ld", (long)locationRequest.requestID);
    
    // Process all location requests now, as we may be able to immediately complete the request just added above
    // if a location update was recently received (stored in self.currentLocation) that satisfies its criteria.
    [self processLocationRequests];
}

/**
 Removes a given location request from the array of requests, updates the maximum desired accuracy, and stops location updates if needed.
 */
- (void)removeLocationRequest:(INTULocationRequest *)locationRequest
{
    __INTU_GENERICS(NSMutableArray, INTULocationRequest *) *newLocationRequests = [NSMutableArray arrayWithArray:self.locationRequests];
    [newLocationRequests removeObject:locationRequest];
    self.locationRequests = newLocationRequests;
    
    switch (locationRequest.type) {
        case INTULocationRequestTypeSingle:
        case INTULocationRequestTypeSubscription:
        {
            // Determine the maximum desired accuracy for all remaining location requests
            INTULocationAccuracy maximumDesiredAccuracy = INTULocationAccuracyNone;
            for (INTULocationRequest *locationRequest in [self activeLocationRequestsExcludingType:INTULocationRequestTypeSignificantChanges]) {
                if (locationRequest.desiredAccuracy > maximumDesiredAccuracy) {
                    maximumDesiredAccuracy = locationRequest.desiredAccuracy;
                }
            }
            [self updateWithMaximumDesiredAccuracy:maximumDesiredAccuracy];
            
            [self stopUpdatingLocationIfPossible];
        }
            break;
        case INTULocationRequestTypeSignificantChanges:
            [self stopMonitoringSignificantLocationChangesIfPossible];
            break;
    }
}

/**
 Returns the most recent current location, or nil if the current location is unknown, invalid, or stale.
 */
- (CLLocation *)currentLocation
{
    if (_currentLocation) {
        // Location isn't nil, so test to see if it is valid
        if (![GoogleUtils isLocationValid:_currentLocation.coordinate]) {
            // The current location is invalid; discard it and return nil
            _currentLocation = nil;
        }
    }
    
    // Location is either nil or valid at this point, return it
    return _currentLocation;
}

- (void)updateCurrentLocation:(CLLocation*)cloc
{
    _currentLocation = cloc;
}

/**
 Requests permission to use location services on devices with iOS 8+.
 */
- (void)requestAuthorizationIfNeeded
{
#if __IPHONE_OS_VERSION_MAX_ALLOWED > __IPHONE_7_1
    // As of iOS 8, apps must explicitly request location services permissions. INTULocationManager supports both levels, "Always" and "When In Use".
    // INTULocationManager determines which level of permissions to request based on which description key is present in your app's Info.plist
    // If you provide values for both description keys, the more permissive "Always" level is requested.
    
    double iOSVersion = floor(NSFoundationVersionNumber);
    BOOL isiOSVersion7to10 = iOSVersion > NSFoundationVersionNumber_iOS_7_1 && iOSVersion <= NSFoundationVersionNumber10_11_Max;
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined) {
        if (isiOSVersion7to10) {
            BOOL hasAlwaysKey = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSLocationAlwaysUsageDescription"] != nil;
            hasAlwaysKey = hasAlwaysKey || [[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSLocationAlwaysAndWhenInUseUsageDescription"] != nil;
            BOOL hasWhenInUseKey = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSLocationWhenInUseUsageDescription"] != nil;
            if (hasAlwaysKey) {
                [self.locationManager requestAlwaysAuthorization];
            } else if (hasWhenInUseKey) {
                [self.locationManager requestWhenInUseAuthorization];
            } else {
                // At least one of the keys NSLocationAlwaysUsageDescription or NSLocationWhenInUseUsageDescription MUST be present in the Info.plist file to use location services on iOS 8+.
                NSAssert(hasAlwaysKey || hasWhenInUseKey, @"To use location services in iOS 8+, your Info.plist must provide a value for either NSLocationWhenInUseUsageDescription or NSLocationAlwaysUsageDescription.");
            }
        } else {
            BOOL hasAlwaysAndInUseKey = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSLocationAlwaysAndWhenInUseUsageDescription"] != nil;
            if (hasAlwaysAndInUseKey) {
                [self.locationManager requestAlwaysAuthorization];
            } else {
                // Key NSLocationAlwaysAndWhenInUseUsageDescription MUST be present in the Info.plist file to use location services on iOS 11+.
                NSAssert(hasAlwaysAndInUseKey, @"To use location services in iOS 11+, your Info.plist must provide a value for NSLocationAlwaysAndWhenInUseUsageDescription.");
            }
        }
    }
#endif /* __IPHONE_OS_VERSION_MAX_ALLOWED > __IPHONE_7_1 */
}

/**
 Sets the CLLocationManager desiredAccuracy based on the given maximum desired accuracy (which should be the maximum desired accuracy of all active location requests).
 */
- (void)updateWithMaximumDesiredAccuracy:(INTULocationAccuracy)maximumDesiredAccuracy
{
    switch (maximumDesiredAccuracy) {
        case INTULocationAccuracyNone:
            break;
        case INTULocationAccuracyCity:
            if (self.locationManager.desiredAccuracy != kCLLocationAccuracyThreeKilometers) {
                self.locationManager.desiredAccuracy = kCLLocationAccuracyThreeKilometers;
                INTULMLog(@"Changing location services accuracy level to: low (minimum).");
            }
            break;
        case INTULocationAccuracyNeighborhood:
            if (self.locationManager.desiredAccuracy != kCLLocationAccuracyKilometer) {
                self.locationManager.desiredAccuracy = kCLLocationAccuracyKilometer;
                INTULMLog(@"Changing location services accuracy level to: medium low.");
            }
            break;
        case INTULocationAccuracyBlock:
            if (self.locationManager.desiredAccuracy != kCLLocationAccuracyHundredMeters) {
                self.locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
                INTULMLog(@"Changing location services accuracy level to: medium.");
            }
            break;
        case INTULocationAccuracyHouse:
            if (self.locationManager.desiredAccuracy != kCLLocationAccuracyNearestTenMeters) {
                self.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
                INTULMLog(@"Changing location services accuracy level to: medium high.");
            }
            break;
        case INTULocationAccuracyRoom:
            if (self.locationManager.desiredAccuracy != kCLLocationAccuracyBest) {
                self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
                INTULMLog(@"Changing location services accuracy level to: high (maximum).");
            }
            break;
        default:
            NSAssert(nil, @"Invalid maximum desired accuracy!");
            break;
    }
}

/**
 Inform CLLocationManager to start monitoring significant location changes.
 */
- (void)startMonitoringSignificantLocationChangesIfNeeded
{
    [self requestAuthorizationIfNeeded];
    
    NSArray *locationRequests = [self activeLocationRequestsWithType:INTULocationRequestTypeSignificantChanges];
    if (locationRequests.count == 0) {
        [self.locationManager startMonitoringSignificantLocationChanges];
        if (self.isMonitoringSignificantLocationChanges == NO) {
            INTULMLog(@"Significant location change monitoring has started.")
        }
        self.isMonitoringSignificantLocationChanges = YES;
    }
}

/**
 Inform CLLocationManager to start sending us updates to our location.
 */
- (void)startUpdatingLocationIfNeeded
{
    [self requestAuthorizationIfNeeded];
    
    NSArray *locationRequests = [self activeLocationRequestsExcludingType:INTULocationRequestTypeSignificantChanges];
    if (locationRequests.count == 0) {
        [self.locationManager startUpdatingLocation];
        if (self.isUpdatingLocation == NO) {
            INTULMLog(@"Location services updates have started.");
        }
        self.isUpdatingLocation = YES;
    }
}

- (void)stopMonitoringSignificantLocationChangesIfPossible
{
    NSArray *locationRequests = [self activeLocationRequestsWithType:INTULocationRequestTypeSignificantChanges];
    if (locationRequests.count == 0) {
        [self.locationManager stopMonitoringSignificantLocationChanges];
        if (self.isMonitoringSignificantLocationChanges) {
            INTULMLog(@"Significant location change monitoring has stopped.");
        }
        self.isMonitoringSignificantLocationChanges = NO;
    }
}

/**
 Checks to see if there are any outstanding locationRequests, and if there are none, informs CLLocationManager to stop sending
 location updates. This is done as soon as location updates are no longer needed in order to conserve the device's battery.
 */
- (void)stopUpdatingLocationIfPossible
{
    NSArray *locationRequests = [self activeLocationRequestsExcludingType:INTULocationRequestTypeSignificantChanges];
    if (locationRequests.count == 0) {
        [self.locationManager stopUpdatingLocation];
        if (self.isUpdatingLocation) {
            INTULMLog(@"Location services updates have stopped.");
        }
        self.isUpdatingLocation = NO;
    }
}

/**
 Iterates over the array of active location requests to check and see if the most recent current location
 successfully satisfies any of their criteria.
 */
- (void)processLocationRequests
{
    CLLocation *mostRecentLocation = self.currentLocation;
    
    for (INTULocationRequest *locationRequest in self.locationRequests) {
        if (locationRequest.hasTimedOut) {
            // Non-recurring request has timed out, complete it
            [self completeLocationRequest:locationRequest];
            continue;
        }
        
        if (mostRecentLocation != nil) {
            if (locationRequest.isRecurring) {
                // This is a subscription request, which lives indefinitely (unless manually canceled) and receives every location update we get
                [self processRecurringRequest:locationRequest];
                continue;
            } else {
                // This is a regular one-time location request
                NSTimeInterval currentLocationTimeSinceUpdate = fabs([mostRecentLocation.timestamp timeIntervalSinceNow]);
                CLLocationAccuracy currentLocationHorizontalAccuracy = mostRecentLocation.horizontalAccuracy;
                NSTimeInterval staleThreshold = [locationRequest updateTimeStaleThreshold];
                CLLocationAccuracy horizontalAccuracyThreshold = [locationRequest horizontalAccuracyThreshold];
                if (currentLocationTimeSinceUpdate <= staleThreshold &&
                    currentLocationHorizontalAccuracy <= horizontalAccuracyThreshold) {
                    // The request's desired accuracy has been reached, complete it
                    [self completeLocationRequest:locationRequest];
                    continue;
                }
            }
        }
    }
}

/**
 Immediately completes all active location requests.
 Used in cases such as when the location services authorization status changes to Denied or Restricted.
 */
- (void)completeAllLocationRequests
{
    // Iterate through a copy of the locationRequests array to avoid modifying the same array we are removing elements from
    __INTU_GENERICS(NSArray, INTULocationRequest *) *locationRequests = [self.locationRequests copy];
    for (INTULocationRequest *locationRequest in locationRequests) {
        [self completeLocationRequest:locationRequest];
    }
    INTULMLog(@"Finished completing all location requests.");
}

/**
 Completes the given location request by removing it from the array of locationRequests and executing its completion block.
 */
- (void)completeLocationRequest:(INTULocationRequest *)locationRequest
{
    if (locationRequest == nil) {
        return;
    }
    
    [locationRequest complete];
    [self removeLocationRequest:locationRequest];
    
    INTULocationStatus status = [self statusForLocationRequest:locationRequest];
    CLLocation *currentLocation = self.currentLocation;
    INTULocationAccuracy achievedAccuracy = [self achievedAccuracyForLocation:currentLocation];
    
    // INTULocationManager is not thread safe and should only be called from the main thread, so we should already be executing on the main thread now.
    // dispatch_async is used to ensure that the completion block for a request is not executed before the request ID is returned, for example in the
    // case where the user has denied permission to access location services and the request is immediately completed with the appropriate error.
    dispatch_async(dispatch_get_main_queue(), ^{
        if (locationRequest.block) {
            locationRequest.block(currentLocation, achievedAccuracy, status);
        }
    });
    
    INTULMLog(@"Location Request completed with ID: %ld, currentLocation: %@, achievedAccuracy: %lu, status: %lu", (long)locationRequest.requestID, currentLocation, (unsigned long) achievedAccuracy, (unsigned long)status);
}

// added
- (void)completeLocationRequestForContinues:(INTULocationRequest *)locationRequest
{
    if (locationRequest == nil) {
        return;
    }
    
    //[locationRequest complete];
    //[self removeLocationRequest:locationRequest];
    
    //INTULocationStatus status = [self statusForLocationRequest:locationRequest];
    CLLocation *currentLocation = self.currentLocation;
    INTULocationAccuracy achievedAccuracy = [self achievedAccuracyForLocation:currentLocation];
    
    // INTULocationManager is not thread safe and should only be called from the main thread, so we should already be executing on the main thread now.
    // dispatch_async is used to ensure that the completion block for a request is not executed before the request ID is returned, for example in the
    // case where the user has denied permission to access location services and the request is immediately completed with the appropriate error.
    dispatch_async(dispatch_get_main_queue(), ^{
        if (locationRequest.block) {
            locationRequest.block(currentLocation, achievedAccuracy, INTULocationStatusLocalTimedOut);
        }
    });
    
    //INTULMLog(@"Location Request Timer completed with ID: %ld, currentLocation: %@, achievedAccuracy: %lu, status: %lu", (long)locationRequest.requestID, currentLocation, (unsigned long) achievedAccuracy, (unsigned long)status);
}

/**
 Handles calling a recurring location request's block with the current location.
 */
- (void)processRecurringRequest:(INTULocationRequest *)locationRequest
{
    NSAssert(locationRequest.isRecurring, @"This method should only be called for recurring location requests.");
    
    INTULocationStatus status = [self statusForLocationRequest:locationRequest];
    CLLocation *currentLocation = self.currentLocation;
    INTULocationAccuracy achievedAccuracy = [self achievedAccuracyForLocation:currentLocation];
    
    // INTULocationManager is not thread safe and should only be called from the main thread, so we should already be executing on the main thread now.
    // dispatch_async is used to ensure that the completion block for a request is not executed before the request ID is returned.
    dispatch_async(dispatch_get_main_queue(), ^{
        if (locationRequest.block) {
            @try {
                locationRequest.block(currentLocation, achievedAccuracy, status);
            }
            @catch (NSException *exception) {
                NSLog(@"Exception: %@, %@", exception.name, exception.reason);
            }
        }
    });
}

/**
 Returns all active location requests with the given type.
 */
- (NSArray *)activeLocationRequestsWithType:(INTULocationRequestType)locationRequestType
{
    return [self.locationRequests filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(INTULocationRequest *evaluatedObject, NSDictionary *bindings) {
        return evaluatedObject.type == locationRequestType;
    }]];
}

/**
 Returns all active location requests excluding requests with the given type.
 */
- (NSArray *)activeLocationRequestsExcludingType:(INTULocationRequestType)locationRequestType
{
    return [self.locationRequests filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(INTULocationRequest *evaluatedObject, NSDictionary *bindings) {
        return evaluatedObject.type != locationRequestType;
    }]];
}

/**
 Returns the location manager status for the given location request.
 */
- (INTULocationStatus)statusForLocationRequest:(INTULocationRequest *)locationRequest
{
    INTULocationServicesState locationServicesState = [INTULocationManager locationServicesState];
    
    if (locationServicesState == INTULocationServicesStateDisabled) {
        return INTULocationStatusServicesDisabled;
    }
    else if (locationServicesState == INTULocationServicesStateNotDetermined) {
        return INTULocationStatusServicesNotDetermined;
    }
    else if (locationServicesState == INTULocationServicesStateDenied) {
        return INTULocationStatusServicesDenied;
    }
    else if (locationServicesState == INTULocationServicesStateRestricted) {
        return INTULocationStatusServicesRestricted;
    }
    else if (self.updateFailed) {
        return INTULocationStatusError;
    }
    else if (locationRequest.hasTimedOut) {
        return INTULocationStatusTimedOut;
    }
    
    return INTULocationStatusSuccess;
}

/**
 Returns the associated INTULocationAccuracy level that has been achieved for a given location,
 based on that location's horizontal accuracy and recency.
 */
- (INTULocationAccuracy)achievedAccuracyForLocation:(CLLocation *)location
{
    if (!location) {
        return INTULocationAccuracyNone;
    }
    
    NSTimeInterval timeSinceUpdate = fabs([location.timestamp timeIntervalSinceNow]);
    CLLocationAccuracy horizontalAccuracy = location.horizontalAccuracy;
    
    if (horizontalAccuracy <= kINTUHorizontalAccuracyThresholdRoom &&
        timeSinceUpdate <= kINTUUpdateTimeStaleThresholdRoom) {
        return INTULocationAccuracyRoom;
    }
    else if (horizontalAccuracy <= kINTUHorizontalAccuracyThresholdHouse &&
             timeSinceUpdate <= kINTUUpdateTimeStaleThresholdHouse) {
        return INTULocationAccuracyHouse;
    }
    else if (horizontalAccuracy <= kINTUHorizontalAccuracyThresholdBlock &&
             timeSinceUpdate <= kINTUUpdateTimeStaleThresholdBlock) {
        return INTULocationAccuracyBlock;
    }
    else if (horizontalAccuracy <= kINTUHorizontalAccuracyThresholdNeighborhood &&
             timeSinceUpdate <= kINTUUpdateTimeStaleThresholdNeighborhood) {
        return INTULocationAccuracyNeighborhood;
    }
    else if (horizontalAccuracy <= kINTUHorizontalAccuracyThresholdCity &&
             timeSinceUpdate <= kINTUUpdateTimeStaleThresholdCity) {
        return INTULocationAccuracyCity;
    }
    else {
        return INTULocationAccuracyNone;
    }
}

#pragma mark - Internal heading methods

/**
 Returns the most recent heading, or nil if the current heading is unknown or invalid.
 */
- (CLHeading *)currentHeading
{
    // Heading isn't nil, so test to see if it is valid
    if (!INTUCLHeadingIsIsValid(_currentHeading)) {
        // The current heading is invalid; discard it and return nil
        _currentHeading = nil;
    }
    
    // Heading is either nil or valid at this point, return it
    return _currentHeading;
}

/**
 Checks whether the given @c CLHeading has valid properties.
 */
BOOL INTUCLHeadingIsIsValid(CLHeading *heading)
{
    return heading.trueHeading > 0 &&
    heading.headingAccuracy > 0;
}

/**
 Adds the given heading request to the array of requests and starts heading updates.
 */
- (void)addHeadingRequest:(INTUHeadingRequest *)headingRequest
{
    NSAssert(headingRequest, @"Must pass in a non-nil heading request.");
    
    // If heading services are not available, just return
    if ([INTULocationManager headingServicesState] == INTUHeadingServicesStateUnavailable) {
        // dispatch_async is used to ensure that the completion block for a request is not executed before the request ID is returned.
        dispatch_async(dispatch_get_main_queue(), ^{
            if (headingRequest.block) {
                headingRequest.block(nil, INTUHeadingStatusUnavailable);
            }
        });
        INTULMLog(@"Heading Request (ID %ld) NOT added since device heading is unavailable.", (long)headingRequest.requestID);
        return;
    }
    
    __INTU_GENERICS(NSMutableArray, INTUHeadingRequest *) *newHeadingRequests = [NSMutableArray arrayWithArray:self.headingRequests];
    [newHeadingRequests addObject:headingRequest];
    self.headingRequests = newHeadingRequests;
    INTULMLog(@"Heading Request added with ID: %ld", (long)headingRequest.requestID);
    
    [self startUpdatingHeadingIfNeeded];
}

/**
 Inform CLLocationManager to start sending us updates to our heading.
 */
- (void)startUpdatingHeadingIfNeeded
{
    if (self.headingRequests.count != 0) {
        [self.locationManager startUpdatingHeading];
        if (self.isUpdatingHeading == NO) {
            INTULMLog(@"Heading services updates have started.");
        }
        self.isUpdatingHeading = YES;
    }
}

/**
 Removes a given heading request from the array of requests and stops heading updates if needed.
 */
- (void)removeHeadingRequest:(INTUHeadingRequest *)headingRequest
{
    __INTU_GENERICS(NSMutableArray, INTUHeadingRequest *) *newHeadingRequests = [NSMutableArray arrayWithArray:self.headingRequests];
    [newHeadingRequests removeObject:headingRequest];
    self.headingRequests = newHeadingRequests;
    
    [self stopUpdatingHeadingIfPossible];
}

/**
 Checks to see if there are any outstanding headingRequests, and if there are none, informs CLLocationManager to stop sending
 heading updates. This is done as soon as heading updates are no longer needed in order to conserve the device's battery.
 */
- (void)stopUpdatingHeadingIfPossible
{
    if (self.headingRequests.count == 0) {
        [self.locationManager stopUpdatingHeading];
        if (self.isUpdatingHeading) {
            INTULMLog(@"Location services heading updates have stopped.");
        }
        self.isUpdatingHeading = NO;
    }
}

/**
 Iterates over the array of active heading requests and processes each
 */
- (void)processRecurringHeadingRequests
{
    for (INTUHeadingRequest *headingRequest in self.headingRequests) {
        [self processRecurringHeadingRequest:headingRequest];
    }
}

/**
 Handles calling a recurring heading request's block with the current heading.
 */
- (void)processRecurringHeadingRequest:(INTUHeadingRequest *)headingRequest
{
    NSAssert(headingRequest.isRecurring, @"This method should only be called for recurring heading requests.");
    
    INTUHeadingStatus status = [self statusForHeadingRequest:headingRequest];
    
    // Check if the request had a fatal error and should be canceled
    if (status == INTUHeadingStatusUnavailable) {
        // dispatch_async is used to ensure that the completion block for a request is not executed before the request ID is returned.
        dispatch_async(dispatch_get_main_queue(), ^{
            if (headingRequest.block) {
                headingRequest.block(nil, status);
            }
        });
        
        [self cancelHeadingRequest:headingRequest.requestID];
        return;
    }
    
    // dispatch_async is used to ensure that the completion block for a request is not executed before the request ID is returned.
    dispatch_async(dispatch_get_main_queue(), ^{
        if (headingRequest.block) {
            headingRequest.block(self.currentHeading, status);
        }
    });
}

/**
 Returns the status for the given heading request.
 */
- (INTUHeadingStatus)statusForHeadingRequest:(INTUHeadingRequest *)headingRequest
{
    if ([INTULocationManager headingServicesState] == INTUHeadingServicesStateUnavailable) {
        return INTUHeadingStatusUnavailable;
    }
    
    // The accessor will return nil for an invalid heading results
    if (!self.currentHeading) {
        return INTUHeadingStatusInvalid;
    }
    
    return INTUHeadingStatusSuccess;
}

#pragma mark - INTULocationRequestDelegate method

- (void)locationRequestDidTimeout:(INTULocationRequest *)locationRequest
{
    // For robustness, only complete the location request if it is still active (by checking to see that it hasn't been removed from the locationRequests array).
    for (INTULocationRequest *activeLocationRequest in self.locationRequests) {
        if (activeLocationRequest.requestID == locationRequest.requestID) {
            [self completeLocationRequest:locationRequest];
            break;
        }
    }
}

// added
- (void)locationTimerDidTimeout:(INTULocationRequest *)locationRequest
{
    // For robustness, only complete the location request if it is still active (by checking to see that it hasn't been removed from the locationRequests array).
    for (INTULocationRequest *activeLocationRequest in self.locationRequests) {
        if (activeLocationRequest.requestID == locationRequest.requestID) {
            [self completeLocationRequestForContinues:locationRequest];
            break;
        }
    }
}

#pragma mark - CLLocationManagerDelegate methods

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations_org:(NSArray *)locations
{
    // Received update successfully, so clear any previous errors
    self.updateFailed = NO;
    
    CLLocation *mostRecentLocation = [locations lastObject];
    self.currentLocation = mostRecentLocation;
    
    // Process the location requests using the updated location
    [self processLocationRequests];
}

// added
//******************************************************************************************
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    // Received update successfully, so clear any previous errors
    self.updateFailed = NO;
    
    _realTimeCLocation = [locations lastObject];
    //NSLog(@"locationManager didUpdateLocations:%@",_realTimeCLocation);
    if (!_usefulLocations) {
        _usefulLocations = [[NSMutableArray alloc] init];
    }
    
    for (INTULocationRequest *locationRequest in self.locationRequests) {
        if (!locationRequest.isRecurring) {// 현재의 위치 요청
            CLLocation *mostRecentLocation = [locations lastObject];
            self.currentLocation = mostRecentLocation;
            
            // Process the location requests using the updated location
            [self processLocationRequests];
            return;
        }
    }

    // 이동중 위치 요청
    _usefulLocations = [self arrayOfUsefulLocationsWithLocations:locations];
    
    CLLocation *mostRecentLocation = [_usefulLocations lastObject];
    if ([_usefulLocations count] > 0) {
        
        if (_lastDirection != mostRecentLocation.course) {
            //[self willChangeValueForKey:@"lastDirection"];
            _lastDirection = mostRecentLocation.course;
            //[self didChangeValueForKey:@"lastDirection"];
        }
        
        if (_lastSpeed != mostRecentLocation.speed) {
            //[self willChangeValueForKey:@"lastSpeed"];
            _lastSpeed = mostRecentLocation.speed;
            //[self didChangeValueForKey:@"lastSpeed"];
        }
        
        if (_lastCoordinates.latitude != mostRecentLocation.coordinate.latitude && _lastCoordinates.longitude != mostRecentLocation.coordinate.longitude) {
            //[self willChangeValueForKey:@"lastCoordinates"];
            //_lastCoordinates = mostRecentLocation.coordinate;
            _lastCoordinates = self.currentLocation.coordinate;
            //[self didChangeValueForKey:@"lastCoordinates"];
        }
        
        self.currentLocation = mostRecentLocation;
        
        // Process the location requests using the updated location
        [self processLocationRequests];
    }
}
// end of added
//******************************************************************************************

- (void)locationManager:(CLLocationManager *)manager didUpdateHeading:(CLHeading *)newHeading
{
    self.currentHeading = newHeading;
    
    // Process the heading requests using the updated heading
    [self processRecurringHeadingRequests];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    INTULMLog(@"Location services error: %@", [error localizedDescription]);
    self.updateFailed = YES;
    
    for (INTULocationRequest *locationRequest in self.locationRequests) {
        if (locationRequest.isRecurring) {
            // Keep the recurring request alive
            [self processRecurringRequest:locationRequest];
        } else {
            // Fail any non-recurring requests
            [self completeLocationRequest:locationRequest];
        }
    }
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    if (status == kCLAuthorizationStatusDenied || status == kCLAuthorizationStatusRestricted) {
        // Clear out any active location requests (which will execute the blocks with a status that reflects
        // the unavailability of location services) since we now no longer have location services permissions
        [self completeAllLocationRequests];
    }
#if __IPHONE_OS_VERSION_MAX_ALLOWED > __IPHONE_7_1
    else if (status == kCLAuthorizationStatusAuthorizedAlways || status == kCLAuthorizationStatusAuthorizedWhenInUse)
#else
    else if (status == kCLAuthorizationStatusAuthorized)
#endif /* __IPHONE_OS_VERSION_MAX_ALLOWED > __IPHONE_7_1 */
    {
        
        // Start the timeout timer for location requests that were waiting for authorization
        for (INTULocationRequest *locationRequest in self.locationRequests) {
            [locationRequest startTimeoutTimerIfNeeded];
        }
    }
}

#pragma mark - Utils
-(void)activeKorea {
    if ([self.countryCode isEqualToString:@"KR"]) {
        self.isKorea = YES;
    }
    else {
        self.isKorea = NO;
    }
}

#pragma mark - STEP1
-(void)resetMgr {
    _steps = [NSMutableArray array];
    _features = nil;
    _paths = [GMSMutablePath path];
    _mapApilastRunTime = [NSDate dateWithTimeIntervalSince1970:0];
    _histline = nil;
    _histline.map = nil;
    _tripline = nil;
    _tripline.map = nil;
    _bestIndex = 0;
    _distancematrix = nil;
    _mapView = nil;
    _mapType = CMapTypeGoogleMaps;
    _isKorea = NO;
    _countryCode = @"US";
    _isMoveMapBYGesture = NO;
    
    [self activeLocale];
}

-(void)activeLocale {
    // carrier로 국가 확인하기
    CTTelephonyNetworkInfo *info = [CTTelephonyNetworkInfo new];
    CTCarrier *carrier = info.subscriberCellularProvider;
    NSString *countryCode = @"";
    if (carrier == nil) {
        NSLocale *locale = [NSLocale currentLocale];
        countryCode = [locale objectForKey: NSLocaleCountryCode];
    }
    else {
        countryCode = carrier.isoCountryCode;
        NSLog(@"Country code is: %@",carrier.mobileCountryCode);
        NSLog(@"ISO country code is: %@", carrier.isoCountryCode);
    }
    self.countryCode = [countryCode uppercaseString];
    
    [self activeKorea];
    
/*
    NSString *langID    = [[NSLocale preferredLanguages] objectAtIndex:0];
    NSArray* foo = [langID componentsSeparatedByString: @"-"];
    if ([foo count]>1) {
        self.countryCode = [foo objectAtIndex:1]; // [0]language [1]national
        if ([self.countryCode isEqualToString:@"KR"]) {
            self.isKorea = YES;
        }
    }
*/
    // timezone으로 국가 확인
    NSTimeZone* SystemTimeZone = [NSTimeZone systemTimeZone];
    if ([SystemTimeZone.name isEqualToString:@"Asia/Seoul"]) {
        self.countryCode = @"KR";
    }
    
    [self activeKorea];
    
    NSLog(@"countryCode[%@] isKorea:%d",self.countryCode, self.isKorea);
}

#pragma mark - Additions
/** It is possible to force enable background location fetch even if your set any kind of Authorizations */
- (void)setBackgroundLocationUpdate:(BOOL) enabled {
    _locationManager.allowsBackgroundLocationUpdates = enabled;
}

// added
//******************************************************************************************
-(NSMutableArray *)arrayOfUsefulLocationsWithLocations:(NSArray <CLLocation *> *)locations {
    NSMutableArray *usefulLocations = [[NSMutableArray alloc] init];
    for (CLLocation *location in locations) {
        if ([self ageOfLocationUpdate:location] < DESIRED_LOCATION_AGE) {
            //if (location.horizontalAccuracy < DESIRED_HORIZONTAL_ACCURACY && location.speed > DESIRED_SPEED) {
            if (location.horizontalAccuracy < DESIRED_HORIZONTAL_ACCURACY) {
                [usefulLocations addObject:location];
            }
        }
    }
    
    return usefulLocations;
}

/**
 * Helper method to extract the age of a CLLocation object
 *
 */
-(float)ageOfLocationUpdate:(CLLocation *)location {
    NSDate* eventDate = location.timestamp;
    NSTimeInterval howRecent = [eventDate timeIntervalSinceNow];
    return fabs(howRecent);
}

-(void)setFeatures:(NSDictionary*)feature {
    if (_features == nil) {
        _features = [NSDictionary dictionary];
    }
    _features = feature;
}

-(BOOL)isValidLocation:(CLLocationCoordinate2D)position {
    if (position.latitude == 0.0f && position.longitude == 0.0f) {
        return NO;
    }
    if (position.latitude == 180.0f && position.longitude == 180.0f) {
        return NO;
    }
    if (position.latitude == -180.0f && position.longitude == -180.0f) {
        return NO;
    }
    
    return YES;
}

+ (CLLocationCoordinate2D)coordinateFromDictionary:(NSDictionary *)dictionary {
    NSNumber *lat = [dictionary objectForKey:@"lat"];
    NSNumber *lng = [dictionary objectForKey:@"lng"];
    
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(lat.doubleValue, lng.doubleValue);
    return coordinate;
}

-(UIImage *)image:(UIImage*)originalImage scaledToSize:(CGSize)size
{
    if (CGSizeEqualToSize(originalImage.size, size))
    {
        return originalImage;
    }
    
    UIGraphicsBeginImageContextWithOptions(size, NO, 0.0f);
    
    [originalImage drawInRect:CGRectMake(0.0f, 0.0f, size.width, size.height)];
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

#pragma mark - MarkerUtils
#pragma mark - STEP3
-(void)initMarker {
    
    self.markerPickup  = [GMSMarker new];
    self.markerStart   = [GMSMarker new];
    self.markerFinish  = [GMSMarker new];
    self.markerDrive   = [GMSMarker new];
    
    self.markerPickup.map  = nil;
    self.markerStart.map   = nil;
    self.markerFinish.map  = nil;
    self.markerDrive.map   = nil;
    
    self.markerPickup.icon = [self getMarkerIcon:@"pickup"];
    self.markerStart.icon  = [self getMarkerIcon:@"route_start"];
    self.markerFinish.icon = [self getMarkerIcon:@"route_end"];
    self.markerDrive.icon  = nil;
    
    self.markerDrive.groundAnchor = CGPointMake(0.5, 0.5);
}

-(void)showStartMarker:(CLLocationCoordinate2D)lastCoordinates {
    
    if (self.markerStart.map == nil || [GoogleUtils nullToImage:self.markerStart.icon]) {
        self.markerStart.icon  = [self getMarkerIcon:@"route_start"];
        [self updateStartMarker:lastCoordinates];
    }
}

-(void)showFinishMarker:(CLLocationCoordinate2D)lastCoordinates {
    
    if (self.markerFinish.map == nil || [GoogleUtils nullToImage:self.markerFinish.icon]) {
        self.markerFinish.icon  = [self getMarkerIcon:@"route_end"];
        [self updateFinishMarker:lastCoordinates];
    }
}

-(void)showDriveMarker:(CLLocationCoordinate2D)lastCoordinates{
    
    if (self.markerDrive.map == nil || [GoogleUtils nullToImage:self.markerDrive.icon]) {
        [self updateDriveMarker];
        //self.markerDrive.icon  = [self getMarkerIcon:service];
        
        if (!CLLocationCoordinate2DIsValid(lastCoordinates)) {
            [self updateDriveMarker:self.realTimeCLocation.coordinate];
        }
        else {
            [self updateDriveMarker:lastCoordinates];
        }
        
    }
}

-(void)updateStartMarker:(CLLocationCoordinate2D)lastCoordinates
{
    self.markerStart.position = lastCoordinates;
    self.markerStart.map = self.mapView;
}

-(void)updateFinishMarker:(CLLocationCoordinate2D)lastCoordinates
{
    self.markerFinish.position = lastCoordinates;
    self.markerFinish.map = self.mapView;
}

-(void)updateDriveMarker:(CLLocationCoordinate2D)lastCoordinates
{
    // 차량 마커 생성
    //self.markerDrive = [GMSMarker markerWithPosition:lastCoordinates];
    self.markerDrive.position = lastCoordinates;
    self.markerDrive.groundAnchor = CGPointMake(0.5, 0.5);
    self.markerDrive.map = self.mapView;
}

/*
 service = X;
 "service_name" = Witz;
 service = P;
 "service_name" = WitzPlus;
 service = L;
 "service_name" = WitzLuxury;
 service = A;
 "service_name" = WitzAgent;
 */
-(UIImage*)getMarkerIcon:(NSString*)icon__ {
    UIImage *icon = nil;
    NSLog(@"getMarkerIcon:%@",icon__);
    if (icon__.length == 1) {
        //UIImage *img = [UIImage imageNamed:@"img_mapcar"];
        UIImage *img = [UIImage imageNamed:[NSString stringWithFormat:@"img_mapcar_%@",icon__]];
        icon = [self image:img scaledToSize:CGSizeMake(kDriverMarkerSizeW, kDriverMarkerSizeH)];
    }
    else if ([icon__ isEqualToString:@"route_start"])
        icon = [self image:[UIImage imageNamed:@"icon_pin_start"] scaledToSize:CGSizeMake(kMarkerSizeW, kMarkerSizeH)];
    else if ([icon__ isEqualToString:@"route_end"])
        icon = [self image:[UIImage imageNamed:@"icon_pin_end"] scaledToSize:CGSizeMake(kMarkerSizeW, kMarkerSizeH)];
    else if ([icon__ isEqualToString:@"pickup"])
        icon = [self image:[UIImage imageNamed:@"icon_pin_start"] scaledToSize:CGSizeMake(kMarkerSizeW, kMarkerSizeH)];
    else {
        NSLog(@"getMarkerIcon ERROR:%@",icon__);
    }
    return icon;
}

-(void)clearMarker:(GMSMarker*)marker__ {
    if (self.appDelegate.isDriverMode && self.appDelegate.pstate > 210) {
        if ([marker__ isEqual:self.markerDrive]) return;
    }
    
    if (marker__.map != nil || CLLocationCoordinate2DIsValid(marker__.position)) {
        marker__.map = nil;
//        marker__.icon = nil;
        marker__.position = kCLLocationCoordinate2DInvalid;
    }
}

#pragma mark - MapUtils
- (void)initMapInfo:(id)mapview mapType:(CMapType)mapType
{
    if (self.mapView == nil) {
        self.mapView = mapview;
    }
    if ( mapType != CMapTypeNone) {
        self.mapType = mapType;
    }
    else {
        if (self.isKorea) {
            self.mapType = CMapTypeTmaps;
        }
        else {
            self.mapType = CMapTypeGoogleMaps;
        }
    }
}

#pragma mark - STEP2
- (void)initMapInfo:(id)mapview
{
    [self initMapInfo:mapview mapType:CMapTypeNone];
}

- (void)drawTripLine {
    
    if ([self.paths count]) {
        self.tripline.map = nil;
        self.tripline = [GMSPolyline polylineWithPath:self.paths];
        self.tripline.strokeWidth = 5.0f;
        self.tripline.strokeColor = [UIColor blackColor];
        self.tripline.map = self.mapView;
        
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_ETA_PRINT object:nil userInfo:nil];
    }
}

- (void)drawHistoryPolyline:(CLLocationCoordinate2D)bestPoint
                    current:(CLLocationCoordinate2D)currpos
{
    
    GMSMutablePath *paths = [[GMSMutablePath alloc] init];
    NSInteger count = self.paths.count;
    for (int i=0; i < count ; i++) {
        CLLocationCoordinate2D point = [self.paths coordinateAtIndex:i];
        
        //[paths addCoordinate:point];
        
        if ([GoogleUtils isEqualLocation:bestPoint location:point]) {
            // 위치를 찾으면 해당 위치의까지 history line을 그린다.
            [paths addCoordinate:currpos];
            
            break;
        }
    }
    
    if ([paths count]) {
        self.histline.map = nil;
        self.histline = [GMSPolyline polylineWithPath:paths];
        self.histline.strokeWidth = 2.0f;
        self.histline.strokeColor = [UIColor redColor];
        self.histline.map = self.mapView;
    }
}

- (void)updateMapFitBounds:(NSArray *_Nullable)coord__
{
    GMSCoordinateBounds *bounds = nil;
    NSInteger __pathCount = [self.tripline.path count];
    for (int i =0; i<__pathCount ; i++) {
        CLLocationCoordinate2D coord = [self.tripline.path coordinateAtIndex:i];
        if (bounds == nil) {
            bounds = [[GMSCoordinateBounds alloc] initWithCoordinate:coord
                                                          coordinate:coord];
        }
        bounds = [bounds includingCoordinate:coord];
    }
    
//    if ([GoogleUtils isArray:coord__] && [coord__ count]) {
//        for (NSValue *aValue in coord__) {
//            CLLocationCoordinate2D coord = [aValue MKCoordinateValue];
//            if (bounds == nil) {
//                bounds = [[GMSCoordinateBounds alloc] initWithCoordinate:coord
//                                                              coordinate:coord];
//            }
//            bounds = [bounds includingCoordinate:coord];
//        }
//    }
    //CGFloat currentZoom = self.mapView.camera.zoom;
    
    [CATransaction begin];
    [CATransaction setAnimationDuration:0.5];
    
    GMSCameraUpdate *update = [GMSCameraUpdate fitBounds:bounds withPadding:kMapPaddingDefault];
    //GMSCameraUpdate *update = [GMSCameraUpdate fitBounds:bounds];
    [self.mapView animateWithCameraUpdate:update];
    
    [CATransaction commit];
}

-(void)moveDriveMarker:(float)animationDuration
                  coor:(CLLocationCoordinate2D)coordinate
                  head:(BOOL)isChangedHeading
               heading:(CLLocationDirection)heading
{
    if (animationDuration) {
        [CATransaction begin];
        [CATransaction setValue:[NSNumber numberWithFloat: animationDuration] forKey:kCATransactionAnimationDuration];
        [CATransaction setCompletionBlock:^{
            // ... whatever you want to do when the animation is complete
            //currLocMarker.rotation = heading; // 지도를 돌리면 차량을 돌릴 필요없음.
        }];
    }
    
    if (!self.isMoveMapBYGesture) {
        [self.mapView animateToLocation:coordinate];
    }
    
    self.markerDrive.position = coordinate;
    
    if (isChangedHeading) {
        //currLocMarker.rotation = heading; // 지도를 돌리면 차량을 돌릴 필요없음.
        [self.mapView animateToBearing:heading];
    }
    
    if (animationDuration) {
        [CATransaction commit];
    }
}

-(void)moveToCurrentLocation:(float)animationDuration
                lineDistance:(int)lineDistance
                    location:(CLLocation *)polylineLocation {
    
    //polylineLocation이 없으면 현재 좌표로 처리한다.
    if (polylineLocation == nil) {
        polylineLocation = [self currentLocation];
        
        if (polylineLocation == nil) {
            [self requestLocationWithDesiredAccuracy:INTULocationAccuracyBlock
                                                                        timeout:0.1
                                                           delayUntilAuthorized:YES
                                                                          block:
                                      ^(CLLocation *currentLocation, INTULocationAccuracy achievedAccuracy, INTULocationStatus status) {
                                          
                                          if (status == INTULocationStatusSuccess) {
                                              double delayInSeconds = 0.000001;
                                              dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
                                              dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                                                  [self moveToCurrentLocation:animationDuration lineDistance:lineDistance location:currentLocation];
                                              });
                                          }
                                          else if (status == INTULocationStatusTimedOut) {
                                          }
                                          else {
                                              // An error occurred
                                          }
                                      }];
            return;
        }
    }
    // [self.mapView animateToBearing:0];
    
    // 1.1 heading을 구한다.
    BOOL isChangedHeading = YES;
    CLLocationDirection heading = GMSGeometryHeading(self.markerDrive.position, polylineLocation.coordinate);
    
    // 1.2 polyline이 있으면 현재 이전의 coordinate값으로 계산, 없으면 현재위치로 계산
    //if (locMgr.lastPolyLocation != nil) {
    //의미없어 주석처리
    if (lineDistance > OUT_OF_LINE_DISTANCE) { // polyline에서 벗어났으면 gps에 대해서 heading은 제한을 둔다.
        CLLocationDirection pheading = GMSGeometryHeading(self.markerDrive.position, self.lastCoordinates);
        if (!pheading) {
            pheading = GMSGeometryHeading(self.markerDrive.position, self.lastPolyLocation.coordinate);
        }
        
        float angle = fabs(heading - pheading);
        if (angle > 180) angle -= 180;
        if ( fabs(angle) < 30 ) {
            isChangedHeading = NO;
        }
        //NSLog(@"heading:%f,%f(%f,%f)",angle,fabs(heading - pheading),heading,pheading);
    }
    
    //NSLog(@"isChangedHeading:%d",isChangedHeading);
    // 2. polylineLocation으로 map움직이기-> polyline을 따라 움직이게 하기 위해 observeValueForKeyPath를 사용하지 않는다.
    // if 좌표의 거리가 100m 이상이면 바로 이동한다.
    // 3. 차량 marker를 polylineLocation으로 움직인다. polyline을 벗어나면 currentLocation으로 움직인다.
    //CLLocationDistance newDistance =  GMSGeometryDistance(locMgr.lastCoordinates,polylineLocation.coordinate);
    CLLocationDistance newDistance =  GMSGeometryDistance(self.markerDrive.position,polylineLocation.coordinate);
    if (newDistance>100) {
        
        [self moveDriveMarker:0 coor:polylineLocation.coordinate head:isChangedHeading heading:heading];
    }
    else {
        // 화면이동은 1.5초 이내로 제한한다.
        double eventTime = 1.0;
        if (animationDuration < 0) {
            animationDuration = eventTime - (newDistance*eventTime*0.01);
        }
        //NSLog(@"animationDuration:%f,%f",animationDuration,newDistance);
        
        [self moveDriveMarker:0 coor:polylineLocation.coordinate head:isChangedHeading heading:heading];
        
    }
}

// map update : 차량이동 및 지도 이동
-(void) updateDriveMap:(CLLocation *)currentLocation dst:(CLLocationCoordinate2D)_endPoint
{
    // 0.좌표가 오류면 무시한다.
    if (![GoogleUtils isLocationValid:currentLocation.coordinate] ||
        ![GoogleUtils isLocationValid:_endPoint]) {
        return;
    }
    // 1.동일한 좌표면 무시한다.
    if (self.lastCoordinates.latitude == currentLocation.coordinate.latitude &&
        self.lastCoordinates.longitude == currentLocation.coordinate.longitude) {
        return;
    }
    
    // 2.speed가 1mps 이상 일때만 유효한 좌표로 인식한다
#if TARGET_IPHONE_SIMULATOR
    if(1)
#else
    if(currentLocation.speed >= 1)
#endif
    {
        // 운행중일때 좌표가 polyline의 가장 가까운 지점으로 처리한다. 좌표는 polyline의 좌표가 된다.
        
        // 1. currentLocation을 가지고 polyline의 polylineLocation 좌표를 찾는다. polyline을 벗어나면 현재위치를 좌표로 한다.
        CLLocation *polylineLocation = currentLocation;
        
        // 2. polyline을 벗어났는지 체크한다.
        NSDictionary *points = [self checkOutOfBoundPolyLine];
        if (points != nil) {
            // 2.1 polyline에 존재하면
            CGPoint bestPoint = [points[@"bestPoint"] CGPointValue];
            CLLocationCoordinate2D point = CLLocationCoordinate2DMake(bestPoint.y, bestPoint.x);
            
            // 2.2 ployline과 gps의 거리를 가져온다.
            int lineDistance  = [points[@"newDistance"] intValue];
            
            if (lineDistance <= OUT_OF_LINE_DISTANCE) {
                // 2.3 ployline과 gps의 거리가 10m 이하면 polyline의 bestPoint를 지도의 현재 위치로 한다.
                polylineLocation = [[CLLocation alloc] initWithLatitude:point.latitude longitude:point.longitude];
            }
            else {
                // 2.3 ployline과의 gps의 거리가 10m 이상이면 길을 벚어난걸로 처리하고 현재 gps를 지도의 현재 위치로 한다.
                polylineLocation = currentLocation; // 50m 이내면 모두 라인이라고 판단하도록 한다.
                //polylineLocation = [[CLLocation alloc] initWithLatitude:point.latitude longitude:point.longitude];
            }
            
#ifdef _ETA_LOCAL
            // dashboard계산
            [self calculateDistanceAndDuration:points cloc:currentLocation];
#endif
            
            // polyline history
            CGPoint bestCoord = [points[@"bestCoord"] CGPointValue];
            [self drawHistoryPolyline:CLLocationCoordinate2DMake(bestCoord.y, bestCoord.x)
                              current:self.markerDrive.position];
            
            // 차량과 맵을 해당 위치로 옮기고 heading(지도,차량)을 돌린다.
            [self moveToCurrentLocation:-1 lineDistance:lineDistance location:polylineLocation];
        }
        else {
            
            // 라인을 벗어났으면 api를 다시 요청한다.
            
            //CLLocationCoordinate2D beforeLocation = self.currLocMarker.position;
            CLLocationCoordinate2D beforeLocation = self.lastCoordinates;
            // 이전 좌표가 존재하면 방향성을 줄지 결정한다.
            //if ([locMgr isValidLocation:locMgr.lastCoordinates]) {
            if ([self isValidLocation:beforeLocation]) {
                // 현재 좌표가 존재하면 방향성을 준다.
                if ([self isValidLocation:currentLocation.coordinate]) {
                    
                    // marker가 존재하지 않으면 현재위치로 마커를 찍는다.
                    if (![GoogleUtils isLocationValid:self.markerDrive.position]) {
                        [self updateDriveMarker:currentLocation.coordinate];
                        //self.markerDrive.position = currentLocation.coordinate;
                        //[self initCurrLocMarker:currentLocation.coordinate];
                    }
                    
                    // start와 현재 위치가 동일하면 waypoints는 없다.
                    CLLocationCoordinate2D waypoints = kCLLocationCoordinate2DInvalid;
                    if (![GoogleUtils isEqualLocation:self.markerDrive.position location:currentLocation.coordinate]) {
                        waypoints = currentLocation.coordinate;
                    }
                    
                    [self requestDirection:self.markerDrive.position
                                      endPos:_endPoint
                                andWaypoints:waypoints // waypoint 추가(경로지를 현재 위치로 하고 차량의 진행 방향을 결정한다.
                                    andParam:nil
                                  andMapType:self.mapType completion:^(NSDictionary *paramDic, int status) {
                                      
                                      if (status==1) {
                                          dispatch_async(dispatch_get_main_queue(), ^{
                                              // Update the UI
                                              // 지도에 표시
                                              [self showStartMarker:self.markerDrive.position];
                                              [self showFinishMarker:_endPoint];
                                              [self updateDriveMarker];
                                              
                                              [self drawTripLine];
                                              
                                          });
                                      }
                                  }];
                }
                else {
                    // 현재 좌표가 없으면 이전 좌표로 조회한다.
                    [self requestDirection:beforeLocation
                                      endPos:_endPoint
                                andWaypoints:kCLLocationCoordinate2DInvalid
                                    andParam:nil
                                  andMapType:self.mapType completion:^(NSDictionary *paramDic, int status) {
                                      
                                      if (status==1) {
                                          dispatch_async(dispatch_get_main_queue(), ^{
                                              // Update the UI
                                              // 지도에 표시
                                              [self showStartMarker:beforeLocation];
                                              [self showFinishMarker:_endPoint];
                                              
                                              [self drawTripLine];
                                          });
                                      }
                                  }];
                }
            }
            else {
                if ([self isValidLocation:currentLocation.coordinate]) {
                    // 이전 좌표가 없으면 현재 좌표로 조화한다.
                    [self requestDirection:currentLocation.coordinate
                                      endPos:_endPoint
                                andWaypoints:kCLLocationCoordinate2DInvalid
                                    andParam:nil
                                  andMapType:self.mapType completion:^(NSDictionary *paramDic, int status) {
                                      
                                      if (status==1) {
                                          dispatch_async(dispatch_get_main_queue(), ^{
                                              // Update the UI
                                              // 지도에 표시
                                              [self showStartMarker:currentLocation.coordinate];
                                              [self showFinishMarker:_endPoint];
                                              
                                              [self drawTripLine];
                                          });
                                      }
                                  }];
                }
                else {
                    NSLog(@"Location Error");
                    return;
                }
            }
            CLLocationDistance newDistance =  GMSGeometryDistance(self.markerDrive.position,polylineLocation.coordinate);
            // 차량과 맵을 해당 위치로 옮기고 heading(지도,차량)을 돌린다.
            [self moveToCurrentLocation:-1 lineDistance:newDistance location:polylineLocation];
        }
    }
}

- (void)updateDriveMarker
{
    if (self.markerDrive.map == nil || [GoogleUtils nullToImage:self.markerDrive.icon]) {
        //NSDictionary *pvehicle = [self.dicRider objectForKey:@"provider_vehicle"];
        NSDictionary *pvehicle;
        if (self.appDelegate.isDriverMode) {
            pvehicle = [self.appDelegate.dicDriver objectForKey:@"provider_device"];
        }
        else {
            pvehicle = [self.appDelegate.dicRider objectForKey:@"provider_device"];
        }

        NSString *service = [pvehicle objectForKey:@"service"];

        self.markerDrive.icon = [self getMarkerIcon:service];
    }
}

#pragma mark - MapAPI
/*
// setSKPMapApiKey: 성공시 발생하는 callback
- (void)SKPMapApikeySucceed
{
    NSLog(@"SKPMapApikeySucceed");
}

// setSKPMapApiKey: 실패시 발생하는 callback
- (void)SKPMapApikeyFailed:(NSError*)error
{
    NSLog(@"SKPMapApikeyFailed : %@", [error localizedDescription]);
}

- (void)SKPMapAuth
{
    static BOOL skpflag = NO;
    if (skpflag) {
        return;
    }
    [TMapTapi setSKPMapAuthenticationWithDelegate:self apiKey:_TMAP_API_KEY];
    skpflag = YES;
}
*/

-(void)googleMapsAPICall:(CLLocationCoordinate2D)position
                  endPos:(CLLocationCoordinate2D)position1
                andParam:(NSDictionary*)param
              completion: (user_completion_block)completion
{
    @try{
        //https://developers.google.com/maps/documentation/directions/intro?hl=ko
        NSString *url_String = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/directions/json"];
        
        if ([param valueForKey:@"matrix"]) {
            //https://developers.google.com/maps/documentation/distance-matrix/intro?hl=ko#travel_modes
            url_String = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/distancematrix/json"];
        }
        NSMutableDictionary *parameters=[[NSMutableDictionary alloc]init];
        [parameters setValue:[NSString stringWithFormat:@"%f,%f",position.latitude,position.longitude] forKey:@"origin"];
        [parameters setValue:[NSString stringWithFormat:@"%f,%f",position1.latitude,position1.longitude] forKey:@"destination"];
        [parameters setValue:@"now" forKey:@"departure_time"];
        [parameters setValue:@"tolls,highways" forKey:@"avoid"];
        [parameters setValue:@"best_guess" forKey:@"traffic_model"];
        [parameters setValue:@"driving" forKey:@"mode"];
        [parameters setValue:_GOOGLE_API_KEY forKey:@"key"];
        [parameters setValue:[NSString stringWithFormat:@"%lu",(unsigned long)CMapTypeGoogleMaps] forKey:@"mapType"];
        
        if (param != nil) {
            [parameters addEntriesFromDictionary:param];
        }
        
        [self callGetWebService:url_String method:@"GET" andDictionary:parameters completion:^(NSDictionary* responseDict, NSString*params, NSError*error, long code)
         {
             if(error)
             {
                 if(completion)
                 {
                     completion(responseDict,params,@"OOPS Seems like something is wrong with server",-1);
                 }
             }
             else{
                 if(completion)
                 {
                     //NSLog(@"responseDict:%@",responseDict);
                     if([responseDict valueForKey:@"routes"])
                     {
                         completion(responseDict,params,@"Got Response ",1);
                     }
                 }
             }
         }];
    } @catch (NSException *exception) {
        NSLog(@"Exception At: %s %d %s %s %@", __FILE__, __LINE__, __PRETTY_FUNCTION__, __FUNCTION__,exception);
    }
}

-(void)tMapsAPICall:(CLLocationCoordinate2D)position
             endPos:(CLLocationCoordinate2D)position1
           andParam:(NSDictionary*)param
         completion: (user_completion_block)completion
{
    @try{
        
        NSString *url_String = [NSString stringWithFormat:@"https://api2.sktelecom.com/tmap/routes?version=1"];
        
        NSMutableDictionary *parameters=[[NSMutableDictionary alloc]init];
        //[parameters setValue:@"1" forKey:@"version"];
        [parameters setValue:[NSString stringWithFormat:@"%f",position.longitude] forKey:@"startX"];
        [parameters setValue:[NSString stringWithFormat:@"%f",position.latitude] forKey:@"startY"];
        //[parameters setValue:@"1" forKey:@"directionOption"];
        [parameters setValue:@"WGS84GEO" forKey:@"reqCoordType"];
        [parameters setValue:@"WGS84GEO" forKey:@"resCoordType"];
        [parameters setValue:[NSString stringWithFormat:@"%f",position1.longitude] forKey:@"endX"];
        [parameters setValue:[NSString stringWithFormat:@"%f",position1.latitude] forKey:@"endY"];
        [parameters setValue:[NSString stringWithFormat:@"%lu",(unsigned long)CMapTypeTmaps] forKey:@"mapType"];
        if (param != nil) {
            [parameters addEntriesFromDictionary:param];
        }
        
        [self callGetWebService:url_String method:@"GET" andDictionary:parameters completion:^(NSDictionary* responseDict, NSString* params, NSError*error, long code)
         {
             if(error)
             {
                 if(completion)
                 {
                     completion(responseDict,params,@"OOPS Seems like something is wrong with server",-1);
                 }
                 
             }
             else{
                 if(completion)
                 {
                     //NSLog(@"responseDict:%@",responseDict);
                     if([responseDict valueForKey:@"features"])
                     {
                         completion(responseDict,params,@"Got Response ",1);
                     }
                 }
             }
         }];
    } @catch (NSException *exception) {
        NSLog(@"Exception At: %s %d %s %s %@", __FILE__, __LINE__, __PRETTY_FUNCTION__, __FUNCTION__,exception);
    }
}

-(void)witzMapsAPICall:(CLLocationCoordinate2D)position
                endPos:(CLLocationCoordinate2D)position1
              andParam:(NSDictionary*)param
            completion: (user_completion_block)completion
{
    @try{
        
//        NSString *url_String = [NSString stringWithFormat:@"http://api.tb.strastar.com/witzapi/2.0/provider/map.php?cmd=provider/map&req=direction/request&mtd=GET&mid=PM400&did=AAAAAAAA-AAAA-AAAA-AAAA-AAAAAAAAAAAA&time=1511849414&datoken=AAAAAAAA-AAAA-AAAA-AAAA-AAAAAAAAAAAA:ac9af3febaeb5f93d5624c640c15964fa82b0c3e811ce95c2e9c1375f7d6e5a9&location=37.625790,126.832148&lang=ko&model=x86_64&os=iOS&app=kr.co.chachacreation.tbrider:1.0.0&service=drive&mode=driving&running=forground&version=1.0"];
        
        NSString *url_String = API_DIRECTION_URI;
        
        //NSString *url_String = [NSString stringWithFormat:@"%@&%@",API_DIRECTION_URI,[Utils get20Header]];
        /*
         service=drive
         mode=driving
         running=forground
         version=1.0
         etype=text
         useq=19
         cseq=213
         did=AAAAAAAA-AAAA-AAAA-AAAA-AAAAAAAAAAAA
         lang=ko
         model=x86_64
         os=iOS:11.1
         app=kr.co.chachacreation.tbrider:1.0.0
         atoken=AAAAAAAA-AAAA-AAAA-AAAA-AAAAAAAAAAAA:ac9af3febaeb5f93d5624c640c15964fa82b0c3e811ce95c2e9c1375f7d6e5a9
         lat=37.625790
         lng=126.832148
         midx=UQ002
         cmd=user/drive
         req=query/driver
         */
        
        NSMutableDictionary *parameters=[[NSMutableDictionary alloc]init];
        [parameters setValue:[NSString stringWithFormat:@"%f,%f",position.latitude,position.longitude] forKey:@"lsrc"];
        [parameters setValue:[NSString stringWithFormat:@"%f,%f",position1.latitude,position1.longitude] forKey:@"ldst"];
        [parameters setValue:[NSString stringWithFormat:@"%lu",(unsigned long)CMapTypeWitz] forKey:@"mapType"];
        if (param != nil) {
            [parameters addEntriesFromDictionary:param];
        }
        
        [self callGetWebService:url_String method:@"GET" andDictionary:parameters completion:^(NSDictionary* responseDict, NSString* params, NSError*error, long code)
         {
             if(error)
             {
                 if(completion)
                 {
                     completion(responseDict,params,@"OOPS Seems like something is wrong with server",-1);
                 }
                 
             }
             else{
                 if(completion)
                 {
                     //NSLog(@"responseDict:%@",responseDict);
                     if([responseDict valueForKey:@"direction"])
                     {
                         completion(responseDict,params,@"Got Response ",1);
                     }
                 }
             }
         }];
    } @catch (NSException *exception) {
        NSLog(@"Exception At: %s %d %s %s %@", __FILE__, __LINE__, __PRETTY_FUNCTION__, __FUNCTION__,exception);
    }
}
/*
 GMSPlacesClient *_placesClient;
 GMSPlace *_place;
 
 
 [GMSPlacesClient provideAPIKey:_GOOGLE_PLACE_API_KEY];
 //https://console.developers.google.com/flows/enableapi?apiid=placesios,maps_ios_backend&keyType=CLIENT_SIDE_IOS
 _placesClient = [GMSPlacesClient sharedClient];
 
 GMSGeocoder *_geocoder;
 _placeMarker = [[GMSMarker alloc] init];
 _geocoder = [[GMSGeocoder alloc] init];
 
 #import <GooglePlaces/GooglePlaces.h>
 
 
 #import "TMapView.h"
 
 */
-(void)geocodeAPICall:(CLLocationCoordinate2D)position
           completion: (user_completion_block)completion
{
    if (self.isKorea) {
        [self geocodeAPICall:position mapType:CMapTypeTmaps completion:completion];
    }
    else {
        [self geocodeAPICall:position mapType:CMapTypeNone completion:completion];
    }
}

-(void)geocodeAPICall:(CLLocationCoordinate2D)position
              mapType:(CMapType)mapType
           completion: (user_completion_block)completion
{
    
    if (![GoogleUtils isLocationValid:position]) {
        NSLog(@"geocodeAPICall Invalid : %f,%f",position.latitude,position.longitude);
        return;
    }
    
    if (mapType == CMapTypeNone) {
        mapType = self.mapType;
    }
    @try{
        if (mapType == CMapTypeTmaps) {

            NSString *url_String = [NSString stringWithFormat:@"https://api2.sktelecom.com/tmap/geo/reversegeocoding?version=1"];
            
            NSMutableDictionary *parameters=[[NSMutableDictionary alloc]init];
            [parameters setValue:[NSString stringWithFormat:@"%f",position.longitude] forKey:@"lon"];
            [parameters setValue:[NSString stringWithFormat:@"%f",position.latitude] forKey:@"lat"];
            [parameters setValue:@"WGS84GEO" forKey:@"coordType"];
            [parameters setValue:@"A04" forKey:@"addressType"];
            //[parameters setValue:_TMAP_API_KEY forKey:@"appKey"];
            [parameters setValue:[NSString stringWithFormat:@"%lu",(unsigned long)CMapTypeTmaps] forKey:@"mapType"];
            
            [self callGetWebService:url_String method:@"GET" andDictionary:parameters completion:^(NSDictionary* responseDict, NSString*params, NSError*error, long code)
             {
                 if(error)
                 {
                     if(completion)
                     {
                         completion(responseDict,params,@"OOPS Seems like something is wrong with server",-1);
                     }
                 }
                 else{
                     if(completion)
                     {
                         if ([GoogleUtils isDictionary:responseDict[@"addressInfo"]]) {
                             NSString *address = responseDict[@"addressInfo"][@"fullAddress"];
                             address = [GoogleUtils replaceStr:address search:responseDict[@"addressInfo"][@"city_do"] replace:@""];
                             NSDictionary *addressDic = @{@"fullAddress":address};
                             completion(addressDic,params,@"Got Response ",1);
                         }
                         else {
                             completion(responseDict,params,@"OOPS Seems like something is wrong with server",-1);
                         }
                     }
                 }
             }];

        }
        else if (mapType == CMapTypeWitz) {
        }
        else if (mapType == CMapTypeGoogleMaps) {
            //https://maps.googleapis.com/maps/api/geocode/json?address=1600+Amphitheatre+Parkway,+Mountain+View,+CA&key=AIzaSyBh5P0FtiZS-jmiUbXYFWBysJA8_H9ptkA
            NSString *url_String = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/geocode/json"];
            
            NSMutableDictionary *parameters=[[NSMutableDictionary alloc]init];
            [parameters setValue:[NSString stringWithFormat:@"%f,%f",position.latitude,position.longitude] forKey:@"address"];
            [parameters setValue:_GOOGLE_API_KEY forKey:@"key"];
            [parameters setValue:[NSString stringWithFormat:@"%lu",(unsigned long)CMapTypeGoogleMaps] forKey:@"mapType"];
            
            [self callGetWebService:url_String method:@"GET" andDictionary:parameters completion:^(NSDictionary* responseDict, NSString*params, NSError*error, long code)
             {
                 if(error)
                 {
                     if(completion)
                     {
                         completion(responseDict,params,@"OOPS Seems like something is wrong with server",-1);
                     }
                 }
                 else{
                     if(completion)
                     {
                         if ([responseDict[@"status"] isEqualToString:@"OK"]) {
                             NSString *address = responseDict[@"results"][0][@"formatted_address"];
                             NSString *search = @"";
                             NSString *search2 = @"";
                             //NSDictionary *dic =responseDict[@"results"][0][@"address_components"][0];
                             for (NSDictionary *dic in responseDict[@"results"][0][@"address_components"]) {
                                 NSArray *array = dic[@"types"];
                                 if ([array containsObject: @"country"]) // YES
                                 {
                                     // Do something
                                     search = dic[@"long_name"];
                                     if (![search isEqualToString:@""] && ![search2 isEqualToString:@""]) {
                                         break;
                                     }
                                 }
                                 else if ([array containsObject: @"administrative_area_level_1"]) // YES
                                 {
                                     // Do something
                                     search2 = dic[@"long_name"];
                                     if (![search isEqualToString:@""] && ![search2 isEqualToString:@""]) {
                                         break;
                                     }
                                 }
                             }
                             if (![search isEqualToString:@""]) {
                                 address = [GoogleUtils replaceStr:address search:search replace:@""];
                             }
                             if (![search2 isEqualToString:@""]) {
                                 address = [GoogleUtils replaceStr:address search:search2 replace:@""];
                             }
                             NSDictionary *addressDic = @{@"fullAddress":address};
                             completion(addressDic,params,@"Got Response ",1);
                         }
                         else {
                             completion(responseDict,params,@"OOPS Seems like something is wrong with server",-1);
                         }
                     }
                 }
             }];
        }
    } @catch (NSException *exception) {
        NSLog(@"Exception At: %s %d %s %s %@", __FILE__, __LINE__, __PRETTY_FUNCTION__, __FUNCTION__,exception);
    }
}
/*
-(void)POIAPICall:(NSString*)address
              mapType:(CMapType)mapType
           completion: (user_completion_block)completion
{
    @try{
        if (mapType == CMapTypeTmaps) {

            dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                
                [self SKPMapAuth];
                
                TMapPathData* path = [[TMapPathData alloc] init];
                
                //통합검색
                NSArray* results = [path requestFindAllPOI:searchString resultCount:5];
                
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    if (isNull) {
                        return;
                    }
                    
                    if(results.count>0){
                        [self.matchedAddress removeAllObjects];
                        NSLog(@"result.count:%d",(int)results.count);
                        for(TMapPOIItem* result in results)
                        {
                            [self.matchedAddress addObject:result];
                        }
                        
                        NSLog(@"result.count2:%d",(int)self.matchedAddress.count);
                        
                        [self onReloadResultController];
                    }
                });
                
            });
        }
            
            NSMutableDictionary *parameters=[[NSMutableDictionary alloc]init];
            [parameters setValue:address forKey:@"searchKeyword"];
            [parameters setValue:@"5" forKey:@"count"];
            [parameters setValue:@"1" forKey:@"page"];
            [parameters setValue:@"WGS84GEO" forKey:@"reqCoordType"];
            [parameters setValue:@"WGS84GEO" forKey:@"resCoordType"];
            [parameters setValue:@"TMAP" forKey:@"mapType"];
            
            [self callGetWebService:url_String method:@"GET" andDictionary:parameters completion:^(NSDictionary* responseDict, NSString*params, NSError*error, long code)
             {
                 if(error)
                 {
                     if(completion)
                     {
                         completion(responseDict,params,@"OOPS Seems like something is wrong with server",-1);
                     }
                 }
                 else{
                     if(completion)
                     {
                         if ([GoogleUtils isDictionary:responseDict[@"addressInfo"]]) {
                             NSDictionary *addressDic = @{@"fullAddress":@""};
                             completion(addressDic,params,@"Got Response ",1);
                         }
                         else {
                             completion(responseDict,params,@"OOPS Seems like something is wrong with server",-1);
                         }
                     }
                 }
             }];
            
        }
        else if (mapType == CMapTypeGoogleMaps) {
            GMSVisibleRegion visibleRegion = self.mapView.projection.visibleRegion;
            GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithCoordinate:visibleRegion.farLeft
                                                                               coordinate:visibleRegion.nearRight];
            GMSAutocompleteFilter *filter = [[GMSAutocompleteFilter alloc] init];
            //filter.type = kGMSPlacesAutocompleteTypeFilterAddress;
            filter.type = kGMSPlacesAutocompleteTypeFilterNoFilter;
            
            //if (!appDelegate.isKorea) {
            filter.country = self->country;
            //}
            
            [_placesClient autocompleteQuery:searchString
                                      bounds:bounds
                                      filter:filter
                                    callback:^(NSArray *results, NSError *error) {
                                        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                                        if (error != nil) {
                                            if(completion)
                                            {
                                                completion(responseDict,params,[NSString stringWithFormat:@"Autocomplete failed with error: %@", error.localizedDescription],-1);
                                            }
                                        }
                                        if(results.count>0){
                                            if (isNull) {
                                                return;
                                            }
                                            [self.matchedAddress removeAllObjects];
                                            NSLog(@"result.count:%d",(int)results.count);
                                            for (GMSAutocompletePrediction* result in results) {
                                                [self.matchedAddress addObject:result];
                                            }
                                            NSLog(@"result.count2:%d",(int)self.matchedAddress.count);
                                            
                                            [self onReloadResultController];
                                        }
                                    }];
            
            
                 if(error)
                 {
                     if(completion)
                     {
                         completion(responseDict,params,@"OOPS Seems like something is wrong with server",-1);
                     }
                 }
                 else{
                     if(completion)
                     {
                         if ([responseDict[@"status"] isEqualToString:@"OK"]) {
                             completion(responseDict[@"results"][0],params,@"Got Response ",1);
                         }
                         else {
                             completion(responseDict,params,@"OOPS Seems like something is wrong with server",-1);
                         }
                     }
                 }
             }];
        }
    } @catch (NSException *exception) {
        NSLog(@"Exception At: %s %d %s %s %@", __FILE__, __LINE__, __PRETTY_FUNCTION__, __FUNCTION__,exception);
    }
}
 */

-(void)POIAPICall:(NSString*)address
       completion: (user_completion_block)completion
{
    if (self.isKorea) {
        [self POIAPICall:address mapType:CMapTypeTmaps completion:completion];
    }
    else {
        [self POIAPICall:address mapType:CMapTypeNone completion:completion];
    }
}


-(void)POIAPICall:(NSString*)address
          mapType:(CMapType)mapType
       completion: (user_completion_block)completion
{
    if (mapType == CMapTypeNone) {
        mapType = self.mapType;
    }
    @try{
        if (mapType == CMapTypeTmaps) {

            NSString *url_String = [NSString stringWithFormat:@"https://api2.sktelecom.com/tmap/pois?version=1"];
            
            NSMutableDictionary *parameters=[[NSMutableDictionary alloc]init];
            [parameters setValue:address forKey:@"searchKeyword"];
            [parameters setValue:@"5" forKey:@"count"];
            [parameters setValue:@"1" forKey:@"page"];
//            [parameters setValue:@"R" forKey:@"searchtypCd"]; // 거리순
//            [parameters setValue:@"30" forKey:@"radius"];
//            [parameters setValue:[NSString stringWithFormat:@"%f",self.realTimeCLocation.coordinate.latitude] forKey:@"centerLat"];
//            [parameters setValue:[NSString stringWithFormat:@"%f",self.realTimeCLocation.coordinate.longitude] forKey:@"centerLon"];
            [parameters setValue:@"WGS84GEO" forKey:@"reqCoordType"];
            [parameters setValue:@"WGS84GEO" forKey:@"resCoordType"];
            [parameters setValue:[NSString stringWithFormat:@"%lu",(unsigned long)CMapTypeTmaps] forKey:@"mapType"];
            
            [self callGetWebService:url_String method:@"GET" andDictionary:parameters completion:^(NSDictionary* responseDict, NSString*params, NSError*error, long code)
             {
                 if(error)
                 {
                     if(completion)
                     {
                         completion(responseDict,params,@"OOPS Seems like something is wrong with server",-1);
                     }
                 }
                 else{
                     if(completion)
                     {
                         if ([GoogleUtils isDictionary:responseDict[@"searchPoiInfo"]]) {
                             NSMutableArray *addresses = [NSMutableArray array];
                             for (NSDictionary *dic in responseDict[@"searchPoiInfo"][@"pois"][@"poi"]) {
                                 NSString *address = [NSString stringWithFormat:@"%@ %@ %@",dic[@"upperAddrName"],dic[@"middleAddrName"],dic[@"lowerAddrName"]];
                                 
                                 [addresses addObject:@{@"name":dic[@"name"], @"address":address, @"lat":dic[@"frontLat"], @"lon":dic[@"frontLon"]}];
                             }
                             NSDictionary *addressDic = @{@"poi":addresses};
                             completion(addressDic,params,@"Got Response ",1);
                         }
                         else {
                             completion(responseDict,params,@"OOPS Seems like something is wrong with server",-1);
                         }
                     }
                 }
             }];
            
        }
        else if (mapType == CMapTypeGoogleMaps) {
            //https://maps.googleapis.com/maps/api/geocode/json?address=1600+Amphitheatre+Parkway,+Mountain+View,+CA&key=YOUR_API_KEY
            NSString *url_String = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/autocomplete/json"];
            
            NSMutableDictionary *parameters=[[NSMutableDictionary alloc]init];
            [parameters setValue:address forKey:@"input"];
            [parameters setValue:@"establishment" forKey:@"types"];
            [parameters setValue:[NSString stringWithFormat:@"%f,%f",self.realTimeCLocation.coordinate.latitude,self.realTimeCLocation.coordinate.longitude] forKey:@"location"];
            [parameters setValue:@"500" forKey:@"radius"];
            [parameters setValue:_GOOGLE_API_KEY forKey:@"key"];
            [parameters setValue:[NSString stringWithFormat:@"%lu",(unsigned long)CMapTypeGoogleMaps] forKey:@"mapType"];
            
            [self callGetWebService:url_String method:@"GET" andDictionary:parameters completion:^(NSDictionary* responseDict, NSString*params, NSError*error, long code)
             {
                 if(error)
                 {
                     if(completion)
                     {
                         completion(responseDict,params,@"OOPS Seems like something is wrong with server",-1);
                     }
                 }
                 else{
                     if(completion)
                     {
                         if ([responseDict[@"status"] isEqualToString:@"OK"]) {
                             NSMutableArray *addresses = [NSMutableArray array];
                             for (NSDictionary *dic in responseDict[@"predictions"]) {
                                 
                                 //Convert the contents of the outer dictionary to a mutable array
                                 NSMutableArray *dictValues = [dic[@"terms"] mutableCopy];
                                 [dictValues sortUsingComparator: (NSComparator)^(NSDictionary *a, NSDictionary *b)
                                  {
                                      NSString *key1 = [a objectForKey: @"offset"];
                                      NSString *key2 = [b objectForKey: @"offset"];
                                      return [key1 compare: key2];
                                  }
                                  ];
                                 NSInteger cnt = [dictValues count];
                                 if (cnt>=4) {
                                     cnt = 3;
                                 }
                                 NSString *address = @"";
                                 for (int i=1; i<=cnt; i++) {
                                     address = [address stringByAppendingString:dictValues[i][@"value"]];
                                     address = [address stringByAppendingString:@" "];
                                 }
                                 
                                 [addresses addObject:@{@"placeID":dic[@"place_id"],@"name":dic[@"structured_formatting"][@"main_text"], @"address":address}];
                             }
                             NSDictionary *addressDic = @{@"poi":addresses};
                             completion(addressDic,params,@"Got Response ",1);
                         }
                         else {
                             completion(responseDict,params,@"OOPS Seems like something is wrong with server",-1);
                         }
                     }
                 }
             }];
        }
    } @catch (NSException *exception) {
        NSLog(@"Exception At: %s %d %s %s %@", __FILE__, __LINE__, __PRETTY_FUNCTION__, __FUNCTION__,exception);
    }
}

#ifdef _DRIVER_MODE
-(BOOL)openNavigationApp:(CMMapApp)mapApp destAddr:(NSString*)mapDestAddr coord:(CLLocationCoordinate2D)coordinate {
    
    if (self.isKorea) {
        BOOL installed = [CMMapLauncher isMapAppInstalled:CMMapAppTmap];
        if (!installed) {
            return NO;
        }
        [CMMapLauncher launchMapApp:CMMapAppTmap
                    forDirectionsTo:[CMMapPoint mapPointWithAddress:mapDestAddr
                                                         coordinate:coordinate]];
    }
    else {
        BOOL installed = [CMMapLauncher isMapAppInstalled:mapApp];
        if (!installed) {
            return NO;
        }
        
        if (mapApp == CMMapAppGoogleMaps) {
            [CMMapLauncher launchMapApp:mapApp
                        forDirectionsTo:[CMMapPoint mapPointWithCoordinate:coordinate]];
        }
        else {
            [CMMapLauncher launchMapApp:mapApp
                        forDirectionsTo:[CMMapPoint mapPointWithName:mapDestAddr
                                                          coordinate:coordinate]];
        }
    }
    return YES;
    
    //    dispatch_async(dispatch_get_main_queue(), ^{
    //        // Update the UI
    //        NSString *escaped = [GoogleUtils escapeValueForURLParameter:@"행신역"];
    //        NSString *url = [NSString stringWithFormat:@"tmap://?rGoName=%@&rGoX=%@&rGoY=%@", escaped,@"126.831865",@"37.6118502"];
    //        //NSString *url = [NSString stringWithFormat:@"tmap://search="];
    //        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
    //    });
    
}
#endif

-(void)callGetWebService:(NSString *)urlStr
                  method:(NSString*)method
           andDictionary:(NSDictionary *)parameter
              completion:(completion_handler_t)completion{
    
    // cache
    NSURLRequestCachePolicy cachePolicy = NSURLRequestReloadIgnoringCacheData;
    //cachePolicy = NSURLRequestReturnCacheDataElseLoad;
    
    // timeout
    int timeOutSecs = 20;
    
    NSURL* pathURL = [NSURL URLWithString:urlStr];
    
    // request
    NSMutableURLRequest *httpRequest = [[NSMutableURLRequest alloc] initWithURL: pathURL
                                                                    cachePolicy: cachePolicy
                                                                timeoutInterval: timeOutSecs];
    
    [httpRequest setHTTPMethod: method];
    NSString* accept = [httpRequest valueForHTTPHeaderField:HDR_ACCEPT];
    if ( accept == nil ) {
        [httpRequest addValue:@"application/json" forHTTPHeaderField:HDR_ACCEPT];
    }
   
    // parameter 직렬화
    NSString *params = [GoogleUtils serializeParams:parameter];
    
    // method 별 parameter 적용
    if ([method isEqualToString:@"POST"]) {
        //default content type to json if it wasn't set by headers already
        NSString* ctype = [httpRequest valueForHTTPHeaderField:HDR_CONTENTTYPE];
        if ( ctype == nil )
            [httpRequest addValue:@"application/json" forHTTPHeaderField:HDR_CONTENTTYPE];
        
        if ([params length] > 0)
        {
            NSString *rLen = [NSString stringWithFormat:@"%lu", (unsigned long)[params length]];
            [httpRequest addValue:rLen forHTTPHeaderField:HDR_CONTENTLEN];
            
            [httpRequest setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
        }
    }
    else {
        NSURL* pathURL = [NSURL URLWithString:[GoogleUtils appendUrl:urlStr param:params]];
        [httpRequest setURL:pathURL];
    }
    
    if ( [parameter[@"mapType"] integerValue] == CMapTypeTmaps) {
        [httpRequest setValue:_TMAP_API_KEY forHTTPHeaderField:@"appKey"];

        [httpRequest setValue:@"application/json; charset=UTF-8" forHTTPHeaderField:HDR_CONTENTTYPE];
    }
    
    NSLog(@"\n[URL   ] %@\n[Header] %@\n[Body  ] %@\n",
          httpRequest.URL.absoluteString,
          httpRequest.allHTTPHeaderFields,
          [[NSString alloc] initWithData:httpRequest.HTTPBody encoding:NSUTF8StringEncoding]);
    
//    NSURL *URL = [NSURL URLWithString:urlString];
    //NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    NSURLSessionDataTask *task =
    [[NSURLSession sharedSession] dataTaskWithRequest:httpRequest
                                    completionHandler:^(NSData *data,
                                                        NSURLResponse *response,
                                                        NSError *error) {
                                        // Code to run when the response completes...
//                                        if ([NSThread isMainThread]){
//                                            NSLog(@"0In main thread--completion handler");
//                                        }
//                                        else{
//                                            NSLog(@"0Not in main thread--completion handler");
//                                        }
                                        
                                        if (error) {
                                            //log error here if the request had no error handler
                                            if ([error code]) {
                                                NSString *errorStr = [NSString stringWithFormat:@"%@ - %@ %ld %@", @"HTTP Get Error", [error domain], (long)[error code], [error localizedDescription]];
                                                NSLog(@"%@", errorStr);
                                            }
                                            
                                            if (completion) {
                                                long code=(long)[error code];
                                                if (data == nil) {
                                                    completion(nil, params, nil,code);
                                                    return;
                                                }
                                                NSMutableData *receivedData = (NSMutableData*)data;
                                                
                                                id jsonResponse = [NSJSONSerialization JSONObjectWithData:receivedData options:0 error:nil];
                                                NSDictionary* json = jsonResponse;
                                                completion([json mutableCopy], params, nil,code);
                                            }
                                        }
                                        else {
                                            int statusCode = (int)[(NSHTTPURLResponse *)response statusCode];
                                            if (statusCode != 200)
                                            {
                                                NSString* statusMsg    = [NSHTTPURLResponse localizedStringForStatusCode:statusCode];
                                                
                                                NSString *errorStr = [NSString stringWithFormat:@"Async Req To Url [%d] - received response with error: %@", statusCode, statusMsg];
                                                NSLog(@"%@", errorStr);
                                            }
                                            else {
                                                // 받은 header들을 dictionary형태로 받고
                                                //NSDictionary *responseParameters = [(NSHTTPURLResponse *)response allHeaderFields];
                                                
                                                if (data == nil) {
                                                    if (completion) {
                                                        long code=405;
                                                        completion(nil, params, nil,code);
                                                    }
                                                    
                                                    return;
                                                }
                                                
                                                NSMutableData *receivedData = (NSMutableData*)data;
                                                
                                                id jsonResponse = [NSJSONSerialization JSONObjectWithData:receivedData options:0 error:nil];
                                                
                                                /*
                                                NSString *convertedString = [NSString stringWithFormat:@"%@", [jsonResponse description]];
                                                const char *cString = [convertedString cStringUsingEncoding:NSUTF8StringEncoding];
                                                NSString *resultStr = [NSString stringWithCString:cString encoding:NSNonLossyASCIIStringEncoding];
                                                
                                                NSLog(@"receive Data - : %@",resultStr);
                                                 */
                                                
//                                                if ([NSThread isMainThread]){
//                                                    NSLog(@"1In main thread--completion handler");
//                                                }
//                                                else{
//                                                    NSLog(@"1Not in main thread--completion handler");
//                                                }
                                                
                                                if (completion) {
                                                    long code=200;
                                                    NSDictionary* json = jsonResponse;
                                                    completion([json mutableCopy], params, nil,code);
                                                }
                                            }
                                        }
                                    }];
    [task resume];
}

- (void)requestDirection:(CLLocationCoordinate2D)spoint
                  endPos:(CLLocationCoordinate2D)epoint
            andWaypoints:(CLLocationCoordinate2D)waypoints
                andParam:(NSMutableDictionary* _Nullable)param
              completion:(request_completion_block)completion
{
    if (self.isKorea) {
        [self requestDirection:spoint endPos:epoint andWaypoints:waypoints andParam:param andMapType:CMapTypeTmaps completion:completion];
    }
    else {
        [self requestDirection:spoint endPos:epoint andWaypoints:waypoints andParam:param andMapType:CMapTypeNone completion:completion];
    }
}

- (void)requestDirection:(CLLocationCoordinate2D)spoint
                  endPos:(CLLocationCoordinate2D)epoint
            andWaypoints:(CLLocationCoordinate2D)waypoints
                andParam:(NSMutableDictionary* _Nullable)param
              andMapType:(CMapType)mapType
              completion:(request_completion_block)completion
{
    NSDate *chkTime = [NSDate date];
    
    if (![GoogleUtils isLocationValid:spoint]) {
        NSLog(@"requestDirection Invalid spoint : %f,%f",spoint.latitude,spoint.longitude);
        return;
    }
    
    if (![GoogleUtils isLocationValid:epoint]) {
        NSLog(@"requestDirection Invalid epoint : %f,%f",epoint.latitude,epoint.longitude);
        return;
    }
    
    NSTimeInterval executionTime = [chkTime timeIntervalSinceDate:self.mapApilastRunTime]/60*60; //초
    if (executionTime<5) { // 5초 이내 다시 요청하지 않음
        NSLog(@"requestDirection[NOK]: executionTime = %f", executionTime);
        return;
    }
    NSLog(@"executionTime = %f", executionTime);
    
    // api요청 시간 저장
    _mapApilastRunTime = [NSDate date];
    
    if (mapType == CMapTypeNone) {
        mapType = self.mapType;
    }
    
    @autoreleasepool {
        @try{
            if (mapType == CMapTypeTmaps) {

                if ([GoogleUtils isLocationValid:waypoints]) {
                    
                    NSString *waypoint = [NSString stringWithFormat:@"%f,%f",
                                          waypoints.longitude,
                                          waypoints.latitude];
                    if (param == nil) {
                        param = (NSMutableDictionary*)@{@"passList": waypoint};
                    }
                    else {
                        [param setValue:waypoint forKey:@"passList"];
                    }
                }
                
                [self tMapsAPICall:spoint
                            endPos:epoint
                          andParam:param
                        completion:^(NSDictionary *user, NSString* params, NSString *str, int status) {
                            
                            [self responseApiCallback:user params:params str:str status:status completion:completion];
                            
                        }];
            }
            else if (mapType == CMapTypeGoogleMaps) {
                
                if ([GoogleUtils isLocationValid:waypoints]) {
                    
                    NSString *waypoint = [NSString stringWithFormat:@"%f,%f",
                                          waypoints.latitude,
                                          waypoints.longitude];
                    if (param == nil) {
                        param = (NSMutableDictionary*)@{@"waypoints": waypoint};
                    }
                    else {
                        [param setValue:waypoint forKey:@"waypoints"];
                    }
                }
                
                [self googleMapsAPICall:spoint
                                 endPos:epoint
                               andParam:param
                             completion:^(NSDictionary *user, NSString* params, NSString *str, int status) {
                                 
                                 [self responseApiCallback:user params:params str:str status:status completion:completion];
                                 
                             }];
            }
            else if (mapType == CMapTypeWitz) {
                
                if ([GoogleUtils isLocationValid:waypoints]) {
                    
                    NSString *waypoint = [NSString stringWithFormat:@"%f,%f",
                                          waypoints.latitude,
                                          waypoints.longitude];
                    if (param == nil) {
                        param = (NSMutableDictionary*)@{@"waypoint": waypoint};
                    }
                    else {
                        [param setValue:waypoint forKey:@"waypoint"];
                    }
                }
                
                [self witzMapsAPICall:spoint
                               endPos:epoint
                             andParam:param
                           completion:^(NSDictionary *user, NSString* params, NSString *str, int status) {
                               
                               [self responseApiCallback:user params:params str:str status:status completion:completion];
                               
                           }];
            }
            
        }
        @catch (NSException *exception) {
            NSLog(@"Exception At: %s %d %s %s %@", __FILE__, __LINE__, __PRETTY_FUNCTION__, __FUNCTION__,exception);
        }
    }
}

- (void)responseApiCallback:(NSDictionary *)user
                     params:(NSString*)params
                        str:(NSString *)str
                     status:(int)status
                 completion:(request_completion_block)completion
{
    NSDictionary *paramDic = [GoogleUtils getDataOfQueryString:params];
    if (status == 1) {
        if ([paramDic[@"mapType"] integerValue] == CMapTypeTmaps) {
            if ([paramDic valueForKey:@"passList"]) {
                user = [self formatTmapToGoogleJSON:user passList:YES];
            }
            else {
                user = [self formatTmapToGoogleJSON:user passList:NO];
            }
        }
        else if ([paramDic[@"mapType"] integerValue] == CMapTypeWitz) {
            user = [self formatWitzToGoogleJSON:user];
        }
        
        //NSLog(@"user:%@",user);
        if (![user[@"status"] isEqualToString:@"OK"]) {
            // 응답없음.
            NSLog(@"API Error: status:%@", user[@"status"]);
            return;
        }
        
        [self setFeatures:[user copy]];
        //locMgr.features = [user copy];
        NSArray *routesArray = user[@"routes"];
        NSArray *legs = routesArray[0][@"legs"];
        NSInteger legCount = [legs count];
        NSLog(@"legCount:%d",(int)legCount);
        
        for (int l = 0; l < legCount; l++)
        {
            NSArray *steps = legs[l][@"steps"];
            NSInteger count = [steps count];
            
            // 신규데이터가 존재하면 이전 데이터를 지운다.
            if (count && l == 0) {
                [self.steps removeAllObjects];
                [self.paths removeAllCoordinates];
            }
            
            for (int i = 0; i < count; i++)
            {
                if ([paramDic[@"mapType"] integerValue] == CMapTypeTmaps) {
                    for (CLLocation *point in steps[i][@"polyline"]) {
                        [self.paths addCoordinate:point.coordinate];
                    }
                    [self.steps addObject:steps[i]];
                }
                else if ([paramDic[@"mapType"] integerValue] == CMapTypeWitz) {
                    for (CLLocation *point in steps[i][@"polyline"]) {
                        [self.paths addCoordinate:point.coordinate];
                    }
                    [self.steps addObject:steps[i]];
                }
                else { // google
                    GMSPath *path = [GMSPath pathFromEncodedPath:steps[i][@"polyline"][@"points"]];
                    NSMutableArray *substep = [NSMutableArray array];
                    for (int j = 0; j<path.count; j++) {
                        [self.paths addCoordinate:[path coordinateAtIndex:j]];
                        
                        CLLocation *pointLocation = [[CLLocation alloc] initWithLatitude:[path coordinateAtIndex:j].latitude
                                                                               longitude:[path coordinateAtIndex:j].longitude];
                        
                        [substep addObject:pointLocation];
                    }
                    
                    NSMutableDictionary *dic = [steps[i] mutableCopy];
                    dic[@"polyline"] = substep;
                    
                    [self.steps addObject:dic];
                }
            }
        }
        
        //double totalDistance = [GoogleUtils findTotalDistanceOfPath:paths];
        dispatch_async(dispatch_get_main_queue(), ^{
            // Update the UI
            
//            [self drawTripLine];
//
//            NSString *directionName = [GoogleUtils directionNameFromCompassValue:locMgr.lastDirection];
//            strongSelf.timeoutLabel.text = [NSString stringWithFormat:@"%@ (%@°)", directionName, @(locMgr.lastDirection)];
            
            //NSLog(@"steps:%@",locMgr.steps);
        });
    }
    else {
    }
    
    // 초기값 세팅
    NSDictionary *legs = [self distanceOffeature];
    int total_distance = [legs[@"distance"] intValue];
    int total_duration = [legs[@"duration"] intValue];
    self.distancematrix = @{
                            @"remain_distance":[NSString stringWithFormat:@"%d", total_distance],
                            @"remain_duration":[NSString stringWithFormat:@"%d", total_duration]
                            };
//    if (self.distancematrix == nil) {
//
//    }
//    else {
//        [self.distancematrix setValue:[NSString stringWithFormat:@"%d", total_distance] forKey:@"remain_distance"];
//        [self.distancematrix setValue:[NSString stringWithFormat:@"%d", total_duration] forKey:@"remain_duration"];
//    }
    
    
    //dispatch_async(dispatch_get_main_queue(), ^{
        // Update the UI
        completion(paramDic, status);
    //});
    
}

#pragma mark - Timer

-(void)initTimer {
    self.timer = [MSWeakTimer scheduledTimerWithTimeInterval:1
                                                      target:self
                                                    userInfo:nil
                                                     repeats:YES
                                               dispatchQueue:dispatch_get_main_queue()
                                                       block:^(){
                                                           
                                                           NSAssert([NSThread isMainThread], @"This should be called from the main thread");
                                                           NSLog(@"timer");
                                                       }];
    
    [self.timer fire];
}

-(void)stopTimer {
    [self.timer invalidate];
}

#pragma mark - LocationUtils

- (NSString *)getLocationErrorDescription:(INTULocationStatus)status
{
    if (status == INTULocationStatusServicesNotDetermined) {
        return @"Error: User has not responded to the permissions alert.";
    }
    if (status == INTULocationStatusServicesDenied) {
        return @"Error: User has denied this app permissions to access device location.";
    }
    if (status == INTULocationStatusServicesRestricted) {
        return @"Error: User is restricted from using location services by a usage policy.";
    }
    if (status == INTULocationStatusServicesDisabled) {
        return @"Error: Location services are turned off for all apps on this device.";
    }
    return @"An unknown error occurred.\n(Are you using iOS Simulator with location set to 'None'?)";
}

-(NSDictionary*)startAndEndOffeature {
    NSDictionary *dic = @{
                          @"start_location":self.features[@"routes"][0][@"legs"][@"start_location"],
                          @"end_location":self.features[@"routes"][0][@"legs"][@"end_location"],
                          };
    return dic;
}

-(NSDictionary*)distanceOffeature {
    
    NSArray *routesArray = self.features[@"routes"];
    NSArray *legs = routesArray[0][@"legs"];
    NSInteger legCount = [legs count];
    NSInteger distance_val = 0;
    NSInteger duration_val = 0;
    
    for (int l = 0; l < legCount; l++)
    {
        NSDictionary *distance = legs[l][@"distance"];
        distance_val += [[NSString stringWithFormat:@"%@", distance[@"value"]] integerValue];
        
        NSDictionary *duration = legs[l][@"duration"];
        duration_val += [[NSString stringWithFormat:@"%@", duration[@"value"]] integerValue];
    }
    
    return @{@"distance" : [NSString stringWithFormat:@"%d",(int)distance_val],
             @"duration" : [NSString stringWithFormat:@"%d",(int)duration_val],
             @"edistance" : [NSString stringWithFormat:@"%d",(int)distance_val],
             @"eduration" : [NSString stringWithFormat:@"%d",(int)duration_val],
             };
}

-(NSArray*)findBestIndexOfSteps:(NSInteger)index originPoint:(CGPoint)originPoint{
    double bestDistance = DBL_MAX;
    CLLocationCoordinate2D bestCoord = kCLLocationCoordinate2DInvalid;
    
    CGPoint bestPoint = CGPointZero;
    NSInteger bestIndex   = 0;
    NSInteger bestIndexJ  = 0;
    
    NSInteger count = [self.steps count];
    
    for (; index < count ; index++)
    {
        // 찾지 못하면 sub polyline을 비교한다.
        NSInteger pointCount = [self.steps[index][@"polyline"] count];
        int j=0;
        
        for (; j<pointCount-1; j++) {
            CLLocation *pointLocation1 =  self.steps[index][@"polyline"][j];
            CLLocation *pointLocation2 =  self.steps[index][@"polyline"][j+1];
            
            CLLocationCoordinate2D startCoordinate = pointLocation1.coordinate;
            CGPoint startPoint = CGPointMake(startCoordinate.longitude, startCoordinate.latitude);
            CLLocationCoordinate2D endCoordinate = pointLocation2.coordinate;
            CGPoint endPoint = CGPointMake(endCoordinate.longitude, endCoordinate.latitude);
            
            double distance;
            CGPoint point = [self nearestPointToPoint:originPoint onLineSegmentPointA:startPoint pointB:endPoint distance:&distance];
            
            CLLocationDistance newDistance1 =  GMSGeometryDistance(startCoordinate,CLLocationCoordinate2DMake(point.y, point.x));
            CLLocationDistance newDistance2 =  GMSGeometryDistance(endCoordinate,CLLocationCoordinate2DMake(point.y, point.x));
            
            if (distance < bestDistance) {
                bestDistance = distance;
                
                if (newDistance1 < newDistance2) {
                    bestCoord = startCoordinate;
                    bestIndex = index;
                    bestIndexJ = j;
                }
                else {
                    bestCoord = endCoordinate;
                    bestIndex = index;
                    bestIndexJ = j;
                }
                
                bestPoint = point;
                //bestPoint = CGPointMake(bestCoord.longitude, bestCoord.latitude);
                bestIndex = index;
            }
        }
    }
    
    NSArray *points = [NSArray arrayWithObjects:
                       [NSValue valueWithCGPoint:bestPoint],
                       [NSString stringWithFormat:@"%ld",(long)bestIndex],
                       [NSString stringWithFormat:@"%ld",(long)bestIndexJ],
                       [NSString stringWithFormat:@"%f",bestDistance],
                       [NSValue valueWithCGPoint:CGPointMake(bestCoord.longitude, bestCoord.latitude)],
                       
                       nil];
    
    return points;
}

/*
 * polyline과 가장 가까운 점을 찾는다.
 * 이떄 가장 가까운 점이 현재위치와 50m 이상이면 처음부터 polyline을 다시 찾아보고 가장 가까운 점을 찾는다.
 */
-(NSDictionary*)nearestPolylineLocationToCoordinate:(CLLocationCoordinate2D)coordinate {
    double bestDistance = DBL_MAX;
    CLLocationCoordinate2D bestCoord = kCLLocationCoordinate2DInvalid;
    CGPoint bestPoint;
    CGPoint beforePoint = CGPointZero;
    CGPoint afterPoint = CGPointZero;
    //BOOL bestFlag = NO;
    CGPoint originPoint = CGPointMake(coordinate.longitude, coordinate.latitude);
    
    NSInteger beforeIndex = -1;
    NSInteger bestIndex   = 0;
    NSInteger bestIndexJ  = 0;
    NSInteger afterIndex  = -1;
    
    double dist = 0;
    double durt = 0;
    
    //NSDictionary *legs = [GoogleUtils distanceOffeature];
    //INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    
    //NSInteger index = locMgr.bestIndex;
    //index = stepsIndex;
    
    //NSLog(@"locMgr.bestIndex:%ld",(long)locMgr.bestIndex+1);
    
    // 이전 위치에서부터 가장 가짜운 점을 찾는다.
    NSArray *fpoints = [self findBestIndexOfSteps:self.bestIndex originPoint:originPoint];
    
    bestPoint   = [[fpoints objectAtIndex:0] CGPointValue];
    bestIndex   = [[fpoints objectAtIndex:1] integerValue];
    bestIndexJ  = [[fpoints objectAtIndex:2] integerValue];
    bestDistance= [[fpoints objectAtIndex:3] doubleValue];
    
    if (![GoogleUtils isArray:self.steps[bestIndex][@"polyline"]]) {
        return nil;
    }
    bestCoord   = [self.steps[bestIndex][@"polyline"][bestIndexJ] coordinate];
    
    // 현재위치와 bestpoint와의 거리
    CLLocationDistance newDistance =  GMSGeometryDistance(coordinate,CLLocationCoordinate2DMake(bestPoint.y, bestPoint.x));
    // best가 50m를 넘거나 현재위치와 best가 50을 넘으면 처음부터 다시 찾는다.
    //if (bestDistance > 50 || newDistance > 50) {
    if (newDistance > OUT_OF_DISTANCE) {
        NSLog(@"over %fl",newDistance);
        NSArray *fpoints = [self findBestIndexOfSteps:0 originPoint:originPoint];
        
        bestPoint   = [[fpoints objectAtIndex:0] CGPointValue];
        bestIndex   = [[fpoints objectAtIndex:1] integerValue];
        bestIndexJ  = [[fpoints objectAtIndex:2] integerValue];
        bestDistance= [[fpoints objectAtIndex:3] doubleValue];
        bestCoord   = [self.steps[bestIndex][@"polyline"][bestIndexJ] coordinate];
    }
    
    // 현재위치와 bestpoint와의 거리 다시 계산
    newDistance =  GMSGeometryDistance(coordinate,CLLocationCoordinate2DMake(bestPoint.y, bestPoint.x));
    
    self.bestIndex  = bestIndex;
    self.bestIndexJ = bestIndexJ;
    
    // steps 의 위치를 찾는다.
    //CLLocationCoordinate2D point = CLLocationCoordinate2DMake(bestPoint.y, bestPoint.x);
    NSInteger count = [self.steps count];
    
    // 찾은 위치의 i,j [steps][points]
    // 거리와 시간을 계산한다.
    int i = 0;
    int j = 0;
    NSInteger pointCount = 0;
    BOOL isFind = NO;
    for (; i < count; i++)
    {
        pointCount = [self.steps[i][@"polyline"] count];
        
        double _dist = 0;
        double _durt = 0;
        j = 0;
        
        double stepDist = [self.steps[i][@"distance"][@"value"] doubleValue]/(pointCount);
        double stepDurt = [self.steps[i][@"duration"][@"value"] doubleValue]/(pointCount);
        
        // 찾지 못하면 sub polyline을 비교한다.
        if (!isFind) {
            for (; j<pointCount; j++) {
                CLLocation *pointLocation =  self.steps[i][@"polyline"][j];
                
                if (pointLocation.coordinate.latitude == bestCoord.latitude &&
                    pointLocation.coordinate.longitude == bestCoord.longitude) {
                    // 위치를 찾으면 해당 위치의 i,j를 가지고 거리와 시간을 계산한다.
                    isFind = YES;
                    
                    //                    NSLog(@"Find (%d/%d,%d/%d) %f/%f",
                    //                          i+1, (int)count,
                    //                          j+1, (int)pointCount,
                    //                          _dist, _durt);
                    
                    //                    NSLog(@"Find (%@/%d,%@/%d) %f/%f %f,%f/%f,%f",
                    //                          locMgr.steps[i][@"distance"][@"value"], (int)(stepDist*pointCount),
                    //                          locMgr.steps[i][@"duration"][@"value"], (int)(stepDurt*pointCount),
                    //                          _dist, _durt,
                    //                          pointLocation.coordinate.latitude, pointLocation.coordinate.longitude,
                    //                          bestCoord.latitude, bestCoord.longitude
                    //                          );
                    break;
                }
                
                _dist += stepDist;
                _durt += stepDurt;
                
                //                NSLog(@"===> (%d/%d,%d/%d) %f/%f",
                //                      i+1, (int)count,
                //                      j+1, (int)pointCount,
                //                      _dist, _durt);
            }
        }
        
        if (!isFind) {
            dist += [self.steps[i][@"distance"][@"value"] doubleValue];
            durt += [self.steps[i][@"duration"][@"value"] doubleValue];
        }
        else {
            dist += _dist;
            durt += _durt;
            break;
        }
    }
    
    //    NSLog(@"paths.count:%lu (%d/%d)",
    //          locMgr.paths.count,
    //          i, (int)count
    //          );
    
    //    NSLog(@"%d: %d distance(%d/%d  duration(%d/%d)",
    //          __LINE__,
    //          isFind,
    //          (int)dist, [legs[@"distance"] intValue],
    //          (int)durt, [legs[@"duration"] intValue]);
    
    // 마지막이면
    if (bestIndex == (self.paths.count-1)) {
        afterIndex = bestIndex;
        afterPoint = bestPoint;
    }
    // 처음이면
    if (bestIndex == 0) {
        beforeIndex = bestIndex;
        beforePoint = bestPoint;
    }
    
    // 바로이전 point
    if (beforeIndex == -1) {
        beforeIndex = bestIndex - 1;
        CLLocationCoordinate2D coordinate = [self.paths coordinateAtIndex:beforeIndex];
        beforePoint = CGPointMake(coordinate.longitude, coordinate.latitude);
    }
    
    // 바로 앞 point
    if (afterIndex == -1) {
        afterIndex = bestIndex + 1;
        CLLocationCoordinate2D coordinate = [self.paths coordinateAtIndex:afterIndex];
        afterPoint = CGPointMake(coordinate.longitude, coordinate.latitude);
    }
    
    NSDictionary *points = @{
                             @"bestPoint":[NSValue valueWithCGPoint:bestPoint],
                             @"afterPoint":[NSValue valueWithCGPoint:afterPoint],
                             @"beforePoint":[NSValue valueWithCGPoint:beforePoint],
                             
                             @"bestIndex":[NSString stringWithFormat:@"%d", (int)bestIndex],
                             @"afterIndex":[NSString stringWithFormat:@"%d", (int)afterIndex],
                             @"beforeIndex":[NSString stringWithFormat:@"%d", (int)beforeIndex],
                             
                             @"dist":[NSString stringWithFormat:@"%d", (int)dist],
                             @"durt":[NSString stringWithFormat:@"%d", (int)durt],
                             
                             @"bestDistance":[NSString stringWithFormat:@"%d", (int)bestDistance],
                             @"newDistance":[NSString stringWithFormat:@"%d", (int)newDistance],
                             
                             @"bestCoord":[NSValue valueWithCGPoint:CGPointMake(bestCoord.longitude, bestCoord.latitude)],
                             
                             };
    
    return points;
}

// taken and modified from: http://stackoverflow.com/questions/849211/shortest-distance-between-a-point-and-a-line-segment
- (CGPoint)nearestPointToPoint:(CGPoint)origin onLineSegmentPointA:(CGPoint)pointA pointB:(CGPoint)pointB distance:(double *)distance {
    CGPoint dAP = CGPointMake(origin.x - pointA.x, origin.y - pointA.y);
    CGPoint dAB = CGPointMake(pointB.x - pointA.x, pointB.y - pointA.y);
    CGFloat dot = dAP.x * dAB.x + dAP.y * dAB.y;
    CGFloat squareLength = dAB.x * dAB.x + dAB.y * dAB.y;
    CGFloat param = dot / squareLength;
    
    CGPoint nearestPoint;
    if (param < 0 || (pointA.x == pointB.x && pointA.y == pointB.y)) {
        nearestPoint.x = pointA.x;
        nearestPoint.y = pointA.y;
    } else if (param > 1) {
        nearestPoint.x = pointB.x;
        nearestPoint.y = pointB.y;
    } else {
        nearestPoint.x = pointA.x + param * dAB.x;
        nearestPoint.y = pointA.y + param * dAB.y;
    }
    
    CGFloat dx = origin.x - nearestPoint.x;
    CGFloat dy = origin.y - nearestPoint.y;
    *distance = sqrtf(dx * dx + dy * dy);
    
    return nearestPoint;
}

-(double)speedOfKm:(CLLocationSpeed)speed {
    double kspeed = speed * 18/5; // km
    return kspeed;
}

-(double)speedOfMl:(CLLocationSpeed)speed {
    double mspeed = speed * 18/5 * 0.621371; // mile
    return mspeed;
}

// 거리계산 : 남은거리,이동거리,전체거리
// 시간계산 : 남은시간,이동시간,전체시간
-(NSDictionary*)calculateDistanceAndDuration:(NSDictionary*)points cloc:(CLLocation*)cLocation{
    NSDictionary *legs = [self distanceOffeature];
    
    int kspeed = (int)[self speedOfKm:cLocation.speed];
    int mspeed = (int)[self speedOfMl:cLocation.speed];
    
    if (kspeed<0) {
        kspeed = 0;
    }
    if (mspeed<0) {
        mspeed = 0;
    }
    
    //NSString *directionName = [GoogleUtils directionNameFromCompassValue:locMgr.lastDirection];
    
    // 계산된 이동시간과 거리를 가져온다.
    int edistance = [points[@"dist"] intValue];
    int eduration = [points[@"durt"] intValue];
    
    int total_distance = [legs[@"distance"] intValue];
    int total_duration = [legs[@"duration"] intValue];
    
    int remain_distance = total_distance - edistance;
    int remain_duration = total_duration - eduration;
    
    //dashboard
    
    NSLog(@"dashboard: 거리:%d/%d(%d) 시간:%d/%d(%d)",
          edistance, total_distance, remain_distance,
          eduration, total_duration, remain_duration
          );
    
    _distancematrix = @{
                        @"kspeed":[NSString stringWithFormat:@"%d", kspeed],
                        @"mspeed":[NSString stringWithFormat:@"%d", mspeed],
                        
                        @"edistance":[NSString stringWithFormat:@"%d", edistance],
                        @"eduration":[NSString stringWithFormat:@"%d", eduration],
                        
                        @"total_distance":[NSString stringWithFormat:@"%d", total_distance],
                        @"total_duration":[NSString stringWithFormat:@"%d", total_duration],
                        
                        @"remain_distance":[NSString stringWithFormat:@"%d", remain_distance],
                        @"remain_duration":[NSString stringWithFormat:@"%d", remain_duration]
                        
                        };
    return _distancematrix;
}


/*
 * polyline을 벗어났는지 확인한다.
 * 50m를 벗어나면 api를 다시 호출해야 된다.
 */
-(NSDictionary*)checkOutOfBoundPolyLine
{
    CLLocationCoordinate2D position = [[self currentLocation] coordinate];
    //CLLocationCoordinate2D point = kCLLocationCoordinate2DInvalid;
    
    if (self.features == nil) {
        return nil;
    }
    if (![GoogleUtils isLocationValid:position]) {
        return nil;
    }
    NSDictionary *points = [self nearestPolylineLocationToCoordinate:position];
    if (points == nil) {
        return nil;
    }
    CLLocationDistance dist = [points[@"newDistance"] doubleValue];
    
    // 50m를 벗어나면
    if (dist > OUT_OF_DISTANCE) {
        NSLog(@"newDistance:%f",dist);
        return nil;
    }
    
    //locMgr.lastPolyLocation =
    return points;
}

- (NSDictionary *)formatTmapToGoogleJSON:(NSDictionary *)responseObj passList:(BOOL)passList
{
    /*
    routes [0]
        [legs][0]
            distance.value - features[0][properties][totalDistance]
            duration.value - features[0][properties][totalTime]
            start_location - features[0][geometry][coordinates]
            end_location - features[n-1][properties][totalTime] // passList없는경우
            end_location - features[n-3][properties][totalTime] // passList있는경우
            steps[] - features[1-n][geometry][type] == "LineString"
                distance.value - features[1-n][properties][distance]
                duration.value - features[1-n][properties][time]
                html_instructions - features[1-n][properties][description]??
                polyline[] - features[1-n][geometry][coordinates][]
        overview_polyline[] - [coordinates][]
    status
    
    
    NSMutableDictionary *googleJSON = (NSMutableDictionary*)@{
                                                              @"routes":@[
                                                                      @{
                                                                          @"legs":@[
                                                                                  @{
                                                                                      @"distance" : @{@"value": @""},
                                                                                      @"duration" : @{@"value": @""},
                                                                                      @"start_location" : @{@"lat": @"", @"lng": @""},
                                                                                      @"end_location" : @{@"lat": @"", @"lng": @""}
                                                                                      //@"steps" : @[]
                                                                                      }
                                                                                  ]
                                                                          }
                                                                      ],
                                                              @"status":@"OK"
                                                              };
    
    NSDictionary *steps = @{
                            @"distance" : @{@"value": @""},
                            @"duration" : @{@"value": @""},
                            @"start_location" : @{@"lat": @"", @"lng": @""},
                            @"end_location" : @{@"lat": @"", @"lng": @""}
                            //@"polyline" : @[]
                            };
    */

    NSArray *features = [(NSDictionary*)responseObj objectForKey:@"features"];
    NSString* distance_val = features[0][@"properties"][@"totalDistance"]; // m
    NSString* duration_val = features[0][@"properties"][@"totalTime"]; // 초
    NSString* start_location_lat = [GoogleUtils formatedLocationOne:features[0][@"geometry"][@"coordinates"][1]];
    NSString* start_location_lng = [GoogleUtils formatedLocationOne:features[0][@"geometry"][@"coordinates"][0]];
    
    int end_idx = (int)[features count] - 1;
    if (passList) {
        end_idx -= 2; // passList가 존재하면
    }
    if ([features count]<end_idx) {
        return @{@"status":@"NOK"};
    }
    //NSLog(@"features:%@",features);
    NSString* end_location_lat = [GoogleUtils formatedLocationOne:features[end_idx][@"geometry"][@"coordinates"][1]];
    NSString* end_location_lng = [GoogleUtils formatedLocationOne:features[end_idx][@"geometry"][@"coordinates"][0]];
    
    NSDictionary *turnType = @{@"0": @"휴게소",
                               @"1": @"도곽에 의한 점",
                               @"2": @"타일에 의한 점",
                               @"3": @"고속도로에 의한 안내없음",
                               @"4": @"일반도로에 의한 안내없음",
                               @"5": @"특수한 경우 안내없음",
                               @"6": @"Y자 오른쪽 안내없음",
                               @"7": @"Y자 왼쪽 안내없음",
                               @"11": @"직진",
                               @"12": @"좌회전",
                               @"13": @"우회전",
                               @"14": @"U 턴",
                               @"15": @"P 턴",
                               @"16": @"8시 방향 좌회전",
                               @"17": @"10시 방향 좌회전",
                               @"18": @"2시 방향 우회전",
                               @"19": @"4시 방향 우회전",
                               @"43": @"오른쪽",
                               @"44": @"왼쪽",
                               @"51": @"직진 방향",
                               @"52": @"왼쪽 차선",
                               @"53": @"오른쪽 차선",
                               @"54": @"1차선",
                               @"55": @"2차선",
                               @"56": @"3차선",
                               @"57": @"4차선",
                               @"58": @"5차선",
                               @"59": @"6차선",
                               @"60": @"7차선",
                               @"61": @"8차선",
                               @"62": @"9차선",
                               @"63": @"10차선",
                               @"71": @"첫번째 출구",
                               @"72": @"두번째 출구",
                               @"73": @"첫번째 오른쪽 길",
                               @"74": @"두번째 오른쪽 길",
                               @"75": @"첫번째 왼쪽 길",
                               @"76": @"두번째 왼쪽 길",
                               @"101": @"우측 고속도로 입구",
                               @"102": @"좌측 고속도로 입구",
                               @"103": @"전방 고속도로 입구",
                               @"104": @"우측 고속도로 출구",
                               @"105": @"좌측 고속도로 출구",
                               @"106": @"전방 고속도로 출구",
                               @"111": @"우측 도시고속도로 입구",
                               @"112": @"좌측 도시고속도로 입구",
                               @"113": @"전방 도시고속도로 입구",
                               @"114": @"우측 도시고속도로 출구",
                               @"115": @"좌측 도시고속도로 출구",
                               @"116": @"전방 도시고속도로 출구",
                               @"117": @"우측 방향",
                               @"118": @"좌측 방향",
                               @"119": @"지하차도",
                               @"120": @"고가도로",
                               @"121": @"터널",
                               @"122": @"교량",
                               @"123": @"지하차도 옆",
                               @"124": @"고가도로 옆",
                               @"130": @"토끼굴 진입",
                               @"131": @"1시 방향",
                               @"132": @"2시 방향",
                               @"133": @"3시 방향",
                               @"134": @"4시 방향",
                               @"135": @"5시 방향",
                               @"136": @"6시 방향",
                               @"137": @"7시 방향",
                               @"138": @"8시 방향",
                               @"139": @"9시 방향",
                               @"140": @"10시 방향",
                               @"141": @"11시 방향",
                               @"142": @"12시 방향",
                               @"150": @"졸음쉼터",
                               @"151": @"휴게소",
                               @"182": @"왼쪽방향 도착안내",
                               @"183": @"오른쪽방향 도착안내",
                               @"184": @"경유지",
                               @"185": @"첫번째경유지",
                               @"186": @"두번째경유지",
                               @"187": @"세번째경유지",
                               @"188": @"네번째경유지",
                               @"189": @"다섯번째경유지",
                               @"191": @"제한속도",
                               @"192": @"사고다발",
                               @"193": @"급커브",
                               @"194": @"낙석주의",
                               @"200": @"출발지",
                               @"201": @"도착지",
                               @"203": @"목적지건너편",
                               @"211": @"횡단보도",
                               @"212": @"좌측 횡단보도",
                               @"213": @"우측 횡단보도",
                               @"214": @"8시 방향 횡단보도",
                               @"215": @"10시 방향 횡단보도",
                               @"216": @"2시 방향 횡단보도",
                               @"217": @"4시 방향 횡단보도",
                               @"218": @"엘리베이터",
                               @"233": @"직진 임시"};
    
    NSMutableArray *steps = [NSMutableArray array];
    NSInteger fcount = [features count];
    for (NSInteger geometryindex = 1; geometryindex < fcount ; geometryindex++) {
        NSDictionary *step = features[geometryindex];
        if ([GoogleUtils isDictionary:step]) {
            NSDictionary *geometry   = [step objectForKey:@"geometry"];
            NSDictionary *properties = [step objectForKey:@"properties"];
            if ([[geometry objectForKey:@"type"] isEqualToString:@"LineString"] && ![[GoogleUtils nullToString:properties[@"name"]] isEqualToString:@""]) {
                //if ([[geometry objectForKey:@"type"] isEqualToString:@"LineString"]) {
                NSArray *coordinates = [geometry objectForKey:@"coordinates"];
                
                NSInteger coordIdx = [coordinates count]-1;
                NSDictionary *properties = [step objectForKey:@"properties"];
                NSString *distance = [NSString stringWithFormat:@"%@", properties[@"distance"]];
                NSString *duration = [NSString stringWithFormat:@"%@", properties[@"time"]];
                
                NSMutableArray *polyline = [NSMutableArray array];
                for (int i = 0; i <= coordIdx; i++)
                {
                    CLLocation *pointLocation = [[CLLocation alloc] initWithLatitude:[coordinates[i][1] doubleValue]
                                                                           longitude:[coordinates[i][0] doubleValue]];
                    
                    [polyline addObject:pointLocation];
                }
                
                if (properties[@"turnType"]) {
                    NSString *ttype = [NSString stringWithFormat:@"%@",properties[@"turnType"]];
                    NSString *turn = [turnType objectForKey:ttype];
                    
                    if (turn) {
                        //NSString *description = [NSString stringWithFormat:@"%@ %@",turn, properties[@"nextRoadName"]];
                        NSString *description = [NSString stringWithFormat:@"%@ %@ %@", properties[@"nextRoadName"], distance, turn];
                        
                        [steps addObject:@{@"start_location":@{@"lat":[GoogleUtils formatedLocationOne:coordinates[0][1]],
                                                               @"lng":[GoogleUtils formatedLocationOne:coordinates[0][0]]},
                                           @"end_location":@{@"lat":[GoogleUtils formatedLocationOne:coordinates[coordIdx][1]],
                                                             @"lng":[GoogleUtils formatedLocationOne:coordinates[coordIdx][0]]},
                                           
                                           @"distance":@{@"text":distance,@"value":distance},
                                           @"duration":@{@"text":duration,@"value":duration},
                                           @"polyline":polyline,
                                           @"html_instructions":description,
                                           @"maneuver":turn}];
                    }
                    else {
                        [steps addObject:@{@"start_location":@{@"lat":[GoogleUtils formatedLocationOne:coordinates[0][1]],
                                                               @"lng":[GoogleUtils formatedLocationOne:coordinates[0][0]]},
                                           @"end_location":@{@"lat":[GoogleUtils formatedLocationOne:coordinates[coordIdx][1]],
                                                             @"lng":[GoogleUtils formatedLocationOne:coordinates[coordIdx][0]]},
                                           
                                           @"distance":@{@"text":distance,@"value":distance},
                                           @"duration":@{@"text":duration,@"value":duration},
                                           @"polyline":polyline,
                                           @"html_instructions":properties[@"nextRoadName"]}];
                    }
                }
                else {
                    [steps addObject:@{@"start_location":@{@"lat":[GoogleUtils formatedLocationOne:coordinates[0][1]],
                                                           @"lng":[GoogleUtils formatedLocationOne:coordinates[0][0]]},
                                       @"end_location":@{@"lat":[GoogleUtils formatedLocationOne:coordinates[coordIdx][1]],
                                                         @"lng":[GoogleUtils formatedLocationOne:coordinates[coordIdx][0]]},
                                       
                                       @"distance":@{@"text":distance,@"value":distance},
                                       @"duration":@{@"text":duration,@"value":duration},
                                       @"polyline":polyline,
                                       @"html_instructions":properties[@"description"]}];
                }
            }
            else if ([[geometry objectForKey:@"type"] isEqualToString:@"Point"]) {
                NSDictionary *properties = [step objectForKey:@"properties"];
                //NSString *distance = [NSString stringWithFormat:@"%@", properties[@"distance"]];
                
                NSMutableDictionary *updateSteps = [[steps lastObject] mutableCopy];
                if (properties[@"turnType"]) {
                    //NSString *turn = turnType[properties[@"turnType"]];
                    NSString *ttype = [NSString stringWithFormat:@"%@",properties[@"turnType"]];
                    NSString *turn = [turnType objectForKey:ttype];
                    
                    if (turn) {
                        //NSString *description = [NSString stringWithFormat:@"%@ %@",turn, properties[@"nextRoadName"]];
                        //NSString *description = [NSString stringWithFormat:@"%@", properties[@"nextRoadName"]];
                        NSString *distance = [NSString stringWithFormat:@"%@", properties[@"distance"]];
                        NSString *description = [NSString stringWithFormat:@"%@ %@ %@", properties[@"nextRoadName"], distance, turn];
                        
                        updateSteps[@"html_instructions"] = description;
                        updateSteps[@"maneuver"] = turn;
                        
                        [steps replaceObjectAtIndex:([steps count]-1) withObject:updateSteps];
                    }
                }
            }
        }
    }
    
    NSMutableDictionary *googleJSON = (NSMutableDictionary*)@{
                                                              @"routes":@[
                                                                      @{
                                                                          @"legs":@[
                                                                                  @{
                                                                                      @"distance" : @{@"value": distance_val},
                                                                                      @"duration" : @{@"value": duration_val},
                                                                                      @"start_location" : @{@"lat": start_location_lat, @"lng": start_location_lng},
                                                                                      @"end_location" : @{@"lat": end_location_lat, @"lng": end_location_lng},
                                                                                      @"steps":steps
                                                                                      }
                                                                                  ]
                                                                          }
                                                                      ],
                                                              @"status":@"OK"
                                                              };

    return googleJSON;
}

- (NSDictionary *)formatWitzToGoogleJSON:(NSDictionary *)responseObj {
    
    long rlt = [responseObj[@"rlt"] longValue];
    NSDictionary *legs = responseObj[@"direction"][@"legs"];
    NSString* distance_val = legs[@"distance"]; // m
    NSString* duration_val = legs[@"duration"]; // 초
    NSString* start_location_lat = [GoogleUtils formatedLocationOne:legs[@"start_location"][@"lat"]];
    NSString* start_location_lng = [GoogleUtils formatedLocationOne:legs[@"start_location"][@"lng"]];
    
    NSString* end_location_lat = [GoogleUtils formatedLocationOne:legs[@"end_location"][@"lat"]];
    NSString* end_location_lng = [GoogleUtils formatedLocationOne:legs[@"end_location"][@"lng"]];
    
    NSMutableArray *steps = [NSMutableArray array];
    NSInteger fcount = [legs[@"steps"] count];
    for (NSInteger geometryindex = 0; geometryindex < fcount ; geometryindex++) {
        NSDictionary *step = legs[@"steps"][geometryindex];
        if ([GoogleUtils isDictionary:step] && [step[@"type"] isEqualToString:@"LineString"]) {
            
            //if ([[geometry objectForKey:@"type"] isEqualToString:@"LineString"] && ![[GoogleUtils nullToString:properties[@"name"]] isEqualToString:@""])
            {
                //if ([[geometry objectForKey:@"type"] isEqualToString:@"LineString"]) {
                NSArray *coordinates = [step objectForKey:@"polylines"];
                
                NSInteger coordIdx = [coordinates count]-1;
                
                NSString *distance = [NSString stringWithFormat:@"%@", step[@"distance"]];
                NSString *duration = [NSString stringWithFormat:@"%@", step[@"duration"]];
                
                NSMutableArray *polyline = [NSMutableArray array];
                for (int i = 0; i <= coordIdx; i++)
                {
                    CLLocation *pointLocation = [[CLLocation alloc] initWithLatitude:[coordinates[i][0] doubleValue]
                                                                           longitude:[coordinates[i][1] doubleValue]];
                    
                    [polyline addObject:pointLocation];
                }
                
                [steps addObject:@{@"start_location":@{@"lat":[GoogleUtils formatedLocationOne:coordinates[0][0]],
                                                       @"lng":[GoogleUtils formatedLocationOne:coordinates[0][1]]},
                                   @"end_location":@{@"lat":[GoogleUtils formatedLocationOne:coordinates[coordIdx][0]],
                                                     @"lng":[GoogleUtils formatedLocationOne:coordinates[coordIdx][1]]},
                                   
                                   @"distance":@{@"text":distance,@"value":distance},
                                   @"duration":@{@"text":duration,@"value":duration},
                                   @"polyline":polyline,
                                   @"html_instructions":step[@"html_instructions"],
                                   @"maneuver":step[@"maneuver"]}];
                
            }
        }
    }
    
    NSMutableDictionary *googleJSON = (NSMutableDictionary*)@{
                                                              @"routes":@[
                                                                      @{
                                                                          @"legs":@[
                                                                                  @{
                                                                                      @"distance" : @{@"value": distance_val},
                                                                                      @"duration" : @{@"value": duration_val},
                                                                                      @"start_location" : @{@"lat": start_location_lat, @"lng": start_location_lng},
                                                                                      @"end_location" : @{@"lat": end_location_lat, @"lng": end_location_lng},
                                                                                      @"steps":steps
                                                                                      }
                                                                                  ]
                                                                          }
                                                                      ],
                                                              @"status":(rlt == 1?@"OK":@"NOK")
                                                              };
    
    return googleJSON;
}

// end of added
//******************************************************************************************

@end

