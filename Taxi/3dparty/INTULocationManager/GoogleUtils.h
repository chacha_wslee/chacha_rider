

#import <UIKit/UIKit.h>

@import GoogleMaps;

#define OUT_OF_DISTANCE_FOR_HEADING    20 // 20m
#define OUT_OF_DISTANCE                50 // polyline(gps간 거리)에서 벗어 났다고 판단 하는 거리
#define OUT_OF_LINE_DISTANCE           20 // polyline의 bestpoint와 현재 gps간의 거리

@interface GoogleUtils : NSObject

/** added **/
+(double)findTotalDistanceOfPath:(GMSMutablePath *)path;
+(CLLocationCoordinate2D)findMiddlePointInPath:(GMSMutablePath *)path distance :(double)totalDistance threshold:(int)threshold;

+(BOOL)pointCompare:(double)param1 comp:(double)param2;
+(BOOL)isEqualLocation:(CLLocationCoordinate2D) coord1 location:(CLLocationCoordinate2D) coord2;
+(CLLocationCoordinate2D)formatedLocation:(NSString *)coord1 lng:(NSString*)coord2;
+(NSString *)formatedLocationOne:(NSString *)coord1;
+ (NSString *)directionNameFromCompassValue:(CGFloat)compassValue;
+(BOOL)isLocationValid:(CLLocationCoordinate2D)coor;

#pragma mark - Utils
+(NSString*)replaceStr:(NSString*)mstr search:(NSString*)search replace:(NSString*)replace;
+(NSString*)appendUrl:(NSString*)url param:(NSString*)param;
+ (NSString *)serializeParams:(NSDictionary *)params;
+ (NSString *)escapeValueForURLParameter:(NSString *)valueToEscape;
+ (NSDictionary*)getDataOfQueryString:(NSString*)url;

+ (NSString*)trimStringOnly:(NSString*)str;
+ (NSString*)nullToString:(id)string;
+ (BOOL)isArray:(NSArray*)dic;
+ (BOOL)isDictionary:(NSDictionary*)dic;
+ (NSString*)nowTimestamp;
+ (BOOL)nullToImage:(UIImage*)image;
@end
