//
//  INTULocationManager.h
//
//  Copyright (c) 2014-2017 Intuit Inc.
//
//  Permission is hereby granted, free of charge, to any person obtaining
//  a copy of this software and associated documentation files (the
//  "Software"), to deal in the Software without restriction, including
//  without limitation the rights to use, copy, modify, merge, publish,
//  distribute, sublicense, and/or sell copies of the Software, and to
//  permit persons to whom the Software is furnished to do so, subject to
//  the following conditions:
//
//  The above copyright notice and this permission notice shall be
//  included in all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
//  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
//  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
//  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
//  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
//  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

#import "INTULocationRequestDefines.h"

// added
//******************************************************************************************
@import GoogleMaps;

#ifdef _DRIVER_MODE
#import "CMMapLauncher.h"
#endif

typedef void(^completion_handler_t)(NSMutableDictionary *_Nullable, NSString *_Nullable, NSError* _Nullable error, long code);
typedef void(^user_completion_block)(NSDictionary *   _Nonnull user,NSString *_Nullable, NSString * _Nullable _Nullable_Nullable, int status);
typedef void(^request_completion_block)(NSDictionary *_Nullable, int status);
// end of added
//******************************************************************************************


//! Project version number for INTULocationManager.
FOUNDATION_EXPORT double INTULocationManagerVersionNumber;

//! Project version string for INTULocationManager.
FOUNDATION_EXPORT const unsigned char INTULocationManagerVersionString[];


NS_ASSUME_NONNULL_BEGIN

/**
 An abstraction around CLLocationManager that provides a block-based asynchronous API for obtaining the device's location.
 INTULocationManager automatically starts and stops system location services as needed to minimize battery drain.
 */
@interface INTULocationManager : NSObject

/** Returns the current state of location services for this app, based on the system settings and user authorization status. */
+ (INTULocationServicesState)locationServicesState;

/** Returns the current state of heading services for this device. */
+ (INTUHeadingServicesState)headingServicesState;

/** Returns the singleton instance of this class. */
+ (instancetype)sharedInstance;

#pragma mark Location Requests

/**
 Asynchronously requests the current location of the device using location services.
 
 @param desiredAccuracy The accuracy level desired (refers to the accuracy and recency of the location).
 @param timeout         The maximum amount of time (in seconds) to wait for a location with the desired accuracy before completing. If
 this value is 0.0, no timeout will be set (will wait indefinitely for success, unless request is force completed or canceled).
 @param block           The block to execute upon success, failure, or timeout.
 
 @return The location request ID, which can be used to force early completion or cancel the request while it is in progress.
 */
- (INTULocationRequestID)requestLocationWithDesiredAccuracy:(INTULocationAccuracy)desiredAccuracy
                                                    timeout:(NSTimeInterval)timeout
                                                      block:(INTULocationRequestBlock)block;

/**
 Asynchronously requests the current location of the device using location services, optionally delaying the timeout countdown until the user has
 responded to the dialog requesting permission for this app to access location services.
 
 @param desiredAccuracy      The accuracy level desired (refers to the accuracy and recency of the location).
 @param timeout              The maximum amount of time (in seconds) to wait for a location with the desired accuracy before completing. If
 this value is 0.0, no timeout will be set (will wait indefinitely for success, unless request is force completed or canceled).
 @param delayUntilAuthorized A flag specifying whether the timeout should only take effect after the user responds to the system prompt requesting
 permission for this app to access location services. If YES, the timeout countdown will not begin until after the
 app receives location services permissions. If NO, the timeout countdown begins immediately when calling this method.
 @param block                The block to execute upon success, failure, or timeout.
 
 @return The location request ID, which can be used to force early completion or cancel the request while it is in progress.
 */
- (INTULocationRequestID)requestLocationWithDesiredAccuracy:(INTULocationAccuracy)desiredAccuracy
                                                    timeout:(NSTimeInterval)timeout
                                       delayUntilAuthorized:(BOOL)delayUntilAuthorized
                                                      block:(INTULocationRequestBlock)block;

/**
 Creates a subscription for location updates that will execute the block once per update indefinitely (until canceled), regardless of the accuracy of each location.
 This method instructs location services to use the highest accuracy available (which also requires the most power).
 If an error occurs, the block will execute with a status other than INTULocationStatusSuccess, and the subscription will be canceled automatically.
 
 @param block The block to execute every time an updated location is available.
 The status will be INTULocationStatusSuccess unless an error occurred; it will never be INTULocationStatusTimedOut.
 
 @return The location request ID, which can be used to cancel the subscription of location updates to this block.
 */
- (INTULocationRequestID)subscribeToLocationUpdatesWithBlock:(INTULocationRequestBlock)block;

/**
 Creates a subscription for location updates that will execute the block once per update indefinitely (until canceled), regardless of the accuracy of each location.
 The specified desired accuracy is passed along to location services, and controls how much power is used, with higher accuracies using more power.
 If an error occurs, the block will execute with a status other than INTULocationStatusSuccess, and the subscription will be canceled automatically.
 
 @param desiredAccuracy The accuracy level desired, which controls how much power is used by the device's location services.
 @param block           The block to execute every time an updated location is available. Note that this block runs for every update, regardless of
 whether the achievedAccuracy is at least the desiredAccuracy.
 The status will be INTULocationStatusSuccess unless an error occurred; it will never be INTULocationStatusTimedOut.
 
 @return The location request ID, which can be used to cancel the subscription of location updates to this block.
 */
- (INTULocationRequestID)subscribeToLocationUpdatesWithDesiredAccuracy:(INTULocationAccuracy)desiredAccuracy
                                                                 block:(INTULocationRequestBlock)block;
// added
//******************************************************************************************
- (INTULocationRequestID)subscribeToLocationUpdatesWithDesiredAccuracy:(INTULocationAccuracy)desiredAccuracy
                                                        distanceFilter:(int)distanceFilter
                                                                 block:(INTULocationRequestBlock)block;
// end of added
//******************************************************************************************
/**
 Creates a subscription for significant location changes that will execute the block once per change indefinitely (until canceled).
 If an error occurs, the block will execute with a status other than INTULocationStatusSuccess, and the subscription will be canceled automatically.
 
 @param block The block to execute every time an updated location is available.
 The status will be INTULocationStatusSuccess unless an error occurred; it will never be INTULocationStatusTimedOut.
 
 @return The location request ID, which can be used to cancel the subscription of significant location changes to this block.
 */
- (INTULocationRequestID)subscribeToSignificantLocationChangesWithBlock:(INTULocationRequestBlock)block;

/** Immediately forces completion of the location request with the given requestID (if it exists), and executes the original request block with the results.
 For one-time location requests, this is effectively a manual timeout, and will result in the request completing with status INTULocationStatusTimedOut.
 If the requestID corresponds to a subscription, then the subscription will simply be canceled. */
- (void)forceCompleteLocationRequest:(INTULocationRequestID)requestID;

/** Immediately cancels the location request (or subscription) with the given requestID (if it exists), without executing the original request block. */
- (void)cancelLocationRequest:(INTULocationRequestID)requestID;

#pragma mark Heading Requests

/**
 Creates a subscription for heading updates that will execute the block once per update indefinitely (until canceled), assuming the heading update exceeded the heading filter threshold.
 If an error occurs, the block will execute with a status other than INTUHeadingStatusSuccess, and the subscription will be canceled automatically.
 
 @param block           The block to execute every time an updated heading is available. The status will be INTUHeadingStatusSuccess unless an error occurred.
 
 @return The heading request ID, which can be used to cancel the subscription of heading updates to this block.
 */
- (INTUHeadingRequestID)subscribeToHeadingUpdatesWithBlock:(INTUHeadingRequestBlock)block;

/** Immediately cancels the heading subscription request with the given requestID (if it exists), without executing the original request block. */
- (void)cancelHeadingRequest:(INTUHeadingRequestID)requestID;

#pragma mark - Additions

/** It is possible to force enable background location fetch even if your set any kind of Authorizations */
- (void)setBackgroundLocationUpdate:(BOOL) enabled;

// added
//******************************************************************************************
@property (strong, nonatomic) NSMutableArray *usefulLocations;
@property (nonatomic, readwrite) CLLocationDirection lastDirection;
@property (nonatomic, readwrite) CLLocationSpeed lastSpeed;
@property (nonatomic, readwrite) CLLocationCoordinate2D lastCoordinates;
@property (nonatomic, readwrite) BOOL isMonitoringLocationUpdates;

@property (nonatomic, readwrite) CMapType mapType;
@property (nonatomic, readwrite) NSMutableArray *steps;
@property (nonatomic, readwrite) NSDictionary *features;
@property (nonatomic, readwrite) GMSMutablePath *paths;
@property (nonatomic, readwrite) GMSMapView *mapView;

@property (nonatomic, readwrite) NSDate *mapApilastRunTime;
@property (nonatomic, readwrite) GMSPolyline *tripline;
@property (nonatomic, readwrite) GMSPolyline *histline;

@property (nonatomic, readwrite) NSInteger bestIndex; // 현재위치와 가장 가까운 steps의 index
@property (nonatomic, readwrite) NSInteger bestIndexJ; // 현재위치와 가장 가까운 steps.points의 index

@property (nonatomic, readwrite) CLLocation *lastPolyLocation; // 미자막 polyline의 location

@property (nonatomic, readwrite) CLLocation *realTimeCLocation; // 필터되지 않은 realtime gps정보

// rider, driver 정보 - 미정의
@property (nonatomic, readwrite) NSDictionary *dicDriver;
@property (nonatomic, readwrite) NSDictionary *dicRider;

// 국가정보
@property (nonatomic, readwrite) NSString *countryCode;
@property (nonatomic, readwrite) BOOL isKorea;

// marker
@property (strong, nonatomic) GMSMarker *markerPickup;
@property (strong, nonatomic) GMSMarker *markerStart;
@property (strong, nonatomic) GMSMarker *markerFinish;
@property (strong, nonatomic) GMSMarker *markerDrive;

// map 터치이동 여부
@property (nonatomic, readwrite) BOOL isMoveMapBYGesture;

// 실시간 남은시간, 남은거리
@property (nonatomic, readwrite) NSDictionary *distancematrix;
// 현재위치 또는 차량 마커
//@property (nonatomic, readwrite) GMSMarker *currLocMarker;

- (CLLocation *)currentLocation;
- (void)updateCurrentLocation:(CLLocation*)cloc;
-(void)setFeatures:(NSDictionary*)feature;
-(BOOL)isValidLocation:(CLLocationCoordinate2D)position;

- (NSString *)getLocationErrorDescription:(INTULocationStatus)status;
-(NSDictionary*)checkOutOfBoundPolyLine;


#pragma mark - Util
-(void)resetMgr;
-(void)activeLocale;
-(double)speedOfKm:(CLLocationSpeed)speed;
-(double)speedOfMl:(CLLocationSpeed)speed;
-(NSDictionary*)calculateDistanceAndDuration:(NSDictionary*)points cloc:(CLLocation*)cLocation;
-(NSDictionary*)startAndEndOffeature;
-(NSDictionary*)distanceOffeature;

#pragma mark - Marker
-(void)initMarker;
-(void)showStartMarker:(CLLocationCoordinate2D)lastCoordinates;
-(void)showFinishMarker:(CLLocationCoordinate2D)lastCoordinates;
-(void)showDriveMarker:(CLLocationCoordinate2D)lastCoordinates;
-(void)updateStartMarker:(CLLocationCoordinate2D)lastCoordinates;
-(void)updateFinishMarker:(CLLocationCoordinate2D)lastCoordinates;
-(void)updateDriveMarker:(CLLocationCoordinate2D)lastCoordinates;
-(UIImage*)getMarkerIcon:(NSString*)icon;
-(void)clearMarker:(GMSMarker*)marker;

#pragma mark - Map
- (void)initMapInfo:(id)mapview mapType:(CMapType)mapType;
- (void)initMapInfo:(id)mapview;
- (void)updateMapFitBounds:(NSArray *_Nullable)coord__;

- (void)drawTripLine;
- (void)drawHistoryPolyline:(CLLocationCoordinate2D)bestPoint
                    current:(CLLocationCoordinate2D)currpos;

-(void) updateDriveMap:(CLLocation *)currentLocation dst:(CLLocationCoordinate2D)_endPoint;
-(void)moveDriveMarker:(float)animationDuration
                  coor:(CLLocationCoordinate2D)coordinate
                  head:(BOOL)isChangedHeading
               heading:(CLLocationDirection)heading;

-(void)moveToCurrentLocation:(float)animationDuration
                lineDistance:(int)lineDistance
                    location:(CLLocation *)polylineLocation;

#pragma mark - Direction
- (void)requestDirection:(CLLocationCoordinate2D)spoint
                  endPos:(CLLocationCoordinate2D)epoint
            andWaypoints:(CLLocationCoordinate2D)waypoint
                andParam:(NSMutableDictionary* _Nullable)param
              completion:(request_completion_block)completion;
- (void)requestDirection:(CLLocationCoordinate2D)spoint
                  endPos:(CLLocationCoordinate2D)epoint
            andWaypoints:(CLLocationCoordinate2D)waypoint
                andParam:(NSMutableDictionary* _Nullable)param
              andMapType:(CMapType)mapType
              completion:(request_completion_block)completion;

#pragma mark - Geocode
-(void)geocodeAPICall:(CLLocationCoordinate2D)position
              mapType:(CMapType)mapType
           completion: (user_completion_block)completion;
-(void)geocodeAPICall:(CLLocationCoordinate2D)position
           completion: (user_completion_block)completion;

#pragma mark - POI
-(void)POIAPICall:(NSString*)address
       completion: (user_completion_block)completion;

-(void)POIAPICall:(NSString*)address
          mapType:(CMapType)mapType
       completion: (user_completion_block)completion;

#pragma mark - MapOpen
#ifdef _DRIVER_MODE
-(BOOL)openNavigationApp:(CMMapApp)mapApp
                destAddr:(NSString*)mapDestAddr
                   coord:(CLLocationCoordinate2D)coordinate;
#endif

#pragma mark - HTTP
-(void)callGetWebService:(NSString *)urlStr
                  method:(NSString*)method
           andDictionary:(NSDictionary *)parameter
              completion:(completion_handler_t)completion;
// end of added
//******************************************************************************************

@end

NS_ASSUME_NONNULL_END
