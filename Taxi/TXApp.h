//
//  TXApp.h
//  Taxi
//

//

#import <Foundation/Foundation.h>
#import "TXSettings.h"

@interface TXApp : NSObject

+(TXApp*) instance;
-(TXSettings *) getSettings;
-(NSString *) getDeviceUID;
-(NSString *) getDeviceModel;
-(NSString *) getSystemVersion;


@end
