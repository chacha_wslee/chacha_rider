//
//  TXAppDelegate.h
//  Taxi
//

//

#import <UIKit/UIKit.h>
#import "TXEventTarget.h"
#import "REFrostedViewController.h"
#import "TXDBHelper.h"
//#import "Selene.h"
//#import "EDQueue.h"
//#import "DFCache.h"
#import "CLLocation+Utils.h"
#import "INTULocationManager.h"

#ifdef _DRIVER_MODE
    #import <SpotifyAudioPlayback/SpotifyAudioPlayback.h>
    #import <AVFoundation/AVFoundation.h>
#endif


//#import "INTULocationManager.h"

//@import BlocksKit;

@protocol SpotifyDelegate <NSObject>
@required
- (void) didChangePositionSpotify:(NSTimeInterval)position;
- (void) didChangeUISpotify:(BOOL)isPlay;

@end

@protocol CLocationDelegate <NSObject>
@required
- (void) didUpdateLocations:(CLLocation *)location;

@end

@interface TXAppDelegate : UIResponder <UIApplicationDelegate, TXEventListener, REFrostedViewControllerDelegate
#ifdef _DRIVER_MODE
                                        ,AVAudioSessionDelegate,SPTAudioStreamingDelegate, SPTAudioStreamingPlaybackDelegate
#endif
>

@property (nonatomic, assign) BOOL stdErrRedirected;
@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, strong) REFrostedViewController *frostedViewController;
@property (nonatomic, assign) id<CLocationDelegate> locationDelegate;

#ifdef _DRIVER_MODE
    // spotify
    @property (nonatomic, strong) SPTAudioStreamingController *player;
    @property (nonatomic) BOOL isChangingProgress;
    @property (nonatomic) NSTimeInterval playerDuration;

    @property (nonatomic, assign) id<SpotifyDelegate> spotifyDelegate;
#endif

//// location
//@property (assign, nonatomic) INTULocationAccuracy desiredAccuracy;
//@property (assign, nonatomic) NSTimeInterval timeout;
//
//@property (assign, nonatomic) INTULocationRequestID locationRequestID;
//@property (assign, nonatomic) INTULocationRequestID locationMonitoringID;
//@property (assign, nonatomic) INTUHeadingRequestID headingRequestID;

@property (assign, nonatomic) INTULocationRequestID locationRequestID;
@property (assign, nonatomic) INTUHeadingRequestID headingRequestID;


//@property (assign, nonatomic) CLLocationCoordinate2D currLocation;
@property (strong, nonatomic) CLLocation *location;

//@property (nonatomic, strong) DFCache *cache;

@property (assign, nonatomic) BOOL isDebugMode;
@property (assign, nonatomic) BOOL isNetwork;
@property (assign, nonatomic) BOOL isKorea;

@property(nonatomic, strong) NSString *mapVCBeforeVC;
@property (assign, nonatomic) BOOL isMapView;
@property (assign, nonatomic) BOOL isUploadPushToken;
@property (assign, nonatomic) BOOL isBecomeMode;
@property (assign, nonatomic) BOOL isCanDriverMode;
@property (assign, nonatomic) BOOL isDriverMode;

@property (assign, nonatomic) BOOL isChangeState;
@property (assign, nonatomic) BOOL isMyCancel; // trip cancel

@property (assign, nonatomic) int uistate;
@property (assign, nonatomic) int uistate2;

@property (assign, nonatomic) int ustate;
@property (assign, nonatomic) int ustate2;
@property (assign, nonatomic) int ustateold;
@property (assign, nonatomic) int ustate2old;
@property (assign, nonatomic) int uustate;

@property (assign, nonatomic) int pstate;
@property (assign, nonatomic) int pstate2;
@property (assign, nonatomic) int pstateold;
@property (assign, nonatomic) int pstate2old;
@property (assign, nonatomic) int ppstate;

@property (strong, nonatomic) NSDictionary *checkr_data;

@property (assign, nonatomic) int msgtime;

@property(nonatomic, strong) UIImage *imgProfileRider;
@property(nonatomic, strong) UIImage *imgProfileDriver;
@property(nonatomic, strong) UIImage *imgVehicleDriver;

@property(nonatomic, strong) NSDictionary *dicDriver; // 로그인정보, user_payment, provider,
@property(nonatomic, strong) NSDictionary *dicRider; // 로그인정보, user_payment, provider,
@property(nonatomic, strong) NSDictionary *dicDrive; // drive user, provider,
@property(nonatomic, strong) NSString *pictureSizeRider;
@property(nonatomic, strong) NSString *pictureSizeDriver;
@property(nonatomic, strong) NSString *pictureSizeVehicle;
@property(nonatomic, strong) NSString *becomePseq;
@property(nonatomic, strong) NSString *becomeVseq;

@property(nonatomic, strong) NSString *appstoredriverurl;
@property(nonatomic, strong) NSString *appstoreurl;
@property(nonatomic, strong) NSString *appversion;
@property(nonatomic, strong) NSString *paykey;

@property(nonatomic, strong) NSString *chatOseq;
@property(nonatomic, strong) NSString *shareOseq;
@property(nonatomic, strong) NSDictionary *customShareDic;
@property(nonatomic, assign) NSInteger signupViewStep;
@property(nonatomic, strong) NSString* userPushToken;

// UI 상태 정보
@property(nonatomic, strong) NSMutableDictionary *stateUI;
@property(nonatomic, strong) NSMutableDictionary *stateUIBefore;

@property(nonatomic, strong) NSMutableDictionary *loopArray;
//@property(nonatomic, readonly, strong) NSDictionary *cardInfo;

@property(nonatomic, strong) NSDictionary *api_config;
@property(nonatomic, strong) NSDictionary *estimate_driver;
@property(nonatomic, strong) NSDictionary *drive_estimates;


- (void)removeAllVCFromNavigation;
-(void)resetRootVC:(UIViewController*)firstVC;
-(void)menuViewDelegate:(id)sender;
-(void) updateActivityState:(id)target;
-(void)debugActivityState;
-(void)resetState;
-(void)resetState2;
-(void)initProtocolMessage;
-(void)onTokenToServer;
-(void)resetTripInfo;
-(void)updateDic:(TXEvent*)event;

-(void) showBusyIndicator;
-(void) showBusyIndicator:(NSString *)title;
-(void)hideBusyIndicator;

- (void)playSound:(NSString *)filename vibrate:(BOOL)vibrate;
- (void)loopSound:(NSString *)filename;
- (void)stopSound;

-(void)onCommonErrorAlert:(NSString*)failureMessage;
-(void)onCommonErrorAlert:(NSString*)failureMessage timeout:(NSInteger)timeout;

-(BOOL)isLoginState;

- (void)redirectStdErrToFile;
- (void)restoreStdErr;
-(NSString *)readStringFromFile;

#ifdef _DRIVER_MODE
    -(NSError*)handleNewSession;
#endif

// location
- (void)startSingleLocationRequest;
- (void)startLocationUpdateSubscription;
- (void)forceCompleteRequest;
- (void)cancelRequest;
@end
