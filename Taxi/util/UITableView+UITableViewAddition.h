@interface UITableView (UITableViewAddition)
-(void)reloadDataAndWait:(void(^)(void))waitBlock;
@end;
