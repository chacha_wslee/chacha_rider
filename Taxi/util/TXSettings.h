//
//  TXSettings.h
//  Taxi
//

//

#import <Foundation/Foundation.h>

@interface TXSettings : NSObject {
    NSString*               stgPath;
    NSMutableDictionary*    root;           //main storage, gets recorded in the local file. Holds user settings as well.
    
}

/*!
 @function instance Convenience function to obtain settings singleton object
 @return TXSettings object
 */
+(TXSettings*) instance;

/*!
 @function saveSettings Saves settings to the storage.
 @return void
 */
-(void)saveSettings;

-(void)initWithDefaults;

-(void)initWithObject:(NSDictionary*)userProfile;

/*!
 @function setProperty Set the value of a property
 @param property The name of the property to set
 @param value The value for the property
 @return void
 */
-(void)setProperty:(NSString*)property value:(id)value;

/*!
 @function getProperty Get the value of a property
 @param property The name of the property to get
 @return The value for the property
 */
-(id)getProperty:(NSString*)property;

-(NSString *)getUserId;
-(NSString*)getUserTelno;
-(NSString*)getPassword;

-(void)setUserId:(NSString*)userId;
-(void)setUserTelno:(NSString*)userTelno;
-(void)setPassword:(NSString*)pwd;

-(void)setUserToken:(NSString *)token;
-(void)removeUserToken;
-(void)removeFDKeychain:(NSString *)key;
-(NSString *)getUserToken;



-(void) setNotificationsToken : (NSString *)token;
-(NSString*)getNotificationsToken;
-(void)setDeviceID:(NSString *)deviceid;
-(NSString *)getDeviceID;

-(NSString *)getFDKeychain:(NSString*)key;
-(void)setFDKeychain:(NSString *)str value:(NSString*)key;
-(void) setDriveMode : (NSString *)mode;
-(NSString *) getDriveMode;

-(void) setGoogleUserId : (NSString *)userId;
-(NSString*)getGoogleUserId;
-(void) setFBUserId : (NSString *)userId;
-(NSString*)getFBUserId;

-(int) getMaxHTTPConnectionsNumber;

@end
