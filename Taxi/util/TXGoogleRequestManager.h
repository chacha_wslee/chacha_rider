//
//  TXGoogleAPIUtil.h
//  Taxi

//

#import <Foundation/Foundation.h>
#import "TXHttpRequestManager.h"
#import "TXEventTarget.h"
#import "TXGoogleObjectsCache.h"

extern NSString const *EVENT_PROPERTY;

@class GMSPolyline;

@interface TXGoogleRequestManager : TXEventTarget<TXHttpRequestListener>

-(BOOL) sendPlaceNearbySearchAsync:(NSString *) location radius:(NSString *)radius sensor:(BOOL) sensor rankBy:(NSString *)rankBy optional:(NSString *) parameters;
-(BOOL) sendPlaceTextSearchAsync:(NSString *) query sensor:(BOOL) sensor optional:(NSString *) parameters;
-(BOOL) sendPlaceRadarSearchAsync:(NSString *) location radius:(NSString *)radius sensor:(BOOL) sensor optional:(NSString *) parameters;
-(BOOL) sendPlaceAutocompleteAsync:(NSString *) input sensor:(BOOL) sensor optional:(NSString *) parameters;

-(BOOL) sendDirectionsByCoordinatesAsync:(double) startLat startLongitude:(double)startLng endLocation:(NSString *) endLocation sensor:(BOOL) sensor optional:(NSString *) parameters;
-(BOOL) sendDirectionsMatrixByCoordinatesAsync:(double) startLat startLongitude:(double)startLng endLocation:(NSString *) endLocation sensor:(BOOL) sensor optional:(NSString *) parameters;

-(BOOL) sendDirectionsByCoordinatesSync:(double) startLat startLongitude:(double)startLng endLocation:(NSString *) endLocation sensor:(BOOL) sensor optional:(NSString *) parameters;

-(BOOL) sendTmapDirectionsByCoordinatesAsync:(double) startLat startLongitude:(double)startLng endLocation:(NSString *) endLocation sensor:(BOOL) sensor optional:(NSString *) parameters;
-(BOOL) sendTmapPoiByCoordinatesAsync:(NSString*) address optional:(NSString *) parameters;
-(BOOL) sendTmapGeocodingsByCoordinatesAsync:(double) startLat startLongitude:(double)startLng optional:(NSString *) parameters;

-(BOOL) sendTmapDirectionsByCoordinatesSync:(double) startLat startLongitude:(double)startLng endLocation:(NSString *) endLocation sensor:(BOOL) sensor optional:(NSString *) parameters;
-(BOOL) sendTmapDirectionsMatrixByCoordinatesAsync:(double) startLat startLongitude:(double)startLng endLocation:(NSString *) endLocation sensor:(BOOL) sensor optional:(NSString *) parameters;

-(BOOL) sendTmapDirectionsMatrixByCoordinatesSync:(double) startLat startLongitude:(double)startLng endLocation:(NSString *) endLocation sensor:(BOOL) sensor optional:(NSString *) parameters;

+(GMSPolyline *)polylineWithEncodedString:(NSString *)encodedString;
+(GMSPolyline *)polylineWithCoordinate:(NSArray*)coords;

-(TXEvent*)requestCompleted:(id)object;

@end
