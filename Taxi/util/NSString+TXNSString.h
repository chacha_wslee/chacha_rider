//
//  NSString+TXNSString.h
//  Taxi
//

//

#import <Foundation/Foundation.h>

@interface NSString (TXNSString)

-(NSString *) substringUpToFirstOccurance : (char) c;
-(NSString *) substringFromFirstOccurance : (char) c;
-(NSString *) substringUpToLastOccurance : (char) c;
-(NSString *) substringFromRightFromCharacter : (char) c;
-(NSString *) substringFromRightToCharacter : (char) c;
-(NSString *) substringFromLastOccurance: (NSString *) str;

-(int)indexOf:(char)c;
- (BOOL) containsString: (NSString*) substring;

@end
