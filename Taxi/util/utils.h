//
//  utils.h
//  Taxi
//

//

#ifndef __AMDCUTILS__
#define __AMDCUTILS__

#import <GoogleMaps/GoogleMaps.h>
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "Macro.h"
#import "RTLabel.h"
#import "RTUILabel.h"
#import "LGAlertView.h"
//#import "APIClient.h"
//#import "APIErrorHandler.h"

#import "libPhoneNumber_iOS/NBPhoneNumberUtil.h"
#import "libPhoneNumber_iOS/NBPhoneNumber.h"

#import "TXUILayers.h"
#import "TXTextField.h"
#import "TXButton.h"
#import "UIUtil.h"
#import "LGAlertView.h"


//#import "UIImage+Alpha.h"
//#import "UIImage+Resize.h"
//#import "UIImage+RoundedCorner.h"

#define AES_KEY             @"DBE5A9C6F68B6C6D724D6E3E9EBA5F8B"
#define AES_IV              @"C923D1C574EA6F6E452F4406D87A15F3"

#define CURRENCY_FORMAT(c) [NSString stringWithFormat:LocalizedStr(@"String.Currency"),[Utils numberFormater:c]]
#define CURRENCY_FORMAT2(c) [NSString stringWithFormat:LocalizedStr(@"String.Currency2"),[Utils numberFormater:c]]
#define CURRENCY_FORMAT3(c) [NSString stringWithFormat:LocalizedStr(@"Menu.Driver.Earning.Currency"),[Utils numberFormater:c]]
#define NUMBER_FORMAT(c) [Utils numberCheck:c]

NSString* TXLocalizedString(NSString* key, NSString* comment);
NSString* base64String(NSString* str);
NSString* attachmentNameFromDao(NSString* type, NSString* amdcRowId, NSString* originalFileName);

NSString* getJSONStr (id jsonObj);
id getJSONObj (NSString* jsonStr);
id getJSONObjFromData (NSData* jsonData);
id getStringFromObj (NSDictionary* dictionary);

NSString* date2MilliSecStr(NSDate* date);
unsigned long long date2MilliSecs(NSDate* date);

NSString* quoteString(NSString* src);
NSString* id2JSONStr(id newValue);
NSString* getDicStr(id newValue);

/**
 matches-contains or not target str a regular expression pattern
 
 @param regex - what to search in regular expression pattern
 @param targetStr - where to search
 @result - bool
 */
BOOL containsRegExprStr(NSString * regex, NSString * targetStr);
/**
 replaces into targetStr a regular expression pattern with replaceWithStr
 
 @param regexpStr - what to search in regular expression pattern
 @parAM replaceWithStr  - replacement string
 @param targetStr - where to search
 @result - replaced NSString
 */
NSString* replaceRegexpStr(NSString *regexpStr, NSString *replaceWithStr, NSString *targetStr);

NSData* getSHA256(NSString *str);
NSData* getSHA512(NSString *str);
NSString* getHexString(NSData * data);
NSString* getIPv4Address();

#endif //__AMDCUTILS__


@interface Utils : NSObject

+(NSString*)getConfigurationValueForKey:(NSString*)key;
+(UIImage *)image:(UIImage*)originalImage scaledToSize:(CGSize)size;
+ (void)downloadImageWithURL:(NSString *)code property:(NSDictionary*)pMap completionBlock:(void (^)(BOOL succeeded, UIImage *image))completionBlock;
+ (NSString*)getXHTTPType;
+ (NSString*)getXHTTPHeader;
+ (NSString*)getXHTTPToken;
+ (NSString*)getXHTTPLocation;
+ (NSString*)get20Header;
+ (NSString*)get20ImageHeader;

+ (NSString *)passcodeBysha256:(NSString *)passwd;
+ (NSString*)Encrypt:(NSString*)plainText;
+ (NSString*)Decrypt:(NSString*)base64Text;
+ (NSData*)EncryptData:(NSData*)plainText;
+ (NSData*)DecryptData:(NSData*)base64Text;
+ (id) clone:(id)target;
+ (int)ProfileLevel:(NSString*)trips;
+ (NSString*)ProfileLevelString:(NSString*)trips capitalizedString:(BOOL)isCapital;
+ (UIImage*)ProfileLevelImage:(NSString*)imageName;
+ (UIView*)addLine:(CGRect)viewFrame dim:(float)pix width:(float)w height:(float)h;
+ (NSDictionary*)getDataOfQueryString:(NSString*)url;
+(NSString*)appVersion;
+(NSString*)nameOfuname:(NSString*)uname__;
+ (NSString*)getMessageUserId:(NSDictionary*)mtext;
+ (UIImage*)getCountryFlag:(NSString*)country__;
+ (NSString*)getCountryCode;

+ (NSString *)replacingWithPattern:(NSString *)str pattern:(NSString *)pattern withTemplate:(NSString *)withTemplate error:(NSError **)error;
+ (void)updateCurrentPosition:(NSString*)lat lng:(NSString*)lng;
+ (void)updateSrcPosition:(NSString*)lat lng:(NSString*)lng;
+ (void)updateDstPosition:(NSString*)lat lng:(NSString*)lng;
+(NSString*)currentPosition;
+(CLLocationCoordinate2D)currentPositionCoor;
+(NSString*)destinationPosition;
+(CLLocationCoordinate2D)destinationPositionCoor;
+(NSString*)srcPosition;
+(CLLocationCoordinate2D)srcPositionCoor;

+(BOOL)checkAppInstalled;
+(BOOL)checkAppUpdate:(NSString*)ver;
+(NSString *) getSpaceReplacedWithPrcnt20:(NSString *) source;
+(NSString *) stringByStrippingHTML:(NSString*)s;
+(NSString *) addressToString:(GMSAddress*)address;
+(NSString*)getUserName;
+(NSString*)getDriverRate;
+(NSDictionary*)getEstimate:(NSString*)service;
+(NSString*)dicToString:(NSDictionary*)dic;
+(NSDictionary*)stringToDic:(NSString*)kJson;
+(NSString*)getStringFromDate:(NSDate*)date;
+(NSDate *)getDateFromString:(NSString *)string;
+(NSString*)getBCDSTEP;
+(void)setBCDSTEP:(NSString*)__step;
+(void)setFirstLogin;
+(BOOL)getIsFirstLogin;
//------------------------------------------------------------------------------------------------------------------------
+ (NSString *)encodeToBase64String:(UIImage *)image;
+ (UIImage *)decodeBase64ToImage:(NSString *)strEncodeData;
+ (NSInteger)getImageSize:(UIImage*)originalImage;
+ (UIImage *)resizeImage:(UIImage *)image;
//------------------------------------------------------------------------------------------------------------------------
+ (NSString*)getNumber:(NSString*)phone;
+ (NSString*)formatPhoneNum:(NSString*)phone;
+ (NSString*)NBPhoneNumber:(NSString*)phone locale:(NSString*)locale;
+ (NSString*)NBPhoneNumberRFC3966:(NSString*)phone locale:(NSString*)locale;
+ (NSString*)trimString:(NSString*)str;
+ (NSString*)trimStringOnly:(NSString*)str;
+ (NSString*)numberCheck:(NSString*)num;
+ (NSString*)numberFormater:(NSString*)number;
+ (NSString*)strTofloat:(NSString*)number;
+ (BOOL)nullToImage:(UIImage*)image;
+ (NSString*)nullToString:(id)string;
+ (NSString*)nullTolocation:(id)string;
+ (NSString*)nullToIntString:(id)string;
+ (NSString*)nullToStringNoTrim:(id)string;
+ (NSString*)nullToStringEscape:(id)string;
+ (NSString *)replaceSpace:(NSString *)input;
+ (NSString*)getAddressFull:(NSString*)full;
+ (NSString*)nameToString:(id)string;
+ (BOOL)validateEmailWithString:(NSString*)checkString;
+(BOOL)isLocationValid:(CLLocationCoordinate2D)coor;
+(BOOL)isLocationValid:(double)lat long:(double)lng;
+(void)updateApp:(BOOL)flag;

+ (double)meterTomi:(double)distance;
+ (double)miTometer:(double)distance;

//------------------------------------------------------------------------------------------------------------------------
+(int)checkStateUIInfo:(NSString*)key oldvalue:(NSString*)oldvalue;
+(void)setStateUIInfo:(NSString*)key value:(int)value oldvalue:(NSString*)oldvalue;
+(NSString*)getStateUIInfo:(NSString*)key;
+(void)resetStateUIInfo;

//------------------------------------------------------------------------------------------------------------------------
+ (CGSize) getBasicButtonSize:(UIView*)view;
+ (CGRect) getRect:(CGRect)btn size:(CGSize)size;
+ (void)labelAttrBasic:(UILabel*) obj;
+ (void)updateTextFontSize:(NSString*)text label:(UILabel*)label size:(NSInteger)size;
+ (void)updateTextFontSize2:(NSString*)text label:(RTLabel*)label size:(NSInteger)size;
+ (void)labelAttrHeader:(UILabel*) obj;
+ (void)buttonAttrRight:(UIButton*) obj;

//------------------------------------------------------------------------------------------------------------------------
+ (UILabel*)topHeader:(UIView*)view text:(NSString*)text;
+ (TXButton*)topHeaderRight:(UIView*)view text:(NSString*)text;
+ (UILabel*)topHeaderLabel:(UIView*)view text:(NSString*)text;
+ (UILabel*)leftLabel:(UIView*)view text:(NSString*)text dim:(NSInteger)dim;

+ (NSString*)labelStringUserInfo:(NSString*)trips rate:(NSString*)rate;
+ (void)labelMenuUserInfo:(UILabel*)label trips:(NSString*)trips rate:(NSString*)rate;
//------------------------------------------------------------------------------------------------------------------------
+ (UIView*)addBackView:(UIView*)view targetView:(UIView*)target;
+ (CGRect)frameUnder:(UIView *) view dim:(float) pix;
+ (float)frameOverY:(UIView *) view overView:(UIView *) overview dim:(float) pix;
+ (float)frameCenterX:(UIView *) view baseView:(UIView*)baseView;
+ (float)frameUnderY:(UIView *) view dim:(float) pix;

//-------------------------------------------------------------------------------------------------------------------------------------------------
+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize;
+ (UIImage *)imageFromInfoView:(NSString *) str image:(UIImage*)icon color:(UIColor*)color;
+ (void)stretching:(UIButton*)aButton image:(NSString*)image;
+ (void)stretchingView:(UIView*)aButton image:(NSString*)image;
+ (void)stretchingImageView:(UIImageView*)aButton image:(NSString*)image;
//-------------------------------------------------------------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------------------------------------------
+ (BOOL)isNillDictionary:(NSDictionary*)dic;
+ (BOOL)isNillArray:(NSArray*)dic;
+ (BOOL)isNill:(id)obj;
+ (BOOL)isDictionary:(NSDictionary*)dic;
+ (BOOL)isArray:(NSArray*)dic;
//------------------------------------------------------------------------------------------------------------------------

//+ (void)setCircleImage:(UIImageView*)view;
+ (void)setCircleImage:(UIView*)view;
+(UIImageView*)makeCircleProfile:(CGRect)rect image:(UIImage*)image tag:(NSInteger)tag;
+ (void)layerButton:(UIButton*)button;

//------------------------------------------------------------------------------------------------------------------------
+ (UIView*)addLabelView:(UIView*)view underView:(UIView*)target dim:(float) pix;
+ (UIView*)addDoubleLabelView:(UIView*)view underView:(UIView*)target dim:(float) pix;
+ (UIView*)addTweenLabelView:(UIView*)view underView:(UIView*)target dim:(float) pix;
+ (UIView*)addRTLabelView:(UIView*)view underView:(UIView*)target dim:(float) pix;
+ (UIView*)addFooterLabelView:(UIView*)view underView:(UIView*)target dim:(float) pix;
+ (UIView*)addInputTextView:(UIView*)view underView:(UIView*)target dim:(float) pix;
+ (UIView*)addWhiteLabelView:(UIView*)view underView:(UIView*)target dim:(float) pix;
+ (UIView*)addWhiteLabelViewSwitch:(UIView*)view underView:(UIView*)target dim:(float) pix;
+ (UIView*)addButtonBasic:(UIView*)view underView:(UIView*)target dim:(float) pix;
+ (void)addButtonBlank:(UIView*)view target:(id)target selector:(SEL)selector;
+ (UIButton*)addBottomButton:(NSString*)title target:(id)target selector:(SEL)selector color:(BOOL)color;
+ (UIButton*)addBottomButtonRound:(NSString*)title target:(id)target selector:(SEL)selector color:(BOOL)color;
+ (UIView*)addViewButtonText:(NSString*)title target:(id)target selector:(SEL)selector color:(BOOL)color;

+ (void)addShadow:(UIView*)v;
+ (void)addShadowOnlyRight:(UIView*)v;
+ (void)addBorder:(UIView*)view;
+ (void)RTLabelReSize:(UIView*)view tag:(NSInteger)tag;
+ (CGSize)cGRectLabel:(UILabel*)label;
+(BOOL) isNotAllowedMenu;
+(void) isNotOnlineMenu;
+(void)initAlertButtonColor:(LGAlertView*)alertView;
+ (void)onErrorAlert:(NSString *)failureMessage;
+ (void)onErrorAlert:(NSString *)failureMessage kwtlong:(BOOL)kwtlong;
+ (void)onErrorAlertView:(NSString *)failureMessage;
+ (void)onErrorAlertViewTitle:(NSString *)failureMessage title:(NSString*)title;
+ (void)onErrorAlertViewTitle:(NSString *)failureMessage title:(NSString*)title timeout:(NSInteger)timeout;
+ (void)onErrorAlertViewTitleAlign:(NSString *)failureMessage title:(NSString*)title align:(NSTextAlignment)align;

+ (void)runBlock:(void (^)())block;
+ (void)runAfterDelay:(CGFloat)delay block:(void (^)())block;

+(void)updateChatInfo:(NSArray*)mtext;
+(void)updateBGChatInfo:(NSString*)oseq;
+(BOOL)checkAlrim:(NSString*)type;
+(BOOL)checkAlrim:(NSString*)type code:(NSString*)code;
+(BOOL)showAlrim;
+(BOOL)showAlrim:(NSString*)type;
+(BOOL)showAlrim:(NSString*)type code:(NSString*)code;
+(void)deleteAlrim:(NSString*)type code:(NSString*)code;
+(void)updateAlrim:(NSString*)type code:(NSString*)code value:(NSString*)val;

+(void)newBadge:(UIButton*)btnBadge show:(BOOL)flag;

+(void)sendNotofication:(NSString*)msg title:(NSString*)title;
+(void)sendNotofication:(NSString*)msg;

//------------------------------------------------------------------------------------------------------------------------
#pragma mark - Additional Utils
+(BOOL)pointCompare:(double)param1 comp:(double)param2;
+(NSString*)formattedPositionR:(CLLocationCoordinate2D)position;
+(NSString*)formattedPosition:(CLLocationCoordinate2D)position;
+(double)formattedPositionCoor:(double)coor;

+(BOOL) isEqualLocation:(CLLocationCoordinate2D) coord1 location:(CLLocationCoordinate2D) coord2;
+(CLLocationCoordinate2D) getLocation:(NSString*) location;
+(NSString*) getLocationByString:(CLLocationCoordinate2D) location;
+(double) geoConvert:(double)dist unit:(NSString*)unit;

+(NSString*)acall:(NSDictionary*)item;

+ (NSDate*)StringFormat2Date:(NSString *)dateStr;
+ (NSString*)StringFormattedDate:(NSString *)dateStr format:(NSString *)format;
@end
