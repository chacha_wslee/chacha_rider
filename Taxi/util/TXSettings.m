//
//  TXSettings.m
//  Taxi
//

//

#import "TXSettings.h"
#import "StrConsts.h"
#import "Macro.h"
#import "TXError.h"
#import "FDKeyChain.h"


static NSString* const HTTPAPI_PLIST_FILE = @"httpapi";

@interface TXSettings()

-(TXSettings*)init;

-(void)initWithDefaults;
-(void)initWithObject:(NSDictionary*)userProfile;
-(void)onRequestCompleted:(id)object;
-(void)onFail:(id)object error:(TXError *)error;

@end

@implementation TXSettings

+(TXSettings *) instance {
    static dispatch_once_t settingsPred;
    static TXSettings* _settingsInstance;
    dispatch_once(&settingsPred, ^{ _settingsInstance = [[self alloc] init]; });
    return _settingsInstance;
}


-(id) init {
	self = [super init];
	if ( self ) {
//        NSURL* t = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
//		self->stgPath = [[t URLByAppendingPathComponent:Files.SETTINGSFILE] path];
//		NSData *plistXML = [[NSFileManager defaultManager] contentsAtPath:stgPath];
//        
//		NSPropertyListFormat format;
//		NSString *errorString;
//		self->root = [NSPropertyListSerialization propertyListFromData:plistXML
//                                                mutabilityOption:NSPropertyListMutableContainersAndLeaves
//                                                          format:&format
//                                                errorDescription:&errorString];
		if (self->root == nil) {
            self->root = [[NSMutableDictionary alloc] initWithCapacity:10];
            [self initWithDefaults];
		}
        
	}
	return self;
}

-(void)saveSettings {
    [root writeToFile:self->stgPath atomically:YES];
}

-(void)initWithDefaults {

    [self setProperty:SettingsConst.Property.BASEURL value:API_SERVER_IP];
    [self setProperty:SettingsConst.Property.PORT value:API_SERVER_PORT];
    [self setProperty:SettingsConst.Property.URLPATH value:API_SERVER_URI];
    
//    [self setUserName:@"tomcat"];
//    [self setPassword:@"tomcat"];
    
    NSString* path = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:Files.BUNDLE_PATH];
    NSBundle *thisBundle = [NSBundle bundleWithPath:path];
    NSString *plistPath = [thisBundle pathForResource:HTTPAPI_PLIST_FILE ofType:@"plist"];
    NSDictionary* rootObj = [NSDictionary dictionaryWithContentsOfFile:plistPath];
    [self setProperty:SettingsConst.Property.HTTP_API value:rootObj];
}

//receives server side parameters for the user, for initial settings will have default user settings, and admin settings
-(void)initWithObject:(NSDictionary*)userProfile {
    for (NSString *key in [userProfile keyEnumerator]) {
        [self setProperty:key value:[userProfile objectForKey:key]];
    }
}

-(void)setProperty:(NSString*)property value:(id)value {
    
    if ( [property length] > 0 && value != nil ) {
        [self->root setObject:value forKey:property];
    }
}

-(id)getProperty:(NSString*)property {
    if ( [property length] > 0 )
        return [self->root objectForKey:property];
    DLogE(@"Illegal parameter, empty key name!");
    return nil;
    
    return [NSNull null];
}

-(NSString*)getUserTelno {
//    return [FDKeychain itemForKey:SettingsConst.CryptoKeys.UTELNO
//                       forService:FDKEYCHAIN_SERVICE];
    
    NSError *error = nil;
    return [FDKeychain itemForKey: SettingsConst.CryptoKeys.UTELNO
                       forService: FDKEYCHAIN_SERVICE
                            error: &error];
}

-(NSString*)getUserId {
//    return [FDKeychain itemForKey:SettingsConst.CryptoKeys.USERID
//                       forService:FDKEYCHAIN_SERVICE];
    
    NSError *error = nil;
    return [FDKeychain itemForKey: SettingsConst.CryptoKeys.USERID
                       forService: FDKEYCHAIN_SERVICE
                            error: &error];
}

-(NSString*)getPassword {
//    return [FDKeychain itemForKey:SettingsConst.CryptoKeys.PASSWORD
//                       forService:FDKEYCHAIN_SERVICE];
    
    NSError *error = nil;
    return [FDKeychain itemForKey: SettingsConst.CryptoKeys.PASSWORD
                       forService: FDKEYCHAIN_SERVICE
                            error: &error];
}

-(void)setUserTelno:(NSString*)userTelno {
//    [FDKeychain saveItem:userTelno forKey:SettingsConst.CryptoKeys.UTELNO
//              forService:FDKEYCHAIN_SERVICE];
    
    NSError *error = nil;
    [FDKeychain saveItem: userTelno
                  forKey: SettingsConst.CryptoKeys.UTELNO
              forService: FDKEYCHAIN_SERVICE
                   error: &error];
    
}

-(void)setUserId:(NSString*)userId {
//    [FDKeychain saveItem:userId forKey:SettingsConst.CryptoKeys.USERID
//              forService:FDKEYCHAIN_SERVICE];

    NSError *error = nil;
    [FDKeychain saveItem: userId
                  forKey: SettingsConst.CryptoKeys.USERID
              forService: FDKEYCHAIN_SERVICE
                   error: &error];
}

-(void)setPassword:(NSString*)pwd {
//    [FDKeychain saveItem:pwd forKey:SettingsConst.CryptoKeys.PASSWORD
//              forService:FDKEYCHAIN_SERVICE];
    
    NSError *error = nil;
    [FDKeychain saveItem: pwd
                  forKey: SettingsConst.CryptoKeys.PASSWORD
              forService: FDKEYCHAIN_SERVICE
                   error: &error];
}

-(void) setGoogleUserId : (NSString *)userId {
//    [FDKeychain saveItem:userId forKey:SettingsConst.SignInProviders.Google.USERID
//              forService:FDKEYCHAIN_SERVICE];
    
    NSError *error = nil;
    [FDKeychain saveItem: userId
                  forKey: SettingsConst.SignInProviders.Google.USERID
              forService: FDKEYCHAIN_SERVICE
                   error: &error];
}

-(NSString*)getGoogleUserId {
//    return [FDKeychain itemForKey:SettingsConst.SignInProviders.Google.USERID
//                       forService:FDKEYCHAIN_SERVICE];
    
    NSError *error = nil;
    return [FDKeychain itemForKey: SettingsConst.SignInProviders.Google.USERID
                       forService: FDKEYCHAIN_SERVICE
                            error: &error];
}

-(void) setFBUserId : (NSString *)userId {
//    [FDKeychain saveItem:userId forKey:SettingsConst.SignInProviders.Facebook.USERID
//              forService:FDKEYCHAIN_SERVICE];
    
    NSError *error = nil;
    [FDKeychain saveItem: userId
                  forKey: SettingsConst.SignInProviders.Facebook.USERID
              forService: FDKEYCHAIN_SERVICE
                   error: &error];
}

-(NSString*)getFBUserId {
//    return [FDKeychain itemForKey:SettingsConst.SignInProviders.Facebook.USERID
//                       forService:FDKEYCHAIN_SERVICE];
    
    NSError *error = nil;
    return [FDKeychain itemForKey: SettingsConst.SignInProviders.Facebook.USERID
                       forService: FDKEYCHAIN_SERVICE
                            error: &error];
}

-(void)setUserToken:(NSString *)token {
//    [FDKeychain saveItem:token forKey:SettingsConst.CryptoKeys.USERTOKEN
//              forService:FDKEYCHAIN_SERVICE];
    
    NSError *error = nil;
    [FDKeychain saveItem: token
                  forKey: SettingsConst.CryptoKeys.USERTOKEN
              forService: FDKEYCHAIN_SERVICE
                   error: &error];
}

-(void)removeUserToken {
//    [FDKeychain deleteItemForKey:SettingsConst.CryptoKeys.USERTOKEN
//                forService:FDKEYCHAIN_SERVICE];
    
    NSError *error = nil;
    [FDKeychain deleteItemForKey: SettingsConst.CryptoKeys.USERTOKEN
                      forService: FDKEYCHAIN_SERVICE
                           error: &error];
}

-(NSString *)getUserToken {
//    return [FDKeychain itemForKey:SettingsConst.CryptoKeys.USERTOKEN
//                       forService:FDKEYCHAIN_SERVICE];
    
    NSError *error = nil;
    return [FDKeychain itemForKey: SettingsConst.CryptoKeys.USERTOKEN
                       forService: FDKEYCHAIN_SERVICE
                            error: &error];
}

-(void)setDeviceID:(NSString *)deviceid {
    NSString *UDID = @"";
    if([deviceid isEqualToString:@""])
        UDID = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    else
        UDID = deviceid;
    NSLog(@"UDID:%@",UDID);
//    [FDKeychain saveItem:UDID forKey:SettingsConst.CryptoKeys.DEVICEID
//              forService:FDKEYCHAIN_SERVICE];
    
    NSError *error = nil;
    [FDKeychain saveItem: UDID
                  forKey: SettingsConst.CryptoKeys.DEVICEID
              forService: FDKEYCHAIN_SERVICE
                   error: &error];
}

-(NSString *)getDeviceID {
//    return [FDKeychain itemForKey:SettingsConst.CryptoKeys.DEVICEID
//                       forService:FDKEYCHAIN_SERVICE];
    
    NSError *error = nil;
    return [FDKeychain itemForKey: SettingsConst.CryptoKeys.DEVICEID
                       forService: FDKEYCHAIN_SERVICE
                            error: &error];
}

-(NSString *)getFDKeychain:(NSString*)key {
//    return [FDKeychain itemForKey:key
//                       forService:FDKEYCHAIN_SERVICE];
    
    NSError *error = nil;
    return [FDKeychain itemForKey: key
                       forService: FDKEYCHAIN_SERVICE
                            error: &error];
}

-(void)setFDKeychain:(NSString *)key value:(NSString*)str {
//    [FDKeychain saveItem:str forKey:key
//              forService:FDKEYCHAIN_SERVICE];
    
    NSError *error = nil;
    [FDKeychain saveItem: str
                  forKey: key
              forService: FDKEYCHAIN_SERVICE
                   error: &error];
}

-(void)removeFDKeychain:(NSString *)key {
    //    [FDKeychain deleteItemForKey:SettingsConst.CryptoKeys.USERTOKEN
    //                forService:FDKEYCHAIN_SERVICE];
    
    NSError *error = nil;
    [FDKeychain deleteItemForKey: key
                      forService: FDKEYCHAIN_SERVICE
                           error: &error];
}

-(void) setNotificationsToken : (NSString *)token {
//    [FDKeychain saveItem:token forKey:SettingsConst.CryptoKeys.NOTIFICATIONS_TOKEN
//              forService:FDKEYCHAIN_SERVICE];
    
    NSError *error = nil;
    [FDKeychain saveItem: token
                  forKey: SettingsConst.CryptoKeys.NOTIFICATIONS_TOKEN
              forService: FDKEYCHAIN_SERVICE
                   error: &error];
}

-(NSString*)getNotificationsToken {
//    return [FDKeychain itemForKey:SettingsConst.CryptoKeys.NOTIFICATIONS_TOKEN
//                       forService:FDKEYCHAIN_SERVICE];
    
    NSError *error = nil;
    return [FDKeychain itemForKey: SettingsConst.CryptoKeys.NOTIFICATIONS_TOKEN
                       forService: FDKEYCHAIN_SERVICE
                            error: &error];
}

-(void) setDriveMode : (NSString *)mode {
    NSError *error = nil;
    [FDKeychain saveItem: mode
                  forKey: SettingsConst.CryptoKeys.MODE
              forService: FDKEYCHAIN_SERVICE
                   error: &error];
}

-(NSString*)getDriveMode {
    NSError *error = nil;
    return [FDKeychain itemForKey: SettingsConst.CryptoKeys.MODE
                       forService: FDKEYCHAIN_SERVICE
                            error: &error];
}

-(int) getMaxHTTPConnectionsNumber {
    return [ [self getProperty:SettingsConst.Property.MAXHTTPCONNECTIONS] intValue];
}

-(void)onRequestCompleted:(id)object {
    
}

-(void)onFail:(id)object error:(TXError *)error {
   
}

@end
