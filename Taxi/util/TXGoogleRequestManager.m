//
//  TXGoogleAPIUtil.m
//  Taxi
//
//

#import "TXGoogleRequestManager.h"
#import "utils.h"
#import <GoogleMaps/GoogleMaps.h>
#import "StrConsts.h"

/* ===================== KEYWORDS ===================== */

static NSString* const K_JSON = @"JSON";
static NSString* const K_PREDICTIONS = @"predictions";
static NSString* const K_ID = @"id";
static NSString* const K_DESCRIPTION = @"description";

/* ===================== REQUESTS ===================== */

static NSString* const GOOGLE_KEY = _GOOGLE_HTTP_KEY;

@implementation TXGoogleRequestManager {
    TXHttpRequestManager *httpMgr;
}



-(id)init {
    
    if(self=[super init]) {
        self->httpMgr = [TXHttpRequestManager instance];
    }
    
    return self;
}

- (void)postNotification:(NSDictionary *)object
{
    [[NSNotificationCenter defaultCenter] postNotificationName:[object objectForKey:@"name"] object:[object objectForKey:@"data"]];
}

-(BOOL) sendPlaceNearbySearchAsync:(NSString *) location radius:(NSString *)radius sensor:(BOOL) sensor rankBy:(NSString *)rankBy optional:(NSString *) parameters {
    if(location.length!=0 && radius.length!=0 && rankBy.length!=0) {
        
        location = [self getSpaceReplacedWithPrcnt20:location];
        
        NSMutableString *params = [NSMutableString stringWithFormat:@"key=%@", GOOGLE_KEY];
        [params appendFormat:@"&location=%@", location];
        [params appendFormat:@"&radius=%@", radius];
        [params appendFormat:@"&sensor=%@", (sensor == YES ? @"true" : @"false")];
        [params appendFormat:@"&rankby=%@", rankBy];
        
        if(parameters.length!=0) {
            [params appendFormat:@"&%@", parameters];
        }
        
        TXRequestObj* request = [TXRequestObj create:GoogleAPIRequestConsts.PLACES_NEARBYSEARCH urlParams:params listener:self];
        return [self sendAsyncRequest:request];
        
    } else {
        
        return NO;
        
    }
}

-(BOOL) sendPlaceTextSearchAsync:(NSString *) query sensor:(BOOL) sensor optional:(NSString *) parameters {
   
    if(query.length!=0) {
        
        query = [self getSpaceReplacedWithPrcnt20:query];
        
        NSMutableString *params = [NSMutableString stringWithFormat:@"key=%@", GOOGLE_KEY];
        [params appendFormat:@"&query=%@", query];
        [params appendFormat:@"&sensor=%@", (sensor == YES ? @"true" : @"false")];
        
        if(parameters.length!=0) {
            [params appendFormat:@"&%@", parameters];
        }
        
        TXRequestObj* request = [TXRequestObj create:GoogleAPIRequestConsts.PLACES_TEXTSEARCH urlParams:params listener:self];
        return [self sendAsyncRequest:request];
        
    } else {
        
        return NO;
        
    }
}

-(BOOL) sendPlaceRadarSearchAsync:(NSString *) location radius:(NSString *)radius sensor:(BOOL) sensor optional:(NSString *) parameters {
    
    if(location.length!=0 && radius.length!=0) {
        
        location = [self getSpaceReplacedWithPrcnt20:location];
        
        NSMutableString *params = [NSMutableString stringWithFormat:@"key=%@", GOOGLE_KEY];
        [params appendFormat:@"&location=%@", location];
        [params appendFormat:@"&radius=%@", radius];
        [params appendFormat:@"&sensor=%@", (sensor == YES ? @"true" : @"false")];
        
        if(parameters.length!=0) {
            [params appendFormat:@"&%@", parameters];
        }
        
        TXRequestObj* request = [TXRequestObj create:GoogleAPIRequestConsts.PLACES_RADARSEARCH urlParams:params listener:self];
        return [self sendAsyncRequest:request];
        
    } else {
        
        return NO;
        
    }
}

-(BOOL) sendPlaceAutocompleteAsync:(NSString *) input sensor:(BOOL) sensor optional:(NSString *) parameters {
    
    if(input.length!=0) {
        
        input = [self getSpaceReplacedWithPrcnt20:input];
        
        NSMutableString *params = [NSMutableString stringWithFormat:@"key=%@", GOOGLE_KEY];
        [params appendFormat:@"&input=%@", input];
        [params appendFormat:@"&sensor=%@", (sensor == YES ? @"true" : @"false")];
        
        if(parameters.length!=0) {
            [params appendFormat:@"&%@", parameters];
        }
        
        TXRequestObj* request = [TXRequestObj create:GoogleAPIRequestConsts.PLACES_AUTOCOMPLETE urlParams:params listener:self];
        return [self sendAsyncRequest:request];
        
    } else {
        
        return NO;
        
    }
}

-(BOOL) sendDirectionsByCoordinatesAsync:(double) startLat startLongitude:(double)startLng endLocation:(NSString *) endLocation sensor:(BOOL) sensor optional:(NSString *) parameters {
    
    if(endLocation.length!=0) {
        
        endLocation = [self getSpaceReplacedWithPrcnt20:endLocation];
        
        NSMutableString *params = [NSMutableString stringWithFormat:@"key=%@", GOOGLE_KEY];
        [params appendFormat:@"&origin=%f,%f", startLat, startLng];
        [params appendFormat:@"&destination=%@", endLocation];
        [params appendFormat:@"&departure_time=now"];
        //[params appendFormat:@"&avoid=tolls,highways"];
        [params appendFormat:@"&avoid=tolls"];
        [params appendFormat:@"&traffic_model=best_guess"];
        [params appendFormat:@"&mode=driving"];
        //[params appendFormat:@"&sensor=%@", (sensor == YES ? @"true" : @"false")];
        
        if(parameters.length!=0) {
            [params appendFormat:@"&%@", parameters];
        }
        
        TXRequestObj* request = [TXRequestObj create:GoogleAPIRequestConsts.DIRECTIONS_BYCOORDINATES urlParams:params listener:self];
#ifdef _DEBUG_ACTIVITY
        NSString *data = [NSString stringWithFormat:@"%@,%f,%f,%@,%@",request.reqConfig.name, startLat, startLng,endLocation,parameters];
        [self performSelectorOnMainThread:@selector(postNotification:) withObject:[NSDictionary dictionaryWithObjectsAndKeys:@"EDQueueJobDidSucceed", @"name", data, @"data", nil] waitUntilDone:false];
#endif
        return [self sendAsyncRequest:request];
        
    } else {
        
        return NO;
        
    }
}

-(BOOL) sendDirectionsMatrixByCoordinatesAsync:(double) startLat startLongitude:(double)startLng endLocation:(NSString *) endLocation sensor:(BOOL) sensor optional:(NSString *) parameters {
    
    if(endLocation.length!=0) {
        
        endLocation = [self getSpaceReplacedWithPrcnt20:endLocation];
        
        NSMutableString *params = [NSMutableString stringWithFormat:@"key=%@", GOOGLE_KEY];
        [params appendFormat:@"&origins=%f,%f", startLat, startLng];
        [params appendFormat:@"&destinations=%@", endLocation];
        [params appendFormat:@"&departure_time=now"];
        //[params appendFormat:@"&avoid=tolls,highways"];
        [params appendFormat:@"&avoid=tolls"];
        [params appendFormat:@"&traffic_model=best_guess"];
        [params appendFormat:@"&mode=driving"];
        
        if(parameters.length!=0) {
            [params appendFormat:@"&%@", parameters];
        }
        
        TXRequestObj* request = [TXRequestObj create:GoogleAPIRequestConsts.DIRECTIONS_MATRIX_BYCOORDINATES urlParams:params listener:self];
        
#ifdef _DEBUG_ACTIVITY
        NSString *data = [NSString stringWithFormat:@"%@,%@",request.reqConfig.name, [Utils getXHTTPLocation]];
        [self performSelectorOnMainThread:@selector(postNotification:) withObject:[NSDictionary dictionaryWithObjectsAndKeys:@"EDQueueJobDidSucceed", @"name", data, @"data", nil] waitUntilDone:false];
#endif
        return [self sendAsyncRequest:request];
        
    } else {
        
        return NO;
        
    }
}

-(BOOL) sendDirectionsByCoordinatesSync:(double) startLat startLongitude:(double)startLng endLocation:(NSString *) endLocation sensor:(BOOL) sensor optional:(NSString *) parameters {
    
    if(endLocation.length!=0) {
        
        endLocation = [self getSpaceReplacedWithPrcnt20:endLocation];
        
        NSMutableString *params = [NSMutableString stringWithFormat:@"key=%@", GOOGLE_KEY];
        [params appendFormat:@"&origin=%f,%f", startLat, startLng];
        [params appendFormat:@"&destination=%@", endLocation];
        [params appendFormat:@"&departure_time=now"];
        //[params appendFormat:@"&avoid=tolls,highways"];
        [params appendFormat:@"&avoid=tolls"];
        [params appendFormat:@"&traffic_model=best_guess"];
        [params appendFormat:@"&mode=driving"];
        
        if(parameters.length!=0) {
            [params appendFormat:@"&%@", parameters];
        }
        
        TXRequestObj* request = [TXRequestObj create:GoogleAPIRequestConsts.DIRECTIONS_BYCOORDINATES urlParams:params listener:self];
        return [self sendSyncRequest:request];
        
    } else {
        
        return NO;
        
    }
    
}

-(BOOL) sendTmapDirectionsByCoordinatesAsync:(double) startLat startLongitude:(double)startLng endLocation:(NSString *) endLocation sensor:(BOOL) sensor optional:(NSString *) parameters {
    
    if(endLocation.length!=0) {
        
        endLocation = [self getSpaceReplacedWithPrcnt20:endLocation];
        
        NSArray* loc = [endLocation componentsSeparatedByString:@","];
        
        NSMutableString *params = [NSMutableString stringWithFormat:@"startX=%f", startLng];
        [params appendFormat:@"&startY=%f", startLat];
        [params appendFormat:@"&directionOption=1"];
        [params appendFormat:@"&reqCoordType=WGS84GEO"];
        [params appendFormat:@"&resCoordType=WGS84GEO"];
        [params appendFormat:@"&endX=%@", loc[1]];
        [params appendFormat:@"&endY=%@", loc[0]];
        
        if(parameters.length!=0) {
            [params appendFormat:@"&%@", parameters];
        }
//        [params appendFormat:@"&viaY=%@", @"127.101072"];
//        [params appendFormat:@"&viaX=%@", @"37.403017"];
//
        TXRequestObj* request = [TXRequestObj create:TmapAPIRequestConsts.TMAP_DIRECTIONS_BYCOORDINATES urlParams:nil listener:self];
//        if ([request.reqUrl containsString:@"tmap/routes"] && [params containsString:@"viaX"]) {
//            request.reqUrl = [request.reqUrl stringByReplacingOccurrencesOfString:@"tmap/routes"
//                                                                       withString:@"tmap/routes/routeSequential30"];
//            request.reqConfig.url = request.reqUrl;
//        }
        
#ifdef _DEBUG_ACTIVITY
        NSString *data = [NSString stringWithFormat:@"%@,%@,%@",request.reqConfig.name, [Utils getXHTTPLocation],params];
        [self performSelectorOnMainThread:@selector(postNotification:) withObject:[NSDictionary dictionaryWithObjectsAndKeys:@"EDQueueJobDidSucceed", @"name", data, @"data", nil] waitUntilDone:false];
#endif
        request.body = params;
        return [self sendAsyncRequest:request];
        
    } else {
        
        return NO;
        
    }
    
}

-(BOOL) sendTmapGeocodingsByCoordinatesAsync:(double) startLat startLongitude:(double)startLng optional:(NSString *) parameters {
    
//lon=127.100989&callback=&coordType=WGS84GEO&addressType=A02&lat=37.402984&version=1
    
    NSMutableString *params = [NSMutableString stringWithFormat:@"lon=%f", startLng];
    [params appendFormat:@"&lat=%f", startLat];
    [params appendFormat:@"&addressType=A02"];
    [params appendFormat:@"&coordType=WGS84GEO"];
    
    if(parameters.length!=0) {
        [params appendFormat:@"&%@", parameters];
    }

    TXRequestObj* request = [TXRequestObj create:TmapAPIRequestConsts.TMAP_GEOCODING_BYCOORDINATES urlParams:nil listener:self];

#ifdef _DEBUG_ACTIVITY
    NSString *data = [NSString stringWithFormat:@"%@,%@,%@",request.reqConfig.name, [Utils getXHTTPLocation],params];
    [self performSelectorOnMainThread:@selector(postNotification:) withObject:[NSDictionary dictionaryWithObjectsAndKeys:@"EDQueueJobDidSucceed", @"name", data, @"data", nil] waitUntilDone:false];
#endif
    request.body = params;
    return [self sendAsyncRequest:request];
}

-(BOOL) sendTmapPoiByCoordinatesAsync:(NSString*) address optional:(NSString *) parameters {
    
    //areaLMCode=&centerLon=&centerLat=&count=5&page=1&reqCoordType=WGS84GEO&searchKeyword=판교역&callback=&areaLLCode=&multiPoint=&searchtypCd=&radius=&searchType=&resCoordType=WGS84GEO&version=1
    
    NSString *escaped = [address stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSMutableString *params = [NSMutableString stringWithFormat:@"searchKeyword=%@", escaped];
    [params appendFormat:@"&count=5"];
    [params appendFormat:@"&page=1"];
    [params appendFormat:@"&reqCoordType=WGS84GEO"];
    [params appendFormat:@"&resCoordType=WGS84GEO"];
    
    if(parameters.length!=0) {
        [params appendFormat:@"&%@", parameters];
    }
    
    TXRequestObj* request = [TXRequestObj create:TmapAPIRequestConsts.TMAP_POI_BYCOORDINATES urlParams:nil listener:self];
    
#ifdef _DEBUG_ACTIVITY
    NSString *data = [NSString stringWithFormat:@"%@,%@,%@",request.reqConfig.name, [Utils getXHTTPLocation],params];
    [self performSelectorOnMainThread:@selector(postNotification:) withObject:[NSDictionary dictionaryWithObjectsAndKeys:@"EDQueueJobDidSucceed", @"name", data, @"data", nil] waitUntilDone:false];
#endif
    request.body = params;
    return [self sendAsyncRequest:request];
}

-(BOOL) sendTmapDirectionsByCoordinatesSync:(double) startLat startLongitude:(double)startLng endLocation:(NSString *) endLocation sensor:(BOOL) sensor optional:(NSString *) parameters {
    
    if(endLocation.length!=0) {
        
        endLocation = [self getSpaceReplacedWithPrcnt20:endLocation];
        
        NSArray* loc = [endLocation componentsSeparatedByString:@","];
        
        NSMutableString *params = [NSMutableString stringWithFormat:@"startX=%f", startLng];
        [params appendFormat:@"&startY=%f", startLat];
        [params appendFormat:@"&directionOption=1"];
        [params appendFormat:@"&reqCoordType=WGS84GEO"];
        [params appendFormat:@"&resCoordType=WGS84GEO"];
        [params appendFormat:@"&endX=%@", loc[1]];
        [params appendFormat:@"&endY=%@", loc[0]];
        
        if(parameters.length!=0) {
            [params appendFormat:@"&%@", parameters];
        }
        
        TXRequestObj* request = [TXRequestObj create:TmapAPIRequestConsts.TMAP_DIRECTIONS_BYCOORDINATES urlParams:nil listener:self];
        request.body = params;
        return [self sendSyncRequest:request];
        
    } else {
        
        return NO;
        
    }
    
}

-(BOOL) sendTmapDirectionsMatrixByCoordinatesAsync:(double) startLat startLongitude:(double)startLng endLocation:(NSString *) endLocation sensor:(BOOL) sensor optional:(NSString *) parameters {
    
    if(endLocation.length!=0) {
        
        endLocation = [self getSpaceReplacedWithPrcnt20:endLocation];
        
        NSArray* loc = [endLocation componentsSeparatedByString:@","];
        
        NSMutableString *params = [NSMutableString stringWithFormat:@"startX=%f", startLng];
        [params appendFormat:@"&startY=%f", startLat];
        [params appendFormat:@"&directionOption=1"];
        [params appendFormat:@"&reqCoordType=WGS84GEO"];
        [params appendFormat:@"&resCoordType=WGS84GEO"];
        [params appendFormat:@"&endX=%@", loc[1]];
        [params appendFormat:@"&endY=%@", loc[0]];
        
        if(parameters.length!=0) {
            [params appendFormat:@"&%@", parameters];
        }
        
        TXRequestObj* request = [TXRequestObj create:TmapAPIRequestConsts.TMAP_DIRECTIONS_MATRIX_BYCOORDINATES urlParams:nil listener:self];
#ifdef _DEBUG_ACTIVITY
        NSString *data = [NSString stringWithFormat:@"%@,%@,%@",request.reqConfig.name, [Utils getXHTTPLocation],params];
        [self performSelectorOnMainThread:@selector(postNotification:) withObject:[NSDictionary dictionaryWithObjectsAndKeys:@"EDQueueJobDidSucceed", @"name", data, @"data", nil] waitUntilDone:false];
#endif
        request.body = params;
        return [self sendAsyncRequest:request];
        
    } else {
        
        return NO;
        
    }
    
}

-(BOOL) sendTmapDirectionsMatrixByCoordinatesSync:(double) startLat startLongitude:(double)startLng endLocation:(NSString *) endLocation sensor:(BOOL) sensor optional:(NSString *) parameters {
    
    if(endLocation.length!=0) {
        
        endLocation = [self getSpaceReplacedWithPrcnt20:endLocation];
        
        NSArray* loc = [endLocation componentsSeparatedByString:@","];
        
        NSMutableString *params = [NSMutableString stringWithFormat:@"startX=%f", startLng];
        [params appendFormat:@"&startY=%f", startLat];
        [params appendFormat:@"&directionOption=1"];
        [params appendFormat:@"&reqCoordType=WGS84GEO"];
        [params appendFormat:@"&resCoordType=WGS84GEO"];
        [params appendFormat:@"&endX=%@", loc[1]];
        [params appendFormat:@"&endY=%@", loc[0]];
        
        if(parameters.length!=0) {
            [params appendFormat:@"&%@", parameters];
        }
        
        TXRequestObj* request = [TXRequestObj create:TmapAPIRequestConsts.TMAP_DIRECTIONS_MATRIX_BYCOORDINATES urlParams:nil listener:self];
        request.body = params;
        return [self sendSyncRequest:request];
        
    } else {
        
        return NO;
        
    }
    
}

-(void)onRequestCompleted:(id)object {
    
    [self fireEvent:[self requestCompleted:object]];
}

-(TXEvent*)requestCompleted:(id)object {
    
    TXRequestObj *request  = (TXRequestObj *)object;
    NSString     *response = [[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding];
    id           jsonObj   = getJSONObj(response);
    id           prop      = nil;
    NSString     *evtName  = nil;
    
    if([request.reqConfig.name isEqualToString:GoogleAPIRequestConsts.PLACES_AUTOCOMPLETE]) {
        
        NSArray *predictions = [jsonObj objectForKey:K_PREDICTIONS];
        NSMutableArray *array = [NSMutableArray arrayWithCapacity:[predictions count]];
        
        for (NSDictionary *pred in predictions) {
            
            TXPrediction *prediction = [TXPrediction create];
            prediction.id_ = [pred objectForKey:K_ID];
            prediction.description_ = [pred objectForKey:K_DESCRIPTION];
            
            [array addObject:prediction];
            
        }
        
        evtName = TXEvents.GOOGLE_PLACES_AUTOCOMP_REQ_COMPLETED;
        prop = array;
        
    } else if ([request.reqConfig.name isEqualToString:GoogleAPIRequestConsts.DIRECTIONS_BYCOORDINATES]) {
        
        NSString *text = nil;
        long value = 0;
        NSDictionary *responseObj = getJSONObj([[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding]);
        NSString *status = [(NSDictionary*)responseObj objectForKey:@"status"];
        if ([status isEqualToString:@"OK"]) {
            NSArray *routes = [(NSDictionary*)responseObj objectForKey:@"routes"];
            NSArray *legs = [[routes objectAtIndex:0] objectForKey:@"legs"];
            
            NSDictionary *distance = [legs[0] objectForKey:@"distance"];
            text = [distance objectForKey:@"text"];
            value = [[distance objectForKey:@"value"] longValue];
            
            evtName = TXEvents.GOOGLE_DIRECTIONS_REQ_COMPLETED;
        }
        else {
            evtName = TXEvents.GOOGLE_REQ_FAILED;
            NSLog(@"GMS Status:%@",status);
            text = status;
        }
        
        prop = @{ @"description" : text, @"distance" : [NSNumber numberWithLong:value] };
    } else if ([request.reqConfig.name isEqualToString:GoogleAPIRequestConsts.DIRECTIONS_MATRIX_BYCOORDINATES]) {
        
        NSString *text = nil;
        long value = 0;
        NSDictionary *responseObj = getJSONObj([[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding]);
        NSString *status = [(NSDictionary*)responseObj objectForKey:@"status"];
        if ([status isEqualToString:@"OK"]) {
            text = status;
            //            NSArray *routes = [(NSDictionary*)responseObj objectForKey:@"routes"];
            //            NSArray *legs = [[routes objectAtIndex:0] objectForKey:@"legs"];
            //
            //            NSDictionary *distance = [legs[0] objectForKey:@"distance"];
            //            text = [distance objectForKey:@"text"];
            //            value = [[distance objectForKey:@"value"] longValue];
            
            evtName = TXEvents.GOOGLE_DIRECTIONS_MATRIX_REQ_COMPLETED;
        }
        else {
            evtName = TXEvents.GOOGLE_REQ_FAILED;
            NSLog(@"GMS Status:%@",status);
            text = status;
        }
        
        prop = @{ @"description" : text, @"distance" : [NSNumber numberWithLong:value] };
    } else if ([request.reqConfig.name isEqualToString:TmapAPIRequestConsts.TMAP_DIRECTIONS_BYCOORDINATES]) {
        
        long distance = 0;
        long value = 0;
        NSDictionary *responseObj = getJSONObj([[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding]);
        NSArray *features = [(NSDictionary*)responseObj objectForKey:@"features"];
        if ([features count]) {
            
            distance = [[[[features objectAtIndex:0] objectForKey:@"properties"] objectForKey:@"totalDistance"] longValue];
            value = [[[[features objectAtIndex:0] objectForKey:@"properties"] objectForKey:@"taxiFare"] longValue];
            
            evtName = TXEvents.TMAP_DIRECTIONS_REQ_COMPLETED;
        }
        else {
            evtName = TXEvents.TMAP_REQ_FAILED;
        }
        
        prop = @{ @"taxiFare" : [NSNumber numberWithLong:value], @"distance" : [NSNumber numberWithLong:distance] };
    } else if ([request.reqConfig.name isEqualToString:TmapAPIRequestConsts.TMAP_DIRECTIONS_MATRIX_BYCOORDINATES]) {
        
        long distance = 0;
        long value = 0;
        NSDictionary *responseObj = getJSONObj([[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding]);
        NSArray *features = [(NSDictionary*)responseObj objectForKey:@"features"];
        if ([features count]) {
            
            distance = [[[[features objectAtIndex:0] objectForKey:@"properties"] objectForKey:@"totalDistance"] longValue];
            value = [[[[features objectAtIndex:0] objectForKey:@"properties"] objectForKey:@"taxiFare"] longValue];
            
            evtName = TXEvents.TMAP_DIRECTIONS_MATRIX_REQ_COMPLETED;
        }
        else {
            evtName = TXEvents.TMAP_REQ_FAILED;
        }
        
        prop = @{ @"taxiFare" : [NSNumber numberWithLong:value], @"distance" : [NSNumber numberWithLong:distance] };
    }
    
    return [TXEvent createEvent:evtName eventSource:self eventProps:@{ TXEvents.Params.GOOGLEOBJECT : prop, TXEvents.Params.REQUEST    : request  }];
}

-(void)onFail:(id)object error:(TXError *)error {
    
}

+(GMSPolyline *)polylineWithEncodedString:(NSString *)encodedString {
    
    const char *bytes = [encodedString UTF8String];
    NSUInteger length = [encodedString lengthOfBytesUsingEncoding:NSUTF8StringEncoding];
    NSUInteger idx = 0;
    
    NSUInteger count = length / 4;
    CLLocationCoordinate2D *coords = calloc(count, sizeof(CLLocationCoordinate2D));
    NSUInteger coordIdx = 0;
    
    float latitude = 0;
    float longitude = 0;
    while (idx < length) {
        char byte = 0;
        int res = 0;
        char shift = 0;
        
        do {
            byte = bytes[idx++] - 63;
            res |= (byte & 0x1F) << shift;
            shift += 5;
        } while (byte >= 0x20);
        
        float deltaLat = ((res & 1) ? ~(res >> 1) : (res >> 1));
        latitude += deltaLat;
        
        shift = 0;
        res = 0;
        
        do {
            byte = bytes[idx++] - 0x3F;
            res |= (byte & 0x1F) << shift;
            shift += 5;
        } while (byte >= 0x20);
        
        float deltaLon = ((res & 1) ? ~(res >> 1) : (res >> 1));
        longitude += deltaLon;
        
        float finalLat = latitude * 1E-5;
        float finalLon = longitude * 1E-5;
        
        CLLocationCoordinate2D coord = CLLocationCoordinate2DMake(finalLat, finalLon);
        coords[coordIdx++] = coord;
        
        if (coordIdx == count) {
            NSUInteger newCount = count + 10;
            coords = realloc(coords, newCount * sizeof(CLLocationCoordinate2D));
            count = newCount;
        }
    }
    
    GMSMutablePath *path = [[GMSMutablePath alloc] init];
    
    int i;
    for (i = 0; i < coordIdx; i++)
    {
        [path addCoordinate:coords[i]];
    }
    
    GMSPolyline *polyline = [GMSPolyline polylineWithPath:path];
    free(coords);
    
    return polyline;
}

+(GMSPolyline *)polylineWithCoordinate:(NSArray*)coords{
    
    NSInteger coordIdx = [coords count];
    
    GMSMutablePath *path = [[GMSMutablePath alloc] init];
    int i;
    for (i = 0; i < coordIdx; i++)
    {
        CLLocationCoordinate2D coord = CLLocationCoordinate2DMake([coords[i][1] floatValue], [coords[i][0] floatValue]);
        [path addCoordinate:coord];
    }
    
    GMSPolyline *polyline = [GMSPolyline polylineWithPath:path];
    
    return polyline;
}


-(NSString *) getSpaceReplacedWithPrcnt20:(NSString *) source {
    return [source stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
}

-(BOOL) sendAsyncRequest:(TXRequestObj *) request {
    return [self->httpMgr sendAsyncRequest:request];
}

-(BOOL) sendSyncRequest:(TXRequestObj *) request {
    return [self->httpMgr sendSyncRequest:request];
}

@end
