@implementation UITableView (UITableViewAddition)

-(void)reloadDataAndWait:(void(^)(void))waitBlock {
    [self reloadData];
    if(waitBlock){
        waitBlock();
    }
}
@end

