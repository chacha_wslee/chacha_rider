//
//  utils.m
//  Taxi
//

//
#import <QuartzCore/QuartzCore.h>
#import <sys/utsname.h>
#import "utils.h"
#import "StrConsts.h"
#import "NSString+TXNSString.h"
#import "NSString+sha256.h"
#import "NSData+AES.h"
#import <CommonCrypto/CommonDigest.h>
#import "TXApp.h"
#import "UIImage+ResizeMagick.h"
#import "TXHttpRequestManager.h"
#import "TXUserModel.h"

#import "TXAppDelegate.h"
#import "WToast.h"
//#import "CRToast.h"

#import "WZLBadgeImport.h"
#import "UIButton+Badge.h"

#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import <CoreTelephony/CTCarrier.h>

#pragma clang diagnostic ignored "-Warc-performSelector-leaks"

//load Taxi resource bundle if not already loaded and retreive localized string by key
NSString* TXLocalizedString(NSString* key, NSString* comment) {
    static NSBundle *bundle = nil;
    if (bundle == nil)
    {
        NSString* bundlePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:Files.BUNDLE_PATH];
        
        bundle = [NSBundle bundleWithPath:bundlePath];
        NSString *language = [[NSLocale preferredLanguages] count]? [NSLocale preferredLanguages][0]: DEFAULT_LANG;
#ifdef _WITZM
        language = DEFAULT_LANG; // 강제설정
#elif defined _CHACHA
        language = DEFAULT_LANG; // 강제설정
#endif
        NSLog(@"Current locale: %@, Bundle localizations: %@, %@", [[NSLocale currentLocale] localeIdentifier], [bundle localizations], language);
        if (![[bundle localizations] containsObject:language])
        {
            language = [language componentsSeparatedByString:@"-"][0];
        }
        if ([[bundle localizations] containsObject:language])
        {
            bundlePath = [bundle pathForResource:language ofType:@"lproj"];
        }
        
        bundle = [NSBundle bundleWithPath:bundlePath] ?: [NSBundle mainBundle];
    }
    NSString* defaultString = [bundle localizedStringForKey:key value:key table:@"lang"];
    //NSLog(@"defaultString:%@",defaultString);
    return [[NSBundle mainBundle] localizedStringForKey:key value:defaultString table:@"lang"];
}

NSString* base64String(NSString* str) {
    NSData *theData = [str dataUsingEncoding: NSASCIIStringEncoding];
    const uint8_t* input = (const uint8_t*)[theData bytes];
    NSInteger length = [theData length];
    
    static char table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    
    NSMutableData* data = [NSMutableData dataWithLength:((length + 2) / 3) * 4];
    uint8_t* output = (uint8_t*)data.mutableBytes;
    
    NSInteger i;
    for (i=0; i < length; i += 3) {
        NSInteger value = 0;
        NSInteger j;
        for (j = i; j < (i + 3); j++) {
            value <<= 8;
            
            if (j < length) {
                value |= (0xFF & input[j]);
            }
        }
        
        NSInteger theIndex = (i / 3) * 4;
        output[theIndex + 0] =                    table[(value >> 18) & 0x3F];
        output[theIndex + 1] =                    table[(value >> 12) & 0x3F];
        output[theIndex + 2] = (i + 1) < length ? table[(value >> 6)  & 0x3F] : '=';
        output[theIndex + 3] = (i + 2) < length ? table[(value >> 0)  & 0x3F] : '=';
    }
    
    return [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
}

NSString* attachmentNameFromDao(NSString* type, NSString* amdcRowId, NSString* originalFileName) {

    if ([originalFileName rangeOfString:@"."].location == NSNotFound) {
        DLogE(@"Invalid file name, %@ does not contain extension.", originalFileName);
        return nil;
    }
    
    return [NSString stringWithFormat:@"%@_%@.%@", type, amdcRowId, [originalFileName substringUpToLastOccurance:'.']];
    
}

BOOL containsRegExprStr(NSString * regex, NSString * targetStr) {
    NSPredicate *regextest = [NSPredicate predicateWithFormat:@"SELF MATCHES[c] %@", regex];
    if ([regextest evaluateWithObject:targetStr] == YES) {
        /* when matchess template */
        return YES;
    }
    else {
        /* when not matches */
        return NO;
    }
}

NSString* replaceRegexpStr(NSString *regexpStr, NSString *replaceWithStr, NSString *targetStr) {
    NSError *error = NULL;
    NSRegularExpression * regexStr = [NSRegularExpression regularExpressionWithPattern:regexpStr options:NSRegularExpressionCaseInsensitive error:&error] ;
    NSString * replacedStr = [regexStr stringByReplacingMatchesInString:targetStr options:0 range:NSMakeRange(0, [targetStr length]) withTemplate:replaceWithStr];
    return replacedStr;
}

/*********************** Json Utils ************************/

NSString* getJSONStr (id jsonObj) {
    
#ifndef DEBUG
    NSJSONWritingOptions options = 0;
#else
    NSJSONWritingOptions options = NSJSONWritingPrettyPrinted;
#endif
    
    if ( jsonObj != nil ) {
        
        if ( ![jsonObj isKindOfClass:[NSString class]] ) {
            NSError *error_;
            NSData* jsonData = [NSJSONSerialization dataWithJSONObject:jsonObj options:options error:&error_];
            return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        }
        return jsonObj;
    }
    
    return @"null";
}

inline id getJSONObj (NSString* jsonStr) {
    NSError *error_;
    return [NSJSONSerialization JSONObjectWithData:[jsonStr dataUsingEncoding:NSUTF8StringEncoding] options:0 error:&error_] ;
}

inline id getStringFromObj (NSDictionary* dictionary) {
    NSError *error_;
    return [NSJSONSerialization dataWithJSONObject:dictionary options:0 error:&error_];
}


inline id getJSONObjFromData (NSData* jsonData) {
    NSError *error_;
    return [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&error_] ;
}

NSString* id2JSONStr(id newValue) {
    NSString* valStr;
    //add quotes around all values except numbers
    if(newValue == nil || newValue == [NSNull null]) {
        valStr = @"null";
    } else if (![newValue isKindOfClass:[NSNumber class]] )
        valStr = [NSString stringWithFormat:@"\"%@\"", newValue];
    else
        valStr = [NSString stringWithFormat:@"%@", newValue];
    return valStr;
}

NSString* getDicStr(id newValue) {
    NSError *error;
    NSData *json = [NSJSONSerialization dataWithJSONObject:newValue
                                                   options:kNilOptions
                                                     error:&error];
    if(!json && error){
        NSLog(@"Error creating JSON: %@", [error localizedDescription]);
        return @"null";
    }
    NSString *jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
    // This will be the json string in the preferred format
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\\/" withString:@"/"];
    //jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\"" withString:@"\\\""];
    jsonString = [jsonString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]; // 앞뒤 공백제거
    return jsonString;
}
/*********************** Json Utils ************************/


/*********************** Date utils ************************/
inline NSString* date2MilliSecStr(NSDate* date) {
    return [NSString stringWithFormat:@"%llu", date2MilliSecs(date)];
}

inline unsigned long long date2MilliSecs(NSDate* date) {
    return (unsigned long long)[date timeIntervalSince1970]*MILLISECONDSINSECOND;
}

inline NSString* quoteString(NSString* src) {
    if ( src != nil )
        return [NSString stringWithFormat:@"\"%@\"", src];
    else
        return @"\"\"";
}

/*********************** Date utils ************************/

/*********************** SHA ************************/

NSData* getSHA256(NSString *str) {
    
    NSData *dataIn = [str dataUsingEncoding:NSASCIIStringEncoding];
    NSMutableData *dataOut = [NSMutableData dataWithLength:CC_SHA256_DIGEST_LENGTH];
    CC_SHA256(dataIn.bytes, (double)dataIn.length,  dataOut.mutableBytes);
    
    return dataOut;
    
}

NSString* getHexString(NSData * data) {
    /* Returns hexadecimal string of NSData. Empty string if data is empty.   */
    
    const unsigned char *dataBuffer = (const unsigned char *)[data bytes];
    
    if (!dataBuffer)
    {
        return [NSString string];
    }
    
    NSUInteger          dataLength  = [data length];
    NSMutableString     *hexString  = [NSMutableString stringWithCapacity:(dataLength * 2)];
    
    for (int i = 0; i < dataLength; ++i)
    {
        [hexString appendString:[NSString stringWithFormat:@"%02lx", (unsigned long)dataBuffer[i]]];
    }
    
    return [NSString stringWithString:hexString];
}

NSData* getSHA512(NSString *str) {
    
    NSData *dataIn = [str dataUsingEncoding:NSASCIIStringEncoding];
    NSMutableData *dataOut = [NSMutableData dataWithLength:CC_SHA512_DIGEST_LENGTH];
    CC_SHA512(dataIn.bytes, (double)dataIn.length,  dataOut.mutableBytes);
    return dataOut;
    
}

/*********************** SHA ************************/

/*********************** DEVICE *********************/

NSString* getIPv4Address() {
    
    id host =[NSClassFromString(@"NSHost") performSelector:NSSelectorFromString(@"currentHost")];
    if (host) {
        for (NSString* address in [host performSelector:NSSelectorFromString(@"addresses")]) {
            if ([address rangeOfString:@"::"].location == NSNotFound) {
                return address;
            }
        }
    }
    
    return @"127.0.0.1";
}

/****************************************************/

@implementation Utils

+(NSString*)getConfigurationValueForKey:(NSString*)key {
    NSDictionary *mainDictionary = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Configuration" ofType:@"plist"]];
    
    return [mainDictionary objectForKey:key];
}

//
//NSString* deviceName()
//{
//    struct utsname systemInfo;
//    uname(&systemInfo);
//    
//    return [NSString stringWithCString:systemInfo.machine
//                              encoding:NSUTF8StringEncoding];
//}

+ (void)downloadImageWithURL:(NSString *)code property:(NSDictionary*)pMap completionBlock:(void (^)(BOOL succeeded, UIImage *image))completionBlock
{
    TXRequestObj *request     = [[TXUserModel instance] createRequest:code];
    
    request.body = getJSONStr(pMap);
    
    NSMutableURLRequest *httpRequest = [request createHTTPRequest];
    DLogI(@"Sent Async Request to URL - %@\n\n", request.reqUrl);
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:httpRequest
                                            completionHandler:
                                  ^(NSData *data, NSURLResponse *response, NSError *error) {
                                      if ([response isKindOfClass:[NSHTTPURLResponse class]] &&
                                          ((NSHTTPURLResponse *)response).statusCode == 200 &&
                                          error == nil &&
                                          data != nil) {
                                          if ([data length]) {
                                              
                                              UIImage *image = [UIImage imageWithData:data];
                                              completionBlock(YES,image);
                                          }
                                          else {
                                              if ([code isEqualToString:@"PP603"] || [code isEqualToString:@"UR603"]) {
                                                  completionBlock(YES,[UIImage imageNamed:@"img_car_small"]);
                                              }
                                              else {
                                                  completionBlock(YES,[UIImage imageNamed:@"ic_user_white"]);
                                              }
                                          }
                                      } else {
                                          completionBlock(NO,nil);
                                      }
                                  }];
    [task resume];
    /*
     [NSURLConnection sendAsynchronousRequest:httpRequest
     queue:[NSOperationQueue mainQueue]
     completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
     if ( !error )
     {
     if ([data length]) {
     
     UIImage *image = [UIImage imageWithData:data];
     completionBlock(YES,image);
     }
     else {
     completionBlock(YES,[UIImage imageNamed:@"img_car_small"]);
     }
     } else{
     completionBlock(NO,nil);
     }
     }];
     */
}

+ (NSString*)getXHTTPType {
    
    NSString* mode = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.MODE];
    NSString* running = @"forground";
    if([UIApplication sharedApplication].applicationState != UIApplicationStateActive) {
        running = @"background";
    }
    //mode=driving|training
    NSString *xsString = [NSString stringWithFormat:@"service=drive,mode=%@,running=%@,version=%@",mode,running,PROTOCOL_VER];
    return xsString;
}


+ (NSString*)getXHTTPHeader {
    
    NSString* deviceID = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.DEVICEID];
    NSString* useq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.USEQ];
    NSString* cseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.CSEQ];
    NSString* pseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.PSEQ];
    //NSString* vseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.VSEQ];
    
//    NSString *langID    = [[NSLocale preferredLanguages] objectAtIndex:0];
//    NSArray* foo = [langID componentsSeparatedByString: @"-"];
//    NSString *lang = [foo objectAtIndex:0]; // [0]language [1]national
    NSString *lang = [[NSLocale preferredLanguages] count]? [NSLocale preferredLanguages][0]: DEFAULT_LANG;
    lang = [lang componentsSeparatedByString:@"-"][0];
    
    if ([lang isEqualToString:@""]) {
        lang = DEFAULT_LANG;
    }
#ifdef _WITZM
    lang = DEFAULT_LANG; // 강제설정
#elif defined _CHACHA
    lang = DEFAULT_LANG; // 강제설정
#endif
    //NSLog(@"lang:%@:%@",lang,language);
    //NSString *mode = [NSString stringWithFormat:@"%@",[UIDevice currentDevice].model];
    
    struct utsname systemInfo;
    uname(&systemInfo);
    
    NSString *mode =  [NSString stringWithCString:systemInfo.machine
                              encoding:NSUTF8StringEncoding];
    mode = [mode stringByReplacingOccurrencesOfString:@"," withString:@"_"];
    
    NSString* os = [NSString stringWithFormat:@"%@:%@",[UIDevice currentDevice].systemName, [UIDevice currentDevice].systemVersion];
    NSDictionary *infoDictionary = [[NSBundle mainBundle]infoDictionary];
    NSString *version = infoDictionary[@"CFBundleShortVersionString"];
    NSString *app = [NSString stringWithFormat:@"%@:%@",[[NSBundle mainBundle] bundleIdentifier],version];
    
    //NSString *xsString = [NSString stringWithFormat:@"etype=text,useq=%@,cseq=%@,pseq=%@,vseq=%@,did=%@,lang=%@,model=%@,os=%@,app=%@",
//                          useq,cseq,pseq,vseq,deviceID,lang,mode,os,app];
    NSString *xsString = [NSString stringWithFormat:@"etype=text,useq=%@,cseq=%@,pseq=%@,did=%@,lang=%@,model=%@,os=%@,app=%@",
                          useq,cseq,pseq,deviceID,lang,mode,os,app];
    return xsString;
}

+ (NSString*)getXHTTPToken {
    
    NSString *xsString = [NSString stringWithFormat:@"atoken=%@",
                          [[[TXApp instance] getSettings] getUserToken]];
    return xsString;
}

+ (NSString*)getXHTTPLocation {
#ifdef _DRIVER_MODE
    NSString* lat = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.LAT];
    NSString* lng = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.LNG];
    
    NSString *xsString = [NSString stringWithFormat:@"lat=%@,lng=%@",
                          lat,lng];
    return xsString;
#else
    return [Utils getXHTTPLocationRider];
#endif
}

+ (NSString*)getXHTTPLocationRider {
    
    NSString* lat = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.SLAT];
    NSString* lng = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.SLNG];
    
    if (![Utils isLocationValid:[lat doubleValue] long:[lng doubleValue]]) {
        lat = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.LAT];
        lng = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.LNG];
    }
    NSString *xsString = [NSString stringWithFormat:@"lat=%@,lng=%@",
                          lat,lng];
    return xsString;
}

+ (NSString*)get20Header {
    
    NSString* deviceID = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.DEVICEID];
    deviceID = [deviceID stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLUserAllowedCharacterSet]];
    //    NSString *langID    = [[NSLocale preferredLanguages] objectAtIndex:0];
    //    NSArray* foo = [langID componentsSeparatedByString: @"-"];
    //    NSString *lang = [foo objectAtIndex:0]; // [0]language [1]national
    NSString *lang = [[NSLocale preferredLanguages] count]? [NSLocale preferredLanguages][0]: DEFAULT_LANG;
    lang = [lang componentsSeparatedByString:@"-"][0];
    
    if ([lang isEqualToString:@""]) {
        lang = DEFAULT_LANG;
    }
#ifdef _WITZM
    lang = DEFAULT_LANG; // 강제설정
#elif defined _CHACHA
    lang = DEFAULT_LANG; // 강제설정
#endif
    //NSLog(@"lang:%@:%@",lang,language);
    //NSString *mode = [NSString stringWithFormat:@"%@",[UIDevice currentDevice].model];
    
    struct utsname systemInfo;
    uname(&systemInfo);
    
    NSString *model =  [NSString stringWithCString:systemInfo.machine
                                         encoding:NSUTF8StringEncoding];
    model = [model stringByReplacingOccurrencesOfString:@"," withString:@"_"];
    
    NSString* os = [NSString stringWithFormat:@"%@:%@",[UIDevice currentDevice].systemName, [UIDevice currentDevice].systemVersion];
    os = [os stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLUserAllowedCharacterSet]];
    NSDictionary *infoDictionary = [[NSBundle mainBundle]infoDictionary];
    NSString *version = infoDictionary[@"CFBundleShortVersionString"];
    NSString *app = [NSString stringWithFormat:@"%@:%@",[[NSBundle mainBundle] bundleIdentifier],version];
    
    NSString* lat = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.SLAT];
    NSString* lng = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.SLNG];
    
//    if (![Utils isLocationValid:[lat doubleValue] long:[lng doubleValue]]) {
        lat = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.LAT];
        lng = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.LNG];
//    }
    
    NSString* mode = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.MODE];
    NSString* running = @"forground";
    if([UIApplication sharedApplication].applicationState != UIApplicationStateActive) {
        running = @"background";
    }
    
    NSString *datoken = [[[TXApp instance] getSettings] getUserToken];
//    NSString *xsString = [NSString stringWithFormat:@"time=%d&datoken=%@&location=%@,%@&lang=%@&model=%@&os=%@&app=%@&service=drive&mode=%@&running=%@&version=%@",
//                          (int)[[NSDate date] timeIntervalSince1970],[[[TXApp instance] getSettings] getUserToken],lat,lng,lang,model,os,app,mode,running,PROTOCOL_VER];
    NSString *xsString = [NSString stringWithFormat:@"did=%@&time=%d&location=%@,%@&lang=%@&model=%@&os=%@&app=%@&service=drive&mode=%@&running=%@&version=%@",
                          deviceID,(int)[[NSDate date] timeIntervalSince1970],lat,lng,lang,model,os,app,mode,running,PROTOCOL_VER];
    if (datoken && datoken != nil) {
        xsString = [NSString stringWithFormat:@"did=%@&time=%d&datoken=%@&location=%@,%@&lang=%@&model=%@&os=%@&app=%@&service=drive&mode=%@&running=%@&version=%@",
                    deviceID,(int)[[NSDate date] timeIntervalSince1970],datoken,lat,lng,lang,model,os,app,mode,running,PROTOCOL_VER];
    }
    
    return xsString;
}

+ (NSString*)get20ImageHeader {
    
    NSString* deviceID = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.DEVICEID];
    deviceID = [deviceID stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLUserAllowedCharacterSet]];
    //    NSString *langID    = [[NSLocale preferredLanguages] objectAtIndex:0];
    //    NSArray* foo = [langID componentsSeparatedByString: @"-"];
    //    NSString *lang = [foo objectAtIndex:0]; // [0]language [1]national
    NSString *lang = [[NSLocale preferredLanguages] count]? [NSLocale preferredLanguages][0]: DEFAULT_LANG;
    lang = [lang componentsSeparatedByString:@"-"][0];
    
    if ([lang isEqualToString:@""]) {
        lang = DEFAULT_LANG;
    }
#ifdef _WITZM
    lang = DEFAULT_LANG; // 강제설정
#elif defined _CHACHA
    lang = DEFAULT_LANG; // 강제설정
#endif
    //NSLog(@"lang:%@:%@",lang,language);
    //NSString *mode = [NSString stringWithFormat:@"%@",[UIDevice currentDevice].model];
    
    struct utsname systemInfo;
    uname(&systemInfo);
    
    NSString *model =  [NSString stringWithCString:systemInfo.machine
                                          encoding:NSUTF8StringEncoding];
    model = [model stringByReplacingOccurrencesOfString:@"," withString:@"_"];
    
    NSString* os = [NSString stringWithFormat:@"%@:%@",[UIDevice currentDevice].systemName, [UIDevice currentDevice].systemVersion];
    os = [os stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLUserAllowedCharacterSet]];
    NSDictionary *infoDictionary = [[NSBundle mainBundle]infoDictionary];
    NSString *version = infoDictionary[@"CFBundleShortVersionString"];
    NSString *app = [NSString stringWithFormat:@"%@:%@",[[NSBundle mainBundle] bundleIdentifier],version];
    
    NSString* lat = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.SLAT];
    NSString* lng = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.SLNG];
    
//    if (![Utils isLocationValid:[lat doubleValue] long:[lng doubleValue]]) {
        lat = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.LAT];
        lng = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.LNG];
//    }
    
    NSString* mode = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.MODE];
    NSString* running = @"forground";
    if([UIApplication sharedApplication].applicationState != UIApplicationStateActive) {
        running = @"background";
    }
    
    NSString *datoken = [[[TXApp instance] getSettings] getUserToken];
//    NSString *xsString = [NSString stringWithFormat:@"datoken=%@&location=%@,%@&lang=%@&model=%@&os=%@&app=%@&service=drive&mode=%@&running=%@&version=%@",
//                          [[[TXApp instance] getSettings] getUserToken],lat,lng,lang,model,os,app,mode,running,PROTOCOL_VER];
    NSString *xsString = [NSString stringWithFormat:@"did=%@&location=%@,%@&lang=%@&model=%@&os=%@&app=%@&service=drive&mode=%@&running=%@&version=%@",
                          deviceID,lat,lng,lang,model,os,app,mode,running,PROTOCOL_VER];
    if (datoken && datoken != nil) {
        xsString = [NSString stringWithFormat:@"did=%@&datoken=%@&location=%@,%@&lang=%@&model=%@&os=%@&app=%@&service=drive&mode=%@&running=%@&version=%@",
                    deviceID,datoken,lat,lng,lang,model,os,app,mode,running,PROTOCOL_VER];
    }
    return xsString;
}

+ (NSString *)passcodeBysha256:(NSString *)passwd
{
    // Create the hash
    //    NSData *passwordData = [passwd sha256Data];
    //    NSString* password = [[NSString alloc] initWithData:passwordData encoding:NSUTF8StringEncoding];
    //    NSString *password = [NSString base64StringFromData:passwordData];
    
    NSString* password = [passwd sha256];
    return password;
}


+ (NSString*)Encrypt:(NSString*)plainText {
    NSData *cipherData;
    NSString *base64Text;
    
    cipherData = [[plainText dataUsingEncoding:NSUTF8StringEncoding] AES128EncryptedDataWithKey:AES_KEY iv:AES_IV];
    base64Text = [cipherData base64EncodedStringWithOptions:0];
    
    return base64Text;
}

+ (NSString*)Decrypt:(NSString*)base64Text {
    NSData *cipherData;
    NSString *plainText;
    
    cipherData = [[[NSData alloc] initWithBase64EncodedString:base64Text options:0] AES128DecryptedDataWithKey:AES_KEY iv:AES_IV];
    plainText = [[NSString alloc] initWithData:cipherData encoding:NSUTF8StringEncoding];
    
    return plainText;
}

+ (NSData*)EncryptData:(NSData*)plainText {
    NSData *cipherData;
    NSString *base64Text;
    
    cipherData = [plainText AES128EncryptedDataWithKey:AES_KEY iv:AES_IV];
    base64Text = [cipherData base64EncodedStringWithOptions:0];
    NSData* data = [base64Text dataUsingEncoding:NSUTF8StringEncoding];
    
    return data;
}

+ (NSData*)DecryptData:(NSData*)base64Text {
    NSData *cipherData;
    NSString *plainText;
    
    cipherData = [base64Text AES128DecryptedDataWithKey:AES_KEY iv:AES_IV];
    plainText = [[NSString alloc] initWithData:cipherData encoding:NSUTF8StringEncoding];
    NSData* data = [plainText dataUsingEncoding:NSUTF8StringEncoding];
    
    return data;
}

+ (id) clone:(id)target {
    NSData *archivedViewData = [NSKeyedArchiver archivedDataWithRootObject: target];
    id clone = [NSKeyedUnarchiver unarchiveObjectWithData:[archivedViewData mutableCopy]];
    return clone;
}

+ (int)ProfileLevel:(NSString*)trips {
    int level = 0;
    
    int trip = [trips intValue];
    if (trip >= 0 && trip < 10) {
        level = 1;
    }
    else if (trip > 10 && trip < 20) {
        level = 2;
    }
    else if (trip > 10 && trip < 20) {
        level = 3;
    }
    else if (trip > 10 && trip < 20) {
        level = 4;
    }
    else if (trip > 10 && trip < 20) {
        level = 5;
    }
    else if (trip > 10 && trip < 20) {
        level = 6;
    }
    else {
        level = 7;
    }
    
    return level;
}

+ (NSString*)ProfileLevelString:(NSString*)trips capitalizedString:(BOOL)isCapital {
    NSString *imageName;
    
    //int trip = [Utils ProfileLevel:trips];
    int trip = [trips intValue];
    
    if (trip < 20)         imageName = @"debut";
    else if (trip <  500)  imageName = @"trainee";
    else if (trip < 1500)  imageName = @"bronze";
    else if (trip < 3000)  imageName = @"silver";
    else if (trip < 4500)  imageName = @"gold";
    else if (trip < 6000)  imageName = @"platinum";
    else if (trip < 7500)  imageName = @"diamond";
    else                   imageName = @"master";

    if (isCapital) {
        return [imageName capitalizedString];
    }
    return imageName;
}

+ (UIImage*)ProfileLevelImage:(NSString*)trips {
    UIImage *image;
    NSString* imageName= [Utils ProfileLevelString:trips capitalizedString:NO];
    image = [UIImage imageNamed:[NSString stringWithFormat:@"ic_level_%@",imageName]];
    return image;
}

+ (TXTextField*)txTextField:(CGRect)rect placeholder:(NSString*)holder {
    TXTextField *tf = [[TXTextField alloc]
                        initWithFrame:rect];
    tf.placeholder = holder;
    tf.opaque = NO;
    tf.autocapitalizationType = UITextAutocapitalizationTypeNone;
    tf.autoresizingMask = UITextAutocapitalizationTypeNone;
    
    return tf;
}

+ (UIView*)addLine:(CGRect)viewFrame dim:(float)pix width:(float)w height:(float)h{
    
    CGRect frame = CGRectMake(0,
                              (viewFrame.origin.y + viewFrame.size.height + pix),
                              w,
                              h);
    
    UIView *lineView = [[UIView alloc] initWithFrame:frame];
    lineView.backgroundColor = [UIColor redColor];
    return lineView;
}

+ (NSDictionary*)getDataOfQueryString:(NSString*)url {
    
    NSString* urlString = @"";
    
    NSArray *strURLParse = [url componentsSeparatedByString:@"?"];
    if ([strURLParse count] == 2) {
        urlString = strURLParse[1];
    }
    else {
        urlString = strURLParse[0];
    }
    
    NSMutableDictionary *queryStringDictionary = [[NSMutableDictionary alloc] init];
    NSArray *urlComponents = [urlString componentsSeparatedByString:@"&"];
    
    for (NSString *keyValuePair in urlComponents)
    {
        NSArray *pairComponents = [keyValuePair componentsSeparatedByString:@"="];
        NSString *key = [[pairComponents firstObject] stringByRemovingPercentEncoding];
        NSString *value = [[pairComponents lastObject] stringByRemovingPercentEncoding];
        
        [queryStringDictionary setObject:value forKey:key];
    }
    
    return queryStringDictionary;
}

+(NSString*)appVersion_old{
    //NSInteger ver = [Utils replacePoint:[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"]];
    NSString *strVer = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"];
    return strVer;
}

+(NSString*)appVersion{
    //NSInteger ver = [Utils replacePoint:[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"]];
    //NSString *strVer = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"];
    NSString *strVer = [NSString stringWithFormat:@"%@/%@",
                        [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"],
                        [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"]];
    return strVer;
}

+ (NSString *)escapeValueForURLParameter:(NSString *)valueToEscape {
    NSCharacterSet *set = [NSCharacterSet URLQueryAllowedCharacterSet];
    return [valueToEscape stringByAddingPercentEncodingWithAllowedCharacters:set];
}

+(void)updateApp:(BOOL)flag{
    
    static BOOL isclick = NO;
    
    if (isclick) {
        //return;
    }
    
    TXAppDelegate *appDelegate = (TXAppDelegate *)[[UIApplication sharedApplication] delegate];
    //    NSString *server_version;
    //    if (appDelegate.isDriverMode) {
    //        server_version = [appDelegate.dicDriver objectForKey:@"version"];
    //    }
    //    else {
    //        server_version = [appDelegate.dicRider objectForKey:@"version"];
    //    }
    //
    //
    if ([Utils checkAppUpdate:appDelegate.appversion]) {
        NSDictionary *dic = nil;
        BOOL forced_install = NO;
        if (appDelegate.isDriverMode) {
            dic = appDelegate.dicDriver[@"provider_config"];
        }
        else {
            dic = appDelegate.dicRider[@"user_config"];
        }
        
        if ([[Utils nullToString:dic[@"APP_VERSION_FORCE"]] isEqualToString:@"1"]) {
            forced_install = YES;
        }
        
        // update
        if (forced_install) {
            LGAlertView *alertView = [[LGAlertView alloc] initWithTitle:LocalizedStr(@"String.App.ForcedUpdate.title")
                                                                message:(![[Utils nullToString:dic[@"APP_VERSION_DESC"]] isEqualToString:@""]?dic[@"APP_VERSION_DESC"]:LocalizedStr(@"String.App.Update.text"))
                                                                  style:LGAlertViewStyleAlert
                                                           buttonTitles:@[LocalizedStr(@"Button.Update")]
                                                      cancelButtonTitle:nil
                                                 destructiveButtonTitle:nil
                                                          actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                                                              [alertView setButtonAtIndex:0 enabled:NO];
                                                              isclick = YES;
#ifdef _DEBUG
                                                              if(SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(@"11.0")) {
                                                                  [[UIApplication sharedApplication] openURL:[NSURL URLWithString:appDelegate.appstoreurl] options:@{} completionHandler:^(BOOL success) {
                                                                      if (success) {
                                                                          NSLog(@"Opened url");
                                                                          exit(0);
                                                                      }
                                                                  }];
                                                              }
                                                              else {
                                                                  [[UIApplication sharedApplication] openURL:[NSURL URLWithString:appDelegate.appstoreurl]];
                                                                  //exit(0);
                                                              }
                                                              
#else
                                                              [[UIApplication sharedApplication] openURL:[NSURL URLWithString:appDelegate.appstoreurl]];
                                                              //exit(0);
#endif
                                                              
                                                              
                                                          }
                                                          cancelHandler:^(LGAlertView *alertView) {
                                                              
                                                          }
                                                     destructiveHandler:^(LGAlertView *alertView) {
                                                         
                                                     }];
            [Utils initAlertButtonColor:alertView];
            alertView.cancelOnTouch = NO;
            [alertView setDismissOnAction:NO];
            [alertView showAnimated:YES completionHandler:^(void)
             {
                 
             }];
        }
        else {
            LGAlertView *alertView = [[LGAlertView alloc] initWithTitle:LocalizedStr(@"String.App.Update.title")
                                                                message:LocalizedStr(@"String.App.Update.text")
                                                                  style:LGAlertViewStyleAlert
                                                           buttonTitles:@[LocalizedStr(@"Button.YES")]
                                                      cancelButtonTitle:LocalizedStr(@"Button.NO")
                                                 destructiveButtonTitle:nil
                                                          actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                                                              isclick = YES;
                                                              NSLog(@"__IPHONE_OS_VERSION_MAX_ALLOWED:%d",__IPHONE_OS_VERSION_MAX_ALLOWED);
#ifdef _DEBUG
                                                              if(SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(@"11.0")) {
                                                                  [[UIApplication sharedApplication] openURL:[NSURL URLWithString:appDelegate.appstoreurl] options:@{} completionHandler:^(BOOL success) {
                                                                      if (success) {
                                                                          NSLog(@"Opened url");
                                                                          exit(0);
                                                                      }
                                                                  }];
                                                              }
                                                              else {
                                                                  [[UIApplication sharedApplication] openURL:[NSURL URLWithString:appDelegate.appstoreurl]];
                                                                  //exit(0);
                                                              }
#else
                                                              [[UIApplication sharedApplication] openURL:[NSURL URLWithString:appDelegate.appstoreurl]];
                                                              //exit(0);
#endif
                                                              
                                                          }
                                                          cancelHandler:^(LGAlertView *alertView) {
                                                              
                                                          }
                                                     destructiveHandler:^(LGAlertView *alertView) {
                                                         
                                                     }];
            [Utils initAlertButtonColor:alertView];
            [alertView showAnimated:YES completionHandler:^(void)
             {
                 
             }];
        }
        
        
    }
    else {
        if (!flag) return;
        LGAlertView *alertView = [[LGAlertView alloc] initWithTitle:LocalizedStr(@"String.App.Update.title")
                                                            message:LocalizedStr(@"String.App.Update.text2")
                                                              style:LGAlertViewStyleAlert
                                                       buttonTitles:@[LocalizedStr(@"Button.OK")]
                                                  cancelButtonTitle:nil
                                             destructiveButtonTitle:nil
                                                      actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                                                      }
                                                      cancelHandler:^(LGAlertView *alertView) {
                                                      }
                                                 destructiveHandler:^(LGAlertView *alertView) {
                                                 }];
        [Utils initAlertButtonColor:alertView];
        [alertView showAnimated:YES completionHandler:^(void)
         {
             dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                 [alertView dismissAnimated:YES completionHandler:nil];
             });
         }];
        [alertView showAnimated:YES completionHandler:nil];
    }
}

+(NSString*)nameOfuname:(NSString*)uname__{
    //NSString *name = [uname__ stringByReplacingOccurrencesOfString:@";" withString:@" "];
    NSString *name = [Utils nameToString:uname__];
//    name = [Utils trimString:name];
    return name;
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
+ (NSString*)getMessageUserId:(NSDictionary*)mtext
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    TXAppDelegate *appDelegate = (TXAppDelegate *)[[UIApplication sharedApplication] delegate];
    if (appDelegate.isDriverMode) {
        NSString *userId = [NSString stringWithFormat:@"pseq,%@",mtext[@"pseq"]];
        return userId;
    }
    else {
        NSString *userId = [NSString stringWithFormat:@"useq,%@",mtext[@"useq"]];
        return userId;
    }
}


+ (NSString*)NBPhoneNumber:(NSString*)phone locale:(NSString*)locale
{
    NBPhoneNumberUtil *phoneUtil = [[NBPhoneNumberUtil alloc] init];
    NSError *anError = nil;
    NBPhoneNumber *myNumber = [phoneUtil parse:phone
                                 defaultRegion:locale error:&anError];
    
    return [phoneUtil format:myNumber
                numberFormat:NBEPhoneNumberFormatE164
                       error:&anError];
}

+ (NSString*)NBPhoneNumberRFC3966:(NSString*)phone locale:(NSString*)locale
{
    NBPhoneNumberUtil *phoneUtil = [[NBPhoneNumberUtil alloc] init];
    NSError *anError = nil;
    NBPhoneNumber *myNumber = [phoneUtil parse:phone
                                 defaultRegion:locale error:&anError];
    
    return [phoneUtil format:myNumber
                numberFormat:NBEPhoneNumberFormatINTERNATIONAL
                       error:&anError];
}

+ (NSString*)getNumber:(NSString*)phone
{
    NSString *converStr = [[phone componentsSeparatedByCharactersInSet:
                            [[NSCharacterSet characterSetWithCharactersInString:phone] invertedSet]] componentsJoinedByString:@""];
    
    return converStr;
}

+ (NSString*)formatPhoneNum:(NSString*)phone
{
    phone = [phone stringByReplacingOccurrencesOfString:@"+" withString:@""];
    phone = [phone stringByReplacingOccurrencesOfString:@"-" withString:@""];
    phone = [phone stringByReplacingOccurrencesOfString:@" " withString:@""];
    phone = [phone stringByReplacingOccurrencesOfString:@"(" withString:@""];
    phone = [phone stringByReplacingOccurrencesOfString:@")" withString:@""];
    
    return phone;
}

+ (NSString*)numberCheck:(NSString*)num
{
    NSString *number = @"";
    if (![num isKindOfClass:[NSString class]]) {
        number = [NSString stringWithFormat:@"%ld",(long)[num integerValue]];
    }
    else {
        number = num;
    }
    return number;
}

+ (NSString*)numberFormater:(NSString*)number
{
//    if ([number isKindOfClass:[NSString class]]) {
//        if (![number containsString:@","]) {
//            return number;
//        }
//    }
    
    NSString *num = [Utils numberCheck:number];
    NSArray *items = [num componentsSeparatedByString:@"."];
    //NSLog(@"items:%@,%@",number,items);
    NSNumberFormatter *currencyFormatter = [[NSNumberFormatter alloc] init];
    [currencyFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    NSString *result = [currencyFormatter stringFromNumber:[NSNumber numberWithInteger:[items[0] integerValue]]];
#ifdef _WITZM
    
#elif defined _CHACHA

#else
    if ([items count]>1) {
        return [NSString stringWithFormat:@"%@.%@",result,items[1]];
    }
#endif
    return result;
}

+ (NSString*)strTofloat:(NSString*)number
{
    NSString *num = [Utils numberCheck:number];
    return [NSString stringWithFormat:@"%@",[NSNumber numberWithFloat:[num floatValue]]];
}

+ (NSString*)trimString:(NSString*)str
{
    NSString *trimmedString = [str stringByTrimmingCharactersInSet:
                               [NSCharacterSet whitespaceCharacterSet]];
    return [Utils formatPhoneNum:trimmedString];
}

+ (NSString*)trimStringOnly:(NSString*)str
{
    NSString *trimmedString = [str stringByTrimmingCharactersInSet:
                               [NSCharacterSet whitespaceCharacterSet]];
    return trimmedString;
}

// 중복 공백제거
+ (NSString *)replaceSpace:(NSString *)input {
    
    NSArray *array = [input componentsSeparatedByString:@" "];
    NSMutableArray *convertedArray = [NSMutableArray array];
    
    for (NSString *arrayString in array)    {
        
        if (![arrayString isEqualToString:@""] && ![arrayString isEqualToString:@","]) {
            [convertedArray addObject:arrayString];
        }
    }
    
    return [convertedArray componentsJoinedByString:@" "];
}

+(NSString*)getAddressFull:(NSString*)full {
    NSString* string = @"";
    
    NSArray *strParse = [full componentsSeparatedByString:@","];
    NSInteger count = [strParse count];
    if (count > 1) {
        string = strParse[(count-1)];
    }
    else {
        string = full;
    }
    
    return string;
}

+ (BOOL)nullToImage:(UIImage*)image
{
    CGImageRef cgref = [image CGImage];
    CIImage *cim = [image CIImage];
    
    if (cim == nil && cgref == NULL)
    {
        return YES;
    }
    return NO;
}

+ (NSString*)nullToString:(id)string
{
    if ([string isKindOfClass:[NSNumber class]]) {
        string = [NSString stringWithFormat:@"%@",string];
    }
    if (string && string != [NSNull null]) {
        return [Utils trimStringOnly:string];
    }
    return @"";
}

+ (NSString*)nullTolocation:(id)string
{
    if (string && string != [NSNull null]) {
        return [Utils trimStringOnly:[string stringByReplacingOccurrencesOfString:@"<null>" withString:@""]];
    }
    return @"0,0";
}

+ (NSString*)nullToStringNoTrim:(id)string
{
    if (string && string != [NSNull null]) {
        return string;
    }
    return @"";
}

+ (NSString*)nullToStringEscape:(id)string
{
    if (string && string != [NSNull null]) {
        return [Utils trimString:string];
    }
    return @"";
}

+ (NSString*)nullToIntString:(id)string
{
    if (string && string != [NSNull null]) {
        return [Utils trimString:string];
    }
    return @"0";
}

+ (double)miTometer:(double)distance
{
    return (distance*1609.344);
}

+ (double)meterTomi:(double)distance
{
    return (distance/1609.344);
}

+ (NSString*)nameToString:(id)string
{
    return [[Utils nullToString:string] stringByReplacingOccurrencesOfString:@";" withString:@" "];
}

+ (BOOL)validateEmailWithString:(NSString*)checkString
{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

+(BOOL)isLocationValid:(CLLocationCoordinate2D)coor
{
    if ((coor.latitude == 0.0f && coor.longitude == 0.0f) ||
        (coor.latitude == 180.0f || coor.longitude == 180.0f) ||
        (coor.latitude == -180.0f || coor.longitude == -180.0f)
        ) {
        return NO;
    }
    return YES;
}

+(BOOL)isLocationValid:(double)lat long:(double)lng
{
    if ((lat == 0.0f && lng == 0.0f) || (lat == 180.0f || lng == 180.0f) || (lat == -180.0f || lng == -180.0f)) {
        return NO;
    }
    return YES;
}

+(BOOL)isLocationValidString:(double)lat long:(double)lng
{
    
    if ((lat == 0.0f && lng == 0.0f) || (lat == 180.0f || lng == 180.0f) || (lat == -180.0f || lng == -180.0f)) {
        return NO;
    }
    return YES;
}

+ (UIImage*)getCountryFlag:(NSString*)country__
{
    NSString *imagePath = [NSString stringWithFormat:@"CountryPicker.bundle/%@.png", country__];
    UIImage *image = [UIImage imageNamed:imagePath];
    return image;
}

+ (NSString*)getCountryCode
{
    CTTelephonyNetworkInfo *info = [CTTelephonyNetworkInfo new];
    CTCarrier *carrier = info.subscriberCellularProvider;
    NSString *countryCode = @"";
    if (carrier == nil) {
        NSLocale *locale = [NSLocale currentLocale];
        countryCode = [locale objectForKey: NSLocaleCountryCode];
    }
    else {
        countryCode = carrier.isoCountryCode;
        NSLog(@"Country code is: %@",carrier.mobileCountryCode);
        NSLog(@"ISO country code is: %@", carrier.isoCountryCode);
    }
    
    return [countryCode uppercaseString];
}

+ (NSString *)replacingWithPattern:(NSString *)str pattern:(NSString *)pattern withTemplate:(NSString *)withTemplate error:(NSError **)error {
    
    return str;
//    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pattern
//                                                                           options:NSRegularExpressionCaseInsensitive
//                                                                             error:error];
//    return [regex stringByReplacingMatchesInString:str
//                                           options:0
//                                             range:NSMakeRange(0, str.length)
//                                      withTemplate:withTemplate];
}

+ (NSString *)replacingWithPattern2:(NSString *)str pattern:(NSString *)pattern withTemplate:(NSString *)withTemplate error:(NSError **)error {
    
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pattern
                                                                           options:NSRegularExpressionCaseInsensitive
                                                                             error:error];
    return [regex stringByReplacingMatchesInString:str
                                           options:0
                                             range:NSMakeRange(0, str.length)
                                      withTemplate:withTemplate];
}

+ (void)updateCurrentPosition:(NSString*)lat lng:(NSString*)lng
{
    if (![Utils isLocationValid:[lat doubleValue] long:[lng doubleValue]]) {
        return;
    }
    
    lat = [Utils replacingWithPattern:[NSString stringWithFormat:@"%.6f",[lat doubleValue]] pattern:@"0*$" withTemplate:@"" error:nil];
    lng = [Utils replacingWithPattern:[NSString stringWithFormat:@"%.6f",[lng doubleValue]] pattern:@"0*$" withTemplate:@"" error:nil];
    
    [[[TXApp instance] getSettings] setFDKeychain:SettingsConst.CryptoKeys.LAT value:lat];
    [[[TXApp instance] getSettings] setFDKeychain:SettingsConst.CryptoKeys.LNG value:lng];
    
#ifdef _DEBUG_USA
    [[[TXApp instance] getSettings] setFDKeychain:SettingsConst.CryptoKeys.LAT value:[NSString stringWithFormat:@"%.6f",[_GEO_LAT doubleValue]]];
    [[[TXApp instance] getSettings] setFDKeychain:SettingsConst.CryptoKeys.LNG value:[NSString stringWithFormat:@"%.6f",[_GEO_LNG doubleValue]]];
#endif
}

+ (void)updateSrcPosition:(NSString*)lat lng:(NSString*)lng
{
    lat = [Utils replacingWithPattern:[NSString stringWithFormat:@"%.6f",[lat doubleValue]] pattern:@"0*$" withTemplate:@"" error:nil];
    lng = [Utils replacingWithPattern:[NSString stringWithFormat:@"%.6f",[lng doubleValue]] pattern:@"0*$" withTemplate:@"" error:nil];
    
    [[[TXApp instance] getSettings] setFDKeychain:SettingsConst.CryptoKeys.SLAT value:lat];
    [[[TXApp instance] getSettings] setFDKeychain:SettingsConst.CryptoKeys.SLNG value:lng];
}

+ (void)updateDstPosition:(NSString*)lat lng:(NSString*)lng
{
    lat = [Utils replacingWithPattern:[NSString stringWithFormat:@"%.6f",[lat doubleValue]] pattern:@"0*$" withTemplate:@"" error:nil];
    lng = [Utils replacingWithPattern:[NSString stringWithFormat:@"%.6f",[lng doubleValue]] pattern:@"0*$" withTemplate:@"" error:nil];
    
    [[[TXApp instance] getSettings] setFDKeychain:SettingsConst.CryptoKeys.DLAT value:lat];
    [[[TXApp instance] getSettings] setFDKeychain:SettingsConst.CryptoKeys.DLNG value:lng];
}

+(CLLocationCoordinate2D)stringToPosition:(NSString*)lat lng:(NSString*)lng {
    // 위치
    CLLocationCoordinate2D position;
    position.latitude  = [Utils formattedPositionCoor:[lat doubleValue]];
    position.longitude = [Utils formattedPositionCoor:[lng doubleValue]];
    
    return position;
}


+(NSString*)currentPosition {
    // 현재 위치
    NSString *lat = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.LAT];
    NSString *lng = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.LNG];
    return [NSString stringWithFormat:@"%@,%@",lat,lng];
}

+(CLLocationCoordinate2D)currentPositionCoor {
    // 현재 위치
    NSString *lat = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.LAT];
    NSString *lng = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.LNG];
    
    // 위치
    return [Utils stringToPosition:lat lng:lng];
}

+(NSString*)destinationPosition {
    // 목적지 위치
    NSString *lat = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.DLAT];
    NSString *lng = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.DLNG];
    
#ifdef _DRIVER_MODE
    TXAppDelegate *appDelegate = (TXAppDelegate *)[[UIApplication sharedApplication] delegate];
    if (appDelegate.pstate == 230 || appDelegate.pstate == 220) {
        lat = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.SLAT];
        lng = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.SLNG];
    }
#endif
    
    return [NSString stringWithFormat:@"%@,%@",lat,lng];
}

+(CLLocationCoordinate2D)destinationPositionCoor {
    // 목적지 위치
    NSString *lat = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.DLAT];
    NSString *lng = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.DLNG];
    
#ifdef _DRIVER_MODE
    TXAppDelegate *appDelegate = (TXAppDelegate *)[[UIApplication sharedApplication] delegate];
    if (appDelegate.pstate == 230 || appDelegate.pstate == 220) {
        lat = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.SLAT];
        lng = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.SLNG];
    }
#endif
    
    // 위치
    return [Utils stringToPosition:lat lng:lng];
}

+(NSString*)srcPosition {
    // 목적지 위치
    NSString *lat = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.SLAT];
    NSString *lng = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.SLNG];
    
    return [NSString stringWithFormat:@"%@,%@",lat,lng];
}

+(CLLocationCoordinate2D)srcPositionCoor {
    // 목적지 위치
    NSString *lat = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.SLAT];
    NSString *lng = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.SLNG];
    
    // 위치
    return [Utils stringToPosition:lat lng:lng];
}

+(BOOL)checkAppInstalled
{
    NSURL *appUrl = [NSURL URLWithString:_DRIVER_APP_NAME];
    
    if ([UIApplication.sharedApplication canOpenURL:appUrl]) {
        return YES;
    }
    return NO;
}

+(BOOL)checkAppUpdate:(NSString*)server_version
{
    NSArray *pairComponents = [[Utils appVersion] componentsSeparatedByString:@"/"];
    NSInteger cver = [[[[pairComponents firstObject] stringByRemovingPercentEncoding] stringByReplacingOccurrencesOfString:@"." withString:@""] intValue];
    NSInteger cbuild = [[[pairComponents lastObject] stringByRemovingPercentEncoding] intValue];
    
    pairComponents = [server_version componentsSeparatedByString:@"/"];
    NSInteger sver = [[[[pairComponents firstObject] stringByRemovingPercentEncoding] stringByReplacingOccurrencesOfString:@"." withString:@""] intValue];
    NSInteger sbuild = [[[pairComponents lastObject] stringByRemovingPercentEncoding] intValue];
    
    if (sver > cver) {
        return YES;
    }
    if (sver >= cver && sbuild > cbuild) {
        return YES;
    }
    return NO;
}

+(BOOL)checkAppUpdate_old2:(NSString*)server_version
{
    // 1.0.1/1
    NSArray* foo = [server_version componentsSeparatedByString: @"/"];
    NSString *sversion = [foo objectAtIndex:0];
    NSString *sbuild   = [foo objectAtIndex:1];
    
    foo = [[Utils appVersion] componentsSeparatedByString: @"/"];
    NSString *lversion = [foo objectAtIndex:0];
    NSString *lbuild   = [foo objectAtIndex:1];
    
    if ([sversion compare:lversion options:NSNumericSearch] == NSOrderedDescending) {
        // actualVersion is lower than the requiredVersion
        NSLog(@"sversion");
        return YES;
    }
    
    if ([sversion compare:lversion options:NSNumericSearch] == NSOrderedDescending && [sbuild compare:lbuild options:NSNumericSearch] == NSOrderedDescending) {
        // actualVersion is lower than the requiredVersion
        NSLog(@"sversion");
        return YES;
    }
    return NO;
}


+(BOOL)checkAppUpdate_old:(NSString*)ver
{

    // 1.0.1/1
    NSArray* foo = [ver componentsSeparatedByString: @"/"];
    NSString *sversion = [foo objectAtIndex:0];
    NSString *sbuild   = [foo objectAtIndex:1];
    
    foo = [[Utils appVersion] componentsSeparatedByString: @"/"];
    NSString *lversion = [foo objectAtIndex:0];
    NSString *lbuild   = [foo objectAtIndex:1];
    
    if ([sversion compare:lversion options:NSNumericSearch] == NSOrderedDescending) {
        // actualVersion is lower than the requiredVersion
        NSLog(@"sversion");
        return YES;
    }
    
    if ([sbuild compare:lbuild options:NSNumericSearch] == NSOrderedDescending) {
        // actualVersion is lower than the requiredVersion
        NSLog(@"sbuild");
        return YES;
    }
    
    return NO;
}

+(NSString *) getSpaceReplacedWithPrcnt20:(NSString *) source {
    return [source stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
}

+(NSString *) stringByStrippingHTML:(NSString*)s {
    NSRange r;
    while ((r = [s rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound)
        s = [s stringByReplacingCharactersInRange:r withString:@""];
    return s;
}

+(NSString *) addressToString:(GMSAddress*)address {
    NSString *str = [Utils nullToString:address.thoroughfare];
    
    return str;
//    if ([str isEqualToString:@""]) {
//        return @"";
//    }
//    return [NSString stringWithFormat:@"%@,%@", address.thoroughfare, address.locality];
}

+(NSString*)getUserName {
    NSDictionary *user;
    TXAppDelegate *appDelegate = (TXAppDelegate *)[[UIApplication sharedApplication] delegate];
    if (appDelegate.isDriverMode) {
        user = [appDelegate.dicDriver objectForKey:@"provider_device"];
    }
    else {
        user = [appDelegate.dicRider objectForKey:@"user_device"];
    }
    
    NSString* _name = [Utils nameToString:[user objectForKey:@"name"]];
    return _name;
}

+(NSString*)getDriverRate {
    NSDictionary *user;
    TXAppDelegate *appDelegate = (TXAppDelegate *)[[UIApplication sharedApplication] delegate];
    if (appDelegate.isDriverMode) {
        user = [appDelegate.dicDriver objectForKey:@"provider"];
    }
    else {
        user = [appDelegate.dicRider objectForKey:@"provider"];
    }
    
    NSString* _rate = @"";
    if ([Utils isDictionary:user]) {
        // 소숫점 두째자리 버림으로 %.1f 표기
        _rate = [[NSString stringWithFormat:@"%.2f",[[user objectForKey:@"rate"] floatValue]] substringWithRange:NSMakeRange(0,[[NSString stringWithFormat:@"%.2f",[[user objectForKey:@"rate"] floatValue]] length]-1)];
        // 2자리
        //_rate = [NSString stringWithFormat:@"%@",[NSNumber numberWithFloat:[[user objectForKey:@"rate"] floatValue]]];
    }
    return _rate;
}

+(NSString*)dicToString:(NSDictionary*)dic {
    NSData* kData = [NSJSONSerialization dataWithJSONObject:dic options:NSJSONWritingPrettyPrinted error:nil];
    NSString* kJson = [[NSString alloc] initWithData:kData encoding:NSUTF8StringEncoding];
    
    return kJson;
}

+(NSDictionary*)stringToDic:(NSString*)kJson {
    //NSError *error;
    NSData *objectData = [kJson dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData options:0 error:nil];
//    
//    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData
//                                                         options:NSJSONReadingMutableContainers
//                                                           error:&error];
//    NSLog(@"error:%@",error);
    return json;
}

+(NSDate *)getDateFromString:(NSString *)string
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:DATE_FORMAT_YYYYMMDDHH24MISS];
    NSDate *date = [dateFormatter dateFromString:string];
    return date;
}

+(NSString*)getStringFromDate:(NSDate*)date
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:DATE_FORMAT_YYYYMMDDHH24MISS];
    NSString *string = [dateFormatter stringFromDate:date];
    return string;
}

+(NSString*)getBCDSTEP {
    NSString *seq = @"BCD_STEP";
    TXAppDelegate *appDelegate = (TXAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *seqNo = @"";
    
    if (appDelegate.isDriverMode) {
        seqNo = [Utils nullToString:[[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.PSEQ]];
    }
    else {
        seqNo = [Utils nullToString:[[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.USEQ]];
    }
    
    if ([seqNo isEqualToString:@""]) {
        return nil;
    }
    
    seq = [NSString stringWithFormat:@"%@_%@",seq,seqNo];
    NSLog(@"getBCDSTEP:%@",seq);
    NSString *step = [Utils nullToString:[[[TXApp instance] getSettings] getFDKeychain:seq]];
    NSLog(@"getBCDSTEP:%@:%@",seq,step);
    if ([step isEqualToString:@""]) {
        return nil;
    }
    
    return step;
}

+(void)setBCDSTEP:(NSString*)__step {
    NSString *seq = @"BCD_STEP";
    TXAppDelegate *appDelegate = (TXAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *seqNo = @"";
    
    if (appDelegate.isDriverMode) {
        seqNo = [Utils nullToString:[[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.PSEQ]];
    }
    else {
        seqNo = [Utils nullToString:[[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.USEQ]];
    }
    
    if ([seqNo isEqualToString:@""]) {
        return;
    }
    
    seq = [NSString stringWithFormat:@"%@_%@",seq,seqNo];
    [[[TXApp instance] getSettings] setFDKeychain:seq value:__step];
}

+(BOOL)getIsFirstLogin {
    NSString* notsignup = [[[TXApp instance] getSettings] getFDKeychain:IS_SIGN_UP];
    // 로그인 이력이 있다. -> 로그인페이지
    if ([[Utils nullToString:notsignup] isEqualToString:@"1"]) {
        return NO;
    }
    return YES;
}

+(void)setFirstLogin {
    [[[TXApp instance] getSettings] setFDKeychain:IS_SIGN_UP value:@"1"];
}

#pragma mark - Estimate
+(NSDictionary*)getEstimate:(NSString*)service
{
    TXAppDelegate *appDelegate = (TXAppDelegate *)[[UIApplication sharedApplication] delegate];
    for (NSDictionary *dic in appDelegate.drive_estimates) {
        if ([dic[@"service"] isEqualToString:service]) {
            return dic;
        }
    }
    
    return nil;
}

#pragma mark - StateUI
// 0:요청대기 1: 요청중 2:처리완료
+(int)checkStateUIInfo:(NSString*)key oldvalue:(NSString*)oldvalue
{
    TXAppDelegate *appDelegate = (TXAppDelegate *)[[UIApplication sharedApplication] delegate];
    if ([appDelegate.stateUIBefore[key] isEqualToString:oldvalue]) {
        NSLog(@"checkStateUIInfo1:%@:%d",key,[appDelegate.stateUI[key] intValue]);
        return [appDelegate.stateUI[key] intValue];
    }
    else {
        [Utils setStateUIInfo:key value:UI_STATE_STEP_WAIT oldvalue:oldvalue];
        //NSLog(@"checkStateUIInfo2:%@:%d",key,UI_STATE_STEP_WAIT);
        return UI_STATE_STEP_WAIT;
    }
}

+(void)setStateUIInfo:(NSString*)key value:(int)value oldvalue:(NSString*)oldvalue
{
    TXAppDelegate *appDelegate = (TXAppDelegate *)[[UIApplication sharedApplication] delegate];
    if ([key isEqualToString:UI_STATE_TRIP_LINE]) {
        //
    }

    appDelegate.stateUI[key] = [NSString stringWithFormat:@"%d", value];

    if (oldvalue == nil) {
        return;
    }

    appDelegate.stateUIBefore[key] = oldvalue;

}

+(NSString*)getStateUIInfo:(NSString*)key
{
    TXAppDelegate *appDelegate = (TXAppDelegate *)[[UIApplication sharedApplication] delegate];
    return appDelegate.stateUI[key];
}

+(void)resetStateUIInfo
{
    TXAppDelegate *appDelegate = (TXAppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate.stateUI removeAllObjects];
    [appDelegate.stateUIBefore removeAllObjects];
}


#pragma mark Streching
//-------------------------------------------------------------------------------------------------------------------------------------------------
+ (void)stretching:(UIButton*)aButton image:(NSString*)image
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    UIImage *stretchImage = nil;
    //UIImage *stretchOverImage = nil;
    
    // 34, 44 인셋을 지정한 만큼 안의 1px를 늘려준다.
    stretchImage = [[UIImage imageNamed:image] resizableImageWithCapInsets:UIEdgeInsetsMake(21, 16, 21, 16) resizingMode:UIImageResizingModeStretch];
    
    //stretchOverImage = [[UIImage imageNamed:@"loginPrimaryButtonBackgroundPressed"] resizableImageWithCapInsets:UIEdgeInsetsMake(21, 16, 21, 16) resizingMode:UIImageResizingModeStretch];
    
    [aButton setBackgroundImage:stretchImage forState:UIControlStateNormal];
    //    [aButton setBackgroundImage:stretchOverImage forState:UIControlStateHighlighted];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
+ (void)stretchingView:(UIView*)aButton image:(NSString*)image
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    UIImage *stretchImage = nil;
    //UIImage *stretchOverImage = nil;
    
    // 34, 44 인셋을 지정한 만큼 안의 1px를 늘려준다.
    stretchImage = [[UIImage imageNamed:image] resizableImageWithCapInsets:UIEdgeInsetsMake(21, 16, 21, 16) resizingMode:UIImageResizingModeStretch];
    
    //stretchOverImage = [[UIImage imageNamed:@"loginPrimaryButtonBackgroundPressed"] resizableImageWithCapInsets:UIEdgeInsetsMake(21, 16, 21, 16) resizingMode:UIImageResizingModeStretch];
    
    //UIImage *myIcon = [Utils imageWithImage:stretchImage scaledToSize:aButton.frame.size];
    UIImage *myIcon = [Utils image:stretchImage scaledToSize:aButton.frame.size];
    
    [aButton setBackgroundColor:[UIColor colorWithPatternImage:myIcon]];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
+ (void)stretchingImageView:(UIImageView*)aButton image:(NSString*)image
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    UIImage *stretchImage = nil;
    //UIImage *stretchOverImage = nil;
    
    // 34, 44 인셋을 지정한 만큼 안의 1px를 늘려준다.
    stretchImage = [[UIImage imageNamed:image] resizableImageWithCapInsets:UIEdgeInsetsMake(21, 16, 21, 16) resizingMode:UIImageResizingModeStretch];
    
    //stretchOverImage = [[UIImage imageNamed:@"loginPrimaryButtonBackgroundPressed"] resizableImageWithCapInsets:UIEdgeInsetsMake(21, 16, 21, 16) resizingMode:UIImageResizingModeStretch];
    
    //UIImage *myIcon = [Utils imageWithImage:stretchImage scaledToSize:aButton.frame.size];
    UIImage *myIcon = [Utils image:stretchImage scaledToSize:aButton.frame.size];
    aButton.image = myIcon;
}

#pragma mark - imageUtil
// marker와 infoview를 합쳐준다
+ (UIImage *)imageFromInfoView:(NSString *) str image:(UIImage*)icon color:(UIColor*)color;
{
    float infoHeight = 38;
    float infoWindowWidth = 80;
    float infoWindowHeight = infoHeight + icon.size.height;
    float anchorSize = 20;
    
    UIView *calloutView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, infoWindowWidth, infoWindowHeight)];
    
    UIImageView *viewImage = [[UIImageView alloc] initWithImage:icon];
    viewImage.frame = CGRectMake(infoWindowWidth/2 - icon.size.width/2, infoWindowHeight - icon.size.height, icon.size.width, icon.size.height);
    [calloutView addSubview:viewImage];
    
    float offset = anchorSize * M_SQRT2 + icon.size.height;
    CGAffineTransform rotateBy45Degrees = CGAffineTransformMakeRotation(M_PI_4);
    UIView *arrow = [[UIView alloc] initWithFrame:CGRectMake((infoWindowWidth - anchorSize)/2.0, infoWindowHeight - offset, anchorSize, anchorSize)];
    arrow.transform = rotateBy45Degrees;
    arrow.backgroundColor = color;
    arrow.layer.cornerRadius = 2;
    arrow.layer.masksToBounds = YES;
    [calloutView addSubview:arrow];
    
    infoWindowHeight = infoHeight;
    offset = anchorSize * M_SQRT2;
    UIView *contentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, infoWindowWidth, infoWindowHeight - offset/2)];
    [contentView setBackgroundColor:color];
    //contentView.alpha = 0.5f;
    
    contentView.layer.cornerRadius = 2;
    contentView.layer.masksToBounds = YES;
    //
    //    contentView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    //    contentView.layer.borderWidth = 1.0f;
    
    UILabel *label = [UILabel new];
    label.text = str;
    [label setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:12.0f]];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor whiteColor];
    [label sizeToFit];
    label.frame = contentView.frame;
    [contentView addSubview:label];
    
    
    [calloutView addSubview:contentView];
    
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        UIGraphicsBeginImageContextWithOptions(calloutView.frame.size, NO, [[UIScreen mainScreen] scale]);
    } else {
        UIGraphicsBeginImageContext(calloutView.frame.size);
    }
    [calloutView.layer renderInContext: UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
+(UIImage *)makeRoundedImage:(UIImage *) image
                      radius: (float) radius;
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    CALayer *imageLayer = [CALayer layer];
    imageLayer.frame = CGRectMake(0, 0, image.size.width, image.size.height);
    imageLayer.contents = (id) image.CGImage;
    
    imageLayer.masksToBounds = YES;
    imageLayer.cornerRadius = radius;
    
    UIGraphicsBeginImageContext(image.size);
    [imageLayer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *roundedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return roundedImage;
}

+ (UIImage *)image:(UIImage *)image scaledToSize:(CGSize)newSize {
    //UIGraphicsBeginImageContext(newSize);
    // In next line, pass 0.0 to use the current device's pixel scaling factor (and thus account for Retina resolution).
    // Pass 1.0 to force exact pixel size.
    if (CGSizeEqualToSize(image.size, newSize))
    {
        return image;
    }
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

+ (UIImage *)imageByCropping:(UIImage *)imageToCrop toRect:(CGRect)rect;
{
    CGImageRef imageRef = CGImageCreateWithImageInRect([imageToCrop CGImage], rect);
    UIImage *croppedImage = [UIImage imageWithCGImage:imageRef
                                                scale:imageToCrop.scale
                                          orientation:imageToCrop.imageOrientation];
    CGImageRelease(imageRef);
    
    return croppedImage;
}

+ (UIImage*) croppedImageWithRect:(UIImage*)image andFrame: (CGRect) rect;
{
    
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGRect drawRect = CGRectMake(-rect.origin.x, -rect.origin.y, image.size.width, image.size.height);
    CGContextClipToRect(context, CGRectMake(0, 0, rect.size.width, rect.size.height));
    [image drawInRect:drawRect];
    UIImage* subImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return subImage;
}

#pragma mark Image Encoding
+ (NSInteger)getImageSize:(UIImage*)originalImage
{
    NSData *imgData = UIImageJPEGRepresentation(originalImage, 1); //1 it represents the quality of the image.
    return [imgData length];
}

+ (UIImage *)decodeBase64ToImage:(NSString *)strEncodeData {
    NSData *data = [[NSData alloc]initWithBase64EncodedString:strEncodeData options:NSDataBase64DecodingIgnoreUnknownCharacters];
    return [UIImage imageWithData:data];
}

+ (NSString *)encodeToBase64String_old:(UIImage *)image {
    return [UIImagePNGRepresentation(image) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
}

+ (NSString *)encodeToBase64String:(UIImage *)image;
{
    //return [UIImagePNGRepresentation(image) base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithCarriageReturn];
    
    //Reconvert UIImage to NSData
    //NSData *imageData = UIImageJPEGRepresentation([Utils resizeImage:image], 1.0);
    NSData *imageData = UIImageJPEGRepresentation(image, compressionQuality);
    
    //And then apply Base64 encoding to convert it into a base-64 encoded string:
    NSString *encodedString = [Utils base64forData:imageData];
    return encodedString;
}

+ (NSString*)base64forData:(NSData*)theData;
{
    
    const uint8_t* input = (const uint8_t*)[theData bytes];
    NSInteger length = [theData length];
    
    static char table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    
    NSMutableData* data = [NSMutableData dataWithLength:((length + 2) / 3) * 4];
    uint8_t* output = (uint8_t*)data.mutableBytes;
    
    NSInteger i;
    for (i=0; i < length; i += 3) {
        NSInteger value = 0;
        NSInteger j;
        for (j = i; j < (i + 3); j++) {
            value <<= 8;
            
            if (j < length) {
                value |= (0xFF & input[j]);
            }
        }
        
        NSInteger theIndex = (i / 3) * 4;
        output[theIndex + 0] =                    table[(value >> 18) & 0x3F];
        output[theIndex + 1] =                    table[(value >> 12) & 0x3F];
        output[theIndex + 2] = (i + 1) < length ? table[(value >> 6)  & 0x3F] : '=';
        output[theIndex + 3] = (i + 2) < length ? table[(value >> 0)  & 0x3F] : '=';
    }
    
    return [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
}

+ (UIImage *)resizeImage:(UIImage *)croppedImage;
{
    UIImage *image = [croppedImage resizedImageByMagick: @"2048x2048"];
    return image;
//    NSData *imgData = UIImageJPEGRepresentation(image, 0.6);
//    return [UIImage imageWithData:imgData];
    
//    NSData *imgData = UIImageJPEGRepresentation(croppedImage, 1.0);
//    NSLog(@"Size of Image1(bytes):%d",(int)[imgData length]);
//    
//    UIImage *image = [croppedImage resizedImageByMagick: @"2048x2048"];
//    
//    imgData = UIImageJPEGRepresentation(image, 1.0);
//    NSLog(@"Size of Image2(bytes):%d",(int)[imgData length]);
//    
//    imgData = UIImageJPEGRepresentation(image, 0.6);
//    NSLog(@"Size of Image3(bytes):%d",(int)[imgData length]);
//    
//    NSString *picture = [Utils base64forData:imgData];
//    NSLog(@"picture:%d",(int)[picture length]);
/*
    float actualHeight = image.size.height;
    float actualWidth = image.size.width;
    float maxHeight = 2048;
    float maxWidth = 2048;
    float imgRatio = actualWidth/actualHeight;
    float maxRatio = maxWidth/maxHeight;
    //float compressionQuality = 0.5;//50 percent compression
    
    if (actualHeight > maxHeight || actualWidth > maxWidth)
    {
        if(imgRatio < maxRatio)
        {
            //adjust width according to maxHeight
            imgRatio = maxHeight / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = maxHeight;
        }
        else if(imgRatio > maxRatio)
        {
            //adjust height according to maxWidth
            imgRatio = maxWidth / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = maxWidth;
        }
        else
        {
            actualHeight = maxHeight;
            actualWidth = maxWidth;
        }
    }
    
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [image drawInRect:rect];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    NSData *imageData = UIImageJPEGRepresentation(img, compressionQuality);
    UIGraphicsEndImageContext();
    NSLog(@"imageData:%d",(int)[imageData length]);
    return [UIImage imageWithData:imageData];
*/
}

//+(UIImage*)resizedImageWithMaxEdge:(UIImage*)image maxEdge:(float)maxEdge {
//    float aspectRatio = image.size.width/image.size.height;
//    CGSize size;
//    
//    if (aspectRatio >= 1 && image.size.width > maxEdge) {
//        size = CGSizeMake(maxEdge, maxEdge * (1/aspectRatio));
//    } else if (aspectRatio < 1 && image.size.height > maxEdge) {
//        size = CGSizeMake(maxEdge * aspectRatio, maxEdge);
//    } else {
//        return [self copy];
//    }
//    
//    return [image resizedImage:size interpolationQuality:kCGInterpolationNone];
//}

#pragma mark Custom UI
+ (CGSize) getBasicButtonSize:(UIView*)view
{
    return CGSizeMake(view.frame.size.width - kDefaultPaddingFromLeft * 2,kTextFieldDefaultHeight);
}

+ (CGRect) getRect:(CGRect)btn size:(CGSize)size
{
    return CGRectMake(btn.origin.x, btn.origin.y, size.width, size.height);
}

+ (void)labelAttrBasic:(UILabel*) obj
{
    [obj setTextColor:UIColorBasicText];
    //[obj setFont:[UIFont fontWithName:@"HelveticaNeue" size:13.0f]];
    [obj setFont:[UIFont systemFontOfSize:13]];
    [obj setNumberOfLines:1];
    obj.textAlignment = NSTextAlignmentRight;
}

+ (void)labelAttrHeader:(UILabel*) obj
{
#ifdef _WITZM
    [obj setTextColor:UIColorBasicText];
#elif defined _CHACHA
    [obj setTextColor:UIColorLabelTextColor];
#endif
    [obj setFont:[UIFont fontWithName:@"ArialRoundedMTBold" size:48.0f]];
    [obj setAdjustsFontSizeToFitWidth:YES];
    [obj setNumberOfLines:1];
    obj.textAlignment = NSTextAlignmentLeft;
}

+ (void)buttonAttrRight:(UIButton*) obj
{
#ifdef _WITZM
    [obj setTitleColor:UIColorBasicText forState:UIControlStateNormal];
#elif defined _CHACHA
    [obj setTitleColor:UIColorLabelTextColor forState:UIControlStateNormal];
#endif

    obj.backgroundColor = [UIColor clearColor];
    //obj.layer.cornerRadius = 3;
    obj.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:18.0f];
    obj.titleLabel.textAlignment = NSTextAlignmentRight;
}

+ (UILabel*)topHeader:(UIView*)view text:(NSString*)text
{
    UILabel *lbhead = [[UILabel alloc] initWithFrame:CGRectMake(kTitlePaddingFromLeft,
                                                                kTitlePaddingFromTop,
                                                                250,
                                                                55)];
    
    lbhead.text = text;
    [Utils labelAttrHeader:lbhead];
    return lbhead;
}

+ (TXButton*)topHeaderRight:(UIView*)view text:(NSString*)text
{
    CGRect frame = view.frame;
    frame.origin.x = view.frame.size.width - 26;
    frame.origin.y = 69;
    frame.size.width = 26;
    frame.size.height = 2;
    
    UIView *v = [[UIView alloc] initWithFrame:frame];
    v.backgroundColor = UIColorDefault;
    [view addSubview:v];
    
    //
    frame.origin.x = view.frame.size.width - 120 - 30;
    frame.origin.y = 57;
    frame.size.width = 120;
    frame.size.height = 24;
    
    TXButton *btn = [[TXButton alloc] initWithFrame:frame];
    
    btn.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleBottomMargin;
    [btn setTitle:text forState:UIControlStateNormal];
    [btn setTitleColor:UIColorDefault forState:UIControlStateNormal];
    [[btn titleLabel] setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:18.0f]];
    btn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    btn.backgroundColor = [UIColor clearColor];
    
    return btn;
}

+ (UILabel*)topHeaderLabel:(UIView*)view text:(NSString*)text
{
    UILabel *lbhead = [[UILabel alloc] initWithFrame:CGRectMake(kDefaultPaddingFromLeft,
                                                                kNaviTopHeight,
                                                                view.frame.size.width - kDefaultPaddingFromLeft * 2,
                                                                kTextFieldDefaultHeight)];
    
    lbhead.text = text;
    [Utils labelAttrBasic:lbhead];
    return lbhead;
}

+ (UILabel*)leftLabel:(UIView*)view text:(NSString*)text dim:(NSInteger)dim
{
    UILabel *lbhead = [[UILabel alloc] initWithFrame:CGRectMake(kTitlePaddingFromLeft,
                                                                dim,
                                                                62,
                                                                15)];
    
    lbhead.text = text;
    
    [lbhead setTextColor:[UIColor whiteColor]];
    [lbhead setFont:[UIFont fontWithName:@"ArialRoundedMTBold" size:13.0f]];
    [lbhead setAdjustsFontSizeToFitWidth:YES];
    [lbhead setNumberOfLines:1];
    lbhead.textAlignment = NSTextAlignmentLeft;
    
    return lbhead;
}

+ (void)updateTextFontSize:(NSString*)text label:(UILabel*)label size:(NSInteger)size
{
    label.text = [Utils nullToStringNoTrim:text];
    //NSLog(@"label.text:%@",label.text);
    //[label setNumberOfLines:1];
    //[label setTextColor:[UIColor blackColor]];
    label.backgroundColor = [UIColor clearColor];
    //[label setFont:[UIFont systemFontOfSize:size]];
    //label.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    //[label sizeToFit];
}

+ (void)updateTextFontSize2:(NSString*)text label:(RTLabel*)label size:(NSInteger)size
{
    label.text = [Utils nullToStringNoTrim:text];
    //NSLog(@"label.text:%@",label.text);
    //[label setNumberOfLines:1];
    [label setTextColor:[UIColor blackColor]];
    //label.backgroundColor = [UIColor clearColor];
    [label setFont:[UIFont systemFontOfSize:size]];
    //label.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    //[label sizeToFit];
}

+ (NSString*)labelStringUserInfo:(NSString*)trips rate:(NSString*)rate
{
    //return [NSString stringWithFormat:@"%@ ★ %.1f",[Utils ProfileLevelString:trips capitalizedString:YES],[rate floatValue]];
    return [NSString stringWithFormat:@"%d",[rate intValue]];
}

+ (void)labelMenuUserInfo:(UILabel*)label trips:(NSString*)trips rate:(NSString*)rate
{
    label.text = [Utils labelStringUserInfo:trips rate:rate];
}

// 흰색배경
+ (UIView*)addBackView:(UIView*)view targetView:(UIView*)target
{
//    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0,
//                                                              target.frame.origin.y,
//                                                              view.frame.size.width,
//                                                              kTextFieldDefaultHeight)];
    
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(kLabelPaddingFromLeft,
                                                              target.frame.origin.y + target.frame.size.height + 10,
                                                              view.frame.size.width - kLabelPaddingFromLeft*2,
                                                              1)];
    bgView.backgroundColor = [UIColor whiteColor];
    bgView.alpha = 0.5;
    return bgView;
}

// 회색배경에 진한 회색글자 기본색(하늘) 배경에
+ (UIView*)addLabelView:(UIView*)view underView:(UIView*)target dim:(float) pix;
{
    CGRect frame = CGRectZero;
    
    if (target != nil) {
        frame = target.frame;
    }
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(kBasicMargin,
                                                              (frame.origin.y + frame.size.height + pix),
                                                              view.frame.size.width-kBasicMargin*2,
                                                              kSettingTitletHeight)];
    bgView.backgroundColor = [UIColor whiteColor];
    //bgView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    UILabel *obj = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, bgView.frame.size.width, bgView.frame.size.height)];
    [obj setTextColor:HEXCOLOR(0x666666ff)];
    obj.tag = 11;
    [obj setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:12.0f]];
    //[obj setFont:[UIFont systemFontOfSize:12.0f]];
    [obj setNumberOfLines:0];
    obj.textAlignment = NSTextAlignmentLeft;
    obj.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    
    UIView *lineview = [[UIView alloc] initWithFrame:CGRectMake(0,
                                                                bgView.frame.size.height - 1,
                                                                bgView.frame.size.width,
                                                                1)];
    lineview.backgroundColor = UIColorBasicBack;
    lineview.tag = 222;
    
    [bgView addSubview:obj];
    [bgView addSubview:lineview];
    
    return bgView;
}

// 회색배경에 왼쪽 진한 회색글자 , 오른쪽 검은글자
+ (UIView*)addDoubleLabelView:(UIView*)view underView:(UIView*)target dim:(float) pix;
{
    CGRect frame = CGRectZero;
    
    if (target != nil) {
        frame = target.frame;
    }
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0,
                                                              (frame.origin.y + frame.size.height + pix),
                                                              view.frame.size.width,
                                                              62)];
    bgView.backgroundColor = UIColorLabelBG;
    //bgView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    UILabel *obj = [[UILabel alloc] initWithFrame:CGRectMake(kBasicMargin, 0, bgView.frame.size.width - kBasicMargin*2, bgView.frame.size.height)];
    [obj setTextColor:HEXCOLOR(0x333333FF)];
    obj.tag = 11;
    [obj setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:16.0f]];
    //[obj setFont:[UIFont systemFontOfSize:12.0f]];
    [obj setNumberOfLines:1];
    obj.textAlignment = NSTextAlignmentLeft;
    obj.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    
    [bgView addSubview:obj];
    
    obj = [[UILabel alloc] initWithFrame:CGRectMake(kBasicMargin, 0, bgView.frame.size.width - kBasicMargin*2, bgView.frame.size.height)];
    [obj setTextColor:HEXCOLOR(0x333333FF)];
    obj.tag = 12;
    [obj setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:16.0f]];
    //[obj setFont:[UIFont systemFontOfSize:12.0f]];
    [obj setNumberOfLines:1];
    obj.textAlignment = NSTextAlignmentRight;
    obj.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    
    [bgView addSubview:obj];
    
    UIView *lineview = [[UIView alloc] initWithFrame:CGRectMake(kBasicMargin,
                                                                bgView.frame.size.height - 1,
                                                                bgView.frame.size.width - kBasicMargin*2,
                                                                1)];
    lineview.backgroundColor = UIColorBasicBack;
    lineview.tag = 222;
    [bgView addSubview:lineview];
    
    return bgView;
}

+ (UIView*)addTweenLabelView:(UIView*)view underView:(UIView*)target dim:(float) pix;
{
    CGRect frame = CGRectZero;
    
    if (target != nil) {
        frame = target.frame;
    }
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0,
                                                              (frame.origin.y + frame.size.height + pix),
                                                              view.frame.size.width,
                                                              kTextFieldDefaultHeight)];
    bgView.backgroundColor = [UIColor whiteColor];
    //bgView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    UILabel *obj = [[UILabel alloc] initWithFrame:CGRectMake(kBasicMargin,
                                                             0,
                                                             bgView.frame.size.width/2 - kBasicMargin,
                                                             bgView.frame.size.height)];
    [obj setTextColor:UIColorLabelText];
    obj.tag = 11;
    //[obj setFont:[UIFont fontWithName:@"HelveticaNeue" size:12.0f]];
    [obj setFont:[UIFont systemFontOfSize:12.0f]];
    [obj setNumberOfLines:1];
    obj.textAlignment = NSTextAlignmentLeft;
    obj.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    
    [bgView addSubview:obj];
    
    obj = [[UILabel alloc] initWithFrame:CGRectMake(bgView.frame.size.width/2 + kBasicMargin,
                                                    0,
                                                    bgView.frame.size.width/2 - kBasicMargin*2,
                                                    bgView.frame.size.height)];
    [obj setTextColor:[UIColor blackColor]];
    obj.tag = 12;
    //[obj setFont:[UIFont fontWithName:@"HelveticaNeue" size:12.0f]];
    [obj setFont:[UIFont systemFontOfSize:12.0f]];
    [obj setNumberOfLines:1];
    obj.textAlignment = NSTextAlignmentRight;
    obj.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    
    [bgView addSubview:obj];
    
    return bgView;
}

// 회색배경에 진한 회색글자
+ (UIView*)addRTLabelView:(UIView*)view underView:(UIView*)target dim:(float) pix;
{
    CGRect frame = CGRectZero;
    
    if (target != nil) {
        frame = target.frame;
    }
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0,
                                                              (frame.origin.y + frame.size.height + pix),
                                                              view.frame.size.width,
                                                              kTextFieldDefaultHeight)];
    bgView.backgroundColor = UIColorLabelBG;
    //bgView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    RTLabel *obj = [[RTLabel alloc] initWithFrame:CGRectMake(kBasicMargin, 0, bgView.frame.size.width - kBasicMargin*2, bgView.frame.size.height)];
    [obj setTextColor:UIColorLabelText];
    obj.tag = 11;
    [obj sizeToFit];
    obj.backgroundColor = [UIColor clearColor];
    //[obj setFont:[UIFont fontWithName:@"HelveticaNeue" size:12.0f]];
    [obj setFont:[UIFont systemFontOfSize:12.0f]];
    //obj.textAlignment = NSTextAlignmentLeft;
    //obj.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
    obj.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    [bgView addSubview:obj];
    
    return bgView;
}

+ (UIView*)addFooterLabelView:(UIView*)view underView:(UIView*)target dim:(float) pix;
{
    CGRect frame = CGRectZero;
    
    if (target != nil) {
        frame = target.frame;
    }
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0,
                                                              (frame.origin.y + frame.size.height + pix),
                                                              view.frame.size.width,
                                                              kTextFieldDefaultHeight)];
    RTLabel *obj = [[RTLabel alloc] initWithFrame:CGRectMake(0, 0, bgView.frame.size.width, bgView.frame.size.height)];
    [obj setTextColor:UIColorLabelText];
    obj.tag = 11;
    [obj sizeToFit];
    obj.backgroundColor = [UIColor clearColor];
    //[obj setFont:[UIFont fontWithName:@"HelveticaNeue" size:12.0f]];
    [obj setFont:[UIFont systemFontOfSize:12.0f]];
    //obj.textAlignment = NSTextAlignmentLeft;
    //obj.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
    obj.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    [bgView addSubview:obj];
    
    return bgView;
}

+ (void)RTLabelReSize:(UIView*)view tag:(NSInteger)tag
{
    RTLabel *label = [view viewWithTag:tag];
    [label sizeToFit];
    CGSize optimumSize = [label optimumSize];
    CGRect frame = [label frame];
    frame.size.height = (int)optimumSize.height;
    frame.origin.y = view.frame.size.height/2 - frame.size.height/2;
    
    [label setFrame:frame];
}

+ (CGSize)cGRectLabel:(UILabel*)label;
{
    // Calculate size of string
    CGRect labelRect = CGRectZero;
    CGSize labelSize = CGSizeZero;
    
    if(![label.text isEqualToString:@""]) {
        CGSize constraintSize = CGSizeMake(200.0f, 300.0f);
        labelRect = [label.text boundingRectWithSize:constraintSize
                                                        options:(NSStringDrawingOptions)(NSStringDrawingUsesFontLeading | NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin)
                                                     attributes:@{NSFontAttributeName: label.font}
                                                        context:NULL];
        labelSize.height = ceilf(CGRectGetHeight(labelRect));
        labelSize.width = ceilf(CGRectGetWidth(labelRect));
    }
    
    return labelSize;
}

// 흰색배경에 연한검은색글자
+ (UIView*)addInputTextView:(UIView*)view underView:(UIView*)target dim:(float) pix;
{
    CGRect frame = CGRectZero;
    
    if (target != nil) {
        frame = target.frame;
    }
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0,
                                                              (frame.origin.y + frame.size.height + pix),
                                                              view.frame.size.width,
                                                              kTextFieldDefaultHeight)];
    bgView.backgroundColor = UIColorInputTextBG;
    //bgView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    UITextField *obj = [[UITextField alloc] initWithFrame:CGRectMake(kBasicMargin, 0, bgView.frame.size.width - kBasicMargin*2, bgView.frame.size.height)];
    [obj setTextColor:UIColorLabelText];
    obj.tag = 11;
    //[obj setFont:[UIFont fontWithName:@"HelveticaNeue" size:12.0f]];
    [obj setFont:[UIFont systemFontOfSize:14.0f]];
    obj.textAlignment = NSTextAlignmentLeft;
    obj.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    
    [bgView addSubview:obj];
    
    return bgView;
}
// 흰색배경에 검은색글자
+ (UIView*)addWhiteLabelView:(UIView*)view underView:(UIView*)target dim:(float) pix;
{
    CGRect frame = CGRectZero;
    
    if (target != nil) {
        frame = target.frame;
    }
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(kBasicMargin,
                                                              (frame.origin.y + frame.size.height + pix),
                                                              view.frame.size.width-kBasicMargin*2,
                                                              kSettingDefaultHeight)];
    bgView.backgroundColor = [UIColor whiteColor];
    //bgView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    UILabel *obj = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, bgView.frame.size.width, bgView.frame.size.height)];
    [obj setTextColor:HEXCOLOR(0x333333ff)];
    obj.tag = 11;
    [obj setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:16.0f]];
    //[obj setFont:[UIFont systemFontOfSize:14.0f]];
    [obj setNumberOfLines:1];
    obj.textAlignment = NSTextAlignmentLeft;
    obj.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    
    UIView *lineview = [[UIView alloc] initWithFrame:CGRectMake(0,
                                                            bgView.frame.size.height - 1,
                                                            bgView.frame.size.width,
                                                            1)];
    lineview.backgroundColor = UIColorBasicBack;
    lineview.tag = 222;
    
    UIImageView *arrow = [[UIImageView alloc] init];
    arrow.frame = CGRectMake(bgView.frame.size.width - 10 -7, bgView.frame.size.height/2 - 10/2, 7, 10);
    arrow.image = [UIImage imageNamed:@"btn_list_detail"];
    
    [bgView addSubview:obj];
    [bgView addSubview:lineview];
    [bgView addSubview:arrow];
    
    return bgView;
}
// 흰색배경에 검은색글자
+ (UIView*)addWhiteLabelViewSwitch:(UIView*)view underView:(UIView*)target dim:(float) pix;
{
    CGRect frame = CGRectZero;
    
    if (target != nil) {
        frame = target.frame;
    }
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(kBasicMargin,
                                                              (frame.origin.y + frame.size.height + pix),
                                                              view.frame.size.width-kBasicMargin*2,
                                                              kSettingDefaultHeight)];
    bgView.backgroundColor = [UIColor whiteColor];
    //bgView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    UILabel *obj = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, bgView.frame.size.width, bgView.frame.size.height)];
    [obj setTextColor:HEXCOLOR(0x333333ff)];
    obj.tag = 11;
    [obj setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:16.0f]];
    //[obj setFont:[UIFont systemFontOfSize:14.0f]];
    [obj setNumberOfLines:1];
    obj.textAlignment = NSTextAlignmentLeft;
    obj.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    
    UIView *lineview = [[UIView alloc] initWithFrame:CGRectMake(0,
                                                                bgView.frame.size.height - 1,
                                                                bgView.frame.size.width,
                                                                1)];
    lineview.backgroundColor = UIColorBasicBack;
    lineview.tag = 222;
    
    UISwitch *s = [[UISwitch alloc] initWithFrame:CGRectMake(bgView.frame.size.width - 10 - 49,
                                                             bgView.frame.size.height/2 - 31/2, 49, 31)];
    s.on = NO;
    s.tag = 333;
    s.onTintColor = UIColorDefault;
    
    [bgView addSubview:obj];
    [bgView addSubview:lineview];
    [bgView addSubview:s];
    
    return bgView;
}
+ (void)addShadow:(UIView*)v;
{
    v.clipsToBounds = NO;
    v.layer.masksToBounds = NO;
    
    // border radius
    //[v.layer setCornerRadius:30.0f];
    
    // border
//    [v.layer setBorderColor:[UIColor blackColor].CGColor];
//    [v.layer setBorderWidth:1.0f];
    
    //UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:v.bounds];
    
    // drop shadow
    [v.layer setShadowColor:[UIColor blackColor].CGColor];
    [v.layer setShadowOpacity:0.6];
    [v.layer setShadowRadius:4.0];
    [v.layer setShadowOffset:CGSizeMake(3.0, 3.0)];
    //v.layer.shadowPath = shadowPath.CGPath;
}

+ (void)addShadowOnlyRight:(UIView*)v;
{
    v.clipsToBounds = NO;
    v.layer.masksToBounds = NO;
    
    // border radius
    //[v.layer setCornerRadius:30.0f];
    
    // border
    //    [v.layer setBorderColor:[UIColor blackColor].CGColor];
    //    [v.layer setBorderWidth:1.0f];
    
    //UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:v.bounds];
    
    // drop shadow
    [v.layer setShadowColor:[UIColor blackColor].CGColor];
    [v.layer setShadowOpacity:0.6];
    [v.layer setShadowRadius:4.0];
    [v.layer setShadowOffset:CGSizeMake(3.0, 0.0)];
    //v.layer.shadowPath = shadowPath.CGPath;
}

+ (void)addBorder:(UIView*)view;
{
    view.layer.borderWidth = 2;
    view.layer.borderColor = UIColorDefault.CGColor;
}

+ (void)layerButton:(UIButton*)button
{
//    button.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
    button.tag = 11;
    //    [button setTitle:@"Add Payment" forState:UIControlStateNormal];
    //    [button addTarget:self action:@selector(selector:) forControlEvents:UIControlEventTouchUpInside];
    
    [button setTitleColor:UIColorButtonText forState:UIControlStateNormal];
    button.backgroundColor = UIColorDefault;
    button.layer.cornerRadius = 3;
    button.titleLabel.font = [UIFont boldSystemFontOfSize: 16];
    //button.autoresizingMask = UIViewAutoresizingFlexibleHeight;
}

// 기본색에 버튼
+ (UIView*)addButtonBasic:(UIView*)view underView:(UIView*)target dim:(float) pix;
{
    CGRect frame = CGRectZero;
    
    if (target != nil) {
        frame = target.frame;
    }
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0,
                                                              pix,
                                                              view.frame.size.width,
                                                              46)];
    bgView.backgroundColor = [UIColor whiteColor];
    //bgView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    //UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(kBasicMargin, 0, bgView.frame.size.width - kBasicMargin*2, bgView.frame.size.height)];
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(kBasicMargin, 0, bgView.frame.size.width - kBasicMargin*2, bgView.frame.size.height)];
    [Utils layerButton:button];
    
//    button.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
//    button.tag = 11;
////    [button setTitle:@"Add Payment" forState:UIControlStateNormal];
////    [button addTarget:self action:@selector(selector:) forControlEvents:UIControlEventTouchUpInside];
//    
//    [button setTitleColor:UIColorButtonText forState:UIControlStateNormal];
//    button.backgroundColor = UIColorDefault;
//    button.layer.cornerRadius = 5;
    //button.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    
    [bgView addSubview:button];
    
    return bgView;
}

// 기본색에 버튼
+ (void)addButtonBlank:(UIView*)view target:(id)target selector:(SEL)selector;
{
    CGRect rect = view.frame;
    rect.origin = CGPointZero;
    
    UIButton *button = [[UIButton alloc] initWithFrame:rect];
    //button.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
    button.tag = 21;
    //    [button setTitle:@"Add Payment" forState:UIControlStateNormal];
    [button addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
    button.backgroundColor = [UIColor clearColor];
    
    [view addSubview:button];
}

+ (UIButton*)addBottomButton:(NSString*)title target:(id)target selector:(SEL)selector color:(BOOL)color;
{
    UIViewController *self_ = (UIViewController*)target;
    CGRect frame = self_.view.frame;
    
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(10, frame.size.height - kBottomButtonHeight - kBottomBottonSafeArea, frame.size.width - 20, kBottomButtonHeight)];
    button.tag = 999;
    [button setTitle:title forState:UIControlStateNormal];
    [button addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
    
    if (color) {
        button.backgroundColor = UIColorDefault;
#ifdef _WITZM
        [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
#elif defined _CHACHA
        [button setTitleColor:UIColorButtonText forState:UIControlStateNormal];
#endif
    }
    else {
        button.backgroundColor = [UIColor blackColor];
        [button setTitleColor:UIColorDefault forState:UIControlStateNormal];
    }
    button.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:18.0f];
    button.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
    
    return button;
}

+ (UIButton*)addBottomButtonRound:(NSString*)title target:(id)target selector:(SEL)selector color:(BOOL)color;
{
    UIViewController *self_ = (UIViewController*)target;
    CGRect frame = self_.view.frame;
    
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(20, frame.size.height - kBottomButtonHeight -20, frame.size.width -20*2, kBottomButtonHeight)];
    button.tag = 999;
    [button setTitle:title forState:UIControlStateNormal];
    [button addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
    
    if (color) {
        button.backgroundColor = UIColorDefault;
#ifdef _WITZM
        [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
#elif defined _CHACHA
        [button setTitleColor:UIColorButtonText forState:UIControlStateNormal];
#endif
    }
    else {
        button.backgroundColor = [UIColor blackColor];
        [button setTitleColor:UIColorDefault forState:UIControlStateNormal];
    }
    button.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:18.0f];
    
    button.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
    
    button.layer.cornerRadius = 3;
    button.clipsToBounds = YES;
    button.layer.masksToBounds = YES;
    
    return button;
}

+ (UIView*)addViewButtonText:(NSString*)title target:(id)target selector:(SEL)selector color:(BOOL)color;
{
    UIViewController *self_ = (UIViewController*)target;
    CGRect frame = self_.view.frame;
    
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, frame.size.height - kBottomButtonHeight, frame.size.width, kBottomButtonHeight)];
    
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(frame.size.width - 80 - 38, 0, 80, kBottomButtonHeight)];
    button.tag = 999;
    [button setTitle:title forState:UIControlStateNormal];
    [button addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
    button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    button.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
    
    UILabel *lbhead = [[UILabel alloc] initWithFrame:CGRectMake(17,
                                                                0,
                                                                200,
                                                                kBottomButtonHeight)];
    lbhead.tag = 998;
    lbhead.backgroundColor = [UIColor clearColor];
    [lbhead setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:14.0f]];
    [lbhead setAdjustsFontSizeToFitWidth:YES];
    [lbhead setNumberOfLines:1];
    lbhead.textAlignment = NSTextAlignmentLeft;
    
    if (color) {
        button.backgroundColor = UIColorDefault;
        bgView.backgroundColor = UIColorDefault;
        [lbhead setTextColor:[UIColor whiteColor]];
        [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    }
    else {
        button.backgroundColor = [UIColor blackColor];
        bgView.backgroundColor = [UIColor blackColor];
        [lbhead setTextColor:[UIColor whiteColor]];
        [button setTitleColor:UIColorDefault forState:UIControlStateNormal];
    }
    button.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:18.0f];
    
    button.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
    
    [bgView addSubview:button];
    [bgView addSubview:lbhead];
    
    return bgView;
}

+ (void)setCircleImage:(UIView*)view
{
    view.layer.cornerRadius = view.frame.size.width/2;
    view.clipsToBounds = YES;
    view.contentMode = UIViewContentModeScaleAspectFill;
    //view.contentMode = UIViewContentModeScaleAspectFit;
    view.layer.masksToBounds = YES;
}

+(UIImageView*)makeCircleProfile:(CGRect)rect image:(UIImage*)image tag:(NSInteger)tag
{
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:rect];
    //imageView.autoresizingMask = UIViewAutoresizingFlexibleRightMargin;
    if (image == nil) {
        imageView.image = [UIImage imageNamed:@"ic_user_white"];//ic_user
    }
    
    imageView.tag = tag;
    imageView.layer.masksToBounds = YES;
    imageView.layer.cornerRadius = imageView.frame.size.width/2;
    //    imageView.layer.borderColor = [UIColor whiteColor].CGColor;
    //    imageView.layer.borderWidth = 3.0f;
//    imageView.layer.rasterizationScale = [UIScreen mainScreen].scale;
//    imageView.layer.shouldRasterize = YES;
    imageView.clipsToBounds = YES;
    imageView.contentMode = UIViewContentModeScaleAspectFill;
    
    return imageView;
}

+ (CGRect)frameUnder:(UIView *) view dim:(float) pix;
{
    CGRect viewFrame = view.frame;
    return CGRectMake(viewFrame.origin.x,
                      (viewFrame.origin.y + viewFrame.size.height + pix),
                      viewFrame.size.width,
                      viewFrame.size.height);
}

+ (float)frameOverY:(UIView *) view overView:(UIView *) overview dim:(float) pix;
{
    return (overview.frame.origin.y - pix - view.frame.size.height);
}

+ (float)frameUnderY:(UIView *) view dim:(float) pix;
{
    CGRect viewFrame = view.frame;
    return (viewFrame.origin.y + viewFrame.size.height + pix);
}

+ (float)frameCenterX:(UIView *) view baseView:(UIView*)baseView
{
    CGRect viewFrame = view.frame;
    CGRect baseFrame = baseView.frame;
    
    return (baseFrame.size.width/2 - viewFrame.size.width/2);
}

+ (BOOL)isDictionary:(NSDictionary*)dic
{
    if (!dic || dic == nil || dic == (id)[NSNull null]) return NO;
    if (dic && ([dic isKindOfClass:[NSDictionary class]] || [dic isKindOfClass:[NSMutableDictionary class]]) && [dic count]) return YES;
    return NO;
}

+ (BOOL)isArray:(NSArray*)dic
{
    if (!dic || dic == nil || dic == (id)[NSNull null]) return NO;
    if (dic && ([dic isKindOfClass:[NSArray class]] || [dic isKindOfClass:[NSMutableArray class]]) && [dic count]) return YES;
    return NO;
}

+ (BOOL)isNill:(id)obj
{
    if([obj isKindOfClass:[NSArray class]] || [obj isKindOfClass:[NSMutableArray class]]){
        NSArray *dic = (NSArray*)obj;
        //Is array
        if (dic && dic != (id)[NSNull null] && [dic count]) return NO;
        return YES;
    }else if([obj isKindOfClass:[NSDictionary class]] || [obj isKindOfClass:[NSMutableDictionary class]]){
        NSDictionary *dic = (NSDictionary*)obj;
        //is dictionary
        if (dic && dic != (id)[NSNull null] && dic != NULL) return NO;
        return YES;
    }else if([obj isKindOfClass:[NSNull class]]){
        NSString *dic = (NSString*)obj;
        if (dic && dic != (id)[NSNull null] && dic != NULL) return NO;
        return YES;
        //is something else
    }else{
        if (obj) return NO;
        return YES;
        //is something else
    }
    return NO;
}


+ (BOOL)isNillDictionary:(NSDictionary*)dic
{
    if (dic && dic != (id)[NSNull null] && dic != NULL && dic != NO  && dic != nil) return NO;
    return YES;
}

+ (BOOL)isNillArray:(NSArray*)dic
{
    if (dic && dic != (id)[NSNull null] && [dic count]) return NO;
    return YES;
}

+(BOOL) isNotAllowedMenu
{
    TXAppDelegate *appDelegate = (TXAppDelegate *)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.isDriverMode && appDelegate.ustate < 120) {
        return NO;
    }
    if (appDelegate.isDriverMode && appDelegate.pstate < 220) {
        return NO;
    }
    
    static LGAlertView *alertView = nil;
    
    if (alertView) {
        return YES;
    }
    
    alertView = [[LGAlertView alloc] initWithTitle:nil
                                           message:LocalizedStr(@"Map.Drive.Block.Menu.title")
                                             style:LGAlertViewStyleAlert
                                      buttonTitles:@[LocalizedStr(@"Button.OK")]
                                 cancelButtonTitle:nil
                            destructiveButtonTitle:nil
                                     actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                                         alertView = nil;
                                     }
                                     cancelHandler:^(LGAlertView *alertView) {
                                         NSLog(@"cancelHandler");
                                     }
                                destructiveHandler:^(LGAlertView *alertView) {
                                    NSLog(@"destructiveHandler");
                                }];
    [Utils initAlertButtonColor:alertView];
    [alertView showAnimated:YES completionHandler:^(void)
     {
         dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
             [alertView dismissAnimated:YES completionHandler:nil];
             alertView = nil;
         });
     }];
    
    return YES;
}

+(void) isNotOnlineMenu
{
    LGAlertView* alertView = [[LGAlertView alloc] initWithTitle:nil
                                                        message:LocalizedStr(@"String.Driver.Permit.None.online")
                                                          style:LGAlertViewStyleAlert
                                                   buttonTitles:@[LocalizedStr(@"Button.OK")]
                                              cancelButtonTitle:nil
                                         destructiveButtonTitle:nil
                                                  actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                                                      alertView = nil;
                                                  }
                                                  cancelHandler:^(LGAlertView *alertView) {
                                                      NSLog(@"cancelHandler");
                                                  }
                                             destructiveHandler:^(LGAlertView *alertView) {
                                                 NSLog(@"destructiveHandler");
                                             }];
    [Utils initAlertButtonColor:alertView];
    [alertView showAnimated:YES completionHandler:^(void)
     {
         dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
             [alertView dismissAnimated:YES completionHandler:nil];
         });
     }];
}

+(void)initAlertButtonColor:(LGAlertView*)alertView
{
    alertView.buttonsTitleColor = UIColorBasicText;
    alertView.buttonsTitleColorHighlighted = UIColorBasicText;
    alertView.buttonsBackgroundColorHighlighted = HEXCOLOR(0x7777770A);
    alertView.buttonsHeight = 48;
    
#ifdef _WITZM
    alertView.cancelButtonTitleColor = UIColorRedColor;
    alertView.cancelButtonTitleColorHighlighted = UIColorRedColor;
#elif defined _CHACHA
    alertView.cancelButtonTitleColor = [UIColor blackColor];
    alertView.cancelButtonTitleColorHighlighted = [UIColor blackColor];
#endif
    alertView.cancelButtonBackgroundColorHighlighted = HEXCOLOR(0x7777770A);
}

+ (void)onErrorAlert:(NSString *)failureMessage
{
    
//    NSMutableDictionary *options = [@{kCRToastNotificationTypeKey               : @(CRToastTypeNavigationBar),
//                                      kCRToastNotificationPresentationTypeKey   : @(CRToastPresentationTypeCover),
//                                      kCRToastUnderStatusBarKey                 : @(YES),
//                                      kCRToastTextKey                           : failureMessage,
//                                      kCRToastFontKey                           : [UIFont systemFontOfSize:14],
//                                      kCRToastTextAlignmentKey                  : @(NSTextAlignmentCenter),
//                                      kCRToastTimeIntervalKey                   : @(2),
//                                      kCRToastAnimationInTypeKey                : @(CRToastAnimationTypeGravity),
//                                      kCRToastAnimationOutTypeKey               : @(CRToastAnimationTypeGravity),
//                                      kCRToastAnimationInDirectionKey           : @(CRToastAnimationDirectionTop),
//                                      kCRToastAnimationOutDirectionKey          : @(CRToastAnimationDirectionTop),
//                                      kCRToastNotificationPreferredPaddingKey   : @(15)} mutableCopy];
//    [CRToastManager showNotificationWithOptions:options
//                                completionBlock:^{
//                                    NSLog(@"Completed");
//                                }];
    
    [WToast showWithText:failureMessage duration:kWTShort roundedCorners:NO gravity:kWTGravityTop];
}

+ (void)onErrorAlert:(NSString *)failureMessage kwtlong:(BOOL)kwtlong
{
    if (kwtlong) {
        [WToast showWithText:failureMessage duration:kWTLong roundedCorners:NO gravity:kWTGravityTop];
    }
    else {
        [WToast showWithText:failureMessage duration:kWTShort roundedCorners:NO gravity:kWTGravityTop];
    }
}

+ (void)onErrorAlertViewTitleAlign:(NSString *)failureMessage title:(NSString*)title align:(NSTextAlignment)align
{
    LGAlertView *alertView = [[LGAlertView alloc] initWithTitle:title
                                                        message:failureMessage
                                                          style:LGAlertViewStyleAlert
                                                   buttonTitles:@[LocalizedStr(@"Button.OK")]
                                              cancelButtonTitle:nil
                                         destructiveButtonTitle:nil
                                                  actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                                                      
                                                  }
                                                  cancelHandler:^(LGAlertView *alertView) {
                                                      
                                                  }
                                             destructiveHandler:^(LGAlertView *alertView) {
                                                 
                                             }];
    [Utils initAlertButtonColor:alertView];
    alertView.messageTextAlignment = align;
    [alertView showAnimated:YES completionHandler:^(void)
     {
         
     }];
    
}

+ (void)onErrorAlertView:(NSString *)failureMessage
{
    [Utils onErrorAlertViewTitle:failureMessage title:@""];
    
}

+ (void)onErrorAlertViewTitle:(NSString *)failureMessage title:(NSString*)title
{
    LGAlertView *alertView = [[LGAlertView alloc] initWithTitle:title
                                                        message:failureMessage
                                                          style:LGAlertViewStyleAlert
                                                   buttonTitles:@[LocalizedStr(@"Button.OK")]
                                              cancelButtonTitle:nil
                                         destructiveButtonTitle:nil
                                                  actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                                                      
                                                  }
                                                  cancelHandler:^(LGAlertView *alertView) {
                                                      
                                                  }
                                             destructiveHandler:^(LGAlertView *alertView) {
                                                 
                                             }];
    [Utils initAlertButtonColor:alertView];
    [alertView showAnimated:YES completionHandler:^(void)
     {
         
     }];
    
}

+ (void)onErrorAlertViewTitle:(NSString *)failureMessage title:(NSString*)title timeout:(NSInteger)timeout
{
    __block LGAlertView *alertView = [[LGAlertView alloc] initWithTitle:title
                                                        message:failureMessage
                                                          style:LGAlertViewStyleAlert
                                                   buttonTitles:@[LocalizedStr(@"Button.OK")]
                                              cancelButtonTitle:nil
                                         destructiveButtonTitle:nil
                                                  actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                                                      
                                                  }
                                                  cancelHandler:^(LGAlertView *alertView) {
                                                      
                                                  }
                                             destructiveHandler:^(LGAlertView *alertView) {
                                                 
                                             }];
    [Utils initAlertButtonColor:alertView];
    [alertView showAnimated:YES completionHandler:^(void)
     {
         if (timeout) {
             dispatch_after(dispatch_time(DISPATCH_TIME_NOW, timeout * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                 [alertView dismissAnimated:YES completionHandler:nil];
                 alertView = nil;
             });
         }
         
     }];
}

+ (void)runBlock:(void (^)())block
{
    block();
}
+ (void)runAfterDelay:(CGFloat)delay block:(void (^)())block
{
    void (^block_)() = [block copy];
    [self performSelector:@selector(runBlock:) withObject:block_ afterDelay:delay];
}

#pragma mark - Notification
+(void)sendNotofication:(NSString*)msg title:(NSString*)title
{
    UILocalNotification* local = [[UILocalNotification alloc]init];
    if (local)
    {
        [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
        [[UIApplication sharedApplication] cancelAllLocalNotifications];
        
        local.fireDate = [NSDate dateWithTimeIntervalSinceNow:0.1];
        local.alertTitle = title;
        local.alertBody = msg;
        local.timeZone = [NSTimeZone defaultTimeZone];
        local.soundName = SOUND_NOTI;//UILocalNotificationDefaultSoundName;
        [[UIApplication sharedApplication] scheduleLocalNotification:local];
        
        //[self performSelector:@selector(changeDestinationNotofication) withObject:nil afterDelay:5];
    }
}

+(void)sendNotofication:(NSString*)msg
{
    UILocalNotification* local = [[UILocalNotification alloc]init];
    if (local)
    {
        [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
        [[UIApplication sharedApplication] cancelAllLocalNotifications];
        
        local.fireDate = [NSDate dateWithTimeIntervalSinceNow:0.1];
        local.alertBody = msg;
        local.timeZone = [NSTimeZone defaultTimeZone];
        local.soundName = SOUND_NOTI;//UILocalNotificationDefaultSoundName;
        [[UIApplication sharedApplication] scheduleLocalNotification:local];
        
        //[self performSelector:@selector(changeDestinationNotofication) withObject:nil afterDelay:5];
    }
}

#pragma mark - ALRIM
+(void)updateChatInfo:(NSArray*)mtext
{
    NSString *oseq = mtext[0][@"oseq"];
    NSArray* chatList = (NSArray*)CHAT_SELECT(oseq);
    if ([chatList count]) {
        oseq = chatList[0][@"oseq"];
        NSString *mseq = chatList[0][@"mseq"];
        CHAT_UPDATE(oseq, mseq);
    }
    else {
        oseq = mtext[[mtext count]-1][@"oseq"];
        NSString *mseq = mtext[[mtext count]-1][@"mseq"];
        CHAT_INSERT(oseq, mseq);
    }
    
    [Utils updateAlrim:CODE_TYPE_CHAT code:CODE_ALRIM_CHAT(oseq) value:CODE_FLAG_OFF];
}

+(void)updateBGChatInfo:(NSString*)oseq
{
    NSArray* chatList = (NSArray*)CHAT_SELECT(oseq);
    //NSLog(@"updateBGChatInfo:%@",chatList);
    if ([chatList count]) {
        oseq = chatList[0][@"oseq"];
        CHAT_UPDATE_BG(oseq);
    }
    else {
        CHAT_INSERT_BG(oseq);
    }
    [Utils updateAlrim:CODE_TYPE_CHAT code:CODE_ALRIM_CHAT(oseq) value:CODE_FLAG_ON];
}

+(BOOL)showAlrim
{
    NSArray* alrimList = (NSArray*)ALRIM_SELECT_TOTAL_ALL;
    //NSLog(@"alrimList %@",alrimList);
    if ([alrimList count] == 1) {
        if ([alrimList[0][@"read"] isKindOfClass:[NSNull class]]) {
            return NO;
        }
    }
    
    if ([alrimList count]) {
        NSString *read = [NSString stringWithFormat:@"%@", alrimList[0][@"read"]];
        if (![read isEqualToString:@"0"]) {
            // badge on
            return YES;
        }
    }
    // badge off
    return NO;
}

+(BOOL)showAlrim:(NSString*)type
{
    NSArray* alrimList = (NSArray*)ALRIM_SELECT_TOTAL(type);
    //NSLog(@"alrimList %@:%@",type, alrimList);
    if ([alrimList count] == 1) {
        if ([alrimList[0][@"read"] isKindOfClass:[NSNull class]]) {
            return NO;
        }
    }
    
    if ([alrimList count]) {
        NSString *read = [NSString stringWithFormat:@"%@", alrimList[0][@"read"]];
        if (![read isEqualToString:@"0"]) {
            // badge on
            return YES;
        }
    }
    // badge off
    return NO;
}

+(BOOL)checkAlrim:(NSString*)type
{
    NSArray* alrimList = (NSArray*)ALRIM_SELECT_TOTAL(type);
    //NSLog(@"alrimList %@:%@",type, alrimList);
    if ([alrimList count] == 1) {
        if ([alrimList[0][@"read"] isKindOfClass:[NSNull class]]) {
            return NO;
        }
    }
    
    if ([alrimList count]) {
        return YES;
    }
    return NO;
}

+(BOOL)checkAlrim:(NSString*)type code:(NSString*)code
{
    NSArray* alrimList = (NSArray*)ALRIM_SELECT(type,code);
    //NSLog(@"alrimList:%@",alrimList);
    if ([alrimList count]) {
        return YES;
    }
    return NO;
}

+(BOOL)showAlrim:(NSString*)type code:(NSString*)code
{
    NSArray* alrimList = (NSArray*)ALRIM_SELECT(type,code);
    
    if ([alrimList count]) {
        //NSLog(@"alrimList code:%@",alrimList);
        NSString *read = [NSString stringWithFormat:@"%@", alrimList[[alrimList count]-1][@"read"]];
        if (![read isEqualToString:@"0"]) {
            // badge on
            return YES;
        }
    }
    
    // badge off
    return NO;
}

+(void)deleteAlrim:(NSString*)type code:(NSString*)code
{
    ALRIM_DELETE(type,code);
}

// 햄버거 메뉴에 표시되는 붉은 점
+(void)updateAlrim:(NSString*)type code:(NSString*)code value:(NSString*)val
{
    if ([Utils checkAlrim:type code:code]) {
        ALRIM_UPDATE(type,code,val);
    }
    else {
        ALRIM_INSERT(type,code,val);
    }
}


+(void)newBadge:(UIButton*)btnBadge show:(BOOL)flag
{
    //btnBadge.badge = @"N";
    if (flag) {
        //[btnBadge showBadgeWithStyle:WBadgeStyleNew value:0 animationType:WBadgeAnimTypeNone];
        btnBadge.badgeValue   = @"new";
    }
    else {
        //[btnBadge showBadgeWithStyle:WBadgeStyleNew value:0 animationType:WBadgeAnimTypeNone];
        //[btnBadge clearBadge];
        btnBadge.badgeValue   = @"";
    }
    
    //    btnBadge.badgeValue   = @"N";
    btnBadge.badgeOriginX = btnBadge.frame.size.width - btnBadge.badge.frame.size.width/2;
    btnBadge.badgeOriginY = 1;
    btnBadge.badgeBGColor = [UIColor redColor];
    
    CGRect frame = btnBadge.badge.frame;
    frame.size.width = 22;
    frame.size.height = 13;
    btnBadge.badge.frame = frame;
    
    //btnBadge.badge.center = CGPointMake(CGRectGetWidth(btnBadge.frame) + 2 + self.badgeCenterOffset.x, self.badgeCenterOffset.y);
    btnBadge.badge.font = [UIFont boldSystemFontOfSize:9];
    btnBadge.badge.layer.cornerRadius = CGRectGetHeight(btnBadge.badge.frame) / 3;
}

#pragma mark - Additional Util
+(BOOL)pointCompare:(double)param1 comp:(double)param2 {
    
    NSString *p1 = [NSString stringWithFormat:@"%.6f",param1];
    NSString *p2 = [NSString stringWithFormat:@"%.6f",param2];
    
    p1 = [Utils replacingWithPattern2:p1 pattern:@"0*$" withTemplate:@"" error:nil];
    p2 = [Utils replacingWithPattern2:p2 pattern:@"0*$" withTemplate:@"" error:nil];
    
    if ([p1 isEqualToString:p2]) {
        return YES;
    }
    return NO;
}

+(NSString*)formattedPositionR:(CLLocationCoordinate2D)position {
    
    //NSLog(@"formattedPosition:%f,%f",position.latitude, position.longitude);
    NSString *fpos = @"0.0,0.0";
    @try {
        fpos = [NSString stringWithFormat:@"%.6f,%.6f",position.longitude,position.latitude];
    }
    @catch (NSException *exception) {
        NSLog(@"Exception: %@, %@", exception.name, exception.reason);
    }
    return fpos;
}

+(NSString*)formattedPosition:(CLLocationCoordinate2D)position {
    
    //NSLog(@"formattedPosition:%f,%f",position.latitude, position.longitude);
    NSString *fpos = @"0.0,0.0";
    @try {
        fpos = [NSString stringWithFormat:@"%.6f,%.6f",position.latitude,position.longitude];
    }
    @catch (NSException *exception) {
        NSLog(@"Exception: %@, %@", exception.name, exception.reason);
    }
    return fpos;
}

+(double)formattedPositionCoor:(double)coor
{
    return [[NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%.6f",coor]] doubleValue];
}

+(BOOL) isEqualLocation:(CLLocationCoordinate2D) coord1 location:(CLLocationCoordinate2D) coord2{
    if ([Utils pointCompare:coord1.latitude comp:coord2.latitude] && [Utils pointCompare:coord1.longitude comp:coord2.longitude]) {
        return YES;
    }
    return NO;
}

+(CLLocationCoordinate2D) getLocation:(NSString*) location{
    location = [Utils nullToString:location];
    if ([location isEqualToString:@""]) {
        location = @"0,0";
    }
    NSArray* stringComponents = [location componentsSeparatedByString:@","];
    
    CLLocationCoordinate2D position;
    position.latitude  = [Utils formattedPositionCoor:[[stringComponents objectAtIndex:0] doubleValue]];
    position.longitude = [Utils formattedPositionCoor:[[stringComponents objectAtIndex:1] doubleValue]];
    
    return position;
}

+(NSString*) getLocationByString:(CLLocationCoordinate2D) location{
    return [Utils formattedPosition:location];
}

+(double) geoConvert:(double)dist unit:(NSString*)unit {
    if ([unit isEqualToString:@"K"]) {
        return (dist * 1.609344);
    } else {
        return (dist * 1609.344);
    }
}

#pragma mark - Call Utils

+(NSString*)acall:(NSDictionary*)item {
    NSDictionary *phoneno  = nil;
    NSString* acall = @"";
    NSDictionary *drive_order  = nil;
    NSDictionary *drive_pickup = nil;
    NSDictionary *drive_trip   = nil;
    
    drive_order  = [item objectForKey:@"drive_order"];
    drive_pickup = [item objectForKey:@"drive_pickup"];
    drive_trip   = [item objectForKey:@"drive_trip"];

    // 드라이버가 acall로 라이더에게 전화를 건다.
    // 라이더가 bcall로 드라이버에게 전화를 건다.
#ifdef _DRIVER_MODE
    if ([Utils isDictionary:drive_order] && ![[Utils nullToString:drive_order[@"acall"]] isEqualToString:@""] && [drive_order[@"acall"] length]>=8) {
        acall = [drive_order valueForKey:@"acall"];;
    }
    else if ([Utils isDictionary:drive_pickup] && ![[Utils nullToString:drive_pickup[@"acall"]] isEqualToString:@""] && [drive_pickup[@"acall"] length]>=8) {
        acall = [drive_pickup valueForKey:@"acall"];;
    }
    else if ([Utils isDictionary:drive_trip] && ![[Utils nullToString:drive_trip[@"acall"]] isEqualToString:@""] && [drive_trip[@"acall"] length]>=8) {
        acall = [drive_trip valueForKey:@"acall"];;
    }
#else
    if ([Utils isDictionary:drive_order] && ![[Utils nullToString:drive_order[@"bcall"]] isEqualToString:@""] && [drive_order[@"bcall"] length]>=8) {
        acall = [drive_order valueForKey:@"bcall"];;
    }
    else if ([Utils isDictionary:drive_pickup] && ![[Utils nullToString:drive_pickup[@"bcall"]] isEqualToString:@""] && [drive_pickup[@"bcall"] length]>=8) {
        acall = [drive_pickup valueForKey:@"bcall"];;
    }
    else if ([Utils isDictionary:drive_trip] && ![[Utils nullToString:drive_trip[@"bcall"]] isEqualToString:@""] && [drive_trip[@"bcall"] length]>=8) {
        acall = [drive_trip valueForKey:@"bcall"];;
    }
#endif
    
    TXAppDelegate *appDelegate = (TXAppDelegate *)[[UIApplication sharedApplication] delegate];
    if ([acall isEqualToString:@""]) {
        
#ifdef _WITZM
        if (appDelegate.isDriverMode) {
            phoneno = [item objectForKey:@"user_device"];
        }
        else {
            phoneno  = [item objectForKey:@"provider_device"];
        }
#elif defined _CHACHA
        if (appDelegate.isDriverMode) {
            phoneno = [item objectForKey:@"user_device"];
        }
        else {
            phoneno  = [item objectForKey:@"provider_device"];
        }
#endif

        if ([Utils isDictionary:phoneno]) {
            //acall = [drive_order valueForKey:@"acall"];
            acall = [phoneno valueForKey:@"telno"];
            //acall = [phoneno valueForKey:@"utelno"];
        }
    }
    
    if ([[UIDevice currentDevice].model isEqualToString:@"iPad"]) {
        acall = @"";
    }
    
    if (![acall isEqualToString:@""] && acall != nil) {
        
        // Check if the device can place a phone call
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"tel://"]]) {
            // Device supports phone calls, lets confirm it can place one right now
            CTTelephonyNetworkInfo *netInfo = [[CTTelephonyNetworkInfo alloc] init];
            CTCarrier *carrier = [netInfo subscriberCellularProvider];
            NSString *mnc = [carrier mobileNetworkCode];
            NSLog(@"mnc:%@",mnc);
            if (([mnc length] == 0) || ([mnc isEqualToString:@"65535"]) || mnc == nil || [mnc isEqualToString:@""]) {
                // Device cannot place a call at this time.  SIM might be removed.
                //isSimCardAvailable = NO;
                return @"";
            } else {
                // Device can place a phone call
                //isSimCardAvailable = YES;
                return acall;
            }
        } else {
            // Device does not support phone calls
            //isSimCardAvailable =  NO;
            return @"no";
        }
    }
    return acall;
}

#pragma mark - Date Utils

//-------------------------------------------------------------------------------------------------------------------------------------------------
+ (NSDate*)StringFormat2Date:(NSString *)dateStr
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:DATE_FORMAT_YYYYMMDDHH24MISS];
    //[formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    return [formatter dateFromString:dateStr];
}

//-------------------------------------------------------------------------------------------------------------------------------------------------
+ (NSString*)StringFormattedDate:(NSString *)dateStr format:(NSString *)format
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:format];
    NSDate *date;
    if (dateStr == nil) {
        date = [NSDate date];
    }
    else {
        date = [Utils StringFormat2Date:dateStr];
    }
    
    return [formatter stringFromDate:date];
}

@end
