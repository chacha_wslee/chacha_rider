//
//  TXAppDelegate.m
//  Taxi
//

//

@import Firebase;
@import FirebaseInstanceID;
//@import FirebaseMessaging;

#ifndef _DRIVER_MODE
#import <AdSupport/AdSupport.h>
#import <IgaworksCore/IgaworksCore.h>
#endif

#ifdef _DRIVER_MODE
//    @import Dotzu;
#endif

#ifdef _WITZM
#import <Bolts/Bolts.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <NaverThirdPartyLogin/NaverThirdPartyLogin.h>
#import <KakaoOpenSDK/KakaoOpenSDK.h>
#elif defined _CHACHA
#import <Bolts/Bolts.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <NaverThirdPartyLogin/NaverThirdPartyLogin.h>
#import <KakaoOpenSDK/KakaoOpenSDK.h>
#endif

#ifdef _DRIVER_MODE
    #import <SpotifyAuthentication/SpotifyAuthentication.h>
    #import <SpotifyMetadata/SpotifyMetadata.h>
    #import <MediaPlayer/MediaPlayer.h>

#ifdef _WITZM
#import "TXAboutVC.h"
#elif defined _CHACHA
#import "TXAboutVC.h"
#endif

#endif

#import <AVFoundation/AVFoundation.h>
#import "TXAppDelegate.h"
#import "TXHttpRequestManager.h"
#import "utils.h"
#import "TXUserModel.h"
#import "TXFileManager.h"
#import <Foundation/Foundation.h>
#import <GoogleMaps/GoogleMaps.h>
#import "TXCode2MsgTranslator.h"
#import "TXApp.h"
#import "TXLauncherVC.h"
#import "NSData+StringBytes.h"
#import "TXMapVC.h"
#import "TXUserModel.h"
#import "TXApp.h"
#import "SVProgressHUD.h"
#import "TXMainVC.h"
//#import "TXCallModel.h"
#import "WToast.h"
#import <unistd.h>
#import "INTULocationManager.h"

//------------------------------------------------------------------------------------------------------------------------------

#import "REFrostedViewController.h"
#import "MenuNavigationController.h"
#import "MenuViewController.h"

//#import "Reachability.h"
#import "RealReachability.h"
#import "PingHelper.h"

#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import <CoreTelephony/CTCarrier.h>
#import <UserNotifications/UserNotifications.h>

#define kDefaultHost @"www.apple.com"
#define kDefaultCheckInterval 2.0f
#define kDefaultPingTimeout 2.0f

#define kMinAutoCheckInterval 0.3f
#define kMaxAutoCheckInterval 60.0f

//#ifdef _DEBUG
////#define NSLog( s, ... ) _Log( @"<%@:(%d)> %@", [[NSString stringWithUTF8String:__FILE__] lastPathComponent], __LINE__, [NSString stringWithFormat:(s), ##__VA_ARGS__] )
//#define NSLog( s, ... ) _Log(@"DEBUG ", [[NSString stringWithUTF8String:__FILE__] lastPathComponent],__LINE__,__PRETTY_FUNCTION__,[NSString stringWithFormat:(s), ##__VA_ARGS__]);
//#else
//#define NSLog( s, ... )
//#endif
//
//@interface Log : NSObject
//void _Log(NSString *prefix, NSString *file, int lineNumber, NSString *funcName, NSString *format,...);
//@end
//
//void _Log(NSString *prefix, NSString *file, int lineNumber, NSString *funcName, NSString *format,...) {
//    va_list ap;
//    va_start (ap, format);
//    format = [format stringByAppendingString:@"\n"];
//    NSString *msg = [[NSString alloc] initWithFormat:[NSString stringWithFormat:@"%@",format] arguments:ap];
//    va_end (ap);
//    fprintf(stderr,"%s%s:%3d - %s",[prefix UTF8String], [funcName UTF8String], lineNumber, [msg UTF8String]);
//    NSLog(@"%s",[prefix UTF8String]);
//}

@interface TXAppDelegate () <AVAudioPlayerDelegate, UNUserNotificationCenterDelegate, CLLocationManagerDelegate> {
    NSInteger errorCount;
    
    CLLocationManager *locationMgr;
    
    unsigned long longRunTask;
    NSTimer *theTimer;
    int count;
    
    LGAlertView *alertView;
    
    NSDate *networkTimer;
    
    WToast * networkToast;
    
    // redis
    id observer;
}
@property (nonatomic, retain) AVAudioPlayer *loopPlayer;
// dictionary
@property (nonatomic, strong) NSMutableArray *keys;;

@end

@implementation TXAppDelegate
@synthesize frostedViewController;
@synthesize stdErrRedirected;


BOOL iShouldKeepBuzzing;


#pragma mark - logfile
static int savedStdErr = 0;

- (void)redirectStdErrToFile
{
    if (!stdErrRedirected)
    {
        stdErrRedirected = YES;
        savedStdErr = dup(STDERR_FILENO);
        
        NSString* oseq = @"";
        if (self.isDriverMode) {
            oseq = self.dicDriver[@"provider_state"];
        }
        else {
            oseq = self.dicRider[@"user_state"];
        }
        if ([oseq isEqualToString:@""]) return;
        NSString *fileName = [NSString stringWithFormat:@"%@.txt",oseq];
        
        NSString *cachesDirectory = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString *logPath = [cachesDirectory stringByAppendingPathComponent:fileName];
        freopen([logPath cStringUsingEncoding:NSASCIIStringEncoding], "a+", stderr);
    }
}

- (void)restoreStdErr
{
    if (stdErrRedirected)
    {
        stdErrRedirected = NO;
        fflush(stderr);
        
        dup2(savedStdErr, STDERR_FILENO);
        close(savedStdErr);
        savedStdErr = 0;
    }
}

-(NSString *)readStringFromFile{
    
    NSString* oseq = @"";
    if (self.isDriverMode) {
        oseq = self.dicDriver[@"provider_state"];
    }
    else {
        oseq = self.dicRider[@"user_state"];
    }
    if ([oseq isEqualToString:@""]) return @"";
    NSString *fileName = [NSString stringWithFormat:@"%@.txt",oseq];
    
    NSString *cachesDirectory = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *logPath = [cachesDirectory stringByAppendingPathComponent:fileName];
    return [[NSString alloc] initWithData:[NSData dataWithContentsOfFile:logPath] encoding:NSUTF8StringEncoding];
}

-(NSString *)readStringFromFile:(NSString*)oseq{
    
    NSString *fileName = [NSString stringWithFormat:@"%@.txt",oseq];
    
    NSString *cachesDirectory = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *logPath = [cachesDirectory stringByAppendingPathComponent:fileName];
    return [[NSString alloc] initWithData:[NSData dataWithContentsOfFile:logPath] encoding:NSUTF8StringEncoding];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
#ifdef _WITZM
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    
    if (launchOptions[UIApplicationLaunchOptionsURLKey] == nil) {
        [FBSDKAppLinkUtility fetchDeferredAppLink:^(NSURL *url, NSError *error) {
            if (error) {
                NSLog(@"Received error while fetching deferred app link %@", error);
            }
            if (url) {
                [[UIApplication sharedApplication] openURL:url];
            }
        }];
    }
    
    [KOSession sharedSession].clientSecret = kKakaoConsumerSecret;
#elif defined _CHACHA
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    
    if (launchOptions[UIApplicationLaunchOptionsURLKey] == nil) {
        [FBSDKAppLinkUtility fetchDeferredAppLink:^(NSURL *url, NSError *error) {
            if (error) {
                NSLog(@"Received error while fetching deferred app link %@", error);
            }
            if (url) {
                [[UIApplication sharedApplication] openURL:url];
            }
        }];
    }
    
    [KOSession sharedSession].clientSecret = kKakaoConsumerSecret;
#endif

#ifndef _DRIVER_MODE
    if (NSClassFromString(@"ASIdentifierManager")){
        NSUUID *ifa =[[ASIdentifierManager sharedManager]advertisingIdentifier];
        BOOL isAppleAdvertisingTrackingEnalbed = [[ASIdentifierManager sharedManager]isAdvertisingTrackingEnabled];
        [IgaworksCore setAppleAdvertisingIdentifier:[ifa UUIDString] isAppleAdvertisingTrackingEnabled:isAppleAdvertisingTrackingEnalbed];
        
        NSLog(@"[ifa UUIDString] %@", [ifa UUIDString]);
    }
    
    [IgaworksCore igaworksCoreWithAppKey:kIgaWorksAppKey andHashKey:kIgaWorksHashKey];
    [IgaworksCore setLogLevel:IgaworksCoreLogInfo];
#endif
    
    [Utils setFirstLogin];

    self.locationDelegate = nil;
    self.isMapView = NO;
    networkToast = nil;
    //[[NSUserDefaults standardUserDefaults] setBool:NO forKey:VIEWTUTORIAL];
    //[application setStatusBarHidden:YES];
    self.stateUI        = [[NSMutableDictionary alloc] init];
    self.stateUIBefore  = [[NSMutableDictionary alloc] init];
    self.checkr_data    = [[NSMutableDictionary alloc] init];
    self.loopArray      = [[NSMutableDictionary alloc] init];
    self.keys           = [[NSMutableArray alloc] init];
    // 앱이 꺼지지 않게 처리
    [[UIApplication sharedApplication] setIdleTimerDisabled:YES];

    //---------------------------------------------------------------------------------------------------------------------------------------------
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(networkChanged:)
                                                 name:kRealReachabilityChangedNotification
                                               object:nil];
    
    //---------------------------------------------------------------------------------------------------------------------------------------------
    NSDictionary *remoteNotificationInfo = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    if ( remoteNotificationInfo ) {
        // noti로 들어오면 push를 지운다
        [[UIApplication sharedApplication] cancelAllLocalNotifications];
    }
    
    self.signupViewStep = -1;
    errorCount = 0;
    self.isNetwork = YES;
    [self checkNetwork];
    self.locationRequestID = NSNotFound;

    //------------------------------------------------------------------------------------------------------------------------------
    self.becomePseq = @"";
    self.becomeVseq = @"";
    // 백그라운드에서 포그라운드로 2분이네 돌아오면 로그인하지않는다. 초기화
#ifndef _DRIVER_MODE
    [[[TXApp instance] getSettings] setFDKeychain:RIDER_ENTER_BACKGROUND_TIME value:@"0"];
#endif
    
    //------------------------------------------------------------------------------------------------------------------------------
    _isKorea = NO;
#ifdef _WITZM
    _isKorea = YES;
#elif defined _CHACHA
    _isKorea = YES;
#else
    NSString *langID    = [[NSLocale preferredLanguages] objectAtIndex:0];
    NSArray* foo = [langID componentsSeparatedByString: @"-"];
    if ([foo count]>1) {
        NSString *national = [foo objectAtIndex:1]; // [0]language [1]national
        if ([national isEqualToString:@"KR"]) {
            _isKorea = YES;
        }
    }
    
    NSLocale *locale = [NSLocale currentLocale];
    NSString *countryCode = [locale objectForKey: NSLocaleCountryCode];
    if ([countryCode isEqualToString:@"KR"]) {
        _isKorea = YES;
    }
    NSTimeZone* SystemTimeZone = [NSTimeZone systemTimeZone];
    if ([SystemTimeZone.name isEqualToString:@"Asia/Seoul"]) {
        _isKorea = YES;
    }
    else {
        _isKorea = NO;
    }
#endif
    
#ifdef _DEBUG_USA
    _isKorea = NO;
#endif
    
    //    NSLog(@"lang:%@:%@",langID,national);
    //
    //    NSTimeZone* SystemTimeZone = [NSTimeZone systemTimeZone];
    //    NSLog(@"lang:%@",[NSLocale currentLocale]);
    //    NSLog(@"lang:%@,%@",SystemTimeZone,SystemTimeZone.name);
    //------------------------------------------------------------------------------------------------------------------------------
    
    //------------------------------------------------------------------------------------------------------------------------------
    // push token 업로드 여부
//    self.isUploadPushToken = NO;
//    if (![[[[TXApp instance] getSettings] getNotificationsToken] isEqualToString:@""])
//        self.isUploadPushToken = YES;
    
//    self.isUploadPushToken = NO;
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    
    //------------------------------------------------------------------------------------------------------------------------------
    [self startSingleLocationRequest];
    //------------------------------------------------------------------------------------------------------------------------------
    
    // drive mode 초기화
    NSString* mode = [[[TXApp instance] getSettings] getDriveMode];
    if ([[Utils nullToString:mode] isEqualToString:@""]) {
        [[[TXApp instance] getSettings] setDriveMode:DRIVE_MODE_DRIVING];
    }
    [[[TXApp instance] getSettings] setDriveMode:DRIVE_MODE_DRIVING];
    
    //------------------------------------------------------------------------------------------------------------------------------
    // Register for remote notifications
    [self registerForRemoteNotification];

    // [START configure_firebase]
    
#ifdef _WITZM
    [Fabric.sharedSDK setDebug:YES];
#elif defined _CHACHA
    [FIRApp configure];
#endif
    
//    [Fabric.sharedSDK setDebug:YES];
    // [END configure_firebase]
    
    // Add observer for InstanceID token refresh callback.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tokenRefreshNotification:)
                                                 name:kFIRInstanceIDTokenRefreshNotification object:nil];
    
    //------------------------------------------------------------------------------------------------------------------------------
    
    [GMSServices provideAPIKey:_GOOGLE_API_KEY];
    //------------------------------------------------------------------------------------------------------------------------------
    TXSettings  *settings  = [[TXApp instance] getSettings];
    TXBaseViewController    *firstVC   = nil;
    
    //[settings setUserTelno:@"+16573345371"];
    //NSLog(@"getDeviceUID:%@",[[TXApp instance] getDeviceUID]);
    //
    self.isDriverMode = NO;
    self.isCanDriverMode = NO;
    self.isBecomeMode = NO;
#ifdef _DRIVER_MODE
        self.isDriverMode = YES;
#endif
    //------------------------------------------------------------------------------------------------------------------------------
#if TARGET_IPHONE_SIMULATOR
    //[[[TXApp instance] getSettings] setNotificationsToken:@"AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"];
#endif

    NSString *userToken = [settings getUserToken];
    NSString *userUid   = [settings getUserId];
    NSString *userTelno = [settings getUserTelno];
    NSString *userPasswd = [settings getPassword];

    NSString *t = [[[TXApp instance] getSettings] getNotificationsToken];
    NSLog(@"getNotificationsToken:%@",t);
    // setDeviceID
#if TARGET_IPHONE_SIMULATOR
    [settings setDeviceID:@"AAAAAAAA-AAAA-AAAA-AAAA-AAAAAAAAAAAA"];
    _userPushToken = @"ee5026f87008a894fbb9264b9d55400e2bc3bd5103195ee85d62e6f64ab1f8c1";
    [self onTokenToServer];
#else
    [settings setDeviceID:@""];
#endif
    
//for rider
    //[[NSUserDefaults standardUserDefaults] setBool:NO forKey:VIEWTUTORIAL];
    if (![[NSUserDefaults standardUserDefaults] boolForKey:VIEWTUTORIAL]) {
        //[[TXCallModel instance] onCleanSession];
        [settings setUserId:@""];
        userUid = @"";
    }
    
    // 로그인 회원가입 표시 순서
#ifdef _WITZM
    [[[TXApp instance] getSettings] setFDKeychain:IS_SIGN_UP value:@"0"];
#elif defined _CHACHA
    [Utils setFirstLogin];
#endif
    
    NSLog(@">>%@,%@,%@,%@",userUid,userTelno,userToken,userPasswd);
    
#ifdef _WITZM
    if (userUid == nil || [userUid isEqualToString:@""]) {
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:VIEWTUTORIAL];
    }
#elif defined _CHACHA
    if (userUid == nil || [userUid isEqualToString:@""]) {
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:VIEWTUTORIAL];
    }
#endif
    
    {
        if((![userToken isEqual:[NSNull null]] && [userToken length] > 0) &&
           (![userUid isEqual:[NSNull null]] && [userUid length] > 0) ) {
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
            //firstVC = [[TXLauncherVC alloc] initWithFlag:FALSE];
            //firstVC = [[TXLauncherVC alloc] initWithNibName:@"TXLauncherDriverVC" bundle:nil flag:NO];
            firstVC = [[TXLauncherVC alloc] initWithNibName:@"TXLauncherVC" bundle:nil flag:NO];
        } else {
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
            // 첫화면 로딩
            //firstVC = [[TXLauncherVC alloc] initWithNibName:@"TXLauncherDriverVC" bundle:nil];
            firstVC = [[TXLauncherVC alloc] initWithNibName:@"TXLauncherVC" bundle:nil];
        }
    }
    
    //------------------------------------------------------------------------------------------------------------------------------
    // Create content and menu controllers
    //
    MenuNavigationController *navigationController = [[MenuNavigationController alloc] initWithRootViewController:firstVC];
    MenuViewController *menuController = [[MenuViewController alloc] initWithNibName:@"MenuViewController" bundle:nil];
    
    // Create frosted view controller
    //
    self.frostedViewController = [[REFrostedViewController alloc] initWithContentViewController:navigationController menuViewController:menuController];
    //[self.frostedViewController resizeMenuViewControllerToSize:CGSizeMake(self.window.frame.size.width/2, self.window.frame.size.height)];
    self.frostedViewController.direction = REFrostedViewControllerDirectionRight;
    
#ifdef _DRIVER_MODE
    
#ifdef _WITZM
    self.frostedViewController.liveBlurBackgroundStyle = REFrostedViewControllerLiveBackgroundStyleLight;
    self.frostedViewController.liveBlur = YES;
    self.frostedViewController.delegate = self;
#elif defined _CHACHA
    self.frostedViewController.liveBlurBackgroundStyle = REFrostedViewControllerLiveBackgroundStyleLight;
    self.frostedViewController.liveBlur = YES;
    self.frostedViewController.delegate = self;
#endif
    
#else
    
#ifdef _WITZM
    self.frostedViewController.direction = REFrostedViewControllerDirectionLeft;
    self.frostedViewController.panGestureEnabled = NO;
#elif defined _CHACHA
    self.frostedViewController.direction = REFrostedViewControllerDirectionLeft;
    self.frostedViewController.panGestureEnabled = NO;
#endif
    
#endif

    
    self.frostedViewController.view.tag = 7;
    
#ifdef _DRIVER_MODE
    
#ifdef _WITZM
    if ([[UIDevice currentDevice].model isEqualToString:@"iPad"]) {
        [self menuViewSize:YES];
    }
    else {
        [self menuViewSize:YES];
        // 1/3
        //self.frostedViewController.menuViewSize = CGSizeMake((self.window.frame.size.width/3)*2, self.frostedViewController.view.frame.size.height);
    }
#elif defined _CHACHA
    if ([[UIDevice currentDevice].model isEqualToString:@"iPad"]) {
        [self menuViewSize:YES];
    }
    else {
        [self menuViewSize:YES];
    }
#endif
    
#else
    
#ifdef _WITZM
    if ([[UIDevice currentDevice].model isEqualToString:@"iPad"]) {
    }
    else {
        [self menuViewSize:NO];
    }
#elif defined _CHACHA
    if ([[UIDevice currentDevice].model isEqualToString:@"iPad"]) {
    }
    else {
        [self menuViewSize:NO];
    }
#endif
    
#endif

    //NSLog(@"frostedViewController:%@",NSStringFromCGSize(self.frostedViewController.menuViewSize));
    
    //NSLog(@"mainframe:%@",NSStringFromCGRect(self.frostedViewController.view.frame));
    
    self.window.rootViewController = self.frostedViewController;
    //self.window.backgroundColor = [UIColor whiteColor];
    
//    if ([launchOptions objectForKey:UIApplicationLaunchOptionsLocationKey]) {
//        [self startMonitoringSignificantLocationChanges];
//    }
//    [self startSingleLocationRequest];
    
//    FIRCrashLog(@"Cause Crash button clicked");
//    assert(NO);
    
    // spotify
#ifdef _DRIVER_MODE
        // Set up shared authentication information
        SPTAuth *auth = [SPTAuth defaultInstance];
        auth.clientID = @kClientId;
        auth.requestedScopes = @[SPTAuthStreamingScope];
        auth.redirectURL = [NSURL URLWithString:@kCallbackURL];
#ifdef kTokenSwapServiceURL
        auth.tokenSwapURL = [NSURL URLWithString:@kTokenSwapServiceURL];
#endif
#ifdef kTokenRefreshServiceURL
        auth.tokenRefreshURL = [NSURL URLWithString:@kTokenRefreshServiceURL];
#endif
        auth.sessionUserDefaultsKey = @kSessionUserDefaultsKey;
#endif
    
    return YES;
}

-(void)menuViewSize:(BOOL) isFull{
    
    if (isFull) {
        self.frostedViewController.menuViewSize = CGSizeMake(self.frostedViewController.view.frame.size.width, self.frostedViewController.view.frame.size.height);
    }
    else {
        self.frostedViewController.menuViewSize = CGSizeMake(self.frostedViewController.view.frame.size.width*2/3, self.frostedViewController.view.frame.size.height);
    }
    
}

//Called when a notification is delivered to a foreground app.
-(void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler{
    NSLog(@"User Info : %@",notification.request.content.userInfo);
    
    
    __block NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    [notificationCenter postNotificationName:@"NotificationPushReceived" object:nil userInfo:notification.request.content.userInfo];
    
    if([UIApplication sharedApplication].applicationState == UIApplicationStateInactive) {
        
        //[self playSound:SOUND_NOTI vibrate:YES];
        [self loopSound:SOUND_NOTI repeat:NO];
        NSLog(@"Inactive");
        
        //Show the view with the content of the push
        
        //completionHandler(UIBackgroundFetchResultNewData);
        
    } else if ([UIApplication sharedApplication].applicationState == UIApplicationStateBackground) {

        //[self playSound:SOUND_NOTI vibrate:YES];
        [self loopSound:SOUND_NOTI repeat:NO];
        NSLog(@"Background");
        
        //Refresh the local model
        
        //completionHandler(UIBackgroundFetchResultNewData);
        
    } else {
        
        //[self playSound:SOUND_NOTI vibrate:YES];
        [self loopSound:SOUND_NOTI repeat:NO];
        NSLog(@"Active");
        
        //Show an in-app banner
        
        //completionHandler(UIBackgroundFetchResultNewData);
        
    }
    
    completionHandler(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge);
}

//Called to let your app know which action was selected by the user for a given notification.
-(void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void(^)())completionHandler{
    NSLog(@"User Info : %@",response.notification.request.content.userInfo);
    completionHandler();
}

- (void)resetRootVC:(UIViewController*)firstVC
{
    [self.frostedViewController.contentViewController removeFromParentViewController];
    MenuNavigationController *navigationController = [[MenuNavigationController alloc] initWithRootViewController:firstVC];
    
    self.frostedViewController.contentViewController = navigationController;
}

- (void)removeAllVCFromNavigation {
    NSArray* tempVCA = [[self.frostedViewController.contentViewController navigationController] viewControllers];
    
    for(UIViewController *tempVC in tempVCA)
    {
        [tempVC removeFromParentViewController];
    }
}

- (void)menuViewDelegate:(id)sender {
    MenuViewController *m = (MenuViewController*)self.frostedViewController.menuViewController;
    m.delegate = sender;
}

#pragma mark - Parse installation
/**
 Notification Registration
 */
- (void)registerForRemoteNotification {

    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_9_x_Max) {
        UIUserNotificationType allNotificationTypes =
        (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
        UIUserNotificationSettings *settings =
        [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
    } else {
        // iOS 10 or later
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
        // For iOS 10 display notification (sent via APNS)
        [UNUserNotificationCenter currentNotificationCenter].delegate = self;
        UNAuthorizationOptions authOptions =
        UNAuthorizationOptionAlert
        | UNAuthorizationOptionSound
        | UNAuthorizationOptionBadge;
        [[UNUserNotificationCenter currentNotificationCenter] requestAuthorizationWithOptions:authOptions completionHandler:^(BOOL granted, NSError * _Nullable error) {
        }];
#endif
    }
    
    [[UIApplication sharedApplication] registerForRemoteNotifications];


    if(SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(@"10.0")) {
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        center.delegate = self;
        [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error){
            if( !error ){
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[UIApplication sharedApplication] registerForRemoteNotifications];
                });
            }
        }];
    }
    else {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }

}

- (void)application:(UIApplication *)application
didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {

    NSLog(@"Device Token : [%@] %lu", [deviceToken stringRepresentation], (unsigned long)[[deviceToken stringRepresentation] length]);
    _userPushToken = [deviceToken stringRepresentation];
//    [[[TXApp instance] getSettings] setNotificationsToken:[deviceToken stringRepresentation]];
    
    //[[FIRInstanceID instanceID] setAPNSToken:deviceToken type:FIRInstanceIDAPNSTokenTypeUnknown];
//#if TARGET_IPHONE_SIMULATOR
//    //Tricky line
//    [[FIRInstanceID instanceID] setAPNSToken:deviceToken type:FIRInstanceIDAPNSTokenTypeSandbox];
//#else
//    [[FIRInstanceID instanceID] setAPNSToken:deviceToken type:FIRInstanceIDAPNSTokenTypeProd];
//#endif
    // gcm
    [self onTokenToServer];
}



- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier
forRemoteNotification:(NSDictionary *)notification completionHandler:(void(^)())completionHandler
{
    NSLog(@"Received push notification: %@, identifier: %@", notification, identifier); // iOS 8
    completionHandler();
}

- (void)application:(UIApplication *)application
didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@"Registration for remote notification failed with error: %@", error.localizedDescription);
    
}


//// [START ack_message_reception] < iOS8.0
//- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
//    NSLog(@"Notification received: %@", userInfo);
//}


// > iOS8.0
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    NSLog(@"Notification received fetch: %@", userInfo);
    
    // Print message ID.
    //NSLog(@"Message ID: %@", userInfo[@"gcm.message_id"]);
    
    // Pring full message.
    //NSLog(@"%@", userInfo);
    
    /*
    __block NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    [notificationCenter postNotificationName:@"NotificationPushReceived" object:nil userInfo:userInfo];
    
    if(application.applicationState == UIApplicationStateInactive) {
        
        //[self playSound:SOUND_NOTI vibrate:YES];
        [self loopSound:SOUND_NOTI repeat:NO];
        NSLog(@"Inactive");
        
        //Show the view with the content of the push
        
        completionHandler(UIBackgroundFetchResultNewData);
        
    } else if (application.applicationState == UIApplicationStateBackground) {
        
//        UILocalNotification* local = [[UILocalNotification alloc]init];
//        if (local)
//        {
//            local.fireDate = [NSDate dateWithTimeIntervalSinceNow:10];
//            local.alertBody = @"Hey this is background local notification!!!";
//            local.timeZone = [NSTimeZone defaultTimeZone];
//            [[UIApplication sharedApplication] scheduleLocalNotification:local];
//            
//        }
        
        //[self playSound:SOUND_NOTI vibrate:YES];
        [self loopSound:SOUND_NOTI repeat:NO];
        NSLog(@"Background");
        
        //Refresh the local model
        
        completionHandler(UIBackgroundFetchResultNewData);
        
    } else {
        
        //[self playSound:SOUND_NOTI vibrate:YES];
        [self loopSound:SOUND_NOTI repeat:NO];
        NSLog(@"Active");
        
        //Show an in-app banner
        
        completionHandler(UIBackgroundFetchResultNewData);
        
    }
     */
}
// [END ack_message_reception]


- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
    UIApplicationState state = [application applicationState];
    if (state == UIApplicationStateActive)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Reminder" message:notification.alertBody
                                                       delegate:self cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
    
}

- (void)didDeleteMessagesOnServer {
    // Some messages sent to this device were deleted on the GCM server before reception, likely
    // because the TTL expired. The client should notify the app server of this, so that the app
    // server can resend those messages.
}

#ifdef __IPHONE_8_0
- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {
    
    if (notificationSettings.types == UIUserNotificationTypeNone) {
        NSLog(@"Registering device for push notifications... User Don't Allow"); // iOS 8
//        [application unregisterForRemoteNotifications];
    }
    else {
//        NSLog(@"Registering device for push notifications...:%@",notificationSettings); // iOS 8
        [application registerForRemoteNotifications];
        
    }
}
#endif

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    
    //[[EDQueue sharedInstance] stop];
}

-(void)application:(UIApplication *)application willChangeStatusBarFrame:(CGRect)newStatusBarFrame{
    //NSLog(@">>>>>>>>>>>> %f %f", newStatusBarFrame.size.width, newStatusBarFrame.size.height);
}

-(void)application:(UIApplication *)application didChangeStatusBarFrame:(CGRect)oldStatusBarFrame{
    //NSLog(@">>>>>>>>>>>> %f %f",oldStatusBarFrame.size.width, oldStatusBarFrame.size.height);
}

#pragma mark - Sound
- (void)playSound:(NSString *)filename vibrate:(BOOL)vibrate
{
    //NSString *path = [NSString stringWithFormat:@"%@%@", [[NSBundle mainBundle] resourcePath], @"/genericsuccess.wav"];
    NSString *path = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:filename];
    
    SystemSoundID soundID;
    NSURL *filePath = [NSURL fileURLWithPath:path isDirectory:NO];
    AudioServicesCreateSystemSoundID((CFURLRef)CFBridgingRetain(filePath), &soundID);
    
    AudioServicesPlaySystemSound(soundID);
    
    if (vibrate) {
        //also vibrate
        AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
    }
}

- (void)loopSound:(NSString *)filename
{
#if TARGET_IPHONE_SIMULATOR
#else
    NSString *filePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:filename];
    
    {
        //NSURL *fileURL = [NSURL URLWithString:filePath];
        NSURL *fileURL = [NSURL fileURLWithPath:filePath isDirectory:NO];
        
        NSError* err;
        
        //Initialize our player pointing to the path to our resource
        self.loopPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error:&err];
        
        if( err ){
            //bail!
            NSLog(@"Failed with reason: %@", [err localizedDescription]);
        }
        else{
            //set our delegate and begin playback
            self.loopPlayer.delegate = self;
            [self.loopPlayer prepareToPlay];
            [self.loopPlayer play];
            self.loopPlayer.numberOfLoops = -1; // repeat
            //self.player.currentTime = 0;
            self.loopPlayer.volume = 1.0;
            
            iShouldKeepBuzzing = YES;
            AudioServicesAddSystemSoundCompletion (
                                                   kSystemSoundID_Vibrate,
                                                   NULL,
                                                   NULL,
                                                   MyAudioServicesSystemSoundCompletionProc,
                                                   NULL
                                                   );
            AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
        }
    }
#endif
}

- (void)loopSound:(NSString *)filename repeat:(BOOL)repeat
{
    NSString *filePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:filename];
    
    {
        //NSURL *fileURL = [NSURL URLWithString:filePath];
        NSURL *fileURL = [NSURL fileURLWithPath:filePath isDirectory:NO];
        
        NSError* err;
        
        //Initialize our player pointing to the path to our resource
        AVAudioPlayer *aplayer = [[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error:&err];
        
        if( err ){
            //bail!
            NSLog(@"Failed with reason: %@", [err localizedDescription]);
        }
        else{
            //set our delegate and begin playback
            aplayer.delegate = self;
            [aplayer prepareToPlay];
            [aplayer play];
            if (repeat) {
                aplayer.numberOfLoops = repeat;
            }
            else {
                aplayer.numberOfLoops = -1; // repeat
            }
            //self.player.currentTime = 0;
            aplayer.volume = 1.0;
            
            AudioServicesAddSystemSoundCompletion (
                                                   kSystemSoundID_Vibrate,
                                                   NULL,
                                                   NULL,
                                                   MyAudioServicesSystemSoundCompletionProc,
                                                   NULL
                                                   );
            AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
        }
    }
}


- (void)playSound2:(NSString *)filename vibrate:(BOOL)vibrate
{
    NSString *filePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:filename];
    
    if ( [[NSFileManager defaultManager] fileExistsAtPath:filePath] ) {
        NSURL *fileURL = [NSURL URLWithString:filePath];
        
        __block SystemSoundID audioEffect;
        AudioServicesCreateSystemSoundID((__bridge CFURLRef) fileURL, &audioEffect);
        AudioServicesPlayAlertSound(audioEffect);
        if (vibrate) {
            AudioServicesPlayAlertSound(kSystemSoundID_Vibrate);
        }
        
        dispatch_time_t dispatch_disposeSound = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(10.0 * NSEC_PER_SEC));
        dispatch_after(dispatch_disposeSound, dispatch_get_main_queue(), ^{
            AudioServicesDisposeSystemSoundID(audioEffect);
            NSLog(@"Disposed sound.");
        });
    } else {
        NSLog(@"Notification sound not found.");
    }
}

- (void)loopSound2:(NSString *)filename
{
    NSString *filePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:filename];
    
    if ( [[NSFileManager defaultManager] fileExistsAtPath:filePath] ) {
        NSURL *fileURL = [NSURL URLWithString:filePath];
        
        NSError* err;
        
        //Initialize our player pointing to the path to our resource
        self.loopPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:fileURL error:&err];
        
        if( err ){
            //bail!
            NSLog(@"Failed with reason: %@", [err localizedDescription]);
        }
        else{
            //set our delegate and begin playback
            self.loopPlayer.delegate = self;
            [self.loopPlayer play];
            self.loopPlayer.numberOfLoops = -1; // repeat
            self.loopPlayer.currentTime = 0;
            self.loopPlayer.volume = 1.0;
            
            iShouldKeepBuzzing = YES;
            AudioServicesAddSystemSoundCompletion (
                                                   kSystemSoundID_Vibrate,
                                                   NULL,
                                                   NULL,
                                                   MyAudioServicesSystemSoundCompletionProc,
                                                   NULL
                                                   );
            AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
        }
    } else {
        NSLog(@"Notification sound not found.");
    }
}

- (void)stopSound
{
    iShouldKeepBuzzing = NO;
    [self.loopPlayer stop];
}

#pragma mark AudioService callback function prototypes
void MyAudioServicesSystemSoundCompletionProc (
                                               SystemSoundID  ssID,
                                               void           *clientData
                                               );

#pragma mark AudioService callback function implementation

// Callback that gets called after we finish buzzing, so we
// can buzz a second time.
void MyAudioServicesSystemSoundCompletionProc (
                                               SystemSoundID  ssID,
                                               void           *clientData
                                               ) {
    if (iShouldKeepBuzzing) { // Your logic here...
        AudioServicesPlaySystemSound(kSystemSoundID_Vibrate); 
    } else {
        //Unregister, so we don't get called again...
        AudioServicesRemoveSystemSoundCompletion(kSystemSoundID_Vibrate);
    }  
}

//- (void)application:(UIApplication *)application performFetchWithCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
//    [SLNScheduler startWithCompletion:completionHandler];
//}

//-(void)application:(UIApplication *)application performFetchWithCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
//{
////    NSLog(@"########### Received Background Fetch ###########");
////    //Download the Content .
////    NSURLSession *urlsession = [[NSURLSession alloc] init];
////    [urlsession dataTaskWithHTTPGetRequest:[NSURL URLWithString:@"download-new.utorrent.com/endpoint/utmac/os/osx-ppc/track/stable/"]];
////    
////    counterTask = [application
////                   beginBackgroundTaskWithExpirationHandler:^{
////                       [application endBackgroundTask:counterTask];
////                       //counterTask = UIBackgroundTaskInvalid;
////                       
////                       // If you're worried about exceeding 10 minutes, handle it here
////                       // theTimer=[NSTimer scheduledTimerWithTimeInterval:1
////                       // target:self
////                       // selector:@selector(countUp)
////                       // userInfo:nil
////                       // repeats:YES];
////                   }];
////    
////    // download-new.utorrent.com/endpoint/utmac/os/osx-ppc/track/stable/
////    //Cleanup
////    completionHandler(UIBackgroundFetchResultNewData);
////    
////    
////    NSDate *fetchStart = [NSDate date];
////    
////    ViewController *viewController = (ViewController *)self.window.rootViewController;
////    
////    [viewController fetchNewDataWithCompletionHandler:
////     ^(UIBackgroundFetchResult result)
////     {
////         completionHandler(result);
////         
////         NSDate *fetchEnd = [NSDate date];
////         
////         NSTimeInterval timeElapsed = [fetchEnd timeIntervalSinceDate:fetchStart];
////         NSLog(@"Background Fetch Duration: %f seconds", timeElapsed);
////     }];
////
//    //NSDate *fetchStart = [NSDate date];
//    //NSLog(@"Background Fetch Duration: %f seconds", fetchStart);
//    NSLog(@"Background Fetch");
//    
//    // noti
//    __block NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
//    [notificationCenter postNotificationName:@"NotificationNetworkReachableReceived" object:nil userInfo:nil];
//    //[self startBackgroundTask];
//    completionHandler(UIBackgroundFetchResultNewData);
//}

#pragma mark - runLoop

- (void)startBackgroundTask {
    
    if (longRunTask != UIBackgroundTaskInvalid) {
        NSLog(@"Already started bg task");
        return;
    }
    
    NSLog(@"start bg task");
    longRunTask = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{
        [[UIApplication sharedApplication] endBackgroundTask:longRunTask];
    }];
    
    count=0;
    theTimer=[NSTimer scheduledTimerWithTimeInterval:5.0f
                                              target:self
                                            selector:@selector(increment)
                                            userInfo:nil
                                             repeats:YES];
}

- (void)increment {
    // 20초 후에 처리한다
    if (count==20) {
        //[self removeNotification:@"BG"];
        //[self endBackgroundTask];
        NSLog(@"BackgroundTask Fetch");
        
        // noti
        __block NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
        [notificationCenter postNotificationName:@"NotificationNetworkReachableReceived" object:nil userInfo:nil];
        count=0;
    } else {
        
        //		NSLog(@"longRunTask : %d",count);
        count++;
    }
}

- (void)endBackgroundTask {
    if (longRunTask != UIBackgroundTaskInvalid) {
        [[UIApplication sharedApplication] endBackgroundTask:longRunTask];
        longRunTask = UIBackgroundTaskInvalid;
        
        [theTimer invalidate];
        count=0;
    }
}

#pragma mark - EDQueue
//- (EDQueueResult)queue:(EDQueue *)queue processJob:(NSDictionary *)job
//{
//    sleep(1);           // This won't block the main thread. Yay!
//    
//    // Wrap your job processing in a try-catch. Always use protection!
//    @try {
//        if ([[job objectForKey:@"task"] isEqualToString:@"success"]) {
//            return EDQueueResultSuccess;
//        } else if ([[job objectForKey:@"task"] isEqualToString:@"fail"]) {
//            return EDQueueResultFail;
//        }
//    }
//    @catch (NSException *exception) {
//        return EDQueueResultCritical;
//    }
//    
//    return EDQueueResultCritical;
//}

//- (void)queue:(EDQueue *)queue processJob:(NSDictionary *)job completion:(void (^)(EDQueueResult))block
//{
//    sleep(1);
//    
//    @try {
//        if ([[job objectForKey:@"task"] isEqualToString:@"success"]) {
//            block(EDQueueResultSuccess);
//        } else if ([[job objectForKey:@"task"] isEqualToString:@"fail"]) {
//            block(EDQueueResultFail);
//        } else {
//            block(EDQueueResultCritical);
//        }
//    }
//    @catch (NSException *exception) {
//        block(EDQueueResultCritical);
//    }
//}

#pragma mark - FCM
// [START refresh_token]
- (void)tokenRefreshNotification:(NSNotification *)notification {
    // Note that this callback will be fired everytime a new token is generated, including the first
    // time. So if you need to retrieve the token as soon as it is available this is where that
    // should be done.
    NSString *refreshedToken = [[FIRInstanceID instanceID] token];
    NSLog(@"InstanceID token: %@", refreshedToken);
    
    // Connect to FCM since connection may have failed when attempted before having a token.
    //[self connectToFcm];
    
    // TODO: If necessary send token to application server.
}
// [END refresh_token]

// [START connect_to_fcm]
- (void)connectToFcm {
    
    [[FIRMessaging messaging] connectWithCompletion:^(NSError * _Nullable error) {
        if (error != nil) {
            // 이미 연결되어 있다
            if (error.code != 2001) {
                NSLog(@"Unable to connect to FCM. %@", error);
                if (errorCount == 0) {
                    //[Utils onErrorAlertView:LocalizedStr(@"Intro.FCM.Error.text")];
                }
            }
            errorCount++;
        } else {
            NSLog(@"Connected to FCM.");

            // [START get_iid_token]
            NSString *token = [[FIRInstanceID instanceID] token];
            NSLog(@"InstanceID token: %@", token);
            // [END get_iid_token]

//            NSString *t = [[[TXApp instance] getSettings] getNotificationsToken];
//            NSData* deviceToken = [t dataUsingEncoding:NSUTF8StringEncoding];
//            #if TARGET_IPHONE_SIMULATOR
//                //Tricky line
//                [[FIRInstanceID instanceID] setAPNSToken:deviceToken type:FIRInstanceIDAPNSTokenTypeSandbox];
//            #else
//                [[FIRInstanceID instanceID] setAPNSToken:deviceToken type:FIRInstanceIDAPNSTokenTypeProd];
//            #endif
//            
            [self onTokenToServer];
            
        }
    }];
}
// [END connect_to_fcm]

- (void)applicationDidBecomeActive:(UIApplication *)application {
#ifdef _WITZM
    [FBSDKAppEvents activateApp];
#elif defined _CHACHA
    [FBSDKAppEvents activateApp];
#endif

    errorCount = 0;
    //[self checkNetwork];
    
    if (!self.isNetwork) {
        [self performSelector:@selector(checkRetry) withObject:nil afterDelay:2];
    }
    
    //[self connectToFcm];
    
//    [[EDQueue sharedInstance] setDelegate:self];
//    [[EDQueue sharedInstance] start];
    
//    if ([INTULocationManager locationServicesState]) {
//        [self onCommonErrorAlert:LocalizedStr(@"Intro.GPS.Error.text")];
//    }
}

// [START disconnect_from_fcm]
- (void)applicationDidEnterBackground:(UIApplication *)application {
//    [[FIRMessaging messaging] disconnect];
//    NSLog(@"Disconnected from FCM");
    
//    // background fetch on
//    [[UIApplication sharedApplication] setMinimumBackgroundFetchInterval:60];
//    NSLog(@"background timer");
    
    //[self startBackgroundTask];
}
// [END disconnect_from_fcm]


- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    
//    // background fetch off
//    [[UIApplication sharedApplication] setMinimumBackgroundFetchInterval:UIApplicationBackgroundFetchIntervalNever];
    
    //[self endBackgroundTask];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

// Google oauth callback

//- (BOOL)application: (UIApplication *)application openURL: (NSURL *)url sourceApplication: (NSString *)sourceApplication annotation: (id)annotation
//{
//    return [GPPURLHandler handleURL:url sourceApplication:sourceApplication annotation:annotation];
//}

// customeURL
- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url {
    
    if ([@"driveshare" isEqual:[url host]]) {
        NSDictionary *q = [Utils getDataOfQueryString:[url query]];
        self.shareOseq = q[@"oseq"];
        
        if (self.isMapView) {
            //push
            __block NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
            [notificationCenter postNotificationName:@"NotificationCustomURLReceived" object:nil userInfo:nil];
        }
        else {
            
        }
        
        return YES;
    }
    return NO;
    
    
}

//iOS 9.0 이상일 때 구현
- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString *,id> *)options {
    
    NSLog(@" openURL:%@",[url host]);
    NSLog(@" openURL:%@",[url parameterString]);
    NSLog(@" openURL:%@",[url query]);
    NSLog(@" openURL:%@",[url scheme]);

#ifdef _DRIVER_MODE
    NSString *scheme = _DRIVER_APP_URL;
#else
    NSString *scheme = _RIDER_APP_URL;
#endif
    
    NSLog(@"application openURL");
    if ([@"driveshare" isEqual:[url host]]) {
        NSDictionary *q = [Utils getDataOfQueryString:[url query]];
        self.shareOseq = q[@"oseq"];
        self.customShareDic = q;
        
        //if (self.isMapView) {
        //push
        __block NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
        [notificationCenter postNotificationName:@"NotificationCustomURLReceived" object:nil userInfo:q];
        
        return YES;
    }
    else if ([@"resetpassword" isEqual:[url host]]) {
        NSDictionary *q = [Utils getDataOfQueryString:[url query]];
        //self.shareOseq = q[@"datoken"];
        self.customShareDic = q;
        
        //push
        __block NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
        
#if defined (_CHACHA) || defined (_WITZM)
        // login view로 먼저 이동한다.
        if (self.signupViewStep == -1) { // 최초실행이면 로그인 화면으로 이동
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 3 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [notificationCenter postNotificationName:NOTIFICATION_ENTER_LOGINVIEW object:nil userInfo:q];
            });
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 4 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [notificationCenter postNotificationName:@"NotificationCustomURLReceived" object:nil userInfo:q];
            });
        }
        else if (self.signupViewStep == 9) { // lanucher 화면이면 로그인 화면으로 이동
            [notificationCenter postNotificationName:NOTIFICATION_ENTER_LOGINVIEW object:nil userInfo:q];
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [notificationCenter postNotificationName:@"NotificationCustomURLReceived" object:nil userInfo:q];
            });
        }
        else {
            [notificationCenter postNotificationName:@"NotificationCustomURLReceived" object:nil userInfo:q];
        }
#else
        [notificationCenter postNotificationName:@"NotificationCustomURLReceived" object:nil userInfo:q];
#endif
        
        return YES;
    }
    
#ifdef _DRIVER_MODE
    if ([@"spotify" isEqual:[url host]]) {
        SPTAuth *auth = [SPTAuth defaultInstance];
        
        SPTAuthCallback authCallback = ^(NSError *error, SPTSession *session) {
            // This is the callback that'll be triggered when auth is completed (or fails).
            
            if (error) {
                NSLog(@"*** Auth error: %@", error);
            } else {
                auth.session = session;
            }
            [[NSNotificationCenter defaultCenter] postNotificationName:@"sessionUpdated" object:self];
        };
        
        /*
         Handle the callback from the authentication service. -[SPAuth -canHandleURL:]
         helps us filter out URLs that aren't authentication URLs (i.e., URLs you use elsewhere in your application).
         */
        
        if ([auth canHandleURL:url]) {
            [auth handleAuthCallbackWithTriggeredAuthURL:url callback:authCallback];
            return YES;
        }
    }
#endif
    
#if defined (_CHACHA) || defined (_WITZM)
    if ([url.scheme hasPrefix:@"fb"]) {
        return [[FBSDKApplicationDelegate sharedInstance] application:app openURL:url options:options];
    }
    
    if ([url.scheme hasPrefix:scheme] && ([url.host hasPrefix:@"authorize"] || [url.host hasPrefix:@"thirdPartyLoginResult"])) {
        return [[NaverThirdPartyLoginConnection getSharedInstance] application:app openURL:url options:options];
    }
    
    if ([url.scheme hasPrefix:@"kakao"]) {
        if ([KOSession isKakaoAccountLoginCallback:url]) {
            return [KOSession handleOpenURL:url];
        }
    }
    
#endif
    
    return NO;
}


-(void) registerListeners {
    
}

-(void)onTokenToServer {

    
    NSString *t = [[[TXApp instance] getSettings] getNotificationsToken];
    NSLog(@"onTokenToServer:%@,%@",t,_userPushToken);
    
    if ([t isEqualToString:_userPushToken]) {
        return;
    }
    NSLog(@"onTokenToServer:%@",_userPushToken);
    
    TXSettings  *settings  = [[TXApp instance] getSettings];
//    NSString *userToken = [settings getUserToken];
    NSString *userUid   = [settings getUserId];

    // login이 되어 있어야 올릴수 있다.
    if((![_userPushToken isEqual:[NSNull null]] && [_userPushToken length] > 0) &&
       (![userUid isEqual:[NSNull null]] && [userUid length] > 0) ) {
#ifdef _DRIVER_MODE
        [[TXUserModel instance] addEventListener:self forEvent:@"PP112" eventParams:nil];
        [[TXUserModel instance] PP112:_userPushToken];
#else
        [[TXUserModel instance] addEventListener:self forEvent:@"UR112" eventParams:nil];
        [[TXUserModel instance] UR112:_userPushToken];
#endif
        NSLog(@"onTokenToServer if:%@",_userPushToken);
    }
    else {
        NSLog(@"onTokenToServer else:%@",_userPushToken);
    }
}


-(void)onEvent:(TXEvent *)event eventParams:(id)subscriptionParams {
    
    TXResponseDescriptor *descriptor = [event getEventProperty:TXEvents.Params.DESCRIPTOR];
    //TXRequestObj *request = [event getEventProperty:TXEvents.Params.REQUEST];
    
    if([event.name isEqualToString:@"PP112"] || [event.name isEqualToString:@"UR112"]) {
        if(descriptor.success) {
            [[[TXApp instance] getSettings] setNotificationsToken:_userPushToken];
        }
    }
}

-(void) onTokenValidateSucceeded:(TXEvent *) event {
    
}

-(void) onTokenValidateFailed:(TXEvent *) event {
    
}


#pragma mark Common Method

-(void) updateActivityState:(id)target  {
    
    return;
    
//    if (!self.isDriverMode) {
//        if (_ustate != _ustateold || _ustate2 != _ustate2old) {
//            _ustateold = _ustate;
//            _ustate2old = _ustate2;
//            
//            if ([target respondsToSelector:@selector(onChangeUI)]) {
//                [target onChangeUI];
//            }
//        }
//    }
//    else {
//        if (_pstate != _pstateold || _pstate2 != _pstate2old) {
//            _pstateold = _pstate;
//            _pstate2old = _pstate2;
//            
//            if (_pstate == 210 && _pstate2 != 0) {
//                _pstate2 = 0;
//            }
//            
//            if ([target respondsToSelector:@selector(onChangeUI)]) {
//                [target onChangeUI];
//            }
//        }
//    }
}

-(void)debugActivityState {
    
    @try {
        //    NSString* useq2 = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.USEQD];
        //    NSString* pseq2 = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.PSEQD];
        //
        //    NSString* useq1 = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.USEQ];
        //    NSString* pseq1 = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.PSEQ];
        //    NSString* oseq1 = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.OSEQ];
        //    NSString* vseq1 = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.VSEQ];
        
        NSString *useq = [[self.dicRider objectForKey:@"user_device"] objectForKey:@"useq"];
        NSString *pseq = [[self.dicDriver objectForKey:@"provider_device"] objectForKey:@"pseq"];
        //    NSString *oseq = [[self.dicDriver objectForKey:@"provider_device"] objectForKey:@"oseq"];
        //    NSString *vseq = [[self.dicDriver objectForKey:@"provider_device"] objectForKey:@"vseq"];
        
        //    NSLog(@"appDelegate.isDriverMode:%d(%d)(%d)", self.isDriverMode,self.isCanDriverMode,self.isBecomeMode);
        if (self.isDriverMode) {
            NSLog(@"appDelegate.isDriverMode:%d(%d)(%d):%d,%d(%d,%d) : p:%@", self.isDriverMode,self.isCanDriverMode,self.isBecomeMode, self.pstate, self.pstate2, self.pstateold, self.pstate2old, pseq);
        }
        else {
            NSLog(@"appDelegate.isDriverMode:%d(%d)(%d):%d,%d(%d,%d) : u:%@", self.isDriverMode,self.isCanDriverMode,self.isBecomeMode, self.ustate, self.ustate2, self.ustateold, self.ustate2old, useq);
        }
        
        //    NSLog(@"appDelegate.ustate:%d,%d(%d,%d)", self.ustate, self.ustate2, self.ustateold, self.ustate2old);
        //    NSLog(@"appDelegate.pstate:%d,%d(%d,%d)", self.pstate, self.pstate2, self.pstateold, self.pstate2old);
        //    NSLog(@"useq:%@,pseq:%@,vseq:%@,oseq:%@|useq:%@,pseq:%@", useq1, pseq1, vseq1, oseq1, useq2, pseq2);
        //    NSLog(@"appDelegate.useq:%@,pseq:%@,vseq:%@,oseq:%@", useq, pseq, vseq, oseq);
    }
    @catch (NSException *exception) {
        NSLog(@"Exception: %@, %@", exception.name, exception.reason);
    }


}

-(void)initProtocolMessage {
    self.dicDrive = nil;
    self.dicDriver = nil;
    self.dicRider = nil;
    self.pictureSizeRider = nil;
    self.pictureSizeDriver = nil;
    
    self.pstate = 0;
    self.pstate2 = 0;
    self.pstateold = 0;
    self.pstate2old = 0;
    
    self.ustate = 0;
    self.ustate2 = 0;
    self.ustateold = 0;
    self.ustate2old = 0;
}

-(void)resetState {
    self.pstate = 0;
    self.pstate2 = 0;
    self.pstateold = 0;
    self.pstate2old = 0;
    //                self.isCanDriverMode = NO;
    self.isDriverMode = NO;
    self.dicDriver = nil;
#ifdef _DRIVER_MODE
        self.isDriverMode = YES;
#endif
    
    self.ustate = 0;
    self.ustate2 = 0;
    self.ustateold = 0;
    self.ustate2old = 0;
    //                self.isCanDriverMode = NO;
    self.isDriverMode = NO;
    self.dicRider = nil;
#ifdef _DRIVER_MODE
        self.isDriverMode = YES;
#endif
    
    self.dicDrive = nil;
    self.uistate = 0;
    self.uistate2 = 0;
}

-(void)resetState2 {
    self.pstate2 = 0;
    self.pstate2old = 0;
    
    self.ustate2 = 0;
    self.ustate2old = 0;
    
    [self resetTripInfo];
}

-(void)resetTripInfo
{
    if (self.isDriverMode) {
        //    self.dicDriver = nil;
        //    self.dicRider = nil;
        //    self.dicDrive = nil;
        self.pictureSizeRider = nil;
        //self.pictureSizeDriver = nil;
        
        self.imgProfileRider = nil;
        //self.imgProfileDriver = nil;
        
        [[[TXApp instance] getSettings] setFDKeychain:SettingsConst.CryptoKeys.USEQ value:@""];
        [[[TXApp instance] getSettings] setFDKeychain:SettingsConst.CryptoKeys.OSEQ value:@""];
    }
    else {
        //self.pictureSizeRider = nil;
        self.pictureSizeDriver = nil;
        
        //self.imgProfileRider = nil;
        self.imgProfileDriver = nil;
        self.imgVehicleDriver = nil;
        
        [[[TXApp instance] getSettings] setFDKeychain:SettingsConst.CryptoKeys.PSEQ value:@""];
        [[[TXApp instance] getSettings] setFDKeychain:SettingsConst.CryptoKeys.VSEQ value:@""];
        [[[TXApp instance] getSettings] setFDKeychain:SettingsConst.CryptoKeys.OSEQ value:@""];
    }
}

-(void)updateDic:(TXEvent*)event {
    TXResponseDescriptor * descriptor = [event getEventProperty:TXEvents.Params.DESCRIPTOR];
    if(descriptor.success == true) {
        
        TXRequestObj *request = [event getEventProperty:TXEvents.Params.REQUEST];
        NSString     *response = [[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding];
        NSDictionary *result   = getJSONObj(response);
        [self.keys removeAllObjects];
        
        NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
        if (self.isDriverMode) {
            if (self.dicDriver != nil) {
                dic = (NSMutableDictionary*)[self.dicDriver mutableCopy];
            }
            // 로그인이면 초기화
            if([event.name isEqualToString:@"PP110"]) {
                [dic removeAllObjects];
            }
            
            [self.keys addObject:@"provider"];
            [self.keys addObject:@"provider_dinfos"];
            [self.keys addObject:@"provider_config"];
            
            [self.keys addObject:@"provider_matchs"];
            [self.keys addObject:@"provider_places"];
            [self.keys addObject:@"user_payments"];
            [self.keys addObject:@"provider_codes"];
            [self.keys addObject:@"news_providers"];
            [self.keys addObject:@"user_device"];
            [self.keys addObject:@"provider_device"];
            [self.keys addObject:@"provider_vehicle"];
            [self.keys addObject:@"provider_vehicles"];
            
            [self.keys addObject:@"drive_order"];
            [self.keys addObject:@"drive_pickup"];
            [self.keys addObject:@"drive_pickup_step"];
            [self.keys addObject:@"drive_trip"];
            [self.keys addObject:@"drive_trip_step"];
            [self.keys addObject:@"drive_charge"];
        }
        else {
            if (self.dicRider != nil) {
                dic = (NSMutableDictionary*)[self.dicRider mutableCopy];
            }
            
            // 로그인이면 초기화
            if([event.name isEqualToString:@"UR110"]) {
                [dic removeAllObjects];
            }
            
            [self.keys addObject:@"user"];
            [self.keys addObject:@"user_dinfos"];
            [self.keys addObject:@"user_config"];
            
            [self.keys addObject:@"provider"];
            
            [self.keys addObject:@"user_matchs"];
            [self.keys addObject:@"user_places"];
            [self.keys addObject:@"user_payments"];
            [self.keys addObject:@"user_codes"];
            [self.keys addObject:@"news_users"];
            [self.keys addObject:@"user_device"];
            [self.keys addObject:@"drive_estimates"];
            [self.keys addObject:@"provider_device"];
            [self.keys addObject:@"provider_devices"];
            [self.keys addObject:@"drive_order"];
            [self.keys addObject:@"drive_pickup"];
            [self.keys addObject:@"drive_pickup_step"];
            [self.keys addObject:@"drive_trip"];
            [self.keys addObject:@"drive_trip_step"];
            [self.keys addObject:@"drive_charge"];
        }
        
/*
        if([event.name isEqualToString:@"UR100"]) {
            [self.keys addObject:@"user"];
            [self.keys addObject:@"user_dinfos"];
        }
        else if([event.name isEqualToString:@"UR110"]) {
            [self.keys addObject:@"provider_matchs"];
        }
        else if([event.name isEqualToString:@"UR120"]) {
            [self.keys addObject:@"provider_matchs"];
        }
        else if([event.name isEqualToString:@"UR206"] ||
                [event.name isEqualToString:@"UR216"] ||
                [event.name isEqualToString:@"UR226"] ||
                [event.name isEqualToString:@"UR236"]) {
            [self.keys addObject:@"user_matchs"];
        }
        else if([event.name isEqualToString:@"UR207"] ||
                [event.name isEqualToString:@"UR217"] ||
                [event.name isEqualToString:@"UR227"] ||
                [event.name isEqualToString:@"UR237"]) {
            [self.keys addObject:@"user_places"];
        }
        else if([event.name isEqualToString:@"UR300"] ||
                [event.name isEqualToString:@"UR332"]) {
            [self.keys addObject:@"user_payments"];
        }
        else if([event.name isEqualToString:@"UR303"] ||
                [event.name isEqualToString:@"UR323"] ||
                [event.name isEqualToString:@"UR331"]) {
            [self.keys addObject:@"user_codes"];
        }
        else if([event.name isEqualToString:@"UR503"]) {
            [self.keys addObject:@"news_users"];
        }
        //
        else if([event.name isEqualToString:@"UQ001"]) {
            [self.keys addObject:@"user_device"];
        }
        else if([event.name isEqualToString:@"UQ002"]) {
            [self.keys addObject:@"user_device"];
            [self.keys addObject:@"drive_estimates"];
            [self.keys addObject:@"provider_devices"];
        }
        else if([event.name isEqualToString:@"UQ101"]) {
            [self.keys addObject:@"user_device"];
            [self.keys addObject:@"drive_estimates"];
        }
        else if([event.name isEqualToString:@"UQ100"] ||
                [event.name isEqualToString:@"UD110"] ||
                [event.name isEqualToString:@"UD120"] ||
                [event.name isEqualToString:@"UD130"] ||
                [event.name isEqualToString:@"UD131"]) {
            [self.keys addObject:@"user_device"];
            [self.keys addObject:@"drive_order"];
            [self.keys addObject:@"provider_device"];
        }
        else if([event.name isEqualToString:@"UQ200"] ||
                [event.name isEqualToString:@"UD210"] ||
                [event.name isEqualToString:@"UD230"]) {
            [self.keys addObject:@"user_device"];
            [self.keys addObject:@"drive_pickup"];
            [self.keys addObject:@"provider_device"];
            [self.keys addObject:@"drive_charge"];
        }
        else if([event.name isEqualToString:@"UQ200"] ||
                [event.name isEqualToString:@"UD210"] ||
                [event.name isEqualToString:@"UD230"]) {
            [self.keys addObject:@"user_device"];
            [self.keys addObject:@"drive_pickup"];
            [self.keys addObject:@"provider_device"];
            [self.keys addObject:@"drive_charge"];
        }
        
        
        
        
        
        else if([event.name isEqualToString:@"PP206"]) {
            [self.keys addObject:@"provider_matchs"];
        }
        else if([event.name isEqualToString:@"PP206"]) {
            [self.keys addObject:@"provider_matchs"];
        }
        else if([event.name isEqualToString:@"PP206"]) {
            [self.keys addObject:@"provider_matchs"];
        }
        else if([event.name isEqualToString:@"PP206"]) {
            [self.keys addObject:@"provider_matchs"];
        }
        else if([event.name isEqualToString:@"PP206"]) {
            [self.keys addObject:@"provider_matchs"];
        }
        else if([event.name isEqualToString:@"PP206"]) {
            [self.keys addObject:@"provider_matchs"];
        }
        else if([event.name isEqualToString:@"PP206"]) {
            [self.keys addObject:@"provider_matchs"];
        }
        else if([event.name isEqualToString:@"PP206"]) {
            [self.keys addObject:@"provider_matchs"];
        }
        else if([event.name isEqualToString:@"PP206"]) {
            [self.keys addObject:@"provider_matchs"];
        }
        else if([event.name isEqualToString:@"PP206"]) {
            [self.keys addObject:@"provider_matchs"];
        }
        else if([event.name isEqualToString:@"PP206"]) {
            [self.keys addObject:@"provider_matchs"];
        }
        
        
        
        else if([event.name isEqualToString:@"PP206"]) {
            [self.keys addObject:@"provider_matchs"];
        }
        else if([event.name isEqualToString:@"PP206"]) {
            [self.keys addObject:@"provider_matchs"];
        }
*/
        
        
        for (NSString *key in self.keys) {
            if ([Utils isDictionary:result[key]] || [Utils isArray:result[key]]) {
                [dic setObject:result[key] forKey:key];
            }
        }
        
        if (self.isDriverMode) {
            self.dicDriver = dic;
        }
        else {
            self.dicRider = dic;
        }
        dic = nil;
    }
}

#pragma mark Indicator
-(void) showBusyIndicator {
    [self showBusyIndicator:nil];
}

-(void) showBusyIndicator:(NSString *)title {
    if ([SVProgressHUD isVisible])
        return;
    //    if (!appDelegate.isNetwork) {
    //        return;
    //    }
    [SVProgressHUD showWithStatus:title];
    //[SVProgressHUD setBackgroundColor:SVProgressHUDMaskTypeGradient];
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleLight];
    
    //[SVProgressHUD showWithStatus:nil];
    //[SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeNone];
    //[SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
    //[SVProgressHUD setDefaultStyle:SVProgressHUDStyleCustom];
    //[SVProgressHUD setBackgroundColor:[UIColor clearColor]];
    [SVProgressHUD setForegroundColor:UIColorDefault];
    [SVProgressHUD setRingThickness:5];
}

-(void)hideBusyIndicator {
    if ([SVProgressHUD isVisible])
        [SVProgressHUD dismiss];
}

#pragma mark frostedViewController

- (void)frostedViewController:(REFrostedViewController *)frostedViewController didRecognizePanGesture:(UIPanGestureRecognizer *)recognizer
{
    
}

- (void)frostedViewController:(REFrostedViewController *)frostedViewController willShowMenuViewController:(UIViewController *)menuViewController
{
    //NSLog(@"willShowMenuViewController");
}

- (void)frostedViewController:(REFrostedViewController *)frostedViewController didShowMenuViewController:(UIViewController *)menuViewController
{
    //NSLog(@"didShowMenuViewController");
}

- (void)frostedViewController:(REFrostedViewController *)frostedViewController willHideMenuViewController:(UIViewController *)menuViewController
{
    //NSLog(@"willHideMenuViewController");
}

- (void)frostedViewController:(REFrostedViewController *)frostedViewController didHideMenuViewController:(UIViewController *)menuViewController
{
    //NSLog(@"didHideMenuViewController");
}

-(BOOL)isLoginState
{
//    NSString *userToken  = [[[TXApp instance] getSettings] getUserToken];
    NSString *userUid    = [[[TXApp instance] getSettings] getUserId];
    NSString *userTelno  = [[[TXApp instance] getSettings] getUserTelno];
    NSString *userPasswd = [[[TXApp instance] getSettings] getPassword];
    
    if (![[Utils nullToString:userUid] isEqualToString:@""] &&
        ![[Utils nullToString:userTelno] isEqualToString:@""] &&
        ![[Utils nullToString:userPasswd] isEqualToString:@""]
        ) {
        return YES;
    } else {
        return NO;
    }
}

#pragma mark - Common Alert
-(void)onCommonErrorAlert:(NSString*)failureMessage
{
    if (alertView != nil) {
        [alertView dismissAnimated:NO completionHandler:nil];
    }
    alertView = [[LGAlertView alloc] initWithTitle:@""
                                                        message:failureMessage
                                                          style:LGAlertViewStyleAlert
                                                   buttonTitles:@[LocalizedStr(@"Button.OK")]
                                              cancelButtonTitle:nil
                                         destructiveButtonTitle:nil
                                                  actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                                                      
                                                  }
                                                  cancelHandler:^(LGAlertView *alertView) {
                                                      
                                                  }
                                             destructiveHandler:^(LGAlertView *alertView) {
                                                 
                                             }];
    [Utils initAlertButtonColor:alertView];
    [alertView showAnimated:YES completionHandler:^(void)
     {
         
     }];
}

-(void)onCommonErrorAlert:(NSString*)failureMessage timeout:(NSInteger)timeout
{
    if (alertView != nil) {
        [alertView dismissAnimated:NO completionHandler:nil];
    }
    alertView = [[LGAlertView alloc] initWithTitle:@""
                                           message:failureMessage
                                             style:LGAlertViewStyleAlert
                                      buttonTitles:@[LocalizedStr(@"Button.OK")]
                                 cancelButtonTitle:nil
                            destructiveButtonTitle:nil
                                     actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                                         
                                     }
                                     cancelHandler:^(LGAlertView *alertView) {
                                         
                                     }
                                destructiveHandler:^(LGAlertView *alertView) {
                                    
                                }];
    [Utils initAlertButtonColor:alertView];
    [alertView showAnimated:YES completionHandler:^(void)
     {
         if (timeout) {
             dispatch_after(dispatch_time(DISPATCH_TIME_NOW, timeout * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                 [alertView dismissAnimated:YES completionHandler:nil];
             });
         }
         
     }];
}

#pragma mark -
#pragma mark Netowrk management
- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)checkNetwork {
/*
    // Allocate a reachability object
    Reachability* reach = [Reachability reachabilityWithHostname:@"www.google.com"];
    
    // Set the blocks
    reach.reachableBlock = ^(Reachability*reach)
    {
        // keep in mind this is called on a background thread
        // and if you are updating the UI it needs to happen
        // on the main thread, like this:
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self hideBusyIndicator];
            NSLog(@"REACHABLE!");
            self.isNetwork = YES;
            __block NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
            [notificationCenter postNotificationName:@"NotificationNetworkReachableReceived" object:nil userInfo:nil];
            
            //[self startSingleLocationRequest];
        });
    };
    
    reach.unreachableBlock = ^(Reachability*reach)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self showBusyIndicator];
        });
        NSLog(@"UNREACHABLE!");
        self.isNetwork = NO;
        __block NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
        [notificationCenter postNotificationName:@"NotificationNetworkReachableReceived" object:nil userInfo:nil];
    };
    
    // Start the notifier, which will cause the reachability object to retain itself!
    [reach startNotifier];
*/
    [GLobalRealReachability startNotifier];
    GLobalRealReachability.autoCheckInterval = 2;
    GLobalRealReachability.hostForPing = @"www.apple.com";
    ReachabilityStatus status = [GLobalRealReachability currentReachabilityStatus];
    [self networkCheck:status];
}

-(void)checkRetry
{
//    [GLobalRealReachability reachabilityWithBlock:^(ReachabilityStatus status) {
//        [self networkCheck:status];
//        //[self performSelector:@selector(checkRetry) withObject:nil afterDelay:2];
//    }];
    
    [GLobalRealReachability stopNotifier];
    [self checkNetwork];
    //ReachabilityStatus status = [GLobalRealReachability currentReachabilityStatus];
    //[self networkCheck:status];
//
}

-(void)networkReachable
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self hideBusyIndicator];
        NSLog(@"REACHABLE!");
        self.isNetwork = YES;
        [networkToast __hidePersistence];
        networkToast = nil;
        networkTimer = [NSDate date];
        [Utils resetStateUIInfo];
        __block NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
        [notificationCenter postNotificationName:@"NotificationNetworkReachableReceived" object:nil userInfo:nil];
        
        if ([Utils isDictionary:self.api_config]) {
//            [self connectRedis];
        }
    });
}

-(void)networkUnreachable
{
    dispatch_async(dispatch_get_main_queue(), ^{
        //[self showBusyIndicator:LocalizedStr(@"String.Network.Errro.text")];
        if (networkToast == nil) {
            networkToast = [WToast showWithTextPersistence:LocalizedStr(@"String.Network.Errro.text") duration:kWTShort roundedCorners:NO gravity:kWTGravityTop];
        }
    });
    
    NSDate *chkTime = [NSDate date];
    
    NSTimeInterval executionTime = [chkTime timeIntervalSinceDate:networkTimer]/60*60; //초
    NSLog(@"executionTime = %f", executionTime);
    if (executionTime <= 4) {
//        return;
    }
    
    NSLog(@"UNREACHABLE!");
    self.isNetwork = NO;
    __block NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    [notificationCenter postNotificationName:@"NotificationNetworkReachableReceived" object:nil userInfo:nil];
    
//    [self redisClose];
    
    if([UIApplication sharedApplication].applicationState == UIApplicationStateActive) {
        [self performSelector:@selector(checkRetry) withObject:nil afterDelay:5];
        //[self performSelectorInBackground:@selector(checkRetry) withObject:nil];
        //[self checkRetry];
    }
}

-(void)networkCheck:(ReachabilityStatus)status
{
    if (status == RealStatusNotReachable)
    {
        // @"Network unreachable!";
        [self networkUnreachable];
    }
    
    if (status == RealStatusViaWiFi)
    {
        // @"Network wifi! Free!";
        [self networkReachable];
    }
    
    if (status == RealStatusViaWWAN)
    {
        // @"Network WWAN! In charge!";
        [self networkReachable];
    }
}
- (void)networkChanged:(NSNotification *)notification
{
    RealReachability *reachability = (RealReachability *)notification.object;
    ReachabilityStatus status = [reachability currentReachabilityStatus];
    ReachabilityStatus previousStatus = [reachability previousReachabilityStatus];
    NSLog(@"networkChanged, currentStatus:%@, previousStatus:%@", @(status), @(previousStatus));
    
    [self networkCheck:status];
/*
    WWANAccessType accessType = [GLobalRealReachability currentWWANtype];
    
    if (status == RealStatusViaWWAN)
    {
        if (accessType == WWANType2G)
        {
            // @"RealReachabilityStatus2G";
            [self networkReachable];
        }
        else if (accessType == WWANType3G)
        {
            // @"RealReachabilityStatus3G";
            [self networkReachable];
        }
        else if (accessType == WWANType4G)
        {
            // @"RealReachabilityStatus4G";
            [self networkReachable];
        }
        else
        {
            // @"Unknown RealReachability WWAN Status, might be iOS6";
            [self networkReachable];
        }
    }
*/
}

#pragma mark - Locations
/**
 Starts a new one-time request for the current location.
 */
//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)startSingleLocationRequest
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    if (self->locationMgr == nil) {
        self->locationMgr = [[CLLocationManager alloc] init];
        self->locationMgr.delegate = self;
        //        self->locationMgr.allowsBackgroundLocationUpdates = YES;
        self->locationMgr.pausesLocationUpdatesAutomatically = NO;
        self->locationMgr.activityType = CLActivityTypeAutomotiveNavigation;
        
        if (self.isDriverMode) {
            // 항상 위치 정보 사용 요청
            //            if ([self->locationMgr respondsToSelector:@selector(requestAlwaysAuthorization)]) {
            //                [self->locationMgr requestAlwaysAuthorization];
            //            }
            if ([self->locationMgr respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
                [self->locationMgr requestWhenInUseAuthorization];
            }
        }
        else {
            // 사용중일때만 요청
            if ([self->locationMgr respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
                [self->locationMgr requestWhenInUseAuthorization];
            }
        }
        
        self->locationMgr.distanceFilter = kCLDistanceFilterNone;
        self->locationMgr.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
    }
    [self->locationMgr startUpdatingLocation];
}

#pragma mark CLLocationManager delegate
-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways) {
        [self->locationMgr startUpdatingLocation];
    }
    else if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse) {
        [self->locationMgr startUpdatingLocation];
    }
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    
    CLLocation *location = [manager location];
    
    NSNumber *myDoubleNumberLat = [NSNumber numberWithDouble:location.coordinate.latitude];
    NSNumber *myDoubleNumberLng = [NSNumber numberWithDouble:location.coordinate.longitude];
    
    [Utils updateCurrentPosition:[myDoubleNumberLat stringValue] lng:[myDoubleNumberLng stringValue]];
    
    [self->locationMgr stopUpdatingLocation];
    NSLog(@"didUpdateLocations: %@", location);
    
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    self.location = location;
    [locMgr updateCurrentLocation:location];
    locMgr.realTimeCLocation = location;
    
    [self startLocationUpdateSubscription];
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
    NSLog(@"Error while getting core location : %@",[error localizedFailureReason]);
    if ([error code] == kCLErrorDenied) {
        //you had denied
    }
    //[manager stopUpdatingLocation];
}

- (void)startSingleLocationRequest_old
{
    __weak __typeof(self) weakSelf = self;
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    
    self.locationRequestID = [locMgr requestLocationWithDesiredAccuracy:INTULocationAccuracyRoom
                                                                timeout:10
                                                   delayUntilAuthorized:YES
                                                                  block:
                              ^(CLLocation *currentLocation, INTULocationAccuracy achievedAccuracy, INTULocationStatus status) {
                                  __typeof(weakSelf) strongSelf = weakSelf;
                                  
                                  NSLog(@"timeout0 : %ld, %.6f/%.6f",(long)self.locationRequestID,currentLocation.coordinate.latitude,currentLocation.coordinate.longitude);
                                  
                                  if (status == INTULocationStatusSuccess) {
                                      // achievedAccuracy is at least the desired accuracy (potentially better)
                                      NSLog(@"timeout0 OK : %ld",(long)self.locationRequestID);
                                      self.location = currentLocation;
                                      [locMgr updateCurrentLocation:currentLocation];
                                      locMgr.realTimeCLocation = currentLocation;
                                      
                                      NSNumber *myDoubleNumberLat = [NSNumber numberWithDouble:currentLocation.coordinate.latitude];
                                      NSNumber *myDoubleNumberLng = [NSNumber numberWithDouble:currentLocation.coordinate.longitude];
                                      
                                      [Utils updateCurrentPosition:[myDoubleNumberLat stringValue] lng:[myDoubleNumberLng stringValue]];
                                  }
                                  else if (status == INTULocationStatusTimedOut) {
                                      // You may wish to inspect achievedAccuracy here to see if it is acceptable, if you plan to use currentLocation
                                      NSLog(@"timeout0 timeout : %ld, %@",(long)self.locationRequestID, locMgr.realTimeCLocation);
                                  }
                                  else {
                                      // An error occurred
                                      NSLog(@"timeout0 error : %@",[locMgr getLocationErrorDescription:status]);
                                  }
                                  
                                  strongSelf.locationRequestID = NSNotFound;
                                  
                                  [self startLocationUpdateSubscription];
                              }];
}

/**
 Starts a new subscription for location updates.
 */
- (void)startLocationUpdateSubscription
{
    if (self.locationRequestID != NSNotFound) {
        NSLog(@"startLocationUpdateSubscription Already : %d",(int)self.locationRequestID);
        return;
    }
    __weak __typeof(self) weakSelf = self;
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    
    INTULocationAccuracy desiredAccuracy = INTULocationAccuracyRoom; //INTULocationAccuracyBlock; //INTULocationAccuracyRoom
    //int distanceFilter = 5; //kCLDistanceFilterNone
    int distanceFilter = kCLDistanceFilterNone; //kCLDistanceFilterNone
    
    NSLog(@"startLocationUpdateSubscription OK : %.6f/%.6f",self.location.coordinate.latitude,self.location.coordinate.longitude);

    self.locationRequestID = [locMgr subscribeToLocationUpdatesWithDesiredAccuracy:desiredAccuracy
                                                                    distanceFilter:distanceFilter
                                                                             block:^(CLLocation *currentLocation, INTULocationAccuracy achievedAccuracy, INTULocationStatus status) {
        __typeof(weakSelf) strongSelf = weakSelf;
        if (status == INTULocationStatusSuccess) {
            // A new updated location is available in currentLocation, and achievedAccuracy indicates how accurate this particular location is
            strongSelf.location = currentLocation;
            
            @try {
                
                //NSLog(@"startLocationUpdateSubscription OK : %ld, %.6f/%.6f",(long)strongSelf.locationRequestID,currentLocation.coordinate.latitude,currentLocation.coordinate.longitude);
                //NSLog(@"startLocationUpdateSubscription locMgr.realTimeCLocation.coordinate:%f,%f",locMgr.realTimeCLocation.coordinate.latitude,locMgr.realTimeCLocation.coordinate.longitude);
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (strongSelf.locationRequestID != NSNotFound && strongSelf.locationDelegate != nil && [GoogleUtils isLocationValid:currentLocation.coordinate]) {
                        if ( [strongSelf.locationDelegate respondsToSelector:@selector(didUpdateLocations:)] ) {
                            [strongSelf.locationDelegate didUpdateLocations:currentLocation];
                        }
                    }
                });
                
                NSNumber *myDoubleNumberLat = [NSNumber numberWithDouble:currentLocation.coordinate.latitude];
                NSNumber *myDoubleNumberLng = [NSNumber numberWithDouble:currentLocation.coordinate.longitude];
                
                [Utils updateCurrentPosition:[myDoubleNumberLat stringValue] lng:[myDoubleNumberLng stringValue]];
            }
            @catch (NSException *exception) {
                NSLog(@"Exception: %@, %@", exception.name, exception.reason);
            }
            
        }
        else {
            // An error occurred
            NSLog(@"startLocationUpdateSubscription error : %@",[locMgr getLocationErrorDescription:status]);
        }
    }];
}

/**
 Starts a new subscription for significant location changes.
 */
- (void)startMonitoringSignificantLocationChanges
{
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    self.locationRequestID = [locMgr subscribeToSignificantLocationChangesWithBlock:^(CLLocation *currentLocation, INTULocationAccuracy achievedAccuracy, INTULocationStatus status) {
        
        if (status == INTULocationStatusSuccess) {
            // A new updated location is available in currentLocation, and achievedAccuracy indicates how accurate this particular location is
            self.location = currentLocation;
            
            if (self.locationDelegate != nil) {
                if ( [self.locationDelegate respondsToSelector:@selector(didUpdateLocations:)] ) {
                    [self.locationDelegate didUpdateLocations:currentLocation];
                }
            }
            
            NSNumber *myDoubleNumberLat = [NSNumber numberWithDouble:currentLocation.coordinate.latitude];
            NSNumber *myDoubleNumberLng = [NSNumber numberWithDouble:currentLocation.coordinate.longitude];
            
            [Utils updateCurrentPosition:[myDoubleNumberLat stringValue] lng:[myDoubleNumberLng stringValue]];
        }
        else {
            // An error occurred
            
        }
    }];
}

/**
 Callback when the "Force Complete Request" button is tapped.
 */
- (void)forceCompleteRequest
{
    [[INTULocationManager sharedInstance] forceCompleteLocationRequest:self.locationRequestID];
    // Clear the location request ID, since this will not be handled inside the subscription block
    // (This is not necessary for regular one-time location requests, since they will handle this inside the completion block.)
    self.locationRequestID = NSNotFound;
}

/**
 Callback when the "Cancel Request" button is tapped.
 */
- (void)cancelRequest
{
    [[INTULocationManager sharedInstance] cancelLocationRequest:self.locationRequestID];
    self.locationRequestID = NSNotFound;
}

#ifdef _DRIVER_MODE
#pragma mark - Spotify
#pragma mark -

-(NSError*)handleNewSession {
    SPTAuth *auth = [SPTAuth defaultInstance];
    
    if (self.player == nil) {
        NSError *error = nil;
        self.player = [SPTAudioStreamingController sharedInstance];
        [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
        if ([self.player startWithClientId:auth.clientID audioController:nil allowCaching:YES error:&error]) {
            self.player.delegate = self;
            self.player.playbackDelegate = self;
            self.player.diskCache = [[SPTDiskCache alloc] initWithCapacity:1024 * 1024 * 64];
            [self.player loginWithAccessToken:auth.session.accessToken];
            
            if ( [self.spotifyDelegate respondsToSelector:@selector(didChangeUISpotify:)] ) {
                [self.spotifyDelegate didChangeUISpotify:YES];
            }
        } else {
            self.player = nil;
            
            return error;
        }
    }
    return nil;
}

#pragma mark - Track Player Delegates

- (void)audioStreaming:(SPTAudioStreamingController *)audioStreaming didReceiveMessage:(NSString *)message {
    UIAlertView *__alertView = [[UIAlertView alloc] initWithTitle:@"Message from Spotify"
                                                        message:message
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
    [__alertView show];
}

- (void)audioStreaming:(SPTAudioStreamingController *)audioStreaming didChangePlaybackStatus:(BOOL)isPlaying {
    NSLog(@"is playing = %d", isPlaying);
    if (isPlaying) {
        [self activateAudioSession];
    } else {
        [self deactivateAudioSession];
    }
}

-(void)audioStreaming:(SPTAudioStreamingController *)audioStreaming didChangeMetadata:(SPTPlaybackMetadata *)metadata {
    //update UI
    [[NSNotificationCenter defaultCenter] postNotificationName:@"NotificationSpotifyUpdateReceived" object:nil userInfo:nil];
}

-(void)audioStreaming:(SPTAudioStreamingController *)audioStreaming didReceivePlaybackEvent:(SpPlaybackEvent)event withName:(NSString *)name {
    NSLog(@"didReceivePlaybackEvent: %zd %@", event, name);
    NSLog(@"isPlaying=%d isRepeating=%d isShuffling=%d isActiveDevice=%d positionMs=%f",
          self.player.playbackState.isPlaying,
          self.player.playbackState.isRepeating,
          self.player.playbackState.isShuffling,
          self.player.playbackState.isActiveDevice,
          self.player.playbackState.position);
}

- (void)audioStreamingDidLogout:(SPTAudioStreamingController *)audioStreaming {
    // close session
    [[NSNotificationCenter defaultCenter] postNotificationName:@"NotificationSpotifyCloseSessionReceived" object:nil userInfo:nil];
}

- (void)audioStreaming:(SPTAudioStreamingController *)audioStreaming didReceiveError:(NSError* )error {
    NSLog(@"didReceiveError: %zd %@", error.code, error.localizedDescription);
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"NotificationSpotifyErrorReceived" object:nil userInfo:@{@"error" : error}];
}

- (void)audioStreaming:(SPTAudioStreamingController *)audioStreaming didChangePosition:(NSTimeInterval)position {
    if (self.isChangingProgress) {
        return;
    }
    
    if (self.spotifyDelegate == nil) {
        return;
    }
    
    self.playerDuration = position;
    //NSLog(@"xx:%f,%f",self.player.playbackState.position,position);
    
    if ( [self.spotifyDelegate respondsToSelector:@selector(didChangePositionSpotify:)] ) {
        [self.spotifyDelegate didChangePositionSpotify:position];
    }
    
    //self.progressSlider.value = position/self.player.metadata.currentTrack.duration;
    
}

- (void)audioStreaming:(SPTAudioStreamingController *)audioStreaming didStartPlayingTrack:(NSString *)trackUri {
    NSLog(@"Starting %@", trackUri);
    NSLog(@"Source %@", self.player.metadata.currentTrack.playbackSourceUri);
    // If context is a single track and the uri of the actual track being played is different
    // than we can assume that relink has happended.
    BOOL isRelinked = [self.player.metadata.currentTrack.playbackSourceUri containsString: @"spotify:track"]
    && ![self.player.metadata.currentTrack.playbackSourceUri isEqualToString:trackUri];
    NSLog(@"Relinked %d", isRelinked);
    
    [self updatePlayingNowInfo];
}

- (void)audioStreaming:(SPTAudioStreamingController *)audioStreaming didStopPlayingTrack:(NSString *)trackUri {
    NSLog(@"Finishing: %@", trackUri);
}

- (void)audioStreamingDidLogin:(SPTAudioStreamingController *)audioStreaming {
    
    //update UI
    [[NSNotificationCenter defaultCenter] postNotificationName:@"NotificationSpotifyUpdateReceived" object:nil userInfo:nil];
    
    if (self.isKorea) {
        [self.player playSpotifyURI:@"spotify:user:spotify:playlist:37i9dQZF1DX1wdZM1FEz79" startingWithIndex:0 startingWithPosition:10 callback:^(NSError *error) {
            if (error != nil) {
                NSLog(@"*** failed to play: %@", error);
                return;
            }
        }];
    }
    else {
        [self.player playSpotifyURI:@"spotify:user:spotify:playlist:5FJXhjdILmRA2z5bvz4nzf" startingWithIndex:0 startingWithPosition:10 callback:^(NSError *error) {
            if (error != nil) {
                NSLog(@"*** failed to play: %@", error);
                return;
            }
        }];
    }
    
}

#pragma mark - Audio Session

- (void)activateAudioSession
{
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback
                                           error:nil];
    [[AVAudioSession sharedInstance] setActive:YES error:nil];
}

- (void)deactivateAudioSession
{
    [[AVAudioSession sharedInstance] setActive:NO error:nil];
}

#pragma mark -
#pragma mark ===========   Remote Control  =========
#pragma mark -

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)remoteControlReceivedWithEvent:(UIEvent *)event
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    
    
    if (event.type == UIEventTypeRemoteControl)
    {
        switch (event.subtype)
        {
            case UIEventSubtypeRemoteControlPlay:
                //[hysteriaPlayer play];
                [self.player setIsPlaying:YES callback:nil];
                
                if ( [self.spotifyDelegate respondsToSelector:@selector(didChangeUISpotify:)] ) {
                    [self.spotifyDelegate didChangeUISpotify:YES];
                }
                break;
                
            case UIEventSubtypeRemoteControlPause:
                //[hysteriaPlayer pause];
                [self.player setIsPlaying:NO callback:nil];
                
                if ( [self.spotifyDelegate respondsToSelector:@selector(didChangeUISpotify:)] ) {
                    [self.spotifyDelegate didChangeUISpotify:NO];
                }
                break;
                
            case UIEventSubtypeRemoteControlTogglePlayPause:
                
                if (self.player.playbackState.isPlaying)
                {
                    [self.player setIsPlaying:NO callback:nil];
                    if ( [self.spotifyDelegate respondsToSelector:@selector(didChangeUISpotify:)] ) {
                        [self.spotifyDelegate didChangeUISpotify:NO];
                    }
                }else{
                    [self.player setIsPlaying:YES callback:nil];
                    if ( [self.spotifyDelegate respondsToSelector:@selector(didChangeUISpotify:)] ) {
                        [self.spotifyDelegate didChangeUISpotify:YES];
                    }
                }
                
                
                break;
                
            case UIEventSubtypeRemoteControlNextTrack:
                
                [self.player skipNext:nil];
                
                if ( [self.spotifyDelegate respondsToSelector:@selector(didChangeUISpotify:)] ) {
                    [self.spotifyDelegate didChangeUISpotify:YES];
                }
                break;
                
            case UIEventSubtypeRemoteControlPreviousTrack:
                
                [self.player skipPrevious:nil];
                
                if ( [self.spotifyDelegate respondsToSelector:@selector(didChangeUISpotify:)] ) {
                    [self.spotifyDelegate didChangeUISpotify:YES];
                }
                break;
                
            default:
                break;
        }
    }
}

#pragma mark Playing Now

//-------------------------------------------------------------------------------------------------------------------------------------------------
- (void)updatePlayingNowInfo
//-------------------------------------------------------------------------------------------------------------------------------------------------
{
    
    
    NSDictionary *playingNowInfo = @{MPMediaItemPropertyTitle: self.player.metadata.currentTrack.name,
                                     MPMediaItemPropertyPlaybackDuration: @(self.player.metadata.currentTrack.duration),
                                     MPNowPlayingInfoPropertyPlaybackRate: @(self.player.targetBitrate),
                                     MPNowPlayingInfoPropertyElapsedPlaybackTime: @(self.player.playbackState.position)
                                     };
    [MPNowPlayingInfoCenter defaultCenter].nowPlayingInfo = playingNowInfo;
}

#endif

@end
