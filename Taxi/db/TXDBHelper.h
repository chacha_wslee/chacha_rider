//
//  TXDBHelper.h
//  Taxi
//

//

#import <Foundation/Foundation.h>

@interface TXDBHelper : NSObject

+(TXDBHelper *) mainDB;

/*!@function executeQuery
 * Executes the query with spcified query string (with parameters map)
 @param queryStr
 @param arguments
 @return NSArray.
 */
-(NSArray *) executeQuery : (NSString *) queryStr withParameterDictionary:(NSDictionary *)arguments;

/*!@function executeUpdate
 * Executes the sql string with specified parameters in arguments array
 @param sqlStr
 @param arguments
 @return BOOL
 */
-(BOOL) executeUpdate : (NSString *) sqlStr withArgumentsInArray:(NSArray *)arguments;

-(int) close;

@end
