//
//  TXTaxiModel.m
//  Taxi
//

//

#import "TXTaxiModel.h"
#import "TXError.h"
#import <objc/runtime.h>

@implementation TXTaxi

@end

@implementation TXTaxiModel

/** Creates the single instance within the application
 
 @return TXTaxiModel
 */
+(TXTaxiModel *) instance {
    static dispatch_once_t pred;
    static TXTaxiModel* _instance;
    dispatch_once(&pred, ^{ _instance = [[self alloc] init]; });
    return _instance;
}


- (NSDictionary *)dictionaryValue
{
    NSMutableArray *propertyKeys = [NSMutableArray array];
    Class currentClass = self.class;
    
    while ([currentClass superclass]) { // avoid printing NSObject's attributes
        unsigned int outCount, i;
        objc_property_t *properties = class_copyPropertyList(currentClass, &outCount);
        for (i = 0; i < outCount; i++) {
            objc_property_t property = properties[i];
            const char *propName = property_getName(property);
            if (propName) {
                NSString *propertyName = [NSString stringWithUTF8String:propName];
                [propertyKeys addObject:propertyName];
            }
        }
        free(properties);
        currentClass = [currentClass superclass];
    }
    
    return [self dictionaryWithValuesForKeys:propertyKeys];
}

@end
