//
//  TXTaxiModel.h
//  Taxi
//

//

#import <Foundation/Foundation.h>
#import "TXBaseObj.h"
#import "TXModelBase.h"

@interface TXTaxi : TXBaseObj

@property (nonatomic, retain) id provider;

@end

@interface TXTaxiModel : TXModelBase

/** Creates the single instance within the application
 
 @return TXTaxiModel
 */
+(TXTaxiModel *) instance;

- (NSDictionary *)dictionaryValue;

@end
