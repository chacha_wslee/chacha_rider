//
//  TXHttpRequestManager.m
//  Taxi
//

//
#import "JSNetworkActivityIndicatorManager.h"
#import "TXAppDelegate.h"
#import "TXHttpRequestManager.h"
#import "StrConsts.h"
#import "Macro.h"
#import "utils.h"
#import "TXFileManager.h"
#import "TXApp.h"
#import "NSString+TXNSString.h"

//#import "RedisSingleton.h"

static NSString* const HTTPAPI_PLIST_FILE = @"httpapi";
static NSString* const DEFAULT_CONFIG = @"register";
static NSString* const HDR_ACCEPT         = @"Accept";
static NSString* const HDR_CONTENTTYPE    = @"Content-Type";

@implementation TXRequestConfig

@synthesize name, url, httpMethod, headers, hasUrlBase, req, bodyTemplate, useCache, isRedis;

/*
 IC이미지
 rider GET  POST
 UR601      UR621
 PP602
 PP603
 
 driver GET  Account GET POST
 UR601
 PP602       PP605       PP628
 PP603       PP606       PP629
 */

+(TXRequestConfig *) configForName:(NSString*) name{
    
    NSDictionary* root = [[[TXApp instance] getSettings] getProperty:SettingsConst.Property.HTTP_API];
    
    if ( [root count] > 0 ) {
        
        if( [name length]==0 ) {
            name = DEFAULT_CONFIG;
        }
        
        TXRequestConfig *reqConfig = [[TXRequestConfig alloc] init];
        
        NSMutableDictionary *config = [root objectForKey:name];
        NSLog(@"-------name:%@",name);
        
        if ( config != nil ) {
            reqConfig.name = name;
//            reqConfig.local = [[config objectForKey:@"local"] boolValue];
            reqConfig.headers = [config objectForKey:@"headers"];
            reqConfig.httpMethod = [config objectForKey:@"httpMethod"];
            reqConfig.url = [config objectForKey:@"url"];
            reqConfig.req = [config objectForKey:@"req"];
            reqConfig.hasUrlBase = [[config objectForKey:@"hasUrlBase"] boolValue];
            reqConfig.bodyTemplate = [config objectForKey:@"bodyTemplate"];
            reqConfig.timeOut = [config objectForKey:@"timeOut"];
            reqConfig.useCache = NO;
            reqConfig.isRedis = NO;
            
            if ([name isEqualToString:@"UR601"] ||
                [name isEqualToString:@"UR602"] ||
                [name isEqualToString:@"UR603"] ||
                
                [name isEqualToString:@"PP601"] ||
                [name isEqualToString:@"PP602"] ||
                [name isEqualToString:@"PP603"] ||
                [name isEqualToString:@"PP605"] ||
                [name isEqualToString:@"PP606"]
                
                //[name isEqualToString:@"PP301"] ||
                //[name isEqualToString:@"PP302"] ||
                //[name isEqualToString:@"PP401"] ||
                //[name isEqualToString:@"PP402"] ||
                //[name isEqualToString:@"PP403"] ||
                //[name isEqualToString:@"PP404"]
                
                ) {
                reqConfig.useCache = YES;
            }
            
            if ([name isEqualToString:@"UQ002"] ||
                [name isEqualToString:@"PP404"]
                
                ) {
                reqConfig.isRedis = NO;
            }
            return  reqConfig;
        }
    }
    
    DLogE(@"Invalid http api definitions file!");
    return nil;
}

@end

@interface TXRequestObj() {
    TXSettings *settings;
}
-(id) initWithConfig:(NSString *)config urlParams:(NSString *)urlParams_ listener:(id<TXHttpRequestListener>) listener_;
@end

@implementation TXRequestObj

@synthesize	reqUrl, receivedData, listener, target, reqConfig, body, messageUID;

+(id) create:(NSString *)config urlParams:(NSString *)urlParams listener:(id<TXHttpRequestListener>) listener {
    
    if ( config.length == 0 ) {
        return nil;
    }
    
    return [[self alloc] initWithConfig:config urlParams:urlParams listener:listener];
    
}

-(id) initWithConfig:(NSString *)config urlParams:(NSString *)urlParams_ listener:(id<TXHttpRequestListener>) listener_ {
    
    if(self = [super init]) {
    
//        NSLog(@"config:%@",config);
        self.messageUID = config;
        self.listener = listener_;
        self.reqConfig = [TXRequestConfig configForName:config];
        self.urlParams = urlParams_;

        settings = [[TXApp instance] getSettings];
        
        if(self.reqConfig.hasUrlBase == YES) {
            self.baseURL = [NSString stringWithFormat:@"%@:%@%@", [settings getProperty:SettingsConst.Property.BASEURL], [settings getProperty:SettingsConst.Property.PORT],
                [settings getProperty:SettingsConst.Property.URLPATH]
                            ];
            self.reqUrl = [NSString stringWithFormat:@"%@/%@.php", self.baseURL, self.reqConfig.url];
            
            [settings setFDKeychain:SettingsConst.CryptoKeys.MID value:self.messageUID];
            
        } else {
            self.reqUrl = [NSString stringWithFormat:self.reqConfig.url, self.urlParams];
        }

        self.attemptCount = 0;
        
    }
    
    return self;
    
}

-(NSString*)createHTTPUrl {
    NSString* _url = self.reqUrl;
    
    if ( self.body && [self.body length] > 0)
    {
        NSDictionary* dict = getJSONObj(self.body);
        
        NSMutableString * url = [[NSMutableString alloc] init];
        for (NSString *aKey in [dict allKeys]) {
            if (url.length) {
                [url appendString:@"&"];
            }
            NSString* name = [NSString stringWithFormat:@"%@=%@",aKey,[dict valueForKey:aKey]];
            [url appendString:name];
        }
        
        if (url.length) {
            _url = [NSString stringWithFormat:@"%@?%@",_url,url];
        }
    }
    return _url;
}

-(NSString*)appendTimestampUrl:(NSString*)url param:(NSString*)param useCache:(BOOL)cache{
    
    if (!cache) {
//        if ([url rangeOfString:@"&"].location != NSNotFound) {
//            url = [NSString stringWithFormat:@"%@&%d",url,(int)[[NSDate date] timeIntervalSince1970]];
//        }
//        else if ([url rangeOfString:@"?"].location == NSNotFound) {
//            url = [NSString stringWithFormat:@"%@?%d",url,(int)[[NSDate date] timeIntervalSince1970]];
//        }
//        
        if ([url rangeOfString:@"&"].location != NSNotFound) {
            url = [NSString stringWithFormat:@"%@&%@&%@",url,param,[Utils get20Header]];
        }
        else if ([url rangeOfString:@"?"].location == NSNotFound) {
            url = [NSString stringWithFormat:@"%@?%@&%@",url,param,[Utils get20Header]];
        }
        else {
            url = [NSString stringWithFormat:@"%@&%@&%@",url,param,[Utils get20Header]];
        }
    }
    else {
        if ([url rangeOfString:@"&"].location != NSNotFound) {
            url = [NSString stringWithFormat:@"%@&%@&%@",url,param,[Utils get20ImageHeader]];
        }
        else if ([url rangeOfString:@"?"].location == NSNotFound) {
            url = [NSString stringWithFormat:@"%@?%@&%@",url,param,[Utils get20ImageHeader]];
        }
        else {
            url = [NSString stringWithFormat:@"%@&%@&%@",url,param,[Utils get20ImageHeader]];
        }
    }
    
    return url;
}

-(NSMutableURLRequest*)createHTTPRequest {
    
    TXRequestConfig* rqCfg = self.reqConfig;
    if ( rqCfg != nil )
	{
        NSURLRequestCachePolicy cachePolicy = NSURLRequestReloadIgnoringCacheData;
        if (rqCfg.useCache) {
            cachePolicy = NSURLRequestReturnCacheDataElseLoad;
        }
        
        NSString *extParam = @"";
        if(self.reqConfig.hasUrlBase == YES) {
            extParam = [NSString stringWithFormat:@"cmd=%@&req=%@&mtd=%@&mid=%@",rqCfg.url,rqCfg.req,rqCfg.httpMethod,self.messageUID];
        }
        else {
            extParam = [NSString stringWithFormat:@"req=%@&mtd=%@&mid=%@",rqCfg.req,rqCfg.httpMethod,self.messageUID];
        }
        
        // image upload
        if ([rqCfg.name isEqualToString:@"UR621"] ||
            [rqCfg.name isEqualToString:@"PP628"] ||
            [rqCfg.name isEqualToString:@"PP629"]
            ) {
            NSDictionary *dicBody = getJSONObj(self.body);
            
            for (NSString *key in dicBody) {
                id value = [dicBody objectForKey:key];
                
                if ([key isEqualToString:@"picture"] ||
                    [key isEqualToString:@"driver"] ||
                    [key isEqualToString:@"vehicle"] ||
                    [key isEqualToString:@"registration"] ||
                    [key isEqualToString:@"insurance"] ||
                    [key isEqualToString:@"inspection"] ||
                    [key isEqualToString:@"taxpayer"]
                    ) {
                //if ([key rangeOfString:@"IMAGE"].location != NSNotFound) {
                    
                    id decodedData = [[NSData alloc] initWithBase64EncodedString:value options:NSDataBase64DecodingIgnoreUnknownCharacters];
                    //NSString *receivedDataString = [[NSString alloc] initWithData:decodedData encoding:NSUTF8StringEncoding];
                    self.body = decodedData;
                }
                else {
                    extParam = [NSString stringWithFormat:@"%@&%@=%@",extParam,key,value];
                }
            }
        }
        
        NSURL* pathURL = [NSURL URLWithString:[self appendTimestampUrl:self.reqUrl param:extParam useCache:rqCfg.useCache]];
        int timeOutSecs = rqCfg.timeOut != nil ? [rqCfg.timeOut intValue] : DEFTIMEOUT;
        NSMutableURLRequest *httpRequest = [[NSMutableURLRequest alloc] initWithURL: pathURL
                                                                        cachePolicy: cachePolicy
                                                                        timeoutInterval: timeOutSecs];
        
        [httpRequest setHTTPMethod: rqCfg.httpMethod];

        if ( rqCfg.headers != nil ) {
            
            NSEnumerator* e = [rqCfg.headers keyEnumerator];
            NSString* hdrField;
            while( (hdrField = [e nextObject]) )
            {
                NSString *value = [rqCfg.headers objectForKey:hdrField];
                
                if( [hdrField isEqualToString:@"Authorization"] && (value == nil || [value length] == 0) ) {
                    value = [NSString stringWithFormat:@"%@:%@", [self->settings getUserId], [self->settings getPassword]];
                    value = [NSString stringWithFormat:@"Basic %@", base64String(value) ];
                }
                
                [httpRequest addValue:value forHTTPHeaderField:hdrField];
                
            }
        }

        //check if we have set Accept header field, if not set json as default
        NSString* accept = [httpRequest valueForHTTPHeaderField:HDR_ACCEPT];
        if ( accept == nil )
            [httpRequest addValue:@"application/json" forHTTPHeaderField:HDR_ACCEPT];
        
        if ([rqCfg.httpMethod isEqualToString:@"POST"] || [rqCfg.httpMethod isEqualToString:@"PUT"] || [rqCfg.httpMethod isEqualToString:@"DELETE"])
        {
            NSMutableData *postBody = [NSMutableData data];
            
            if ( self.body && [self.body length] > 0)
            {
                // image upload
                if ([rqCfg.name isEqualToString:@"UR621"] ||
                    [rqCfg.name isEqualToString:@"PP628"] ||
                    [rqCfg.name isEqualToString:@"PP629"]
                    ) {
                    [postBody appendData:  (NSMutableData*)self.body ];
                }
                else {
                    [postBody appendData:  [self.body dataUsingEncoding:NSUTF8StringEncoding] ];
                }
            }
            
            //default content type to json if it wasn't set by headers already
            NSString* ctype = [httpRequest valueForHTTPHeaderField:HDR_CONTENTTYPE];
            if ( ctype == nil )
                [httpRequest addValue:@"application/json" forHTTPHeaderField:HDR_CONTENTTYPE];
            
            if ([postBody length] > 0)
            {
                NSString *rLen = [NSString stringWithFormat:@"%lu", (unsigned long)[postBody length]];
                [httpRequest addValue:rLen forHTTPHeaderField:@"Content-Length"];
                
                [httpRequest setHTTPBody:postBody];
            }
        }
        else {
            
            if ( self.body && [self.body length] > 0)
            {
                NSString* url = [self createHTTPUrl];;
                if ([rqCfg.name containsString:@"Tmap"]) {
                    if ([extParam length]) {
                        extParam = [NSString stringWithFormat:@"%@&%@",extParam,self.body];
                    }
                    else {
                        extParam = self.body;
                    }
                }
                if (url.length) {
                    self.reqUrl = url;
                    
                    NSURL* pathURL = [NSURL URLWithString:[self appendTimestampUrl:self.reqUrl param:extParam useCache:rqCfg.useCache]];
                    [httpRequest setURL:pathURL];
                }
                
                self.body = nil;
            }
        }
        
        self.reqUrl = [self appendTimestampUrl:self.reqUrl param:extParam useCache:rqCfg.useCache];
        
        //if ([rqCfg.name isEqualToString:@"TmapDirectionsByCoordinates"]) {
        if ([rqCfg.name containsString:@"Tmap"]) {
            [httpRequest setValue:_TMAP_API_KEY forHTTPHeaderField:@"appkey"];
        }
        else {
            NSString *xpString = [Utils getXHTTPType];
            NSString *xhString = [Utils getXHTTPHeader];
            NSString *xtString = [Utils getXHTTPToken];
            NSString *xlString = [Utils getXHTTPLocation];
            [httpRequest setValue:xpString forHTTPHeaderField:@"X-Service-Type"];
            [httpRequest setValue:xhString forHTTPHeaderField:@"X-Service-Header"];
            [httpRequest setValue:xtString forHTTPHeaderField:@"X-Service-Token"];
            [httpRequest setValue:xlString forHTTPHeaderField:@"X-Service-Location"];
            
            NSString *xrString = [NSString stringWithFormat:@"midx=%@,cmd=%@,req=%@",
                                  self.messageUID, rqCfg.url, rqCfg.req];
            [httpRequest setValue:xrString forHTTPHeaderField:@"X-Service-Request"];
            
            NSLog(@"Request-Header:\n%@\n%@\n%@\n%@\n%@",xpString,xhString,xtString,xlString,xrString);
        }
        
//        DLogI(@"\n[URL   ] %@\n[Header] %@\n[Body  ] %@\n",
//              httpRequest.URL.absoluteString,
//              httpRequest.allHTTPHeaderFields,
//              [[NSString alloc] initWithData:httpRequest.HTTPBody encoding:NSUTF8StringEncoding]);
        
        return httpRequest;
    }
    
    return nil;
}

@end

@interface TXHttpRequestManager() {
    NSArray*            trustedHosts;
}
@property (nonatomic, strong) NSURLSession *session;

-(TXRequestObj*)requestForConnection:(NSURLConnection*)connection;
-(TXRequestObj*)requestForSession:(NSURLSessionTask*)connection;

+(NSString*)urlEncode:(NSString*)src;

@end

@implementation TXHttpRequestManager

/** creates the single instance within the application
 
 @return TXHttpRequestManager
 */
+(TXHttpRequestManager *) instance {
    static dispatch_once_t pred;
    static TXHttpRequestManager* _instance;
    dispatch_once(&pred, ^{ _instance = [[self alloc] init]; });
    return _instance;
}

-(id) init {
    
    self = [super init];
    if(self!=nil) {
        self->pendingRequests = [[NSMutableDictionary alloc] initWithCapacity:5];
    }
    
    return self;
}

-(TXRequestObj*)requestForConnection:(NSURLConnection*)connection {
    
    NSValue* connVal = [NSValue valueWithNonretainedObject:connection];
    return [self->pendingRequests objectForKey:connVal];
}

-(TXRequestObj*)requestForSession:(NSURLSessionTask*)connection {
    
    NSValue* connVal = [NSValue valueWithNonretainedObject:connection];
    return [self->pendingRequests objectForKey:connVal];
}

-(TXRequestObj*)requestForRedis:(NSString*)connection {
    
    NSValue* connVal = [NSValue valueWithNonretainedObject:connection];
    return [self->pendingRequests objectForKey:connVal];
}


/**
 Encodes string correctly, including & and / unlike Apple's crappy encode
 **/
+(NSString*)urlEncode:(NSString*)src {
    return CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL, (CFStringRef)src, NULL, (CFStringRef)@"!*'\"();:@&=+$,/?%#[]% ", kCFStringEncodingUTF8));
}


-(BOOL)sendSyncRequest:(TXRequestObj*)request {
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
        // 작업이 오래 걸리는 API를 백그라운드 스레드에서 실행한다.
        id result = nil;
        
        if ( request != nil )
        {
            NSMutableURLRequest* httpRequest = [request createHTTPRequest];
            NSError* error_ = nil;
            NSURLResponse* response;
            
            // Toggle the network activity indicator
            // Check if the indicator is on
            dispatch_async(dispatch_get_main_queue(), ^{
            if (![UIApplication sharedApplication].isNetworkActivityIndicatorVisible) {
                
                // Turn the indicator on
                [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
                
            }
            });
            
            if ([request.reqConfig.name isEqualToString:@"UR601"] ||
                [request.reqConfig.name isEqualToString:@"UR602"] ||
                [request.reqConfig.name isEqualToString:@"UR603"] ||
                [request.reqConfig.name isEqualToString:@"UR621"] ||
                
                [request.reqConfig.name isEqualToString:@"PP601"] ||
                [request.reqConfig.name isEqualToString:@"PP602"] ||
                [request.reqConfig.name isEqualToString:@"PP603"] ||
                [request.reqConfig.name isEqualToString:@"PP605"] ||
                [request.reqConfig.name isEqualToString:@"PP606"] ||
                [request.reqConfig.name isEqualToString:@"PP628"] ||
                [request.reqConfig.name isEqualToString:@"PP629"]
                
//                [request.reqConfig.name isEqualToString:@"PP301"] ||
//                [request.reqConfig.name isEqualToString:@"PP302"] ||
//                [request.reqConfig.name isEqualToString:@"PP303"] ||
//                [request.reqConfig.name isEqualToString:@"PP311"] ||
//                [request.reqConfig.name isEqualToString:@"PP312"] ||
//                [request.reqConfig.name isEqualToString:@"PP313"] ||
                
//                [request.reqConfig.name isEqualToString:@"PP401"] ||
//                [request.reqConfig.name isEqualToString:@"PP402"] ||
//                [request.reqConfig.name isEqualToString:@"PP403"] ||
//                [request.reqConfig.name isEqualToString:@"PP404"] ||
                
//                [request.reqConfig.name isEqualToString:@"PP411"] ||
//                [request.reqConfig.name isEqualToString:@"PP412"] ||
//                [request.reqConfig.name isEqualToString:@"PP413"] ||
//                [request.reqConfig.name isEqualToString:@"PP414"]
                
                
                ) {
                DLogI(@"Sent sync Request to URL(%@) - %@\n\n", request.reqConfig.name, request.reqUrl);
            }
            else {
                DLogI(@"Sent sync Request to URL(%@) - %@\n\nBody:\n%@", request.reqConfig.name, request.reqUrl, request.body );
            }
            
            request.attemptCount++;
            NSData* responseData = [NSURLConnection sendSynchronousRequest:httpRequest returningResponse:&response error:&error_];
            int responseStatusCode = (int)[(NSHTTPURLResponse*)response statusCode];
            
            if ( [error_ code] || responseStatusCode != HTTP_OK )
            {
                NSString *msgFmt = @"HTTP %@ Error - Status Code %d\n %@ %d %@\nresponse data - %@\n for URL - %@\nrequest body:\n%@";
                NSString *message = [NSString stringWithFormat:msgFmt, request.reqConfig.httpMethod,responseStatusCode,[error_ domain], [error_ code],
                                     [error_ localizedDescription],
                                     [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding],
                                     request.reqUrl,
                                     request.body];
                
                NSLog(@"%@", message);
                
            } else {
                
                request.receivedData      = (NSMutableData*)responseData;
                NSMutableString *respStr_ = [[[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding] mutableCopy];
                NSString        *respStr  = [respStr_ stringByReplacingOccurrencesOfString:@"=" withString:@":"];
                
                NSLog(@"Received response from server: %@, response body: %@", request.reqUrl , respStr);
                
                result = getJSONObj(respStr);
                
            }
            
        }
        dispatch_async(dispatch_get_main_queue(), ^{
        if ([UIApplication sharedApplication].isNetworkActivityIndicatorVisible) {
            // Turn the indicator off
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        }
        });
        
        dispatch_async(dispatch_get_main_queue(), ^{
            // 이 블럭은 메인스레드(UI)에서 실행된다.
            if(request.listener!=nil) {
                [request.listener onRequestCompleted:request];
                //return YES;
//                return @"";
            }
            
//            return result;
        });
        
    });
    return YES;
}

-(void) cancelRequest:(TXRequestObj*)request {
    
    for ( NSValue *connection in [self->pendingRequests keyEnumerator] ) {
        TXRequestObj *creq = [self->pendingRequests objectForKey:connection];
        if ( request == creq ) {
            NSURLConnection* urlConn = [connection nonretainedObjectValue];
            [urlConn cancel];
            return;
        }
    }
}

-(void) cancelRequestsByName:(NSString*)requestName {
    for ( NSValue *connection in [self->pendingRequests keyEnumerator] ) {
        TXRequestObj *creq = [self->pendingRequests objectForKey:connection];
        if ( [creq.reqConfig.name isEqualToString:requestName] ) {
            NSURLConnection* urlConn = [connection nonretainedObjectValue];
            [urlConn cancel];
        }
    }
}

-(BOOL)sendAsyncRequest:(TXRequestObj*)request
{
    if ( request != nil )
    {
        NSMutableURLRequest *httpRequest = [request createHTTPRequest];
        request.attemptCount++;
        
        if (request.reqConfig.isRedis) {
            NSMutableDictionary *dic;;
            
            if ( request.body && [request.body length] > 0)
            {
                dic = getJSONObj(request.body);
            }
            if ( request.reqUrl && [request.reqUrl length] > 0)
            {
                [dic addEntriesFromDictionary:[Utils getDataOfQueryString:request.reqUrl]];
            }
            
            //[RedisSingleton redisPush:request dic:dic];
            return YES;
        }
        
        //dispatch_semaphore_t holdOn = dispatch_semaphore_create(0);
        if ( httpRequest != nil )
        {
            __block NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithRequest:httpRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                
                if (error) {
                    //  handle error
                    TXRequestObj* request = [self requestForSession:task];
                    [self disposeSessionJob:task];
                    
                    // Toggle the network activity indicator
                    // Check if the indicator is on
                    dispatch_async(dispatch_get_main_queue(), ^{
                    if ([UIApplication sharedApplication].isNetworkActivityIndicatorVisible) {
                        // Turn the indicator off
                        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                    }
                    });
                    
                    NSString *errorStr = [NSString stringWithFormat:@"%@ - %@ - %@ %ld %@", @"HTTP Get Error", request.reqConfig.name, [error domain], (long)[error code], [error localizedDescription]];
#ifdef _DEBUG_ACTIVITY
                    
                    [self performSelectorOnMainThread:@selector(postNotification:) withObject:[NSDictionary dictionaryWithObjectsAndKeys:@"EDQueueJobDidSucceed", @"name", errorStr, @"data", nil] waitUntilDone:false];
#endif
                    
                    if ([request.reqConfig.name containsString:@"ByCoordinates"]) {
                        [Utils setStateUIInfo:UI_STATE_TRIP_LINE value:UI_STATE_STEP_FIN oldvalue:@""];
                    }
                    
                    //log error here if the request had no error handler
                    if ([error code]) {
                        DLogE(@"%@", errorStr);
                    }
                    if ([error code] && request.listener == nil ) {
                        DLogE(@"%@", errorStr);
                    } else if (request.listener!=nil) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [request.listener onFail:request error:[TXError error:(int)[error code] message:[error domain] description:[error localizedDescription]]];
                        });
                    }
                }
                else
                {
                    int statusCode = (int)[(NSHTTPURLResponse *)response statusCode];
                    if (statusCode != HTTP_OK)
                    {
                        TXRequestObj* request = [self requestForSession:task];
                        if( statusCode == HTTP_UNAUTHORIZED ) {
                            
                        }
                        
                        NSString* statusMsg	= [NSHTTPURLResponse localizedStringForStatusCode:statusCode];

                        NSString *errorStr = [NSString stringWithFormat:@"Async Req To Url [%d] - %@ received response with error: %@", statusCode, request.reqConfig.name, statusMsg];
#ifdef _DEBUG_ACTIVITY
                        
                        [self performSelectorOnMainThread:@selector(postNotification:) withObject:[NSDictionary dictionaryWithObjectsAndKeys:@"EDQueueJobDidSucceed", @"name", errorStr, @"data", nil] waitUntilDone:false];
#endif
                        if (request.listener == nil ) {
                            DLogE(@"%@", errorStr);
                        }
                        else {
                            DLogE(@"%@", errorStr);
                            
                            request.statusCode = statusCode;
                            request.statusMsg = statusMsg;
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                [request.listener onFail:request error:[TXError error:statusCode message:statusMsg description:[NSString stringWithFormat:@"AMDCHttpReqManager, didReceiveResponse, url: %@", request.reqUrl]]];
                            });
                        }
                        
                        if ([request.reqConfig.name containsString:@"ByCoordinates"]) {
                            [Utils setStateUIInfo:UI_STATE_TRIP_LINE value:UI_STATE_STEP_FIN oldvalue:@""];
                        }
                        
                        //return NO;
                        //[connection cancel];
                        [self disposeSessionJob:task];
                    }
                    else {
                        TXRequestObj* request = [self requestForSession:task];
                        // 받은 header들을 dictionary형태로 받고
                        request.responseParameters = [(NSHTTPURLResponse *)response allHeaderFields];
                        
                        request.receivedData = (NSMutableData*)data;
                        
                        [self disposeSessionJob:task];
                        
                        // Toggle the network activity indicator
                        // Check if the indicator is on
                        dispatch_async(dispatch_get_main_queue(), ^{
                        if ([UIApplication sharedApplication].isNetworkActivityIndicatorVisible) {
                            // Turn the indicator off
                            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                        }
                        });
                        
                        //DLogI(@"receive Header - : %@",request.responseParameters);
                        
                        //UR601
                        //PP602
                        
                        if ([request.reqConfig.name isEqualToString:@"UR601"] ||
                            [request.reqConfig.name isEqualToString:@"UR602"] ||
                            [request.reqConfig.name isEqualToString:@"UR603"] ||
                            [request.reqConfig.name isEqualToString:@"UR621"] ||
                            
                            [request.reqConfig.name isEqualToString:@"PP601"] ||
                            [request.reqConfig.name isEqualToString:@"PP602"] ||
                            [request.reqConfig.name isEqualToString:@"PP603"] ||
                            [request.reqConfig.name isEqualToString:@"PP605"] ||
                            [request.reqConfig.name isEqualToString:@"PP606"] ||
                            [request.reqConfig.name isEqualToString:@"PP628"] ||
                            [request.reqConfig.name isEqualToString:@"PP629"]
                            
                            ) {
                            //DLogI(@"receive Data(%@) - :",request.reqConfig.name);
                        }
                        else {
                            id jsonResponse = [NSJSONSerialization JSONObjectWithData:request.receivedData options:0 error:nil];
                            DLogI(@"receive Data(%@) - : %@",request.reqConfig.name,[jsonResponse description]);
                        }
                        
                        //DLogI(@"Async Req To Url - %@ received response from : %@", request.reqUrl, @"Success ...");
                        
                        if(request.listener!=nil) {
                            [request.listener onRequestCompleted:request];
                        }
                    }
                }
            }];
            [self addPendingSession:request withConnection:task];
            //fire the request
            [task resume];
        }
        
        // Toggle the network activity indicator
        // Check if the indicator is on
        dispatch_async(dispatch_get_main_queue(), ^{
        if (![UIApplication sharedApplication].isNetworkActivityIndicatorVisible) {
            
            // Turn the indicator on
            [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
            
        }
        });
        
        if ([request.reqConfig.name isEqualToString:@"UR601"] ||
            [request.reqConfig.name isEqualToString:@"UR602"] ||
            [request.reqConfig.name isEqualToString:@"UR603"] ||
            [request.reqConfig.name isEqualToString:@"UR621"] ||
            
            [request.reqConfig.name isEqualToString:@"PP601"] ||
            [request.reqConfig.name isEqualToString:@"PP602"] ||
            [request.reqConfig.name isEqualToString:@"PP603"] ||
            [request.reqConfig.name isEqualToString:@"PP605"] ||
            [request.reqConfig.name isEqualToString:@"PP606"] ||
            [request.reqConfig.name isEqualToString:@"PP628"] ||
            [request.reqConfig.name isEqualToString:@"PP629"]
            
//            [request.reqConfig.name isEqualToString:@"PP301"] ||
//            [request.reqConfig.name isEqualToString:@"PP302"] ||
//            [request.reqConfig.name isEqualToString:@"PP303"] ||
//            [request.reqConfig.name isEqualToString:@"PP311"] ||
//            [request.reqConfig.name isEqualToString:@"PP312"] ||
//            [request.reqConfig.name isEqualToString:@"PP313"] ||
//
//            [request.reqConfig.name isEqualToString:@"PP401"] ||
//            [request.reqConfig.name isEqualToString:@"PP402"] ||
//            [request.reqConfig.name isEqualToString:@"PP403"] ||
//            [request.reqConfig.name isEqualToString:@"PP404"] ||
//
//            [request.reqConfig.name isEqualToString:@"PP411"] ||
//            [request.reqConfig.name isEqualToString:@"PP412"] ||
//            [request.reqConfig.name isEqualToString:@"PP413"] ||
//            [request.reqConfig.name isEqualToString:@"PP414"]
            
            
            ) {
            DLogI(@"Sent sync Request to URL(%@) - %@\n\n", request.reqConfig.name, request.reqUrl);
        }
        else {
            DLogI(@"Sent sync Request to URL(%@) - %@\n\nBody:\n%@", request.reqConfig.name, request.reqUrl, request.body );
        }
        
        //dispatch_semaphore_wait(holdOn, DISPATCH_TIME_FOREVER);
        
        return YES;
    }
    return NO;
}

- (void)postNotification:(NSDictionary *)object
{
    [[NSNotificationCenter defaultCenter] postNotificationName:[object objectForKey:@"name"] object:[object objectForKey:@"data"]];
}

-(BOOL)uploadAttachmentAsync: (TXRequestObj *)request {
    return [self sendAsyncRequest:request];
}

-(BOOL)downloadAttachementAsync: (TXRequestObj *)request {
    return [self sendAsyncRequest:request];
}


-(void)addPendingRedis:(TXRequestObj*) request withConnection:(NSString*)connection{
    NSValue* connVal = [NSValue valueWithNonretainedObject:connection];
    [self->pendingRequests setObject:request forKey:connVal];
}

-(void)disposeRedisJob:(NSString*)connection {
    
    NSValue* connVal = [NSValue valueWithNonretainedObject:connection];
    
    DLogI(@"Removed request form pending list - %@", ((TXRequestObj*)[self->pendingRequests objectForKey:connVal]).reqUrl);
    
    [self->pendingRequests removeObjectForKey:connVal];
}

-(void)addPendingRequest:(TXRequestObj*) request withConnection:(NSURLConnection*)connection{
    NSValue* connVal = [NSValue valueWithNonretainedObject:connection];
    [self->pendingRequests setObject:request forKey:connVal];
}

-(void)disposeRequestJob:(NSURLConnection*)connection {
    
    NSValue* connVal = [NSValue valueWithNonretainedObject:connection];
    
    DLogI(@"Removed request form pending list - %@", ((TXRequestObj*)[self->pendingRequests objectForKey:connVal]).reqUrl);
    
    [self->pendingRequests removeObjectForKey:connVal];
}

-(void)addPendingSession:(TXRequestObj*) request withConnection:(NSURLSessionTask*)connection{
    NSValue* connVal = [NSValue valueWithNonretainedObject:connection];
    [self->pendingRequests setObject:request forKey:connVal];
}

-(void)disposeSessionJob:(NSURLSessionTask*)connection {
    
    NSValue* connVal = [NSValue valueWithNonretainedObject:connection];
    
    DLogI(@"Removed request form pending list - %@", ((TXRequestObj*)[self->pendingRequests objectForKey:connVal]).reqUrl);
    
    [self->pendingRequests removeObjectForKey:connVal];
}


-(int)getPendingRequestCount {
    return (int)[self->pendingRequests count];
}

#pragma mark delegate methods for nsurl connection

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError*)error {
    
    TXRequestObj* request = [self requestForConnection:connection];
	[self disposeRequestJob:connection];
    
    // Toggle the network activity indicator
    // Check if the indicator is on
    dispatch_async(dispatch_get_main_queue(), ^{
    if ([UIApplication sharedApplication].isNetworkActivityIndicatorVisible) {
        // Turn the indicator off
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    }
    });
    
    //log error here if the request had no error handler
    if ([error code]) {
        DLogE(@"%@ - %@ - %@ %ld %@", @"HTTP Get Error", request.reqConfig.name, [error domain], (long)[error code], [ error localizedDescription]);
    }
    if ([error code] && request.listener == nil ) {
        DLogE(@"%@ - %@ - %@ %ld %@", @"HTTP Get Error", request.reqConfig.name, [error domain], (long)[error code], [ error localizedDescription]);
    } else if (request.listener!=nil) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [request.listener onFail:request error:[TXError error:(int)[error code] message:[error domain] description:[error localizedDescription]]];
        });
    }
}

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    
	int statusCode = (int)[(NSHTTPURLResponse *)response statusCode];
	if (statusCode != HTTP_OK)
	{
        TXRequestObj* request = [self requestForConnection:connection];
        if( statusCode == HTTP_UNAUTHORIZED ) {
           
        }
        
        NSString* statusMsg	= [NSHTTPURLResponse localizedStringForStatusCode:statusCode];
        
		if (request.listener == nil ) {
			DLogE(@"Async Req To Url [%d] - %@ received response with error: %@", statusCode, request.reqConfig.name, statusMsg);
		}
		else {
            DLogE(@"Async Req To Url [%d] - %@ received response with error: %@", statusCode, request.reqConfig.name, statusMsg);
            
            request.statusCode = statusCode;
            request.statusMsg = statusMsg;
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                    [request.listener onFail:request error:[TXError error:statusCode message:statusMsg description:[NSString stringWithFormat:@"AMDCHttpReqManager, didReceiveResponse, url: %@", request.reqUrl]]];
            });
			
		}
        
		[connection cancel];
		[self disposeRequestJob:connection];
	}
    
    TXRequestObj* request = [self requestForConnection:connection];
    // 받은 header들을 dictionary형태로 받고
    request.responseParameters = [(NSHTTPURLResponse *)response allHeaderFields];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    TXRequestObj* request = [self requestForConnection:connection];
    [request.receivedData appendData:data];
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection {
    
    TXRequestObj* request = [self requestForConnection:connection];
	[self disposeRequestJob:connection];
    
    // Toggle the network activity indicator
    // Check if the indicator is on
    dispatch_async(dispatch_get_main_queue(), ^{
    if ([UIApplication sharedApplication].isNetworkActivityIndicatorVisible) {
        // Turn the indicator off
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    }
    });
    
    //DLogI(@"receive Header - : %@",request.responseParameters);
    
    //UR601
    //PP602
    
    if ([request.reqConfig.name isEqualToString:@"UR601"] ||
        [request.reqConfig.name isEqualToString:@"UR602"] ||
        [request.reqConfig.name isEqualToString:@"UR603"] ||
        [request.reqConfig.name isEqualToString:@"UR621"] ||
        
        [request.reqConfig.name isEqualToString:@"PP601"] ||
        [request.reqConfig.name isEqualToString:@"PP602"] ||
        [request.reqConfig.name isEqualToString:@"PP603"] ||
        [request.reqConfig.name isEqualToString:@"PP605"] ||
        [request.reqConfig.name isEqualToString:@"PP606"] ||
        [request.reqConfig.name isEqualToString:@"PP628"] ||
        [request.reqConfig.name isEqualToString:@"PP629"]
        
//        [request.reqConfig.name isEqualToString:@"PP301"] ||
//        [request.reqConfig.name isEqualToString:@"PP302"] ||
//        [request.reqConfig.name isEqualToString:@"PP303"] ||
//        [request.reqConfig.name isEqualToString:@"PP311"] ||
//        [request.reqConfig.name isEqualToString:@"PP312"] ||
//        [request.reqConfig.name isEqualToString:@"PP313"] ||
//
//        [request.reqConfig.name isEqualToString:@"PP401"] ||
//        [request.reqConfig.name isEqualToString:@"PP402"] ||
//        [request.reqConfig.name isEqualToString:@"PP403"] ||
//        [request.reqConfig.name isEqualToString:@"PP404"] ||
//
//        [request.reqConfig.name isEqualToString:@"PP411"] ||
//        [request.reqConfig.name isEqualToString:@"PP412"] ||
//        [request.reqConfig.name isEqualToString:@"PP413"] ||
//        [request.reqConfig.name isEqualToString:@"PP414"]

        
        ) {
        //DLogI(@"receive Data(%@) - :",request.reqConfig.name);
    }
    else {
        id jsonResponse = [NSJSONSerialization JSONObjectWithData:request.receivedData options:0 error:nil];
        DLogI(@"receive Data(%@) - : %@",request.reqConfig.name,[jsonResponse description]);
    }
    
    //DLogI(@"Async Req To Url - %@ received response from : %@", request.reqUrl, @"Success ...");
    
    //dispatch_async(dispatch_get_main_queue(), ^{
        
        if(request.listener!=nil) {
            [request.listener onRequestCompleted:request];
        }
    //});
}

- (BOOL)connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace {
	return [protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust];
}

- (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
    
	if ([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust])
		if ([trustedHosts containsObject:challenge.protectionSpace.host])
			[challenge.sender useCredential:[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust] forAuthenticationChallenge:challenge];
    
	[challenge.sender continueWithoutCredentialForAuthenticationChallenge:challenge];
}

- (NSURLRequest *)connection:(NSURLConnection *)connection
 			 willSendRequest:(NSURLRequest *)request
 			redirectResponse:(NSURLResponse *)redirectResponse {
 	return request;
}

@end
