//
//  TXModelBase.m
//  Taxi
//

//

#import "TXModelBase.h"
#import "utils.h"
#import "TXUserModel.h"
#import "TXAppDelegate.h"
#import "LGAlertView.h"
#import "SVProgressHUD.h"

@implementation TXResponseDescriptor

-(id) init:(BOOL) succcess_ rlt:(int)rlt_ code:(NSString *) code_ error:(NSString *) error_ atoken:(id) atoken_ {
    if(self = [super init]) {
        self.success = succcess_;
        self.rlt     = rlt_;
        self.code    = code_;
        self.error   = error_;
        self.atoken  = atoken_;
    }
    
    return self;
}


+(id) create:(BOOL) succcess_ rlt:(int)rlt_ code:(NSString *) code_ error:(NSString *) error_ atoken:(id) atoken_ {
    return [[self alloc] init:succcess_ rlt:rlt_ code:code_ error:error_ atoken:atoken_];
}

+(id) create:(BOOL) succcess_ rlt:(int)rlt_ code:(NSString *) code_ {
    return [self create:succcess_ rlt:rlt_ code:code_ error:nil atoken:nil];
}

@end

@interface TXModelBase() {
    TXApp* app;
    TXAppDelegate *appDelegate;
}

@end

@implementation TXModelBase

-(id)init {
    
    if(self = [super init]) {
        self->httpMgr     = [TXHttpRequestManager instance];
        self->app         = [TXApp instance];
        [self addEventListener:self forEvent:TXEvents.HTTPREQUESTCOMPLETED eventParams:nil];
        [self addEventListener:self forEvent:TXEvents.HTTPREQUESTFAILED eventParams:nil];
        [self addEventListener:self forEvent:TXEvents.NULLHTTPRESPONSE eventParams:nil];
    }
    appDelegate = (TXAppDelegate *)[[UIApplication sharedApplication] delegate];
    return self;
}

-(TXApp *)getApp {
    return self->app;
}

-(void)onCleanUseqSession {
//    NSLog(@"onCleanSession");
    
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    
    //TXAppDelegate *appDelegate = (TXAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    appDelegate.imgProfileRider = nil;
    appDelegate.dicRider = nil;
    appDelegate.dicDrive = nil;
    
    appDelegate.pictureSizeRider = nil;
    
    [[[TXApp instance] getSettings] setFDKeychain:SettingsConst.CryptoKeys.USEQ value:@""];
    [[[TXApp instance] getSettings] setFDKeychain:SettingsConst.CryptoKeys.OSEQ value:@""];
}

-(void)onCleanPseqSession {
//    NSLog(@"onCleanSession");
    
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    
    [[[TXApp instance] getSettings] setDriveMode:DRIVE_MODE_DRIVING];
    //TXAppDelegate *appDelegate = (TXAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    appDelegate.imgProfileDriver = nil;
    appDelegate.imgVehicleDriver = nil;
    appDelegate.dicDriver = nil;
    appDelegate.dicDrive = nil;
    appDelegate.becomePseq = @"";
    appDelegate.becomeVseq = @"";
    
    appDelegate.pictureSizeDriver = nil;
    appDelegate.pictureSizeVehicle = nil;
    
    [[[TXApp instance] getSettings] setFDKeychain:SettingsConst.CryptoKeys.PSEQ value:@""];
    [[[TXApp instance] getSettings] setFDKeychain:SettingsConst.CryptoKeys.VSEQ value:@""];
    [[[TXApp instance] getSettings] setFDKeychain:SettingsConst.CryptoKeys.OSEQ value:@""];
}

-(void)onCleanSession {
    NSLog(@"onCleanSession");
    
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    [locMgr resetMgr];
    
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    
    //TXAppDelegate *appDelegate = (TXAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [[[TXApp instance] getSettings] setFDKeychain:@"NEWS_NSEQ" value:@""];
    
    ALRIM_DELETE_ALL;
    
#ifdef _DRIVER_MODE
    NSString *seq = [Utils getBCDSTEP];
    if (seq != nil) {
        [[[TXApp instance] getSettings] setFDKeychain:seq value:@"0"];
    }
#endif
    
    [self onCleanUseqSession];
    [self onCleanPseqSession];
    
    appDelegate.isDriverMode = NO;
#ifdef _DRIVER_MODE
        appDelegate.isDriverMode = YES;
#endif
    appDelegate.isCanDriverMode = NO;
    appDelegate.isBecomeMode = NO;
    appDelegate.api_config = nil;
    
    // ui 초기화
    appDelegate.uistate = 0;
    appDelegate.uistate2 = 0;
    
    NSLog(@"appDelegate.isBecomeMode:%d",appDelegate.isBecomeMode);
    
    [[self->app getSettings] setUserId:nil];
    [[self->app getSettings] setUserTelno:nil];
    [[self->app getSettings] setUserToken:nil];
}

-(void)dismissAlertView:(LGAlertView*)alert
{
    [alert dismissAnimated:YES completionHandler:nil];
    //[alert dismissWithClickedButtonIndex:0 animated:YES];
}

-(void)updateRiderState:(int)newstate {
    //TXAppDelegate *appDelegate = (TXAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (appDelegate.ustate != 0) {
        if (appDelegate.ustate != newstate) {
            appDelegate.ustateold = appDelegate.ustate;
            
            appDelegate.ustate = newstate;
            appDelegate.isChangeState = YES;
        }
    }
    else {
        appDelegate.ustate = newstate;
        appDelegate.isChangeState = YES;
    }
    
    if (newstate == 0) {
        appDelegate.ustateold = 0;
        appDelegate.ustate2old = 0;
    }
}

-(void)updateRiderState2:(int)newstate {
    //TXAppDelegate *appDelegate = (TXAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (appDelegate.ustate2 != 0) {
        if (appDelegate.ustate2 != newstate) {
            appDelegate.ustate2old = appDelegate.ustate2;
            
            appDelegate.ustate2 = newstate;
            appDelegate.isChangeState = YES;
        }
    }
    else {
        appDelegate.ustate2 = newstate;
        appDelegate.isChangeState = YES;
    }
    
    if (newstate == 0) {
        appDelegate.ustateold = 0;
        appDelegate.ustate2old = 0;
    }
}

-(void)updateRiderState3:(int)newstate {
    //TXAppDelegate *appDelegate = (TXAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    appDelegate.uustate = newstate;
}

-(void)updateDriverState:(int)newstate {
    //TXAppDelegate *appDelegate = (TXAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (appDelegate.pstate != 0) {
        if (appDelegate.pstate != newstate) {
            appDelegate.pstateold = appDelegate.pstate;
            
            appDelegate.pstate = newstate;
            appDelegate.isChangeState = YES;
        }
    }
    else {
        appDelegate.pstate = newstate;
        appDelegate.isChangeState = YES;
    }
    
    if (newstate == 0) {
        appDelegate.pstateold = 0;
        appDelegate.pstate2old = 0;
    }
}


-(void)updateDriverState2:(int)newstate {
    //TXAppDelegate *appDelegate = (TXAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (appDelegate.pstate2 != 0) {
        if (appDelegate.pstate2 != newstate) {
            appDelegate.pstate2old = appDelegate.pstate2;
            
            appDelegate.pstate2 = newstate;
            appDelegate.isChangeState = YES;
        }
    }
    else {
        appDelegate.pstate2 = newstate;
        appDelegate.isChangeState = YES;
    }
    
    if (newstate == 0) {
        appDelegate.pstateold = 0;
        appDelegate.pstate2old = 0;
    }
}

-(void)updateDriverState3:(int)newstate {
    //TXAppDelegate *appDelegate = (TXAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    appDelegate.ppstate = (int)newstate;
}

// 통신 응답 메시지 수신처리 부
-(void)onRequestCompleted:(id)object {
    
    TXRequestObj *request = (TXRequestObj*)object;

    NSDictionary *responseObj;

    if (
        [request.reqConfig.name isEqualToString:@"UR601"] ||
        [request.reqConfig.name isEqualToString:@"UR602"] ||
        [request.reqConfig.name isEqualToString:@"UR603"] ||
        [request.reqConfig.name isEqualToString:@"UR621"] ||
        [request.reqConfig.name isEqualToString:@"PP601"] ||
        [request.reqConfig.name isEqualToString:@"PP602"] ||
        [request.reqConfig.name isEqualToString:@"PP603"] ||
        [request.reqConfig.name isEqualToString:@"PP605"] ||
        [request.reqConfig.name isEqualToString:@"PP628"] ||
        [request.reqConfig.name isEqualToString:@"PP629"]
        ) {
        //

        NSString *picture = [request.receivedData base64EncodedStringWithOptions:0];
        responseObj = @{
                        API_JSON.ResponseDescriptor.RLT: @"1",
                        API_JSON.ResponseDescriptor.ERROR: @"",
                        @"picture":picture
                        };
        NSString *dataString = getDicStr(responseObj);
        
        request.receivedData = (NSMutableData*)[dataString dataUsingEncoding:NSUTF8StringEncoding];
    }
    else {
        responseObj = getJSONObj([[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding]);
    }
    
    [self fireEvent:[self requestCompleted:object reqponse:responseObj]];
}

-(TXEvent*)requestCompleted:(TXRequestObj*)object reqponse:(NSDictionary*)responseObj {
    
    TXEvent *event = nil;
    TXRequestObj *request = object;
    
    //NSDictionary *responseObj = getJSONObj([[NSString alloc] initWithData:request.receivedData encoding:NSUTF8StringEncoding]);
    
    //TXAppDelegate *appDelegate = (TXAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (![Utils isDictionary:responseObj]) {
        event = [TXEvent createEvent:TXEvents.NULLHTTPRESPONSE eventSource:self eventProps:nil];
        
        return event;
    }
    
    if(responseObj!=nil) {
        
        int  rlt    = [[responseObj objectForKey:API_JSON.ResponseDescriptor.RLT] intValue];
        id   error  = [responseObj objectForKey:API_JSON.ResponseDescriptor.ERROR];
        id   code   = request.reqConfig.name; // x-service-header.mid
        id   atoken = [responseObj objectForKey:API_JSON.ResponseDescriptor.DATOKEN]; // x-service-token.atoken
        if (![atoken isKindOfClass:[NSNull class]] && atoken) {
            [[[TXApp instance] getSettings] setUserToken:atoken];
        }
        id  alert    = [responseObj objectForKey:@"alert"];
        
        NSLog(@"error:%@",error);
        
        if (request.reqConfig.useCache) {
            BOOL success = NO;
            if (rlt>0) {
                success = YES;
            }
            //NSLog(@"useCache!!");
            TXResponseDescriptor *descriptor = [TXResponseDescriptor create:success rlt:rlt code:code error:error atoken:atoken];
            
            event = [TXEvent createEvent:TXEvents.HTTPREQUESTCOMPLETED
                             eventSource:self
                              eventProps:@{
                                           TXEvents.Params.DESCRIPTOR : descriptor,
                                           TXEvents.Params.REQUEST    : request
                                           }];
            

            return event;
        }
        
        //if (alert && [alert length] > 0 && rlt <= -100) {
        if (rlt <= -100) {
            NSString *alertMessage = error;
            if (alert && [alert length] > 0) {
                alertMessage = alert;
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                if ([SVProgressHUD isVisible])
                    [SVProgressHUD dismiss];
                LGAlertView *alertView = [[LGAlertView alloc] initWithTitle:LocalizedStr(@"String.Alert")
                                                                    message:alertMessage
                                                                      style:LGAlertViewStyleAlert
                                                               buttonTitles:@[LocalizedStr(@"Button.OK")]
                                                          cancelButtonTitle:nil
                                                     destructiveButtonTitle:nil
                                                              actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
                                                              }
                                                              cancelHandler:^(LGAlertView *alertView) {
                                                              }
                                                         destructiveHandler:^(LGAlertView *alertView) {
                                                         }];
                
                [Utils initAlertButtonColor:alertView];
                [alertView showAnimated:YES completionHandler:^(void)
                 {
                     [self performSelector:@selector(dismissAlertView:) withObject:alertView afterDelay:5.0];
                     
                 }];
            });
            
            // 다른단말기 이중로그인일때 로그아웃 처리
            if (rlt == -206) {
                // login시에는 에러로 처리한다.
                if ([code isEqualToString:@"UR110"] || [code isEqualToString:@"PP110"]) {
                    //
                }
                else {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_LOGOUT object:[NSDictionary dictionaryWithObjectsAndKeys:@"logout", @"name", nil] userInfo:[NSDictionary dictionaryWithObjectsAndKeys:@"logout", @"name", nil]];
                    });
                    return nil;
                }
            }
            
            if (appDelegate.isDriverMode) {
                NSDictionary *pdevice = [responseObj valueForKey:@"provider_device"];
                [self updateDriverState:[[pdevice valueForKey:@"state"] intValue]];
            }
            
            
            int orlt = rlt;
            rlt = -1000;
            
            BOOL success = NO;
            if (rlt>0) {
                success = YES;
            }
            
            TXResponseDescriptor *descriptor = [TXResponseDescriptor create:success rlt:rlt code:code error:error atoken:atoken];
            descriptor.orgrlt = orlt;
            
            event = [TXEvent createEvent:TXEvents.HTTPREQUESTCOMPLETED
                             eventSource:self
                              eventProps:@{
                                           TXEvents.Params.DESCRIPTOR : descriptor,
                                           TXEvents.Params.REQUEST    : request
                                           }];
            
            
            return event;
        }
        
        // 1. state 초기화처리
        if ([code isEqualToString:@"UR114"]
            || [code isEqualToString:@"UD130"]
            || [code isEqualToString:@"UD131"]
            || [code isEqualToString:@"UD230"]
            ) {
            appDelegate.ustate2 = 0;
            appDelegate.ustate2old = 0;
            //[appDelegate resetState];
        }
        else if ([code isEqualToString:@"PP114"]
                 || [code isEqualToString:@"PD135"]
                 || [code isEqualToString:@"PD130"]
                 || [code isEqualToString:@"PD131"]
                 || [code isEqualToString:@"PD232"]
                 || [code isEqualToString:@"PD330"]
                 ) {
            appDelegate.pstate2 = 0;
            appDelegate.pstate2old = 0;
        }
        
        // state를 update한다.
        NSDictionary *udevice = nil;
        NSDictionary *pdevice = nil;
        
#ifdef _DRIVER_MODE
        appDelegate.isCanDriverMode = YES;
        appDelegate.isDriverMode = YES;
#else
        appDelegate.isCanDriverMode = NO;
        appDelegate.isDriverMode = NO;
#endif
        // 2. user state 처리(seq, state)
        udevice = [responseObj valueForKey:@"user_state"];
        if ([Utils isDictionary:udevice]) {
            // useq
            [[[TXApp instance] getSettings] setFDKeychain:SettingsConst.CryptoKeys.USEQ value:[udevice valueForKey:SettingsConst.CryptoKeys.USEQ]];
            [[[TXApp instance] getSettings] setFDKeychain:SettingsConst.CryptoKeys.USEQD value:[udevice valueForKey:SettingsConst.CryptoKeys.USEQ]];
            if ([udevice valueForKey:SettingsConst.CryptoKeys.OSEQ]) {
                [[[TXApp instance] getSettings] setFDKeychain:SettingsConst.CryptoKeys.OSEQ value:[udevice valueForKey:SettingsConst.CryptoKeys.OSEQ]];
            }
            if ([udevice valueForKey:SettingsConst.CryptoKeys.PSEQ]) {
                [[[TXApp instance] getSettings] setFDKeychain:SettingsConst.CryptoKeys.PSEQ value:[udevice valueForKey:SettingsConst.CryptoKeys.PSEQ]];
                [[[TXApp instance] getSettings] setFDKeychain:SettingsConst.CryptoKeys.PSEQD value:[udevice valueForKey:SettingsConst.CryptoKeys.PSEQ]];
            }
            
            // state
            [self updateRiderState:[[udevice valueForKey:@"udevice"] intValue]];
            [self updateRiderState3:[[udevice valueForKey:@"ustate"] intValue]];
            if (![Utils isNill:[udevice objectForKey:@"ostate"]]) {
                [self updateRiderState2:[[udevice valueForKey:@"ostate"] intValue]];
            }
            else {
                [self updateRiderState2:0];
            }
        }
        
        pdevice = [responseObj valueForKey:@"provider_state"];
        if ([Utils isDictionary:pdevice]) {
            // useq
            [[[TXApp instance] getSettings] setFDKeychain:SettingsConst.CryptoKeys.PSEQ value:[pdevice valueForKey:SettingsConst.CryptoKeys.PSEQ]];
            [[[TXApp instance] getSettings] setFDKeychain:SettingsConst.CryptoKeys.PSEQD value:[pdevice valueForKey:SettingsConst.CryptoKeys.PSEQ]];
            if ([pdevice valueForKey:SettingsConst.CryptoKeys.OSEQ]) {
                [[[TXApp instance] getSettings] setFDKeychain:SettingsConst.CryptoKeys.OSEQ value:[pdevice valueForKey:SettingsConst.CryptoKeys.OSEQ]];
            }
            if ([pdevice valueForKey:SettingsConst.CryptoKeys.USEQ]) {
                [[[TXApp instance] getSettings] setFDKeychain:SettingsConst.CryptoKeys.USEQ value:[pdevice valueForKey:SettingsConst.CryptoKeys.USEQ]];
                [[[TXApp instance] getSettings] setFDKeychain:SettingsConst.CryptoKeys.USEQD value:[pdevice valueForKey:SettingsConst.CryptoKeys.USEQ]];
            }
            
            // state
            [self updateDriverState:[[pdevice valueForKey:@"pdevice"] intValue]];
            [self updateDriverState3:[[pdevice valueForKey:@"pstate"] intValue]];
            if (![Utils isNill:[pdevice objectForKey:@"ostate"]]) {
                [self updateDriverState2:[[pdevice valueForKey:@"ostate"] intValue]];
            }
            else {
                [self updateDriverState2:0];
            }
            
            // drive모드
            if (appDelegate.pstate >= 220) {
                appDelegate.isDriverMode = YES;
            }
        }
        
        // 3. device 처리
        pdevice = [responseObj valueForKey:@"provider_device"];
        if ([Utils isDictionary:pdevice]) {
            
            [[[TXApp instance] getSettings] setFDKeychain:SettingsConst.CryptoKeys.VSEQ value:[pdevice valueForKey:SettingsConst.CryptoKeys.VSEQ]];
        }
        
        // 2. mode별 로그인시에 정보를 저장한다.
        if ([code isEqualToString:@"UR116"] ||
            [code isEqualToString:@"UR110"] || [code isEqualToString:@"UR120"]) {
            //[appDelegate setDicRider:responseObj];
            appDelegate.isBecomeMode = NO;
            
            NSDictionary *user_config = [responseObj objectForKey:@"user_config"];
            if ([Utils isDictionary:user_config]) {
                appDelegate.appstoredriverurl = [user_config objectForKey:@"APP_DRIVERURL"];
                appDelegate.appstoreurl = [user_config objectForKey:@"APP_STOREURL"];
                appDelegate.appversion  = [user_config objectForKey:@"APP_VERSION"];
                appDelegate.paykey = [user_config objectForKey:@"APP_PAYKEY"];
            }
        }
        else if ([code isEqualToString:@"PP116"] || [code isEqualToString:@"PP136"] ||
                 [code isEqualToString:@"PD015"] || [code isEqualToString:@"PD035"] ||
                 [code isEqualToString:@"PP110"] || [code isEqualToString:@"PP120"]) {
            //[appDelegate setDicDriver:responseObj];
            
            NSDictionary *user_config = [responseObj objectForKey:@"provider_config"];
            if ([Utils isDictionary:user_config]) {
                appDelegate.appstoreurl = [user_config objectForKey:@"APP_STOREURL"];
                appDelegate.appversion  = [user_config objectForKey:@"APP_VERSION"];
                appDelegate.paykey = [user_config objectForKey:@"APP_PAYKEY"];
                
                [[[TXApp instance] getSettings] setDriveMode:user_config[@"APP_MODE"]];
            }
        }
        
        // 2.1 상태파악
        if (appDelegate.isDriverMode) {
            //pdevice = [responseObj valueForKey:@"provider_state"];
            pdevice = [responseObj valueForKey:@"provider"];
            /*
             1. 로그인시에 provider 가 있으면 driver일 수 있어서 provider.state > 0 driver 된 사람. 메뉴에는 "드라이버 모드"가 나온다.
             그러면 provider_device를 본다. provider_device.state >= 220 이면 운전중이다. 운전화면으로 이동한다.
             
             그런데 provider가 없거나 provider가 있고 provider.state = 0 이면 드라이버 승인이 않난 경우 "become a driver"가 메뉴로 나온다.
             
             provider.state < 0 면 reject된 경우는 메뉴에 메뉴가 안 본인다.
             */
            
            //1. 로그인시에 provider 가 있으면 driver일 수 있어서 provider.state > 0 driver 된 사람. 메뉴에는 "드라이버 모드"가 나온다.
            if ([Utils isDictionary:pdevice]) {

                if ([[pdevice valueForKey:@"state"] intValue]>=0) {
                    appDelegate.isCanDriverMode = YES;
                    appDelegate.isBecomeMode = YES;
                    
                    //pdevice = [responseObj valueForKey:@"provider_vehicles"];
                    if ([Utils isArray:[responseObj valueForKey:@"provider_vehicles"]]) {
                        appDelegate.isBecomeMode = NO;
                    }
                }
                // provider.state < 0 면 reject된 경우는 메뉴에 메뉴가 안 본인다.
                else {
                    appDelegate.isCanDriverMode = NO;
                    appDelegate.isBecomeMode = NO;
                }
            }
            else {
                if ([code isEqualToString:@"PP110"] || [code isEqualToString:@"PP120"]) {
                    appDelegate.isBecomeMode = YES;
                }
            }
        }
        
        if (appDelegate.isDriverMode) {
            pdevice = [responseObj valueForKey:@"provider_state"];
            if ([Utils isDictionary:pdevice]) {
                
                if ([[pdevice valueForKey:@"pstate"] intValue]==1) {
                    appDelegate.isCanDriverMode = NO;
                    appDelegate.isBecomeMode = NO;
                }
                else {
                    appDelegate.isCanDriverMode = YES;
                    appDelegate.isBecomeMode = YES;
                }
            }
            else {
                if ([code isEqualToString:@"PP110"] || [code isEqualToString:@"PP120"]) {
                    appDelegate.isBecomeMode = YES;
                }
            }
        }
        
        // 예상요금 처리
        if ([Utils isArray:responseObj[@"drive_estimates"]]) {
            NSMutableDictionary *mdic = [[NSMutableDictionary alloc] init];
            for (NSDictionary *dic in responseObj[@"drive_estimates"]) {
                NSString *key = [NSString stringWithFormat:@"estimate_%@",[dic[@"service"] lowercaseString]];
                [mdic setObject:dic forKey:key];
            }
            appDelegate.drive_estimates = [mdic copy];
        }
        
        // 5.1 state2 처리
        if ([Utils isDictionary:responseObj[@"drive_order"]]) {
            //[appDelegate setDicDrive:responseObj];
        }
        else if ([Utils isDictionary:responseObj[@"drive_pickup"]]) {
            //[appDelegate setDicDrive:responseObj];
        }
        else if ([Utils isDictionary:responseObj[@"drive_trip"]]) {
            //[appDelegate setDicDrive:responseObj];
        }
        
        // 6. picture size 확인필요
        //if (appDelegate.dicDrive != nil) {
        if ([Utils isDictionary:responseObj[@"drive_order"]] ||
            [Utils isDictionary:responseObj[@"drive_pickup"]] ||
            [Utils isDictionary:responseObj[@"drive_trip"]]) {
            // drive중이면..
            udevice = [responseObj valueForKey:@"user_device"];
            if ([Utils isDictionary:udevice]) {
                
                if ([udevice valueForKey:@"picture"] != (id)[NSNull null] &&
                    [udevice valueForKey:@"picture"] && [udevice valueForKey:@"picture"] != NULL
                    ) {
                    NSString *picsize = [NSString stringWithFormat:@"%@",[udevice valueForKey:@"picture"]];
                    appDelegate.pictureSizeRider = picsize;
                }
            }
            pdevice = [responseObj valueForKey:@"provider_device"];
            if ([Utils isDictionary:pdevice]) {
                
                if ([pdevice valueForKey:@"picture"] != (id)[NSNull null] &&
                    [pdevice valueForKey:@"picture"] && [pdevice valueForKey:@"picture"] != NULL
                    ) {
                    NSString *picsize = [NSString stringWithFormat:@"%@",[pdevice valueForKey:@"picture"]];
                    appDelegate.pictureSizeDriver = picsize;
                }
            }
        }
        else {
            // 초기
            if (appDelegate.isDriverMode) {
                pdevice = [responseObj valueForKey:@"provider_device"];
                if ([Utils isDictionary:pdevice]) {
                    
                    if ([pdevice valueForKey:@"picture"] != (id)[NSNull null] &&
                        [pdevice valueForKey:@"picture"] && [pdevice valueForKey:@"picture"] != NULL
                        ) {
                        NSString *picsize = [NSString stringWithFormat:@"%@",[pdevice valueForKey:@"picture"]];
                        appDelegate.pictureSizeDriver = picsize;
                    }
                }
            }
            else {
                udevice = [responseObj valueForKey:@"user_device"];
                if ([Utils isDictionary:udevice]) {
                    
                    if ([udevice valueForKey:@"picture"] != (id)[NSNull null] &&
                        [udevice valueForKey:@"picture"] && [udevice valueForKey:@"picture"] != NULL
                        ) {
                        NSString *picsize = [NSString stringWithFormat:@"%@",[udevice valueForKey:@"picture"]];
                        appDelegate.pictureSizeRider = picsize;
                    }
                }
            }
        }
        
        // 7. msg time을 기록한다. push메시지가 오면 msgtime이 작으면 무시한다.
        appDelegate.msgtime = [[responseObj valueForKey:@"mseq"] intValue];
#ifdef _WITZM
#elif defined _CHACHA
#else
        // 8. pay_token처리
        // pay_token은 sync방식으로 처리하기위해 해당 event에서 처리한다.
        if ([code isEqualToString:@"UR320"]) { // 카드등록 -> UR311 pay_nonce등록
            // pay_token local의 값과 다르면 저장하고 pay_nonce를 호출한다.
            id   user_payment = [responseObj objectForKey:@"user_payment"];
            //id   pay_token = [user_payment objectForKey:SettingsConst.CryptoKeys.PAY_TOKEN];
            id   pay_token = [responseObj objectForKey:SettingsConst.CryptoKeys.PAY_TOKEN];
            
            // 카드의 cseq
            NSDictionary *requestObj = getJSONObj(request.body);
            NSString* ono     = [requestObj valueForKey:@"no"];
            NSString* ocvv    = [requestObj valueForKey:@"cvv"];
            NSString* oexpire = [requestObj valueForKey:@"expire"];
            NSString* cseq    =  @"";
            
            //            NSLog(@"requestObj:%@",requestObj);
            /*
             for (NSString* key in user_payment) {
             NSString* no     = [key valueForKey:@"no"];
             //NSString* cvv    = [key valueForKey:@"cvv"];
             NSString* expire = [key valueForKey:@"expire"];
             
             NSLog(@"key:%@",key);
             
             if ([ono isEqualToString:no] && [oexpire isEqualToString:expire]) {
             //
             [[[TXApp instance] getSettings] setFDKeychain:SettingsConst.CryptoKeys.CSEQ value:[key valueForKey:@"cseq"]];
             cseq   = [key valueForKey:@"cseq"];
             break;
             }
             }
             */
            
            NSString* no     = [user_payment valueForKey:@"no"];
            //NSString* cvv    = [user_payment valueForKey:@"cvv"];
            NSString* expire = [user_payment valueForKey:@"expire"];
            
            //NSLog(@"key:%@",user_payment);
            
            if ([ono isEqualToString:no] && [oexpire isEqualToString:expire]) {
                //
                [[[TXApp instance] getSettings] setFDKeychain:SettingsConst.CryptoKeys.CSEQ value:[user_payment valueForKey:@"cseq"]];
                cseq   = [user_payment valueForKey:@"cseq"];
            }
            
            if (![pay_token isKindOfClass:[NSNull class]] && pay_token) {
                //if ([pay_token isEqualToString:[[[TXApp instance] getSettings] getProperty:SettingsConst.CryptoKeys.PAY_TOKEN]]) // 테스트라서 매번 동일한 값이 와서 주석처리 함.
                {
                    [[[TXApp instance] getSettings] setFDKeychain:SettingsConst.CryptoKeys.PAY_TOKEN value:pay_token];
                    
                    // pay_nonce호출처리한다.
                    TXUserModel *model = [TXUserModel instance];
                    
                    NSString *month  = [oexpire substringToIndex:2];
                    NSString *year   = [oexpire substringFromIndex:3];
                    //NSString *month = [NSString stringWithFormat:@"20%@",[oexpire substringFromIndex:2]];
                    NSLog(@"%@,%@",year,month);
                    NSDictionary *propertyMap = @{
                                                  @"clientToken" : pay_token,
                                                  @"cardNumber" : ono,
                                                  @"cvv" : ocvv,
                                                  @"month" : month,
                                                  @"year" : year,
                                                  @"mid" : @"UR311",
                                                  @"cseq" : cseq,
                                                  @"oseq" : @"",
                                                  };
                    
                    [model BTREE:propertyMap];
                }
            }
        }
        else if ([code isEqualToString:@"UR110"]) { // 카드등록 -> UR311 pay_nonce등록
            // pay_token local의 값과 다르면 저장하고 pay_nonce를 호출한다.
            id   user_payment = [responseObj objectForKey:@"user_payment"];
            id   pay_token = [responseObj objectForKey:SettingsConst.CryptoKeys.PAY_TOKEN];
            
            // 카드의 cseq
            NSString* ono     = [user_payment valueForKey:@"no"];
            NSString* ocvv    = [user_payment valueForKey:@"cvv"];
            NSString* oexpire = [user_payment valueForKey:@"expire"];
            NSString* cseq    = [user_payment valueForKey:@"cseq"];
            
            if (![pay_token isKindOfClass:[NSNull class]] && pay_token) {
                //if ([pay_token isEqualToString:[[[TXApp instance] getSettings] getProperty:SettingsConst.CryptoKeys.PAY_TOKEN]]) // 테스트라서 매번 동일한 값이 와서 주석처리 함.
                {
                    [[[TXApp instance] getSettings] setFDKeychain:SettingsConst.CryptoKeys.PAY_TOKEN value:pay_token];
                    
                    // pay_nonce호출처리한다.
                    TXUserModel *model = [TXUserModel instance];
                    
                    NSString *month  = [oexpire substringToIndex:2];
                    NSString *year   = [oexpire substringFromIndex:3];
                    //NSString *month = [NSString stringWithFormat:@"20%@",[oexpire substringFromIndex:2]];
                    NSLog(@"%@,%@",year,month);
                    NSDictionary *propertyMap = @{
                                                  @"clientToken" : pay_token,
                                                  @"cardNumber" : ono,
                                                  @"cvv" : ocvv,
                                                  @"month" : month,
                                                  @"year" : year,
                                                  @"mid" : @"UR311",
                                                  @"cseq" : cseq,
                                                  @"oseq" : @"",
                                                  };
                    
                    [model BTREE:propertyMap];
                }
            }
        }
#endif
        
        BOOL success = NO;
        if (rlt>0) {
            success = YES;
        }
        
        TXResponseDescriptor *descriptor = [TXResponseDescriptor create:success rlt:rlt code:code error:error atoken:atoken];
        
        event = [TXEvent createEvent:TXEvents.HTTPREQUESTCOMPLETED
                         eventSource:self
                          eventProps:@{
                                       TXEvents.Params.DESCRIPTOR : descriptor,
                                       TXEvents.Params.REQUEST    : request
                                       }];
        
        //dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            //Background Thread
            [appDelegate updateDic:event];
        //});
        
    } else {
        
        event = [TXEvent createEvent:TXEvents.NULLHTTPRESPONSE eventSource:self eventProps:nil];
        
    }
    
    return event;
}

// listener가 없을 경우 call된다.
-(void)onFail:(id)object error:(TXError *)error {
    

    TXResponseDescriptor *descriptor = [TXResponseDescriptor create:false rlt:0 code:@"" error:@"Http request failed" atoken:nil];
    TXEvent *event            = [TXEvent createEvent:TXEvents.HTTPREQUESTFAILED
                                         eventSource:self
                                         eventProps:@{
                                                        TXEvents.Params.DESCRIPTOR : descriptor,
                                                        TXEvents.Params.REQUEST    : object,
                                                        TXEvents.Params.ERROR    : error
                                                      }];
    [self fireEvent:event];
    
}

-(TXRequestObj *)createRequest:(NSString *)config {
    return [TXRequestObj create:config urlParams:nil listener:self];
}

-(void)sendAsyncRequest:(TXRequestObj *) request {
    [self->httpMgr sendAsyncRequest:request];
}

-(BOOL)sendSyncRequest:(TXRequestObj *) request {
    return [self->httpMgr sendSyncRequest:request];
}

-(void)onEvent:(TXEvent *)event eventParams:(id)subscriptionParams {
    NSAssert(false, @"Subclasses should override onEvent !");
}

@end
