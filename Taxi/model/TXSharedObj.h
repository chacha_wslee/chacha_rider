//
//  TXVM.h
//  Taxi
//

//

#import <Foundation/Foundation.h>
#import "TXUserModel.h"
#import "TXApp.h"
#import "StrConsts.h"

@interface TXSharedObj : NSObject

@property (nonatomic, strong) TXUser* user;
@property (nonatomic, strong) TXSettings *settings;
@property (nonatomic, strong) TXApp *app;

+(TXSharedObj *) instance;
-(UIStoryboard*) currentStoryBoard;

@end
