//
//  TXGoogleAPI.h
//  Taxi
//

//

#import <Foundation/Foundation.h>

@interface TXGoogleObj : NSObject

+(id)create;

@end

@interface TXPrediction : TXGoogleObj

@property (nonatomic, strong) NSString *description_;
@property (nonatomic, strong) NSString *id_;

@end

@interface TXGoogleObjectsCache : NSObject

/** Creates the single instance within the application
 
 @return TXGoogleObjectsCache
 */
+(TXGoogleObjectsCache *) instance;
-(void) cachePredictions:(NSString *) srcStr predictions:(NSArray *) predictions;
-(NSArray *) predictionsFromCache:(NSString *) srcStr;
-(void) clearPredictionsCache;

@end
