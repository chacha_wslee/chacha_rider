//
//  TXBaseObj.h
//  Taxi

//

#import <Foundation/Foundation.h>


@interface TXBaseObj : NSObject

+(id)create;
+(id)create:(NSDictionary *) properties;
-(NSDictionary *) getProperties;
-(void) setProperties : (NSDictionary *) props;

@end
