//
//  TXUserModel.m
//  Taxi
//

//

#import "TXUserModel.h"
#import "utils.h"

#ifdef _WITZM
#elif defined _CHACHA
#else
#import "BraintreeCard.h"
#endif

#import "TXAppDelegate.h"
#import "SVProgressHUD.h"
#import "TXError.h"

const struct UserConsts UserConsts = {
    
    .UID = @"uid",
    .NAME = @"name",
    .UTELNO = @"utelno",
    .PTOKEN = @"ptoken",
    .DID = @"did",
    .MODEL = @"model",
    .OS = @"os",
    .APP = @"app",
    .PASSWORD = @"pwd",
    .OTP = @"otp",
    
    
};

@implementation TXUser

@end

@implementation TXUserModel

/** Creates the single instance within the application
 
 @return TXUserModel
 */
+(TXUserModel *) instance {
    static dispatch_once_t pred;
    static TXUserModel* _instance;
    dispatch_once(&pred, ^{ _instance = [[self alloc] init]; });
    return _instance;
}
/*
-(id)cacheRequest:(NSString*)url{
    
    TXAppDelegate *appDelegate = (TXAppDelegate *)[[UIApplication sharedApplication] delegate];
    // Retrieve decompressed image
    id object = [appDelegate.cache cachedObjectForKey:url];
    //    if (object) {
    //        //object;
    //    }
    //    else {
    //        SEL sel = NSSelectorFromString(methodName);
    //
    //#pragma clang diagnostic push
    //#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
    //        [self performSelector:sel];
    //#pragma clang diagnostic pop
    //    }
    return object;
}
*/
-(void)checkIfPhoneNumberBlocked:(NSString *) phoneNum loginWithProvider: (BOOL) loginWithProvider {
    
    TXRequestObj *request    = [self createRequest:HttpAPIConsts.CHECKMOBILEPHONEBLOCKED];
    request.body = getJSONStr(@{ API_JSON.SignUp.PHONENUMBER : phoneNum });
    [self callProtocol:request];
    
}

-(NSDictionary*) getCommonHeader {
    
    TXAppDelegate *appDelegate = (TXAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSDictionary *infoDictionary = [[NSBundle mainBundle]infoDictionary];
    NSString *version = infoDictionary[@"CFBundleShortVersionString"];
    NSString *app = [NSString stringWithFormat:@"%@:%@",[[NSBundle mainBundle] bundleIdentifier],version];
    NSString* deviceID = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.DEVICEID];
    NSString* model = [NSString stringWithFormat:@"%@",[UIDevice currentDevice].model];
    NSString* os = [NSString stringWithFormat:@"%@:%@",[UIDevice currentDevice].systemName, [UIDevice currentDevice].systemVersion];
    //NSString* ptoken  = appDelegate.userPushToken;
    NSString* ptoken  = [[[TXApp instance] getSettings] getNotificationsToken];
    NSString *userUid = [[[TXApp instance] getSettings] getUserId];
    NSString *datoken = [[[TXApp instance] getSettings] getUserToken];

    return @{
             API_JSON.Authenticate.UID : (userUid != nil ? userUid : [NSNull null]),
             API_JSON.Authenticate.PTOKEN : (ptoken != nil ? ptoken : [NSNull null]),
             API_JSON.Authenticate.DID : (deviceID != nil ? deviceID : [NSNull null]),
             API_JSON.Authenticate.MODEL : model,
             API_JSON.Authenticate.OS : os,
             API_JSON.Authenticate.APP : app,
             API_JSON.ResponseDescriptor.DATOKEN: (datoken != nil ? datoken : [NSNull null]),
             };
}

-(void) errorResponse:(TXRequestObj*)request
{
    //log error here if the request had no error handler
    if (request.listener == nil ) {
        DLogE(@"%@ - %@ - Network UnReachable!!!", @"HTTP Get Error", request.reqConfig.name);
    } else if (request.listener!=nil) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [request.listener onFail:request error:[TXError error:-1001 message:@"Network UnReachable" description:@"Network UnReachable"]];
        });
    }
}

-(void) callProtocol:(TXRequestObj*)request {
    
    TXAppDelegate *appDelegate = (TXAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if ([appDelegate.loopArray valueForKey:LOOP_MAP_KEY(request.reqConfig.name)]) {
        [appDelegate.loopArray removeObjectForKey:LOOP_MAP_KEY(request.reqConfig.name)];
    }

    
    if (!appDelegate.isNetwork) {
        [self performSelector:@selector(errorResponse:) withObject:request afterDelay:1];
        return;
    }
    
#ifdef _DEBUG_ACTIVITY
    NSString *data = [NSString stringWithFormat:@"%@,%@",request.reqConfig.name, [Utils getXHTTPLocation]];
    [self performSelectorOnMainThread:@selector(postNotification:) withObject:[NSDictionary dictionaryWithObjectsAndKeys:@"EDQueueJobDidSucceed", @"name", data, @"data", nil] waitUntilDone:false];
#endif
    [self sendAsyncRequest:request];
}

- (void)postNotification:(NSDictionary *)object
{
    [[NSNotificationCenter defaultCenter] postNotificationName:[object objectForKey:@"name"] object:[object objectForKey:@"data"]];
}

-(void) requsetAPI:(NSString*)code property:(NSDictionary *) propertyMap {
    
    TXRequestObj *request     = [self createRequest:code];
    if (propertyMap != nil) {
        request.body = getJSONStr(propertyMap);
    }
    [self callProtocol:request];
}

-(void) uploadAPI:(NSString*)code property:(NSDictionary *) propertyMap {
    
    TXRequestObj *request     = [self createRequest:code];
    
    request.body = propertyMap[@"picture"];
    
    [self callProtocol:request];
}

-(void) requestSync:(NSString*)proto {
    
    TXRequestObj *request     = [self createRequest:proto];
    
    //request.body = getJSONStr(propertyMap);
    //[request.reqConfig setUseCache:YES];
    
    [self sendSyncRequest:request];
}

#pragma mark Rider API

-(void) UR110Auto:(TXUser *) user {
    
    NSDictionary* header = [self getCommonHeader];
    
    NSDictionary *propertyMap = @{
                                  API_JSON.Authenticate.UTELNO : (user.utelno != nil ? user.utelno : [NSNull null]),
                                  API_JSON.Authenticate.UID : (user.uid != nil ? user.uid : [NSNull null]),
                                  API_JSON.ResponseDescriptor.DATOKEN : (user.atoken != nil ? user.atoken : [NSNull null]),
                                  API_JSON.Authenticate.USERNAME : (user.name != nil ? user.name : [NSNull null]),
                                  API_JSON.Authenticate.OTP : (user.otp != nil ? user.otp : [NSNull null]),
                                  //@"languages": DEFAULT_LANGUAGES,
                                  API_JSON.Authenticate.PTOKEN : [header objectForKey:API_JSON.Authenticate.PTOKEN],
                                  API_JSON.Authenticate.DID : [header objectForKey:API_JSON.Authenticate.DID],
                                  API_JSON.Authenticate.MODEL : [header objectForKey:API_JSON.Authenticate.MODEL],
                                  API_JSON.Authenticate.OS : [header objectForKey:API_JSON.Authenticate.OS],
                                  API_JSON.Authenticate.APP : [header objectForKey:API_JSON.Authenticate.APP]
                                  };
    
    if (user.otp == nil) {
        propertyMap = @{
                        API_JSON.Authenticate.UTELNO : (user.utelno != nil ? user.utelno : [NSNull null]),
                        API_JSON.Authenticate.UID : (user.uid != nil ? user.uid : [NSNull null]),
                        API_JSON.ResponseDescriptor.DATOKEN : (user.atoken != nil ? user.atoken : [NSNull null]),
                        API_JSON.Authenticate.PTOKEN : [header objectForKey:API_JSON.Authenticate.PTOKEN],
                        API_JSON.Authenticate.DID : [header objectForKey:API_JSON.Authenticate.DID],
                        API_JSON.Authenticate.MODEL : [header objectForKey:API_JSON.Authenticate.MODEL],
                        API_JSON.Authenticate.OS : [header objectForKey:API_JSON.Authenticate.OS],
                        API_JSON.Authenticate.APP : [header objectForKey:API_JSON.Authenticate.APP]
                        };
    }

    if (user.sns) {
        NSMutableDictionary *property = [propertyMap mutableCopy];
        [property removeObjectForKey:API_JSON.Authenticate.PASSWORD];
        property[@"socialsite"]  = user.sns[@"socialsite"];
        property[@"user_social"] = [user.sns mutableCopy];
        propertyMap = property;
    }
    
    TXRequestObj *request     = [self createRequest:@"UR110"];
    
    request.body = getJSONStr(propertyMap);
    [self callProtocol:request];
    //[self sendSyncRequest:request];
}

-(void) UR110:(TXUser *) user {
    
    NSDictionary* header = [self getCommonHeader];
    
    NSDictionary *propertyMap = @{
                                  API_JSON.Authenticate.UTELNO : (user.utelno != nil ? user.utelno : [NSNull null]),
                                  API_JSON.Authenticate.UID : (user.uid != nil ? user.uid : [NSNull null]),
                                  API_JSON.Authenticate.PASSWORD : (user.pwd != nil ? user.pwd : [NSNull null]),
                                  API_JSON.Authenticate.USERNAME : (user.name != nil ? user.name : [NSNull null]),
                                  API_JSON.Authenticate.OTP : (user.otp != nil ? user.otp : [NSNull null]),
                                  //@"languages": DEFAULT_LANGUAGES,
                                  API_JSON.Authenticate.PTOKEN : [header objectForKey:API_JSON.Authenticate.PTOKEN],
                                  API_JSON.Authenticate.DID : [header objectForKey:API_JSON.Authenticate.DID],
                                  API_JSON.Authenticate.MODEL : [header objectForKey:API_JSON.Authenticate.MODEL],
                                  API_JSON.Authenticate.OS : [header objectForKey:API_JSON.Authenticate.OS],
                                  API_JSON.Authenticate.APP : [header objectForKey:API_JSON.Authenticate.APP]
                                  };
    
    if (user.otp == nil) {
        propertyMap = @{
                        API_JSON.Authenticate.UTELNO : (user.utelno != nil ? user.utelno : [NSNull null]),
                        API_JSON.Authenticate.UID : (user.uid != nil ? user.uid : [NSNull null]),
                        API_JSON.Authenticate.PTOKEN : [header objectForKey:API_JSON.Authenticate.PTOKEN],
                        API_JSON.Authenticate.DID : [header objectForKey:API_JSON.Authenticate.DID],
                        API_JSON.Authenticate.MODEL : [header objectForKey:API_JSON.Authenticate.MODEL],
                        API_JSON.Authenticate.OS : [header objectForKey:API_JSON.Authenticate.OS],
                        API_JSON.Authenticate.APP : [header objectForKey:API_JSON.Authenticate.APP],
                        API_JSON.ResponseDescriptor.DATOKEN : [header objectForKey:API_JSON.ResponseDescriptor.DATOKEN]
                        };
    }
    
    if (user.sns) {
        NSMutableDictionary *property = [propertyMap mutableCopy];
        [property removeObjectForKey:API_JSON.Authenticate.PASSWORD];
        property[@"socialsite"]  = user.sns[@"socialsite"];
        property[@"user_social"] = [user.sns mutableCopy];
        propertyMap = property;
    }
    
    NSArray *_splitItems = [NSStringFromSelector(_cmd) componentsSeparatedByString:@":"];
    TXRequestObj *request     = [self createRequest:[_splitItems objectAtIndex:0]];
    
    request.body = getJSONStr(propertyMap);
    [self callProtocol:request];
    //[self sendSyncRequest:request];
}

-(void) UR111:(TXUser *) user {
    
    NSDictionary* header = [self getCommonHeader];
    
    NSDictionary *propertyMap = @{
                                  API_JSON.Authenticate.UID : (user.uid != nil ? user.uid : [NSNull null]),
                                  API_JSON.Authenticate.UTELNO : (user.utelno != nil ? user.utelno : [NSNull null]),
                                  API_JSON.Authenticate.PASSWORD : (user.pwd != nil ? user.pwd : [NSNull null]),
                                  API_JSON.Authenticate.USERNAME : (user.name != nil ? user.name : [NSNull null]),
                                  API_JSON.Authenticate.OTP : (user.otp != nil ? user.otp : [NSNull null]),
                                  
                                  API_JSON.Authenticate.PTOKEN : [header objectForKey:API_JSON.Authenticate.PTOKEN],
                                  API_JSON.Authenticate.DID : [header objectForKey:API_JSON.Authenticate.DID],
                                  API_JSON.Authenticate.MODEL : [header objectForKey:API_JSON.Authenticate.MODEL],
                                  API_JSON.Authenticate.OS : [header objectForKey:API_JSON.Authenticate.OS],
                                  API_JSON.Authenticate.APP : [header objectForKey:API_JSON.Authenticate.APP]
                                  };

    NSArray *_splitItems = [NSStringFromSelector(_cmd) componentsSeparatedByString:@":"];
    TXRequestObj *request     = [self createRequest:[_splitItems objectAtIndex:0]];
//    NSLog(@"%@", NSStringFromSelector(_cmd));

    request.body = getJSONStr(propertyMap);
    [self callProtocol:request];
}


-(void) UR112:(NSString*)ptoken {
    
    NSDictionary* header = [self getCommonHeader];
    
    NSDictionary *propertyMap = @{
                                  API_JSON.Authenticate.UID : [header objectForKey:API_JSON.Authenticate.UID],
                                  API_JSON.Authenticate.PTOKEN : ptoken,
                                  API_JSON.Authenticate.DID : [header objectForKey:API_JSON.Authenticate.DID],
                                  API_JSON.Authenticate.MODEL : [header objectForKey:API_JSON.Authenticate.MODEL],
                                  API_JSON.Authenticate.OS : [header objectForKey:API_JSON.Authenticate.OS],
                                  API_JSON.Authenticate.APP : [header objectForKey:API_JSON.Authenticate.APP]
                                  };
    
    NSArray *_splitItems = [NSStringFromSelector(_cmd) componentsSeparatedByString:@":"];
    TXRequestObj *request     = [self createRequest:[_splitItems objectAtIndex:0]];
    
    request.body = getJSONStr(propertyMap);
    [self callProtocol:request];
}

-(void) UR114:(TXUser *) user {
    
    [self UR114:user withSession:YES];
}

-(void) UR114:(TXUser *) user withSession:(BOOL)session{
    
    if (session) {
        // 데이터 초기화
        TXAppDelegate *appDelegate = (TXAppDelegate *)[[UIApplication sharedApplication] delegate];
        [appDelegate initProtocolMessage];
    }
    
    NSDictionary* header = [self getCommonHeader];
    
    NSDictionary *propertyMap = @{
                                  API_JSON.Authenticate.UID : (user.uid != nil ? user.uid : [NSNull null]),
                                  API_JSON.Authenticate.UTELNO : (user.utelno != nil ? user.utelno : [NSNull null]),
                                  API_JSON.Authenticate.PTOKEN : [header objectForKey:API_JSON.Authenticate.PTOKEN],
                                  API_JSON.Authenticate.DID : [header objectForKey:API_JSON.Authenticate.DID],
                                  API_JSON.Authenticate.MODEL : [header objectForKey:API_JSON.Authenticate.MODEL],
                                  API_JSON.Authenticate.OS : [header objectForKey:API_JSON.Authenticate.OS],
                                  API_JSON.Authenticate.APP : [header objectForKey:API_JSON.Authenticate.APP]
                                  };
    
    NSArray *_splitItems = [NSStringFromSelector(_cmd) componentsSeparatedByString:@":"];
    TXRequestObj *request     = [self createRequest:[_splitItems objectAtIndex:0]];
    
    request.body = getJSONStr(propertyMap);
    [self callProtocol:request];
    //[self sendSyncRequest:request];
}

-(void) UR120:(TXUser *) user {
    
    NSDictionary* header = [self getCommonHeader];
    
    NSDictionary *propertyMap = @{
                                  API_JSON.Authenticate.UTELNO : (user.utelno != nil ? user.utelno : [NSNull null]),
                                  API_JSON.Authenticate.UID : (user.uid != nil ? user.uid : [NSNull null]),
                                  API_JSON.Authenticate.PASSWORD : (user.pwd != nil ? user.pwd : [NSNull null]),
                                  API_JSON.Authenticate.OTP : (user.otp != nil ? user.otp : [NSNull null]),
                                  //@"languages": DEFAULT_LANGUAGES,
                                  API_JSON.Authenticate.USERNAME : (user.name != nil ? user.name : [NSNull null]),
                                  API_JSON.Authenticate.PTOKEN : [header objectForKey:API_JSON.Authenticate.PTOKEN],
                                  API_JSON.Authenticate.DID : [header objectForKey:API_JSON.Authenticate.DID],
                                  API_JSON.Authenticate.MODEL : [header objectForKey:API_JSON.Authenticate.MODEL],
                                  API_JSON.Authenticate.OS : [header objectForKey:API_JSON.Authenticate.OS],
                                  API_JSON.Authenticate.APP : [header objectForKey:API_JSON.Authenticate.APP],
//#ifdef _CHACHA1 // DEBUG
//                                  @"user_agree":@{
//                                          @"service_utility" : @"1",
//                                          @"private_information" : @"1",
//                                          @"private_location" : @"1"
//                                          }
//#endif
                                  };
    
    if (user.sns) {
        NSMutableDictionary *property = [propertyMap mutableCopy];
        [property removeObjectForKey:API_JSON.Authenticate.PASSWORD];
        property[@"socialsite"]  = user.sns[@"socialsite"];
        property[@"user_social"] = [user.sns mutableCopy];
        propertyMap = property;
    }
    
    NSArray *_splitItems = [NSStringFromSelector(_cmd) componentsSeparatedByString:@":"];
    TXRequestObj *request     = [self createRequest:[_splitItems objectAtIndex:0]];
    
    request.body = getJSONStr(propertyMap);
    [self callProtocol:request];
}

-(void) UR200 {
    
//    TXAppDelegate *appDelegate = (TXAppDelegate *)[[UIApplication sharedApplication] delegate];
//    
//    NSDictionary *user = [appDelegate.dicRider objectForKey:@"user"];
//    NSString* useq = [user valueForKey:@"useq"];
//    
//    if (useq == nil) {
//        useq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.USEQ];
//    }
    
    NSString* useq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.USEQ];
    
    NSDictionary *propertyMap = @{
                                  @"useq" : useq
                                  };
    
    NSArray *_splitItems = [NSStringFromSelector(_cmd) componentsSeparatedByString:@":"];
    TXRequestObj *request     = [self createRequest:[_splitItems objectAtIndex:0]];
    
    request.body = getJSONStr(propertyMap);
    //[request.reqConfig setUseCache:YES];
    
    [self callProtocol:request];
}

-(void) UR601 {
    
    TXAppDelegate *appDelegate = (TXAppDelegate *)[[UIApplication sharedApplication] delegate];
    if (appDelegate.pictureSizeRider == nil) {
        [Utils setStateUIInfo:UI_STATE_UR601 value:UI_STATE_STEP_WAIT oldvalue:@""];
        return;
    }
    //NSDictionary *user = [appDelegate.dicRider objectForKey:@"user"];
    //NSString* useq = [user valueForKey:@"useq"];
    
    NSString* useq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.USEQ];
    //NSString* oseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.OSEQ];
    
    //NSDictionary *user = [appDelegate.dicRider objectForKey:@"user"];
    
    NSDictionary *propertyMap = @{
                                  @"useq" : useq,
                                  @"picture" : appDelegate.pictureSizeRider,
                                  };
    
    NSArray *_splitItems = [NSStringFromSelector(_cmd) componentsSeparatedByString:@":"];
    TXRequestObj *request     = [self createRequest:[_splitItems objectAtIndex:0]];

    request.body = getJSONStr(propertyMap);
    //[request.reqConfig setUseCache:YES];
    
    [self callProtocol:request];
}

-(void) UR602 {
    
    TXAppDelegate *appDelegate = (TXAppDelegate *)[[UIApplication sharedApplication] delegate];
    if (appDelegate.pictureSizeDriver == nil) {
        return;
    }
    NSString* pseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.PSEQ];
    //NSString* oseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.OSEQ];
    
    //NSDictionary *user = [appDelegate.dicDriver objectForKey:@"provider"];
    
    NSDictionary *propertyMap = @{
                                  @"pseq" : pseq,
                                  @"picture" : appDelegate.pictureSizeDriver,
                                  };
    
    NSArray *_splitItems = [NSStringFromSelector(_cmd) componentsSeparatedByString:@":"];
    TXRequestObj *request     = [self createRequest:[_splitItems objectAtIndex:0]];
    
    request.body = getJSONStr(propertyMap);
    
    [self callProtocol:request];
}
-(void) UR601:(NSDictionary*)propertyMap {
    
    NSArray *_splitItems = [NSStringFromSelector(_cmd) componentsSeparatedByString:@":"];
    TXRequestObj *request     = [self createRequest:[_splitItems objectAtIndex:0]];
    
    request.body = getJSONStr(propertyMap);
    //[request.reqConfig setUseCache:YES];
    
    [self callProtocol:request];
}

-(void) UR602:(NSDictionary *)propertyMap {
    
    NSArray *_splitItems = [NSStringFromSelector(_cmd) componentsSeparatedByString:@":"];
    TXRequestObj *request     = [self createRequest:[_splitItems objectAtIndex:0]];
    
    request.body = getJSONStr(propertyMap);
    
    [self callProtocol:request];
}

-(void) UR603:(NSDictionary *)propertyMap {
    
    NSArray *_splitItems = [NSStringFromSelector(_cmd) componentsSeparatedByString:@":"];
    TXRequestObj *request     = [self createRequest:[_splitItems objectAtIndex:0]];
    
    request.body = getJSONStr(propertyMap);
    
    [self callProtocol:request];
}

-(void) UR210:(NSDictionary *) propertyMap {
    
    NSArray *_splitItems = [NSStringFromSelector(_cmd) componentsSeparatedByString:@":"];
    TXRequestObj *request     = [self createRequest:[_splitItems objectAtIndex:0]];
    
    request.body = getJSONStr(propertyMap);
    [self callProtocol:request];
}

-(void) UR211:(NSDictionary *) propertyMap {
    
    NSArray *_splitItems = [NSStringFromSelector(_cmd) componentsSeparatedByString:@":"];
    TXRequestObj *request     = [self createRequest:[_splitItems objectAtIndex:0]];
    
    request.body = getJSONStr(propertyMap);
    [self callProtocol:request];
}

-(void) UR300 {
    
    NSString* useq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.USEQ];
    
    NSDictionary *propertyMap = @{
                                  @"useq" : useq
                                  };
    
    NSArray *_splitItems = [NSStringFromSelector(_cmd) componentsSeparatedByString:@":"];
    TXRequestObj *request     = [self createRequest:[_splitItems objectAtIndex:0]];
    
    request.body = getJSONStr(propertyMap);
    [self callProtocol:request];
}

-(void) UR303 {
    
    NSString* useq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.USEQ];
    
    NSDictionary *propertyMap = @{
                                  @"useq" : useq
                                  };
    
    NSArray *_splitItems = [NSStringFromSelector(_cmd) componentsSeparatedByString:@":"];
    TXRequestObj *request     = [self createRequest:[_splitItems objectAtIndex:0]];
    
    request.body = getJSONStr(propertyMap);
    [self callProtocol:request];
}


-(void) UR310:(NSString*)cseq {
    
    NSString* useq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.USEQ];
    
    NSDictionary *propertyMap = @{
                                  @"useq" : useq,
                                  @"cseq" : cseq,
                                  @"mode" : @"1"
                                  };
    
    NSArray *_splitItems = [NSStringFromSelector(_cmd) componentsSeparatedByString:@":"];
    TXRequestObj *request     = [self createRequest:[_splitItems objectAtIndex:0]];
    
    request.body = getJSONStr(propertyMap);
    [self callProtocol:request];
}


-(void) UR323:(NSString*)code {
    
    NSString* useq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.USEQ];
    
    NSDictionary *propertyMap = @{
                                  @"useq" : useq,
                                  @"code" : code,
                                  };
    
    NSArray *_splitItems = [NSStringFromSelector(_cmd) componentsSeparatedByString:@":"];
    TXRequestObj *request     = [self createRequest:[_splitItems objectAtIndex:0]];
    
    request.body = getJSONStr(propertyMap);
    [self callProtocol:request];
}

-(void) UR332:(NSString*)cseq {
    
    NSString* useq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.USEQ];
    
    NSDictionary *propertyMap = @{
                                  @"useq" : useq,
                                  @"cseq" : cseq
                                  };
    
    NSArray *_splitItems = [NSStringFromSelector(_cmd) componentsSeparatedByString:@":"];
    TXRequestObj *request     = [self createRequest:[_splitItems objectAtIndex:0]];
    
    request.body = getJSONStr(propertyMap);
    [self callProtocol:request];
}

-(void) UR331:(NSString*)qseq {
    
    NSString* useq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.USEQ];
    
    NSDictionary *propertyMap = @{
                                  @"useq" : useq,
                                  @"qseq" : qseq,
                                  };
    
    NSArray *_splitItems = [NSStringFromSelector(_cmd) componentsSeparatedByString:@":"];
    TXRequestObj *request     = [self createRequest:[_splitItems objectAtIndex:0]];
    
    request.body = getJSONStr(propertyMap);
    [self callProtocol:request];
}

-(void)UR311:(NSDictionary *) propertyMap {
    
    NSArray *_splitItems = [NSStringFromSelector(_cmd) componentsSeparatedByString:@":"];
    TXRequestObj *request     = [self createRequest:[_splitItems objectAtIndex:0]];
    
    request.body = getJSONStr(propertyMap);
    [self callProtocol:request];
}

-(void) UR501:(NSString*)fseq {
    
    NSString* useq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.USEQ];
    
    NSDictionary *propertyMap = @{
                                  @"useq" : useq
                                  };
    if (![fseq isEqualToString:@""]) {
        propertyMap = @{
                        @"useq" : useq,
                        @"fseq" : fseq
                        };
    }
    
    NSArray *_splitItems = [NSStringFromSelector(_cmd) componentsSeparatedByString:@":"];
    TXRequestObj *request     = [self createRequest:[_splitItems objectAtIndex:0]];
    
    request.body = getJSONStr(propertyMap);
    [self callProtocol:request];
}


-(void) UR503 {
    
    NSString* useq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.USEQ];
    
    NSDictionary *propertyMap = @{
                                  @"useq" : useq
                                  };
    
    NSArray *_splitItems = [NSStringFromSelector(_cmd) componentsSeparatedByString:@":"];
    TXRequestObj *request     = [self createRequest:[_splitItems objectAtIndex:0]];
    
    request.body = getJSONStr(propertyMap);
    [self callProtocol:request];
}

-(void) UQ002 {
    
    NSArray *_splitItems = [NSStringFromSelector(_cmd) componentsSeparatedByString:@":"];
    TXRequestObj *request     = [self createRequest:[_splitItems objectAtIndex:0]];
    
    [self callProtocol:request];
}

-(void) UQ100 {
    
    NSString* useq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.USEQ];
    //NSString* oseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.OSEQ];
    
    NSDictionary *propertyMap = @{
                                  @"useq" : useq,
                                  };
    
    NSArray *_splitItems = [NSStringFromSelector(_cmd) componentsSeparatedByString:@":"];
    TXRequestObj *request     = [self createRequest:[_splitItems objectAtIndex:0]];
    
    request.body = getJSONStr(propertyMap);
    [self callProtocol:request];
}

-(void) UD112:(NSDictionary *) propertyMap {
    
    NSArray *_splitItems = [NSStringFromSelector(_cmd) componentsSeparatedByString:@":"];
    TXRequestObj *request     = [self createRequest:[_splitItems objectAtIndex:0]];
    
    request.body = getJSONStr(propertyMap);
    [self callProtocol:request];
}

-(void) UQ101:(NSDictionary *) propertyMap {
    
    NSArray *_splitItems = [NSStringFromSelector(_cmd) componentsSeparatedByString:@":"];
    TXRequestObj *request     = [self createRequest:[_splitItems objectAtIndex:0]];
    
    request.body = getJSONStr(propertyMap);
    [self callProtocol:request];
}

-(void) UD120:(NSDictionary *) propertyMap  {
    
    NSArray *_splitItems = [NSStringFromSelector(_cmd) componentsSeparatedByString:@":"];
    TXRequestObj *request     = [self createRequest:[_splitItems objectAtIndex:0]];
    
    request.body = getJSONStr(propertyMap);
    [self callProtocol:request];
    //[self sendSyncRequest:request];
}

-(void) UD130 {
    TXAppDelegate *appDelegate = (TXAppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.isMyCancel = YES;
    NSString* useq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.USEQ];
    NSString* oseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.OSEQ];
    
    NSDictionary *propertyMap = @{
                                  @"useq" : useq,
                                  @"oseq" : oseq,
                                  };
    
    NSArray *_splitItems = [NSStringFromSelector(_cmd) componentsSeparatedByString:@":"];
    TXRequestObj *request     = [self createRequest:[_splitItems objectAtIndex:0]];
    
    request.body = getJSONStr(propertyMap);
    [self callProtocol:request];
    //[self sendSyncRequest:request];
}

-(void) UD131 {
    TXAppDelegate *appDelegate = (TXAppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.isMyCancel = YES;
    NSString* useq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.USEQ];
    NSString* oseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.OSEQ];
    
    NSDictionary *propertyMap = @{
                                  @"useq" : useq,
                                  @"oseq" : oseq,
                                  };
    
    NSArray *_splitItems = [NSStringFromSelector(_cmd) componentsSeparatedByString:@":"];
    TXRequestObj *request     = [self createRequest:[_splitItems objectAtIndex:0]];
    
    request.body = getJSONStr(propertyMap);
    [self callProtocol:request];
    //[self sendSyncRequest:request];
}

-(void) UQ200 {
    
    NSString* useq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.USEQ];
    NSString* oseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.OSEQ];
    
    NSDictionary *propertyMap = @{
                                  @"useq" : useq,
                                  @"oseq" : oseq,
                                  };
    
    NSArray *_splitItems = [NSStringFromSelector(_cmd) componentsSeparatedByString:@":"];
    TXRequestObj *request     = [self createRequest:[_splitItems objectAtIndex:0]];
    
    request.body = getJSONStr(propertyMap);
    [self callProtocol:request];
}

// 목적지 업데이트
-(void) UD110 {
    
    NSString* useq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.USEQ];
    NSString* oseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.OSEQ];
    
    NSString* slat = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.SLAT];
    NSString* slng = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.SLNG];
    
    NSString* _lsrc = [NSString stringWithFormat:@"%@,%@",slat,slng];
    if (![Utils isLocationValid:[slat doubleValue] long:[slat doubleValue]]) {
        _lsrc = @"";
    }
    
    NSString* dlat = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.DLAT];
    NSString* dlng = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.DLNG];
    
    NSString* _ldst = [NSString stringWithFormat:@"%@,%@",dlat,dlng];
    if (![Utils isLocationValid:[dlat doubleValue] long:[dlat doubleValue]]) {
        _ldst = @"";
    }
    
    NSMutableDictionary *propertyMap = [NSMutableDictionary dictionaryWithObjectsAndKeys:useq, @"useq", oseq, @"oseq", nil];
    if (![_lsrc isEqualToString:@""]) {
        [propertyMap setObject:_lsrc forKey:@"lsrc"];
    }
    if (![_ldst isEqualToString:@""]) {
        [propertyMap setObject:_ldst forKey:@"ldst"];
    }
    
    NSArray *_splitItems = [NSStringFromSelector(_cmd) componentsSeparatedByString:@":"];
    TXRequestObj *request     = [self createRequest:[_splitItems objectAtIndex:0]];
    
    request.body = getJSONStr(propertyMap);
    [self callProtocol:request];
}

-(void) UD210 {
    
    NSString* useq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.USEQ];
    NSString* oseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.OSEQ];
    
    NSString* slat = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.SLAT];
    NSString* slng = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.SLNG];
    
    NSString* _lsrc = [NSString stringWithFormat:@"%@,%@",slat,slng];
    if (![Utils isLocationValid:[slat doubleValue] long:[slat doubleValue]]) {
        _lsrc = @"";
    }
    
    NSString* dlat = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.DLAT];
    NSString* dlng = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.DLNG];
    
    NSString* _ldst = [NSString stringWithFormat:@"%@,%@",dlat,dlng];
    if (![Utils isLocationValid:[dlat doubleValue] long:[dlat doubleValue]]) {
        _ldst = @"";
    }
    
    NSMutableDictionary *propertyMap = [NSMutableDictionary dictionaryWithObjectsAndKeys:useq, @"useq", oseq, @"oseq", nil];
    if (![_lsrc isEqualToString:@""]) {
        [propertyMap setObject:_lsrc forKey:@"lsrc"];
    }
    if (![_ldst isEqualToString:@""]) {
        [propertyMap setObject:_ldst forKey:@"ldst"];
    }
    
    NSArray *_splitItems = [NSStringFromSelector(_cmd) componentsSeparatedByString:@":"];
    TXRequestObj *request     = [self createRequest:[_splitItems objectAtIndex:0]];
    
    request.body = getJSONStr(propertyMap);
    [self callProtocol:request];
}

-(void) UD230 {
    TXAppDelegate *appDelegate = (TXAppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.isMyCancel = YES;
    NSString* useq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.USEQ];
    NSString* oseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.OSEQ];
    
    NSDictionary *propertyMap = @{
                                  @"useq" : useq,
                                  @"oseq" : oseq,
                                  };
    
    NSArray *_splitItems = [NSStringFromSelector(_cmd) componentsSeparatedByString:@":"];
    TXRequestObj *request     = [self createRequest:[_splitItems objectAtIndex:0]];
    
    request.body = getJSONStr(propertyMap);
    [self callProtocol:request];
    //[self sendSyncRequest:request];
}

-(void) UQ300 {
    
    NSString* useq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.USEQ];
    NSString* oseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.OSEQ];
    
    NSDictionary *propertyMap = @{
                                  @"useq" : useq,
                                  @"oseq" : oseq,
                                  };
    
    NSArray *_splitItems = [NSStringFromSelector(_cmd) componentsSeparatedByString:@":"];
    TXRequestObj *request     = [self createRequest:[_splitItems objectAtIndex:0]];
    
    request.body = getJSONStr(propertyMap);
    [self callProtocol:request];
}

// 목적지 업데이트
-(void) UD310 {
    
    NSString* useq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.USEQ];
    NSString* oseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.OSEQ];
    
    NSString* dlat = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.DLAT];
    NSString* dlng = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.DLNG];
    
    NSString* _ldst = [NSString stringWithFormat:@"%@,%@",dlat,dlng];
    
    NSDictionary *propertyMap = @{
                                  @"useq" : useq,
                                  @"oseq" : oseq,
                                  @"ldst" : _ldst,
                                  };
    
    NSArray *_splitItems = [NSStringFromSelector(_cmd) componentsSeparatedByString:@":"];
    TXRequestObj *request     = [self createRequest:[_splitItems objectAtIndex:0]];
    
    request.body = getJSONStr(propertyMap);
    [self callProtocol:request];
}

-(void) PD351 {
    
    NSString* useq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.USEQ];
    NSString* oseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.OSEQ];
    
    NSString* dlat = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.DLAT];
    NSString* dlng = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.DLNG];
    
    NSString* _ldst = [NSString stringWithFormat:@"%@,%@",dlat,dlng];
    
    NSDictionary *propertyMap = @{
                                  @"useq" : useq,
                                  @"oseq" : oseq,
                                  @"ldst" : _ldst,
                                  };
    
    NSArray *_splitItems = [NSStringFromSelector(_cmd) componentsSeparatedByString:@":"];
    TXRequestObj *request     = [self createRequest:[_splitItems objectAtIndex:0]];
    
    request.body = getJSONStr(propertyMap);
    [self callProtocol:request];
}

-(void) UR301:(NSString*)oseq {
    
    NSString* useq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.USEQ];
    
    NSDictionary *propertyMap = @{
                                  @"useq" : useq
                                  };
    
    if (![oseq isEqualToString:@""]) {
        propertyMap = @{
                        @"useq" : useq,
                        @"oseq" : oseq
                        };
    }
    
    NSArray *_splitItems = [NSStringFromSelector(_cmd) componentsSeparatedByString:@":"];
    TXRequestObj *request     = [self createRequest:[_splitItems objectAtIndex:0]];
    
    request.body = getJSONStr(propertyMap);
    
    [self callProtocol:request];
}

-(void) UD302:(NSString*)oseq {
    
    NSString* useq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.USEQ];
    
    NSDictionary *propertyMap = @{
                                  @"useq" : useq
                                  };
    
    if (![oseq isEqualToString:@""]) {
        propertyMap = @{
                        @"useq" : useq,
                        @"oseq" : oseq
                        };
    }
    
    NSArray *_splitItems = [NSStringFromSelector(_cmd) componentsSeparatedByString:@":"];
    TXRequestObj *request     = [self createRequest:[_splitItems objectAtIndex:0]];
    
    request.body = getJSONStr(propertyMap);
    
    [self callProtocol:request];
}

-(void) UD322:(NSString*)utelno message:(NSString*)message {
    
    NSString* useq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.USEQ];
    NSString* oseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.OSEQ];
    
    NSDictionary *propertyMap = @{
                                  @"useq" : useq,
                                  @"oseq" : oseq,
                                  @"utelno" : utelno,
                                  @"msg" : message
                                  };
    
    NSArray *_splitItems = [NSStringFromSelector(_cmd) componentsSeparatedByString:@":"];
    TXRequestObj *request     = [self createRequest:[_splitItems objectAtIndex:0]];
    
    request.body = getJSONStr(propertyMap);
    
    [self callProtocol:request];
}

-(void) UD306 {
    
    NSString* useq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.USEQ];
    NSString* oseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.OSEQ];
    
    NSDictionary *propertyMap = @{
                                  @"useq" : useq,
                                  @"oseq" : oseq,
                                  };
    
    NSArray *_splitItems = [NSStringFromSelector(_cmd) componentsSeparatedByString:@":"];
    TXRequestObj *request     = [self createRequest:[_splitItems objectAtIndex:0]];
    
    request.body = getJSONStr(propertyMap);
    [self callProtocol:request];
}

-(void) UD410:(NSDictionary *) propertyMap {
    
    NSArray *_splitItems = [NSStringFromSelector(_cmd) componentsSeparatedByString:@":"];
    TXRequestObj *request     = [self createRequest:[_splitItems objectAtIndex:0]];
    
    request.body = getJSONStr(propertyMap);
    [self callProtocol:request];
}


-(void) UD421:(NSDictionary *) propertyMap {
    
    NSArray *_splitItems = [NSStringFromSelector(_cmd) componentsSeparatedByString:@":"];
    TXRequestObj *request     = [self createRequest:[_splitItems objectAtIndex:0]];
    
    request.body = getJSONStr(propertyMap);
    [self callProtocol:request];
}

-(void) UD413:(NSDictionary *) propertyMap {
    
    NSArray *_splitItems = [NSStringFromSelector(_cmd) componentsSeparatedByString:@":"];
    TXRequestObj *request     = [self createRequest:[_splitItems objectAtIndex:0]];
    
    request.body = getJSONStr(propertyMap);
    [self callProtocol:request];
}

// tip -> 사용하지 않음
-(void) UD423 {
    
    NSString* useq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.USEQD];
    NSString* oseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.OSEQ];
    
    NSString* pay_tip = @"";
    
    // useq,oseq,rating,driver,note,tip
    NSDictionary *propertyMap = @{
                                  @"useq" : useq,
                                  @"oseq" : oseq,
                                  @"pay_tip" : pay_tip,
                                  };
    
    NSArray *_splitItems = [NSStringFromSelector(_cmd) componentsSeparatedByString:@":"];
    TXRequestObj *request     = [self createRequest:[_splitItems objectAtIndex:0]];
    
    request.body = getJSONStr(propertyMap);
    [self callProtocol:request];
    //[self sendSyncRequest:request];
}

#pragma mark Driver API
-(void) PQ002 {
    
    NSString* pseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.PSEQ];
    
    NSDictionary *propertyMap = @{
                                  @"pseq" : pseq
                                  };
    
    NSArray *_splitItems = [NSStringFromSelector(_cmd) componentsSeparatedByString:@":"];
    TXRequestObj *request     = [self createRequest:[_splitItems objectAtIndex:0]];
    
    request.body = getJSONStr(propertyMap);
    [self callProtocol:request];
}


-(void) PQ100 {
    
    NSArray *_splitItems = [NSStringFromSelector(_cmd) componentsSeparatedByString:@":"];
    TXRequestObj *request     = [self createRequest:[_splitItems objectAtIndex:0]];
    
    [self callProtocol:request];
}

-(void) PD110 {
    
    NSString* pseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.PSEQ];
    NSString* oseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.OSEQ];
    
    NSDictionary *propertyMap = @{
                                  @"pseq" : pseq,
                                  @"oseq" : oseq,
                                  };
    
    NSArray *_splitItems = [NSStringFromSelector(_cmd) componentsSeparatedByString:@":"];
    TXRequestObj *request     = [self createRequest:[_splitItems objectAtIndex:0]];
    
    request.body = getJSONStr(propertyMap);
    [self callProtocol:request];
}

-(void) PD130:(NSString*)rsncode {
    
    NSString* pseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.PSEQ];
    NSString* oseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.OSEQ];
    
    NSDictionary *propertyMap = @{
                                  @"pseq" : pseq,
                                  @"oseq" : oseq,
                                  @"rsncode" : rsncode,
                                  };
    
    NSArray *_splitItems = [NSStringFromSelector(_cmd) componentsSeparatedByString:@":"];
    TXRequestObj *request     = [self createRequest:[_splitItems objectAtIndex:0]];
    
    request.body = getJSONStr(propertyMap);
    [self callProtocol:request];
}

-(void) PD131 {
    
    NSString* pseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.PSEQ];
    NSString* oseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.OSEQ];
    
    NSDictionary *propertyMap = @{
                                  @"pseq" : pseq,
                                  @"oseq" : oseq,
                                  };
    
    NSArray *_splitItems = [NSStringFromSelector(_cmd) componentsSeparatedByString:@":"];
    TXRequestObj *request     = [self createRequest:[_splitItems objectAtIndex:0]];
    
    request.body = getJSONStr(propertyMap);
    [self callProtocol:request];
}

-(void) PD220:(NSDictionary *) propertyMap {
    
    NSArray *_splitItems = [NSStringFromSelector(_cmd) componentsSeparatedByString:@":"];
    TXRequestObj *request     = [self createRequest:[_splitItems objectAtIndex:0]];
    
    request.body = getJSONStr(propertyMap);
    [self callProtocol:request];
}

-(void) PD211:(NSDictionary *) propertyMap {
    
    NSArray *_splitItems = [NSStringFromSelector(_cmd) componentsSeparatedByString:@":"];
    TXRequestObj *request     = [self createRequest:[_splitItems objectAtIndex:0]];
    
    request.body = getJSONStr(propertyMap);
    [self callProtocol:request];
}

-(void) PQ200 {
    
    NSString* pseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.PSEQ];
    NSString* oseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.OSEQ];
    
    NSDictionary *propertyMap = @{
                                  @"pseq" : pseq,
                                  @"oseq" : oseq,
                                  };
    
    NSArray *_splitItems = [NSStringFromSelector(_cmd) componentsSeparatedByString:@":"];
    TXRequestObj *request     = [self createRequest:[_splitItems objectAtIndex:0]];
    
    request.body = getJSONStr(propertyMap);
    [self callProtocol:request];
}

-(void) PQ300 {
    
    NSString* pseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.PSEQ];
    NSString* oseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.OSEQ];
    
    NSDictionary *propertyMap = @{
                                  @"pseq" : pseq,
                                  @"oseq" : oseq,
                                  };
    
    NSArray *_splitItems = [NSStringFromSelector(_cmd) componentsSeparatedByString:@":"];
    TXRequestObj *request     = [self createRequest:[_splitItems objectAtIndex:0]];
    
    request.body = getJSONStr(propertyMap);
    [self callProtocol:request];
}

-(void) PD213 {
    
    NSString* pseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.PSEQ];
    NSString* oseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.OSEQ];
    
    NSDictionary *propertyMap = @{
                                  @"pseq" : pseq,
                                  @"oseq" : oseq,
                                  };
    
    NSArray *_splitItems = [NSStringFromSelector(_cmd) componentsSeparatedByString:@":"];
    TXRequestObj *request     = [self createRequest:[_splitItems objectAtIndex:0]];
    
    request.body = getJSONStr(propertyMap);
    [self callProtocol:request];
}

-(void) PD232:(NSString*)rsncode {
    
    NSString* pseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.PSEQ];
    NSString* oseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.OSEQ];
    
    NSDictionary *propertyMap = @{
                                  @"pseq" : pseq,
                                  @"oseq" : oseq,
                                  @"rsncode" : rsncode,
                                  };
    
    NSArray *_splitItems = [NSStringFromSelector(_cmd) componentsSeparatedByString:@":"];
    TXRequestObj *request     = [self createRequest:[_splitItems objectAtIndex:0]];
    
    request.body = getJSONStr(propertyMap);
    [self callProtocol:request];
}

-(void) PP301:(NSString*)oseq {
    
    NSString* pseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.PSEQ];
    //NSString* oseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.OSEQ];
    
    NSDictionary *propertyMap = @{
                                  @"pseq" : pseq
                                  };
    
    if (![oseq isEqualToString:@""]) {
        propertyMap = @{
                        @"pseq" : pseq,
                        @"oseq" : oseq
                        };
    }
    
    NSArray *_splitItems = [NSStringFromSelector(_cmd) componentsSeparatedByString:@":"];
    TXRequestObj *request     = [self createRequest:[_splitItems objectAtIndex:0]];
    
    request.body = getJSONStr(propertyMap);
    
    [self callProtocol:request];
}

-(void) PD304:(NSString*)oseq {
    
    NSString* pseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.PSEQ];
    //NSString* oseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.OSEQ];
    
    NSDictionary *propertyMap = @{
                                  @"pseq" : pseq
                                  };
    
    if (![oseq isEqualToString:@""]) {
        propertyMap = @{
                        @"pseq" : pseq,
                        @"oseq" : oseq
                        };
    }
    
    NSArray *_splitItems = [NSStringFromSelector(_cmd) componentsSeparatedByString:@":"];
    TXRequestObj *request     = [self createRequest:[_splitItems objectAtIndex:0]];
    
    request.body = getJSONStr(propertyMap);
    
    [self callProtocol:request];
}

-(void) PD305:(NSString*)oseq {
    
    NSString* pseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.PSEQ];
    //NSString* oseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.OSEQ];
    
    NSDictionary *propertyMap = @{
                                  @"pseq" : pseq
                                  };
    
    if (![oseq isEqualToString:@""]) {
        propertyMap = @{
                        @"pseq" : pseq,
                        @"oseq" : oseq
                        };
    }
    
    NSArray *_splitItems = [NSStringFromSelector(_cmd) componentsSeparatedByString:@":"];
    TXRequestObj *request     = [self createRequest:[_splitItems objectAtIndex:0]];
    
    request.body = getJSONStr(propertyMap);
    
    [self callProtocol:request];
}

-(void) PD306 {
    
    NSString* pseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.PSEQ];
    
    NSDictionary *propertyMap = @{
                                  @"pseq" : pseq
                                  };
    
    NSArray *_splitItems = [NSStringFromSelector(_cmd) componentsSeparatedByString:@":"];
    TXRequestObj *request     = [self createRequest:[_splitItems objectAtIndex:0]];
    
    request.body = getJSONStr(propertyMap);
    [self callProtocol:request];
}

-(void) PD320:(NSDictionary *) propertyMap {
    
    NSArray *_splitItems = [NSStringFromSelector(_cmd) componentsSeparatedByString:@":"];
    TXRequestObj *request     = [self createRequest:[_splitItems objectAtIndex:0]];
    
    request.body = getJSONStr(propertyMap);
    [self callProtocol:request];
}

-(void) PD310:(NSDictionary *) propertyMap {
    
    NSArray *_splitItems = [NSStringFromSelector(_cmd) componentsSeparatedByString:@":"];
    TXRequestObj *request     = [self createRequest:[_splitItems objectAtIndex:0]];
    
    request.body = getJSONStr(propertyMap);
    [self callProtocol:request];
}

-(void) PD311:(NSDictionary *) propertyMap {
    
    NSArray *_splitItems = [NSStringFromSelector(_cmd) componentsSeparatedByString:@":"];
    TXRequestObj *request     = [self createRequest:[_splitItems objectAtIndex:0]];
    
    request.body = getJSONStr(propertyMap);
    [self callProtocol:request];
}

-(BOOL) PD313 {
    
    NSString* pseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.PSEQ];
    NSString* oseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.OSEQ];
    
    NSDictionary *propertyMap = nil;
    @try {
        propertyMap = @{
                        @"pseq" : pseq,
                        @"oseq" : oseq,
                        };
    }
    @catch (NSException *exception) {
        NSLog(@"Exception:%@, %@", exception.name, exception.reason);
        return NO;
    };
    
    NSArray *_splitItems = [NSStringFromSelector(_cmd) componentsSeparatedByString:@":"];
    TXRequestObj *request     = [self createRequest:[_splitItems objectAtIndex:0]];
    
    request.body = getJSONStr(propertyMap);
    [self callProtocol:request];
    return YES;
}

-(void) PD410:(NSDictionary *) propertyMap {
    
    NSArray *_splitItems = [NSStringFromSelector(_cmd) componentsSeparatedByString:@":"];
    TXRequestObj *request     = [self createRequest:[_splitItems objectAtIndex:0]];
    
    request.body = getJSONStr(propertyMap);
    [self callProtocol:request];
}

-(void) PD421:(NSDictionary *) propertyMap {
    
    NSArray *_splitItems = [NSStringFromSelector(_cmd) componentsSeparatedByString:@":"];
    TXRequestObj *request     = [self createRequest:[_splitItems objectAtIndex:0]];
    
    request.body = getJSONStr(propertyMap);
    [self callProtocol:request];
}

-(void) PD330:(NSString*)rsncode {
    
    NSString* pseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.PSEQ];
    NSString* oseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.OSEQ];
    
    NSDictionary *propertyMap = @{
                                  @"pseq" : pseq,
                                  @"oseq" : oseq,
                                  @"rsncode" : rsncode,
                                  };
    
    NSArray *_splitItems = [NSStringFromSelector(_cmd) componentsSeparatedByString:@":"];
    TXRequestObj *request     = [self createRequest:[_splitItems objectAtIndex:0]];
    
    request.body = getJSONStr(propertyMap);
    [self callProtocol:request];
}

-(void) PP402 {
    
    NSString* pseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.PSEQ];
    
    NSDictionary *propertyMap = @{
                                  @"pseq" : pseq
                                  };
    
    NSArray *_splitItems = [NSStringFromSelector(_cmd) componentsSeparatedByString:@":"];
    TXRequestObj *request     = [self createRequest:[_splitItems objectAtIndex:0]];
    
    request.body = getJSONStr(propertyMap);
    [self callProtocol:request];
}

-(void) PP110Auto:(TXUser *) user {
    
    NSDictionary* header = [self getCommonHeader];
    
    NSDictionary *propertyMap = @{
                                  API_JSON.Authenticate.UTELNO : (user.utelno != nil ? user.utelno : [NSNull null]),
                                  API_JSON.Authenticate.UID : (user.uid != nil ? user.uid : [NSNull null]),
                                  API_JSON.ResponseDescriptor.DATOKEN : (user.atoken != nil ? user.atoken : [NSNull null]),
                                  API_JSON.Authenticate.USERNAME : (user.name != nil ? user.name : [NSNull null]),
                                  API_JSON.Authenticate.OTP : (user.otp != nil ? user.otp : [NSNull null]),
                                  //@"languages": DEFAULT_LANGUAGES,
                                  API_JSON.Authenticate.PTOKEN : [header objectForKey:API_JSON.Authenticate.PTOKEN],
                                  API_JSON.Authenticate.DID : [header objectForKey:API_JSON.Authenticate.DID],
                                  API_JSON.Authenticate.MODEL : [header objectForKey:API_JSON.Authenticate.MODEL],
                                  API_JSON.Authenticate.OS : [header objectForKey:API_JSON.Authenticate.OS],
                                  API_JSON.Authenticate.APP : [header objectForKey:API_JSON.Authenticate.APP]
                                  };
    
    if (user.otp == nil) {
        propertyMap = @{
                        API_JSON.Authenticate.UTELNO : (user.utelno != nil ? user.utelno : [NSNull null]),
                        API_JSON.Authenticate.UID : (user.uid != nil ? user.uid : [NSNull null]),
                        API_JSON.ResponseDescriptor.DATOKEN : (user.atoken != nil ? user.atoken : [NSNull null]),
                        API_JSON.Authenticate.PTOKEN : [header objectForKey:API_JSON.Authenticate.PTOKEN],
                        API_JSON.Authenticate.DID : [header objectForKey:API_JSON.Authenticate.DID],
                        API_JSON.Authenticate.MODEL : [header objectForKey:API_JSON.Authenticate.MODEL],
                        API_JSON.Authenticate.OS : [header objectForKey:API_JSON.Authenticate.OS],
                        API_JSON.Authenticate.APP : [header objectForKey:API_JSON.Authenticate.APP]
                        };
    }
    if (user.sns) {
        NSMutableDictionary *property = [propertyMap mutableCopy];
        [property removeObjectForKey:API_JSON.Authenticate.PASSWORD];
        property[@"socialsite"]  = user.sns[@"socialsite"];
        property[@"provider_social"] = [user.sns mutableCopy];
        propertyMap = property;
    }
    
    TXRequestObj *request     = [self createRequest:@"PP110"];
    
    request.body = getJSONStr(propertyMap);
    [self callProtocol:request];
    //[self sendSyncRequest:request];
}

-(void) PP110:(TXUser *) user {
    
    NSDictionary* header = [self getCommonHeader];
    
    NSDictionary *propertyMap = @{
                                  API_JSON.Authenticate.UTELNO : (user.utelno != nil ? user.utelno : [NSNull null]),
                                  API_JSON.Authenticate.UID : (user.uid != nil ? user.uid : [NSNull null]),
                                  API_JSON.Authenticate.PASSWORD : (user.pwd != nil ? user.pwd : [NSNull null]),
                                  API_JSON.Authenticate.USERNAME : (user.name != nil ? user.name : [NSNull null]),
                                  API_JSON.Authenticate.OTP : (user.otp != nil ? user.otp : [NSNull null]),
                                  //@"languages": DEFAULT_LANGUAGES,
                                  API_JSON.Authenticate.PTOKEN : [header objectForKey:API_JSON.Authenticate.PTOKEN],
                                  API_JSON.Authenticate.DID : [header objectForKey:API_JSON.Authenticate.DID],
                                  API_JSON.Authenticate.MODEL : [header objectForKey:API_JSON.Authenticate.MODEL],
                                  API_JSON.Authenticate.OS : [header objectForKey:API_JSON.Authenticate.OS],
                                  API_JSON.Authenticate.APP : [header objectForKey:API_JSON.Authenticate.APP]
                                  };
    
    if (user.otp == nil) {
        propertyMap = @{
                        API_JSON.Authenticate.UTELNO : (user.utelno != nil ? user.utelno : [NSNull null]),
                        API_JSON.Authenticate.UID : (user.uid != nil ? user.uid : [NSNull null]),
                        API_JSON.Authenticate.PTOKEN : [header objectForKey:API_JSON.Authenticate.PTOKEN],
                        API_JSON.Authenticate.DID : [header objectForKey:API_JSON.Authenticate.DID],
                        API_JSON.Authenticate.MODEL : [header objectForKey:API_JSON.Authenticate.MODEL],
                        API_JSON.Authenticate.OS : [header objectForKey:API_JSON.Authenticate.OS],
                        API_JSON.Authenticate.APP : [header objectForKey:API_JSON.Authenticate.APP]
                        };
    }
    if (user.sns) {
        NSMutableDictionary *property = [propertyMap mutableCopy];
        [property removeObjectForKey:API_JSON.Authenticate.PASSWORD];
        property[@"socialsite"]  = user.sns[@"socialsite"];
        property[@"provider_social"] = [user.sns mutableCopy];
        propertyMap = property;
    }
    
    NSArray *_splitItems = [NSStringFromSelector(_cmd) componentsSeparatedByString:@":"];
    TXRequestObj *request     = [self createRequest:[_splitItems objectAtIndex:0]];
    
    request.body = getJSONStr(propertyMap);
    [self callProtocol:request];
    //[self sendSyncRequest:request];
}

-(void) PP111:(TXUser *) user {
    
    NSDictionary* header = [self getCommonHeader];
    
    NSDictionary *propertyMap = @{
                                  API_JSON.Authenticate.UID : (user.uid != nil ? user.uid : [NSNull null]),
                                  API_JSON.Authenticate.UTELNO : (user.utelno != nil ? user.utelno : [NSNull null]),
                                  API_JSON.Authenticate.PASSWORD : (user.pwd != nil ? user.pwd : [NSNull null]),
                                  API_JSON.Authenticate.USERNAME : (user.name != nil ? user.name : [NSNull null]),
                                  API_JSON.Authenticate.OTP : (user.otp != nil ? user.otp : [NSNull null]),
                                  
                                  API_JSON.Authenticate.PTOKEN : [header objectForKey:API_JSON.Authenticate.PTOKEN],
                                  API_JSON.Authenticate.DID : [header objectForKey:API_JSON.Authenticate.DID],
                                  API_JSON.Authenticate.MODEL : [header objectForKey:API_JSON.Authenticate.MODEL],
                                  API_JSON.Authenticate.OS : [header objectForKey:API_JSON.Authenticate.OS],
                                  API_JSON.Authenticate.APP : [header objectForKey:API_JSON.Authenticate.APP]
                                  };
    
    NSArray *_splitItems = [NSStringFromSelector(_cmd) componentsSeparatedByString:@":"];
    TXRequestObj *request     = [self createRequest:[_splitItems objectAtIndex:0]];
    //    NSLog(@"%@", NSStringFromSelector(_cmd));
    
    request.body = getJSONStr(propertyMap);
    [self callProtocol:request];
}


-(void) PP112:(NSString*)ptoken {
    
    NSDictionary* header = [self getCommonHeader];
    
    NSDictionary *propertyMap = @{
                                  API_JSON.Authenticate.UID : [header objectForKey:API_JSON.Authenticate.UID],
                                  API_JSON.Authenticate.PTOKEN : ptoken,
                                  API_JSON.Authenticate.DID : [header objectForKey:API_JSON.Authenticate.DID],
                                  API_JSON.Authenticate.MODEL : [header objectForKey:API_JSON.Authenticate.MODEL],
                                  API_JSON.Authenticate.OS : [header objectForKey:API_JSON.Authenticate.OS],
                                  API_JSON.Authenticate.APP : [header objectForKey:API_JSON.Authenticate.APP]
                                  };
    
    NSArray *_splitItems = [NSStringFromSelector(_cmd) componentsSeparatedByString:@":"];
    TXRequestObj *request     = [self createRequest:[_splitItems objectAtIndex:0]];
    
    request.body = getJSONStr(propertyMap);
    [self callProtocol:request];
}

-(void) PD015:(NSDictionary *) propertyMap  {
    
    NSArray *_splitItems = [NSStringFromSelector(_cmd) componentsSeparatedByString:@":"];
    TXRequestObj *request     = [self createRequest:[_splitItems objectAtIndex:0]];
    
    request.body = getJSONStr(propertyMap);
    [self callProtocol:request];
}

-(void) PP114 {
    
    [self PP114:YES];
}
    
-(void) PP114:(BOOL)session {
    
    if (session) {
        // 데이터 초기화
        TXAppDelegate *appDelegate = (TXAppDelegate *)[[UIApplication sharedApplication] delegate];
        [appDelegate initProtocolMessage];
    }
    
    NSString* pseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.PSEQ];
    
    NSDictionary *propertyMap = @{
                                  @"pseq" : pseq,
                                  };
    
    NSArray *_splitItems = [NSStringFromSelector(_cmd) componentsSeparatedByString:@":"];
    TXRequestObj *request     = [self createRequest:[_splitItems objectAtIndex:0]];
    
    request.body = getJSONStr(propertyMap);
    [self callProtocol:request];
}


-(void) PP120:(TXUser *) user {
    
    NSDictionary* header = [self getCommonHeader];
    
    NSDictionary *propertyMap = @{
                                  API_JSON.Authenticate.UTELNO : (user.utelno != nil ? user.utelno : [NSNull null]),
                                  API_JSON.Authenticate.UID : (user.uid != nil ? user.uid : [NSNull null]),
                                  API_JSON.Authenticate.PASSWORD : (user.pwd != nil ? user.pwd : [NSNull null]),
                                  API_JSON.Authenticate.OTP : (user.otp != nil ? user.otp : [NSNull null]),
                                  //@"languages": DEFAULT_LANGUAGES,
                                  API_JSON.Authenticate.USERNAME : (user.name != nil ? user.name : [NSNull null]),
                                  API_JSON.Authenticate.PTOKEN : [header objectForKey:API_JSON.Authenticate.PTOKEN],
                                  API_JSON.Authenticate.DID : [header objectForKey:API_JSON.Authenticate.DID],
                                  API_JSON.Authenticate.MODEL : [header objectForKey:API_JSON.Authenticate.MODEL],
                                  API_JSON.Authenticate.OS : [header objectForKey:API_JSON.Authenticate.OS],
                                  API_JSON.Authenticate.APP : [header objectForKey:API_JSON.Authenticate.APP],
//#ifdef _CHACHA1 // DEBUG
//                                  @"provider_agree":@{
//                                          @"service_utility" : @"1",
//                                          @"private_information" : @"1",
//                                          @"private_location" : @"1"
//                                          }
//#endif
                                  };
    
    if (user.sns) {
        NSMutableDictionary *property = [propertyMap mutableCopy];
        [property removeObjectForKey:API_JSON.Authenticate.PASSWORD];
        property[@"socialsite"]  = user.sns[@"socialsite"];
        property[@"provider_social"] = [user.sns mutableCopy];
        propertyMap = property;
    }
    
    NSArray *_splitItems = [NSStringFromSelector(_cmd) componentsSeparatedByString:@":"];
    TXRequestObj *request     = [self createRequest:[_splitItems objectAtIndex:0]];
    
    request.body = getJSONStr(propertyMap);
    [self callProtocol:request];
}

-(void) PD035:(NSDictionary *) propertyMap  {
    
    NSArray *_splitItems = [NSStringFromSelector(_cmd) componentsSeparatedByString:@":"];
    TXRequestObj *request     = [self createRequest:[_splitItems objectAtIndex:0]];
    
    request.body = getJSONStr(propertyMap);
    [self callProtocol:request];
}

-(void) PP601 {
    
    TXAppDelegate *appDelegate = (TXAppDelegate *)[[UIApplication sharedApplication] delegate];
    if (appDelegate.pictureSizeRider == nil) {
        [Utils setStateUIInfo:UI_STATE_PP601 value:UI_STATE_STEP_WAIT oldvalue:@""];
        return;
    }
    //NSDictionary *user = [appDelegate.dicRider objectForKey:@"user"];
    //NSString* useq = [user valueForKey:@"useq"];
    
    NSString* useq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.USEQ];
    //NSString* oseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.OSEQ];
    
    //NSDictionary *user = [appDelegate.dicRider objectForKey:@"user"];
    
    NSDictionary *propertyMap = @{
                                  @"useq" : useq,
                                  @"picture" : appDelegate.pictureSizeRider,
                                  };
    
    NSArray *_splitItems = [NSStringFromSelector(_cmd) componentsSeparatedByString:@":"];
    TXRequestObj *request     = [self createRequest:[_splitItems objectAtIndex:0]];
    
    request.body = getJSONStr(propertyMap);
    //[request.reqConfig setUseCache:YES];
    
    [self callProtocol:request];
}

-(void) PP602 {
    
    TXAppDelegate *appDelegate = (TXAppDelegate *)[[UIApplication sharedApplication] delegate];
    if (appDelegate.pictureSizeDriver == nil) {
        return;
    }
    NSString* pseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.PSEQ];
    //NSString* oseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.OSEQ];
    
    //NSDictionary *user = [appDelegate.dicDriver objectForKey:@"provider"];
    
    NSDictionary *propertyMap = @{
                                  @"pseq" : pseq,
                                  @"picture" : appDelegate.pictureSizeDriver,
                                  };
    
    NSArray *_splitItems = [NSStringFromSelector(_cmd) componentsSeparatedByString:@":"];
    TXRequestObj *request     = [self createRequest:[_splitItems objectAtIndex:0]];
    
    request.body = getJSONStr(propertyMap);
    
    [self callProtocol:request];
}

-(void) PP601:(NSDictionary *)propertyMap {
    
    NSArray *_splitItems = [NSStringFromSelector(_cmd) componentsSeparatedByString:@":"];
    TXRequestObj *request     = [self createRequest:[_splitItems objectAtIndex:0]];
    
    request.body = getJSONStr(propertyMap);
    
    [self callProtocol:request];
}

-(void) PP602:(NSDictionary *)propertyMap {
    
    NSArray *_splitItems = [NSStringFromSelector(_cmd) componentsSeparatedByString:@":"];
    TXRequestObj *request     = [self createRequest:[_splitItems objectAtIndex:0]];
    
    request.body = getJSONStr(propertyMap);
    
    [self callProtocol:request];
}

-(void) PP603:(NSDictionary *)propertyMap {
    
    NSArray *_splitItems = [NSStringFromSelector(_cmd) componentsSeparatedByString:@":"];
    TXRequestObj *request     = [self createRequest:[_splitItems objectAtIndex:0]];
    
    request.body = getJSONStr(propertyMap);
    
    [self callProtocol:request];
}

-(void) UR621:(NSDictionary *)propertyMap {
    
    NSArray *_splitItems = [NSStringFromSelector(_cmd) componentsSeparatedByString:@":"];
    TXRequestObj *request     = [self createRequest:[_splitItems objectAtIndex:0]];
    
    request.body = getJSONStr(propertyMap);
    
    [self callProtocol:request];
}

-(void) PP200 {
    
    //TXAppDelegate *appDelegate = (TXAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString* pseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.PSEQ];
//    NSDictionary *user = [appDelegate.dicDrive objectForKey:@"provider_device"];
//    NSString* pseq = [user valueForKey:@"pseq"];
    
    NSDictionary *propertyMap = @{
                                  @"pseq" : pseq
                                  };
    
    NSArray *_splitItems = [NSStringFromSelector(_cmd) componentsSeparatedByString:@":"];
    TXRequestObj *request     = [self createRequest:[_splitItems objectAtIndex:0]];
    
    request.body = getJSONStr(propertyMap);
    
    [self callProtocol:request];
}

-(void) PP210:(NSDictionary *) propertyMap {
    
    NSArray *_splitItems = [NSStringFromSelector(_cmd) componentsSeparatedByString:@":"];
    TXRequestObj *request     = [self createRequest:[_splitItems objectAtIndex:0]];
    
    request.body = getJSONStr(propertyMap);
    [self callProtocol:request];
}

-(void) PP211:(NSDictionary *) propertyMap {
    
    NSArray *_splitItems = [NSStringFromSelector(_cmd) componentsSeparatedByString:@":"];
    TXRequestObj *request     = [self createRequest:[_splitItems objectAtIndex:0]];
    
    request.body = getJSONStr(propertyMap);
    [self callProtocol:request];
}


-(void) PP501:(NSString*)fseq {
    
    NSString* pseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.PSEQ];
    
    NSDictionary *propertyMap = @{
                                  @"pseq" : pseq
                                  };
    if (![fseq isEqualToString:@""]) {
        propertyMap = @{
                        @"pseq" : pseq,
                        @"fseq" : fseq
                        };
    }
    
    NSArray *_splitItems = [NSStringFromSelector(_cmd) componentsSeparatedByString:@":"];
    TXRequestObj *request     = [self createRequest:[_splitItems objectAtIndex:0]];
    
    request.body = getJSONStr(propertyMap);
    [self callProtocol:request];
}

-(void) PP502:(NSString*)qseq {
    
    NSString* pseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.PSEQ];
    
    NSDictionary *propertyMap = @{
                                  @"pseq" : pseq
                                  };
    if (![qseq isEqualToString:@""]) {
        propertyMap = @{
                        @"pseq" : pseq,
                        @"qseq" : qseq
                        };
    }
    
    NSArray *_splitItems = [NSStringFromSelector(_cmd) componentsSeparatedByString:@":"];
    TXRequestObj *request     = [self createRequest:[_splitItems objectAtIndex:0]];
    
    request.body = getJSONStr(propertyMap);
    [self callProtocol:request];
}

-(void) PP503 {
    
    NSString* pseq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.PSEQ];
    
    NSDictionary *propertyMap = @{
                                  @"pseq" : pseq
                                  };
    
    NSArray *_splitItems = [NSStringFromSelector(_cmd) componentsSeparatedByString:@":"];
    TXRequestObj *request     = [self createRequest:[_splitItems objectAtIndex:0]];
    
    request.body = getJSONStr(propertyMap);
    [self callProtocol:request];
}

#ifdef _WITZM
#elif defined _CHACHA
#else
#pragma mark BrainTree
-(void)BTREE:(NSDictionary *) propertyMap {
    
//    NSLog(@"clientToken:%@",[propertyMap valueForKey:@"clientToken"]);
    // Get your tokenization key from the control panel
    // or fetch a client token
    //BTAPIClient *braintreeClient = [[BTAPIClient alloc] initWithAuthorization:[propertyMap valueForKey:@"clientToken"]];
    TXAppDelegate *appDelegate = (TXAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    BTAPIClient *braintreeClient = [[BTAPIClient alloc] initWithAuthorization:appDelegate.paykey];//
    BTCardClient *cardClient = [[BTCardClient alloc] initWithAPIClient:braintreeClient];
    BTCard *card = [[BTCard alloc] initWithNumber:[propertyMap valueForKey:@"cardNumber"]
                                  expirationMonth:[propertyMap valueForKey:@"month"]
                                   expirationYear:[propertyMap valueForKey:@"year"]
                                              cvv:[propertyMap valueForKey:@"cvv"]];
    
    NSLog(@"%@,%@,%@",[propertyMap valueForKey:@"cardNumber"],[propertyMap valueForKey:@"month"],[propertyMap valueForKey:@"year"]);
    NSString* midx = [propertyMap valueForKey:@"mid"];
    NSString* cseq = [propertyMap valueForKey:@"cseq"];
    NSString* oseq = [propertyMap valueForKey:@"oseq"];
    NSString* tip  = [propertyMap valueForKey:@"tip"];
    
    NSString* useq = [[[TXApp instance] getSettings] getFDKeychain:SettingsConst.CryptoKeys.USEQ];
    
    [cardClient tokenizeCard:card
                  completion:^(BTCardNonce *tokenizedCard, NSError *error) {
                      // Communicate the tokenizedCard.nonce to your server, or handle error
                      NSLog(@"%@ nonce=%@",midx,tokenizedCard.nonce);
                      if ([midx isEqualToString:@"UR311"]) {
                          //
                          NSDictionary *propertyMap = @{
                                                        @"useq" : useq,
                                                        @"cseq" : cseq,
//                                                        @"pay_balance" : @"0.0",
                                                        @"pay_nonce" : tokenizedCard.nonce,
                                                        };
                          [self UR311:propertyMap];
                      }
                      else if ([midx isEqualToString:@"UD413"]) {
                          //
                          NSDictionary *propertyMap = @{
                                                        @"useq" : useq,
                                                        @"oseq" : oseq,
                                                        @"pay_tip" : tip,
                                                        @"pay_nonce" : tokenizedCard.nonce,
                                                        };
                          [self UD413:propertyMap];
                      }
                      else if ([midx isEqualToString:@"UD112"]) {
                          //
                          NSDictionary *propertyMap = @{
                                                        @"useq" : useq,
                                                        @"oseq" : oseq,
                                                        @"pay_nonce" : tokenizedCard.nonce,
                                                        };
                          [self UD112:propertyMap];
                      }
                  }];
}
#endif

-(void)confirm:(int) userId code:(int) code {
    
    TXRequestObj *request     = [self createRequest:HttpAPIConsts.CONFIRM];
    NSDictionary *propertyMap = @{
                                    API_JSON.Request.USERID : [NSNumber numberWithInt:userId],
                                    API_JSON.VERIFICATIONCODE : [NSNumber numberWithInt:code]
                                 };
    
    request.body = getJSONStr(propertyMap);
    [self callProtocol:request];
    
}

-(void)resendVerificationCode:(int) userId {
 
    TXRequestObj *request            = [self createRequest:HTTP_API.USER];
    request.body = getJSONStr(@{ API_JSON.Request.USERID : [NSNumber numberWithInt:userId] });
    [self callProtocol:request];
}

-(void)updateMobile:(int) userId mobile:(NSString *)mobile {
    
    TXRequestObj *request            = [self createRequest:HttpAPIConsts.UPDATEUSERMOBILE];
    
    NSDictionary *propertyMap = @{
                                    API_JSON.Request.USERID : [NSNumber numberWithInt:userId],
                                    API_JSON.SignUp.PHONENUMBER : mobile
                                  };
    
    request.body = getJSONStr(propertyMap);
    [self callProtocol:request];
}

-(void)logout {
    
}

-(void)deleteUser {
    
}

-(void)onEvent:(TXEvent *)event eventParams:(id)subscriptionParams {
    
    [self fireEvent:[self onEventManual:event eventParams:subscriptionParams]];
}

-(TXEvent*)onEventManual:(TXEvent *)event eventParams:(id)subscriptionParams {
    
    @try {
        if([event.name isEqualToString:TXEvents.HTTPREQUESTCOMPLETED]) {
            
            TXRequestObj *request = [event getEventProperty:TXEvents.Params.REQUEST];
            TXResponseDescriptor *descriptor = [event getEventProperty:TXEvents.Params.DESCRIPTOR];
            
            TXEvent *event = [TXEvent createEvent:request.reqConfig.name
                                      eventSource:self
                                       eventProps:@{ TXEvents.Params.DESCRIPTOR : descriptor,
                                                     TXEvents.Params.REQUEST : request}];
            return event;
        }
        else {
            //        NSLog(@"event.name:%@",event.name);
            
            TXRequestObj *request = [event getEventProperty:TXEvents.Params.REQUEST];
            TXResponseDescriptor *descriptor = [event getEventProperty:TXEvents.Params.DESCRIPTOR];
            
            descriptor.success = NO;
            descriptor.orgrlt = descriptor.rlt;
            descriptor.rlt = -999999;
            descriptor.error = event.name;
            
            TXEvent *event = [TXEvent createEvent:request.reqConfig.name
                                      eventSource:self
                                       eventProps:@{ TXEvents.Params.DESCRIPTOR : descriptor,
                                                     TXEvents.Params.REQUEST : request}];
            return event;
        }
    }
    @catch (NSException *exception) {
        NSLog(@"Exception event.name:%@",event.name);
        NSLog(@"Exception:%@, %@", exception.name, exception.reason);

        if ([SVProgressHUD isVisible])
            [SVProgressHUD dismiss];
        
//        dispatch_async(dispatch_get_main_queue(), ^{
//            LGAlertView *alertView = [[LGAlertView alloc] initWithTitle:@"Error"
//                                                                message:event.name
//                                                                  style:LGAlertViewStyleAlert
//                                                           buttonTitles:@[LocalizedStr(@"Button.OK")]
//                                                      cancelButtonTitle:nil
//                                                 destructiveButtonTitle:nil
//                                                          actionHandler:^(LGAlertView *alertView, NSString *title, NSUInteger index) {
//                                                          }
//                                                          cancelHandler:^(LGAlertView *alertView) {
//                                                          }
//                                                     destructiveHandler:^(LGAlertView *alertView) {
//                                                     }];
//            
//            [Utils initAlertButtonColor:alertView];
//            [alertView showAnimated:YES completionHandler:^(void)
//             {
//             }];
//        });
        
//        TXRequestObj *request = [event getEventProperty:TXEvents.Params.REQUEST];
//        TXResponseDescriptor *descriptor = [TXResponseDescriptor create:NO rlt:-999999 code:request.reqConfig.name error:event.name atoken:@""];
//        
//        TXEvent *event = [TXEvent createEvent:request.reqConfig.name
//                                  eventSource:self
//                                   eventProps:@{ TXEvents.Params.DESCRIPTOR : descriptor,
//                                                 TXEvents.Params.REQUEST : request}];
//        return event;
    }
//    @finally {
        return nil;
//    }
}

@end
