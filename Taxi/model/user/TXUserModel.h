//
//  TXUserModel.h
//  Taxi
//

//

#import "TXBaseObj.h"
#import "TXModelBase.h"

#ifdef _WITZM
#elif defined _CHACHA
#else
#import "BraintreeCore.h"
#endif

extern const struct UserConsts {
    
    __unsafe_unretained NSString* UID;
    __unsafe_unretained NSString* NAME;
    __unsafe_unretained NSString* UTELNO;
    __unsafe_unretained NSString* PTOKEN;
    __unsafe_unretained NSString* ATOKEN;
    __unsafe_unretained NSString* DID;
    __unsafe_unretained NSString* MODEL;
    __unsafe_unretained NSString* OS;
    __unsafe_unretained NSString* APP;
    
    __unsafe_unretained NSString* PASSWORD;
    __unsafe_unretained NSString* OTP;
    __unsafe_unretained NSString* SNS;
    
} UserConsts;

@interface TXUser : TXBaseObj

@property (nonatomic, retain) NSString* uid;
@property (nonatomic, retain) NSString* name;
@property (nonatomic, retain) NSString* utelno;
@property (nonatomic, retain) NSString* ptoken;
@property (nonatomic, retain) NSString* atoken;
@property (nonatomic, retain) NSString* did;
@property (nonatomic, retain) NSString* model;
@property (nonatomic, retain) NSString* os;
@property (nonatomic, retain) NSString* app;

@property (nonatomic, retain) NSString* pwd;
@property (nonatomic, retain) NSString* otp;
@property (nonatomic, retain) NSDictionary* sns;

@end

@interface TXUserModel : TXModelBase

/** Creates the single instance within the application
 
 @return TXUserModel
 */
+(TXUserModel *) instance;

-(void) deleteUser;
/******************************************************/
-(void) requsetAPI:(NSString*)code property:(NSDictionary *) propertyMap;
-(void) uploadAPI:(NSString*)code property:(NSDictionary *) propertyMap;
-(void) requestSync:(NSString*)proto;

-(void) PP110Auto:(TXUser *) user;
-(void) UR110Auto:(TXUser *) user;

// Rider API
-(void) logout;
-(void) UR110:(TXUser *) user;
-(void) UR111:(TXUser *) user;

-(void) UR112:(NSString*)ptoken;
-(void) UR114:(TXUser *) user;
-(void) UR114:(TXUser *) user withSession:(BOOL)session;
-(void) UR120:(TXUser *) user;
-(void) UR200;
-(void) UR210:(NSDictionary *) propertyMap;
-(void) UR211:(NSDictionary *) propertyMap;
-(void) UR311:(NSDictionary *) propertyMap;
-(void) UR300;
-(void) UR303;
-(void) UR310:(NSString*)cseq;
-(void) UR323:(NSString*)code;
-(void) UR332:(NSString*)cseq;
-(void) UR331:(NSString*)qseq;
-(void) UR501:(NSString*)fseq;
-(void) UR503;

#ifdef _WITZM
#elif defined _CHACHA
#else
-(void) BTREE:(NSDictionary *) propertyMap;
#endif

-(void) UQ002;
-(void) UQ100;
-(void) UD110;
-(void) UQ101:(NSDictionary *) propertyMap;
-(void) UD112:(NSDictionary *) propertyMap;
-(void) UD120:(NSDictionary *) propertyMap;
-(void) UD130;
-(void) UD131;
-(void) UQ200;
-(void) UD210;
-(void) UD230;
-(void) UD310;
-(void) UQ300;
-(void) UR301:(NSString*)oseq;
-(void) UD302:(NSString*)oseq;
-(void) UD322:(NSString*)utelno message:(NSString*)message;
-(void) UD306;
-(void) UD413:(NSDictionary *) propertyMap;
-(void) UD410:(NSDictionary *) propertyMap;
-(void) UD421:(NSDictionary *) propertyMap;
-(void) UD423;

/******************************************************/
// Provider API
-(void) PQ002;
-(void) PQ100;
-(void) PD110;
-(void) PD130:(NSString*)rsncode;
-(void) PD131;
-(void) PQ200;
-(void) PD220:(NSDictionary *) propertyMap;
-(void) PD211:(NSDictionary *) propertyMap;
-(void) PD213;
-(void) PD232:(NSString*)rsncode;

-(void) PQ300;
-(void) PP301:(NSString*)oseq;
-(void) PD304:(NSString*)oseq;
-(void) PD305:(NSString*)oseq;
-(void) PD306;
-(void) PD320:(NSDictionary *) propertyMap;
-(void) PD310:(NSDictionary *) propertyMap;
-(void) PD311:(NSDictionary *) propertyMap;
-(BOOL) PD313;
-(void) PD410:(NSDictionary *) propertyMap;
-(void) PD421:(NSDictionary *) propertyMap;
-(void) PD330:(NSString*)rsncode;
-(void) PP402;

-(void) PD351;
-(void) PP110:(TXUser *) user;
-(void) PP111:(TXUser *) user;
-(void) PP112:(NSString*)ptoken;
-(void) PD015:(NSDictionary *) propertyMap;
-(void) PP120:(TXUser *) user;
-(void) PD035:(NSDictionary *) propertyMap;
-(void) PP114;
-(void) PP114:(BOOL)session;
-(void) PP200;

-(void) PP211:(NSDictionary *) propertyMap;
-(void) PP210:(NSDictionary *) propertyMap;
-(void) PP501:(NSString*)fseq;
-(void) PP502:(NSString*)qseq;
-(void) PP503;

/******************************************************/
// IMAGE API
-(void) UR601;
-(void) UR602;
-(void) UR601:(NSDictionary *)propertyMap;
-(void) UR602:(NSDictionary *)propertyMap;
-(void) UR603:(NSDictionary *)propertyMap;
-(void) PP601;
-(void) PP602;
-(void) PP601:(NSDictionary *)propertyMap;
-(void) PP602:(NSDictionary *)propertyMap;
-(void) PP603:(NSDictionary *)propertyMap;

-(void) UR621:(NSDictionary *)propertyMap;

-(void) checkIfPhoneNumberBlocked:(NSString *) phoneNum loginWithProvider: (BOOL) loginWithProvider;
-(void)confirm:(int) userId code:(int) code;
-(void) resendVerificationCode:(int) userId;
-(void) updateMobile:(int) userId mobile:(NSString *)mobile;
-(TXEvent*)onEventManual:(TXEvent *)event eventParams:(id)subscriptionParams;

#ifdef _WITZM
#elif defined _CHACHA
#else
@property (nonatomic, strong) BTAPIClient *braintreeClient;
#endif

@end
