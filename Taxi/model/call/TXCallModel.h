//
//  TXCallModel.h
//  Taxi

//

#import "TXModelBase.h"

@interface TXCallModel : TXModelBase

/** Creates the single instance within the application
 
 @return TXCallModel
 */
+(TXCallModel *) instance;

-(void) requestChargeForCountry:(NSString *) country distance:(long) distance;

@end
