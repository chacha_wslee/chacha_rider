//
//  TXModelBase.h
//  Taxi
//

//

#import <Foundation/Foundation.h>
#import "TXHttpRequestManager.h"
#import "TXEventTarget.h"
#import "StrConsts.h"
#import "utils.h"
#import "TXApp.h"

@interface TXResponseDescriptor : NSObject

@property (nonatomic, assign) BOOL      success;
@property (nonatomic, assign) int       orgrlt;
@property (nonatomic, assign) int       rlt;
@property (nonatomic, weak)   NSString* code;
@property (nonatomic, weak)   NSString* error;
@property (nonatomic, weak)   id        atoken;

+(id) create:(BOOL) succcess_ rlt:(int)rlt_ code:(NSString*) code_ error:(NSString *) error_ atoken:(id) atoken_;
+(id) create:(BOOL) succcess_ rlt:(int)rlt_ code:(NSString*) code_;

@end

@class TXSharedObj;

@interface TXModelBase : TXEventTarget <TXHttpRequestListener, TXEventListener> {
    TXHttpRequestManager *httpMgr;
}

@property (nonatomic, strong) TXSharedObj *application;

-(TXRequestObj *) createRequest:(NSString *) config;
-(void)sendAsyncRequest:(TXRequestObj *) request;
-(BOOL)sendSyncRequest:(TXRequestObj *) request;
-(TXApp *)getApp;
-(void)onCleanUseqSession;
-(void)onCleanPseqSession;
-(void)onCleanSession;
-(void)onRequestCompleted:(id)object;
-(TXEvent*)requestCompleted:(TXRequestObj*)object reqponse:(NSDictionary*)responseObj;
-(void)onFail:(id)object error:(TXError *)error;

@end
