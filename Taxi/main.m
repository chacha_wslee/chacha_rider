 //
//  main.m
//  Taxi
//

//

#import <UIKit/UIKit.h>

#import "TXAppDelegate.h"
#import "_TB_ChaCha-Swift.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
#ifdef REFACTORING_CHACHA
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
#else
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([TXAppDelegate class]));
#endif
    }
}
